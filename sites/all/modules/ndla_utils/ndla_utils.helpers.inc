<?php
/**
 * @file
 * @ingroup ndla_utils
 * @brief
 *  This file contains various helper functions.
 */

// Define vocabulary for classes (trinn).
define('NDLA_UTILS_CLASS_VOCAB', 21);

/**
 * Menu items shared between the red and disp module.
 */
function _ndla_utils_common_menu_items() {
  $items = array();

  $items['news/history'] = array(
    'page callback' => 'ndla_utils_view_slideshow_history',
    'access arguments' => array('access content'),
    'file' => 'ndla_utils.pages.inc',
    'title' => t('News items'),
    'type' => MENU_CALLBACK,
  );

  $items['node/%/embed'] = array(
    'page callback' => 'ndla_utils_view_embed',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    'file' => 'ndla_utils.pages.inc',
    'type' => MENU_CALLBACK,
  );

	$items['ndla_utils/itslearning_embed_details/%'] = array(
		'page callback' => '_ndla_utils_itslearning_embed_get_details',
		'page arguments' => array(2),
		'access callback' => TRUE,
		'access arguments' => array('access content'),
		'file' => 'ndla_utils.pages.inc',
		'type' => MENU_CALLBACK,
	);
	$items['ndla_utils/itslearning_js'] = array(
		'page callback' => '_ndla_utils_itslearning_get_js',
		'access callback' => TRUE,
		'access arguments' => array('access content'),
		'file' => 'ndla_utils.pages.inc',
		'type' => MENU_CALLBACK,
	);

  $items['ndla-utils/grep/%'] = array(
    'page callback' => 'ndla_utils_grep_callback',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'file' => 'ndla_utils.pages.inc',
    'type' => MENU_CALLBACK,
  );
  
  $items['node/%node/grep'] = array(
		'page callback' => 'ndla_utils_node_grep_callback',
		'page arguments' => array(1),
		'access callback' => 'node_access',
		'access arguments' => array('view', 1),
		'file' => 'ndla_utils.pages.inc',
		'type' => MENU_CALLBACK,
	);
  
  $items['ndla_utils/youtube/%'] = array(
    'page callback' => 'ndla_utils_print_youtube_iframe',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'file' => 'ndla_utils.pages.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/ndla-utils/ndla2010-theme'] = array(
    'title' => 'NDLA Utils NDLA2010 theme settings',
    'description' => "Change the behavior of the NDLA2010 theme",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_utils_ndla2010_theme_admin'),
    'access arguments' => array('theme settings'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'ndla_utils.admin.inc'
  );

  $items['admin/settings/ndla-utils/slideshow-frontpage'] = array(
    'title' => 'Frontpage slideshow settings',
    'description' => "Manage nodes in the slideshow displayed on the frontpage",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_utils_slideshow_admin'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'ndla_utils.admin.inc'
  );
  $items['admin/settings/ndla-utils/slideshow-frontpage/add'] = array(
    'title' => t('Add'),
    'description' => "Manage nodes in the slideshow displayed on the frontpage",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_utils_slideshow_admin'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'ndla_utils.admin.inc'
  );
  $items['admin/settings/ndla-utils/slideshow-frontpage/delete'] = array(
    'title' => t('Delete'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_utils_slideshow_admin_delete'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'ndla_utils.admin.inc'
  );

  $items['admin/settings/ndla-utils/ndla2010-theme/new-content-node-types'] = array(
    'title' => 'NDLA Utils NDLA2010 theme new content node types',
    'description' => "Select which node types that can be used on front pages.",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_utils_ndla2010_theme_new_content_node_types_admin'),
    'access arguments' => array('select new content node types'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'ndla_utils.admin.inc'
  );

  $items['admin/settings/ndla_utils_node_type_order'] = array(
    'title' => t('NDLA Node type ordering'),
    'description' => t('Make it possible to create a re-usable node type order for lists'),
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_utils_node_type_order_setting'),
    'file' => 'ndla_utils.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['ndla-utils/ndla2010-theme/xhr/new-content-node-types'] = array(
    'title' => 'NDLA Utils NDLA2010 theme XHR new content node types',
    'type' => MENU_CALLBACK,
    'page callback' => 'ndla_utils_ndla2010_theme_xhr_new_content_types',
    'access arguments' => array('access content')
  );

  $items['ndla-utils/ndla2010-theme/xhr/courses'] = array(
    'title' => 'NDLA Utils NDLA2010 theme XHR courses',
    'type' => MENU_CALLBACK,
    'page callback' => 'ndla_utils_ndla2010_theme_xhr_courses',
    'access arguments' => array('access content')
  );
  
  $items['ndla-utils/ndla2010-theme/xhr/nodes'] = array(
    'title' => 'NDLA Utils NDLA2010 theme XHR nodes',
    'type' => MENU_CALLBACK,
    'page callback' => 'ndla_utils_ndla2010_theme_xhr_nodes',
    'access arguments' => array('access content')
  );

  $items['userguide'] = array(
    'title' => 'User Guide',
    'type' => MENU_CALLBACK,
    'page callback' => 'ndla_utils_user_guide',
    'access arguments' => array('access content'),
    'file' => 'ndla_utils.pages.inc',
  );

  if(module_exists('readspeaker_hl')) {
    $items['admin/settings/readspeaker'] = array(
      'title' => t('Readspeaker dont read settings'),
      'type' => MENU_NORMAL_ITEM,
      'access arguments' => array('administer site configuration'),
      'file' => 'ndla_utils.pages.inc',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('ndla_utils_readspeaker_dont_read_setting'),
    );
  }
  
  $items['admin/settings/ndla-facebook'] = array(
    'title' => t('NDLA Facebook'),
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('administer site configuration'),
    'file' => 'ndla_utils.pages.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_utils_facebook_admin'),
  );
  
  $items['admin/settings/ndla-google-plus'] = array(
    'title' => t('NDLA Google Plus'),
    'access arguments' => array('administer site configuration'),
    'file' => 'ndla_utils.pages.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_utils_googleplus_admin'),
    'type' => MENU_NORMAL_ITEM,
  );
    
	return $items;
}

/**
 * Adds the needed javascript files to a page.
 */
function ndla_utils_add_js_files() {
  $module_path = drupal_get_path('module', 'ndla_utils');
  if(empty($module_path)) {
    $module_path = drupal_get_path('module', 'ndla_utils_disp');
  }

  global $language, $base_url;
  $vars = array();
  $vars['language'] = $language->language;
  $vars['base_url'] = $base_url;
  $vars['theme_url'] = $base_url . "/" . path_to_theme();
  $vars['node_type'] = arg(0) != '' ? arg(0) : 'node';
  $vars['tid'] = is_numeric(arg(3)) ? arg(3) : 0;
  $vars['nid'] = is_numeric(arg(1)) ? arg(1) : 0;

  $languages = array();
  foreach(language_list() as $key => $lang) {
    $languages[$key] = t($lang->name);
  }
  $languages['zh-HANS'] = t('Chinese simplified');
  $languages['zh-HANT'] = t('Chinese traditional');
  $languages['de'] = t('German');
  $vars['languages'] = $languages;

  if(arg(2) == 'grep') {
  	$vars['uuid'] = arg(3);
  }
  if (module_exists('htmlvideo')) {
    $vars['htmlvideoLibraryUrl'] = base_path() . drupal_get_path('module', 'htmlvideo') . '/video-js/video.js';
    $vars['htmlvideoCSSUrl'] = base_path() . drupal_get_path('module', 'htmlvideo') . '/video-js/video-js.css';
  }
  
  drupal_add_js(array('ndla_utils' => $vars), 'setting');
}

/**
 * A quick node_load function
 *
 * @param $nid
 *  Node id
 * @param $vid
 *  Version id. If left empty the currently published vid will be used.
 * @return
 *  A simplified node object.
 */
function ndla_utils_load_node($nid, $vid = 0) {
  static $node_list = array();

  if(!is_numeric($nid)) {
    return NULL;
  }

  if (!$vid) {
    if (isset($node_list[$nid])) {
      return $node_list[$nid];
    }
    else {
      $sql = "SELECT nid, vid, language, type, title, status FROM {node} WHERE nid = $nid";
    }
  }
  else {
    $sql = "SELECT nr.nid, nr.vid, n.language, n.type, nr.title, n.status 
            FROM {node} n 
            INNER JOIN {node_revisions} nr ON n.nid = nr.nid
            WHERE n.nid = $nid
            AND nr.vid = $vid";
  }
  
  if ($sql) {
    $node = db_fetch_object(db_query($sql));
    if (!$vid) {
      $node_list[$nid] = $node;
    }
  }
  
  return $node;
}

/**
 * Exports a couple of settings which are used in javascript.
 * @param $node
 *  stdObject. The node which is viewed.
 * @param $force_add
 *  Force add the js-settings.
 */
function _ndla_utils_setup_js_settings($node, $force_add = FALSE) {
  global $ndla_utils_setup_js_added, $base_url;
  $context = ndla_utils_disp_get_course();
  $context_id = NULL;
  
  if($context) {
    $context_id = $context->nid;
  }

  $settings = array(
    'ndla_utils_url' => url('ndla_utils') . '/',
    'context_id' => $context_id,
    'theme_path' => $base_url."/".drupal_get_path('theme', 'ndla2010')."/",
    'itslearning_embed' => url('ndla_utils/itslearning_embed_details'),
  );
      
  //Only add this setting if we are viewing the current node.
  if($node->nid == arg(1) && $node->type == 'fag' && (!$ndla_utils_setup_js_added && !$force_add)) {
    $settings['node_id'] = $node->nid;
    drupal_add_js($settings, 'setting');
    $ndla_utils_setup_js_added = TRUE;
  }
  else if($force_add) {
    $potential_nid = arg(1);
    if(is_numeric($potential_nid)) {
      $settings['node_id'] = $potential_nid;
    }
    drupal_add_js($settings, 'setting');
    $ndla_utils_setup_js_added = TRUE;
  }
}

/**
 * Build the variable-array needed by the embed-code-function
 *
 * @param $node full node object
 * @return array containing variables needed to produce embed code by node type
 */
function ndla_utils_get_embed_vars($node) {
  $embed_vars = array();
  // Copyright
  $node = node_load($node->nid);
  $embed_vars['node'] = $node;
  $rights = ndla_authors_get_authors($node);

  if (!empty($rights)) {
    $opphav = array();
    foreach ($rights as $right) {
      if ($right['tid'] == 76666) {
        foreach ($right['authors'] as $author) {
          $opphav[$author['nid']] = $author['title'];
        }
      }
    }
    $embed_vars['opphav'] = $opphav;
  }

  // Usufruct
  if (!empty($node->cc_lite_license)) {
    $embed_vars['cc'] = $node->cc_lite_license;
    if (module_exists('creativecommons_lite')) {
      $types = creativecommons_lite_get_license_array();
      $embed_vars['cc_title'] = $types[$node->cc_lite_license];
    }
  }

  // Create embed code for all media types
  if($node->type == 'image'){  
    
    $embed_vars['filepath'] = $node->images['_original'];
    $embed_vars['alt'] = $node->title;
    $embed_vars['nid'] = $node->nid;
  }
  
  else if ($node->type == 'video') {
    $is_istribute = FALSE;
    if(module_exists('istribute')) {
      $node = node_load($node->nid);
      if(istribute_has_video($node)) {
        $is_istribute = TRUE;
      }
    }
    
    if(!$is_istribute) {
      // Create additional embed code for video
      $embed_vars['filepath'] = "";
      $embed_vars['imagepath'] = "";
      foreach($node->files as $vfiles) {
        if ($vfiles->filemime != 'flv-application/octet-stream') {
          $embed_vars['imagepath'] = $vfiles->filepath;
        }
        else {
          $embed_vars['filepath'] = $vfiles->filename;
        }
      }
      $embed_vars['nid'] = $node->nid;
    }

  }
  
  else if ($node->type == 'audio') {  
    $embed_vars['filepath'] = $node->language."/audio/play/".$node->nid;
    $embed_vars['nid'] = $node->nid; 
  }
  
  else if ($node->type == 'flashnode') {
    //Check if this is a zip file
    if(preg_match("/\.zip/i",$node->flashnode['filepath']) == 1) {
      //The directory is the name minus the zip extension.
      $directory = preg_replace("/\.zip/i", "", $node->flashnode['filepath']);

      //Every package should give us a index.swf
      $embed_vars['filepath'] = "$directory/index.swf";

      //Set the base so the flash nows where to pickup its files
      $embed_vars['base'] = $directory."/";

      //Pickup the width/height if the flash
      $info = swftools_get_info("$directory/index.swf");
      $embed_vars['width'] = $info['width'];
      $embed_vars['height'] = $info['height'];
    }
    else {
      $embed_vars['filepath'] = $node->flashnode['filepath'];
      $embed_vars['height'] = $node->flashnode['height'];
      if (!empty($node->flashnode['base'])) {
        $embed_vars['base'] = $node->flashnode['base'];
      }
      else {
        $embed_vars['base'] = "sites/default/files/";
      }
    }//end if zip
    
    $embed_vars['nid'] = $node->nid;
  }
  
  return $embed_vars;
}

/**
 * Run the InsertNode filter on embed code
 *
 * @param body node body
 * @return expanded body 
 */
function ndla_utils_resolve_embeds($body) {
  $text1 = contentbrowser_run_filter($body);
  $text2 = preg_replace_callback('/\[node:([^\s\]]+)(?:\s+(body|link|collapsed|teaser|title|lightbox.*))?\]/', '_InsertNode_replacer', $text1);
  return $text2;
}

/**
 * Fix biblio for embed code
 */
function ndla_utils_resolve_biblio($body) {
  $txt = biblio_filter('process', 0, -1, $body);
  $txt = footnotes_filter('process', 0, -1, $txt);
  return $txt;
}

/**
 * Clean up embed code styles
 */
function ndla_utils_replace_styles($body) {  
  $pattern[] = '@<script[^>]*?.*?</script>@siu';
  $replace[] = '<em>Videoen i denne noden kan sees på NDLA</em>';
  $pattern[] = '/rel="lightbox\[\]\[[^\]]*\]"/';
  $replace[] = '';
   
  $body = preg_replace($pattern,$replace,$body);
  $body = str_replace('"/sites/','"http://ndla.no/sites/',$body);
  
  $body_pattern = '/src="http:\/\/[^"]*?(http:\/\/[^"]*)"/si';
  $body = preg_replace($body_pattern, 'src="\1"', $body);
  
  $body = str_replace('"/nb/','"http://ndla.no/nb/',$body);
  $body = str_replace('"/nn/','"http://ndla.no/nn/',$body);
  $body = str_replace('"/en/','"http://ndla.no/en/',$body);
  
  return $body;
}

/**
 * @param $html
 *  The HTML to be filtered.
 * @return
 *  String
 */
function ndla_utils_convert_chars($html) {
  $html = str_replace('&lt;','<',$html);
  $html = str_replace('&gt;','>',$html);
  $html = str_replace('&amp;nbsp;','&nbsp;',$html);
  $html = str_replace('<a href="#" class="read-more"> les mer</a>', '', $html);
  $html = str_replace('<a class="re-collapse" href="#">skjul</a>', '', $html);
  $html = str_replace('<a class="re-collapse" href="#">skjul </a>', '', $html);
  $html = str_replace('<a href="/nb/biblio/','<a target="_blank" href="http://ndla.no/nb/biblio/', $html);
  $html = str_replace('<a href="/utdanning_content_browser_node/', '<a target="_blank" href="'.url(NULL,array('absolute' => TRUE)).'/node/', $html);
  return $html;
}

function ndla_utils_generate_url() {
  $url = url(NULL,array('absolute' => TRUE));
  $url = str_replace('/nb','',$url);
  $url = str_replace('/nn','',$url);
  $url = str_replace('/en','',$url);
  return $url;
}

function ndla_utils_buildLicense($node) {
  $fdata = ndla_authors_get_authors($node);
  $forfattere = array();
  $imgURL = ndla_utils_generate_url();
  foreach($fdata as $id  => $elm) {
    $forfattere = $elm['authors'];
  }
  
  $lisens = '<div style="border: 1px solid rgb(245, 245, 245); padding: 5px 5px 0pt; font-size: 80%; background-color: rgb(249, 249, 249); color: rgb(102, 102, 102); height: 30px; width: 45.5em; border: solid 1px #ebebeb">';

  $lisens .= '<div style="float:left"><strong style="float:left">'.t('Originator / usufruct').': </strong>';
  if (count($forfattere) > 0) {
    $lisens .= '<ul style="margin: 0pt; padding: 0pt; list-style-type: none; float: left; display: inline;">';
    foreach($forfattere as $person) {
      $lisens .= '<li style="padding: 0pt 2px; display: inline;">';
      $lisens .= '<a href="http://ndla.no/nb/aktor/'.$person['nid'].'" style="color: rgb(102, 102, 102);">'.$person['title'].'</a></li>';
    }
    $lisens .= '</ul></div>';
  }
  else {
    $lisens .= '<span>'.t('No originator / usufruct').'</span></div>';
  }
  

  $cc = array();
  $lisens .= '<div style="float: right; margin-right: 5px; margin-top: 5px; max-width: 45.5em;">';

  if (module_exists('creativecommons_lite')) {

    $cc = get_license_info($node->cc_lite_license);
    if (($node->cc_lite_license != 'copyrighted') && ($node->cc_lite_license != 'gnu') && ($node->cc_lite_license != 'publicdomain') && ($node->cc_lite_license != '')) {
      $lisens .= '<a href="'.$cc['license_uri'].'" title="'.$cc['license_name'].'" target="_blank">';
      $lisens .= '<img alt="Creative Commons License Icon" style="border: none" src="http://ndla.no/sites/all/modules/creativecommons_lite/images/buttons_small/'.$node->cc_lite_license.'.png" /></a> ';
    }
    else{
      if ($node->cc_lite_license == '') {
        $lisens .= '<strong>'.t('Not Licensed').'</strong> ';
      }
      else {
        $lisens .= '<strong>' . $cc['license_name'] . '</strong> ';
      } 
    } 
  }
  
  $lisens .= '<a title="Les på NDLA" href="'.url('node/' . $node->nid,array('absolute' => TRUE)) . '" target="_blank"><img style="border: none; height: 15px" alt="Nasjonal Digital Læringsarena" src="'.$imgURL.'/sites/all/themes/ndla/img/ndlarektangular16h.png" /></a></div></div>';
  
  return $lisens;
}

/**
 *  Function to handle ajax calls when changing classes.
 */
function ndla_utils_classes_select_js() {
  $tid = intval($_POST['selected']);
  $og = intval($_POST['og']);
  $lang = $_POST['lang'];
  $sessionid = session_id();

  if ($og != 0 && !empty($lang) && !empty($sessionid)) {
    $data = cache_get('ndla_utils_cache_classes_user_select', 'cache');


    if ($data != 0 && is_array($data->data)) {
      $data = $data->data;
    }
    else {
      $data = array();
    }

    if (isset($data[$sessionid][$og][$lang])) {
      unset($data[$sessionid][$og][$lang]);
    }

    if($tid != 0) {
      $data[$sessionid][$og][$lang] = $tid;
    }

    cache_set('ndla_utils_cache_classes_user_select', $data, 'cache', CACHE_TEMPORARY);
  }
}

/**
 *
 */
function _ndla_utils_subselector($classes, $prefix, $use_all_link = TRUE) {
  global $language;
  $context_id = (!$use_all_link ? arg(3) : arg(4));
  
  if ($use_all_link) {
    $main_id = (arg(3) ? arg(3) : 'all');
  }
  if (sizeof($classes) > 1) {
    asort($classes);
    $items = array();
    if ($use_all_link) {
      $items[] = array(
      	'title' => t('All'),
      	'href' => "ndla_utils/menu2010/$prefix/" . arg(3) ."/all",
      	'attributes' => array('id' => "class-selector-all", 'class' => 'navContentLink', 'class' => 'navContentLink' . ($context_id == '' ? ' selected' : '')));
    }
    
    $i = 0;
    foreach ($classes as $id => $name) {
      $items[] = array(
      	'title' => $name,
        'href' => "ndla_utils/menu2010/$prefix/" . arg(3) ."/$id",
      	'attributes' => array(
      		'id' => "class-selector-${id}",
      		'class' => 'navContentLink' . ($context_id == $id || (!$use_all_link && !$context_id && $i == 0) ? ' selected' : '')));
      $i++;
    }
    return theme('links', $items, array('class' => 'navContentTabs')) . '<div class="clear"></div>';
  }
}

/*************************************************************************
 * END OF TOPICS AND MENUS
 *************************************************************************/

/**
 * Called from *_preprocess_page
 * Function for populating the submenu (ie menus, topic, grep)
 */
function ndla_utils_set_page_variables(&$variables) {
  $arg1 = arg(1);
  if(is_numeric($arg1)) {	
    if ($variables['node']->type == 'fag') {
      //_ndla_utils_setup_js_settings($variables['node']);
      $arg2 = arg(2);

      // Display content in the navContent div if "fag" front page, or on "fag" subpages topics, grep or menu
      $menu_types = array(NULL, 'topics', 'grep', 'menu');
      if (in_array($arg2, $menu_types)) {
        if ($arg2) {
          // Set new template if the page displayed is not the course front page
          $variables ['template_files'] [] = 'page-fag';

          $variables ['active_menu'] = $arg2;
        }
        else {
          // File missing when on course front page
          //require_once drupal_get_path('module', 'ndla_utils') . '/utdanning_theme_functions.pages.inc';

          // Retrieve default expanded for the "fag"
          $default_expanded = 'none';
          if (module_exists('ndla_fag_tabs')) {
            $query = "SELECT default_tab FROM {ndla_fag_tabs} WHERE nid=%d";
            $result = db_query($query, $arg1);
            $data = db_fetch_object($result);
            $default_expanded = $data->default_tab;
          }

          $variables ['active_menu'] = ($default_expanded == 'none') ? '' : $default_expanded;
          $variables ['fag_front'] = TRUE;
        }

        $func = 'ndla_utils_show_' . $variables ['active_menu'];
        if (function_exists($func)) {
          $nav = $func($variables['node']);
          $variables ['subnavigation'] = $nav ['title'] ;
          $variables ['nav_content'] = $nav ['content'] . '<div class="clear"></div>';
        }
      }
    }
  }
  
  ndla_utils_set_footer_message($variables);
}

/**
 * Sets the footer message in the variables['footer']. Called from ndla_utils_set_page_variables.
 */
function ndla_utils_set_footer_message(&$variables) {
  global $language;
  if($language->language != 'en') {
    $variables['footer_message'] = variable_get('site_footer_' . $language->language, '');
  }
  $variables['footer_info'] = variable_get('site_footer_info_' . $language->language, '');
  $variables['footer_editor_in_chief_name'] = variable_get('site_footer_editor_in_chief_name', 'Øyvind Høines');
  $variables['footer_editor_in_chief_link'] = variable_get('site_footer_editor_in_chief_link', '/kontakt');
  $variables['footer_web_editor_name'] = variable_get('site_footer_web_editor_name', 'Jarle Ragnar Meløy');
  $variables['footer_web_editor_link'] = variable_get('site_footer_web_editor_link', '/kontakt');
 
  if(module_exists('utdanning_browsertest')) {
    $variables['footer_browsertest'] = l(t("Test your browser"), "browsertest", array('attributes' => array('rel' => 'lightframe[|width:450px;height:450px;]')));
  }
}

/**
 * Sets the bottom footer bar with new design
 * 
 * @param $variables vars from template function
 */
function ndla_utils_set_about_footer(&$variables) {
  $editor = '<div>' . t('Editor in chief') . ': ' . $variables['footer_editor_in_chief_name'] . '</div>';
  $webeditor = '<div>' . t('Managing editor') . ': ' . $variables['footer_web_editor_name'] . '</div>';
  $courseeditor = ndla_utils_get_course_editor();
  $ndla = '<div class="footer-about">' . $variables['footer_message'] . '</div>';
  $variables['about_footer'] = '<div class="footer-about">' . $editor . $webeditor . $courseeditor . '</div>' . $ndla;
}

/**
 * Retrieves the course and editor in a string
 * 
 * @return string
 */
function ndla_utils_get_course_editor($gid = FALSE, $only_user = FALSE) {
  if(!$gid) {
    $course = ndla_utils_disp_get_course();
  }
  else {
    $course = node_load($gid);
  }
  $string = NULL;
  /* Fagredaktør i "navn_på_fag": "navn_på_fagredaktør_i_fag" (mailto: "registert epost") - TGP-1238 - jhh */
  if ($course && isset($course->field_contactperson['0']['uid']) && !empty($course->field_contactperson['0']['uid']) && is_string($course->field_contactperson['0']['uid'])) {
    $userobject = user_load($course->field_contactperson['0']['uid']);
    if($only_user) {
      return $userobject;
    }
    $name = "";
    if (!empty($userobject->profile_fornavn)) {
      $name .= $userobject->profile_fornavn;
    }
    if (!empty($userobject->profile_etternavn)) {
      !empty($name) ? $name .= ' ' : $name .= '';
      $name .= $userobject->profile_etternavn;
    }
    if (empty($name)) {
      $name = $userobject->name;
    }

    $title = '<em>' . trim($course->title) . '</em>';
    if (isset($course->ndla_utils_about_red_url) && $course->ndla_utils_about_red_url != '') {
      $title = '<a href="' . check_url($course->ndla_utils_about_red_url) . '">' . $title . '</a>';
 	  } 
    $string = '<div>' . t("Editor of the subject") . ' ' . $title . ': ' . $name . '</div>';
  }
  return $string;
}
  
/**
 * Select language prefix with logic suited for an mainly norwegian audience
 * 
 * @param string $node_lang - the language of the node
 * @param string $og_lang - the language of the organic group
 * @return string
 */
function _ndla_utils_select_language_prefix($node_lang = '', $og_lang = '') {
  // if node has og_language
  if ($og_lang != '') {
    return $og_lang;
  }
  // if node has language
  if ($node_lang != '') {
    return $node_lang;
  }
  // if current language is not english 
  global $language;
  if ($language->language != '' && $language->language != 'en') {
    return $language->language;
  }
  // if user language is not english
  global $user;
  if ($user->language != '' && $user->language != 'en') {
    return $user->language;
  }
  // default is nb
  return 'nb';
}

/**
 * Get a list with all the courses.
 *
 * @return array List with all the courses.
 */
function ndla_utils_ndla2010_theme_xhr_courses() {
  global $language;
  
  $form = array();
  
  $form['tralal'] = 1;
  
  $courses_cache_id = 'ndla2010_xhr_courses_' . $language->language;
  $courses_cache = cache_get($courses_cache_id);
  if ($courses_cache->data) {
    $json = $courses_cache->data;
  }
  else {
    $courses = ndla_utils_courses();
    $to_json = array();
    for ($i = 0, $s = count($courses); $i < $s; $i++) {
      if ($courses[$i]->nid)
      $to_json[] = array(
        'nid' => $courses[$i]->nid,
        'title' => $courses[$i]->name,
        'path' => $courses[$i]->url,
        'indev' => isset($courses[$i]->in_dev) ? 1 : 0
      );
    }
    
    $json = json_encode($to_json);
    cache_set($courses_cache_id, $json, 'cache');
  }
  
  print $json;
}

/**
 * Returns the sorting of content types.
 * 
 * @return
 *  Array of content types.
 */
function ndla_utils_get_sorting() {
  $node_list = variable_get("ndla_utils_node_type_order", array());
  if (!count($node_list)) {
    $types = node_get_types();
    foreach ($types as $id => $type) {
      $node_list[$id] = array();
    }
  }
  else {
    foreach ($node_list as $id => &$val) {
      $val = array();
    }
  }

  return $node_list;
}


/**
 * Sorts nodes depending on their content type.
 * @see ndla_utils_get_sorting()
 *
 * @param $nodes
 *  Array of nodes to sort.
 *
 * @return
 *  Sorted array of nodes.
 */
function ndla_utils_sort_nodes($nodes = array()) {
  $sorting = ndla_utils_get_sorting();
  foreach($nodes as $n) {
    $sorting[$n->type][] = $n;
  }
  
  $nodes = array();
  foreach($sorting as $s) {
    foreach($s as $n) {
      $nodes[] = $n;
    }
  }
  
  return $nodes;
}

function ndla_utils_is_block_collapsed($block) {
  if(module_exists('jstorage')) {
    global $user;
    $block = 'block-' . $block;
    $result = db_fetch_object(db_query("SELECT value FROM {jstorage} where uid = %d AND namespace ='%s' AND name = '%s'", $user->uid, 'theme', 'blocks'))->value;
    if(!empty($result)) {
      $data = json_decode($result);
      return $data->$block;
    }
  }
}

function ndla_utils_get_taxonomy_course_context() {
  $tid = NULL;
  $course = ndla_utils_disp_get_course();
  if(isset($course->nid)) {
    $tid = ndla_fag_taxonomy_get_term_from_og($course->nid);
  }
  
  return $tid;
}
