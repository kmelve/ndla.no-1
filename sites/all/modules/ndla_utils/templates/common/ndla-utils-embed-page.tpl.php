<?php global $language; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html version="XHTML+RDFa 1.0" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" xmlns:og="http://opengraphprotocol.org/schema/" dir="ltr">
<head>
  <title>Nasjonal digital læringsarena - <?php print $title; ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="robots" content="noindex,nofollow"/>
  <?php print $css; ?>
  <?php print $js; ?>
  <style>
    .licenses {
      margin-top: 15px;
    }
    
    .embed-container {
      margin-top: 15px;
      margin-left: 15px;
    }
  </style>
</head>
<body>
  <div class='embed-container'>
    <h1><?php print $title; ?></h1>
    <?php print $content; ?>
    <?php if($usufruct) print $usufruct; ?>
  </div>
</body>
</html>