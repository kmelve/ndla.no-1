<?php
/**
 *
 */
 
$opphav = array();

// Copyright
$rights = ndla_authors_get_authors($node);
if (!empty($rights)) {
  $opphav = array();
  foreach ($rights as $right) {
    if ($right['term_name'] == 'Opphavsmann') {
      foreach ($right['authors'] as $author) {
        $opphav[$author['nid']] = $author['title'];
      }
    }
  }
}

// Usufruct
if (!empty($node->cc_lite_license)) {
  $cc = $node->cc_lite_license;
  if (module_exists('creativecommons_lite')) {
    $types = creativecommons_lite_get_license_array();
    $cc_title = $types[$node->cc_lite_license];
  }
}
 
$url = url(NULL, array('absolute' => TRUE, 'language' => ''));
?>

<div class='licenses' style="border: 1px solid rgb(245, 245, 245); padding: 5px 5px 0pt; font-size: 80%; background-color: rgb(249, 249, 249); color: rgb(102, 102, 102); height: 30px; width: 45.5em; border: solid 1px #ebebeb">
  <div style="float:left">
    <strong style="float:left"><?php print t('Originator'); ?>: </strong>

    <?php if(isset($opphav) && $opphav):; ?>
      <ul style="list-style-type: none; float: left; margin: 0; padding:0; display: inline;">
        <?php foreach($opphav as $nid =>  $title): ?>
          <li style="display: inline; padding: 0 2px 0 2px">
            <a href="<?php print url(NULL,array('absolute' => TRUE)) . '/aktor/' . $nid; ?>" target="_blank" style="color: #666666"><?php print $title; ?></a>
          </li>
        <?php endforeach; ?>
      </ul>
    <?php else: ?>
      <?php print t('No originator'); ?>
    <?php endif; ?>
  </div><!-- END div float left -->

  <div style="float: left; margin-left: 15px; max-width: 600px;">
    <strong style="float:left"><?php print t('Usufruct'); ?>: </strong>
    <?php if(isset($cc)):
      if(!empty($cc) && !in_array($cc, array('copyrighted', 'gnu', 'publicdomain'))): ?>
        <a title="<?php print $cc_title; ?>" href="http://creativecommons.org/licenses/<?php print $cc; ?>/3.0/no/"><img style="border: none" src="<?php print $url; ?>/sites/all/modules/creativecommons_lite/images/buttons_small/<?php print $cc; ?>.png" alt="Creative Commons license icon" /></a>
      <?php elseif(!empty($cc) && in_array($cc, array('copyrighted'))): ?>
        <a title="<?php print $cc_title; ?>" href="http://no.wikipedia.org/wiki/Copyright"><img style="border: none" src="<?php print $url; ?>/sites/all/modules/creativecommons_lite/images/buttons_small/<?php print $cc; ?>.png" alt="Copyright icon" /></a>
      <?php elseif(!empty($cc) && in_array($cc, array('gnu'))): ?>
        <a title="<?php print $cc_title; ?>" href="http://www.gnu.org/licenses/gpl.html"><img style="border: none" src="<?php print $url; ?>/sites/all/modules/creativecommons_lite/images/buttons_small/<?php print $cc; ?>.png" alt="Copyright icon" /></a>
      <?php else: ?>
        <strong><?php print $cc_title; ?></strong>
      <?php endif; ?>
    <?php else: ?>
      <strong><?php print t('Not Licensed')?></strong>
    <?php endif; ?>
  </div>

  <div class="footer_ndla_logo" style="float: right; margin-right: 5px; margin-top: 5px;max-width: 600px;">
    <a title="Les på NDLA" href="<?php print url(NULL,array('absolute' => TRUE)).'/node/'.$node->nid; ?>" target="_blank">
      <img style="border: none; height: 15px" alt="Nasjonal Digital Læringsarena" src="<?php print $url; ?>/sites/all/themes/ndla2010/img/ndlarektangular16h.png" />
    </a>
  </div>

</div><!-- END div class licenses -->
