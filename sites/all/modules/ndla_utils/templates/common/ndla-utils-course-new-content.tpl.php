<ul>
  <?php for ($i = 0, $size = count($new_content); $i < $size; $i++): ?>
  <li<?php print $i == 0 ? ' class="first"' : '' ?>>
    <div class="new-content-image-wrapper">
      <img src="<?php print $base_path . ($new_content[$i]->image ? $new_content[$i]->image : $directory . '/img/placeholder.png') ?>" height="75" alt=""/>
    <?php if ($new_content[$i]->actuality): ?> 
      <a href="<?php print url('search/apachesolr_search/', array('query' => 'sort_by=created&im_vid_100004%5B%5D=' . variable_get('ndla_utils_ndla2010_theme_actuality_content_type', 0) . '&language%5B%5D=und&language%5B%5D=' . $lang_code)) ?>" class="actuality" title="<?php print t('See more actualities') ?>"></a>
    <?php endif ?>
    </div>
    <h3><a href="<?php print url('node/' . $new_content[$i]->nid) ?>"><?php print check_plain($new_content[$i]->title) ?></a></h3>
    <p class="new-content">
      <?php
            $char_count_allowed = 200;
            $content = preg_replace('/\[.*\]/', '', strip_tags(preg_replace('/<br \/>|<\/p>/', ' ', $new_content[$i]->teaser)));
            if(strlen($content) > $char_count_allowed) {
                $content = node_teaser($content,NULL, $char_count_allowed) . "...";
            }
            print $content;
      ?>
    </p>
  </li>
  <?php endfor ?>
</ul>
