<div id="user-guides">
  <div id="user-guide-thumbnail-holder">
    <img id="user-guide-thumbnail" src="<?php print base_path() . drupal_get_path('theme', 'ndla2010') . '/img/user_guide.png' ?>" alt=""/>
    <div id="user-guide-thumbnail-text"><?php print t('Choose a user guide from the menu below!') ?></div>
  </div>
  <div id="user-guide-box" class="tray-stand-out on-top">
    <div id="user-guide-close" class="close-window sprite-image"></div>
    <div id="user-guide-movie"></div>
  </div>
  <ul>
    <?php $i = 0;?>
    <?php for ($i = 0, $num_user_guides = count($user_guides); $i < $num_user_guides; $i++): ?>
      <?php $extra = (($i + 1) % 3 == 0) ? ' class="last-col"' : '';?>
      <li<?php print $extra ?>><?php print l($user_guides[$i]->title . '<span class="user-guide-hover sprite-image"></span>', 'userguide/' . $user_guides[$i]->nid, array('html' => TRUE, 'attributes' => array('class' => 'user-guide', 'target' => '_blank'))); ?></li>
    <?php endfor ?>
  </ul>
</div>