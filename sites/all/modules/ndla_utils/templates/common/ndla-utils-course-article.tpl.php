<?php if ($image): ?>
  <div id="editorial-image">
    <img src="<?php print $image ?>" width="918" alt=""/>
  </div>
<?php endif ?>
<div id="editorial-internal">
  <div id="editorial-article" class="clear-block">
    <div class="text">
      <?php print $body ?>
    </div>
  </div>
  <div id="editorial-right" class="clear-block">
    <?php 
    print $sidebar;
    if(arg(0) == 'node' && arg(1) == '36') {
      if(module_exists('ndla_ntb')) {
        print "<div class='ndla-ntb-block'>" . ndla_ntb_get_news_request() . "</div>";
      }
    }
    ?>
  </div>
</div>
