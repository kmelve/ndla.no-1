<form method='post' id='sync-report'>
  <h2>Filter:</h2>
  <div><span style='width: 100px; display: inline-block;'>Sync period:</span><select style='width: 300px;' name='period' onchange='$("#sync-report").submit();'>
    <?php
      foreach ($periods as $timestamp => $description) {
        echo '<option value="' . $timestamp . '"' . ($_POST['period'] == $timestamp ? ' SELECTED=SELECTED' : '') . '>' . $description . '</option>';
      }
    ?>
  </select>
  </div>
  <div><span style='width: 100px; display: inline-block;'>Course:</span><select style='width: 300px;' name='course' onchange='$("#sync-report").submit();'>
    <option value=''>All subjects</option>
    <?php
      foreach ($courses as $course) {
        echo '<option value="' . $course['nid'] . '"' . ($_POST['course'] == $course['nid'] ? ' SELECTED=SELECTED' : '') . '>' . $course['subject'] . '</option>';
      }
    ?>
  </select>
  </div>
  <div><span style='width: 100px; display: inline-block;'>Node type:</span><select style='width: 300px;' name='nodetype' onchange='$("#sync-report").submit();'>
    <option value=''>All node types</option>
    <?php
      foreach ($nodetypes as $nodetype) {
        echo '<option value="' . $nodetype['type'] . '"' . ($_POST['nodetype'] == $nodetype['type'] ? ' SELECTED=SELECTED' : '') . '>' . $nodetype['name'] . '</option>';
      }
    ?>
  </select>
  </div>
  <div><span style='width: 100px; display: inline-block;'>Status:</span><select style='width: 300px;' name='status' onchange='$("#sync-report").submit();'>
    <option value=''>All statuses</option>
    <option value='INSERT'>Insert</option>
    <option value='UPDATE'>Update</option>
    <option value='DELETE'>Delete</option>
  </select>
  </div>
</form>

<?php
  $counter = array();
  $total_count = 0;
  foreach ($rows as $row) {
    if (!isset($counter[$row['stat']])) $counter[$row['stat']] = 0;
    $counter[$row['stat']]++;
    $total_count++;
  }

  if (sizeof($counter) > 0) echo '<h2>' . t('Statistics') . '</h2>';
  foreach ($counter as $stat => $value) {
    echo '<div>' . $stat . ': ' . $value . '</div>';    
  }
  echo '<div>' . t('Total') . ': ' . $total_count . '</div>';
?>

<table width="100%" class="ndla-utils-red-report">
  <thead>
    <tr>
      <td width="40%">Node</td>
      <td width="40%">Type</td>
      <td width="20%">Status</td>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach ($rows as $row) {
        echo '<tr><td><a href="/node/' . $row['nid'] . '" target="_BLANK">' . $row['nid'] . '</a></td><td>' . $row['type'] . '</td><td>' . $row['stat'] . '</td></tr>';
      }
    ?>
  </tbody>
</table>
