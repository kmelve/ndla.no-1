<?php
if($node):
  $types = node_get_types();
  print '<div id="contentbrowser-node-title">' . l($node->title, "node/$node->nid")." (".$types[$node->type]->name. ")</div>"; ?>

  <?php print theme('ndla_authors_block_view', $node);?>
<?php endif; ?>
<?php if ($creative_commons): ?>
<div class="creativecommons">
<?php print $creative_commons; ?>
</div>
<?php endif; ?>
