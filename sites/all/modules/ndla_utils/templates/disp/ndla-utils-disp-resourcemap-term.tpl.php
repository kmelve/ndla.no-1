<?php global $language; ?>
<div class="term term-<?php print $parent ?>">
  <div class="top"></div>
  <div class="middle">
    <?php if(empty($term->children)): ?>
      <h3 class="tid-<?php print $term->tid ?>"><?php print $term->name ?></h3>
    <?php else: ?>
      <a href="javascript:void(0)" class="h3 tid-<?php print $term->tid ?>"><?php print $term->name ?></a>
    <?php endif; ?>
    <?php print l(format_plural($term->resources, '1 resource', '@count resources'), 
    str_replace("/ndla_search" , "/" . $language->language . "/ndla_search", variable_get('resourcemap_url', 'http://search.ndla.no/ndla_search')),
    $term->url_options); ?>
  </div>
  <div class="bottom"></div>
</div>