<div id="rights-block">
  <div id="node-rights">
    <?php print theme('ndla_utils_disp_rights_content', $node, $creative_commons)?>
  </div>
  <?php
    $nids_inserted = contentbrowser_get_inserted_nodes($node->nid);
    
    /* Remove person nodes from footer */
    $result = db_query("SELECT nid FROM {node} WHERE type = 'person' AND nid IN ('" . implode("','", array_keys($nids_inserted)) . "')");
    while ($row = db_fetch_object($result)) {
      unset($nids_inserted[$row->nid]);
    }

    if(!empty($nids_inserted)):
  ?>
  <h2><?php print t('Elements used on the page') ?></h2>
  <div id="ndla_mediabrowser_inserted_nodes">
    <?php print theme('contentbrowser_related_data_block_content_v1', $nids_inserted);?>
  </div>
  <?php endif; ?>
</div>