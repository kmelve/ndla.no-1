<?php if (is_array($images)): ?>
<div class="vimages">
  <h4><?php print t('Available image sizes')?>:</h4>
	<ul>
		<?php
    if($images['fullbredde']) {
      print '<li class="selected">' . l(t('Full width'), $images['fullbredde'], array('absolute' => TRUE, 'language' => '')) . '</li>';
    }
    if($images['normal']) {
      print '<li>' . l(t('Normal'), $images['normal'], array('absolute' => TRUE, 'language' => '')) . '</li>';
    }
    if($images['thumbnail']) {
      print '<li>' . l(t('Thumbnail'), $images['thumbnail'], array('absolute' => TRUE, 'language' => '')) . '</li>';
    }
    if($images['hovedspalte']) {
      print '<li>' . l(t('Main column'), $images['hovedspalte'], array('absolute' => TRUE, 'language' => '')) . '</li>';
    }
    if($images['_original']) {
      print '<li>' . l(t('Original'), $images['_original'], array('absolute' => TRUE, 'language' => '')) . '</li>';
    }
		?>
	</ul>
	<div class="clear"></div>
</div>
<?php endif; ?>