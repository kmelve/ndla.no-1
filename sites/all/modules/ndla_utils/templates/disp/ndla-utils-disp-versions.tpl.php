<?php if (isset($permanent_link)): ?>
<div class="permanent-link">
	<h4><?php print t('Permanent link'); ?>:</h4>
  <a href="<?php print $permanent_link;?>"><?php print $permanent_link; ?></a>
</div>
<?php endif ?>

<?php if (is_array($difficulties)): ?>
<div class="difficulty">
	<h4><?php print t('Difficulty level'); ?>:</h4>
	<ul>
		<?php $first = TRUE;
      foreach($difficulties as $difficulty):
        if ($first):
          $first = FALSE;
        else:
          print ' | ';
        endif;
        if ($difficulty['current_node']): ?>
          <li class="current-node"><?php print $difficulty['title'] ?></li>
        <?php else: ?>
          <li<?php $difficulty['not_published'] ? print ' class="red"' : NULL ?>><?php print l($difficulty['title'], 'node/' . $difficulty['nid']) ?></li>
        <?php endif;
		  endforeach ?>
	</ul>
	<div class="clear"></div>
</div>
<?php endif ?>

<?php if (is_array($languages)): ?>
<div class="language"><h4><?php print t('Language')?>:</h4>
	<ul>
		<?php $first = TRUE;
		foreach ($languages as $lang):
      if ($first):
        $first = false;
      else:
        print ' | ';
      endif;
      if ($lang['current_node']): ?>
        <li class="current-node"><?php print $lang['title'] ?></li>
      <?php else: ?>
        <li<?php $lang['not_published'] ? print ' class="red"' : NULL ?>><?php print l($lang['title'], 'node/' . $lang['nid'], array('language' => $lang['language'])) ?></li>
      <?php endif;
		endforeach ?>
	</ul>
	<div class="clear"></div>
</div>
<?php endif; ?>
