<div class="author-facet author-facet-<?php print $counter ?>">
	  <?php if ($header != ''): ?>
	    <h2><?php print $header ?></h2>
	  <?php endif ?>
  <ul>
    <?php foreach ($fields as &$field): ?>
      <li><a href="<?php print $url . $field->id ?>"><?php print $field->name ?></a>&nbsp;<span class="count">(<?php print $field->hits ?>)</span></li>
    <?php endforeach ?>
  </ul>
</div>