<ul>
  <?php foreach ($parents as &$parent): ?>
    <?php if ($parent->current): ?>
      <li class="current">
        <?php print check_plain($parent->title) ?> (<?php print $parent->type ?>)
      </li>
    <?php else: ?>
      <li>
        <a href="<?php print $parent->url ?>"><?php print check_plain($parent->title) ?></a> (<?php print $parent->type ?>)
      </li>
    <?php endif ?>
  <?php endforeach ?>
</ul>