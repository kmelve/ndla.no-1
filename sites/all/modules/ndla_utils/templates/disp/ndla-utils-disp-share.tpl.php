<a href='#' class='chosen' id='share-on-email'><?php print t('Share on e-mail');?></a> | <a href='#' id='share-on-internet'><?php print t('Share on Internet');?></a>
<script type='text/javascript'>
  $('#share-on-email').click(function(event) {
    event.preventDefault();
    $('.sharingprograms').hide()
    $('#share-on-internet').removeClass('chosen');
    $(this).addClass('chosen');
    $('.tipForm').show();
  });
  $('#share-on-internet').click(function(event) {
    event.preventDefault();
    $('.tipForm').hide()
    $('#share-on-email').removeClass('chosen');
    $(this).addClass('chosen');
    $('.sharingprograms').show();
  });
</script>
<div class="tipForm" <?php if (!$printmail): print "style=\"display: none;\""; endif; ?>>
  <form action="<?php print $printmail ?>" method="post">
    <div class="tipforminput clearfix">
      <label for="emailExt"><?php print t('Receiver\'s e-mail')?>:</label>
      <p class="txtInput"><input class="share-input" type="text" id="emailExt" name="emailExt" /></p>
    </div>
		
    <div class="tipforminput clearfix">
      <label for="message"><?php print t('Message') ?>:</label>
      <p class="txtInput">
        <textarea class="share-input" name="message" rows="3" id="message" ></textarea>
      </p>
    </div>
    <div class="tipforminput">
      <input type="submit" value="Send tips" />
    </div> 
  </form>
</div>

<div class="sharingprograms">
  <ul>
    <?php
    if (is_array($links)) {
      foreach ($links as $item) {
        print '<li>' . $item . '</li>';
      }
    }
    ?>
  </ul>
  <div class="clear"></div>
</div>
