<?php print t('Choose a user guide from the menu below!'); ?>
<div id="user-guide-box">
  <div id="user-guide-movie">
    <?php if ($start_video):?>
      <?php print $start_video ?>
    <?php else: ?>
      <div id="user-guide-text">
        <div id="user-guide-top-text"><?php print t('User guide for')?></div>
        <div id="user-guide-bottom-text">NDLA.NO</div>
      </div>
    <?php endif ?>
  </div>
</div>
<ul id="user-guide-links" class="clear-block">
  <?php $i = 0;?>
  <?php for ($i = 0, $num_user_guides = count($user_guides); $i < $num_user_guides; $i++): ?>
    <li><?php print l($user_guides[$i]->title . '<span class="user-guide-hover sprite-image"></span>', 'userguide/' . $user_guides[$i]->nid, array('html' => TRUE, 'attributes' => array('class' => 'user-guide', 'target' => '_blank'))); ?></li>
  <?php endfor ?>
</ul>
<div id="user-guide-thumbnail-holder">
  <img id="user-guide-thumbnail" src="<?php print base_path() . drupal_get_path('theme', 'ndla2010') . '/img/user_guide.png' ?>" alt=""/>
</div>