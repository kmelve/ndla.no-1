<?php
/**
 * Template for generating embed code.
 * @node_type, $variables['variables'], $textarea
 */
?>

<?php
if ($textarea): ?>
  <label for="embed-code"><?php print t('Embed code') ?>:</label>
  <textarea rows="1" cols="30"  name="embed-code" onclick="$(this).focus().select()">
<?php endif ?>
<?php if($node_type == 'image'): ?>
  <img src="http://ndla.no/<?php print $variables['variables']['filepath']; ?>" alt="<?php print $variables['variables']['alt']; ?>" />
<?php endif; ?>
<?php if($node_type == 'audio'): ?>
  
  <object height="24" width="290" data="http://ndla.no/sites/all/modules/audio/players/1pixelout.swf" type="application/x-shockwave-flash">
    <param value="http://ndla.no/sites/all/modules/audio/players/1pixelout.swf" name="movie" />    
    <param value="transparent" name="wmode" />
    <param value="false" name="menu" />
    <param value="high" name="quality" />
    <param value="soundFile=http://ndla.no/<?php print $variables['variables']['filepath']; ?>" name="FlashVars" />
    <embed height="24" width="290" flashvars="soundFile=http://ndla.no/<?php print $variables['variables']['filepath']; ?>" src="http://ndla.no/sites/all/modules/audio/players/1pixelout.swf"></embed>
  </object><br><br><?php endif; ?>
<?php if($node_type == 'flashnode'): ?>
  <object data="http://ndla.no/<?php print $variables['variables']['filepath']; ?>" id="swf12573292991" type="application/x-shockwave-flash" height="<?php print $variables['variables']['height']; ?>" width="575">
    <param value="default" name="swliveconnect">
    <param value="true" name="play">
    <param value="false" name="loop">
    <param value="false" name="menu">
    <param value="autohigh" name="quality">
    <param value="showall" name="scale">
    <param value="l" name="align">
    <param value="tl" name="salign">
    <param value="opaque" name="wmode">
    <param value="#FFFFFF" name="bgcolor">
    <param value="7" name="version">
    <param value="true" name="allowfullscreen">
    <param value="sameDomain" name="allowscriptaccess">
    <param value="575" name="width">
    <param value="<?php print $variables['variables']['height']; ?>" name="height">
    <param value="http://ndla.no/<?php print $variables['variables']['base']; ?>" name="base">
    <param value="http://ndla.no/<?php print $variables['variables']['filepath']; ?>" name="src">
    <param value="width=575&amp;height=<?php print $variables['variables']['height']; ?>" name="flashvars">
  </object><?php endif; ?>
<?php 
if($node_type == 'video') {
  $is_istribute = FALSE;
  if(module_exists('istribute')) {
    if(istribute_has_video($variables['variables']['node'])) {
      print theme('istribute_player', $variables['variables']['node']);
      $is_istribute = TRUE;
    }
  }
  if(!$is_istribute) {
  ?>
  <object id="flashvideo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="428" height="337" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab">
    <param name="movie" value="http://ndla.no/sites/default/files/Player.swf" />
    <param name="FlashVars" value="file=<?php print $variables['variables']['filepath']; ?>&#38;image=http://ndla.no/<?php print $variables['variables']['imagepath']; ?>&#38;rotatetime=3&#38;autostart=false&#38;streamer=rtmp://stream.ndla.no/streamingvideo/" />
    <param name="quality" value="high" />
    <param name="wmode" value="window" />
    <param name="allowfullscreen" value="true" />    
    <embed name="flashvideo" allowScriptAccess="always" src="http://ndla.no/sites/default/files/Player.swf" width="428" height="337" border="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="window" allowfullscreen="true" quality="high" flashvars="file=<?php print $variables['variables']['filepath']; ?>&#38;image=<?php print $variables['variables']['imagepath']; ?>&#38;rotatetime=3&#38;autostart=false&#38;streamer=rtmp://stream.ndla.no/streamingvideo/" />
    </embed>
  </object>
  <?php
  }
}?>
<?php if(in_array($node_type, array('amendor_ios', 'amendor_ios_task'))):
  print theme('amendor_ios_external_embed', $variables['variables']['node']); endif; ?>
<?php if($node_type == 'amendor_electure'):
  print theme('amendor_electure_external_embed', $variables['variables']['node']); endif; ?>
<?php print theme('ndla_utils_embed_copyright', $variables['variables']); ?>
<?php if($textarea): ?>
  </textarea>
  <a href="#" onclick="$(this).prev().focus().select();return false"><?php print t('Select embed code') ?></a>
<?php endif ?>