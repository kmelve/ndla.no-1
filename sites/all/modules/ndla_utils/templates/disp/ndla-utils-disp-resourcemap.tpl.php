<div id="resourcemap">
  <div class="top"></div>
  <div class="middle">
    <div class="close-window sprite-image" title="<?php print t('Close window') ?>"></div>
    <div class="breadcrumbs"></div>
    <div class="content-main">
      <div id="resourcemap-drawings"></div>
      <div class="main">
        <div class="top"></div>
        <div class="middle">
          <h2><?php print $main->name ?></h2>
          <?php 
          global $language;
          print l(format_plural($main->resources, '1 resource', '@count resources'), str_replace("/ndla_search" , "/" . $language->language . "/ndla_search", variable_get('resourcemap_url', 'http://search.ndla.no/ndla_search')), $main->url_options) 
          ?>
        </div>
        <div class="bottom"></div>
      </div>
      <?php print $terms ?> 
    </div>
    <div class="navigation"><?php print $navigation ?></div>
  </div>
  <div class="bottom"></div>
</div>
