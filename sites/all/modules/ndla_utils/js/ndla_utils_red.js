Drupal.behaviors.meta_tag_description_count = function(context) {
  $('#edit-nodewords-description-value-wrapper', context).each(function() {
    var wrapper = this;
    var inputBox = $('#edit-nodewords-description-value', wrapper);
    var valueBox = $('div.description', wrapper)
                    .append('<br/><span class="counter">Characters Entered: <span class="value">0</span></span>')
                    .find('.value')
                    .text(inputBox.val().length);
    inputBox.keyup(function(e) { $(valueBox).text(inputBox.val().length) });
  });
}

function selectOgBoxes() {
  num_boxes = $('.user-og-selector input.form-checkbox').length;
  num_boxes_selected = $('.user-og-selector input.form-checkbox:checked').length;

  if(num_boxes > num_boxes_selected) {
    $('.user-og-selector input').attr('checked', 'checked');
  }
  else {
    $('.user-og-selector input').removeAttr('checked');
  }
}
