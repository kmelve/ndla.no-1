Drupal.behaviors.ndla_utils_red_search = function(context) {
  $('.ndla_utils_red_search_table td a').mouseover(function() {
    $(this).siblings().fadeIn('fast');
  });
  $('.ndla_utils_red_search_table td a').mouseout(function() {
    $('.ndla_utils_red_search_table td div.thumbnail').fadeOut('fast');
  });
}