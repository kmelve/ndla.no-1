Drupal.behaviors.fix_slideshow = function(context) {
  setTimeout('fix_slideshow_width_height()', 600);
}
function fix_slideshow_width_height() {
  var width = 0;
  var height = 0;
  $('.views_slideshow_singleframe_hidden').css('visibility', 'visible');
  $('.views_slideshow_singleframe_hidden').css('display', 'block');
  
  $('.views_slideshow_singleframe_slide').addClass('reset_size');
  $('.views_slideshow_singleframe_teaser_section').addClass('reset_size');
  
  $('.reset_size').css('height', '');
  $('.reset_size').css('width', '');
  
  $('.reset_size').each(function() {
    var tmp_height = $(this).height();
    var tmp_width = $(this).width();
    if(typeof tmp_height != 'undefined') {
      if(tmp_width > width) {
        width = tmp_width;
      }
    }
    if(typeof tmp_height != 'undefined') {
      if(tmp_height > height) {
        height = tmp_height;
      }
    }
  });
    
  if(width > 0) {
    $('.reset_size').width(width);
    
  }
  if(height > 0) {
    $('.reset_size').height(height);
  }
}
