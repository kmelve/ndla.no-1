Drupal.behaviors.userGuides = function($context) {
  $('.user-guide:not(.user-guides-processed)')
  .addClass('user-guides-processed')
  .click(UserGuides.clicked)
  .hover(UserGuides.rolledOver, UserGuides.rolledOut);
  if (!UserGuides.init) {
    if ($('.video-js').length) {
      UserGuides.libraryLoaded = true;
      UserGuides.cssLoaded = true;
    }
    UserGuides.init = true;
  }
};

var UserGuides = UserGuides || {};

UserGuides.fullScreen = false;
UserGuides.init = false;

UserGuides.rolledOver = function(event) {
  $('#user-guide-thumbnail').attr('src', Drupal.settings['user_guide_thumbnails'][Tray.getIntFromString($(this).attr('href'))]).show();
};

UserGuides.rolledOut = function(event) {
  $('#user-guide-thumbnail').hide();
};

UserGuides.clicked = function(event) {
  event.preventDefault();
  var $loader = $('#user-guide-movie');
  $loader.parent().show();
  UserGuides.loadLibrary();
  UserGuides.loadCSS();
  $loader.html('<img id="user-guide-loader" src="' + Drupal.settings['ndla_utils_disp']['themeUrl'] + '/img/throbber.gif" alt="' + Drupal.t('Loading') + '"/>');
  $.ajax({
    type: "GET",
    url: $(this).attr('href'),
    data: {ajax: 1, autoplay: 1},
    dataType: "html",
    success: function(html) {
      $loader.html(html);
      VideoJS.setup("All", {
        autoplay: true,
        controlsBelow: false, // Display control bar below video instead of in front of
        controlsHiding: true, // Hide controls when mouse is not over the video
        defaultVolume: 0.85, // Will be overridden by user\'s last volume if available
        flashVersion: 9, // Required flash version for fallback
        linksHiding: true // Hide download links when video is supported});
      });
    }
  });
};

// css and js are only loaded when they are needed.
// Most users will not need these because they don't need
// user guides.
UserGuides.loadLibrary = function() {
  if (UserGuides.libraryLoaded) {
    return;
  }
  UserGuides.libraryLoaded = true;
  $.ajax({
    type: "GET",
    url: Drupal.settings.ndla_utils.htmlvideoLibraryUrl,
    dataType: "script",
    success: function(script) {
      eval(script);
    }
  });
};

UserGuides.loadCSS = function() {
  if (UserGuides.cssLoaded) {
    return;
  }
  UserGuides.cssLoaded = true;
  var fileref = document.createElement("link")
  fileref.setAttribute("rel", "stylesheet")
  fileref.setAttribute("type", "text/css")
  fileref.setAttribute("href", Drupal.settings.ndla_utils.htmlvideoCSSUrl)
  document.getElementsByTagName('head')[0].appendChild(fileref);
};