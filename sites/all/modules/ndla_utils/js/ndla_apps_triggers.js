/**
 * @file
 * @ingroup ndla_utils
 * @brief
 *  Handles the Annotate app.
 */

var hasInsertedAnnotateButtons = false;
var annotateServer = "http://annotate.ndla.no";
var annotateDocumentUrl = window.location.href;

if (annotateDocumentUrl.indexOf("test.ndla.no") != -1) annotateServer = "http://annotate.test.ndla.no";
else annotateServer = "http://annotate.ndla.no";

var annotateUrlParts = annotateDocumentUrl.split("?");
annotateGetParts = (annotateUrlParts[1] ? annotateUrlParts[1] : "");
annotateUrlParts = annotateUrlParts[0].split("#");
annotateDocumentUrl = annotateUrlParts[0];

Drupal.behaviors.triggerAnnotate = function() {
  $('#annotate_app_button').click(function(event) {
    initializeAnnotate();
  });
};


function insertButtons() {
  var annotate_buttons = buildAnnotateButtons();
  if ($(".tx_divs").children().length > 0) {
    var tede = $("img.tx_:first").parent().parent();
    $("table.tx_").parent().parent().parent().parent().parent().css('width','0px');
    tede.before(annotate_buttons);
  } else {
    setTimeout(insertButtons,100);
  }
}

function stopAnnotate() {
	window.location.href = annotateDocumentUrl;
}

function buildAnnotateButtons() {
  var gaa_til_annotate = '<td><div class="imgoff" style="font-family: Arial,Verdana,Geneva,Helvetica,sans-serif; color: black; font-size: 8pt; border-width: 0px; border-style: none; line-height: 11pt; text-align: left; text-decoration: none; font-weight: normal; white-space: normal; min-width: 0px; background-color: transparent; width: 22px; padding: 0px; margin: 0px; position: relative; display: block; -moz-user-select: none; height: 22px; top: 0px; left: 0px; overflow: hidden;" title="Go to annotate"><img class="tx_" id="go_to_annotate" src="http://annotate.ndla.no/php/../img/collage1.png" style="background-color: transparent; padding: 0px; margin: 0px; border: medium none; position: absolute; left: -112px; top: -75px;" onclick="document.location.href = \'http://annotate.ndla.no/php/home.php\';return false" alt="Go to annotate" /></div></td>';
  var avslutt_annotate = '<td><div class="imgoff" style="font-family: Arial,Verdana,Geneva,Helvetica,sans-serif; color: black; font-size: 8pt; border-width: 0px; border-style: none; line-height: 11pt; text-align: left; text-decoration: none; font-weight: normal; white-space: normal; min-width: 0px; background-color: transparent; width: 22px; padding: 0px; margin: 0px; position: relative; display: block; -moz-user-select: none; height: 22px; top: 0px; left: 0px; overflow: hidden;" title="Quit annotation"><img class="tx_" id="quit_annotation" src="http://annotate.ndla.no/php/../img/collage1.png" style="background-color: transparent; padding: 0px; margin: 0px; border: medium none; position: absolute; left: -134px; top: -75px;" onclick="stopAnnotate(); return false;" alt="Quit annotation" /></div></td>';
  return gaa_til_annotate+avslutt_annotate;
}


var _annotate_settings = {
  target_ids: "page-content",
  nocontrols: 0,
  url: annotateDocumentUrl + "?aasf=1"
};

var annotateIsInitialized = false;
function initializeAnnotate() {
  if (typeof(SERIA_VARS) !== "undefined" && SERIA_VARS.IS_LOGGED_IN) {
    if (!annotateIsInitialized) {
      annotateIsInitialized = true;

      var divObj = $('<div id=\'gardin\' style=\'position: absolute; left: 0px; top: 0px; z-index: 1400; background-color: #000000; width: 100%; opacity: 0.4; filter: alpha(opacity=40);\'></div>');
      $(divObj).css({'height': $(document).height() + 'px'});
      setInterval(function() {
        $('.tx_nosel').css('zIndex', 2000);
      }, 1000);

      var imgObj = $('<img src=\'/sites/all/themes/ndla2010/img/lightbox-close.png\' style=\'position: absolute; z-index: 1500; left: 10px; top: 10px; cursor: pointer;\' onclick=\'stopAnnotate();\'>');
      $(document.body).append(imgObj);
      $(document.body).append(divObj);
      $(divObj).fadeIn(1000, function(){});

      $('#page-content').css({'position': 'relative', 'zIndex': '1401', 'backgroundColor': '#ffffff'});
      $('#page-content a').each(function() {
        $(this).unbind('click');
      });

      loadAnnotateJs(annotateServer + "/js/web_annotate.js");
    }
  } else {
    if (typeof(SERIA_VARS) !== "undefined") {

      newUrl = "http://" + document.location.hostname + document.location.pathname + $.query.set('aas', '1').remove('aasf') + document.location.hash;

/*
      // Do this another way. Include jquery query plugin?
      if (annotateGetParts) {
        newUrl = document.location.href + "&aas=1";
      } else {
        newUrl = document.location.href + "?aas=1";
      }

      // remove aasf query parameter. If not a possible loop will accour. F.ex if the login fails.
      newUrl = newUrl.replace("&aasf=1", "");
      newUrl = newUrl.replace("?aasf=1&", "?");
      newUrl = newUrl.replace("?aasf=1", "");
*/
//alert(newUrl);
      top.location.href = '/login.php?continue=' + escape(newUrl);
    }
  }
}

function loadAnnotateJs(filename){
  var fileref=document.createElement("script");
  fileref.setAttribute("type", "text/javascript");
  fileref.setAttribute("src", filename);

  if (typeof fileref!="undefined")
    document.getElementsByTagName("head")[0].insertBefore(fileref, document.getElementsByTagName("head")[0].children[0]);

  waitForAnnotateToInitialize();
}

function waitForAnnotateToInitialize() {
  if (typeof loadJS=="undefined") {
    setTimeout(waitForAnnotateToInitialize, 100);
  } else {
    loadJS();
    // Insert the extra buttons
    insertButtons();
    setTimeout(triggerMouseUp, 2000);
  }
}

function triggerMouseUp() {
  try {
    ndla_textselect_unbind();
  }
  catch(err) {
    ;
  }
  $('.node').mouseup();
}

$(document).ready(function() {
  if (typeof(SERIA_VARS) !== "undefined" && (SERIA_VARS.HTTP_REFERER.indexOf(annotateServer) === 0 || annotateGetParts.indexOf('aas=1') != -1)) {
    if (SERIA_VARS.IS_LOGGED_IN) initializeAnnotate();
  } else if (annotateGetParts.indexOf('aasf=1') != -1) {
    initializeAnnotate();
  }
});




jQuery.query = {numbers: false, hash: false};

/**
 * jQuery.query - Query String Modification and Creation for jQuery
 * Written by Blair Mitchelmore (blair DOT mitchelmore AT gmail DOT com)
 * Licensed under the WTFPL (http://sam.zoy.org/wtfpl/).
 * Date: 2009/8/13
 *
 * @author Blair Mitchelmore
 * @version 2.1.7
 *
 **/
new function(settings) { 
  // Various Settings
  var $separator = settings.separator || '&';
  var $spaces = settings.spaces === false ? false : true;
  var $suffix = settings.suffix === false ? '' : '[]';
  var $prefix = settings.prefix === false ? false : true;
  var $hash = $prefix ? settings.hash === true ? "#" : "?" : "";
  var $numbers = settings.numbers === false ? false : true;
  
  jQuery.query = new function() {
    var is = function(o, t) {
      return o != undefined && o !== null && (!!t ? o.constructor == t : true);
    };
    var parse = function(path) {
      var m, rx = /\[([^[]*)\]/g, match = /^([^[]+)(\[.*\])?$/.exec(path), base = match[1], tokens = [];
      while (m = rx.exec(match[2])) tokens.push(m[1]);
      return [base, tokens];
    };
    var set = function(target, tokens, value) {
      var o, token = tokens.shift();
      if (typeof target != 'object') target = null;
      if (token === "") {
        if (!target) target = [];
        if (is(target, Array)) {
          target.push(tokens.length == 0 ? value : set(null, tokens.slice(0), value));
        } else if (is(target, Object)) {
          var i = 0;
          while (target[i++] != null);
          target[--i] = tokens.length == 0 ? value : set(target[i], tokens.slice(0), value);
        } else {
          target = [];
          target.push(tokens.length == 0 ? value : set(null, tokens.slice(0), value));
        }
      } else if (token && token.match(/^\s*[0-9]+\s*$/)) {
        var index = parseInt(token, 10);
        if (!target) target = [];
        target[index] = tokens.length == 0 ? value : set(target[index], tokens.slice(0), value);
      } else if (token) {
        var index = token.replace(/^\s*|\s*$/g, "");
        if (!target) target = {};
        if (is(target, Array)) {
          var temp = {};
          for (var i = 0; i < target.length; ++i) {
            temp[i] = target[i];
          }
          target = temp;
        }
        target[index] = tokens.length == 0 ? value : set(target[index], tokens.slice(0), value);
      } else {
        return value;
      }
      return target;
    };
    
    var queryObject = function(a) {
      var self = this;
      self.keys = {};
      
      if (a.queryObject) {
        jQuery.each(a.get(), function(key, val) {
          self.SET(key, val);
        });
      } else {
        jQuery.each(arguments, function() {
          var q = "" + this;
          q = q.replace(/^[?#]/,''); // remove any leading ? || #
          q = q.replace(/[;&]$/,''); // remove any trailing & || ;
          if ($spaces) q = q.replace(/[+]/g,' '); // replace +'s with spaces
          
          jQuery.each(q.split(/[&;]/), function(){
            try {
              var key = decodeURIComponent(this.split('=')[0] || "");
              var val = decodeURIComponent(this.split('=')[1] || "");
            }
            catch(e) {
              //Unable to parse URL. This happens while import images from scanpix.no;
            }
            
            if (!key) return;
            
            if ($numbers) {
              if (/^[+-]?[0-9]+\.[0-9]*$/.test(val)) // simple float regex
                val = parseFloat(val);
              else if (/^[+-]?[0-9]+$/.test(val)) // simple int regex
                val = parseInt(val, 10);
            }
            
            val = (!val && val !== 0) ? true : val;
            
            if (val !== false && val !== true && typeof val != 'number')
              val = val;
            
            self.SET(key, val);
          });
        });
      }
      return self;
    };
    
    queryObject.prototype = {
      queryObject: true,
      has: function(key, type) {
        var value = this.get(key);
        return is(value, type);
      },
      GET: function(key) {
        if (!is(key)) return this.keys;
        var parsed = parse(key), base = parsed[0], tokens = parsed[1];
        var target = this.keys[base];
        while (target != null && tokens.length != 0) {
          target = target[tokens.shift()];
        }
        return typeof target == 'number' ? target : target || "";
      },
      get: function(key) {
        var target = this.GET(key);
        if (is(target, Object))
          return jQuery.extend(true, {}, target);
        else if (is(target, Array))
          return target.slice(0);
        return target;
      },
      SET: function(key, val) {
        var value = !is(val) ? null : val;
        var parsed = parse(key), base = parsed[0], tokens = parsed[1];
        var target = this.keys[base];
        this.keys[base] = set(target, tokens.slice(0), value);
        return this;
      },
      set: function(key, val) {
        return this.copy().SET(key, val);
      },
      REMOVE: function(key) {
        return this.SET(key, null).COMPACT();
      },
      remove: function(key) {
        return this.copy().REMOVE(key);
      },
      EMPTY: function() {
        var self = this;
        jQuery.each(self.keys, function(key, value) {
          delete self.keys[key];
        });
        return self;
      },
      load: function(url) {
        var hash = url.replace(/^.*?[#](.+?)(?:\?.+)?$/, "$1");
        var search = url.replace(/^.*?[?](.+?)(?:#.+)?$/, "$1");
        return new queryObject(url.length == search.length ? '' : search, url.length == hash.length ? '' : hash);
      },
      empty: function() {
        return this.copy().EMPTY();
      },
      copy: function() {
        return new queryObject(this);
      },
      COMPACT: function() {
        function build(orig) {
          var obj = typeof orig == "object" ? is(orig, Array) ? [] : {} : orig;
          if (typeof orig == 'object') {
            function add(o, key, value) {
              if (is(o, Array))
                o.push(value);
              else
                o[key] = value;
            }
            jQuery.each(orig, function(key, value) {
              if (!is(value)) return true;
              add(obj, key, build(value));
            });
          }
          return obj;
        }
        this.keys = build(this.keys);
        return this;
      },
      compact: function() {
        return this.copy().COMPACT();
      },
      toString: function() {
        var i = 0, queryString = [], chunks = [], self = this;
        var encode = function(str) {
          str = str + "";
          if ($spaces) str = str.replace(/ /g, "+");
          return encodeURIComponent(str);
        };
        var addFields = function(arr, key, value) {
          if (!is(value) || value === false) return;
          var o = [encode(key)];
          if (value !== true) {
            o.push("=");
            o.push(encode(value));
          }
          arr.push(o.join(""));
        };
        var build = function(obj, base) {
          var newKey = function(key) {
            return !base || base == "" ? [key].join("") : [base, "[", key, "]"].join("");
          };
          jQuery.each(obj, function(key, value) {
            if (typeof value == 'object') 
              build(value, newKey(key));
            else
              addFields(chunks, newKey(key), value);
          });
        };
        
        build(this.keys);
        
        if (chunks.length > 0) queryString.push($hash);
        queryString.push(chunks.join($separator));
        
        return queryString.join("");
      }
    };
    
    return new queryObject(location.search, location.hash);
  };
}(jQuery.query || {}); // Pass in jQuery.query as settings object

