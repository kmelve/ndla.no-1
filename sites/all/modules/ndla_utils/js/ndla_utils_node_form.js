(function () {

  var $win = $(window);

  /**
  * Check if an element is within our viewport.
  *
  * @param {Object} $el
  * @return {Boolean}
  */
  var elOnScreen = function ($el) {
    var bounds = {};
    var viewport = {
        top : $win.scrollTop(),
        left : $win.scrollLeft()
    };

    viewport.right = viewport.left + $win.width();
    viewport.bottom = viewport.top + $win.height();

    bounds = $el.offset();
    bounds.right = bounds.left + $el.outerWidth();
    bounds.bottom = bounds.top + $el.outerHeight();

    return (!(
      viewport.right < bounds.left ||
      viewport.left > bounds.right ||
      viewport.bottom < bounds.top ||
      viewport.top > bounds.bottom
    ));
  };

  /**
  * Make the node-form save-buttons always appear
  * within the viewport.
  */
  Drupal.behaviors.fixButtonsToScreen = function () {
    $stepDisplay = $('#form-step-display');

    // Do nothing if the user has enabled improved editing.
    if ($stepDisplay.length === 0) {
      var $static    = $('#ndla_utils_red--static');
      var $container = $('#ndla_utils_red--buttons');

      $win.scroll(function () {
        if (elOnScreen($static)) {
          $container.removeClass('fixed');
        }
        else {
          $container.addClass('fixed');
        }
      });
    }
  };
}());
