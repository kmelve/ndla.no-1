Drupal.behaviors.istribute_download = function() {
    $('.istribute_download_link').click(function() {
        var id = $(this).attr('id');
        $('#' + id + '_select').fadeIn();
    });
    $('.istribute_download_select').change(function() {
        var url = $(this).val();
        if(url != '') {
            window.location = url;
        }
    });
};