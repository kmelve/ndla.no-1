jQuery(document).ready(function($) {
  if($.cookie("acceptCookie") != "1") {
    $("body").append("<div id='cookie-warning' onClick='acceptCookieWarn()'><img src='/sites/all/modules/ndla_utils/images/disp/close-cross.png'/><div class='content'>" + Drupal.settings.cookie_warning + "</div></div>");
  }
});
function acceptCookieWarn() {
  $.cookie("acceptCookie", "1", {expires: 365});
  $("#cookie-warning").slideUp();
  return false;
}

