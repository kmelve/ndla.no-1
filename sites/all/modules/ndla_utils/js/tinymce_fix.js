/**
 * A strange tinyMCE bug (?). When form steps is
 * not enabled images will not be selectable. But
 * this fixes itself if tinyMCE textarea is hidden
 * and then shown again.
 */
Drupal.behaviors.tinymce_bugfix = function() {
  $('#ndla_paragraphs_wrapper').hide('fast', function(){
    $('#ndla_paragraphs_wrapper').show();
  });
}