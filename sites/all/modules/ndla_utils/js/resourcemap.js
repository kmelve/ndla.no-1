var NdlaRM = NdlaRM || {};

NdlaRM.w = 770;
NdlaRM.h = 440;
NdlaRM.r2 = Math.pow(NdlaRM.h / 2, 2);
NdlaRM.inited = false;
NdlaRM.raphLoaded = false;

Drupal.behaviors.NdlaRM = function() {
  $('.resourcemap-button:not(.ndlarm-processed)').click(NdlaRM.mapBtnClicked).addClass('ndlarm-processed');
  if (NdlaRM.$ != undefined && !NdlaRM.inited) {
    NdlaRM.init();
  }
};

NdlaRM.init = function() {
  $('.close-window', NdlaRM.$).click(function() {
    NdlaRM.unbind_outside_close();
    NdlaRM.$.hide();
  });
  $('.navigation span', NdlaRM.$).click(NdlaRM.navigate);
  
  NdlaRM.$terms = $('.term');
  NdlaRM.$terms.each(function() {
    var $h3 = $('h3, .h3', this),
    id = $h3.attr('class').split('-', 2)[1];
    if (NdlaRM.$terms.filter('.term-' + id).length) {
      $h3.data('id', id).addClass('clickable').click(NdlaRM.termClick);
    }
  });
  
  NdlaRM.center($('.main', NdlaRM.$), NdlaRM.w / 2, NdlaRM.h / 2);
  NdlaRM.svg = Raphael('resourcemap-drawings', NdlaRM.w, NdlaRM.h);
  NdlaRM.positionTerms(NdlaRM.$terms.filter('.term-0'));
  
  NdlaRM.inited = true;
  NdlaRM.$terms.first().find('a').first().focus();
};

NdlaRM.termClick = function() {
  if (NdlaRM.lock) {
    return;
  }
  NdlaRM.lock = true;
  var $this = $(this),
  $grandParent = $this.parent().parent(),
  $main = $('.main', NdlaRM.$),
  $breadcrumbs = $('.breadcrumbs', NdlaRM.$),
  $drawings = $('#resourcemap-drawings', NdlaRM.$),
  doneOnce = false,
  $hide = NdlaRM.$terms.filter(':visible').not($grandParent).add($main).add($drawings);
  $hide.animate({
    opacity: 0.0
  }, 300, function() {
    if (doneOnce) {
      return;
    }
    doneOnce = true;
    $hide.hide().css({opacity: 1.0});
    
  });
  NdlaRM.$active = $grandParent.data('oldLeft', parseInt($grandParent.css('left'))).data('oldTop', parseInt($grandParent.css('top')));
  $grandParent.animate({
    left: (NdlaRM.w / 2) - (parseInt($grandParent.css('width')) / 2), 
    top: (NdlaRM.h / 2) - (parseInt($grandParent.css('height')) / 2)
  }, 400, function() {
    $this.removeClass('clickable').unbind('click');
    $('<span class="clickable">' + $('h2', $main).text() + '</span>').click(NdlaRM.breadcrumbsClick).appendTo($breadcrumbs);
    $('<span>&nbsp;&gt;&nbsp;' + $this.text() + '</span>').appendTo($breadcrumbs);
    var $terms = NdlaRM.$terms.filter('.term-' + $this.data('id')),
    $show = $terms.add($drawings).add($('span', $breadcrumbs)).show().css({opacity: 0.0})
    NdlaRM.positionTerms($terms);
    $show.animate({
      opacity: 1.0
    }, 300, function() {
      NdlaRM.lock = false;
    });
  });
}

NdlaRM.breadcrumbsClick = function() {
  if (NdlaRM.lock) {
    return;
  }
  NdlaRM.lock = true;
  var $breadcrumbs = $('.breadcrumbs span', NdlaRM.$),
  $drawings = $('#resourcemap-drawings', NdlaRM.$),
  doneOnce = false,
  $hide = NdlaRM.$terms.filter(':visible').not(NdlaRM.$active).add($breadcrumbs).add($drawings);
  $hide.animate({
    opacity: 0.0
  }, 300, function() {
    if (doneOnce) {
      return;
    }
    doneOnce = true;
    $hide.hide().css({opacity: 1.0});
    $breadcrumbs.remove();
  });
  NdlaRM.$active.animate({
    left: NdlaRM.$active.data('oldLeft'),
    top: NdlaRM.$active.data('oldTop')
  }, 400, function() {
    var $terms = NdlaRM.$terms.filter('.term-0'),
    $show = $terms.not(NdlaRM.$active).add($drawings).add($('.main', NdlaRM.$)).show().css({opacity: 0.0});
    $('h3', NdlaRM.$active).addClass('clickable').click(NdlaRM.termClick);
    NdlaRM.positionTerms($terms);
    $show.animate({
      opacity: 1.0
    }, 300, function() {
      NdlaRM.lock = false;
    });
  });
}

NdlaRM.mapBtnClicked = function(event) {
  if (NdlaRM.$loader != undefined) {
    return;
  }
  if (NdlaRM.$ != undefined) {
    NdlaRM.bind_outside_close();
    NdlaRM.$.show();
    return;
  }
  NdlaRM.$loader = $('<img id="resourcemap-loader" src="' + Drupal.settings['ndla_utils_disp']['themeUrl'] + '/img/throbber.gif" alt="' + Drupal.t('Loading resource map') + '" title="' + Drupal.t('Loading resource map') + '"/>').appendTo('body');
  if (!$('#container').hasClass('w960')) {
    NdlaRM.$loader.addClass('wide');
  }
  NdlaRM.loadRaph(function() {
    var url = Drupal.settings.ndla_utils_disp.resourceMapUrl
    if (Drupal.settings.ndla_utils_disp.context.fag != undefined) {
      url = url.replace('resourcemap', 'resourcemap/' + Drupal.settings.ndla_utils_disp.context.fag);
    }
  
    $.get(url, function(html) {
      NdlaRM.$ = $(html).appendTo('body');
      Drupal.attachBehaviors(NdlaRM.$);
      NdlaRM.$loader.remove();
      delete NdlaRM.$loader;
      NdlaRM.bind_outside_close();
    });
  });
};

NdlaRM.navigate = function() {
  var url = Drupal.settings.ndla_utils_disp.resourceMapUrl
  if ($(this).hasClass('course')) {
    url = url.replace('resourcemap', 'resourcemap/' + Drupal.settings.ndla_utils_disp.context.fag);
  }
  
  $('.breadcrumbs, .navigation, .content-main', NdlaRM.$).remove();
  $('<img id="resourcemap-loader" src="' + Drupal.settings['ndla_utils_disp']['themeUrl'] + '/img/throbber.gif" alt="' + Drupal.t('Loading resource map') + '" title="' + Drupal.t('Loading resource map') + '"/>').appendTo($('.middle', NdlaRM.$));
  
  $.get(url, function(html) {
    NdlaRM.$.remove();
    NdlaRM.inited = false;
    NdlaRM.$ = $(html).appendTo('body');
    Drupal.attachBehaviors(NdlaRM.$);
  });
}

NdlaRM.loadRaph = function(done) {
  if (NdlaRM.raphLoaded) {
    done();
    return;
  }
  $.ajax({
    type: "GET",
    url: Drupal.settings.ndla_utils_disp.raphUrl,
    dataType: "script",
    success: function(script) {
      eval(script);
      NdlaRM.raphLoaded = true;
      done();
    }
  });
}

NdlaRM.positionTerms = function($terms) {
  NdlaRM.svg.clear();
  NdlaRM.math = {
    xCoords: [],
    tW: $terms.width(),
    tH: $terms.height(),
    cX: NdlaRM.w / 2,
    cY: NdlaRM.h / 2
  }
  var options = NdlaRM.termsPositionMath($terms.length),
  sign = 1, y = 0, i = -1, cX = NdlaRM.math.cX, lastX = cX;
  NdlaRM.yD = options.yD - NdlaRM.math.tH;
  
  if (options.even) {
    var $last;
    $terms.each(function(){
      if (y == 0) {
        lastX = NdlaRM.center($(this), cX, 25);
      }
      else if (Math.round(y) == options.h) {
        if (lastX > cX + 0.5 * NdlaRM.math.tW + NdlaRM.yD) {
          lastX = cX + 0.5 * NdlaRM.math.tW + NdlaRM.yD;
          NdlaRM.left($last, lastX, y - options.yD, true);
          NdlaRM.drawLine($last, true);
        }
        lastX = NdlaRM.center($(this), cX, options.h - 25);
      }
      else if (sign > -1) {
        lastX = NdlaRM.left($(this), lastX, y);
      }
      else {
        lastX = NdlaRM.right($(this), y, i);
      }
      NdlaRM.drawLine($(this));
      $last = $(this);
      i += sign;
      y += sign * options.yD;
      if (y + sign * options.yD > options.h) {
        sign = -1;
      }
    });
  }
  else {
    $terms.each(function(){
      if (y == 0) {
        lastX = NdlaRM.center($(this), cX, $terms.length > 6 ? 25 : 0);
      }
      else if (sign > -1) {
        lastX = NdlaRM.left($(this), lastX, y);
      }
      else {
        lastX = NdlaRM.right($(this), y, i);
      }
      NdlaRM.drawLine($(this));
      i += sign;
      y += sign * options.yD;
      if (sign == 0) {
        sign = -1;
      }
      else if ((Math.round((y + sign * options.yD) * 100) / 100) > (Math.round(options.h * 100) / 100)) {
        sign = 0;
      }
    });
  }
}

NdlaRM.termsPositionMath = function(num) {
  var h, yD, even, rD = 2 * Math.PI / num;
  if (num % 2) {
    even = false;
    var radians = rD * Math.floor(num / 2),
    ratio = (-Math.cos(radians) + 1) / 2;
    h = ratio * NdlaRM.h;
    yD = h / Math.floor(num / 2);
  }
  else {
    even = true;
    yD = NdlaRM.h / (num / 2);
    h = NdlaRM.h;
  }
  return {
    'h': h,
    'yD': yD,
    'even': even
  }
}

NdlaRM.drawLine = function($element, correct) {
  if (correct) {
    NdlaRM.lastLine.remove();
  }
  var pos = $element.position();
  NdlaRM.lastLine = NdlaRM.svg.path('M' + (pos.left + NdlaRM.math.tW / 2) + ' ' + (pos.top + NdlaRM.math.tH / 2) + ' L' + NdlaRM.math.cX + ' ' + NdlaRM.math.cY).show();
  NdlaRM.lastLine.attr({stroke: '#ddd'})
}

NdlaRM.center = function($el, x, y) {
  var xC = x - $el.width()/2;
  var yC = y - $el.height()/2;
  $el.css({
    left: xC,
    top: yC
  });
  return xC;
}

NdlaRM.left = function($el, lastX, y, correct) {
  var xC;
  if (correct) {
    xC = lastX;
  }
  else {
    xC = NdlaRM.math.cX + Math.sqrt(NdlaRM.r2 - Math.pow(y - NdlaRM.math.cY, 2));
    if (xC > lastX + NdlaRM.math.tW + NdlaRM.yD) {
      xC = lastX + NdlaRM.math.tW + NdlaRM.yD;
    }
  }
  var yC = y - $el.height() / 2;
  $el.css({
    left: xC,
    top: yC
  });
  if (correct) {
    NdlaRM.math.xCoords[NdlaRM.math.xCoords.length - 1] = xC - NdlaRM.math.cX;
  }
  else {
    NdlaRM.math.xCoords.push(xC - NdlaRM.math.cX);
  }
  return xC;
}

NdlaRM.right = function($el, y, i) {
  var xC = NdlaRM.math.cX - NdlaRM.math.xCoords[i] - NdlaRM.math.tW;
  var yC = y - $el.height()/2;
  $el.css({
    left: xC,
    top: yC
  });
  return xC;
}



NdlaRM.bind_outside_close = function() {
  $('body').bind('click', function(e) {
    if($(e.target).hasClass('resourcemap-button') == false) {
      NdlaRM.$.hide();
      NdlaRM.unbind_outside_close();
    }
  });
  $('#resourcemap').bind('click', function(event) {
    event.cancelBubble = true;
    if (event.stopPropagation) {
      event.stopPropagation();
    }
  });
  $(document).bind('keyup', function(e) {
    if(e.keyCode == 27) {
      NdlaRM.$.hide();
      NdlaRM.unbind_outside_close();
    }
  });
  if(window.event) {
    window.event.cancelBubble = true;
    if (window.event.stopPropagation) {
      window.event.stopPropagation();
    }
  }
}

NdlaRM.unbind_outside_close = function() {
  $('body').unbind('click');
  $('#resourcemap').unbind('click');
  $(document).unbind('keyup');
}
