<?php
module_load_include('inc', 'biblio', 'biblio_style_classic');

function biblio_style_ndla_author_options() {
  $author_options = array(
    'BetweenAuthorsDelimStandard'     =>  ', ',      //4
    'BetweenAuthorsDelimLastAuthor'   =>  ', ' . t('and'). ' ',    //5
    'AuthorsInitialsDelimFirstAuthor' =>  ' ',      //7
    'AuthorsInitialsDelimStandard'    =>  ' ',       //8
    'betweenInitialsDelim'            =>  '. ',      //9
    'initialsBeforeAuthorFirstAuthor' =>  TRUE,     //10
    'initialsBeforeAuthorStandard'    =>  FALSE,      //11
    'shortenGivenNames'               =>  FALSE,      //12
    'numberOfAuthorsTriggeringEtAl'   =>  10,        //13
    'includeNumberOfAuthors'          =>  10,        //14
    'customStringAfterFirstAuthors'   =>  ', et al.',//15
    'encodeHTML'                      =>  TRUE
  );
  return $author_options;
}

/**
 * Apply a bibliographic style to the node
 *
 *
 * @param $node
 *   An object containing the node data to render
 * @param $base
 *   The base URL of the biblio module (defaults to /biblio)
 * @param $inline
 *   A logical value indicating if this is being rendered within the
 *   Drupal framwork (false) or we are just passing back the html (true)
 * @return
 *   The styled biblio entry
 */
function biblio_style_ndla($node, $base = 'biblio', $inline = FALSE) {
  //echo "<pre>" . print_r($node, true) . "</pre>";
  //die("ok");
  /*Navn (Fornavn Etternavn), 
  År (Year of publication). 
  Tittel, 
  evt. Tittel på avis / tidsskrift (Series Title), 
  evt. Utgave (Edition), 
  evt. Utgiver (Publisher). */
  $output = '';
  $author_options = biblio_style_ndla_author_options();
  $authors = theme('biblio_format_authors', $node->biblio_contributors[1], $author_options, $inline);
  if (!empty ($node->biblio_citekey) &&(variable_get('biblio_display_citation_key',0))) {
    $output .= '[' . check_plain($node->biblio_citekey) . '] ';
  }
  $output .= '<span class="biblio-title">';
  $url = biblio_get_title_url_info($node, $base, $inline);
  $output .= l($node->title, $url['link'], $url['options']);
  if ($node->biblio_secondary_title) {
    $output .= ', ' . check_plain($node->biblio_secondary_title);
  }
  if (isset ($node->biblio_year)) {
    $output .= ' (' . check_plain($node->biblio_year) . ")\n";
  }
  $output .= "</span>, \n";
  $output .= '<span class="biblio-authors">' . $authors . "</span> \n";

  if(!empty($node->biblio_edition)) {
    $output .= ', ' . t('Edition') . ': ' . check_plain($node->biblio_edition);
  }
  if(!empty($node->biblio_publisher)) {
    $output .= ', ' . t('Publisher') . ': ' . check_plain($node->biblio_publisher);
  }
  return filter_xss($output, biblio_get_allowed_tags());
}