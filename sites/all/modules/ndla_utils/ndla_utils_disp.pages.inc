<?php

/**
 * Return at resourcemap for a given course
 *
 * @param object $course
 *  The og node object
 */
function ndla_utils_disp_course_resourcemap($context_id = NULL) {
  global $language;

  $terms = ndla_sitemap_get_types($context_id);
  $course = ndla_utils_disp_get_course();
  
  $main = (object) array(
    'name' => 'NDLA',
    'resources' => 0,
    'url_options' => array(
      'attributes' => array('title' => t('Search for these resources'))
    )
  );
  
  $query = array('language[0]' => $language->language, 'language[1]' => 'und', 'site' => 'ndla');
  if ($context_id) {
    $tid = ndla_fag_taxonomy_get_term_from_og($context_id);
    $query['f[im_vid_100016][]'] = $tid;
    $main->name = i18ntaxonomy_translate_term_name(taxonomy_get_term(ndla_fag_taxonomy_get_term_from_og($context_id)));
    $main->url_options['query'] = $query;
  }
  else {
    $main->url_options['query'] = $query;
  }
  

  foreach ($terms as &$term) {
    $main->resources += $term->resources;
  }

  $navigation = '';
  if ($course) {
    if ($context_id == $course->nid) {
      $navigation = '<span class="clickable">' . t('View for all of NDLA') . '</span>';
    }
    else {
      $navigation = '<span class="clickable course">' . t('View for !fag', array('!fag' =>  i18ntaxonomy_translate_term_name(taxonomy_get_term(ndla_fag_taxonomy_get_term_from_og($course->nid))))) . '</span>';
    }
  }

  print theme('ndla_utils_disp_resourcemap', $main, ndla_utils_disp_resourcemap_terms($terms, $query), $navigation);
}

function ndla_utils_disp_resourcemap_terms(&$terms, &$query = NULL, $parent = 0) {
  $html = '';
  foreach ($terms as &$term) {
    if ($term->resources == 0) {
      continue;
    }
    $term->url_options = array(
      'attributes' => array('title' => t('Search for these resources'))
    );
    if ($query) {
      $term->url_options['query'] = $query;
    }
    $term->url_options['query']['f[im_vid_100004][]'] = $term->tid;
    $html .= theme('ndla_utils_disp_resourcemap_term', $term, $parent) . ndla_utils_disp_resourcemap_terms($term->children, $query, $term->tid);
  }
  return $html;
}

/**
 * Page callback for the user guide page
 *
 * @param string $video
 *  The title of the video we want to show
 * @return int|boolean
 *  nid of the video or FALSE if no video where found
 */
function ndla_utils_disp_user_guide($video_title = FALSE) {
  drupal_add_css(drupal_get_path('module', 'ndla_utils_disp') . '/css/user_guide_page.css');
  drupal_add_js(drupal_get_path('module', 'ndla_utils_disp') . '/js/user_guides.js');
  $guides = ndla_utils_get_user_guides();
  $video_nid = ndla_utils_disp_user_guide_video($video_title);
  $start_video = $video_nid ? htmlvideo_display(htmlvideo_get_light_node($video_nid), array('width' => 611, 'height' => 344, 'autoplay' => TRUE)) : FALSE;
  return theme('ndla_utils_disp_user_guide_page', $guides, $start_video);
}

/**
 * Helper function finding nid for videos from a video title
 *
 * @param string $video_title
 *  Title of the video we are to search for
 */
function ndla_utils_disp_user_guide_video($video_title) {
  $nid = db_result(db_query(
    "SELECT n.nid
    FROM {node} n
    INNER JOIN {ndla_utils_user_guides} nuug ON n.nid = nuug.nid
    WHERE n.type = 'htmlvideo' AND n.title = '%s'",
    $video_title
  ));
  return $nid ? $nid : FALSE;
}
