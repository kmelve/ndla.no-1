<?php
// $Id$

/**
 * @file
 * @ingroup ndla_utils
 * @brief
 *  Helper functions for hook_block
 */

/**
 * Provides content for the rights block
 *
 * @param object $node
 *  The node beeing displayed
 * @return string
 *  Themed block content
 */
function _ndla_utils_disp_rights_content($node = NULL, $version = 1) {
  if ($ajax_loader = _ndla_utils_disp_is_ajax_block('rights', $node)) {
    return $ajax_loader;
  }
  $creative_commons = '';
  if(function_exists('get_license_html') && $node->cc_lite_license) {
    $creative_commons = get_license_html($node->cc_lite_license);
  }
  if(!empty($creative_commons)) {
    return _ndla_utils_disp_ajax_or_not('rights', theme('ndla_utils_disp_rights_v' . $version, $node, $creative_commons));
  }
}

/**
 * Provide content for the copyright page
 *
 * @param object $node
 *  The node we want to show copyright information for
 * @return string
 *  HTML content representing the copyright information
 */
function ndla_utils_disp_rights_page($node) {
  $creative_commons = '';
  if(function_exists('get_license_html') && $node->cc_lite_license) {
    $creative_commons = get_license_html($node->cc_lite_license);
  }
  return theme('ndla_utils_disp_rights_content', $node, $creative_commons);
}

function _ndla_utils_disp_general_use_content($ajax = NULL) {
  if ($ajax_loader = _ndla_utils_disp_is_ajax_block('general_use', $ajax)) {
    return $ajax_loader;
  }
  return _ndla_utils_disp_ajax_or_not('general_use', check_markup(variable_get("ndla_utils_disp_general_use_" . $GLOBALS['language']->prefix, '')));
}

/**
 * Provides content for the node version block
 *
 * The content will be provided directly or via ajax depending on the
 * block setting.
 *
 * @param $node
 *  The node beeing displayed
 * @return
 *  Themed block content or a script for loading the block content using ajax
 */
function _ndla_utils_disp_versions_content($node = NULL) {
  global $language, $user, $base_url;
  
  if ($ajax_loader = _ndla_utils_disp_is_ajax_block('versions', $node))
    return $ajax_loader;

  $permanent_link = $base_url . "/node/".$node->nid;

  if($node->type == 'image') {
    return _ndla_utils_disp_ajax_or_not('versions', theme('ndla_utils_disp_versions_images', $node->images));
  }
  else {
    //$node = node_prepare($node, $teaser);
    if(function_exists('publishing_queue_load_most_recent_revision')) {
      publishing_queue_load_most_recent_revision($node, FALSE, TRUE);
    }

    $difficulties = _ndla_utils_disp_other_versions($node);
    $languages = language_list();
    $translations = translation_node_get_translations($node->tnid);

    //Do we have more language versions of this node?
    if (is_array($translations)) {
      foreach ($translations as $ver) {
        $item_text = t($languages[$ver->language]->native);
        $ver_node = ndla_utils_load_node($ver->nid);

        $lang_versions[$ver->language] = array(
          'nid' => $ver->nid,
          'title' => $item_text,
          'language' => $languages[$ver->language],
          'current_node' => $ver->nid == $node->nid,
          'not_published' => $ver_node->status == 0 && $ver->nid != $node->nid
        );
      }
    }
    else {
      $lang = ($node->language) ? $node->language : $language->language;
      $lang_versions[] = array(
        'nid' => $node->nid,
        'title' => t($languages[$lang]->native),
        'language' => $languages[$node->language],
        'current_node' => TRUE,
        'not_published' => 0
      );
    }

    ksort($lang_versions);
    return _ndla_utils_disp_ajax_or_not('versions', theme('ndla_utils_disp_versions', $difficulties, $lang_versions, "printpdf/$nid", $permanent_link));
  }
}

/**
 * Returns an array with nodes which has the same GUID as the node in question
 * and has the taxonomy "vansklighetsgrad" set.
 *
 * @param stdObject $node The node
 * @param int $vid The vocabulary id
 * @return unknown
 */
function _ndla_utils_disp_other_versions($node, $vid = 14) {
  global $language, $user;
  $current_taxonomy = '';

  /* GUID is empty or utdanning_guid module not installed */
  if(!isset($node->utdanning_guid) || empty($node->utdanning_guid)) {
    return;
  }

  foreach($node->taxonomy as $tax) {
    if($tax->vid == $vid) { //vansklighetsgrad
      $current_taxonomy = $tax->tid;
      break;
    }
  }

  if(empty($current_taxonomy)) {
    return;
  }

  $nodes = array();
  $res = db_query("SELECT ug.nid, n.status, td.name FROM {utdanning_guid} ug
                    INNER JOIN {node} n ON (n.nid = ug.nid)
                    INNER JOIN {term_node} tn ON (tn.vid = n.vid)
                    INNER JOIN {term_data} td ON (td.tid = tn.tid)
                    WHERE ug.guid = '%s' AND (n.language = '%s' OR n.language = '') AND td.vid = %d
                    ORDER BY td.weight", $node->utdanning_guid, $language->language, $vid);

  $item_list = array();
  $is_role_member = (in_array('fagredaksjonen', $user->roles) || in_array('administrator', $user->roles) || in_array('fagansvarlig', $user->roles) || in_array('moderator', $user->roles));

  while($row = db_fetch_object($res)) {
    if($is_role_member || $row->status == 1) {
      $item_list[] = array(
        'nid' => $row->nid,
        'title' => $row->name,
        'current_node' => $row->nid == $node->nid,
        'not_published' => $row->status == 0 && $row->nid != $node->nid
      );
    }
  }

  if(!is_array($item_list)) {
    foreach($node->taxonomy as $tax) {
      if($tax->vid == $vid) {
        $item_list[] = array(
          'nid' => $node->nid,
          'title' => $tax->name,
          'current_node' => 1,
          'not_published' => 0
        );
      }
    }
  }

  return $item_list;
}

/**
 * Provides content for the tips/share block
 *
 * The content will be provided directly or via ajax depending on the
 * block setting.
 *
 * @param $node
 *  The node beeing displayed
 * @return
 *  Themed block content or a script for loading the block content using ajax
 */
function _ndla_utils_disp_share_content($node = NULL) {
  if ($ajax_loader = _ndla_utils_disp_is_ajax_block('share', $node))
    return $ajax_loader;
  $links = $printmail = '';
  if(module_exists('service_links')) {
    $links = service_links_render($node);
  }

  if(module_exists('print_mail') && user_access('access print')) {
    global $language, $base_url;
    if(isset($language->prefix) && is_string($language->prefix) && !empty($language->prefix)) {
      $printmail = $base_url . '/' . $language->prefix . '/printmail/' . $node->nid;
    }
    else {
      $printmail = $base_url . '/printmail/' . $node->nid;
    }
  }

  return _ndla_utils_disp_ajax_or_not('share', theme('ndla_utils_disp_share', $links, $printmail));
}

/**
 * Provides content for the embed block
 *
 * The content will be provided directly or via ajax depending on the
 * block setting.
 *
 * @param $node
 *  The node beeing displayed
 * @return
 *  Themed block content or a script for loading the block content using ajax
 */
function _ndla_utils_disp_embed_code_content($node = NULL) {
  $load_using_ajax = _ndla_utils_disp_is_ajax_block('embed', $node);
  if ($load_using_ajax) {
    return $load_using_ajax;
  }
  
  if (in_array($node->type, array('image', 'audio', 'flashnode', 'video', 'amendor_ios', 'amendor_ios_task', 'amendor_electure'))) {
    $html = theme('ndla_utils_disp_media_embed', $node->type, ndla_utils_get_embed_vars($node), TRUE);
  }
  else {
    $html = theme('ndla_utils_disp_embed', $node);
  }
  
  return _ndla_utils_disp_ajax_or_not('embed', isset($html) ? '<div class="embed-code">' . $html . '</div>' : '');
}

/**
 * Block content for ajax enabled blocks should be returned via this function.
 *
 * If the block has ajax enabled the block content will be printed as json.
 *
 * @param string|int $delta
 * @param string $to_return
 * @return NULL|string
 *  NULL if json is beeing sent
 *  Html(block content) otherwise
 */
function _ndla_utils_disp_ajax_or_not($delta, $to_return) {
  if (variable_get('ajax_load_block_' . $delta, 1)) {
    drupal_json(array(
      'success' => TRUE,
      'data' => $to_return,
    ));
    return NULL;
  }
  else {
    return $to_return;
  }
}

/**
 * Used to determine if a block should load its content using ajax
 *
 * Also populates the node object if needed.
 *
 * @param string|int $delta
 * @param class $node
 * @return FALSE|string
 *  FALSE if the block is to return content
 *  html ajax loader otherwise
 */
function _ndla_utils_disp_is_ajax_block($delta, &$node) {
  if (!isset($node)) {
    if (variable_get('ajax_load_block_' . $delta, 1)) {
      return theme('ndla_utils_disp_loader_animation');
    }
    $node = menu_get_object();
  }
  return FALSE;
}

/**
 * Used to make sure all parent items are published.
 * 
 * @param int $id Parent menu item identifier.
 * @return object Parent menu item.
 */
function ndla_utils_disp_part_of_parent_item($id) {
  global $language;
  $parent_item = (object) array(
    'id' => '',
    'parent_id' => $id,
    'published' => TRUE,
    'title' => '',
  );
  while ($parent_item->parent_id && $parent_item->published) {
    $parent_item = db_fetch_object(db_query("SELECT nmi.parent_id, nmi.id, nmt.published, nmt.title FROM {ndla_menu_translations} nmt JOIN {ndla_menu_items} nmi ON nmi.id = nmt.menu_item_id WHERE nmi.id = %d AND nmt.language IN('', '%s', 'und') AND nmt.published = 1", $parent_item->parent_id, $language->language));
    if($parent_item->parent_id == 0) {
      return $parent_item;
    }
  }
  return false;
}

/**
 * Displays content this content is a part of.
 * 
 * @global object $language Current language.
 * @return array The block.
 */
function ndla_utils_disp_part_of_block() {
  global $language;
  
  $current_course = ndla_utils_disp_get_course();
  $node = menu_get_object();
  
  /**
   * No node found. Might
   * be on node/add
   */
  if(empty($node)) {
    return;
  }

  $item_query = "SELECT nmi.menu_id, nmi.id FROM {ndla_menu_translations} nmt
                 LEFT JOIN {ndla_menu_items} nmi ON (nmt.menu_item_id = nmi.id)
                 LEFT JOIN {ndla_menu_menus} nmm ON (nmi.menu_id = nmm.id)
                 LEFT JOIN {node} n ON (nmm.vid = n.vid)
                 WHERE nmt.path = 'node/%d'
                 AND n.vid IS NOT NULL
                 AND nmt.published = 1
                 AND nmt.language IN ('', '%s')";
  $item_result = db_query($item_query, $node->nid, $language->language);
  while($item_data = db_fetch_object($item_result)) {
    if($parent_item = ndla_utils_disp_part_of_parent_item($item_data->id)) {
      $fag_query = "SELECT nmc.type, n.title, n.nid FROM {ndla_menu_menus} nmm
                    LEFT JOIN {ndla_menu_courses} nmc ON (nmm.nid = nmc.nid)
                    LEFT JOIN {node} n ON (nmc.gid = n.nid)
                    WHERE nmm.id = %d";
      $fag_result = db_query($fag_query, $item_data->menu_id);
      while($fag_data = db_fetch_object($fag_result)) {
        $group_name = i18ntaxonomy_translate_term_name(taxonomy_get_term(ndla_fag_taxonomy_get_term_from_og($fag_data->nid)));
        if($fag_data->type == NDLA_MENU_LEFT) {
          $content_type = ndla_utils_get_node_content_type_string($node->nid);
          if(empty($content_type)) {
            $content_type = t('Menu');
          }
          
          //$name = i18ntaxonomy_translate_term_name($tid);
          $parents[] = (object) array(
            'title' => $parent_item->title,
            'url' => url('node/' . $node->nid, array(
              'query' => ndla_utils_disp_get_context_array(array('course' => $fag_data->nid, 'menu' => '')),
              'ndla_utils_query_set' => TRUE
              )
            ),
            'type' => t('@type for <a href="!course_url">@course_name</a>', array('@type' => $content_type, '!course_url' => url('node/' . $fag_data->nid), '@course_name' => $group_name)),
            'course_name' => $group_name,
            'course_url' => url('node/' . $fag_data->nid),
            'type_name' => t($content_type),
            'current' => $fag_data->nid == $current_course->nid && !ndla_utils_disp_get_context_value('menu'),
          );
        } else {
          $menu_id = ndla_utils_disp_get_context_value('menu');
          $menu_query = "SELECT menu_id FROM {ndla_menu_items} WHERE id = %d";
          $menu_data = db_fetch_object(db_query($menu_query, $menu_id));
          $parents[] = (object) array(
            'title' => $parent_item->title,
            'url' => url('node/' . $node->nid, array(
              'query' => ndla_utils_disp_get_context_array(array('course' => $fag_data->nid, 'menu' => $parent_item->id)),
              'ndla_utils_query_set' => TRUE
            )),
            'type' => t('@type for <a href="!course_url">@course_name</a>', array('@type' => t('Topic'), '!course_url' => url('node/' . $fag_data->nid), '@course_name' => $fag_data->title)),
            'course_name' => $group_name,
            'course_url' => url('node/' . $fag_data->nid),
            'type_name' => t('Topic'),
            'current' => $fag_data->nid == $current_course->nid && $item_data->menu_id == $menu_data->menu_id,
          );
          $courses[$fag_data->nid] = 1;
        }
      }
    }
  }

  $result = db_query("SELECT nftm.nid AS id, td.name FROM {term_data} td JOIN {term_node} tn ON tn.tid = td.tid JOIN {ndla_fag_taxonomy_map} nftm ON nftm.tid = td.tid WHERE td.vid = %d AND tn.vid = %d", variable_get('ndla_fag_taxonomy_vocabulary' , 0), $node->vid);
  while ($course = db_fetch_object($result)) {
    if (!isset($courses[$course->id])) {
      $parents[] = (object) array(
        'title' => i18ntaxonomy_translate_term_name(taxonomy_get_term(ndla_fag_taxonomy_get_term_from_og($course->id))),
        'url' => url('node/' . $course->id),
        'type' => t('Course'),
        'current' => $course->id == $current_course->nid,
      );
    }
  }

  return isset($parents) ? array(
    'subject' => t('Part of'),
    'content' => theme('ndla_utils_disp_part_of', $parents),
  ) : NULL;
}
