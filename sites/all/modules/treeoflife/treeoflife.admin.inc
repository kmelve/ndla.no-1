<?php

/**
 * administration form
 */
function treeoflife_admin_form() {
  $form = array();
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  // XML files
  $form['treeoflife_xml'] = array(
    '#type' => 'fieldset',
    '#title' => t('Upload new texts'),
    '#description' => t('Upload one or both files to update the information in the HTML5-version of Tree of Life'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['treeoflife_xml']['hotspots'] = array(
    '#type' => 'file',
    '#size' => 20,
    '#title' => t('Hotspots'),
    '#description' => t('Upload an XML-file containing updated information about hotspots.'),
  );
  $form['treeoflife_xml']['keys'] = array(
    '#type' => 'file',
    '#size' => 20,
    '#title' => t('Nøkler'),
    '#description' => t('Upload an XML-file containing keys.'),
   );
  $form['treeoflife_xml']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
    '#submit' => array('treeoflife_admin_xml_submit'), 
  );
  // zip file
  $form['treeoflife_zip'] = array(
    '#type' => 'fieldset',
    '#title' => t('Upload flash content'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['treeoflife_zip']['zip'] = array(
    '#type' => 'file',
    '#size' => 20,
    '#title' => t('Zip-file'),
    '#description' => t('Upload a zip-file containing the flash content.'),
   );
  $form['treeoflife_zip']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload zip-file'),
    '#submit' => array('treeoflife_admin_zip_submit'), 
  );
  return $form;
}

/**
 * Handles form submissions containing xml files from admin page
 */
function treeoflife_admin_xml_submit($form, &$form_state) {
  treeoflife_files_dir_check();
  // validate files as .xml
  $validators = array( 'file_validate_extensions' => array( 'xml' ));
  $settings = treeoflife_settings();
  $keys_path = file_create_path(treeoflife_settings()->xml_dir);
  $hotspots_path = file_create_path(treeoflife_settings()->hotspots_dir);
  file_check_directory($keys_path, FILE_CREATE_DIRECTORY);
  file_check_directory($hotspots_path, FILE_CREATE_DIRECTORY);
  // try saving both files
  $result1 = file_save_upload('hotspots', $validators, $hotspots_path, FILE_EXISTS_REPLACE);
  $result2 = file_save_upload('keys', $validators, $keys_path, FILE_EXISTS_REPLACE);
  // give feedback and clear cache
  if ($result1) {
    file_set_status($result1, FILE_STATUS_PERMANENT);
    drupal_set_message(t('Hotspots saved'));
  }
  if ($result2) {
    file_set_status($result2, FILE_STATUS_PERMANENT);
    drupal_set_message(t('Keys saved'));
  }
  if ($result1 || $result2) {
    treeoflife_cache_clear_all();
  }
}
/**
 * handles submissions of zip files
 */
function treeoflife_admin_zip_submit($form, &$form_state) {
  treeoflife_files_dir_check();
  // the allowed dirs at root
  $allowed_root_dirs = array('download', 'Hotspots',  'i', 'Images', 's', 'treeOfLifePoster', 'xml');
  $allowed_root_files = array('index.swf');
  // validate file as .zip
  $settings = treeoflife_settings();
  $path = file_create_path($settings->tree_dir);
  $zip = new ZipArchive;
  if ($zip->open($_FILES['files']['tmp_name']['zip']) == TRUE) {
    // find root files and dirs
    $root_files = array();
    $root_dirs = array();
    $files = array();
    $i = 0;
    
    // get root dirs
    while ($zip->statIndex($i) != FALSE) {
      $item = $zip->statIndex($i);;
      if (substr_count($item['name'], '/') == 1 && substr($item['name'], -1) == '/') {
        // store root dirs in root_dirs
        $root_dirs[] = substr($item['name'], 0, -1);
      }
      if (strpos($item['name'], '/') == FALSE) {
        // store root files in root_files
        $root_files[] = $item['name'];
      }
      $i++;
    }
    // find all files to extract by filtering by root dir
    for ($i = 0; $i < $zip->numFiles; $i++) {
      $name = $zip->getNameIndex($i);
      $parent_root = substr($name, 0, strpos($name, '/'));
      if (in_array($parent_root, $allowed_root_dirs)) {
        $files[] = $name;
      }
    }
    // filter root_files
    foreach ($root_files as $root_file) {
      if (in_array($root_file, $allowed_root_files)) {
        $files[] = $root_file;
      }
    }
    
    // clean up before extracting to avoid leftover files in the dirs
    foreach ($root_dirs as $key => $dir) {
      $success = treeoflife_remove_dir($dir, $allowed_root_dirs, TRUE);
      if ($success == FALSE) {
        drupal_set_message(t('Directory %dir could not be updated', array('%dir' => $dir)));
      }
    }
    // extract files that has passed the filtering
    $zip->extractTo($settings->tree_dir, $files);
    $zip->close();
  }
}

/**
 * Remove the directory and all its children
 * optional: second parameter as an array containing allowed names for third parameter
 * optional: set third parameter to TRUE to not delete the directory, but only its children. Also activates filtering by 2nd parameter
 */
function treeoflife_remove_dir($dir, $allowed_names = array(), $root = FALSE) {
  // check if we have to filter by name
  if ($root && sizeof($allowed_names) > 0) {
    $allowed = in_array($dir, $allowed_names);
  } else {
    $allowed = TRUE;
  }

  if ($root && !$allowed) {
    return FALSE;
  }
  // clean dirs
  $dir_path = treeoflife_settings()->tree_dir . $dir;
  if (is_dir($dir_path) && $allowed) { 
    $objects = scandir($dir_path); 
    foreach ($objects as $object) { 
      if ($object != "." && $object != "..") { 
        if (filetype($dir_path."/".$object) == "dir") treeoflife_remove_dir($dir_path."/".$object, NULL,  FALSE); else unlink($dir_path."/".$object); 
      } 
    } 
    reset($objects); 
    // we want to keep root, but the rest we can remove
    if (!$root) {
      rmdir($dir_path);
    }
  }
  return TRUE;
}

/**
 * validates file uploads
 */
function treeoflife_admin_validate($form, $form_state) {
  $keys = array('hotspots', 'keys', 'zip');
  $valid = false;
  foreach ($keys as $key) {
    // for each key check if we have a file uploaded
    if (isset($_FILES['files']) && is_uploaded_file($_FILES['files']['tmp_name'][$key])) {
      // if one file is uploaded, then the form is valid
      $valid = true;
    }
  }
  if (!$valid) {
    foreach ($keys as $key) {
      form_set_error($key, t('You have to upload a file'));
    }
  }
}

/**
 * Check if dir exists and add an .htaccess-file to override drupals
 */
function treeoflife_files_dir_check() {
    $directory = treeoflife_settings()->files_dir;
    // make sure dir is created
    file_create_path($directory);
    file_check_directory($directory, FILE_CREATE_DIRECTORY);
    
    // add .htaccess dir if needed to override drupals overriding (which disables php)
    if (!is_file("$directory/.htaccess")) {
      $htaccess_lines = "SetHandler None";
      if (($fp = fopen("$directory/.htaccess", 'w')) && fputs($fp, $htaccess_lines)) {
        fclose($fp);
        chmod($directory .'/.htaccess', 0664);
      }
    }
}

/**
 * Returns the admin page including the admin form.
 */
function treeoflife_admin_page() {
  drupal_add_css(treeoflife_settings()->module_dir . '/stylesheets/treeoflife.css');
  $content = treeoflife_admin_check_files();
  $content .= drupal_get_form('treeoflife_admin_form');
  return $content;
}

/**
 * checks if the required files for TreeOfLife to work is present.
 */
function treeoflife_admin_check_files() {
  $settings = treeoflife_settings();
  $validate = array(
    "Hotspots" => array(
      'file' =>  $settings->hotspots,
      'error_message' => t('Hotspots.xml not found or not valid XML'),
      'success_message' => l('Hotspots.xml', $settings->hotspots) . ' OK',
      ),
    "Keys" => array(
      'file' => $settings->keys,
      'error_message' => t('Key-file not found or not valid XML'),
      'success_message' => l('Keys.xml', $settings->keys) . ' OK',
    ), 
    "Download" => array(
      'file' => $settings->tree_dir . '/download/download.php',
      'error_message' => t('Download script download.php not found'),
      'success_message' => t('Download script OK'),
    ),
    "Tree" => array(
      'file' => $settings->tree_dir . '/index.swf',
      'error_message' => t('Flash file for Tree of Life missing'),
      'success_message' => t('Flash file OK'),
    ),
    "OpenLayers" => array(
      'file' => $settings->openlayers,
      'error_message' => t('OpenLayers not found. Contact admin'),
      'success_message' => t('OpenLayers OK'),
    ),
  );
  foreach ($validate as $key => $value) {
    $validate[$key]['valid'] = NULL;
    $validate[$key]['message'] = "&nbsp;";
    foreach ($value as $k => $file) {
      switch ($k) {
        case "file":
          $result = treeoflife_admin_validate_file($file);
          if ($result) {
            $validate[$key]['valid'] = TRUE;
            $validate[$key]['message'] = $validate[$key]['success_message'];
          } else {
            $validate[$key]['valid'] = FALSE;
            $validate[$key]['message'] = $validate[$key]['error_message'];
          }
          break;
        default:
          break;
      }
    }
    switch ($validate[$key]['valid']) {
      case TRUE:
        $validate[$key]['class'] = "treeoflife-valid";
        break;
      case FALSE:
        $validate[$key]['class'] = "treeoflife-invalid";
        break;
      default:
        $validate[$key]['class'] = "treeoflife-unknown";
        break;
    }
  }
  return theme('treeoflife_admin_check_files', $validate);
}

/**
 * check if file exists, if it's html, try to validate it (simple)
 */
function treeoflife_admin_validate_file($file) {
  if (file_exists($file)) {
    $fileext = substr($file, -4);
    switch ($fileext) {
      case '.xml' :
        $result = treeoflife_admin_is_valid_xml($file);
        break;
      default :
        $result = true;
        break;
    }
    return $result;
  }
  return $false;
}

/*
 * try to load the file as xml, return true if success, false if fail
 */
function treeoflife_admin_is_valid_xml($file) {
  $xml = simplexml_load_file($file);
  if (!$xml) {
    return false;
  }
  return true;
}