<?php

/**
 * Tree of life settings
 */
function treeoflife_settings() {
  static $settings;
  
  if (is_null($settings)) {
    $settings = new stdClass;
    // directory paths under default/files
    $settings->module_dir = drupal_get_path('module', 'treeoflife');

    // default/files/treeoflife
    $settings->files_dir = file_directory_path() . '/treeoflife';

    // default/files/treeoflife/tree
    $settings->tree_dir = $settings->files_dir . '/tree';

    // default/files/treeoflife/tree/xml
    $settings->xml_dir = $settings->tree_dir . '/xml';

    // default/files/treeoflife/tree/xml/keys.xml
    $settings->keys = $settings->xml_dir . '/keys.xml';

    // default/files/treeoflife/tree/Hotspots
    $settings->hotspots_dir = $settings->tree_dir . '/Hotspots';

    // default/files/treeoflife/tree/Hotspots/hotspots.xml
    $settings->hotspots = $settings->hotspots_dir . '/hotspots.xml';

    // default/files/treeoflife/cache
    $settings->cache_dir = $settings->files_dir . '/cache';

    $settings->openlayers = 'sites/all/libraries/openlayers/OpenLayers.js';
  }
  return $settings;
}

/**
 * Implementation of hook_menu().
 */
function treeoflife_menu() {
  $items = array();
  $items['treeoflife/mobile'] = array(
    'title' => 'Tree of Life',
    'description' => 'Tree of Life ' . t('mobile'),
    'page callback' => 'treeoflife_mobile_page',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['treeoflife'] = array(
    'title' => 'Tree of Life',
    'description' => 'Tree of Life',
    'page callback' => 'treeoflife_flash_page',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['treeoflife/image_download/%'] = array(
    'title' => 'Tree of Life - ' . t('Image Download'),
    'description' => t('Download an image'),
    'page callback' => 'treeoflife_image_download',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
    'page arguments' => array(2),
  );
  $items['treeoflife/json/keys'] = array(
    'page callback' => 'treeoflife_json_keys',
    'access arguments' => array('access content')
  );
  $items['treeoflife/json/organism/%'] = array(
    'page callback' => 'treeoflife_json_organism',
    'access arguments' => array('access content'),
    'page arguments' => array(3),
  );
  $items['admin/settings/treeoflife'] = array(
    'title' => t('Tree of Life - Admin'),
    'description' => 'Tree of Life ' . t('administering'),
    'file' => 'treeoflife.admin.inc',
    'page callback' => 'treeoflife_admin_page',
    'page arguments' => array('treeoflife_admin'),
    'access arguments' => array('administer tree of life'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function treeoflife_perm() {
  return array('administer tree of life');
}

/**
 * show the tree of life page for mobiles
 */
function treeoflife_mobile_page() {
  $path = drupal_get_path('module', 'treeoflife');
  // add open layers 
  drupal_add_js(treeoflife_settings()->openlayers);

  // load required css 
  drupal_add_css($path . '/stylesheets/jquery.mobile-1.1.1.min.css');
  drupal_add_css($path . '/stylesheets/treeoflife.css');
  
  // add basepath to setting
  drupal_add_js(array('TreeOfLife' => array('basePath' => base_path())), 'setting');
  // add javascript file containing both jquery and jquery mobile to avoid any 
  // conflicts at ndla.no
  drupal_add_js($path . '/javascripts/jquery-1.7.1.mobile-1.1.1.min.js');
  // load autocomplete js for search box
  drupal_add_js($path . '/javascripts/jquery.autocomplete.min.js');
  // finally load the application js
  drupal_add_js($path . '/javascripts/treeoflife-1.0.js');
  // theme it and include hotspots inline
  return theme('treeoflife_mobile_page', treeoflife_json_hotspots());
}

/**
 * Implementation of hook_theme().
 */
function treeoflife_theme($existing, $type, $theme, $path) {
  return array(
    'treeoflife_mobile_page' => array(
      'arguments' => array(
        'hotspots' => NULL
      ),
      'path' => $path . '/theme',
      'template' => 'treeoflife_page',
    ),
    'treeoflife_image_download' => array(
      'arguments' => array(
        'path' => NULL,
        'filename' => NULL,
      ),
      'path' => $path . '/theme',
      'template' => 'treeoflife_image_download',
    ),
    'treeoflife_admin_check_files' => array(
      'arguments' => array(
        'validate' => NULL,
      ),
      'path' => $path . '/theme',
      'template' => 'treeoflife_admin_check_files',
    ),
  );
}

/**
 * tests whether the user is a mobile user
 * returns true if mobile
 */
function treeoflife_is_mobile() {
  // returns true if mobile browser http://detectmobilebrowsers.com/
  $useragent=$_SERVER['HTTP_USER_AGENT'];
  return (preg_match('/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)));
}

/**
 * show download image page
 * responds to ajax requests from the organism-page
 */
function treeoflife_image_download($filename) {
  $path = base_path() . drupal_get_path('module', 'treeoflife') . '/';
  drupal_add_css(drupal_get_path('module', 'treeoflife') . '/stylesheets/treeoflife.css');
  return theme('treeoflife_image_download', $path, $filename);
}

/**
 * Implementation of hook_theme_registry_alter().
 */
function treeoflife_theme_registry_alter(&$registry) {
  // add treeoflife to theme path
  $registry['page']['theme paths'][] = drupal_get_path('module', 'treeoflife') . '/theme';
  if (isset($registry['page'])) {
    // See if our preprocess function is loaded, if so remove it.
    if ($key = array_search('treeoflife_preprocess_page', 
      $registry['page']['preprocess functions'])) {
      unset($registry['page']['preprocess functions'][$key]);
    }
    // Now add it on at the end of the array so that it runs late
    $registry['page']['preprocess functions'][] = 'treeoflife_preprocess_page';
  } 
}

function treeoflife_preprocess_page(&$vars) {
  // if we're on the mobile page
  $uri = request_uri();
  if (is_int(strpos($uri, 'mobile'))) {
    // remove all css that dont belong to treeoflife
    foreach ($vars['css']['all']['module'] as $key => $val) {
      if (strpos($key, 'treeoflife') === FALSE) {
        unset($vars['css']['all']['module'][$key]);
      }
    }
    $vars['css']['all']['theme'] = array();
    $vars['styles'] = drupal_get_css($vars['css']);
    $js = drupal_add_js();
    unset($js['theme']);
    $vars['scripts'] = drupal_get_js('header', $js);
  }
}

/**
 * returns json output of hotpots data
 */
function treeoflife_json_hotspots() {
  if (($cache = treeoflife_cache_get('treeoflife_hotspots')) && !empty($cache->data)) {
    $json = $cache->data;
  }
  else {
    $settings = treeoflife_settings();
    $xml = simplexml_load_file($settings->hotspots);
    $hotspots = array();
    foreach ($xml->HOTSPOT as $val) {
      $hotspots[(int)$val->attributes()->{'ID'}] = array(
        'id'  => (int)$val->attributes()->{'ID'},
        'x'   => (int)$val->attributes()->{'X'},
        'y'   => (int)$val->attributes()->{'Y'},
        'w'   => (int)$val->attributes()->{'W'},
        'h'   => (int)$val->attributes()->{'H'},
        'name'  => (string)$val->attributes()->{'ORGANISM_NAME'},
        'latin' => (string)$val->attributes()->{'ORGANISM_LATINNAME'},
      );
    }
    $json = json_encode($hotspots);
    treeoflife_cache_set('treeoflife_hotspots', $json, 'cache', CACHE_PERMANENT, NULL);
  }
  //drupal_set_header('Content-Type: application/json');
  return $json;
}

/**
 * Searches for an organism and returns its json data
 */
function treeoflife_json_organism($identifier = NULL) {
  $id = intval($identifier);
  if (!$id) {
    return;
  }
  if (($cache = treeoflife_cache_get('treeoflife_organism_' . $id)) && !empty($cache->data)) {
    $organism = $cache->data;
  }
  else {
    $settings = treeoflife_settings();
    $xml = simplexml_load_file($settings->hotspots);
    $organism = array();
    foreach ($xml->HOTSPOT as $val) {
      if ((int)$val->attributes()->{'ID'} == $id) {
        $organism = json_encode(array(
          'id'      => (int)$val->attributes()->{'ID'},
          'name'    => (string)$val->attributes()->{'ORGANISM_NAME'},
          'latin'   => (string)$val->attributes()->{'ORGANISM_LATINNAME'},
          'image'   => substr((string)$val->attributes()->{'ORGANISM_IMAGE'}, 2),
          'desc'    => (string)$val->attributes()->{'ORGANISM_DESC'},
          'furtherreading' => (string)$val->attributes()->{'FURTHERREADING'},
          'url'     => (string)$val->attributes()->{'URL'},
          'colour'  => (string)$val->attributes()->{'SPECIES_COLOUR'},
          'group'   => (string)$val->attributes()->{'ORGANISM_GROUP'},
          'contenttype'   => (string)$val->attributes()->{'CONTENT_TYPE'},
          'extinct' => (string)$val->attributes()->{'EXTINCT'}
        ));
        treeoflife_cache_set('treeoflife_organism_' . $id, $organism);
        break; // out of foreach
      }
    }
  }
  drupal_set_header('Content-Type: application/json');
  print $organism;
  exit;
}

/**
 * retrieves the keys data and returns it as json
 */
function treeoflife_json_keys() {
  $settings = treeoflife_settings();
  $xml = simplexml_load_file($settings->keys);
  drupal_set_header('Content-Type: application/json');
  print json_encode($xml);
  exit;
}

/**
 * gets a cached json file from $key if exists
 */
function treeoflife_cache_get($key) {
  $settings = treeoflife_settings();
  $cache = (object)'cache';
  $cache->data = @file_get_contents($settings->cache_dir . '/' . $key . '.json');
  return $cache;
}

/**
 * Caches $data containing JSON using $key
 */
function treeoflife_cache_set($key, $data) {
  $settings = treeoflife_settings();
  $path = file_create_path($settings->cache_dir);
  file_check_directory($path, FILE_CREATE_DIRECTORY);
  file_save_data($data, $path . '/' . $key . '.json', FILE_EXISTS_REPLACE);
}

/**
 * Removes all cached json files
 */
function treeoflife_cache_clear_all() {
  $settings = treeoflife_settings();
  $files = file_scan_directory($settings->cache_dir, '.json');
  foreach ($files as $file) {
    file_delete($file->filename);
  }
}

/**
 * creates the flash embed code and returns a page containing it
 */
function treeoflife_flash_page() {
  global $base_url;
  
  drupal_add_js(treeoflife_settings()->module_dir . '/javascripts/swfobject.js');
  drupal_add_js('$(document).ready(function(){' ."\n" .
    'function treeoflifeSwfCallback(e) {' .
      'if (e.success === false) { window.location = "' . url('treeoflife/mobile', array('absolute' => TRUE)) . '"; }' .
    '};' . "\n" .
    'swfobject.embedSWF("' .
      $base_url . '/' .  treeoflife_settings()->tree_dir . '/index.swf' . '",' . // swfUrl
      '"treeoflife_flash",'.    // id
      '"600",'.                 // width
      '"418",'.                 // height
      '"8",'.                   // flash version
      '"",'.                    // expressInstallSwfurl
      '{"width":"600","height":"418"},' .   // flashvars (object)
      '{"swliveconnect":"default","play":"true","loop":"false","menu":"false","quality":"autohigh","scale":"showall","align":"l","salign":"tl","wmode":"opaque","bgcolor":"#FFFFFF","version":"7","allowfullscreen":"true","allowscriptaccess":"sameDomain","width":600,"height":418,"base":"'. str_replace('/', '\/', $base_url . '/' .  treeoflife_settings()->tree_dir .'/"') . ',"src":"' . $base_url . '/' .  treeoflife_settings()->tree_dir . '/index.swf"},'.  // params (object)
      '{},'.                    // attributes (object)
      'treeoflifeSwfCallback' .
      ');});', 'inline');
  $content = (treeoflife_is_mobile()) ? '<p>' . l(t('Click here to open mobile version'), 'treeoflife/mobile') . '</p>' : "";
  $content .= '<div id="treeoflife_flash"></div><p>' . l(t('Open in full screen'), $base_url . '/' . treeoflife_settings()->tree_dir . '/index.swf') . '</p>';
  return $content;
}
