<div class="treeoflife-logo"></div>
<p><?php print t("If you want to download a high resolution image, you have to right-click the image below and choose \"Save image as\" from the menu that pops up. Then choose where you want to save the file and click \"save\".") ?></p>
<a href="<?php print $path ."images/highres/" .  $filename ?>"><img class="treeoflife-download" src="<?php print $path . "images/highres/" . $filename ?>">
</a>
<p class="treeoflife-center"><?php print t('Image is available as') ?> <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/uk/" target="_blank">Creative Commons BY-NC-SA <?php print t('license') ?></a>.</p>
<a class="treeoflife-center" href="http://www.open.ac.uk/"><img src="<?php print $path ?>images/logo.png"></a>