<div class="treeoflife-box">
  <h3><?php print t('System status') ?></h3>
  <ul>
  <?php foreach ($validate as $item => $data) : ?>
    <li class="<?php print $data['class'] ?>">
      <?php print $data['message'] ?>
    </li>
  <?php endforeach; ?>
  </ul>
  <p> <?php print t('If there are errors in the application, it may be due to files that are missing or contain errors.') ?></p>
</div>