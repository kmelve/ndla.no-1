/**
 * Tree of life v1.0
 * Copyright 2011 Amendor
 * http://www.amendor.no
 * 
 * by Børge Antonsen
 */

// TinyColor.js - <https://github.com/bgrins/TinyColor> - 2011 Brian Grinstead - v0.5
(function(s){function d(a,b){if(typeof a=="object"&&a.hasOwnProperty("_tc_id"))return a;if(typeof a=="object"&&(!b||!b.skipRatio))for(var c in a)a[c]===1&&(a[c]="1.0");c=v(a);var j=c.r,d=c.g,f=c.b,g=n(c.a);j<1&&(j=e(j));d<1&&(d=e(d));f<1&&(f=e(f));return{ok:c.ok,_tc_id:w++,alpha:g,toHsv:function(){var a=t(j,d,f);return{h:a.h,s:a.s,v:a.v,a:g}},toHsvString:function(){var a=t(j,d,f),b=e(a.h*360),c=e(a.s*100);a=e(a.v*100);return g==1?"hsv("+b+", "+c+"%, "+a+"%)":"hsva("+b+", "+c+"%, "+a+"%, "+g+")"},
toHsl:function(){var a=u(j,d,f);return{h:a.h,s:a.s,l:a.l,a:g}},toHslString:function(){var a=u(j,d,f),b=e(a.h*360),c=e(a.s*100);a=e(a.l*100);return g==1?"hsl("+b+", "+c+"%, "+a+"%)":"hsla("+b+", "+c+"%, "+a+"%, "+g+")"},toHex:function(){return q(j,d,f)},toHexString:function(){return"#"+q(j,d,f)},toRgb:function(){return{r:e(j),g:e(d),b:e(f),a:g}},toRgbString:function(){return g==1?"rgb("+e(j)+", "+e(d)+", "+e(f)+")":"rgba("+e(j)+", "+e(d)+", "+e(f)+", "+g+")"},toName:function(){return x[q(j,d,f)]||
!1},toFilter:function(){var a=q(j,d,f),b=Math.round(n(g)*255).toString(16);return"progid:DXImageTransform.Microsoft.gradient(startColorstr=#"+b+a+",endColorstr=#"+b+a+")"}}}function v(a){var b={r:255,g:255,b:255},c=1,d=!1;typeof a=="string"&&(a=y(a));if(typeof a=="object"){if(a.hasOwnProperty("r")&&a.hasOwnProperty("g")&&a.hasOwnProperty("b"))b={r:i(a.r,255)*255,g:i(a.g,255)*255,b:i(a.b,255)*255},d=!0;else if(a.hasOwnProperty("h")&&a.hasOwnProperty("s")&&a.hasOwnProperty("v")){var h=a.h,f=a.s;b=a.v;
var g,e,o;h=i(h,360);f=i(f,100);b=i(b,100);d=p.floor(h*6);var m=h*6-d;h=b*(1-f);var n=b*(1-m*f);f=b*(1-(1-m)*f);switch(d%6){case 0:g=b;e=f;o=h;break;case 1:g=n;e=b;o=h;break;case 2:g=h;e=b;o=f;break;case 3:g=h;e=n;o=b;break;case 4:g=f;e=h;o=b;break;case 5:g=b,e=h,o=n}b={r:g*255,g:e*255,b:o*255};d=!0}else a.hasOwnProperty("h")&&a.hasOwnProperty("s")&&a.hasOwnProperty("l")&&(b=z(a.h,a.s,a.l),d=!0);if(a.hasOwnProperty("a"))c=a.a}return{ok:d,r:k(255,l(b.r,0)),g:k(255,l(b.g,0)),b:k(255,l(b.b,0)),a:c}}
function u(a,b,c){a=i(a,255);b=i(b,255);c=i(c,255);var d=l(a,b,c),h=k(a,b,c),f,g=(d+h)/2;if(d==h)f=h=0;else{var e=d-h;h=g>0.5?e/(2-d-h):e/(d+h);switch(d){case a:f=(b-c)/e+(b<c?6:0);break;case b:f=(c-a)/e+2;break;case c:f=(a-b)/e+4}f/=6}return{h:f,s:h,l:g}}function z(a,b,c){function d(a,b,c){c<0&&(c+=1);c>1&&(c-=1);if(c<1/6)return a+(b-a)*6*c;if(c<0.5)return b;if(c<2/3)return a+(b-a)*(2/3-c)*6;return a}a=i(a,360);b=i(b,100);c=i(c,100);if(b==0)c=b=a=c;else{var e=c<0.5?c*(1+b):c+b-c*b,f=2*c-e;c=d(f,
e,a+1/3);b=d(f,e,a);a=d(f,e,a-1/3)}return{r:c*255,g:b*255,b:a*255}}function t(a,b,c){a=i(a,255);b=i(b,255);c=i(c,255);var d=l(a,b,c),e=k(a,b,c),f,g=d-e;if(d==e)f=0;else{switch(d){case a:f=(b-c)/g+(b<c?6:0);break;case b:f=(c-a)/g+2;break;case c:f=(a-b)/g+4}f/=6}return{h:f,s:d==0?0:g/d,v:d}}function q(a,b,c){function d(a){return a.length==1?"0"+a:""+a}a=[d(e(a).toString(16)),d(e(b).toString(16)),d(e(c).toString(16))];if(a[0][0]==a[0][1]&&a[1][0]==a[1][1]&&a[2][0]==a[2][1])return a[0][0]+a[1][0]+a[2][0];
return a.join("")}function i(a,b){typeof a=="string"&&a.indexOf(".")!=-1&&n(a)===1&&(a="100%");var c=typeof a==="string"&&a.indexOf("%")!=-1;a=k(b,l(0,n(a)));c&&(a*=b/100);if(p.abs(a-b)<1.0E-6)return 1;else if(a>=1)return a%b/n(b);return a}function y(a){a=a.replace(A,"").replace(B,"").toLowerCase();r[a]&&(a=r[a]);if(a=="transparent")return{r:0,g:0,b:0,a:0};var b;if(b=m.rgb.exec(a))return{r:b[1],g:b[2],b:b[3]};if(b=m.rgba.exec(a))return{r:b[1],g:b[2],b:b[3],a:b[4]};if(b=m.hsl.exec(a))return{h:b[1],
s:b[2],l:b[3]};if(b=m.hsla.exec(a))return{h:b[1],s:b[2],l:b[3],a:b[4]};if(b=m.hsv.exec(a))return{h:b[1],s:b[2],v:b[3]};if(b=m.hex6.exec(a))return{r:parseInt(b[1],16),g:parseInt(b[2],16),b:parseInt(b[3],16)};if(b=m.hex3.exec(a))return{r:parseInt(b[1]+""+b[1],16),g:parseInt(b[2]+""+b[2],16),b:parseInt(b[3]+""+b[3],16)};return!1}var A=/^[\s,#]+/,B=/\s+$/,w=0,p=Math,e=p.round,k=p.min,l=p.max,n=s.parseFloat;d.equals=function(a,b){return d(a).toHex()==d(b).toHex()};d.desaturate=function(a,b){var c=d(a).toHsl();
c.s-=(b||10)/100;c.s=k(1,l(0,c.s));return d(c)};d.saturate=function(a,b){var c=d(a).toHsl();c.s+=(b||10)/100;c.s=k(1,l(0,c.s));return d(c)};d.greyscale=function(a){return d.desaturate(a,100)};d.lighten=function(a,b){var c=d(a).toHsl();c.l+=(b||10)/100;c.l=k(1,l(0,c.l));return d(c)};d.darken=function(a,b){var c=d(a).toHsl();c.l-=(b||10)/100;c.l=k(1,l(0,c.l));return d(c)};d.complement=function(a){a=d(a).toHsl();a.h=(a.h+0.5)%1;return d(a)};d.triad=function(a){var b=d(a).toHsl(),c=b.h*360;return[d(a),
d({h:(c+120)%360,s:b.s,l:b.l}),d({h:(c+240)%360,s:b.s,l:b.l})]};d.tetrad=function(a){var b=d(a).toHsl(),c=b.h*360;return[d(a),d({h:(c+90)%360,s:b.s,l:b.l}),d({h:(c+180)%360,s:b.s,l:b.l}),d({h:(c+270)%360,s:b.s,l:b.l})]};d.splitcomplement=function(a){var b=d(a).toHsl(),c=b.h*360;return[d(a),d({h:(c+72)%360,s:b.s,l:b.l}),d({h:(c+216)%360,s:b.s,l:b.l})]};d.analogous=function(a,b,c){b=b||6;c=c||30;var e=d(a).toHsl();c=360/c;a=[d(a)];e.h*=360;for(e.h=(e.h-(c*b>>1)+720)%360;--b;)e.h=(e.h+c)%360,a.push(d(e));
return a};d.monochromatic=function(a,b){b=b||6;var c=d(a).toHsv(),e=c.h,h=c.s;c=c.v;for(var f=[];b--;)f.push(d({h:e,s:h,v:c})),c=(c+0.2)%1;return f};d.readable=function(a,b){var c=d(a).toRgb(),e=d(b).toRgb();return(e.r-c.r)*(e.r-c.r)+(e.g-c.g)*(e.g-c.g)+(e.b-c.b)*(e.b-c.b)>10404};var r=d.names={aliceblue:"f0f8ff",antiquewhite:"faebd7",aqua:"0ff",aquamarine:"7fffd4",azure:"f0ffff",beige:"f5f5dc",bisque:"ffe4c4",black:"000",blanchedalmond:"ffebcd",blue:"00f",blueviolet:"8a2be2",brown:"a52a2a",burlywood:"deb887",
burntsienna:"ea7e5d",cadetblue:"5f9ea0",chartreuse:"7fff00",chocolate:"d2691e",coral:"ff7f50",cornflowerblue:"6495ed",cornsilk:"fff8dc",crimson:"dc143c",cyan:"0ff",darkblue:"00008b",darkcyan:"008b8b",darkgoldenrod:"b8860b",darkgray:"a9a9a9",darkgreen:"006400",darkgrey:"a9a9a9",darkkhaki:"bdb76b",darkmagenta:"8b008b",darkolivegreen:"556b2f",darkorange:"ff8c00",darkorchid:"9932cc",darkred:"8b0000",darksalmon:"e9967a",darkseagreen:"8fbc8f",darkslateblue:"483d8b",darkslategray:"2f4f4f",darkslategrey:"2f4f4f",
darkturquoise:"00ced1",darkviolet:"9400d3",deeppink:"ff1493",deepskyblue:"00bfff",dimgray:"696969",dimgrey:"696969",dodgerblue:"1e90ff",firebrick:"b22222",floralwhite:"fffaf0",forestgreen:"228b22",fuchsia:"f0f",gainsboro:"dcdcdc",ghostwhite:"f8f8ff",gold:"ffd700",goldenrod:"daa520",gray:"808080",green:"008000",greenyellow:"adff2f",grey:"808080",honeydew:"f0fff0",hotpink:"ff69b4",indianred:"cd5c5c",indigo:"4b0082",ivory:"fffff0",khaki:"f0e68c",lavender:"e6e6fa",lavenderblush:"fff0f5",lawngreen:"7cfc00",
lemonchiffon:"fffacd",lightblue:"add8e6",lightcoral:"f08080",lightcyan:"e0ffff",lightgoldenrodyellow:"fafad2",lightgray:"d3d3d3",lightgreen:"90ee90",lightgrey:"d3d3d3",lightpink:"ffb6c1",lightsalmon:"ffa07a",lightseagreen:"20b2aa",lightskyblue:"87cefa",lightslategray:"789",lightslategrey:"789",lightsteelblue:"b0c4de",lightyellow:"ffffe0",lime:"0f0",limegreen:"32cd32",linen:"faf0e6",magenta:"f0f",maroon:"800000",mediumaquamarine:"66cdaa",mediumblue:"0000cd",mediumorchid:"ba55d3",mediumpurple:"9370db",
mediumseagreen:"3cb371",mediumslateblue:"7b68ee",mediumspringgreen:"00fa9a",mediumturquoise:"48d1cc",mediumvioletred:"c71585",midnightblue:"191970",mintcream:"f5fffa",mistyrose:"ffe4e1",moccasin:"ffe4b5",navajowhite:"ffdead",navy:"000080",oldlace:"fdf5e6",olive:"808000",olivedrab:"6b8e23",orange:"ffa500",orangered:"ff4500",orchid:"da70d6",palegoldenrod:"eee8aa",palegreen:"98fb98",paleturquoise:"afeeee",palevioletred:"db7093",papayawhip:"ffefd5",peachpuff:"ffdab9",peru:"cd853f",pink:"ffc0cb",plum:"dda0dd",
powderblue:"b0e0e6",purple:"800080",red:"f00",rosybrown:"bc8f8f",royalblue:"4169e1",saddlebrown:"8b4513",salmon:"fa8072",sandybrown:"f4a460",seagreen:"2e8b57",seashell:"fff5ee",sienna:"a0522d",silver:"c0c0c0",skyblue:"87ceeb",slateblue:"6a5acd",slategray:"708090",slategrey:"708090",snow:"fffafa",springgreen:"00ff7f",steelblue:"4682b4",tan:"d2b48c",teal:"008080",thistle:"d8bfd8",tomato:"ff6347",turquoise:"40e0d0",violet:"ee82ee",wheat:"f5deb3",white:"fff",whitesmoke:"f5f5f5",yellow:"ff0",yellowgreen:"9acd32"},
x=d.hexNames=function(a){var b={},c;for(c in a)a.hasOwnProperty(c)&&(b[a[c]]=c);return b}(r),m={rgb:RegExp("rgb[\\s|\\(]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))\\s*\\)?"),rgba:RegExp("rgba[\\s|\\(]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))\\s*\\)?"),
hsl:RegExp("hsl[\\s|\\(]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))\\s*\\)?"),hsla:RegExp("hsla[\\s|\\(]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))\\s*\\)?"),hsv:RegExp("hsv[\\s|\\(]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))[,|\\s]+((?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?))\\s*\\)?"),
hex3:/^([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,hex6:/^([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/};s.tinycolor=d})(this);


$(document).ready(function(){
  $.mobile.loadingMessage = Drupal.t("Loading information");
  
  // start with mappage if the user refreshes the page
   window.location.replace(window.location.href.split("#")[0] + "#mappage");
  //  window.location.replace(window.location.href.split("#")[0] + "#mappage");
  // get xml initally, and add a callback
  TreeOfLife.getData(TreeOfLife.json_url,function() {
    // when xml is ready, the callback starts the app
    TreeOfLife.mapInit();
    // init search field 
    TreeOfLife.searchFieldInit();
  });
  $('#mappage').css('padding-bottom', 0).bind('pageshow', function(e) {
    $(this).css('padding-bottom', 0);
    var i;
    // fix content height to screen height
    TreeOfLife.fixContentHeight();
    // make sure the layers are visible (android/ios)
    for (i = 0; i < TreeOfLife.map.layers.length; i += 1) {
      TreeOfLife.map.layers[i].redraw(); 
    }
  });
  // increase horizontal swipe detection to atleast 120px (from 30px)
  $.event.special.swipe.horizontalDistanceThreshold = 150;
  // and increase vertical to 100 (from 70px)
  $.event.special.swipe.verticalDistanceThreshold = 100;
  // when on infopage, enable swipe right to return to map
  $('#infopage').bind('swiperight', function(e) {
    $.mobile.changePage($('#mappage'), {reverse: true});
  }).bind('pageshow', TreeOfLife.fixContentHeight);
  // when on keypage enable swipe left to return to map
  $('#keypage').bind('swipeleft', function(e) {
    $.mobile.changePage($('#mappage'));
  }).bind('pageshow', function() {
    $(this).unbind('pageshow')
    TreeOfLife.loadKeys();
  });
  // when resizing the window, fix content height
  $(window).bind("resize", TreeOfLife.fixContentHeight);
  // also when changing orientation
  $(window).bind("orientationchange", TreeOfLife.fixContentHeight);
});


var TreeOfLife = {
  organisms       : [],     // all organisms in the map
  zoomify_width   : 7191,   // size of original image that has been zoomified
  zoomify_height  : 9976,
  zoomify_url     : "sites/default/files/treeoflife/tree/treeOfLifePoster/", // url to zoomify tiles with trailing /
  ratio           : 1,
  //zoomify_lowRes  : "sites/all/modules/treeoflife/tiles_40_percent/", // low res tiles
  lowResRatio     : 0.4,
  json_url        : "treeoflife/json/hotspots", // xml with organism info
  keys_url        : "treeoflife/json/keys",
  cc_license      : Drupal.t("Image is availble as") + " <em><a href='http://creativecommons.org/licenses/by-nc-sa/2.0/uk/' target='_blank'>Creative Commons</a></b>.",
  map             : {},     // making map and zoomify available throughout the singleton
  zoomify         : {},
  // low res not supported at the moment as they require more work by the editors who update the system (and require training)
  useLowRes       : false, //((screen.width >= 1024) && (screen.height >= 768)) ? false : true, // low res for mobile devices
  
  // starts the map
  mapInit: function(){
    var self = this,
        zoomify,
        maxExtent,
        click;

    /* First we initialize the zoomify pyramid (to get number of tiers) */
    if (self.useLowRes) {
      self.ratio = self.lowResRatio;
      zoomify = new OpenLayers.Layer.Zoomify( 
        "Zoomify", 
        Drupal.settings.basePath + self.zoomify_lowRes, 
        //new OpenLayers.Size(self.zoomify_width * self.ratio, self.zoomify_height * self.ratio)
        new OpenLayers.Size(self.zoomify_width * self.ratio, self.zoomify_height * self.ratio)
      );
        maxExtent = new OpenLayers.Bounds(0, 0, self.zoomify_width * self.ratio, self.zoomify_height * self.ratio);
    } else {
      zoomify = new OpenLayers.Layer.Zoomify( 
        "Zoomify", 
        Drupal.settings.basePath + self.zoomify_url, 
        new OpenLayers.Size( self.zoomify_width, self.zoomify_height )
      );
        maxExtent = new OpenLayers.Bounds(0, 0, self.zoomify_width * self.ratio, self.zoomify_height * self.ratio);
    }
    
    //maxExtent = new OpenLayers.Bounds(0, 0, 2876, 4799);
    /* Map with raster coordinates (pixels) from Zoomify image */
      options = {
        controls: [],
        maxExtent: maxExtent,
        maxResolution: Math.pow(2, zoomify.numberOfTiers-1),
        numZoomLevels: zoomify.numberOfTiers,
        units: 'pixels'
      };
    // put map in #treeoflife-map
    self.map = new OpenLayers.Map("treeoflife-map", options);
    self.map.addLayer(zoomify);
    self.map.addControl(new OpenLayers.Control.Attribution());
    // MouseDefaults enable mouse wheel scrolling on computers
    self.map.addControl(new OpenLayers.Control.MouseDefaults());
    // touch navigation for pads/phones
    self.map.addControl(new OpenLayers.Control.TouchNavigation({
      dragPanOptions: {
        enableKinetic: true
      }
    }));
    self.map.setBaseLayer(zoomify);

    // define a click controller and its options
    OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
      defaultHandlerOptions: {
        'single': true,
        'double': false,
        'pixelTolerance': 0,
        'stopSingle': false,
        'stopDouble': false
      },
      initialize: function(options) {
        this.handlerOptions = OpenLayers.Util.extend(
        {}, this.defaultHandlerOptions
          );
        OpenLayers.Control.prototype.initialize.apply(
          this, arguments
          ); 
        this.handler = new OpenLayers.Handler.Click(
          this, {
            'click': this.trigger
          }, this.handlerOptions
          );
      }, 
      trigger: self.getClickHandler()
    });
    // create a new click controller
    click = new OpenLayers.Control.Click();
    // add it and activate it
    self.map.addControl(click);
    click.activate();
    
    // fix content height when map is ready
    self.fixContentHeight();
    // set zoom
    self.setZoom();
    // initialize zoom buttons
    self.zoomButtonsInit();
    window.preloaderLoadingComplete = true;
  },
   
  /**
   * setCenter and ZoomToMaxExtent is run in a loop to make sure they get run even if
   * something else in the system chokes up and causes delays
   */
  setZoom : function () {
    var self = this,
    // find center of map
      lonlat = new OpenLayers.LonLat((self.zoomify_width / 2) * self.ratio, (self.zoomify_height/2) * self.ratio),
    // use an interval to center map
      setZoomInterval = setInterval(function() {
      // try to check if map is ready
      var success = true;
      try {
        self.map.setCenter(lonlat)
        self.map.zoomToMaxExtent();
      }
      catch (err) {
        // map wasnt ready, success == false!
        success = false;
      }
      if(success) {
        // if successful, clear zoom setter
        clearInterval(setZoomInterval);
      }
    }, 100);
  },
  // fixes content height
  fixContentHeight : function (e) {
    // fix for android reporting wrong height intially
    if (e && e.type === "orientationchange") {
      // delay fixing height by 100ms
      setTimeout("fixContentHeight()", 100);
      // and drop out of this loop
      return;
    }
    var self = this,
        footer        = $("div[data-role='footer']:visible"),
        content       = $("div[data-role='content']:visible"),
        viewHeight    = window.innerHeight ? window.innerHeight: $(window).height(),
        contentHeight = viewHeight - footer.outerHeight(),
        i,
        onMapPage     = (content.children('#treeoflife-map').length > 0) ? true : false;
    // if we're on map page, and content+footer isnt view height
    
    if ((content.outerHeight() + footer.outerHeight()) !== viewHeight && onMapPage) {
      // set contentHeight to screen size
      contentHeight -= (content.outerHeight() - content.height() + 1);
      content.height(contentHeight);
    } else if (!onMapPage) {
        // prevent height from being bugged
        content.height("auto");
    }
    if (self.map) {
      self.map.updateSize();
    }
  },
  // retrieves and parses an xml file (usually from self.xml_url), takes a callback function
  getData : function (file, func) {
    this.parseData(this.hotspots);
    func();
  },
  // converts hotspots.xml to js object
  parseData : function (data) {
    var self = this;
    for (var key in data) {
      self.organisms.push({
        id : parseInt(key),
        x  : data[key].x,
        y  : self.zoomify_height - data[key].y,
        w  : data[key].w,
        h  : data[key].h,
        name  : data[key].name,
        latin : data[key].latin
      });
    }
  },
  // retrieves a clickhandler, closures to keep scope
  getClickHandler : function () {
    var self = this;
    // returning a function which returns self.onMapClick ensures scope
    return function(e) {
      return self.onMapClick(e, self);
    }
  },
  // the clickhandler
  onMapClick : function (e, self) {
    // get clicked coords
    var lonlat = self.map.getLonLatFromViewPortPx(e.xy);
    // call clickLocator to find if an organisms was clicked
    var org = self.clickLocator(lonlat.lon, lonlat.lat);
    // use the result to update info
    self.updateInfo(org);
  },
  // updates the information with info about the organism
  updateInfo : function (org /* organism */) {
    var self = this,
        x, y, ratio;
    // if we found an organism on click, continue
    if (org && org.id && org.name) {
      $.mobile.showPageLoadingMsg();
      $.ajax({
        url : Drupal.settings.basePath + 'treeoflife/json/organism/' + org.id, 
        dataType: 'json',
        success: function (data) {
          var $img;
          // set title
          if (data.name && data.latin) {
            $('#treeoflife-title').html('<h1>' + data.name + '</h1><h2>' + data.latin + '</h2>');
          } else if (data.title) {
            $('#treeoflife-title').html('<h1>' + data.title + '</h1>');
          } else if (data.latin) {
            $('#treeoflife-title').html('<h1>' + data.latin + '</h1>');
          }

          // add image if available
          if (data.image) {
            $img = $('<div class="treeoflife-image"><img src="' + Drupal.settings.basePath + 'sites/default/files/treeoflife/tree/i/' + data.image + '"/><div class="magnifyglass"></div></div>');
            $img.find('img, .magnifyglass').click(function () {
              var dialog = '<div id="img-' + org.id + '" data-role="page" class="img-view" data-theme="a">'
                  dialog += '<div data-role="content">';
                  dialog += '<img src="' + Drupal.settings.basePath + 'sites/default/files/treeoflife/tree/download/' + data.image + '"/>';
                  dialog += '</div>';
                  dialog += '<div data-role="footer" data-position="fixed">';
                  dialog += '<a href="#infopage" data-icon="info" data-role="button" data-transition="pop" data-direction="reverse">' + Drupal.t('Close') + '</a>';
                  dialog += '</div></div>';
              $('body').append(dialog);
              // clean up memory after leaving page
              $('#img-' + org.id).bind('pagehide', function () {
                $(this).remove();
              });
              $.mobile.changePage('#img-' + org.id, {transition: 'pop'});
            })
            $('#treeoflife-title').append($img);
            $.mobile.hidePageLoadingMsg();
          }
          // add the description
          if (data.desc) {
            $('#treeoflife-text').html('<h2>' + Drupal.t('Beskrivelse') + '</h2><p>' + self.parseBBCode(data.desc) + '</p>');
          } else if (data.text) {
            $('#treeoflife-text').html('<p>' + self.parseBBCode(data.text) + '</p>');
          }
          if (data.furtherreading) {
            $('#treeoflife-text').append('<span class="furtherreading">' + self.parseBBCode(data.furtherreading) + '</span>');
          }
          // change the page to the infopage
          $.mobile.changePage($('#infopage'));
          // if we have coords, focus map on them, but delay by 1000ms to make it happen in the background
          if (org.x && org.w && org.y && org.h) {
            setTimeout(function () {self.map.moveTo(
              new OpenLayers.LonLat(
                (org.x * self.ratio) + (org.w * self.ratio)/2, 
                (org.y * self.ratio) - (org.h * self.ratio)/2),
              4);},
            1000);
          }
        }
      });
      
      
    }
  },
  // check if click was within bounds
  clickLocator : function (x,y) {
    var self = this;
    var organisms = this.organisms;
    if (this.useLowRes) {
      x = x / self.ratio;
      y = y / self.ratio;
    }
    // loop through TreeOfLife.organisms array
    for(var i in organisms) {
      // shorten the coords for readability
      var minX = organisms[i].x,
      maxX = organisms[i].x + organisms[i].w,
      maxY = organisms[i].y,
      minY = organisms[i].y - organisms[i].h; 

      if (minX < x && maxX > x) {
        // and y boundaries
        if (minY < y && maxY > y) {
          // then the organism was clicked, so return it
          return organisms[i];
        }
      }
    }
    return null;
  },
  // replaces some bb code with html
  // todo, do this with the original code instead of on the fly
  parseBBCode : function (text) {
    // replace all [i] [/i] with <em> </em> 
    text = text.replace(/\[i\]/g, "<em>");
    text = text.replace(/\[\/i\]/g, "</em>");
    // replace all [b] [/b] with <strong> </strong>
    text = text.replace(/\[b\]/g, "<strong>");
    text = text.replace(/\[\/b\]/g, "</strong>");
    // replace all [a] [/a] with <a> </a>
    text = text.replace(/\[a?(.*?)"?href='?(.*?)'?\](.*?)\[\/a\]/g,"<a$1href=\'$2\'>$3</a>");
    return text;
  },
  // bind clickhandlers to the zoom buttons
  zoomButtonsInit : function () {
    // Map zoom  
    var self = this;
    $("#plus").click(function () {
      self.map.zoomIn();
    });
    $("#minus").click(function () {
      self.map.zoomOut();
    });
  },
  // initialize the search field
  searchFieldInit : function () {
    var self = this;
    var things = [];
    $('#treeoflife-search').autocomplete(self.organisms, {
      formatItem: function(item) {
        if (item.name != "") {
          return item.name;
        }
        else {
          return item.latin;
        }
      }
    }).result(function(event, item) {
      self.updateInfo(item);
    });
  },
  // store handler
  store: (function() {
    // if we dont have a localStorage
    if (!localStorage) {
      // this.store == false
      return false;
    } else {
      // otherwise this.store.get and this.store.set will exist!
      return {
        // Gets stuff from local storage
        get: function(key) {
          return localStorage[key];
        },
        // Set stuff in local storage
        set: function(key, value) {
          localStorage.setItem(key, value);
        }
      }
    }
  })(),
  loadKeys: function () {
    var self = this,
        url  = Drupal.settings.basePath + self.keys_url;
    $.mobile.showPageLoadingMsg();
    $.getJSON(url, function (data) {
      var i, j, // for iteration
          key,  // each key that will become a collapsible 
          link, id, // short names for readability

          // DOM elements
          $container = $('<div data-role="collapsible-set" data-theme="a">'),
          $content,
          contentArray = [],
          $ul,
          $styles;
          
      //add $container to #keypage
      $('#keypage-content').html($container);
      $styles = $('<style>');
      for (i in data.key) {
        if (data.key.hasOwnProperty(i)) {
          key = data.key[i];
          id = key['@attributes'].id;
          // create a div for each key
          $content = $('<div id="key-' + id +
            '"><h3>' + key.title + '</h3><div class="treeoflife-key-content"><p>' + self.parseBBCode(key.content) + '</p></div></div>');
          $content.appendTo($container)
          $styles.append(self.createCssRule(id, key.color));
          // save $content so we can make it collapsible afterwards
          contentArray.push($content);
          if (key.links && key.links.link) {
            $ul = $('<ul></ul>');
            $ul.appendTo($content.find('.treeoflife-key-content')); // children may bug with jquery mobile
            // add any links to the DOM
            for (j = 0; j < key.links.link.length; j++) {
              link = key.links.link[j];
              $('<li><a href="' + link.url + '" rel="external">' + link.name + '</a></li>').appendTo($ul);
            }
          }
        }
      }
      $('body').append($styles);
      // make items collapsible after all is added to ensure correct css
      for (i = 0; i < contentArray.length; i += 1) {
        contentArray[i].collapsible();
      }
      // also whenever the layout is updated
      $('#keypage').bind('updatelayout pageshow', function () {
        TreeOfLife.fixContentHeight();
      });
      $.mobile.hidePageLoadingMsg();
    });
  },
  createCssRule : function (id, color) {
    var normal = [],
        hover = [],
        style = '';
    if (id && color) {
      normal[0] = color;
      normal[1] = tinycolor.darken(color, 20).toHexString();
      hover[0] = tinycolor.lighten(color, 10).toHexString();
      hover[1] = tinycolor.darken(color, 10).toHexString();

      style += '#key-' + id + ' .ui-btn-up-a {';
      style += 'background: '+ normal[0] + ';';
      style += 'background-image: -webkit-gradient(linear,left top,left bottom,from(' + normal[0] + '),to(' + normal[1] + '));';
      style += 'background-image: -webkit-linear-gradient(' + normal[0] + ',' + normal[1] + ');';
      style += 'background-image: -moz-linear-gradient(' + normal[0] + ',' + normal[1] + ');';
      style += 'background-image: -ms-linear-gradient(' + normal[0] + ',' + normal[1] + ');';
      style += 'background-image: -o-linear-gradient(' + normal[0] + ',' + normal[1] + ');';
      style += 'background-image: linear-gradient(' + normal[0] + ',' + normal[1] + ');';
      style += '}';
      style += '#key-' + id + ' .ui-btn-hover-a { ';
      style += 'background: ' + hover[0] + ';';
      style += 'background-image: -webkit-gradient(linear,left top,left bottom,from(' + hover[0] + '),to(' + hover[1] + '));';
      style += 'background-image: -webkit-linear-gradient(' + hover[0] + ',' + hover[1] + ');';
      style += 'background-image: -moz-linear-gradient(' + hover[0] + ',' + hover[1] + ');';
      style += 'background-image: -ms-linear-gradient(' + hover[0] + ',' + hover[1] + ');';
      style += 'background-image: -o-linear-gradient(' + hover[0] + ',' + hover[1] + ');';
      style += 'background-image: linear-gradient(' + hover[0] + ',' + hover[1] + ');';
      style += '}'
      
      return style;
    }
  }
}