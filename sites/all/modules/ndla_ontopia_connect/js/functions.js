if (Drupal.jsEnabled) {
$(document).ready(    
  function (){

    // expand competence aim for apprentices if the user is an apprentice
    if (typeof userData !== 'undefined') {
      userData.ready(function () {
        relevant_for_apprentice = (userData.getData('global', 'relevant_for_apprentice') == true);
        if (relevant_for_apprentice) {
          if ($(".laerling_show").length) {
            showLaerlingAims($(".laerling_show"));
          }
        }
      });
    }


    if (window['ndlaOntopiaConnectInitialized'] != undefined) {
      return;
    }
    ndlaOntopiaConnectInitialized = true;
    
    $(':input[name=fag]').change(function () {
      var uuid = $(':input[name=fag]').val();
      getGrepData('hovedomraade', uuid);
    });
    
    if(typeof $(".aim_tip_wrapper") !== 'undefined'  && location.href.indexOf('deling') == -1 && location.href.indexOf('nygiv') == -1){
      if(typeof $(".aim_tip_wrapper a").ndlaTooltip == 'function') {
        $(".aim_tip_wrapper a").ndlaTooltip(function() {
          return $(this).next().html();
        });
      }
    }
  }
);  
  
}//end jsenabled

function getNodeMetaData(ressursurl){
	nodeid = ressursurl.substring(ressursurl.lastIndexOf('/')+1);
    
    $.ajaxSetup({ 
      scriptCharset: "utf-8" , 
      contentType: "application/x-www-form-urlencoded; charset=utf-8"
    });
    
    
    jasonUrl = Drupal.settings.ndla_ontopia_connect.node_metadata_url+"/"+nodeid;
    
    $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    contentType: "application/x-www-form-urlencoded; charset=utf-8",
    success: function(data) {
      setNodeMetadata(data);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_errors").html(Drupal.t('<p>An error occured when associating resource to the competence aim. Unable to fetch data.</p>'));
    }
    
    });
}

function setTittel(ressursurl,data) {
  var antall = data.values.length;
  var name = data.values[0].topicname;
  var topicid = data.values[0].topicid;
  var type = data.values[0].topic_type;
  if(name != ""){
    //var message = Drupal.t("This resource is already connected to a competence aim, you may however give it a new name that will be valid in the perspective of the resource type scope of your chosen subject");
    /*
    var message = Drupal.t("NB! This resource is already connected to a competence aim");
    $("#resource_name-message").html(message);
    $("#resource_name-message").addClass('message');
    $("#resource_name-message").slideDown(500,function(){});
    */
    
    $(":input[name=resource_type] option[value='"+type+"']").attr('selected', 'selected');
    $(":input[name=resource_type]").attr('disabled', 'disabled');
    $(':input[name=resource_name]')[0].value = name;
    $(':input[name=resource_name-duplicate]')[0].value = name;
    $(':input[name=resource_name-duplicate-topicid]')[0].value = topicid;
    $(':input[name=resource_name-duplicate-type]')[0].value = type;
    $(':input[name=resource_name-all]')[0].value = JSON.stringify(data);
  }
}

function setNodeMetadata(data){
	$(':input[name=resource_meta]')[0].value = JSON.stringify(data);
}

function sjekkNavn() {
  var navn = $(':input[name=resource_name]')[0].value;
  var all_names =  $(':input[name=resource_name-all]')[0].value;
  ascopes = new Array();
  if(all_names != '') {
    var json = $.evalJSON(all_names);
    scopes = json.values[0].scoped_names;
    for (var i = 0; i < scopes.length;i++) {
     ascopes.push(scopes[i].topicname);
    }
  }
 
  
  if ($(':input[name=resource_name-duplicate]')[0].value != "" && (navn == $(':input[name=resource_name-duplicate]')[0].value)){
    alert(Drupal.t("This name alread exists for this resource"));
    return false;
  }
  else if (array_has_value(ascopes,navn)) {
    alert(Drupal.t("This name alread exists for this resource"));
    return false;
  }
  else {
    return true;
  }
}

function filtrerYrkesMaal(trinnid, filter_id,plan_id,modus) {
  var nid = $(':input[name=group-hidden]')[0].value;
  var hidden_json = $(':input[name=kompetansemaal-yrkesfag-trinn-hidden]')[0].value;
  var base = Drupal.settings.ndla_ontopia_connect.base_url;
  var language = Drupal.settings.ndla_ontopia_connect.language;
  var hovedomraade_id = $(':input[name=yrkesfag-first-hovedpsi-hidden]')[0].value;
  if (modus == 'paa') {
    var json = $.evalJSON(hidden_json);
    var data = json[nid][filter_id].filter_data;
    var hoved_output = '';
    var maal_output = '';
    var maal;
    $.each(data, function(key,val){
      
      if (key == plan_id){
        var hovedomraader = val.hovedomraader;
        for(var i = 0; i < $(hovedomraader).length; i++) {
          var hovedomraade = $(hovedomraader)[i];
          var hovedomraade_tittel = $("#"+hovedomraade.hovedomraade_uid.replace(':','')).html();
          if(i == 0) {
            hoved_output += '<li class="grep_first_level selected expanded"><a href="javascript:" id="'+hovedomraade.hovedomraade_uid.replace(':','')+'" onclick="getYrkesKompetanseMaal(\''+hovedomraade.hovedomraade_uid+'\',\''+filter_id+'\',\''+key+'\');"><span>'+hovedomraade_tittel+'</span></a></li>';
            maal = hovedomraade.kompetansemaal;
          }
          else {
            hoved_output += '<li class="grep_first_level"><a href="javascript:" id="'+hovedomraade.hovedomraade_uid.replace(':','')+'" onclick="getYrkesKompetanseMaal(\''+hovedomraade.hovedomraade_uid+'\',\''+filter_id+'\',\''+key+'\');"><span>'+hovedomraade_tittel+'</span></a></li>';
          }
          
        }
      }
      
    });
    
    
    for(var j = 0; j < $(maal).length; j++) {
      var kompetansemaal = $(maal)[j];
      //maal_output += "<li><a id='"+kompetansemaal.kompetansemaal_uuid+"' href=\""+base+"/node/"+nid+"/grep/"+kompetansemaal.kompetansamaal_uuid+"\">"+kompetansemaal.kompetansemaal_title+"</a> ("+filter_id.replace('_',' ')+")</li>";
      maal_output += "<li><a id='"+kompetansemaal.kompetansemaal_uuid+"' href=\""+base+"/"+language+"/node/"+nid+"/grep/"+kompetansemaal.kompetansemaal_uuid+"\"  class=\"kompetansemaal\">"+kompetansemaal.kompetansemaal_title+"</a></li>";
    }
    
    $("#trinnspan_"+trinnid).removeClass('trinnspan_av');
    $("#trinnspan_"+trinnid).addClass('trinnspan_paa');
    var par = $("#trinnspan_"+trinnid).parent();
    trinnhtml = par.html();
    $('.yrkefilter_title').children().filter('.yrkefilter').before('<span>&nbsp;</span>');
    trinnhtml = trinnhtml.replace("'paa'","'av'");
    par.html(trinnhtml);
    $("#trinnspan_"+trinnid).attr('checked','checked');
    hoved_output = '<ul>'+hoved_output+'</ul>';
    maal_output = '<ul>'+maal_output+'</ul>';
    $('#grep-results-hovedomraader').html(hoved_output);
    $('#grep-results-kompetansemaal').html(maal_output);
    Drupal.attachBehaviors($('#grep-results-kompetansemaal'));
    addClickListeners();
    
    grep_resizeContainer();
    
  }
  else {
    maaljson = $(':input[name=grep-hidden]')[0].value;
    var json = $.evalJSON(maaljson);
    var fag_data;
    
    for (var i = 0; i< json.values.length; i++)  {
      if(json.values[i].fag_psi == plan_id) {
        fag_data = json.values[i].fag_data;
      }
    }
    
    var datastr = '{"values": '+JSON.stringify(fag_data)+'}';
    var jsondata = $.evalJSON(datastr);
    
    processFagData(jsondata,nid)
  }
  
  
  
  
  
  

}

function getYrkesKompetanseMaal(hovedomraade_id,filter_id,plan_id) {
  
  
  var nid = $(':input[name=group-hidden]')[0].value;
  var hidden_json = $(':input[name=kompetansemaal-yrkesfag-trinn-hidden]')[0].value;
  var base = Drupal.settings.ndla_ontopia_connect.base_url;
  var language = Drupal.settings.ndla_ontopia_connect.language;
  var json = $.evalJSON(hidden_json);
  var data = json[nid][filter_id].filter_data;
  var maal_output = '';
  var maal;
  
  $.each(data, function(key,val){
    if(key == plan_id) {
      var hovedomraader = val.hovedomraader;
      for(var i = 0; i < $(hovedomraader).length; i++) {
        var hovedomraade = $(hovedomraader)[i];
        if(hovedomraade.hovedomraade_uid == hovedomraade_id) {
          maal = hovedomraade.kompetansemaal; 
        } 
      }
    }
  });
    
  
  
  for(var j = 0; j < $(maal).length; j++) {
    var kompetansemaal = $(maal)[j];
  //maal_output += "<li><a id='"+kompetansemaal.kompetansemaal_uuid+"' href=\""+base+"/node/"+nid+"/grep/"+kompetansemaal.kompetansamaal_uuid+"\">"+kompetansemaal.kompetansemaal_title+"</a> ("+filter_id.replace('_',' ')+")</li>";
    maal_output += "<li><a id='"+kompetansemaal.kompetansemaal_uuid+"' href=\""+base+"/"+language+"/node/"+nid+"/grep/"+kompetansemaal.kompetansemaal_uuid+"\">"+kompetansemaal.kompetansemaal_title+"</a></li>";
  }
  
  maal_output = '<ul>'+maal_output+'</ul>';
  $('.selected').filter(function(){
    $(this).removeClass('selected');
    $(this).removeClass('expanded');
  });
  $('#'+hovedomraade_id.replace(':','')).parent().addClass('selected');
  $('#'+hovedomraade_id.replace(':','')).parent().addClass('expanded');
  
  $('#grep-results-kompetansemaal').html(maal_output);
  Drupal.attachBehaviors($('#grep-results-kompetansemaal'));
  
  grep_resizeContainer();
}


function filterGrepListing(language) {
  var reltypes = new Array();
  var nodetypes = new Array();
  
  $('.relationfilter').each(function(){
    if ($(this).is(':checked')){
      reltypes.push($(this).attr('name'));
    }
  });
  
  $('.typefilter').each(function(){
    if ($(this).is(':checked')){
      nodetypes.push($(this).attr('name'));
    }
  });
  
  var data = $(':input[name=grep-listing-data]')[0].value;
  var types = $(':input[name=nodetype-arr]')[0].value;
  var json = $.evalJSON(data);
  var jtypes = $.evalJSON(types);
  var content = '';
  var len = json.values.length;
  var sorted_data = sortArrByType(json.values,reltypes,jtypes);
  for(var i in sorted_data){
    tmpdata = sorted_data[i];
    for (var j = 0; j  < tmpdata.length; j++) {
      var rdata = tmpdata[j];
      if (array_has_value(nodetypes,'type_'+rdata.type_tid) && (rdata.spraak == '' || rdata.spraak == language)) {
        content += '<div class="node clear-block '+reldata.relation_type+' nodetype_'+rdata.type_tid+' grep-list-item" id="node-'+rdata.nodeid+'">';  
        content += '<div class="ingressView"><div class="grep-listing-subtitle">'+rdata.type_name+'</div>';
        content += '<h2><a title="'+rdata.title+'" href="'+rdata.url.replace('spraak', rdata.spraak == '' ? language : rdata.spraak)+'">'+rdata.title+'</a></h2>';
        
        if (rdata.node_ingress != '' || rdata.image_url_thumbnail != '' || rdata.image_url_original != ''){
          content += '<div class="content">';
          
          if ( rdata.image_url_thumbnail != '' || rdata.image_url_original != '') {
            var imgarr = rdata.image_url_original.split('/images');
            var imgcache_url = Drupal.settings.ndla_ontopia_connect.base_url+"/"+imgarr[0]+"/imagecache/Liten/images"+imgarr[1];
            content += '<img class="imagecache imagecache-Liten" title="'+rdata.image_title+'" alt="'+rdata.image_title+'" src="'+imgcache_url+'">';
            content += '<div class="form-item"><a href="/'+rdata.image_url_original+'">'+Drupal.t('Original size')+'</a></div>';
          }
          
          if (rdata.node_ingress != '') {
            content += rdata.node_ingress;
          }
          
          content += '</div>';
        }
        
        
        content += '</div>';
        content += '</div>';
      }
    }
  }

  $("#resource_content").html(content);
}


function sortArrByType(arr, reltypes, types){
  for(var i = 0; i< arr.length; i++) {
    var site = arr[i].site;
    var sitedata = arr[i].sitedata;
    var slen = sitedata.length;
    
    for (var j = 0; j < slen; j++) {
      reldata = sitedata[j];
      if (array_has_value(reltypes,reldata.relation_type)) {
        var resourcedata = reldata.resource_data;
        for(var k = 0; k < resourcedata.length; k++) {
          var rdata = resourcedata[k];
          if(typeof types[rdata.type_key] != 'undefined'){
            types[rdata.type_key].push(rdata);
          }
        }
      }
    }
  }
  return types;
}

function filtrerMaal(psi,trinnid,modus) {
  var maal = $(':input[name=kompetansemaal-hidden]')[0].value;
  var json = $.evalJSON(maal);
  var nid = $(':input[name=group-hidden]')[0].value;
  var count = json.values.length;
  var trinn = $(':input[name=kompetansemaal-trinn-hidden]')[0].value;
  var output = '<ul>';
  var add = false;
  if ($(':input[name=add_fag]').val()) {
    add = true;
  }
  
  var nodeurl = document.URL;
  var base = Drupal.settings.ndla_ontopia_connect.base_url
  var language = Drupal.settings.ndla_ontopia_connect.language;

  
  if(modus == 'add') {
    var assoctypes = $.evalJSON(Drupal.settings.ndla_ontopia_connect.assoctypes);
    var lang = $(':input[name=grep-language]')[0].value;
    var options = '';    
    
    for (var i in assoctypes) {
        options += '<option value="'+i+'">'+assoctypes[i].kompetansemaal+'/'+assoctypes[i].laeringsressurs+'</option>';
    }
  }
  
  
  
  if($("#trinnspan_"+trinnid).hasClass('trinnspan_paa')) {
    if (trinn == '') {
      for (var i = 0; i < count; i++) {
        if (typeof(json.values[i]) !== 'undefined' && json.values[i] != null) {
          if (add) {
            output += "<input id=\"maal_"+i+"\" type=\"checkbox\" value=\""+json.values[i].uuid+"\" name=\"maal\" onclick=\"settTrinn(\'"+json.values[i].scope+"\')\" />&nbsp;<span id=\"maaltext_"+i+"\">"+json.values[i].uuid_title+"</span> ("+json.values[i].scope_title+") <br />";            
            output += "<select id=\"assoc_"+i+"\" class=\"form-select\" name=\"assoc_"+i+"\"><option selected=\"selected\" value=\"\">Relation type</option>"+options+"</select><br />";
            
          }
          else {
            //output += "<li><a id='"+json.values[i].uuid+"' href=\""+nodeurl+"/grep/"+json.values[i].uuid+"\">"+json.values[i].uuid_title+"</a> ("+json.values[i].scope_title+")</li>";            
            output += "<li><a id='"+json.values[i].uuid+"' href=\""+base+"/"+language+"/node/"+nid+"/grep/"+json.values[i].uuid+"\" class=\"kompetansemaal\" >"+json.values[i].uuid_title+"</a></li>";
            
          }
        }
      }
      $("#trinnspan_"+trinnid).removeClass('trinnspan_paa');
      $("#trinnspan_"+trinnid).addClass('trinnspan_av');
    }
    else {
      var tjson = $.evalJSON($(':input[name=kompetansemaal-trinn-hidden]')[0].value);
      var tcount = tjson.trinn.length;
      tjson.trinn = removeTrinnObject(tjson.trinn,psi);
      for (var i = 0; i < count; i++) {
        if (typeof(json.values[i]) !== 'undefined' && json.values[i] != null) {
          if (tjson.trinn.length >= 1) {
            if (array_object_has_key(tjson.trinn,json.values[i].scope)) {
              if (add) {
                output += "<input id=\"maal_"+i+"\" type=\"checkbox\" value=\""+json.values[i].uuid+"\" name=\"maal\" onclick=\"settTrinn(\'"+json.values[i].scope+"\')\" />&nbsp;<span id=\"maaltext_"+i+"\">"+json.values[i].uuid_title+"</span> ("+json.values[i].scope_title+") <br />";
                output += "<select id=\"assoc_"+i+"\" class=\"form-select\" name=\"assoc_"+i+"\"><option selected=\"selected\" value=\"\">Relation type</option>"+options+"</select><br />";
              }
              else {
                //output += "<li><a id='"+json.values[i].uuid+"' href=\""+nodeurl+"/grep/"+json.values[i].uuid+"\">"+json.values[i].uuid_title+"</a> ("+json.values[i].scope_title+")</li>";
                output += "<li><a id='"+json.values[i].uuid+"' href=\""+base+"/"+language+"/node/"+nid+"/grep/"+json.values[i].uuid+"\" class=\"kompetansemaal\" >"+json.values[i].uuid_title+"</a></li>";
              }
            }
          }
          else {
            if (add) {
              output += "<input id=\"maal_"+i+"\" type=\"checkbox\" value=\""+json.values[i].uuid+"\" name=\"maal\" onclick=\"settTrinn(\'"+json.values[i].scope+"\')\" />&nbsp;<span id=\"maaltext_"+i+"\">"+json.values[i].uuid_title+"</span> ("+json.values[i].scope_title+") <br />";
              output += "<select id=\"assoc_"+i+"\" class=\"form-select\" name=\"assoc_"+i+"\"><option selected=\"selected\" value=\"\">Relation type</option>"+options+"</select><br />"; 
            }
            else {
              //output += "<li><a id='"+json.values[i].uuid+"' href=\""+nodeurl+"/grep/"+json.values[i].uuid+"\">"+json.values[i].uuid_title+"</a> ("+json.values[i].scope_title+")</li>";
              output += "<li><a id='"+json.values[i].uuid+"' href=\""+base+"/"+language+"/node/"+nid+"/grep/"+json.values[i].uuid+"\" class=\"kompetansemaal\" >"+json.values[i].uuid_title+"</a></li>";
            }
          }
        }
        
      }
      trinnout = JSON.stringify(tjson);
      $(':input[name=kompetansemaal-trinn-hidden]')[0].value = trinnout; 
      $("#trinnspan_"+trinnid).removeClass('trinnspan_paa');
      $("#trinnspan_"+trinnid).addClass('trinnspan_av');
    }
  }
  else {
    if (trinn == '') {
      for (var i = 0; i < count; i++) {
        if (typeof(json.values[i]) !== 'undefined' && json.values[i] != null) {
          if (json.values[i].scope == psi) {
            if (add) {
              output += "<input id=\"maal_"+i+"\" type=\"checkbox\" value=\""+json.values[i].uuid+"\" name=\"maal\" onclick=\"settTrinn(\'"+json.values[i].scope+"\')\" />&nbsp;<span id=\"maaltext_"+i+"\">"+json.values[i].uuid_title+"</span> ("+json.values[i].scope_title+") <br />";
              output += "<select id=\"assoc_"+i+"\" class=\"form-select\" name=\"assoc_"+i+"\"><option selected=\"selected\" value=\"\">Relation type</option>"+options+"</select><br />"; 
            }
            else {
              //output += "<li><a id='"+json.values[i].uuid+"' href=\""+nodeurl+"/grep/"+json.values[i].uuid+"\">"+json.values[i].uuid_title+"</a> ("+json.values[i].scope_title+")</li>";
              output += "<li><a id='"+json.values[i].uuid+"' href=\""+base+"/"+language+"/node/"+nid+"/grep/"+json.values[i].uuid+"\" class=\"kompetansemaal\" >"+json.values[i].uuid_title+"</a></li>";
            }
          }
        }
      }
        
      
      trinndata = {"trinn" : [{"trinnid": trinnid , "psi" : psi}]};
      str = JSON.stringify(trinndata);
      $(':input[name=kompetansemaal-trinn-hidden]')[0].value = str; 
      $("#trinnspan_"+trinnid).removeClass('trinnspan_av');
      $("#trinnspan_"+trinnid).addClass('trinnspan_paa');
    }
    else {
      var tjson = $.evalJSON($(':input[name=kompetansemaal-trinn-hidden]')[0].value);
      trinndata = {"trinnid" : trinnid , "psi": psi};
      tjson.trinn.push(trinndata);
      for (var i = 0; i < count; i++) {
        if (typeof(json.values[i]) !== 'undefined' && json.values[i] != null) {
          if (array_object_has_key(tjson.trinn,json.values[i].scope)) {
            if (add) {
              output += "<input id=\"maal_"+i+"\" type=\"checkbox\" value=\""+json.values[i].uuid+"\" name=\"maal\" onclick=\"settTrinn(\'"+json.values[i].scope+"\')\" />&nbsp;<span id=\"maaltext_"+i+"\">"+json.values[i].uuid_title+"</span> ("+json.values[i].scope_title+") <br />";
              output += "<select id=\"assoc_"+i+"\" class=\"form-select\" name=\"assoc_"+i+"\"><option selected=\"selected\" value=\"\">Relation type</option>"+options+"</select><br />"; 
            }
            else {
              //output += "<li><a id='"+json.values[i].uuid+"' href=\""+nodeurl+"/grep/"+json.values[i].uuid+"\">"+json.values[i].uuid_title+"</a> ("+json.values[i].scope_title+")</li>";
              output += "<li><a id='"+json.values[i].uuid+"' href=\""+base+"/"+language+"/node/"+nid+"/grep/"+json.values[i].uuid+"\" class=\"kompetansemaal\" >"+json.values[i].uuid_title+"</a></li>";
            }
          }
        }
      }
     
      trinnout = JSON.stringify(tjson);
      $(':input[name=kompetansemaal-trinn-hidden]')[0].value = trinnout; 
      $("#trinnspan_"+trinnid).removeClass('trinnspan_av');
      $("#trinnspan_"+trinnid).addClass('trinnspan_paa');
    }
  }
  output += '</ul>';
  
  if (modus == '') {
    $('#grep-results-kompetansemaal').html('');
    $('#grep-results-kompetansemaal').html(output);
    Drupal.attachBehaviors($('#grep-results-kompetansemaal'));
  }
  else if(modus == 'add') {
    $('#grep-results-kompetansemaal-light').html('');
    $('#grep-results-kompetansemaal-light').html(output);
  }
}

function settTrinn(trinn) {
  $(":input[name=assoc_trinn] option[value='"+trinn+"']").attr('selected', 'selected');
}

function array_object_has_key(array,psi){
  var acount = array.length;
  var out = false;
  for (var i = 0; i < acount ; i++) {
    if (array[i].psi == psi) {
      out = true;
      break;
    }
  }
  return out;
}

function array_has_value(arr,val){
  var acount = arr.length;
  var out = false;
  for (var i = 0; i < acount ; i++) {
    if(arr[i] == val) {
      out = true;
      break;
    }
  }
  return out;
}

function removeTrinnObject(array,psi) {
  acount = array.length;
  out = new Array();
  for (var i = 0; i < acount; i++) {
    if (array[i].psi != psi) {
      out.push(array[i]);
    }
  }
  return out;
}


function getlaereplaner_by_user(){
  var fmap = Drupal.settings.ndla_ontopia_connect.fagmap;
  var fagmap = $.evalJSON(fmap);
  
  jasonUrl = Drupal.settings.ndla_ontopia_connect.laereplan_by_user+"/json";

  $.ajax({
  type: 'GET',
  url: jasonUrl,
  dataType: 'json',
  success: function(data) {
    opts = '<option value="" selected="selected">Velg Læreplan</option>';
    opts2 = '<option value="" selected="selected">Velg Læreplan</option>';
    for (var i = 0; i< data.planer.length; i++) {
      if(typeof(data.planer[i]) !== 'undefined'){
        opts += '<option value="'+data.planer[i].uuid+'">'+data.planer[i].title+'</option>';
        opts2 += '<option value="'+fagmap[data.planer[i].uuid]+'">'+data.planer[i].title+'</option>';
      }
    }
    $(':input[name=add_fag]').html(opts);
    $(':input[name=assoc_fag]').html(opts);
    $(':input[name=resource_title_scope]').html(opts2);
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    $("#ontopia_connect_erorrs").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
  }
  });
}



function getCurriculaBySubject(subject,div_id) {
  jasonUrl = Drupal.settings.ndla_ontopia_connect.laereplan_by_subject+"/"+subject
  var chosen_uuid = "";
  var chosen_topicid = "";
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
      if (div_id.indexOf('plan') > -1) {
        var fagmap = Drupal.settings.ndla_ontopia_connect.fagmap;
        var nid = '';
        for (var i in fagmap){
        	var sub = fagmap[i];
        	if(typeof sub != 'undefined'){
	        	for(var j = 0; j < sub.length; j++) {;
	        		if(sub[j].uuid == subject){
	        			nid = i;
	                    chosen_uuid = sub[j].uuid;
	                    chosen_topicid = sub[j].topicid;
	                    break;
	        		}
	        	}//end for
        	}//end if
        }
        $(":input[name=assoc_fag] option[value=\'"+nid+"\']").attr('selected', 'selected');
        $(":input[name=chosen-laereplan-hidden]").attr('value',chosen_uuid);
        $(":input[name=chosen-topicid-hidden]").attr('value',chosen_topicid);
        if(typeof $('#grep_search_span').css('display') != 'undefined') {
          $('#grep_search_span').css('display','none');
        }
        processTreeAims(data,div_id); 
      }
      else {
        if(typeof $('#grep_search_span').css('display') != 'undefined') {
          $('#grep_search_span').css('display','none');
        }
        processAims(data,div_id);
      }
      
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_erorrs").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
    }
    });
}

function getCurriculumTree(mode){
  $(':input[name=tree_mode]')[0].value = mode;
  if (mode == 'all_deling') {
    $("#all_deling_link").slideUp(500,function(){});
    $("#all_link").slideUp(500,function(){});
    $("#all_deling_link").parent().append($('<div class="grep-loader"></div>'));
  }
  else if (mode == 'all') {
    $("#all_link").slideUp(500,function(){});
    $("#all_link").parent().append($('<div class="grep-loader"></div>'));
    $("#user_link").slideUp(500,function(){});
  }
  else if (mode == 'user') {
    $("#all_link").slideUp(500,function(){});
    $("#user_link").slideUp(500,function(){});
    $("#all_link").parent().append($('<div class="grep-loader"></div>'));
  }

  $("#curriculum_tree").slideUp(500,function(){});
  
  var jasonUrl = Drupal.settings.ndla_ontopia_connect.curriculatree+'/'+mode;
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
      createTree(data,mode);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_erorrs").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
    }
  });
}


function createTree(data,mode) {
  $('#grep_search_span').css('display','inline');
  $("#all_deling_link").slideUp(500,function(){});
  $("#curriculum_tree").slideUp(500,function(){});
  $(':input[name=tree_json]')[0].value = JSON.stringify(data);
  tView = new curriculatree(data, $('#curriculum_tree'),'curriculum_listing',mode);
  if (mode == 'user') {
    $("#all_link").html('<a href="javascript:" onclick="getCurriculumTree(\'all\')">'+Drupal.t('All curricula')+'</a>');
    $("#all_link").attr('class','inactive-tab');
    $("#user_link").html(Drupal.t('Curricula by user'));
    $("#user_link").attr('class','active-tab');
    $("#user_link").attr('class','active-tab').css('display','block')
  }
  else if (mode == 'all'){
   
    $("#all_link").html(Drupal.t('All curricula'));
    $("#all_link").attr('class','active-tab');
   
    $("#user_link").html('<a href="javascript:" onclick="getCurriculumTree(\'user\')">'+Drupal.t('Curricula by user')+'</a>');
    $("#user_link").attr('class','inactive-tab')
    $("#user_link").slideDown(500,function(){});
    
  }
  else if (mode == 'all_deling'){
    $("#all_link").html(Drupal.t('Curricula'));
    $("#all_link").attr('class','active-tab');
  }
}



function getCurriculaResourcesBySubject(subject,div_id,modus,func) {
  if (modus == 'plan') {
    
    if (func == 'port') {
      
      jasonUrl = Drupal.settings.ndla_ontopia_connect.laereplan_by_subject_port+"/"+subject
    }
    else if (func == 'newport') {
      $("#batch_resource_title_span_"+div_id).append($('<div class="aim_window_loader_white_nb"></div>'));
      jasonUrl = Drupal.settings.ndla_ontopia_connect.laereplan_by_subject+"/"+subject
    } else {
      jasonUrl = Drupal.settings.ndla_ontopia_connect.laereplan_by_subject+"/"+subject  
    }
  }
  else if (modus == 'ressurs'){
    if (func == 'port') {
      jasonUrl = Drupal.settings.ndla_ontopia_connect.laereplan_port_resources_by_subject+"/"+subject
    }
    else {
      jasonUrl = Drupal.settings.ndla_ontopia_connect.laereplan_resources_by_subject+"/"+subject
    }
    
  }
  
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
      processAimResources(data,div_id,modus,subject,func);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_erorrs").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
    }
    });
}

function getGrepData(type, psi, modus) {
  ids = {'light-hoved' : 'grep-results-hovedomraader-light', 'light-kompetanse': 'grep-results-kompetansemaal-light','browse-hoved' : 'grep-results-hovedomraader', 'browse-kompetanse': 'grep-results-kompetansemaal'};
  if (modus == 'lightbox') {
    //$('#grep-results-hovedomraader-light').html('');
  }
  else if (modus == 'browse') {
    //$('#grep-results-hovedomraader').html('');
  }
  
  if(typeof($(':input[name=kompetansemaal-trinn-hidden]')[0] !== 'undefined')){
    $(':input[name=kompetansemaal-trinn-hidden]')[0].value = '';
  }
  
  if (modus != 'ressurs') {
    $('#'+psi).addClass('selected');
    $('#grep-results-kompetansemaal-light').html('');
    $('#grep-results-kompetansemaal').html('');
  }
  
  if(modus == 'lightbox' && type == 'kompetansemaal'){
    $('.selected').filter(function(){
      $(this).removeClass('selected');
    });
    $('#'+psi.replace(':','')).addClass('selected');
  }
  else if(modus == 'browse' || (modus == 'lightbox' && type == 'kompetansemaal')){
    $('.selected').filter(function(){
      $(this).removeClass('selected');
    });
    $('#'+psi.replace(':','')).addClass('selected');
  }
  
  $('#grep-results-ressurser-light').html('');
  $(':input[name=kompetansemaal-hidden]')[0].value = '';
  $('#trinn-wrapper').html('');
  $('#trinn-wrapper').css('display','none');
  
  var add = false;
  var list = false;
  if ($(':input[name=fag]').val()) {
    var fag = $(':input[name=fag]').val();
    list = true;
  }
  
  if ($(':input[name=add_fag]').val()){
    var fag = $(':input[name=add_fag]').val();
    add = true;
  }

  if(!list && !add){
    if ($(':input[name=fag-hidden]')) {
      var fag = $(':input[name=fag-hidden]')[0].value;
    }
  }
  
 
  
  
  if (type != 'ressurs') {
    jasonUrl = Drupal.settings.ndla_ontopia_connect.json_url+"/"+type+"/"+fag+"/"+psi+"/json";
    $.ajax({
    type: 'GET',
    url: jasonUrl,
    beforeSend: function(x) {
      if(x && x.overrideMimeType) {
       x.overrideMimeType("application/j-son;charset=UTF-8");
      }
     },
    dataType: 'json',
    success: function(data) {
      if(modus == 'lightbox' && type == 'hovedomraade') {
        type = 'light-hoved';
      }
      else if(modus == 'lightbox' && type == 'kompetansemaal') {
        type = 'light-kompetanse';
      }
      else if(modus == 'browse' && type == 'kompetansemaal') {
        type = 'browse-kompetanse';
      }
      else if(modus == 'browse' && type == 'hovedomraade') {
        type = 'browse-hoved';
      }
      processData(data,ids[type],add,psi);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_erorrs").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
    }
    });
  }
  else {
    
    var fmap = Drupal.settings.ndla_ontopia_connect.fagmap;
    var fagmap = $.evalJSON(fmap);    
    jasonUrl = Drupal.settings.ndla_ontopia_connect.resource_json_url+"/laeringsressurs/"+fagmap[fag]+"?id="+psi;
    //alert(jasonUrl); 
    $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
      //alert(data);
      processResource(data);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_errors").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
    }
    
    });
  }
  
}

function getGrepListing(psi) {

    $('#'+psi).addClass('selected');
    $('#grep-results-kompetansemaal-light').html('');
    $('#grep-results-hovedomraader-light').html('');

    $('.selected').filter(function(){
      $(this).removeClass('selected');
    });
    $('#'+psi.replace(':','')).addClass('selected');

  
  $('#grep-results-ressurser-light').html('');
  $(':input[name=kompetansemaal-hidden]')[0].value = '';
  $('#trinn-wrapper').html('');
  $('#trinn-wrapper').css('display','none');
  jasonUrl = Drupal.settings.ndla_ontopia_connect.greplisting_json_url+"/"+psi;
  $.ajax({
  type: 'GET',
  url: jasonUrl,
  dataType: 'json',
  success: function(data) {
    datastr = '{"values": [';
    for (var i = 0; i< data.values.length; i++)  {
        datastr += '{"uuid" : "'+data.values[i].hovedomraade_uid+'", "uuid_title" : "'+data.values[i].hovedomraade_tittel+'"},';
    }
    datastr.substring(0,(datastr.lastIndexOf(','))-1);
    datastr += ']}';
    jsondata = $.evalJSON(datastr);
    $(':input[name=grep-hidden]')[0].value = JSON.stringify(data);
    processData(jsondata,'grep-results-hovedomraader-light',false,psi);
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    $("#ontopia_connect_errors").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
  }
  
  });
}


function getKompetanseMaal(psi,type,fagnid,plan_psi) {
  ids = {'light-hoved' : 'grep-results-hovedomraader-light', 'light-kompetanse': 'grep-results-kompetansemaal-light','browse-hoved' : 'grep-results-hovedomraader', 'browse-kompetanse': 'grep-results-kompetansemaal'};

  if(typeof($(':input[name=kompetansemaal-trinn-hidden]')[0] !== 'undefined')){
    $(':input[name=kompetansemaal-trinn-hidden]')[0].value = '';
  }
  
  
  
  //$('#'+psi).addClass('selected');
  $('#grep-results-kompetansemaal-light').html('');
  $('#grep-results-kompetansemaal').html('');

  $('.selected').filter(function(){
    $(this).removeClass('selected');
  });
  $('#'+psi.replace(':','')).parent().addClass('selected');
  
  $('#grep-results-ressurser-light').html('');
  $(':input[name=kompetansemaal-hidden]')[0].value = '';
  
  
  
  if($('.yrkefilter_title').length == 0){
    $('#trinn-wrapper').html('');
    
  }
  
  $('#trinn-wrapper').css('display','none');
  
  
  
  
  if (typeof($(':input[name=fag-hidden]')[0]) !== 'undefined') {
    var fag = $(':input[name=fag-hidden]')[0].value;
  }

  var maaljson = $(':input[name=grep-hidden]')[0].value
  var json = $.evalJSON(maaljson);
  var data;
  
  
 
  if (type != 'light-kompetanse') {
    for (var i = 0; i< json.values.length; i++)  {
      if(json.values[i].fag_psi == plan_psi) {
        var fag_data = json.values[i].fag_data;
      }
    }
    
    for (var j = 0; j< fag_data.length; j++)  {
      if (fag_data[j].hovedomraade_uid == psi) {
        data = fag_data[j].maaldata;
        break;
      } 
    }
  }
  else {
    for (var i = 0; i< json.values.length; i++)  {
      if(json.values[i].hovedomraade_uid == psi) {
        var data = json.values[i].maaldata;
        break;
      }
    }
  }
  
  
  datastr = '{"values": '+JSON.stringify(data)+'}';
  jsondata = $.evalJSON(datastr);
  
  if(type == 'light-kompetanse') {
    processData(jsondata,ids[type],true,plan_psi);
  }
  else {
    processData(jsondata,ids[type],false,plan_psi);
  }
}

function switchSubject(plan_psi,nid) {
  var maaljson = $(':input[name=grep-hidden]')[0].value
  var json = $.evalJSON(maaljson);
  var fag_data;
  
  for (var i = 0; i< json.values.length; i++)  {
    if(json.values[i].fag_psi == plan_psi) {
      fag_data = json.values[i].fag_data;
    }
  }
  ;
  
  var datastr = '{"values": '+JSON.stringify(fag_data)+'}';
  
  var jsondata = $.evalJSON(datastr);
  $('.plan_selected').filter(function(){
    $(this).removeClass('plan_selected');
  });
  $('#'+plan_psi.replace(':','')).addClass('plan_selected');
  
  $('#trinn-wrapper').html('');
  $('#trinn-wrapper').css('display','none');
  
  if(typeof($(':input[name=kompetansemaal-trinn-hidden]')[0] !== 'undefined')){
    $(':input[name=kompetansemaal-trinn-hidden]')[0].value = '';
  }
 
  processFagData(jsondata,nid);
}


function addFragment(timestamp) {
  var fag = $(':input[name=assoc_fag]').val();
  var ressurstype = $(':input[name=resource_type]').val();
  var ressursurl = $(':input[name=resource_url]')[0].value;
  var ressursnavn = $(':input[name=resource_name]')[0].value;
  var ressursnavn_scope = $(':input[name=resource_title_scope]').val();
  var errors = new Array();
  var maalarr = new Array();
  var assoctypearr = new Array();
  var laerlingarr = new Array();
  
  
  $('.assoc-checkbox').each(function(){
    if ($(this).is(':checked')){
     //var uuid = 'uuid_'+$(this).attr('name').substr($(this).attr('name').lastIndexOf('_')+1);
     var uuid = $(this).attr('name').substr($(this).attr('name').indexOf('kompetansemaal'));
     if($('#maal_'+uuid).is(':checked')) {  
       maalarr.push(uuid);
       assoctypearr.push($(this)[0].value);
     }
     
     if($('#laerling_check_'+uuid).is(':checked')){
       laerlingarr.push('true');
     }
     else{
       laerlingarr.push('false');
     }
    }
  });
  
  var maalant = maalarr.length;
  for (var i = 0; i < maalant; i++) {
    if (i == (maalant-1)) {
      sjekkData(ressurstype,ressursurl,ressursnavn,assoctypearr[i], maalarr[i],laerlingarr[i], fag, timestamp,'save',i);
    }
    else {
      sjekkData(ressurstype,ressursurl,ressursnavn,assoctypearr[i], maalarr[i],laerlingarr[i], fag, timestamp,'continue',i);
    }
  }
}



function topicUpdate(action,reifier,predikat,timestamp,ressursid,maal,fag,trinn,apprentice) {
  if (action == 'toggle') {
    var assocname = '';
    var list = '';
    var assoctypes = $.evalJSON(Drupal.settings.ndla_ontopia_connect.assoctypes);
    var options = '';

    var counter = 0;
    for (var i in assoctypes) {
      if (i != predikat) {
        list += '<br /><input type="radio" id="toggle_'+(counter)+'" class="toggle_radio" name="toggle_radio" value="'+i+'" />';
        list +=  '<span id="maal_name_toggle_'+(counter++)+'"> '+assoctypes[i].kompetansemaal+'</span>';
        assocname = i;
      }
    }
    
    list += '<br /><input type="button" onclick="topicUpdate(\'dotoggle\',\''+reifier+'\',\'predikat\',\''+timestamp+'\',\''+ressursid+'\',\''+maal+'\',\''+fag+'\',\''+trinn+'\',\''+apprentice+'\')" value="Next" />';
    
    
    $("#toggle_modal_"+reifier).html(list);
    $("#toggle_modal_"+reifier).slideDown(500,function(){});
    
  }
  else if(action == 'dotoggle') {
    var elms = $("#toggle_modal_"+reifier).children().filter('input.toggle_radio');
    var pred = '';
    var assocname = '';
    var spanid = '';
    elms.each(function(){
      if ($(this).is(':checked')){
        pred = $(this).val();
        spanid = $(this).attr('id');
        assocname = $("#maal_name_"+spanid).html();
      }
    });
    var rand_no = (Math.ceil(100*Math.random())+timestamp);
    var newref = 'a_'+timestamp+'_0'+rand_no;
    var assoc = pred+'('+ressursid+' : laeringsressurs, '+maal+' : kompetansemaal) ~ '+newref; 
    var ref = ' ['+newref+' = "reifier for '+newref+'"  /language-neutral ]';
    ref += ' {'+newref+', fag, [['+fag+']]}';
    ref += ' {'+newref+', trinn, [['+trinn+']]}';
    ref += ' {'+newref+', timestamp, [['+timestamp+']]}';
    ref += ' {'+newref+', apprentice, [['+apprentice+']]}';
    var fragment = assoc+ref;
    jasonUrl = Drupal.settings.ndla_ontopia_connect.updatetopic_json_url+"/"+reifier+"/"+pred+"/"+action+"?fragment="+fragment;
  }
  else if (action == 'delete') {
    jasonUrl = Drupal.settings.ndla_ontopia_connect.updatetopic_json_url+"/"+reifier+"/predikat/"+action;
  }
  else if(action == 'de-apprentice'){
    var rand_no = (Math.ceil(100*Math.random())+timestamp);
    var newref = 'a_'+timestamp+'_0'+rand_no;
    var assoc = predikat+'('+ressursid+' : laeringsressurs, '+maal+' : kompetansemaal) ~ '+newref; 
    var ref = ' ['+newref+' = "reifier for '+newref+'"  /language-neutral ]';
    ref += ' {'+newref+', fag, [['+fag+']]}';
    ref += ' {'+newref+', trinn, [['+trinn+']]}';
    ref += ' {'+newref+', timestamp, [['+timestamp+']]}';
    ref += ' {'+newref+', apprentice, [[false]]}';
    var fragment = assoc+ref;
    jasonUrl = Drupal.settings.ndla_ontopia_connect.updatetopic_json_url+"/"+reifier+"/"+predikat+"/"+action+"?fragment="+fragment;
  } else if (action == 'apprentice-ize') {
    var rand_no = (Math.ceil(100*Math.random())+timestamp);
    var newref = 'a_'+timestamp+'_0'+rand_no;
    var assoc = predikat+'('+ressursid+' : laeringsressurs, '+maal+' : kompetansemaal) ~ '+newref; 
    var ref = ' ['+newref+' = "reifier for '+newref+'"  /language-neutral ]';
    ref += ' {'+newref+', fag, [['+fag+']]}';
    ref += ' {'+newref+', trinn, [['+trinn+']]}';
    ref += ' {'+newref+', timestamp, [['+timestamp+']]}';
    ref += ' {'+newref+', apprentice, [[true]]}';
    var fragment = assoc+ref;
    jasonUrl = Drupal.settings.ndla_ontopia_connect.updatetopic_json_url+"/"+reifier+"/"+predikat+"/"+action+"?fragment="+fragment;
  }
  
  if (action == 'dotoggle' || action == 'delete' || action == 'apprentice-ize' || action == 'de-apprentice') {
    //alert(jasonUrl);
    
    $.ajaxSetup({ 
      scriptCharset: "utf-8" , 
      contentType: "application/x-www-form-urlencoded; charset=utf-8"
    });
    
    $.ajax({
      type: 'GET',
      url: jasonUrl,
      dataType: 'text',
      success: function(data) {
        var res = jQuery.evalJSON(data);
        $("#ndla-ontopia-relation-form").prepend('<div class="messages status">' + Drupal.t(res.message) + '</div>');
        updateData(action,reifier,newref,assocname,timestamp,ressursid,maal,fag,trinn,apprentice);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $("#ontopia_connect_errors").html(Drupal.t('<p>An error occured when associating resource to the competence aim. Unable to fetch data.</p>'));
      }
      
      });
  }
}

function hasDisabledChildren(reifier) {
  hasdisabled = false;
  active = 0;
  inactive = 0;
  children = $("#"+reifier).parent().children();
  children.each(function(){
    if (typeof($(this).attr('style')) !== 'undefined') {
      inactive++;
    }
    else {
      active++;
    }
  });
  
  if(active == 1 && inactive >= 1) {
    hasdisabled = true;
  }
  
  return hasdisabled;
}


function updateData(action,reifier,newreifier,name,timestamp,ressursid,maal,fag,trinn,apprentice) {
  if (action == 'delete') {    
    if ($("#"+reifier).parent().children().length == 1) {
      $("#"+reifier).parent().parent().fadeOut();
    }
    else {
      if  (hasDisabledChildren(reifier)) {
        $("#"+reifier).parent().parent().fadeOut();
      }
      else {
        $("#"+reifier).fadeOut();
      }
      
    }
  }
  else if (action == 'dotoggle') {

    elms = $("#relations").children();
    var foundelm = false;
    elms.each(function() {
      var assocname = $(this).children().filter('span.assoctype_title_span').html();
      
      if (trim(name) == trim(assocname)) {
        var ulelm = $(this).children().filter('ul');
        var predikat = ulelm.parent().attr('id');
        var chosenelm = $("#"+reifier);
        var parentelm = chosenelm.parent().parent();
        chosenpred = chosenelm.parent().parent().attr('id');
        nystr = chosenelm.html().replace(chosenpred,predikat);
        var regstr = new RegExp(reifier,"g");
        nystr = nystr.replace(regstr,newreifier)
        chosenelm.html(nystr);
        ulelm.append(chosenelm);
        if(parentelm.children().filter('ul').children().length == 0) {
          parentelm.remove();
        }
        foundelm = true;
        
        
        var assoctypes = $.evalJSON(Drupal.settings.ndla_ontopia_connect.assoctypes);
        
        var list = '';
        var counter = 0;
        for (var i in assoctypes) {
          if (i != predikat) {
            list += '<br /><input type="radio" id="toggle_'+(counter)+'" class="toggle_radio" name="toggle_radio" value="'+i+'" />';
            list +=  '<span id="maal_name_toggle_'+(counter++)+'"> '+assoctypes[i].kompetansemaal+'</span>';
            assocname = i;
          }
        }
        
        list += '<br /><input type="button" onclick="topicUpdate(\'dotoggle\',\''+newreifier+'\',\'predikat\',\''+timestamp+'0101\',\''+ressursid+'\',\''+maal+'\',\''+fag+'\',\''+trinn+'\',\'name\')" value="Next" />';
        
        $("#toggle_modal_"+newreifier).html('');
        $("#toggle_modal_"+newreifier).html(list);
        $("#toggle_modal_"+newreifier).css('display','none');
        
        return false;
      }
      
      
     
      
    });
    
    if(!foundelm) {
      var preds = new Array();
      $("#relations").children().filter('li.assoctype_title').each(function() {
        preds.push($(this).attr('id'));
      });
      
      var newpred = '';
      
      var assoctypes = $.evalJSON(Drupal.settings.ndla_ontopia_connect.assoctypes);
      var assocname = '';
      var list = '';
      var counter2 = 0;
      for (var i in assoctypes) {
        if (array_has_value(preds,trim(i))) {
          list += '<br /><input type="radio" id="toggle_'+(counter2)+'" class="toggle_radio" name="toggle_radio" value="'+i+'" />';
          list +=  '<span id="maal_name_toggle_'+(counter2++)+'"> '+assoctypes[i].kompetansemaal+'</span>';
        }
        else {
          newpred = trim(i);
        }
      }
      
      
      list += '<br /><input type="button" onclick="topicUpdate(\'dotoggle\',\''+newreifier+'\',\'predikat\',\''+timestamp+'0101\',\''+ressursid+'\',\''+maal+'\',\''+fag+'\',\''+trinn+'\',\'name\')" value="Next" />';
      
      var chosenelm = $("#"+reifier);
      chosenpred = chosenelm.parent().parent().attr('id');
      var parentelm = chosenelm.parent().parent();
      nystr = chosenelm.html().replace(chosenpred,newpred);
      var regstr = new RegExp(reifier,"g");
      nystr = nystr.replace(regstr,newreifier);
      var newelm = '<li id="'+newpred+'" class="assoctype_title ui-tabs-disabled"><span class="assoctype_title_span">'+name+'</span><ul><li id="'+newreifier+'" class="aim_title ui-tabs-disabled">'+nystr+'</li></ul></li>';
  
      $("#"+reifier).remove();
      $("#relations").append(newelm);
      if(parentelm.children().filter('ul').children().length == 0) {
        parentelm.remove();
      }
      $("#toggle_modal_"+newreifier).html('');
      $("#toggle_modal_"+newreifier).html(list);
      $("#toggle_modal_"+newreifier).css('display','none');
    }
  }
}

function addData(ressurstype,ressursurl,ressursnavn,assoctype, maaluid, laerling, data, fag, timestamp,status,count) {
  var fragment = '';
  var topicid = '';
  var associd = '';
  var maaltopic = '';
  var udircode = '';
  
  
  if(maaluid.indexOf('kompetansemaal') > -1) {
	udircode = maaluid;
  }
    
    var duplicate_name = $(':input[name=resource_name-duplicate]')[0].value;
    var duplicate_topicid = $(':input[name=resource_name-duplicate-topicid]')[0].value;
    var duplicate_topictype = $(':input[name=resource_name-duplicate-type]')[0].value;
    var user_name = $(':input[name=grep-user-name]')[0].value;
    var published = $(':input[name=grep-published]')[0].value;
    var node_metadata = $.evalJSON($(':input[name=resource_meta]')[0].value);
    
    var node_id = ressursurl.substring(ressursurl.lastIndexOf('/')+1); 
    
    if ($(':input[name=grep-user-uid]')[0].value != '') {
      var user_url = Drupal.settings.ndla_ontopia_connect.base_url+'/user/'+$(':input[name=grep-user-name]')[0].value;
    }
    
  
  if (data.values[0].maalid == '') {
    maaltopic = 'kt_'+timestamp+'_'+count;
    var kompetansetopic = ' ['+maaltopic+' : kompetansemaal = "'+maaluid+'" /language-neutral @"'+maaluid+'"]';
  }
  else {
    if(data.values[0].duplikatassoc == 'true'){
      alert(Drupal.t('The relation between the competence aim '+data.values[0].maalname+' and '+ressursurl+' already exists with the relation '+assoctype));
      $(':input[name=maal]').each(function(){
        if($(this).is(":checked")) {
          var maalid = $(this).attr('id');
          var assocnum = 'assoc'+maalid.substr(maalid.indexOf('_'));
          var spannum = 'maaltext'+maalid.substr(maalid.indexOf('_'));
          if($('#'+spannum).html() == data.values[0].maalname) {
            $('#'+assocnum).val('');
          }
        }
      });
      return false;
    }
    else {
      maaltopic = data.values[0].maalid;
    }
    
  }
  
  if (ressurstype == 'ndla-node') {
    fag_topicid = $(':input[name=chosen-topicid-hidden]')[0].value;;
  }
  
  if (duplicate_name != '') {
    if(duplicate_name != ressursnavn) {
      
      if ($(':input[name=ressurs-ltm]')[0].value == '') {
      //fagskop i tilfelle duplikat ressurs
        var fagskop = '';
        if ($(":input[name=resource_title_scope]") != null) {
          fagskop = $(":input[name=resource_title_scope]")[0].value;
        }
        var resource = ' ['+duplicate_topicid+' : '+duplicate_topictype+' = "||'+ressursnavn+'||" /'+fagskop+']';
        var assoc = ' '+assoctype+'('+duplicate_topicid+' : laeringsressurs, '+maaluid+' : kompetansemaal) ~ a_'+timestamp+'_'+count;
        $(':input[name=ressurs-ltm]')[0].value = resource;
      }
      else {
        resource = $(':input[name=ressurs-ltm]')[0].value;
        resarr = resource.split(':');
        topicid = trim(resarr[0].replace('[',''));
        var assoc = ' '+assoctype+'('+topicid+' : laeringsressurs, '+maaluid+' : kompetansemaal) ~ a_'+timestamp+'_'+count;
      }
    }
    else {
        var assoc = ' '+assoctype+'('+duplicate_topicid+' : laeringsressurs, '+maaluid+' : kompetansemaal) ~ a_'+timestamp+'_'+count;
    }
  }
  else {
    var resource = '';
   
     // alert("TOPICID: "+ fagmap.topicid);
    var node_topicid = '';
    var langscope = node_metadata.language+" language-neutral";
    if (ressurstype == 'ndla-node' || ressurstype == 'deling-node') {
	    
	   	if(ressurstype == 'ndla-node'){
	   		 node_topicid = 'ndlanode_'+node_id;
	   	}
	   	else{
	   		node_topicid = 'delingnode_'+node_id;
	   	}
    
	   
	   
      if ($(':input[name=ressurs-ltm]')[0].value == '') {
         if (typeof(fagmap) != 'undefined') {
           resource += ' ['+node_topicid+' : '+ressurstype+' = "||'+ressursnavn+'||"  /'+langscope+' '+fag_topicid+' @"psi_'+ressurstype+'"]';
         }
         else {
           resource += ' ['+node_topicid+' : '+ressurstype+' = "||'+ressursnavn+'||"  /'+langscope+'  @"psi_'+ressurstype+'"]';
         }
        
        
        if(published != ''){
          resource += ' {'+node_topicid+',published, [['+published+']]}';
        }
        
        if(node_metadata.ingress != '' && node_metadata.ingress != "undefined") {
        	resource += ' {'+node_topicid+',node-ingress, [['+node_metadata.ingress+']]}';	
        }
        
        if(typeof(node_metadata.authors) != "undefined" &&  node_metadata.authors.length > 0) {
        	for(var ai = 0; ai < node_metadata.authors.length; ai++) {
        		resource += ' {'+node_topicid+',node-author, [['+node_metadata.authors[ai]+']]}';
        	}
        }
        
        var site_url = '';
        if (ressurstype == 'ndla-node'){
        	site_url = 'http://ndla.no/user/';
        }
        else if (ressurstype == 'deling-node') {
        	site_url = 'http://deling.ndla.no/user/';
        }
        
        if(node_metadata.user_id != '' && node_metadata.user_id != 'undefined') {
        	resource += ' {'+node_topicid+',node-author-url, "'+site_url+node_metadata.user_id+'"}';
        }
        
        if(node_metadata.license != '' && node_metadata.license != 'undefined') {
        	resource += ' {'+node_topicid+',node-usufruct, "http://creativecommons.org/licenses/'+node_metadata.license+'"} /en';
        	resource += ' {'+node_topicid+',node-usufruct, "http://creativecommons.org/licenses/'+node_metadata.license+'/no"} /nb';
        }
        var assoc = ' '+assoctype+'('+node_topicid+' : laeringsressurs, '+maaluid+' : kompetansemaal) ~ a_'+timestamp+'_'+count;
        $(':input[name=ressurs-ltm]')[0].value = resource;
      }
      else {
        resource = $(':input[name=ressurs-ltm]')[0].value;
        resarr = resource.split(':');
        topicid = trim(resarr[0].replace('[',''));
        var assoc = ' '+assoctype+'('+topicid+' : laeringsressurs, '+maaluid+' : kompetansemaal) ~ a_'+timestamp+'_'+count;
      }
      
    }
    else {
      if ($(':input[name=ressurs-ltm]')[0].value == '') {
        if (typeof(fagmap) != 'undefined') {
          resource += ' ['+node_topicid+' : '+ressurstype+' = "||'+ressursnavn+'||"  /'+langscope+' '+fag_topicid+' @"'+ressursurl+'"]';
        }
        else {
          resource += ' ['+node_topicid+' : '+ressurstype+' = "||'+ressursnavn+'||"  /'+langscope+'  @"'+ressursurl+'"]';
        }
        
        
	    if(published != ''){
	        resource += ' {'+node_topicid+',published, [['+published+']]}';
	      }
	      
	      if(node_metadata.ingress != '' && node_metadata.ingress != "undefined") {
	      	resource += ' {'+node_topicid+',node-ingress, [['+node_metadata.ingress+']]}';	
	      }
	      
	      if(node_metadata.authors.length > 0) {
	      	for(var ai = 0; ai < node_metadata.authors.length; ai++) {
	      		resource += ' {'+node_topicid+',node-author, [['+node_metadata.authors[ai]+']]}';
	      	}
	      }
	      
	      var site_url = '';
	      if (ressurstype == 'ndla-node'){
	      	site_url = 'http://ndla.no/user/';
	      }
	      else if (ressurstype == 'deling-node') {
	      	site_url = 'http://deling.ndla.no/user/';
	      }
	      
	      if(node_metadata.user_id != '' && node_metadata.user_id != 'undefined') {
	      	resource += ' {'+node_topicid+',node-author-url, "'+site_url+node_metadata.user_id+'"}';
	      }
	      
	      if(node_metadata.license != '' && node_metadata.license != 'undefined') {
	      	resource += ' {'+node_topicid+',node-usufruct, "http://creativecommons.org/licenses/'+node_metadata.license+'"} /en';
	      	resource += ' {'+node_topicid+',node-usufruct, "http://creativecommons.org/licenses/'+node_metadata.license+'/no"} /nb';
	      }

        var site_url = '';
        if (ressurstype == 'ndla-node'){
        	site_url = 'http://ndla.no/user/';
        }
        else if (ressurstype == 'deling-node') {
        	site_url = 'http://deling.ndla.no/user/';
        }
        
        
        var assoc = ' '+assoctype+'('+node_topicid+' : laeringsressurs, '+maaluid+' : kompetansemaal) ~ a_'+timestamp+'_'+count;
        $(':input[name=ressurs-ltm]')[0].value = resource;
      }
      else {
        resource = $(':input[name=ressurs-ltm]')[0].value;
        resarr = resource.split(':');
        topicid = trim(resarr[0].replace('[',''));
        var assoc = ' '+assoctype+'('+topicid+' : laeringsressurs, '+maaluid+' : kompetansemaal) ~ a_'+timestamp+'_'+count;
      }
      
    }
    
    
  }
  
  
  var reifier = ' [a_'+timestamp+'_'+count+' = " reifier for a_'+timestamp+'_'+count+'" /language-neutral ]';
  reifier += ' {a_'+timestamp+'_'+count+', fag, [['+fag+']]}';
  reifier += ' {a_'+timestamp+'_'+count+', timestamp, [['+timestamp+']]}';
  reifier += ' {a_'+timestamp+'_'+count+', apprentice, [['+laerling+']]}';
  
  if (user_name != '') {
    reifier += ' {a_'+timestamp+'_'+count+', bruker-navn, [['+user_name+']]}';
  }
  
  if (user_url != '') {
    reifier += ' {a_'+timestamp+'_'+count+', bruker-url, "'+user_url+'"}';
  }
  
  
  
  if (data.values[0].maalid == '') {
    fragment += kompetansetopic+assoc+reifier;
  }
  else {
    fragment += assoc+reifier;
  }
  
  if (status == 'continue') {
    $(':input[name=ltm]')[0].value += '|'+fragment;
    
    $(':input[name=ucodes]')[0].value += '|'+udircode;
    
  }
  else if (status == 'save') {
  
    if ($(':input[name=ltm]')[0].value != '') {
      var ltm = $(':input[name=ltm]')[0].value;
      var ltmdata = ltm.split('|');
      fragment = fragment += ltmdata.join(''); 
      
    }
    
    resource = $(':input[name=ressurs-ltm]')[0].value;
    ucodes = $(':input[name=ucodes]')[0].value+'|'+udircode;
    
    var dropresource = false;
    if(data.values[0].maalid != '' && duplicate_name != '') {
      if (duplicate_name == ressursnavn) {
        var navn = $(':input[name=resource_name]')[0].value;
        var all_names =  $(':input[name=resource_name-all]')[0].value;
        ascopes = new Array();
        var json = $.evalJSON(all_names);
        
        if(json.values[0].scoped_names != null) {
          scopes = json.values[0].scoped_names;   
            for (var i = 0; i < scopes.length;i++) {
              if (scopes[i] != null){
                ascopes.push(scopes[i].scope);
              }   
            }
        }
        
        if(array_has_value(ascopes,fag)) {
          dropresource = true;
        }
      }
    }
    else if (data.values[0].maalid == '' && duplicate_name != ''){
      if (duplicate_name == ressursnavn) {
        var navn = $(':input[name=resource_name]')[0].value;
        var all_names =  $(':input[name=resource_name-all]')[0].value;
        ascopes = new Array();
        var json = $.evalJSON(all_names);
        if(json.values[0].scoped_names != null) {
          scopes = json.values[0].scoped_names;             
            for (var i = 0; i < scopes.length;i++) {
              if (scopes[i] != null){
                ascopes.push(scopes[i].scope);
              }
            } 
        }
        
        if(array_has_value(ascopes,fag)) {
          dropresource = true;
        }
      }
    }//end if mmalid && duplicate_name
    
    if (!dropresource) {
      fragment = resource+fragment;
    }
    
    jasonUrl = Drupal.settings.ndla_ontopia_connect.fragment_json_url+"/foo/?fragment="+'@"utf-8" '+fragment;
  
    if (ressurstype != 'ndla-node' && ressurstype != 'deling-node') {
      $.ajaxSetup({ 
        scriptCharset: "utf-8" , 
        contentType: "application/x-www-form-urlencoded; charset=utf-8"
      });

      $.ajax({
      type: 'GET',
      url: jasonUrl,
      dataType: 'text',
      contentType: "application/x-www-form-urlencoded; charset=utf-8",
      success: function(data) {
        $(":input[name=resource_title_scope]")[0].value = '';
        $(':input[name=resource_name-duplicate]')[0].value = '';
        $(':input[name=resource_name-duplicate-topicid]')[0].value = '';
        $(':input[name=resource_name-duplicate-type]')[0].value = '';
        $("a#bottomNavClose").click();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $("#ontopia_connect_addressurs").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
      }
      
      });
    }
    else {
      $(':input[name=grep_area]')[0].value = fragment;
      $(':input[name=grep_area_udir_codes]')[0].value = ucodes;
      
      $("#grep_save_message").html(Drupal.t('Press the "Save" button to save the relations'));
      $("a#bottomNavClose").click();
    }

  }
  
}


function sjekkData(ressurstype,ressursurl,ressursnavn,assoctype, maal, laerling, fag, timestamp,status,count) {
  
  if ((ressurstype == 'ndla-node' && ressursurl.substr(ressursurl.lastIndexOf('/')+1) == 'competence_aims') || (ressurstype == 'deling-node' && ressursurl.substr(ressursurl.lastIndexOf('/')+1) == 'competence_aims')) {
    ressursurl = ressursurl.substr(0,ressursurl.lastIndexOf('/'));
    ressursurl = ressursurl.replace('en/','');
    ressursurl = ressursurl.replace('nb/','');
    ressursurl = ressursurl.replace('nn/','');
    ressursurl = ressursurl.replace('/competence_aims','');
  }
  
  if(ressursurl.indexOf('/menu') > -1) {
    ressursurl = ressursurl.substr(0,ressursurl.indexOf('/menu'));
  }
  
  jasonUrl = Drupal.settings.ndla_ontopia_connect.sjekkdata_json_url+"/"+maal+"/kompetansemaal/"+assoctype+"?url="+ressursurl;
  //alert(jasonUrl);
   
  $.ajaxSetup({ 
    scriptCharset: "utf-8" , 
    contentType: "application/x-www-form-urlencoded; charset=utf-8"
  });

  var asyncstatus = true;
  if (ressurstype == 'ndla-node' || ressurstype == 'deling-node') {
    asyncstatus = false;
  }

  $.ajax({
  type: 'GET',
  url: jasonUrl,
  async: asyncstatus,
  dataType: 'json',
  contentType: "application/x-www-form-urlencoded; charset=utf-8",
  success: function(data) {
    addData(ressurstype,ressursurl,ressursnavn,assoctype, maal, laerling, data, fag, timestamp,status,count);
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    $("#ontopia_connect_errors").html(Drupal.t('<p>An error occured when associating resource to the competence aim. Unable to fetch data.</p>'));
  }
  
  });
}

function sjekkForDuplikatRessurs(type, ressursurl){
  if ((type == 'ndla-node' && ressursurl.substr(ressursurl.lastIndexOf('/')+1) == 'competence_aims') || (type == 'deling-node' && ressursurl.substr(ressursurl.lastIndexOf('/')+1) == 'competence_aims')) {
    
    ressursurl = ressursurl.substr(0,ressursurl.lastIndexOf('/'));
    ressursurl = ressursurl.replace('en/','');
    ressursurl = ressursurl.replace('nb/','');
    ressursurl = ressursurl.replace('nn/','');
    ressursurl = ressursurl.replace('/competence_aims','');
    
    if(ressursurl.indexOf('/menu') > -1) {
      ressursurl = ressursurl.substr(0,ressursurl.indexOf('/menu'));
    }
  }
  
    $.ajaxSetup({ 
      scriptCharset: "utf-8" , 
      contentType: "application/x-www-form-urlencoded; charset=utf-8"
    });
    jasonUrl = Drupal.settings.ndla_ontopia_connect.sjekkdata_json_url+"/ressurs/ressurs/ressurs/?ressurs="+ressursurl+"";
    $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    contentType: "application/x-www-form-urlencoded; charset=utf-8",
    success: function(data) {
      setTittel(ressursurl,data);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_errors").html(Drupal.t('<p>An error occured when associating resource to the competence aim. Unable to fetch data.</p>'));
    }
    
    });
  
}

function processFagData(jsondata,nid) {
  var hoved_output = '';
  var maal_output = '';
  var base = Drupal.settings.ndla_ontopia_connect.base_url;
  var groupid = $(':input[name=group-hidden]')[0].value;
  var maaldata;
  scopes = new Array();
  var trinn = ' ';
  var assoc_trinn_opts = '<option value="">Educational level</option>';
  var fag_uuid= '';
  for (var i = 0; i< jsondata.values.length; i++) {
    var fagdata = jsondata.values[i];
    fag_uuid = fagdata['hovedomraade_fag'];
    if (i == 0) {
      hoved_output += '<li class="grep_first_level selected expanded"><a href="javascript:" id="'+fagdata['hovedomraade_uid'].replace(':','')+'" onclick="getKompetanseMaal(\''+fagdata['hovedomraade_uid']+'\',\'browse-kompetanse\',\''+nid+'\',\''+fagdata['hovedomraade_fag']+'\');return false"><span>'+fagdata['hovedomraade_tittel']+'</span></a></li>';
      maaldata = fagdata['maaldata'];
    }
    else {
      hoved_output += '<li class="grep_first_level"><a href="javascript:" id="'+fagdata['hovedomraade_uid'].replace(':','')+'" onclick="getKompetanseMaal(\''+fagdata['hovedomraade_uid']+'\',\'browse-kompetanse\',\''+nid+'\',\''+fagdata['hovedomraade_fag']+'\');return false"><span>'+fagdata['hovedomraade_tittel']+'</span></a></li>';
    }
    
     
  }
  
  for(var j=0; j<maaldata.length;j++) {
    //maal_output += "<li><a id='"+maaldata[j].uuid+"' href=\""+base+"/node/"+groupid+"/grep/"+maaldata[j].uuid+"\">"+maaldata[j].uuid_title+"</a> ("+maaldata[j].scope_title+")</li>";
    maal_output += "<li><a id='"+maaldata[j].uuid+"' href=\""+base+"/node/"+groupid+"/grep/"+maaldata[j].uuid+"\"  class=\"kompetansemaal\">"+maaldata[j].uuid_title+"</a></li>";
    scopes[maaldata[j].scope] = maaldata[j].scope_title;
  }
  
  $(':input[name=kompetansemaal-hidden]')[0].value = '{"values": '+JSON.stringify(maaldata)+'}';;
  
  
  
  scopes = sortAssoc(scopes);
  counter = 1;
  var trinnjson = '{"trinn": [';
  
  for( scope in scopes ) {
    //trinn += '<div id="trinn_'+counter+'" class="filterdiv" onclick="filtrerMaal(\''+scope+'\',\''+counter+'\',\'\')"><span id="trinnspan_'+(counter++)+'" class="trinnspan_av">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+scopes[scope]+"</div>";
    trinn += ' <input type="checkbox" id="trinnspan_'+counter+'" class="trinnspan_paa" checked="checked" onclick="filtrerMaal(\''+scope+'\',\''+counter+'\',\'\')" /> '+scopes[scope];
    assoc_trinn_opts += '<option value="'+scope+'">'+scopes[scope]+'</option>';
    trinnjson += '{"trinnid" : "'+(counter++)+'", "psi" : "'+scope+'"},';
  }
  
  trinnjson = trinnjson.substr(0,trinnjson.lastIndexOf(','));
  trinnjson += ']}';
  setTrinnJson(trinnjson);
  
  var yrkes_json = $(':input[name=kompetansemaal-yrkesfag-trinn-hidden]')[0].value;
  if(yrkes_json != "") {
    var json = $.evalJSON(yrkes_json);
    var data = json[nid];
    var yrkestrinn = '';
    
    $.each(data, function(key,val){
      filter_data = val.filter_data;
      fag = false;
      $.each(filter_data, function(fkey,fval){
        if(fkey == fag_uuid){
          fag = true;
        }
      }); 
      if(fag) {
      //yrkestrinn += '<div id="trinn_'+counter+'" class="filterdiv yrkefilter" onclick="filtrerYrkesMaal(\''+counter+'\',\''+key+'\',\''+fag_uuid+'\',\'paa\')">';
      //yrkestrinn += '<span id="trinnspan_'+(counter++)+'" class="trinnspan_av">&nbsp;&nbsp;&nbsp;&nbsp; </span>'+val.filter_name+'</div>';
        yrkestrinn = ' <span class="yrkefilter_title"><input type="checkbox" id="trinnspan_'+counter+'" class="trinnspan_av yrkefilter" onclick="filtrerYrkesMaal(\''+(counter++)+'\',\''+key+'\',\''+fag_uuid+'\',\'paa\')" /> '+val.filter_name+'</span>';
      }
       
    });
    
  
    if (getScopeLength(scopes) > 1) {
      if (yrkestrinn != ''){
        trinn += yrkestrinn;
      }
      $('.yrkefilter_title').children().filter('.yrkefilter').before('<span>&nbsp;</span>');
      $('#trinn-wrapper').html(trinn);
      $('#trinn-wrapper').slideDown(500,function(){});
    }
    else {
       if (yrkestrinn != ''){
        trinn = yrkestrinn;
       }
       $('.yrkefilter_title').children().filter('.yrkefilter').before('<span>&nbsp;</span>');
       $('#trinn-wrapper').html(trinn);
       $('#trinn-wrapper').slideDown(500,function(){});
    }

  }
  $(':input[name=assoc_trinn]').html(assoc_trinn_opts);
  
  hoved_output = '<ul>'+hoved_output+'</ul>';
  maal_output = '<ul>'+maal_output+'</ul>';
  $('#grep-results-hovedomraader').html(hoved_output);
  $('#grep-results-kompetansemaal').html(maal_output);
  Drupal.attachBehaviors($('#grep-results-kompetansemaal'));
  addClickListeners();
  grep_resizeContainer();
}

function processData(data,id,add,fag_psi) {
  var str = '';
  var output = '';
  var trinn = '';
  var assoc_trinn_opts = '<option value="">Educational level</option>';
  var language = Drupal.settings.ndla_ontopia_connect.language;
  if (!add){
    output += '<ul>';
  }
  scopes = new Array();
  
  
  if (id == 'grep-results-kompetansemaal-light' && add) {
    jstr = Drupal.settings.ndla_ontopia_connect.assoctypes;
    jstr = jstr.replace(/\\/g,'');
    var assoctypes = $.evalJSON(jstr);
    var lang = $(':input[name=grep-language]')[0].value;
    var options = '';
    for (var i in assoctypes) {
        options += '<option value="'+i+'">'+assoctypes[i].kompetansemaal+'/'+assoctypes[i].laeringsressurs+'</option>';
    }
    
  }
  
  
  if (id == 'grep-results-kompetansemaal'){
    var base = Drupal.settings.ndla_ontopia_connect.base_url;
    var groupid = $(':input[name=group-hidden]')[0].value;
  }
  
  for (var i = 0; i< data.values.length; i++) {
    if(typeof(data.values[i]) !== 'undefined'){
      if (id == 'grep-results-hovedomraader-light') {
        output += "<li><a id='"+data.values[i].uuid.replace(':','')+"' href=\"javascript:\" onclick=\"getKompetanseMaal('"+data.values[i].uuid+"','light-kompetanse','','"+fag_psi+"')\">"+data.values[i].uuid_title+"</a></li>";
      }
      else if(id == 'grep-results-hovedomraader') {
        output += "<li><a href=\"javascript:\" onclick=\"getGrepData('kompetansemaal','"+data.values[i].uuid+"','browse')\">"+data.values[i].uuid_title+"</a></li>";
      }
      else if (id == 'grep-results-kompetansemaal-light') {
        if (add) {       
          output += "<input id=\"maal_"+i+"\" type=\"checkbox\" value=\""+data.values[i].uuid+"\" name=\"maal\" onclick=\"settTrinn(\'"+data.values[i].scope+"\')\" />&nbsp;<span id=\"maaltext_"+i+"\">"+data.values[i].uuid_title+"</span> ("+data.values[i].scope_title+") <br />";
          output += "<select id=\"assoc_"+i+"\" class=\"form-select\" name=\"assoc_"+i+"\"><option selected=\"selected\" value=\"\">Relation type</option>"+options+"</select><br />";
        }
        scopes[data.values[i].scope] = data.values[i].scope_title; 
      }
      else if (id == 'grep-results-kompetansemaal'){
        output += "<li><a id='"+data.values[i].uuid+"' href=\""+base+"/"+language+"/node/"+groupid+"/grep/"+data.values[i].uuid+"\" class=\"kompetansemaal\">"+data.values[i].uuid_title+"</a></li>";
        //output += "<li><a id='"+data.values[i].uuid+"' href=\""+base+"/node/"+groupid+"/grep/"+data.values[i].uuid+"\">"+data.values[i].uuid_title+"</a> ("+data.values[i].scope_title+")</li>";
        scopes[data.values[i].scope] = data.values[i].scope_title;
      }
    }
  }
    
  if (!add){
    output += '</ul>';
  }
  
 
  if (id == 'grep-results-kompetansemaal-light' || id == 'grep-results-kompetansemaal') {
    str = JSON.stringify(data);
    $(':input[name=kompetansemaal-hidden]')[0].value = str;
    
    
    scopes = sortAssoc(scopes);
    counter = 1;
    var trinnjson = '{"trinn": [';
    
    for( scope in scopes ) {
       if (add){
         //trinn += '<div id="trinn_'+counter+'" class="filterdiv" onclick="filtrerMaal(\''+scope+'\',\''+counter+'\',\'add\')"><span id="trinnspan_'+(counter++)+'" class="trinnspan_av">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+scopes[scope]+"</div>";
         trinn += '<div id="trinn_'+counter+'" class="filterdiv" onclick="filtrerMaal(\''+scope+'\',\''+counter+'\',\'add\')"><span id="trinnspan_'+counter+'" class="trinnspan_paa" checked="checked">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+scopes[scope]+"</div>";
         trinnjson += '{"trinnid" : "'+(counter++)+'", "psi" : "'+scope+'"},';
       }
       else {
         //trinn += '<div id="trinn_'+counter+'" class="filterdiv" onclick="filtrerMaal(\''+scope+'\',\''+counter+'\',\'\')"><span id="trinnspan_'+(counter++)+'" class="trinnspan_av">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+scopes[scope]+"</div>";
         trinn += ' <input type="checkbox" id="trinnspan_'+counter+'" class="trinnspan_paa" onclick="filtrerMaal(\''+scope+'\',\''+counter+'\',\'\')" checked="checked" /> '+scopes[scope];
         trinnjson += '{"trinnid" : "'+(counter++)+'", "psi" : "'+scope+'"},';
       }
       
       assoc_trinn_opts += '<option value="'+scope+'">'+scopes[scope]+'</option>';
    }
    
    trinnjson = trinnjson.substr(0,trinnjson.lastIndexOf(','));
    trinnjson += ']}';
    setTrinnJson(trinnjson);

    if (getScopeLength(scopes) > 1) {
      if ($('.yrkefilter_title').length > 0){
        var yrkefilters = $('.yrkefilter_title').parent().contents().filter('.yrkefilter_title');
        $('.yrkefilter_title').children().filter('.yrkefilter').before('<span>&nbsp;</span>');
      }
      $('#trinn-wrapper').html(trinn);
      $('#trinn-wrapper').append(yrkefilters);
      $('#trinn-wrapper').slideDown(500,function(){});
    }
    else {
      if ($('.yrkefilter_title').length > 0){
        var yrkefilters = $('.yrkefilter_title').parent().contents().filter('.yrkefilter_title');
        $('.yrkefilter_title').children().filter('.yrkefilter').before('<span>&nbsp;</span>');
        $('#trinn-wrapper').append(yrkefilters);
        $('#trinn-wrapper').slideDown(500,function(){});
      }
      
    }
    $(':input[name=assoc_trinn]').html(assoc_trinn_opts);
    
  }
  $('#'+id+'').html(output);
  Drupal.attachBehaviors($('#'+id+''));
  
  if (id == 'grep-results-kompetansemaal') {
    grep_resizeContainer();
  }
  
}

function visMaalInfo(counter,type) {
  $('#ul_'+type+'_maal_'+counter).children().each(function(){
 
    if($(this).hasClass(type+'_maalinfo_av')){
      $(this).removeClass(type+'_maalinfo_av');
      $(this).addClass(type+'_maalinfo_paa');
      $('#ul_'+type+'_maal_'+counter).removeClass(type+'_maalinfo_av');
      $('#ul_'+type+'_maal_'+counter).addClass(type+'_maalinfo_paa');
      $('#'+type+'_toggle_'+counter).html('[-]');
      $('#'+type+'maal_'+counter).slideDown(500,function(){});
    }
    else {
      $(this).removeClass(type+'_maalinfo_paa');
      $(this).addClass(type+'_maalinfo_av');
      $('#ul_'+type+'_maal_'+counter).removeClass(type+'_maalinfo_paa');
      $('#ul_'+type+'_maal_'+counter).addClass(type+'_maalinfo_av');
      $('#'+type+'_toggle_'+counter).html('[+]');
      $('#'+type+'_maal_'+counter).slideUp(500,function(){});
    }
  });
}


function processResource(data) {
  var length = data.values.length;
  var output = '<ul>';
  if (length > 0) {
    for (var i = 0; i < length; i++) {
      output += '<li><span class="assoctype">'+data.values[i].assoctittel+'</span><ul>';
      ressursdata = data.values[i].ressursdata;
      for (var j = 0; j < ressursdata.length; j++) {
        output += '<li><a href="'+ressursdata[j].ressurs_topicid+'">'+ressursdata[j].ressurs_tittel+'</a> <a href="'+ressursdata[j].ressurs_url+'"><img src="/ndla/sites/all/modules/ndla_ontopia_connect/img/gaDirektePil.gif" /></a> ('+ressursdata[j].ressurs_type+')</li>';
      }
      output += '</ul></li>';
    }
  }
  else {
   output += '<li>Dette kompetansemålet hadde ingen ressurser tilknyttet</li>'; 
  }
  
  
  $('#grep-results-ressurser-light').html(output);
}


function processAims(data, div_id) {
  tView = new treeview(data,div_id, $('#add_curriculum_div_'+div_id));
  $('#curriculum_info_link_'+div_id).removeAttr('class');
  $('#add_curriculum_div_'+div_id).slideDown(500,function(){});
  $('#add_curriculum_button_'+div_id).attr('class','hidden');
  $('#save_filter_button_'+div_id).slideDown(500,function(){});
}


function processTreeAims(data,div_id){
  $('#curriculum_tree').children().remove();
  tView = new curriculatree(data, $('#curriculum_tree'),'curriculum_data');
}

function processAimResources(data,div_id,modus,subject,func) {
	//alert(" DIVID: "+div_id+" MODUS: "+modus+" SUBJECT: "+subject+" FUNC: "+func);
  if (modus == 'plan') {
    if(func == 'port'){
      tView = new port_treeview(data,div_id, $('#alter_assocs_div_'+div_id),modus,subject);
      $('#alter_assocs_div_'+div_id).slideDown(500,function(){});
      $('#fetch_nodes_button_button_'+div_id).attr('class','ochidden');
      $('#process_resource_button_'+div_id).slideDown(500,function(){});
    }
    else if(func == 'newport'){
      tView = new port_treeview(data,div_id, $('#relate_assocs_div_'+div_id),modus,subject);
      $('#ressurs_'+div_id).attr('checked','checked');
      $(".aim_window_loader_white_nb").remove();
      $('#relate_'+div_id).slideDown(500,function(){});
    }
    else {
      tView = new batch_treeview(data,div_id, $('#delete_assocs_div_'+div_id),modus);
      $('#delete_assocs_div_'+div_id).slideDown(500,function(){});
      $('#fetch_nodes_button_button_'+div_id).attr('class','ochidden');
      $('#process_resource_button_'+div_id).slideDown(500,function(){});  
    }
    
  }
  else {
    if (func == 'port') {
      var count = data.values.length;
      if (count > 0) {
        var relasjonstype_contents = $('<div class="NodeContents"></div>');
        for (var i = 0; i < count; i++) {
          var relasjonstype_node = $('<div id="'+data.values[i].relasjon+'-RelasjonstypeNode">'+data.values[i].relasjon+'</div>')
          .css({'margin-top': 5});
          $('<div class="expandNode expand"></div>').prependTo(relasjonstype_node);

          var ressurs_contents = $('<div class="NodeContents"></div>');

          var ressursdata = data.values[i].ressursdata;
          var rcount = ressursdata.length;
          for (var j = 0; j < rcount; j++) {
            var output = '';
            var ressurs = ressursdata[j];
            var fagval = '';
            if(typeof $(".fag_nid_"+div_id)[0] != 'undefined'){
              ftemp = $(".fag_nid_"+div_id)[0].value;
              farr = ftemp.split("_");
              fagval = farr[1];
            }
            
            var nodeurl = ressurs.ressurs_url;
            var nodetitle = ressurs.ressurs_tittel;

            if(ressurs.ressurs_url.indexOf("node") == -1) {
            	nodeurl = nodeurl+"/node/"+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length);
            }
            
            if(ressurs.ressurs_tittel == "") {
            	nodetitle = "NODE: "+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length);
            }
            
            output += '<div class="ItemTxt"><input type="hidden" id="ressurs_topicid_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'" value="'+ressurs.ressurs_topicid+'" /><input id="ressurs_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'" type="checkbox" value="'+ressurs.ressurs_reifierid+'" class="ressurs-checkbox" /> <span id="batch_resource_title_span_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'" class="batch_resource_title_span">'+nodetitle+'</a> (<a href="javascript:" onclick="$(\'#info_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'\').css(\'display\',\'block\')">'+Drupal.t('Information')+'</a>) (<a href="javascript:" onclick="getCurriculaResourcesBySubject(\''+fagval+'\',\''+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'\',\'plan\',\'newport\');return false">'+Drupal.t('Change relations')+'</a>)';
            
            output += '<div id="info_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'" class="grep-ressurs-nodeinfo ochidden">';
            output += '<p><strong>'+Drupal.t('Type')+'</strong>: '+ressurs.ressurs_type+'</p>';
           
            
            output += '<p><strong>'+Drupal.t('Node')+'</strong>: <a href="'+nodeurl+'" target="_blank">'+nodeurl+'</a></p>';
            output += '<p><strong>'+Drupal.t('Relation created by')+'</strong>: <a href="'+ressurs.ressurs_bruker_url+'" id="userdata_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'">'+ressurs.ressurs_bruker_navn+'</a></p>';
            output += '<div class="batch_close_butt" onclick="$(\'#info_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'\').css(\'display\',\'none\')"></div><div class="clear" />';
            output += '</div>';
            
            output += '<div id="relate_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'" class="grep-ressurs-relate ochidden">';
            output += '<div id="relate_assocs_div_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'"><div class="new_curriculum_title">'+Drupal.t('New Curriculum')+'</div></div>';
            output += '<div class="batch_close_butt" onclick="$(\'#relate_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'\').css(\'display\',\'none\');$(\'#ressurs_'+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length)+'\').removeAttr(\'checked\')"></div><div class="clear" />';
            output += '</div>';
            
            output += '</div>';
            $('<div class="NodeItem"></div>').html(output).appendTo(ressurs_contents);
          }
          relasjonstype_node.append(ressurs_contents);
          relasjonstype_contents.append(relasjonstype_node);

          relasjonstype_node.children('.expandNode').click(function() {
            var contents = $(this).parent().children(".NodeContents");
            contents.toggle();
            if(contents.css('display') != "none")
               {
             $(this).attr("class", "expandNode collapse");
            }
            else {
              $(this).attr("class", "expandNode expand");
            }
          });
        }//end for relations

        $(".aim_window_loader_white_nb").remove();
        $("#"+subject.replace(':','_')).parent().parent().append(relasjonstype_contents);
        $("#"+subject.replace(':','_')).parent().parent().children('.NodeContents').css("display", "block");
        var maalnode = $("#"+subject.replace(':','_')).parent().parent();
        $("#"+subject.replace(':','_')).remove();
        
        maalnode.children('.expandNode').unbind('click');
        maalnode.children('.expandNode').click(function() {
        var contents = $(this).parent().children(".NodeContents");
        contents.toggle();
        
        if(contents.css('display') != "none") {
          $(this).attr("class", "expandNode collapse");
        }
        else {
          $(this).attr("class", "expandNode expand");
        }
        });
        
        }
        else {
          var message_node = $('<div class="message_node">'+Drupal.t('There were no resources connected to this competence aim')+'</div>')
          .css({'margin-top': 5});
          $(".aim_window_loader_white_nb").remove();
          $("#"+subject.replace(':','_')).parent().parent().append(message_node);
        }
    }
    else {
      var count = data.values.length;
      if (count > 0) {
        var relasjonstype_contents = $('<div class="NodeContents"></div>');
        for (var i = 0; i < count; i++) {
          var relasjonstype_node = $('<div id="'+data.values[i].relasjon+'-RelasjonstypeNode">'+data.values[i].relasjon+'</div>')
          .css({'margin-top': 5});
          $('<div class="expandNode expand"></div>').prependTo(relasjonstype_node);

          var ressurs_contents = $('<div class="NodeContents"></div>');

          var ressursdata = data.values[i].ressursdata;
          var rcount = ressursdata.length;
          for (var j = 0; j < rcount; j++) {
            var output = '';
            var ressurs = ressursdata[j];
            
            
            var nodeurl = ressurs.ressurs_url;
            var nodetitle = ressurs.ressurs_tittel;
            if(ressurs.ressurs_url == "") {
            	nodeurl = "http://red.ndla.no/node/"+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length);
            }
            
            if(ressurs.ressurs_tittel == "") {
            	nodetitle = "NODE: "+ressurs.ressurs_topicid.substr(ressurs.ressurs_topicid.lastIndexOf("_")+1,ressurs.ressurs_topicid.length);
            }
            
            output += '<div class="ItemTxt"><input type="checkbox" value="'+ressurs.ressurs_reifierid+'" class="ressurs-checkbox" /> <span class="batch_resource_title_span">'+ressurs.ressurs_tittel+'</a> (<a href="javascript:" onclick="$(\'#info_'+ressurs.ressurs_nodeid+'\').css(\'display\',\'block\')">'+Drupal.t('Information')+'</a>)';
            output += '<div id="info_'+ressurs.ressurs_nodeid+'" class="grep-ressurs-nodeinfo ochidden">';
            output += '<p><strong>'+Drupal.t('Type')+'</strong>: '+ressurs.ressurs_type+'</p>';
            output += '<p><strong>'+Drupal.t('Node')+'</strong>: <a href="'+ressurs.ressurs_url+'">'+ressurs.ressurs_url+'</a></p>';
            output += '<p><strong>'+Drupal.t('Relation created by')+'</strong>: <a href="'+ressurs.ressurs_bruker_url+'">'+ressurs.ressurs_bruker_navn+'</a></p>';
            output += '<div class="batch_close_butt" onclick="$(\'#info_'+ressurs.ressurs_nodeid+'\').css(\'display\',\'none\')"></div><div class="clear" />';
            output += '</div></div>';
            $('<div class="NodeItem"></div>').html(output).appendTo(ressurs_contents);
          }
          relasjonstype_node.append(ressurs_contents);
          relasjonstype_contents.append(relasjonstype_node);

          relasjonstype_node.children('.expandNode').click(function() {
            var contents = $(this).parent().children(".NodeContents");
            contents.toggle();
            if(contents.css('display') != "none")
               {
             $(this).attr("class", "expandNode collapse");
            }
            else {
              $(this).attr("class", "expandNode expand");
            }
          });
        }//end for relations

        
        $(".aim_window_loader_white_nb").remove();
        $("#"+subject.replace(':','_')).parent().parent().append(relasjonstype_contents);
        $("#"+subject.replace(':','_')).parent().parent().children('.NodeContents').css("display", "block");
        var maalnode = $("#"+subject.replace(':','_')).parent().parent();
        $("#"+subject.replace(':','_')).remove();
        
        maalnode.children('.expandNode').unbind('click');
        maalnode.children('.expandNode').click(function() {
        var contents = $(this).parent().children(".NodeContents");
        contents.toggle();
        
        if(contents.css('display') != "none") {
          $(this).attr("class", "expandNode collapse");
        }
        else {
          $(this).attr("class", "expandNode expand");
        }
        });
        
        }
        else {
          var message_node = $('<div class="message_node">'+Drupal.t('There were no resources connected to this competence aim')+'</div>')
          .css({'margin-top': 5});
          $(".grep-loader").remove();
          $("#"+subject.replace(':','_')).parent().parent().append(message_node);
        }
    }
    
      
  }
}

function portTree(nid,div_id,modus,subject,func) {
  output += getCurriculaResourcesBySubject(nid,div_id,'plan','newport');  
}

function saveSubjectFilter(nid,faget){
  var doc_uri = document.URL;
  var wrapper = $("#laereplan_"+faget);
  var filter_name = $(':input[name=add_'+faget+']')[0].value;
  var jsonstr = '{"nid" : "'+nid+'", "filter_name" : "'+filter_name+'", "values": [';
  wrapper.find('.aimbox').each(function () {
    if ($(this).attr('checked')) {
      var valarr = $(this).val().split("§");
      jsonstr += '{"laereplan" : "'+valarr[0]+'", "hovedomraade": "'+valarr[1]+'", "kompetansemaal_uuid" : "'+valarr[2]+'", "kompetansemaal_title" : "'+valarr[3]+'" },';
    }
  });
  jsonstr = jsonstr.substr(0,jsonstr.lastIndexOf(','));
  jsonstr += ']}';
  
  $.ajaxSetup({ 
    scriptCharset: "utf-8" , 
    contentType: "application/x-www-form-urlencoded; charset=utf-8"
  });
  jasonUrl = Drupal.settings.ndla_ontopia_connect.setfilter_json_url+"?str="+jsonstr;
 
  $.ajax({
  type: 'POST',
  url: jasonUrl,
  dataType: 'text',
  success: function(data) {
    alert(data);
    window.location = doc_uri;
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    $("#ontopia_connect_errors").html(Drupal.t('<p>An error occured when associating resource to the competence aim. Unable to fetch data.</p>'));
  }
  
  });

}


function saveQueries() {
  var jsonstr = '{ "hovedomraade" : [';
  
  $('textarea.query_mapping.hovedomraade').each(function() {
   var h_name = $(this).attr('name').split(':');
   jsonstr += '{"uuid" : "uuid%'+h_name[1]+'", "query" : "'+$(this).val().replace(/"/g,'§')+'"},';
  });
  jsonstr = jsonstr.substr(0,jsonstr.lastIndexOf(','));
  jsonstr += '],';
  jsonstr += '"kompetansemaal" : [';
  $('textarea.query_mapping.kompetansemaal').each(function() {
    var k_name = $(this).attr('name').split(':');
    jsonstr += '{"uuid" : "uuid%'+k_name[1]+'", "query" : "'+$(this).val().replace(/"/g,'§')+'"},';
   });
  jsonstr = jsonstr.substr(0,jsonstr.lastIndexOf(','));
  jsonstr += ']}';
  
  $(':input[name=save_queries_json]')[0].value =jsonstr;
  $(".query_processing").attr('class','ochidden');
  $(".query_save_submit").removeAttr('class','ochidden');
}


function processCurriculaAssocs(nid,faget) {
  var jsonstr = '{"values" : [';
  $("#laereplan_"+faget).find('.ressurs-checkbox').each( function() {
    if ($(this).is(':checked')){
      var aim_title = $(this).parent().parent().parent().parent().parent().parent().children('.aim_title').html();
      var txt = $(this).parent().find('.batch_resource_title_span').html().substr(0,($(this).parent().find('.batch_resource_title_span').html().indexOf('<a')-1));
      jsonstr += '{"reifier" : "'+$(this).val()+'", "title": "'+txt+'" , "aim_title" : "'+aim_title+'"},';
    }
  }
  );
  jsonstr = jsonstr.substr(0,jsonstr.lastIndexOf(','));
  jsonstr += ']}';

  $(":input[name=delete_assocs_ltm_"+faget+"]")[0].value = jsonstr;
  $(":input[name=faget]")[0].value = faget;
  $(".resource_delete_submit").removeAttr('class','ochidden');
  $('#process_resource_button_'+faget).slideUp(500,function(){});
}


function processPortAssocs(nid,faget) {
  var jsonstr = '{"values" : [';
  $("#laereplan_"+faget).find('.ressurs-checkbox').each( function() {
    if ($(this).is(':checked')){
      var ressurs_id = $(this).attr('id').substr($(this).attr('id').lastIndexOf('_')+1);
      var topicid = $("#ressurs_topicid_"+ressurs_id)[0].value;
      var reifierid = $(this)[0].value;
      var txt = $(this).parent().find('.batch_resource_title_span').html().substr(0,($(this).parent().find('.batch_resource_title_span').html().indexOf('<a')-1));
      var userdata = $("#userdata_"+ressurs_id);
      jsonstr += '{"nid" : "'+ressurs_id+'", "ressurs_topicid": "'+topicid+'", "ressurs_reifier_id" : "'+reifierid+'", "ressurs_username" : "'+userdata.html()+'", "ressurs_userurl" : "'+userdata.attr("href").substring((userdata.attr('href').lastIndexOf('/')+1))+'","ressurs_fag" : "'+faget+'", "new_assocs" : [';
      
      $("#laereplan_"+ressurs_id).find('.newport-checkbox').each( function() {
    	  if ($(this).is(':checked')){
              var theid = $(this).attr('id');
              var node_id = theid.substr(theid.lastIndexOf('_')+1);
            	  tmpid = theid.substr(5,theid.length);
            	  var nodepos = tmpid.lastIndexOf('_');
    	    	  var new_uuid = tmpid.substr(0,nodepos);
    	          var newtxt = $(this).parent().find('.newport-aimtitle').html();
    	          jsonstr += '{"new_uuid" : "'+new_uuid+'", ';
    	          $("#assoctypes_"+tmpid).find('.assoc-checkbox').each( function() {
    	            if ($(this).is(':checked')){
    	              var assoctype = $(this)[0].value;
    	              jsonstr += '"assoctype" : "'+assoctype+'"},';
    	            }
    	          });
            }
      });
      jsonstr = jsonstr.substr(0,jsonstr.lastIndexOf(','));
      jsonstr += ']},';
    }
  }
  );
  
  jsonstr = jsonstr.substr(0,jsonstr.lastIndexOf(','));
  jsonstr += ']}';
  //alert(jsonstr);
  $(':input[name=change_assocs_ltm_'+faget+']')[0].value =jsonstr;
  $(".resource-process-button").slideUp(500,function(){});
  $(":input[name=faget]")[0].value = faget;
  $(".resource_change_submit").removeAttr('class','ochidden');
}

function deleteSubjectFilter(nid,filter_id) {
  var doc_uri = document.URL;
  $.ajaxSetup({ 
    scriptCharset: "utf-8" , 
    contentType: "application/x-www-form-urlencoded; charset=utf-8"
  });
  jasonUrl = Drupal.settings.ndla_ontopia_connect.deletefilter_json_url+"/"+nid+"/"+filter_id;
  
  $.ajax({
  type: 'GET',
  url: jasonUrl,
  dataType: 'text',
  success: function(data) {
    alert(data);
    window.location = doc_uri;
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    $("#ontopia_connect_errors").html(Drupal.t('<p>An error occured when associating resource to the competence aim. Unable to fetch data.</p>'));
  }
  
  });
}

function getScopeLength(tempArray) {
  var result = 0;
  for ( tempValue in tempArray ) {
     result++;
  } 
  return result;
}

function addClickListeners() {
  $('.grep_first_level').click(function() {
    $('.grep_first_level').removeClass('expanded');
    $(this).addClass('expanded');
    
    $('#grep-results-kompetansemaal').hide("slide", 
      {  direction: "left"
      }, 200, function() {
      $('#grep-results-kompetansemaal').show("slide", {
        direction: "left"
      }, 200);
    });
  });
}

function setMaal(data) {
  $(':input[name=kompetansemaal-hidden]')[0].value = data;
}

function setGrep(data) {
  $(':input[name=grep-hidden]')[0].value = data;
}

function setYrkesJson(data){
  $(':input[name=kompetansemaal-yrkesfag-trinn-hidden]')[0].value = data;
}

function setTrinnJson(data) {
  $(':input[name=kompetansemaal-trinn-hidden]')[0].value = data;
}

function setGrepListingJSON(data,typearr) {
  $(':input[name=grep-listing-data]')[0].value = data;
  $(':input[name=nodetype-arr]')[0].value = typearr;
}

function trim (str) {
  str = str.replace(/^\s+/, '');
  for (var i = str.length - 1; i >= 0; i--) {
    if (/\S/.test(str.charAt(i))) {
      str = str.substring(0, i + 1);
      break;
    }
  }
  return str;
}


function sortAssoc(aInput) {
  var aTemp = [];
  for (var sKey in aInput)
  aTemp.push([sKey, aInput[sKey]]);
  aTemp.sort(function () {return arguments[0][1] < arguments[1][1]});
  
  var aOutput = [];
  for (var nIndex = aTemp.length-1; nIndex >=0; nIndex--)
  aOutput[aTemp[nIndex][0]] = aTemp[nIndex][1];
  
  return aOutput;
}

function showMore(type) {
 if (type == 'external') {
   var elms = $("#external_list").children();
   elms.each(function(){
     if ($(this).hasClass('external_hidden')){
       $(this).removeClass('external_hidden');
       var newlink = '<span id="external_list_toggle" onclick="showLess(\'external\')">'+Drupal.t('Less')+'</span>';
       $("#span_external_list_toggle").html(newlink);
     }
   });
 } 
 else if (type == 'deling') {
   var elms = $("#deling_list").children();
   elms.each(function(){
     if ($(this).hasClass('deling_hidden')){
       $(this).removeClass('deling_hidden');
       var newlink = '<span id="deling_list_toggle" onclick="showLess(\'deling\')">'+Drupal.t('Less')+'</span>';
       $("#span_deling_list_toggle").html(newlink);
     }
   });
 }
}

function showMoreAims(type,aimtype) {
  var elms = $("#"+aimtype+"_assocced_resources_"+type).children();
    elms.each(function(){
      if ($(this).hasClass('external_hidden')){
        $(this).removeClass('external_hidden');
        var newlink = '<span id="'+aimtype+'_aim_'+type+'_toggle" onclick="showLessAims(\''+type+'\',\''+aimtype+'\')">'+Drupal.t('Less')+'</span>';
        $("#"+aimtype+"_span_aim_"+type+"_toggle").html(newlink);
      }
    });
}
 
 
function showLessAims(type,aimtype) {
   var elms = $("#"+aimtype+"_assocced_resources_"+type).children();
   var counter = 0;
   elms.each(function(){
     if (counter++ >= 5) {
       if (!$(this).hasClass('external_hidden')){
         $(this).addClass('external_hidden');
         var newlink = '<span id="'+aimtype+'_aim_'+type+'_toggle" onclick="showMoreAims(\''+type+'\',\''+aimtype+'\')">'+Drupal.t('More')+'</span>';
         $("#"+aimtype+"_span_aim_"+type+"_toggle").html(newlink);
       } 
     }
   });
}

function showLess(type) {
  if (type == 'external') {
    var elms = $("#external_list").children();
    //$("#external_list").html('');
    var counter = 1;
    elms.each(function(){
      if (counter++ > 5){
        $(this).addClass('external_hidden');
      }
    });
    var newlink = '<span id="external_list_toggle" onclick="showMore(\'external\')">'+Drupal.t('More')+'</span>';
    $("#span_external_list_toggle").html(newlink);
  } 
  else if (type == 'deling') {
    var elms = $("#deling_list").children();
    $("#deling_list").html('');
    var counter = 1;
    elms.each(function(){
      if (counter++ <= 5){
        $("#deling_list").append($(this));
      }
      else {
        $(this).addClass('deling_hidden');
        $("#deling_list").append($(this));
      }
    });
    var newlink = '<span id="deling_list_toggle" onclick="showMore(\'deling\')">'+Drupal.t('More')+'</span>';
    $("#span_deling_list_toggle").html(newlink);
  }
}

function addClickListeners() {
  $('.grep_first_level').click(function() {
    $('.grep_first_level').removeClass('expanded');
    $(this).addClass('expanded');
    
    $('#grep-results-kompetansemaal').hide("slide", 
      {  direction: "left"
      }, 200, function() {
      $('#grep-results-kompetansemaal').show("slide", {
        direction: "left"
      }, 200);
    });
  });
}


function closeLighty() {
  $("a#bottomNavClose").click();
}


/**
 * Resize tray container to fit it's content.
 */
function grep_resizeContainer() {
  // Determine the animation speed based on distance
  // between old and new height.
  var speed = ($('#tray_parker').height()-$('#tray_menus').height())*10;
  if (speed < 0) speed*=-1;
  if (speed > 400) speed = 400;

  $('#tray_menus').animate({
    height: $('#tray_parker').height() + "px"
    }, speed);
  $('#tray_content').css({
    height: 'auto'
  });
}


function show_aims(serviceurl,language) {
 
  $(".block-ndla_ontopia_connect").before('<div id="show_aims_window" class="ndla_aim_window"></div>');
   
  $("#show_aims_window").html('<div class="aim_window_loader_'+language+'"></div>');
  $("#show_aims_window").fadeIn(750);
  
  
  $.ajaxSetup({ 
    scriptCharset: "utf-8" , 
    contentType: "application/x-www-form-urlencoded; charset=utf-8"
  });

  $.ajax({
  type: 'GET',
  url: serviceurl,
  dataType: 'text',
  contentType: "application/x-www-form-urlencoded; charset=utf-8",
  success: function(data) {
    $("#aim_window_loader_"+language+"").fadeOut(500);
    $("#show_aims_window").html();
    $("#show_aims_window").html(data);
    
    $("#show_aim_resources").tabs({ spinner: ""});
    $("#show_aim_wrapper").after('<div id="show_aims_close_button"></div>');
    $("#show_aims_close_button").bind('click',function(){
        $("#show_aims_window").html();
        $("#show_aims_window").fadeOut(500);
      });
    $("#show_aim_resources_toolbar_sort").bind('click',function(){
    	if($("#show_aim_resources_toolbar_sort").css('background-image').indexOf("down-arrow.png") > 0){
    		$("#show_aim_resources_toolbar_sort").css('background-image','url("/sites/all/modules/ndla_ontopia_connect/img/up-arrow.png")');
    	}
    	else{
    		$("#show_aim_resources_toolbar_sort").css('background-image','url("/sites/all/modules/ndla_ontopia_connect/img/down-arrow.png")');
    	}
    	var $tabs = $('#show_aim_resources').tabs();
    	var selected = $tabs.tabs('option', 'selected');
    	var tabid = $(".ui-tabs-selected").find("a").attr("href");
    	
    	if(tabid == "#ndla_tab"){
    		$("#show_aim_resources_toolbar_sort_popup_list_item_contenttype").removeAttr("style");
    		$("#show_aim_resources_toolbar_sort_popup_list_item_coverage").removeAttr("style");
    	}
    	else if(tabid == "#deling_tab"){
    		$("#show_aim_resources_toolbar_sort_popup_list_item_contenttype").attr("style","display:none");
    		$("#show_aim_resources_toolbar_sort_popup_list_item_coverage").removeAttr("style");
    	}
    	else if(tabid == "#nygiv_tab"){
    		$("#show_aim_resources_toolbar_sort_popup_list_item_contenttype").attr("style","display:none");
    		$("#show_aim_resources_toolbar_sort_popup_list_item_coverage").attr("style","display:none");
    	}
    	
    	$("#show_aim_resources_toolbar_sort_popup_list").toggle("slow");
    	
    });
    
    
    $("#show_aim_resources_toolbar_search_term").bind('click',function(){
    	if($("#show_aim_resources_toolbar_search_term")[0].value == "start your search"){
    		$("#show_aim_resources_toolbar_search_term")[0].value = "";
    	}
    });
    
    $("#show_aim_resources_toolbar_search_term").bind('keyup',function(){
    	var search = $("#show_aim_resources_toolbar_search_term")[0].value;
		if(!$("#ndla_tab").hasClass('ui-tabs-hide')){
			$("#ndla_tab_more").addClass("ndla_show_aim_hidden");
			show_aim_doSearch("ndla_tab",search);
		}
		else if(!$("#deling_tab").hasClass('ui-tabs-hide')){
			$("#deling_tab_more").addClass("ndla_show_aim_hidden");
			show_aim_doSearch("deling_tab",search);
		}
		else if(!$("#nygiv_tab").hasClass('ui-tabs-hide')){
			$("#nygiv_tab_more").addClass("ndla_show_aim_hidden");
			show_aim_doSearch("nygiv_tab",search);
		}
    });
    
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    $("#ontopia_connect_errors").html(Drupal.t('<p>An error occured when fetching the data for this comeptence aim. Unable to fetch data.</p>'));
  }
  
  }); 
}

function show_aim_doSearch(tab,search){
	$("#"+tab+"_list").find('.show_aim_linkspan').each(function(){
		if($(this).html().toLowerCase().indexOf(search.toLowerCase()) >= 0){
			if($(this).parent().hasClass("ndla_show_aim_hidden")){
				$(this).parent().removeClass("ndla_show_aim_hidden");
			}
		}
		else{
			if(!$(this).parent().hasClass("ndla_show_aim_hidden")){
				$(this).parent().addClass("ndla_show_aim_hidden");
			}
			
		}
		
	});
}

function show_aim_resources_sort_by(type){
	$("#show_aim_resources_toolbar_sort").css('background-image','url("/sites/all/modules/ndla_ontopia_connect/img/down-arrow.png")');
	var tab = "";
	var site = "";
	
	if(!$("#ndla_tab").hasClass('ui-tabs-hide')){
		$("#ndla_tab_more").addClass("ndla_show_aim_hidden");
		tab = "ndla_tab";
		site = "ndla";
	}
	else if(!$("#deling_tab").hasClass('ui-tabs-hide')){
		$("#deling_tab_more").addClass("ndla_show_aim_hidden");
		tab = "deling_tab";
		site = "deling";
	}
	else if(!$("#nygiv_tab").hasClass('ui-tabs-hide')){
		$("#nygiv_tab_more").addClass("ndla_show_aim_hidden");
		tab = "nygiv_tab";
		site = "nygiv";
	} 
	
	
	if(type == "coverage") {
		var items = $("#"+tab+"_list li").get();
		items.sort(function(a,b){
			if($(a).hasClass(site+'_show_aim_hidden')){
				$(a).removeClass(site+'_show_aim_hidden');
			}
			if($(b).hasClass(site+'_show_aim_hidden')){
				$(b).removeClass(site+'_show_aim_hidden');
			}
			var arr_A = $(a).attr('class').split(" ");
			rel_A = arr_A[0]
			
			var arr_B = $(b).attr('class').split(" ");
			rel_B = arr_B[0]
			
			if (rel_A < rel_B) return -1;
			  if (rel_A > rel_B) return 1;
			  return 0;
		});
		var ul = $("#"+tab+"_list");
		$.each(items, function(i, li){
		  ul.append(li);
		});
	}
	else if(type == "contenttype") {
		var items = $("#"+tab+"_list li").get();
		items.sort(function(a,b){
			if($(a).hasClass(site+'_show_aim_hidden')){
				$(a).removeClass(site+'_show_aim_hidden');
			}
			if($(b).hasClass(site+'_show_aim_hidden')){
				$(b).removeClass(site+'_show_aim_hidden');
			}
			var arr_A = $(a).attr('class').split(" ");
			rel_A = arr_A[2]
			
			var arr_B = $(b).attr('class').split(" ");
			rel_B = arr_B[2]
			
			if (rel_A < rel_B) return -1;
			  if (rel_A > rel_B) return 1;
			  return 0;
		});
		var ul = $("#"+tab+"_list");
		$.each(items, function(i, li){
		  ul.append(li);
		});
	}
	else if(type == "name") {
		var items = $("#"+tab+"_list li").get();
		items.sort(function(a,b){
			if($(a).hasClass(site+'_show_aim_hidden')){
				$(a).removeClass(site+'_show_aim_hidden');
			}
		
			if($(b).hasClass(site+'_show_aim_hidden')){
				$(b).removeClass(site+'_show_aim_hidden');
			}
			
		  var keyA = $(a).text();
		  var keyB = $(b).text();

		  if (keyA < keyB) return -1;
		  if (keyA > keyB) return 1;
		  return 0;
		});
		var ul = $("#"+tab+"_list");
		$.each(items, function(i, li){
		  ul.append(li);
		});
	}
	
	$("#show_aim_resources_toolbar_sort_popup_list").toggle("slow");
	
}

function show_aim_goTo(url) {
	window.location = url;
} 

function show_aim_view_more(site){
	//"."+site+"_show_aim_hidden"
	$("#"+site+"_tab_list").children("li").each(function(){
		if($(this).hasClass(site+"_show_aim_hidden")) {
			$(this).removeClass(site+"_show_aim_hidden");
		}
	});
	$("#"+site+"_tab_more").addClass(site+"_show_aim_hidden");
}

function searchForCompetenceAims() {
  $(':input[name=competence_aim_search_json]')[0].value = '';
  $('#competence_aim_search_view').html('');
  var search_mode = $(':input[name=competence_aim_search_mode]:checked').val();
  var search = $(':input[name=competence_aim_search_field]')[0].value;
  if (search != '') {
    $('#competence_aim_search_view').append('<div class="search_aim_window_loader_nn"></div>');
    var service_data = {
        "method" : "get.competence_aim", 
        "search" : search,
        "search_mode" : search_mode,
        "format" : "html",
        "language" : "nn"};
      $.ajax({
        url: Drupal.settings.ndla_ontopia_connect.relate_service_url+"/services/json",
        dataType: "jsonp",
        data: service_data,
        jsonp: "callback",
        success: function(data){
        
          if (typeof(data["#error"]) !== 'undefined' && data["#error"] == false) {
            if(data["#data"] != '[]') {
              buildAimTree(data["#data"]);
            }
            else {
              $('.search_aim_window_loader_nn').remove();
              var params = {"@search" : search};
              $('#competence_aim_search_view').append('<div class="messages error">' + Drupal.t('The search "@search" did not return any results',params) + '</div>')
            }
            
          } else {
            // display error message
          }
        },
          error: function(xhr, status, errorThrown) {
          alert("Cannot connect to the Internet.");
        }
      });  
  }
  else {
    alert(Drupal.t('You have to enter a search term'));
    return false;
  }
   
}

function buildAimTree(data) {
  $('#competence_aim_search_view').html(data);
  
  $('.laereplan_node').children('.expandNode').click(function() {
    var contents = $(this).parent().children(".NodeContents");
    if (contents.length > 0) {
      contents.toggle();
      if(contents.css('display') != "none")
      {
        if (typeof($(this)) !== 'undefined') {
          $(this).attr("class", "expandNode collapse");
        }
  
      }
      else
      {
        if (typeof($(this)) !== 'undefined') {
          $(this).attr("class", "expandNode expand");
        }
      }
    }
  });

  $('.hovedomraade_node').children('.expandNode').click(function() {
    var contents = $(this).parent().children(".NodeContents");
    contents.toggle();
    if(contents.css('display') != "none")
    {
      $(this).attr("class", "expandNode collapse");
    }
    else
    {
      $(this).attr("class", "expandNode expand");
    }
  });

  $('.kompetansemaal_node').children('.expandNode').click(function() {
    var contents = $(this).parent().children(".NodeContents");
    var uuid = $(this).parent().attr('id').substr(0,$(this).parent().attr('id').lastIndexOf('-KompetansemaalNode'));
    contents.toggle();
    if(contents.css('display') != "none")
    {
      $(this).attr("class", "expandNode collapse");
      $('#maal_'+uuid).attr('checked','checked');
      $('#maal_'+uuid).attr('class','newport-checkbox');
    }
    else
    {
      $('#maal_'+uuid).removeAttr('checked');
      $('#maal_'+uuid).attr('class','newport-checkbox ochidden');
      $(this).attr("class", "expandNode expand");
    }
  });
}

function showLaerlingAims(the_link){
	if($(the_link).hasClass('show')){
		$('.competence_aim').each(function(){
		  if($(this).hasClass('apprentice')){
			  if($(this).parent().parent().hasClass('relation_hide')){
				  $(this).parent().parent().removeClass('relation_hide');
				  $(this).parent().parent().addClass('relation_show');
				  $(this).parent().parent().addClass('relation_no_felles');
			  }
		    $(this).fadeIn(750);
		  }
		});
		$(the_link).removeClass('show');
		$(the_link).removeClass('laerling_show');
		$(the_link).addClass('laerling_hide'); 
		$(the_link).text(Drupal.t('Hide apprentice competence aims'));
	}
	else {
		$('.competence_aim').each(function(){
			if($(this).hasClass('apprentice')){
		        $(this).fadeOut(750);
		        if($(this).parent().parent().hasClass('relation_no_felles')){
		          $(this).parent().parent().removeClass('relation_show');
		          $(this).parent().parent().removeClass('relation_no_felles');
		          $(this).parent().parent().addClass('relation_hide');
		        }
		      }
		});
		$(the_link).addClass('show');
		$(the_link).removeClass('laerling_hide');
		$(the_link).addClass('laerling_show');
	    $(the_link).text(Drupal.t('Show apprentice competence aims')); 
	}
}
