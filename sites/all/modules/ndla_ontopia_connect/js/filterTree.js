function filterTree() {
  
  var search = $(':input[name=grep_search]')[0].value;
  var mode = $(':input[name=tree_mode]')[0].value;
  var json = $(':input[name=tree_json]')[0].value;
  data = $.evalJSON(json);
    
  var filtered_data = '{"values":[';
  var filtered_temp = '';
  for (var i = 0; i < data.values.length; i++) {
    var plan = data.values[i].name;
    if (plan.indexOf(search) >= 0) {
      filtered_temp += '{"uuid":"'+data.values[i].uuid+'","name":"'+data.values[i].name+'"},';
    }
  }//end for
  filtered_temp = filtered_temp.substring(0,filtered_temp.lastIndexOf(','));
  filtered_data += filtered_temp+']}';
  treedata = $.evalJSON(filtered_data);
  createFilterTree(treedata);
  
}

function createFilterTree(data) {
  
  if ($(':input[name=tree_json]')[0].value == '') {
    $(':input[name=tree_json]')[0].value = JSON.stringify(data);
  }
  
  tView = new curriculaDelingtree(data, $('#curriculum_tree'),'curriculum_listing');
  
  $("#all_link").html(Drupal.t('Curricula'));
  $("#all_link").attr('class','active-tab');
}


function getCurriculumDelingTree(){
  $("#all_deling_link").slideUp(500,function(){});
  $("#all_link").slideUp(500,function(){});
  $("#all_deling_link").parent().append($('<div class="grep-loader"></div>'));
  $("#curriculum_tree").slideUp(500,function(){});
  
  var jasonUrl = Drupal.settings.ndla_ontopia_connect.curriculatree+'/all_deling';
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
    //console.log(JSON.stringify(data));
      createFilterTree(data);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_erorrs").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
    }
  });
}


function getCurriculaDelingBySubject(subject,div_id) {
  jasonUrl = Drupal.settings.ndla_ontopia_connect.laereplan_by_subject+"/"+subject
   
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
      if (div_id.indexOf('plan') > -1) {        
        $(":input[name=assoc_fag] option[value=\'"+subject+"\']").attr('selected', 'selected');
        $('#grep_search_span').css('display','none');
        processDelingTreeAims(data,div_id); 
      }
      else {
        $('#grep_search_span').css('display','none');
        processDelingAims(data,div_id);
      }
      
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_erorrs").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
    }
    });
}

function processDelingAims(data, div_id) {
  tView = new treeview(data,div_id, $('#add_curriculum_div_'+div_id));
  $('#curriculum_info_link_'+div_id).removeAttr('class');
  $('#add_curriculum_div_'+div_id).slideDown(500,function(){});
  $('#add_curriculum_button_'+div_id).attr('class','hidden');
  $('#save_filter_button_'+div_id).slideDown(500,function(){});
}


function processDelingTreeAims(data,div_id){
  $('#curriculum_tree').children().remove();
  tView = new curriculaDelingtree(data, $('#curriculum_tree'),'curriculum_data');
}