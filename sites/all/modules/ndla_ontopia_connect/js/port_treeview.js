port_treeview = function(tData,div_id, container,modus,subject)
{
  this.build = function(nodeInfo,div_id)
  {
    if(isInteger(div_id)) {
      var stem = $('<div id="laereplan_'+div_id+'" class="GREPTreeView_Port"></div>'); 
    }
    else {
      var stem = $('<div id="laereplan_'+div_id+'" class="GREPTreeView"></div>');  
    }
    
    var length = nodeInfo.values.length;
    if (length > 0) {
      for(var i=0;i< length;i++) {
        var node = $('<div id="'+div_id+'-LaereplanNode">'+nodeInfo.values[i].fag_tittel+'</div>')
        .css({'margin-top': 5});
        $('<div class="expandNode expand"></div>')
        .prependTo(node);
        
        var hovedomraade_contents = $('<div class="NodeContents"></div>');
        var fag_data = nodeInfo.values[i].fag_data
        var hlen = fag_data.length;
        
        for(var j = 0; j < hlen; j++) {
          var maaldata = fag_data[j].maaldata;
          var mlen = maaldata.length;
          if(mlen > 0) {
            var hovedomraade_node = $('<div id="'+div_id+'-HovedomraadeNode">'+fag_data[j].hovedomraade_tittel+'</div>')
            .css({'margin-top': 5});
            $('<div class="expandNode expand"></div>')
            .prependTo(hovedomraade_node);
            
            
            
            var kompetansemaal_contents = $('<div class="NodeContents"></div>');
            var maaldata = fag_data[j].maaldata;
            var mlen = maaldata.length;
            
            for(var k = 0; k < mlen; k++) {
                if (isInteger(div_id)) {
                  var kompetansemaal_node = $('<div id="'+maaldata[k].uuid.replace(':','_')+'-KompetansemaalNode"><input type="checkbox" name="maal_'+maaldata[k].uuid.replace(':','_')+'" id="maal_'+maaldata[k].uuid.replace(':','_')+'_'+div_id+'"  class="newport-checkbox ochidden" />&nbsp;<span class="newport-aimtitle">'+maaldata[k].aim_title+'</span></div>')
                  .css({'margin-top': 5});
                  $('<div class="expandNode expand"><span id="'+maaldata[k].uuid.replace(':','_')+'" class="uuid" /></div>')
                  .prependTo(kompetansemaal_node);
                  
                  
                  var relasjonsnode_contents = $('<div class="NodeContents"></div>');
                  var relasjon_node = $('&nbsp; <div id="assoctypes_'+maaldata[k].uuid.replace(':','_')+'_'+div_id+'" class="port_assoctypes">'+getAssoctypes(maaldata[k].uuid.replace(':','_'))+'</div>').appendTo(relasjonsnode_contents);
                  
                }
                else {
                  var kompetansemaal_node = $('<div id="'+div_id+'-KompetansemaalNode">'+maaldata[k].aim_title+'</div>')
                  .css({'margin-top': 5});
                  $('<div class="expandNode expand"><span id="'+maaldata[k].uuid.replace(':','_')+'" class="uuid" /></div>')
                  .prependTo(kompetansemaal_node);
                }
                
                kompetansemaal_node.append(relasjonsnode_contents);
              //hovedomraade_node.append(kompetansemaal_contents);
              kompetansemaal_contents.append(kompetansemaal_node);
              
              kompetansemaal_node.children('.expandNode').click(function() {
                var contents = $(this).parent().children(".NodeContents");
                var uuid = '';
                if (modus == 'plan') {
                  if(isInteger(div_id)) {
                    uuid = $(this).parent().attr('id').substr(0,$(this).parent().attr('id').lastIndexOf('-KompetansemaalNode'));
                  }
                  else {
                    $(this).parent().append($('<div class="aim_window_loader_white_nb"></div>'));
                    uuid = $(this).children().filter('.uuid').attr('id').replace('_',':');
                    getCurriculaResourcesBySubject(uuid,div_id,'ressurs','port');  
                  }
                  
                }
                
                contents.toggle();
                if(contents.css('display') != "none")
                {
                  $(this).attr("class", "expandNode collapse");
                  if(isInteger(div_id)) {
                    $('#maal_'+uuid+'_'+div_id).attr('checked','checked');
                    $('#maal_'+uuid+'_'+div_id).attr('class','newport-checkbox');
                  }
                }
                else
                {
                  $(this).attr("class", "expandNode expand");
                  if(isInteger(div_id)) {
                    $('#maal_'+uuid+'_'+div_id).removeAttr('checked');
                    $('#maal_'+uuid+'_'+div_id).attr('class','newport-checkbox ochidden');
                  }
                }
               
              });
              //$('<div class="NodeItem"></div>').html('<div class="ItemTxt"><input type="checkbox" name="'+div_id+'_'+maaldata[k].uuid.replace(':','_')+'" value="'+nodeInfo.values[i].fag_psi+"§"+fag_data[j].hovedomraade_uid+"§"+maaldata[k].uuid+"§"+maaldata[k].aim_title+'" class="aimbox" /> '+maaldata[k].aim_title+' ('+maaldata[k].sort_key+') </div>').appendTo(kompetansemaal_contents);
            }//end for
            
            hovedomraade_node.append(kompetansemaal_contents);
            hovedomraade_contents.append(hovedomraade_node);
            
            
            hovedomraade_node.children('.expandNode').click(function() {
              var contents = $(this).parent().children(".NodeContents");
              contents.toggle();
              if(contents.css('display') != "none")
              {
                $(this).attr("class", "expandNode collapse");
              }
              else
              {
                $(this).attr("class", "expandNode expand");
              }
             
            });
          }    
        }//end for

        node.append(hovedomraade_contents);
        
        
        node.children('.expandNode').click(function() {
          var contents = $(this).parent().children(".NodeContents");
          if (contents.length > 0) {
            contents.toggle();
            if(contents.css('display') != "none")
            {
              if (typeof($(this)) !== 'undefined') {
                $(this).attr("class", "expandNode collapse");
              }
              
            }
            else
            {
              if (typeof($(this)) !== 'undefined') {
                $(this).attr("class", "expandNode expand");
              }
            }
          }
          
         
        });

        stem.append(node);
      }//end for
    }//end if
   
    return stem;
  }
 
  this.tree = this.build(tData,div_id);
  var treeCon = container;

  treeCon.append(this.tree);
}

function isInteger(s) {
  return (s.toString().search(/^-?[0-9]+$/) == 0);
}

function getAssoctypes() {
  var lang = Drupal.settings.ndla_ontopia_connect.language;
  jstr = Drupal.settings.ndla_ontopia_connect.assoctypes;
  jstr = jstr.replace(/\\/g,'');
  var assoctypes = $.evalJSON(jstr);
  var options = '';
    
  for (var i in assoctypes) {
    options += '<input type="checkbox" class="assoc-checkbox" value="'+i+'" /> '+assoctypes[i].kompetansemaal+'/'+assoctypes[i].laeringsressurs+'<br />';
  }
  return options;
}
