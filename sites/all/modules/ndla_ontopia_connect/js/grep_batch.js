/**
 * @file
 * @ingroup ndla_ontopia_connect
 * @brief
 *  Handles the batch resource / aim add.
 */
Drupal.behaviors.ontopia_batch_add = function(context) {
  $('#flush_search').click(function () {
    location.href = location.href;
 }); 
}

function batch_add_sortData(type) {
  
  if(typeof $(':input[name=current_sort_type]')[0] != 'undefined' && $(':input[name=current_sort_type]')[0].value != type) {
    $('.th_sort_header').each(function(){
      $(this).removeClass('down');
      $(this).removeClass('up');
    });
    $(':input[name=sort_direction]').remove();
  }
  
  if(typeof $(':input[name=current_sort_type]')[0] == 'undefined'){
    $("#main_1").append('<input type="hidden" name="current_sort_type" value="'+type+'" />');
  }
  else {
    $(':input[name=current_sort_type]')[0].value = type;
  }
  
  var offset = 0;
  if(typeof $(':input[name=offset]')[0] != 'undefined') {
     offset = $(':input[name=offset]')[0].value;
  }
  else if(typeof $(':input[name=batch_add_search_offset]')[0] != 'undefined') {
    offset = $(':input[name=batch_add_search_offset]')[0].value;
  }
  
  var html = '';
  var jdata = '';
  
  if (typeof $(':input[name=jdata]')[0] != 'undefined'){
    jdata = $.evalJSON(unescape($(':input[name=jdata]')[0].value));
  }
  else if (typeof $(':input[name=batch_add_search_jdata]')[0] != 'undefined'){
    jdata = $.evalJSON(unescape($(':input[name=batch_add_search_jdata]')[0].value));
  }
 
  
  if (type == 'node') {
    if(typeof $(':input[name=sort_direction]')[0] == 'undefined' || $(':input[name=sort_direction]')[0].value == 'desc') {
      jdata.sort(compareTitle);
      $("#th_sort_header_"+type).removeClass('up');
      $("#th_sort_header_"+type).addClass('down');
    }
    else {
      jdata.sort(compareTitleRev);
      $("#th_sort_header_"+type).removeClass('down');
      $("#th_sort_header_"+type).addClass('up');
    }
  }
  else if (type == 'language') {
    if(typeof $(':input[name=sort_direction]')[0] == 'undefined' || $(':input[name=sort_direction]')[0].value == 'desc') {
      jdata.sort(compareLang);
      $("#th_sort_header_"+type).removeClass('up');
      $("#th_sort_header_"+type).addClass('down');
    }
    else {
      jdata.sort(compareLangRev);
      $("#th_sort_header_"+type).removeClass('down');
      $("#th_sort_header_"+type).addClass('up');
    }
  }
  else if (type == 'type') {
    if(typeof $(':input[name=sort_direction]')[0] == 'undefined' || $(':input[name=sort_direction]')[0].value == 'desc') {
      jdata.sort(compareType);
      $("#th_sort_header_"+type).removeClass('up');
      $("#th_sort_header_"+type).addClass('down');
    }
    else {
      jdata.sort(compareTypeRev);
      $("#th_sort_header_"+type).removeClass('down');
      $("#th_sort_header_"+type).addClass('up');
    }
  }
  else if (type == 'published') {
    if(typeof $(':input[name=sort_direction]')[0] == 'undefined' || $(':input[name=sort_direction]')[0].value == 'desc') {
      jdata.sort(comparePublished);
      $("#th_sort_header_"+type).removeClass('up');
      $("#th_sort_header_"+type).addClass('down');
    }
    else {
      jdata.sort(comparePublishedRev);
      $("#th_sort_header_"+type).removeClass('down');
      $("#th_sort_header_"+type).addClass('up');
    }
  }
  else if (type == 'changed') {
    if(typeof $(':input[name=sort_direction]')[0] == 'undefined' || $(':input[name=sort_direction]')[0].value == 'desc') {
      jdata.sort(compareChanged);
      $("#th_sort_header_"+type).removeClass('up');
      $("#th_sort_header_"+type).addClass('down');
    }
    else {
      jdata.sort(compareChangedRev);
      $("#th_sort_header_"+type).removeClass('down');
      $("#th_sort_header_"+type).addClass('up');
    }
  }//end if type
  
  var jlen = jdata.length;
  for (var i = 0; i < jlen; i++) {
    var node = jdata[i];
    if(i%2 == 1) {
      var stripes = 'odd';
    }
    else {
      var stripes = 'even';
    }
    
    html += '<tr class="'+stripes+'" id="row_'+node.nid+'">';
    html += '<td><div class="add_node_button" id="node_button_'+node.nid+'" onclick="addNode('+node.nid+',\''+node.title+'\')">'+Drupal.t('Add')+'</div></td>';
    html += '<td><a href="/node/'+node.nid+'">'+node.title+'</a></td>';
    html += '<td>'+node.language+'</td>';
    html += '<td>'+node.type+'</td>';
    html += '<td>'+node.published+'</td>';
    html += '<td>'+node.changed+'</td>';
    html += '</tr>';
  }
  
  
  if(typeof $(':input[name=offset]')[0] != 'undefined') {
    html += '<tr><td><input type="hidden" name="offset" id="offset" value="'+offset+'" />';
  }
  else if(typeof $(':input[name=batch_add_search_offset]')[0] != 'undefined') {
    html += '<tr><td><input type="hidden" name="batch_add_search_offset" id="batch_add_search_offset" value="'+offset+'" />';
  }
  
  
  if (typeof $(':input[name=jdata]')[0] != 'undefined'){
    html += '<input type="hidden" name="jdata" id="jdata" value="'+escape(JSON.stringify(jdata))+'" /></td></tr>';
  }
  else if (typeof $(':input[name=batch_add_search_jdata]')[0] != 'undefined'){
    html += '<input type="hidden" name="batch_add_search_jdata" id="batch_add_search_jdata" value="'+escape(JSON.stringify(jdata))+'" /></td></tr>';
    jdata = $(':input[name=batch_add_search_jdata]')[0].value;
  }
  
  
    
  if(typeof $(':input[name=sort_direction]')[0] == 'undefined') {
    $("#main_1").append('<input type="hidden" name="sort_direction" value="asc" />');
  }
  else if($(':input[name=sort_direction]')[0].value == 'desc') {
    $(':input[name=sort_direction]')[0].value = 'asc';
  }
  else{
    $(':input[name=sort_direction]')[0].value = 'desc';
  }

  //$("#janitor_table_body").html('');
  $("#batchadd_table_body").html(html);
}

function batch_add_opentree() {
  var tree = batch_add_getCurriculumTree(Drupal.settings.ndla_ontopia_connect.language,'ndlahtml');
  if ($("#relateframe").length == 0) {
    $("body").append("\
    <div id='relateframe'>\
        <div id='relateframe_veil' style=''>\
        <div id='relateframe_close'>&nbsp;</div>\
        <div class='browser'>"+tree+"\
        </div>\
        </div>\ <style type='text/css'>\
            #relateframe_veil { display: none; position: fixed; width: 800px; height: 800px; top: 0; left: 0; background-color: rgba(255,255,255,.25); cursor: pointer; z-index: 900; }\
            #relateframe_veil p { color: black; font: normal normal bold 20px/20px Helvetica, sans-serif; position: absolute; top: 50%; left: 50%; width: 10em; margin: -10px auto 0 -5em; text-align: center; }\
            #relateframe_close{height: 25px; width: 25px; float: right; cursor: pointer; cursor: hand; background: transparent url('http://relate.ndla.no/sites/all/modules/ndla_relate_public_service/img/close.png') no-repeat}\
        </style>\
    </div>");
    $("#relateframe_veil").fadeIn(750);
  }
  
  $("#relateframe_close").click(function(event){
    $("#relateframe_veil").fadeOut(750);
    setTimeout("$('#relateframe').remove()", 750);
  });
}

/**
 * This function fetches a single Subject curriculum as an HTML tree
 * @param uuid
 * @param parentid
 * @return null
 */
function batch_add_getCurriculaBySubject(uuid,language,format) {
  var odata = {"method" : "get.grep", "subject" : uuid, "language" : language, "format" : format};
  $.ajax({
    url: Drupal.settings.ndla_ontopia_connect.relate_service_url+"/services/json",
    dataType: "jsonp",
    data: odata,
    jsonp: "callback",
    success: function(data){
  
      if (typeof(data["#error"]) !== 'undefined' && data["#error"] == false) {
        aimcallback(data["#data"],language);
        
      } else {
        alert("Error: " + data["#data"]);
      }
    },
      error: function(xhr, status, errorThrown) {
      alert("Cannot connect to the Internet.");
    }
  });
}

/**
 * This functions fetches all curricula as an HTMLtree
 * @return
 */
function batch_add_getCurriculumTree(language,format) {
  
  filterUrl = Drupal.settings.ndla_ontopia_connect.laereplan_by_user+"/json";
  var filters = new Array();
  $.ajax({
  type: 'GET',
  url: filterUrl,
  dataType: 'json',
  success: function(data) {
    for (var i = 0; i< data.planer.length; i++) {
      if(typeof(data.planer[i]) !== 'undefined'){
        filters.push(data.planer[i].uuid);
      }
    }
    
    if (filters.length > 0) {
      var odata = {"method" : "get.grep", "subject" : 'full_tree', "language" : language, "format" : format, "aim_filter" : jQuery.toJSON(filters)};
      $.ajax({
        url: Drupal.settings.ndla_ontopia_connect.relate_service_url+"/services/json",
        dataType: "jsonp",
        data: odata,
        jsonp: "callback",
        success: function(data){
      
          if (typeof(data["#error"]) !== 'undefined' && data["#error"] == false) {
            treecallback(data["#data"],language);
            
          } else {
            alert("Error: " + data["#data"]);
          }
        },
          error: function(xhr, status, errorThrown) {
          alert("Cannot connect to the Internet.");
        }
      });
    }
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    $("#ontopia_connect_erorrs").html(Drupal.t('<p>An error occured. Unable to fetch data.</p>'));
  }
  });
}

labels = new Array();
labels["en"] = {'title' : 'Title', 'resource_type' : 'Resource type', 'curricula' : 'Curricula', 'curriculum' : 'Curriculum', 'all_curricula' : 'All curricula','error_aim' : 'You have to choose a competence aim','error_userid' : 'You have to enter either a user id or a valid email address','error_resourceurl' : 'You have to enter a URL for the resource you want to connect to the competence aim(s)','error_resourcetitle' : 'You have to enter a title for the resource you want to connect to the competence aim(s)','error_resourcetype' : 'You have to choose what kind of resource type you are relating to the competence aim(s)'};
labels["nb"] = {'title' : 'Tittel', 'resource_type' : 'Ressurstype', 'curricula' : 'L&aelig;replaner', 'curriculum' : 'L&aelig;replan', 'all_curricula' : 'Alle l&aelig;replaner','error_aim' : 'Du må velge et kompetansemål','error_userid' : 'Du må angi enten en bruker-id eller en gyldig epostadresse','error_resourceurl' : 'Du må skrive inn en URL for ressursen du ønsker å relatere til kompetansemålet/ene','error_resourcetitle' : 'Du må skrive inn en tittel for ressursen du ønsker å relatere til kompetansemålet/ene','error_resourcetype' : 'Du må velge hvilken ressurstype det er du relaterer til kompetansemålet/ene'};
labels["nn"] = {'title' : 'Tittel', 'resource_type' : 'Ressurstype', 'curricula' : 'L&aelig;replanar', 'curriculum' : 'L&aelig;replan', 'all_curricula' : 'Alle l&aelig;replanar','error_aim' : 'Du må velje eit kompetansemål','error_userid' : 'Du må angi enten ein brukar-id eller ei gyldig epostadresse','error_resourceurl' : 'Du må skrive inn ein URL for ressursen du ynskjer å relatere til kompetansemålet/a','error_resourcetitle' : 'Du må skrive inn ein tittel for ressursen du ynskjer å relatere til kompetansemålet/a','error_resourcetype' : 'Du må velge kva slags ressurstype det er du relaterar til kompetansemålet/a'};



/**
 * This function functions as the callback function for batch_add_getCurriculaBySubject
 * @param rtndata
 * @return
 */
function aimcallback(rtndata,language) {
  $('.browser').children().remove();
  //$('.browser').html('');
  $('.grep-loader').remove();
  //$('.browser').html('<p>rtndata</p>');
  $('.browser').append(rtndata);
  $("#curriculum_tree").slideDown(500,function(){});
  $("#curricula_tab").html(labels[language].curriculum);
  $("#curricula_tab").slideDown(500,function(){});
  $("#tree_tab").css('display','block');
  
  if(typeof($(':input[name=relate_resource_url]')[0]) != 'undefined' && $(':input[name=relate_resource_url]')[0].value != null) {
    $(':input[name=relate_resource_url]')[0].value = location.href;
  }
  
  $('#curriculum_tree-LaereplanNode').children('.expandNode').click(function() {
    var contents = $(this).parent().children(".NodeContents");
    if (contents.length > 0) {
      contents.toggle();
      if(contents.css('display') != "none")
      {
        if (typeof($(this)) !== 'undefined') {
          $(this).attr("class", "expandNode collapse");
        }
        
      }
      else
      {
        if (typeof($(this)) !== 'undefined') {
          $(this).attr("class", "expandNode expand");
        }
      }
    }
  });
  
  $('.hovedomraade_node').children('.expandNode').click(function() {
    var contents = $(this).parent().children(".NodeContents");
    contents.toggle();
    if(contents.css('display') != "none")
    {
      $(this).attr("class", "expandNode collapse");
    }
    else
    {
      $(this).attr("class", "expandNode expand");
    }
  });
  
  $('.kompetansemaal_node').children('.expandNode').click(function() {
    var contents = $(this).parent().children(".NodeContents");
    var uuid = $(this).parent().attr('id').substr(0,$(this).parent().attr('id').lastIndexOf('-KompetansemaalNode'));
    contents.toggle();
    if(contents.css('display') != "none")
    {
      $(this).attr("class", "expandNode collapse");
      $('#maal_'+uuid).attr('checked','checked');
      $('#maal_'+uuid).attr('class','newport-checkbox');
    }
    else
    {
      $('#maal_'+uuid).removeAttr('checked');
      $('#maal_'+uuid).attr('class','newport-checkbox ochidden');
      $(this).attr("class", "expandNode expand");
    }
  });

  $('#lang_en').click(function (){
    if (typeof($(':input[name=relate_curricula_id]')[0]) !== 'undefined') {
      var uuid = $(':input[name=relate_curricula_id]')[0].value;
      batch_add_getCurriculaBySubject(uuid,'en','ndlahtml');
    }
    else {
      batch_add_getCurriculumTree('en','ndlahtml');
    }
  });

  $('#lang_nb').click(function (){
    if (typeof($(':input[name=relate_curricula_id]')[0]) !== 'undefined') {
      var uuid = $(':input[name=relate_curricula_id]')[0].value;
      batch_add_getCurriculaBySubject(uuid,'nb','ndlahtml');
    }
    else {
      batch_add_getCurriculumTree('nb','ndlahtml');
    }
  });

 $('#lang_nn').click(function (){
   if (typeof($(':input[name=relate_curricula_id]')[0]) !== 'undefined') {
     var uuid = $(':input[name=relate_curricula_id]')[0].value;
     batch_add_getCurriculaBySubject(uuid,'nn','ndlahtml');
   }
   else {
     batch_add_getCurriculumTree('nn','ndlahtml');
   }
 });
}

/**
 * This function functions as the callback function for batch_add_getCurriculumTree
 * @param rtndata
 * @return
 */
function treecallback(rtndata,language) {
  $('.browser').children().remove();
  $('.grep-loader').remove();
  $('.browser').html(rtndata);
  $("#curriculum_tree").slideDown(500,function(){});
  $("#curricula_tab").html(labels[language].curricula);
  $("#curricula_tab").slideDown(500,function(){});
  $("#tree_tab").css('display','none');
  
  
  $('.expandNode').click(function() {
    var contents = $(this).parent().children(".NodeContents");
    var uuid = $(this).children().filter('.uuid').attr('id').replace('_',':');
    var parentid = $(this).parent().attr('id');
   
    $("#curricula_tab").slideUp(500,function(){});  
    $("#tree_tab").parent().append($('<div class="grep-loader"></div>'));
    $("#tree_tab").slideUp(500,function(){});
   

    $("#curriculum_tree").slideUp(200,function(){});
    
   
  
    if (contents.length > 0) {
      contents.toggle();
      if(contents.css('display') != "none")
      {
        if (typeof($(this)) !== 'undefined') {
          $(this).attr("class", "expandNode collapse");
        }
        
      }
      else
      {
        if (typeof($(this)) !== 'undefined') {
          $(this).attr("class", "expandNode expand");
        }
      }
    }
    var language = $(':input[name=relate_language]')[0].value;  
    var format = $(':input[name=relate_format]')[0].value;
    
    batch_add_getCurriculaBySubject(uuid,language,format);
    
  });
  
  $('#lang_en').click(function (){
    if (typeof($(':input[name=relate_curricula_id]')[0]) !== 'undefined') {
      var uuid = $(':input[name=relate_curricula_id]')[0].value;
      batch_add_getCurriculaBySubject(uuid,'en','ndlahtml');
    }
    else {
      batch_add_getCurriculumTree('en','ndlahtml');
    }
  });

  $('#lang_nb').click(function (){
    if (typeof($(':input[name=relate_curricula_id]')[0]) !== 'undefined') {
      var uuid = $(':input[name=relate_curricula_id]')[0].value;
      batch_add_getCurriculaBySubject(uuid,'nb','ndlahtml');
    }
    else {
      batch_add_getCurriculumTree('nb','ndlahtml');
    }
  });

 $('#lang_nn').click(function (){
   if (typeof($(':input[name=relate_curricula_id]')[0]) !== 'undefined') {
     var uuid = $(':input[name=relate_curricula_id]')[0].value;
     batch_add_getCurriculaBySubject(uuid,'nn','ndlahtml');
   }
   else {
     batch_add_getCurriculumTree('nn','ndlahtml');
   }
 });
}

function addNode(nodeid,nodetitle) {
  
  if (typeof $(':input[name=hidden_chosen_resources]')[0] != 'undefined') {
    var existingdata = $.evalJSON($(':input[name=hidden_chosen_resources]')[0].value);
    var nodedata = new Object();
    nodedata.nid = nodeid;
    nodedata.title = nodetitle;
    existingdata.nodedata.push(nodedata);
    jsondata = unescape(JSON.stringify(existingdata));
    $(':input[name=hidden_chosen_resources]')[0].value = jsondata;
  }
  else {
    var jsondata = new Object();
    jsondata.nodedata = new Array();
    var add_data = new Object();
    add_data.nid = nodeid;
    add_data.title = nodetitle;
    jsondata.nodedata.push(add_data);
    $("#chosen_resources").append('<input type="hidden" name="hidden_chosen_resources" value="" />');
    $(':input[name=hidden_chosen_resources]')[0].value = unescape(JSON.stringify(jsondata));
  }
  
  
  $("#resource_list").append('<li id="resource_'+nodeid+'"><span class="remove_button" onclick="removeNode('+nodeid+')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'+nodetitle+' ('+nodeid+')</li>');
  $("#row_"+nodeid).css('display','none');
  var collapser = $("#block-ndla_ontopia_connect_3").children().filter('.right-collapsible');
  var collapser_block = $("#block-ndla_ontopia_connect_3").children().filter('.right-collapse');
  if (collapser.hasClass('right-collapsed')) {
    collapser.removeClass('right-collapsed');
    collapser.addClass('right-expanded');
    collapser_block.fadeIn('500');
  }
  
  
}

function removeNode(nodeid) {
  $("#resource_"+nodeid).remove();
  $("#row_"+nodeid).css('display','table-row');
  if($("#resource_list").children().length == 0 && $("#aim_list").children().length == 0) {
    var collapser = $("#block-ndla_ontopia_connect_3").children().filter('.right-collapsible');
    var collapser_block = $("#block-ndla_ontopia_connect_3").children().filter('.right-collapse');
    collapser_block.fadeOut('500');
    collapser.removeClass('right-expanded');
    collapser.addClass('right-collapsed');
  }
  
  if (typeof $(':input[name=hidden_chosen_resources]')[0] != 'undefined') {
    var existingdata = $.evalJSON($(':input[name=hidden_chosen_resources]')[0].value);
    var nodes = existingdata.nodedata;
    for(var i = 0; i < nodes.length;i++) {
      if (nodes[i].nid == nodeid) {
        existingdata.nodedata.splice(i,1);
      }//end if
    }//end for
    
    if (existingdata.nodedata.length > 0) {
      $(':input[name=hidden_chosen_resources]')[0].value = unescape(JSON.stringify(existingdata));
    }
    else {
      $(':input[name=hidden_chosen_resources]').remove();
    }
  }//end if
}

function removeAim(aim_id) {
  $("#aim_"+aim_id).remove();
  
  if (typeof $(':input[name=hidden_chosen_aims]')[0] != 'undefined') {
    var existingdata = $.evalJSON($(':input[name=hidden_chosen_aims]')[0].value);
    var maalarr = existingdata.grepdata.maaldata;
    for (var j = 0; j < maalarr.length; j++) {
      if(maalarr[j] == aim_id.replace('_',':')) {
        existingdata.grepdata.maaldata.splice(j,1);
        existingdata.grepdata.assocdata.splice(j,1);
      }//end if
    }//end for
    
    if (existingdata.grepdata.maaldata.length > 0) {
      $(':input[name=hidden_chosen_aims]')[0].value = unescape(JSON.stringify(existingdata));
    }
    else {
      $(':input[name=hidden_chosen_aims]').remove();
    }
    
  }//end if
  
  
  if($("#resource_list").children().length == 0 && $("#aim_list").children().length == 0) {
    var collapser = $("#block-ndla_ontopia_connect_3").children().filter('.right-collapsible');
    var collapser_block = $("#block-ndla_ontopia_connect_3").children().filter('.right-collapse');
    collapser_block.fadeOut('500');
    collapser.removeClass('right-expanded');
    collapser.addClass('right-collapsed');
  }
}

function batchGetPage(direction) {
  
  if(typeof $(':input[name=current_sort_type]')[0] != 'undefined') {
    $('.th_sort_header').each(function(){
      $(this).removeClass('down');
      $(this).removeClass('up');
    });
  }
  
  var offset = $(':input[name=offset]')[0].value;
  
  if (direction == 'next') {
    var jasonUrl = Drupal.settings.ndla_ontopia_connect.batch_add_json_url+"/"+offset;
  }
  else if (direction == 'previous') {
    var jasonUrl = Drupal.settings.ndla_ontopia_connect.batch_add_json_url+"/"+(offset-200);
  }
  
  //alert (jasonUrl);
  $("#batchadd_table_body").html('');
  $("#batchadd_table_body").html('<tr><td colspan="7" class="aim_window_loader_nb"></td></tr>');
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
    batchShowPage(data);
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    $("#ontopia_connect_errors").html(Drupal.t('<p>Unable to fetch data.</p>'));
  }
  
  });
}

function batchShowPage(data) {
  if(data.count == 0) {
    $(".messages").remove();
    $("#headerSeparator").prepend('<div class="messages error">' + Drupal.t('Something went wrong, and the nodes could not be fetched. Contact administrator') + '</div>');
  }
  else {
    $("#batchadd_table_body").html(data.html);
    Drupal.attachBehaviors($('#batchadd_table_body'));
    
    
    if($(':input[name=offset]')[0].value > 100) {
      $("#batchadd_get_next").before('<div id="batchadd_get_previous" onclick="batchGetPage(\'previous\')">&lt;&lt; '+Drupal.t('Previous')+'</div>');
      $("#batchadd_get_previous").css('display','inline');
    } else {
      $("#batchadd_get_previous").remove();
    } 
  }
}

function batch_add_aims() {
  var language = $(':input[name=relate_language]')[0].value;
  var errors = new Array();
  
  var maalarr = new Array();
  var maalnamearr = new Array();
  var assoctypearr = new Array();
  var data = new Array();
  
  $('.assoc-checkbox').each(function(){
    if ($(this).is(':checked')){
     var uuid = 'uuid_'+$(this).attr('name').substr($(this).attr('name').lastIndexOf('_')+1);
     if($('#maal_'+uuid).is(':checked')) {
       maalnamearr.push($('#maal_'+uuid).next().html());
       maalarr.push(uuid.replace('_',':'));
       assoctypearr.push($(this)[0].value);
     }
    }
  });
  
  
  var maalant = maalarr.length;
  
  if (maalant < 1) {
    errors.push(labels[language]['error_aim']);
  }
  
  if (errors.length > 0) {
    var html = '<ul>';
    for(var i = 0; i< errors.length;i++) {
      html += '<li>'+errors[i]+'</li>';
    }
    html += '</ul>';
    $('#relate_error_message').html(html);
    $('#relate_error_message').css('display','block');
    return false;
  }
  else {
    if (typeof $(':input[name=hidden_chosen_aims]')[0] != 'undefined') {
      var existingdata = $.evalJSON($(':input[name=hidden_chosen_aims]')[0].value);
      for (var j = 0; j < maalarr.length; j++) {
        existingdata.grepdata.maaldata.push(maalarr[j]);
        existingdata.grepdata.assocdata.push(assoctypearr[j]);
      }//end for
      $(':input[name=hidden_chosen_aims]')[0].value = unescape(JSON.stringify(existingdata));
    }
    else {
      var jsondata = new Object();
      jsondata.grepdata = new Object();
      jsondata.grepdata.maaldata = maalarr;
      jsondata.grepdata.assocdata = assoctypearr;
      $("#chosen_aims").append('<input type="hidden" name="hidden_chosen_aims" value="" />');
      $(':input[name=hidden_chosen_aims]')[0].value = unescape(JSON.stringify(jsondata));
    }
    
    var aimhtml = '';
    
    for (var i = 0; i < maalarr.length; i++) {
      $("#aim_list").append('<li id="aim_'+maalarr[i].replace(':','_')+'"><span class="remove_button" onclick="removeAim(\''+maalarr[i].replace(':','_')+'\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'+maalnamearr[i]+' ('+assoctypearr[i]+')</li>');
    }
  
    $("#relateframe_veil").fadeOut(750);
    setTimeout("$('#relateframe').remove()", 750);
  }//end if errors.length 
}


function batch_add_addresources(force_duplicate) {
  
  if(typeof force_duplicate == 'undefined') {
    force_duplicate = 'false';
  }
  
  $(".messages").remove();
  var data = new Array();
  var errors = new Array();
  
  var resource_urls = new Array();
  var resource_titles = new Array();
  var resource_types = new Array();
  
  if (typeof $(':input[name=hidden_chosen_resources]')[0] != 'undefined') {
    var resourcedata = $.evalJSON($(':input[name=hidden_chosen_resources]')[0].value);
    for (var i = 0; i < resourcedata.nodedata.length; i++) {
      var resource = resourcedata.nodedata[i];
      resource_urls[i] = 'http://ndla.no/node/'+resource.nid;
      resource_titles[i] = resource.title;
      resource_types[i] = 'ndla-node';
    }//end for
    data[0] = resource_urls;
    data[1] = resource_titles;
    data[2] = resource_types;
  }
  else {
    errors.push(Drupal.t('You must add resources to be related'));
  }
  
  
  var aim_ids = new Array();
  var assoctypes = new Array();
  
  if (typeof $(':input[name=hidden_chosen_aims]')[0] != 'undefined') {
    var aimdata = $.evalJSON($(':input[name=hidden_chosen_aims]')[0].value);
    var maaldata = aimdata.grepdata.maaldata;
    var assocdata = aimdata.grepdata.assocdata;
    for (var k = 0; k < maaldata.length; k++) {
       aim_ids[k] = maaldata[k];
       assoctypes[k] = assocdata[k];
    }//end for
    data[3] = aim_ids;
    data[4] = assoctypes;
  }
  else {
    errors.push(Drupal.t('You must add competence aims to relate resources to'))
  }
  
  

  if (errors.length > 0) {
    var errorlist = '';
    for (var j=0; j < errors.length; j++) {
      var errorlist = '<li>'+errors[j]+'</li>';
    }

    $("#headerSeparator").prepend('<div class="messages error"><ul>' + errorlist + '</ul></div>');
    errors = new Array();
    return false;
  }
  else {
    $(".messages").remove();
    
    var user_id = $(':input[name=grep-user-name]')[0].value;
    var user_email = '';
    var user_url = 'http://ndla.no/'+$(':input[name=grep-user-uid]')[0].value;
    var subject = 'naturfag';
    var format = 'json';
    var odata = null;
    
    odata = {
        "method" : "add.resources", 
        "curricula_id" : "curricula_id", 
        "data" : jQuery.toJSON(data), 
        "user_id" : user_id,
        "user_email" : user_email, 
        "user_url" : user_url,
        "subject": subject, 
        "format" : format,
        "force_duplicate" : force_duplicate
    };
   
    //alert(JSON.stringify(odata));
    
    jQuery.ajax({
      url: Drupal.settings.ndla_ontopia_connect.relate_service_url+"/services/json",
      dataType: "jsonp",
      data: odata,
      jsonp: "callback",
      success: function(data){

        if (typeof(data["#error"]) !== 'undefined' && data["#error"] == false) {
          //alert("DATUR: "+data["#data"]);
          var res = jQuery.evalJSON(data["#data"]);
          var haserror = false;
          var haswarning = false;
          var list = '<ul>';

          if(typeof res.messages[0].message == 'undefined') {
            if (typeof res.messages[0].warnings != 'undefined') {
              for(var i = 0; i < res.messages[0].warnings.length;i++) {
                list += '<li>'+Drupal.t('WARNING: '+res.messages[0].warnings[i].warning)+'</li>';
              }
            
            }
          
            if (typeof res.messages[0].errors != 'undefined' || typeof res.messages[1] != 'undefined') {
              if (typeof res.messages[0].errors != 'undefined') {
                for(var i = 0; i < res.messages[0].errors.length;i++) {
                  list += '<li>'+Drupal.t('ERROR: '+res.messages[0].errors[i].error)+'</li>';
                }
              }
              else if(typeof res.messages[1].errors != 'undefined'){
                for(var i = 0; i < res.messages[1].errors.length;i++) {
                  list += '<li>'+Drupal.t('ERROR: '+res.messages[1].errors[i].error)+'</li>';
                }
              }
            }
            list += '</ul>';
            if (typeof res.messages[0].warnings != 'undefined' && (typeof res.messages[0].errors == 'undefined' && typeof res.messages[1] == 'undefined')) {
              list += '<br /><br /><div id="batchadd_force_check">'+Drupal.t('Yes')+': <input type="checkbox" name="force_save" id="force_save" value="yes" /><input type="button" name="batchadd_force_button" id="batchadd_force_button" value="'+Drupal.t('Continue')+'" onclick="batch_add_addresources(\'true\')" /></div>';
            }
            
            $("#navigators").prepend('<div class="messages error">' + list + '</div>');
          }else {
            $("#headerSeparator").prepend('<div class="messages status">' + Drupal.t(res.messages[0].message) + '</div>');
            $("#navigators").fadeOut(500);
            $("#node_table").fadeOut(500);
            
            $("#resource_list").html('');
            $("#aim_list").html('');
            var collapser = $("#block-ndla_ontopia_connect_3").children().filter('.right-collapsible');
            var collapser_block = $("#block-ndla_ontopia_connect_3").children().filter('.right-collapse');
            collapser_block.fadeOut('500');
            collapser.removeClass('right-expanded');
            collapser.addClass('right-collapsed');
          }
        }
      },
        error: function(xhr, status, errorThrown) {
        alert("Cannot connect to the Internet.");
        $("#headerSeparator").prepend('<div class="messages error"><ul>' + Drupal.t('Cannot connect to the Internet') + '</ul></div>');
      }
    });
  } 
}

function batch_add_searchNodes() {
  var search = $(':input[name=batch_add_search_title]')[0].value;
  
  $("#batchadd_table_body").html('');
  
  if(typeof $(':input[name=batch_add_search_type]')[0] != 'undefined' && typeof $(':input[name=batch_add_search_type]:checked').val() != 'undefined') {
    $(".messages").remove();
    var search_type = $(':input[name=batch_add_search_type]:checked').val();
  }
  else{
    $("#headerSeparator").prepend('<div class="messages error">' + Drupal.t('You must choose whether you want to search for title or node id') + '</div>');
    return false;
  }
  
  
  $("#batchadd_table_body").html('<tr><td colspan="7" class="aim_window_loader_nb"></td></tr>');
  
  var jasonUrl = Drupal.settings.ndla_ontopia_connect.batch_search_json_url+"/"+search+"/"+search_type
  
  $.ajaxSetup({ 
    scriptCharset: "utf-8" , 
    contentType: "application/x-www-form-urlencoded; charset=utf-8"
  });
  
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
    //console.log(JSON.stringify(data.json));
    
    if(typeof($(':input[name=batch_add_search_jdata]')[0]) != 'undefined') {
      $(':input[name=batch_add_search_jdata]').remove();
    }
    
    if(typeof($(':input[name=batch_add_search_count]')[0]) != 'undefined') {
      $(':input[name=batch_add_search_count]').remove();
    }
   
    
    if(typeof($(':input[name=batch_add_search_offset]')[0]) != 'undefined') {
      $(':input[name=batch_add_search_offset]').remove();
    }
    
    batch_add_showSearch(data,'next');
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_errors").html(Drupal.t('<p>Unable to fetch data.</p>'));
    }
    
    });
  
}

function batch_add_showSearch(data,direction) {
  if(typeof $(':input[name=current_sort_type]')[0] != 'undefined') {
    $('.th_sort_header').each(function(){
      $(this).removeClass('down');
      $(this).removeClass('up');
    });
  }
  
  var json = '';
  var navig = false;
  if (data == '') {
    data = unescape($(':input[name=batch_add_search_jdata]')[0].value);
    json = $.evalJSON(data);
    navig = true;
  }
  else {
    json = $.evalJSON(data.json);  
  }

  if(data.count == 0) {
    
    if (navig){
      var search = $(':input[name=batch_add_search_title]')[0].value;
      var params = {"@search" : search};
    }
    else {
      var params = {"@search" : data.search};  
    }
    
    $(".messages").remove();
    $("#headerSeparator").prepend('<div class="messages error">' + Drupal.t('The search "@search" did not return any results',params) + '</div>');
  }
  else {
    
    var current = 0;
    var show_length = 0;
    
    if (json.length < 20) {
      show_length = json.length;
    }
    else {
      
      if (typeof($(':input[name=batch_add_search_offset]')[0]) != 'undefined') {
        var offset = $(':input[name=batch_add_search_offset]')[0].value;
        current = (offset*1);
        
        if (direction == 'next'){
          show_length = ((offset*1) + 20);
        }
        else {
          show_length = ((offset*1) - 20);
          current = (show_length -20);
        }
        
      }
      else {
        show_length = 20;
      }
      
    }
    
    if (navig){
      var search = $(':input[name=batch_add_search_title]')[0].value;
      var count = $(':input[name=batch_add_search_count]')[0].value;
      if (show_length > count) {
        var params = {"@search" : search, "@data_count" : count, "@current" : current, "@offset" : count};
      }
      else {
        var params = {"@search" : search, "@data_count" : count, "@current" : current, "@offset" : show_length};
      }
      
    }
    else {
      if (show_length > data.count) {
        var params = {"@search" : data.search, "@data_count" : data.count, "@current" : current, "@offset" : data.count}; 
      }
      else {
        var params = {"@search" : data.search, "@data_count" : data.count, "@current" : current, "@offset" : show_length};
      }
      
    }
    
    $(".messages").remove();
    $("#headerSeparator").prepend('<div class="messages status">' + Drupal.t('The search "@search" returned @data_count results',params) + '<br /> '+Drupal.t('Showing from @current to @offset out of @data_count',params)+'</div>');
    
    var html = '';
    var nav = '';
    
    
    if (current > 0) {
      nav += '<div id="batch_add_search_get_previous" onclick="batch_add_showSearch(\'\',\'previous\')">&lt;&lt; '+Drupal.t('Previous')+'</div>';
    }
    
    if (navig) {
      if (show_length >= 20 && show_length < count) {      
        nav += '<div id="batch_add_search_get_next" onclick="batch_add_showSearch(\'\',\'next\')">'+Drupal.t('Next')+' &gt;&gt;</div>';
      }
    }
    else {
      if (show_length >= 20 && show_length < data.count) {      
        nav += '<div id="batch_add_search_get_next" onclick="batch_add_showSearch(\'\',\'next\')">'+Drupal.t('Next')+' &gt;&gt;</div>';
      }
    }
    
    $('#navigators').html(nav);
    
    
    if (current > 0) {
      $('#batch_add_search_get_previous').css('display','block');
    }
    
    for(var i = current; i < show_length; i++ ) {
      var node = json[i];
      if(i%2 == 1) {
        var stripes = 'odd';
      }
      else {
        var stripes = 'even';
      }

      html += '<tr class="'+stripes+'" id="row_'+node.nid+'">';
      html += '<td><div class="add_node_button" id="node_button_'+node.nid+'" onclick="addNode('+node.nid+',\''+node.title+'\')">'+Drupal.t('Add')+'</div></td>';
      html += '<td><a href="/node/'+node.nid+'">'+node.title+'</a></td>';
      html += '<td>'+node.language+'</td>';
      html += '<td>'+node.type+'</td>';
      html += '<td>'+node.published+'</td>';
      html += '<td>'+node.changed+'</td>';
      html += '</tr>';
      
    }//end for
    
    $("#batchadd_table_body").html('');
    $("#batchadd_table_body").html(html);
    $('#flush_search').css('display','block');
    
    if(typeof($(':input[name=batch_add_search_jdata]')[0]) == 'undefined') {
      $("#main_1").append('<input type="hidden" name="batch_add_search_jdata" value="" />');
      $(':input[name=batch_add_search_jdata]')[0].value = data.json;
    }
    else {
      $(':input[name=batch_add_search_jdata]')[0].value = data;
    }
    
    
    if(typeof($(':input[name=batch_add_search_count]')[0]) == 'undefined') {
      $("#main_1").append('<input type="hidden" name="batch_add_search_count" value="'+data.count+'" />');
    }
    else {
      $(':input[name=batch_add_search_count]')[0].value = count;
    }
    

    
    
    if(typeof($(':input[name=batch_add_search_offset]')[0]) == 'undefined') {
      $("#main_1").append('<input type="hidden" name="batch_add_search_offset" value="'+show_length.toString()+'" />');
    }
    else {
      $(':input[name=batch_add_search_offset]')[0].value = show_length.toString();
    }
  }
}

function compareTitle(a,b) {
  var titleA = trim(a.title);
  var titleB = trim(b.title);
  if (titleA < titleB) {return -1}
  if (titleA > titleB) {return 1}
  return 0;
}

function compareTitleRev(a,b) {
  var titleA = trim(a.title);
  var titleB = trim(b.title);
  if (titleA > titleB) {return -1}
  if (titleA < titleB) {return 1}
  return 0;
}

function compareLang(a,b) {
  var titleA = trim(a.language);
  var titleB = trim(b.language);
  if (titleA < titleB) {return -1}
  if (titleA > titleB) {return 1}
  return 0;
}

function compareLangRev(a,b) {
  var titleA = trim(a.language);
  var titleB = trim(b.language);
  if (titleA > titleB) {return -1}
  if (titleA < titleB) {return 1}
  return 0;
}

function compareType(a,b) {
  var titleA = trim(a.type);
  var titleB = trim(b.type);
  if (titleA < titleB) {return -1}
  if (titleA > titleB) {return 1}
  return 0;
}

function compareTypeRev(a,b) {
  var titleA = trim(a.type);
  var titleB = trim(b.type);
  if (titleA > titleB) {return -1}
  if (titleA < titleB) {return 1}
  return 0;
}


function comparePublished(a,b) {
  var pubA = a.published;
  var pubB = b.published;
  if (pubA < pubB) {return -1}
  if (pubA > pubB) {return 1}
  return 0;
}

function comparePublishedRev(a,b) {
  var pubA = a.published;
  var pubB = b.published;
  if (pubA > pubB) {return -1}
  if (pubA < pubB) {return 1}
  return 0;
}

function compareChanged(a,b) {
  var pubA = a.timestamp;
  var pubB = b.timestamp;
  if (pubA < pubB) {return -1}
  if (pubA > pubB) {return 1}
  return 0;
}

function compareChangedRev(a,b) {
  var pubA = a.timestamp;
  var pubB = b.timestamp;
  if (pubA > pubB) {return -1}
  if (pubA < pubB) {return 1}
  return 0;
}