curriculatree = function(tData,container,modus,view)
{

  this.build = function(nodeInfo,div_id,modus)
  {
    if (modus == "curriculum_listing") {
      var stem = $('<div id="laereplaner" class="GREPTreeView"></div>');
      var length = nodeInfo.values.length;
     
      if (length > 0) {
   
        for(var i=0;i< length;i++) {
            
          var node = $('<div id="plan_'+nodeInfo.values[i].uuid.replace(':','_')+'-LaereplanNode">'+nodeInfo.values[i].name+'</div>')
          .css({'margin-top': 5});
          $('<div class="expandNode expand"><span id="'+nodeInfo.values[i].uuid.replace(':','_')+'" class="uuid" /></div>')
          .prependTo(node);
         
            node.children('.expandNode').click(function() {
            var contents = $(this).parent().children(".NodeContents");
            var uuid = $(this).children().filter('.uuid').attr('id').replace('_',':');
            var parentid = $(this).parent().attr('id');

            if (view == 'all_deling') {
              $("#all_link").slideUp(500,function(){});  
              $("#all_deling_link").parent().append($('<div class="grep-loader"></div>'));
              $("#all_deling_link").slideUp(500,function(){});
            }
            else {
              $("#all_link").slideUp(500,function(){});
              $("#user_link").slideUp(500,function(){});
              $("#all_link").parent().append($('<div class="grep-loader"></div>'));
            }

            $("#curriculum_tree").slideUp(500,function(){});

            getCurriculaBySubject(uuid,parentid);
            if (contents.length > 0) {
              contents.toggle();
              if(contents.css('display') != "none")
              {
                if (typeof($(this)) !== 'undefined') {
                  $(this).attr("class", "expandNode collapse");
                }
                
              }
              else
              {
                if (typeof($(this)) !== 'undefined') {
                  $(this).attr("class", "expandNode expand");
                }
              }
            }
            
           
          });
          stem.append(node);
        }//end for
            $('.grep-loader').remove();
            $("#all_link").html(Drupal.t('Curriculum'));
            $("#all_link").slideDown(500,function(){});

            $("#curriculum_tree").slideDown(500,function(){});
      }//end if

    }
    else {
      var all_deling = false;
      
      var stem = $('<div id="laereplan_'+div_id.attr('id')+'" class="GREPTreeView"></div>');
      var length = nodeInfo.values.length;
      if (length > 0) {
        for(var i=0;i< length;i++) {
          var node = $('<div id="'+div_id.attr('id')+'-LaereplanNode">'+nodeInfo.values[i].fag_tittel+'</div>')
          .css({'margin-top': 5});
          $('<div class="expandNode expand"></div>')
          .prependTo(node);
          
          var hovedomraade_contents = $('<div class="NodeContents"></div>');
          var fag_data = nodeInfo.values[i].fag_data
          var hlen = fag_data.length;
          
          for(var j = 0; j < hlen; j++) {
            
            var hovedomraade_node = $('<div id="'+fag_data[j].hovedomraade_uid.replace(':','_')+'-HovedomraadeNode">'+fag_data[j].hovedomraade_tittel+'</div>')
            .css({'margin-top': 5});
            $('<div class="expandNode expand"></div>')
            .prependTo(hovedomraade_node);
            
            
            
            var kompetansemaal_contents = $('<div class="NodeContents"></div>');
            var maaldata = fag_data[j].maaldata;
            var mlen = maaldata.length;
            
            for(var k = 0; k < mlen; k++) {
              
              if (typeof(maaldata[k].scope_title) !== 'undefined') {
                all_deling = true;
                var kompetansemaal_node = $('<div id="'+maaldata[k].uuid.replace(':','_')+'-KompetansemaalNode"><input type="checkbox" name="maal_'+maaldata[k].uuid.replace(':','_')+'" id="maal_'+maaldata[k].uuid.replace(':','_')+'"  class="newport-checkbox ochidden" />&nbsp;<span class="curriculatree-aimtitle">'+maaldata[k].aim_title+' ('+maaldata[k].scope_title+')</span></div>')
                .css({'margin-top': 5});
                $('<div class="expandNode expand"><span id="'+maaldata[k].uuid.replace(':','_')+'" class="uuid" /></div>')
                .prependTo(kompetansemaal_node);
              }
              else {
                var kompetansemaal_node = $('<div id="'+maaldata[k].uuid.replace(':','_')+'-KompetansemaalNode"><input type="checkbox" name="maal_'+maaldata[k].uuid.replace(':','_')+'" id="maal_'+maaldata[k].uuid.replace(':','_')+'"  class="newport-checkbox ochidden" />&nbsp;<span class="curriculatree-aimtitle">'+maaldata[k].aim_title+'</span></div>')
                .css({'margin-top': 5});
                $('<div class="expandNode expand"><span id="'+maaldata[k].uuid.replace(':','_')+'" class="uuid" /></div>')
                .prependTo(kompetansemaal_node);
              }
              
              
              
              var relasjonsnode_contents = $('<div class="NodeContents"></div>');
              var relasjon_node = $('&nbsp; <div id="assoctypes_'+maaldata[k].uuid.replace(':','_')+'" class="port_assoctypes">'+getAssoctypes(maaldata[k].uuid.replace(':','_'))+'</div><div id="laerling_wrapper_'+maaldata[k].uuid.replace(':','_')+'"><label>'+Drupal.t('For apprentices')+'</label><input type="checkbox" id="laerling_check_'+maaldata[k].uuid.replace(':','_')+'" name="laerling_check_'+maaldata[k].uuid.replace(':','_')+'" /></div>').appendTo(relasjonsnode_contents);

              kompetansemaal_node.append(relasjonsnode_contents);
              kompetansemaal_contents.append(kompetansemaal_node);

              kompetansemaal_node.children('.expandNode').click(function() {
                var contents = $(this).parent().children(".NodeContents");
                var uuid = $(this).parent().attr('id').substr(0,$(this).parent().attr('id').lastIndexOf('-KompetansemaalNode'));
                
                contents.toggle();
                if(contents.css('display') != "none")
                {
                  $(this).attr("class", "expandNode collapse");
                  $('#maal_'+uuid).attr('checked','checked');
                  $('#maal_'+uuid).attr('class','newport-checkbox');
                }
                else
                {
                  $('#maal_'+uuid).removeAttr('checked');
                  $('#maal_'+uuid).attr('class','newport-checkbox ochidden');
                  $(this).attr("class", "expandNode expand");
                }
               
              });
            }//end for
            
            hovedomraade_node.append(kompetansemaal_contents);
           
            hovedomraade_contents.append(hovedomraade_node);
           
            
            
            hovedomraade_node.children('.expandNode').click(function() {
              var contents = $(this).parent().children(".NodeContents");
              contents.toggle();
              if(contents.css('display') != "none")
              {
                $(this).attr("class", "expandNode collapse");
              }
              else
              {
                $(this).attr("class", "expandNode expand");
              }
             
            });
          }//end for

          node.append(hovedomraade_contents);
          
          
          node.children('.expandNode').click(function() {
            var contents = $(this).parent().children(".NodeContents");
            if (contents.length > 0) {
              contents.toggle();
              if(contents.css('display') != "none")
              {
                if (typeof($(this)) !== 'undefined') {
                  $(this).attr("class", "expandNode collapse");
                }
                
              }
              else
              {
                if (typeof($(this)) !== 'undefined') {
                  $(this).attr("class", "expandNode expand");
                }
              }
            }
            
           
          });
          
          stem.append(node);
          
        }//end for
        $('.grep-loader').remove();
        $("#curriculum_tree").slideDown(500,function(){});
        
        if (all_deling) {
          $("#all_link").html(Drupal.t('Curriculum'));
          $("#all_link").slideDown(500,function(){});
          $("#all_deling_link").css('display','block');
          
        }
        else {
          $("#user_link").html(Drupal.t('Curriculum'));
          $("#user_link").slideDown(500,function(){});
          if ($("#all_link").attr('class') == 'active-tab') {
            $("#all_link").html('<a href="javascript:" onclick="getCurriculumTree(\'all\')">'+Drupal.t('All curricula')+'</a>');
            $("#all_link").attr('class','inactive-tab');
            $("#user_link").attr('class','active-tab');
          }
          $("#all_link").slideDown(500,function(){});
        }
          
      }//end if
    }
    
    return stem;
  }
 
  this.tree = this.build(tData,container,modus);
  var treeCon = container;
  if (view == 'user') {
    treeCon.children().remove()
    treeCon.append(this.tree);
  }
  else {
    treeCon.children().remove()
    treeCon.append(this.tree);
  }
  
}


function getAssoctypes(id) {
  var lang = Drupal.settings.ndla_ontopia_connect.language;
  jstr = Drupal.settings.ndla_ontopia_connect.assoctypes;
  jstr = jstr.replace(/\\/g,'');
  var assoctypes = $.evalJSON(jstr);
  var options = '';
  
  for (var i in assoctypes) {
    options += '<input type="radio" name="assoc-radio_'+id+'" class="assoc-checkbox" value="'+i+'" /> '+assoctypes[i].kompetansemaal+'/'+assoctypes[i].laeringsressurs+'<br />';
  }
  
  return options;
}