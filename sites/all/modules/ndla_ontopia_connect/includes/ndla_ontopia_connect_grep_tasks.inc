<?php
// $Id$  ndla_ontopia_connect_grep_tasks.inc, v 1.0 2010/04/14 13:00 minimalismore Exp $
/**
 * @brief
 *  Provides functions for maipulating GREP relations
 * @ingroup ndla_ontopia_connect
 * @file
 */

function ndla_ontopia_connect_grep_tasks_list() {
    $form['tasks']['batch'] = array( 
      '#type' => 'markup',  
      '#value' => '<div id="batch_link">'.l(t('Batch delete GREP associations'), 'admin/build/grep/batch').'</div>',  
    );
    
    $form['tasks']['batch_resource'] = array( 
      '#type' => 'markup',  
      '#value' => '<div id="batch_resource_link">'.l(t('Batch add GREP associations'), 'admin/build/grep/batch_resource/0').'</div>',  
    );
    
    $form['tasks']['port'] = array( 
      '#type' => 'markup',  
      '#value' => '<div id="port_link">'.l(t('Port orphaned GREP relations'), 'admin/build/grep/port').'</div>',  
    );
    
    return $form;
}

function ndla_ontopia_connect_grep_tasks_batch_delete() {
  if(ndla_context()){
    drupal_add_js(drupal_get_path('module', 'ndla_ontopia_connect') . '/js/batch_treeview.js');
    drupal_add_css(drupal_get_path('module', 'ndla_ontopia_connect') . '/css/treeview.css');
  }
  
  $form = array();
  
  $faglist = _getFagList(false);

  
  foreach ($faglist as $nid => $fag){ 
    $i=0; 
    $faget = str_replace(' ','_',trim($fag));
    $faget = str_replace('æ','ae',trim($faget));
    $faget = str_replace('ø','oe',trim($faget));
    $faget = str_replace('å','aa',trim($faget));
    
       $form['batch']["ndla_ontopia_connect_grep_tasks_batch_delete_".$faget] = array( 
      '#type' => 'fieldset',  
      '#title' => $fag,
      '#collapsible' => TRUE, 
      '#collapsed' => TRUE,  
    );
    
    
    $form['batch']["ndla_ontopia_connect_grep_tasks_batch_delete_".$faget]['fetch_nodes_button'.$faget] = array( 
      '#type' => 'markup',  
      '#value' => '<div id="fetch_nodes_button_button_'.$faget.'"><input type="button" value="'.t('Fetch nodes').'" onclick="getCurriculaResourcesBySubject(\''.$nid.'\',\''.$faget.'\',\'plan\',\'\');return false " /></div>',  
    );
    
    
    
    $form['batch']["ndla_ontopia_connect_grep_tasks_batch_delete_".$faget]['delete_assocs_div'.$faget] = array( 
      '#type' => 'markup',  
      '#value' => '<div id="delete_assocs_div_'.$faget.'" class="curriculum-wrapper ochidden"></div>',  
    );
    
    $form['batch']["ndla_ontopia_connect_grep_tasks_batch_delete_".$faget]['delete_assocs_ltm_'.$faget] = array( 
      '#type' => 'hidden',    
    );
    
    
    $form['batch']["ndla_ontopia_connect_grep_tasks_batch_delete_".$faget]['delete_assocs_button'.$faget] = array( 
      '#type' => 'markup',  
      '#value' => '<div id="process_resource_button_'.$faget.'" class="resource-process-button ochidden"><input type="button" value="'.t('Process associations').'" onclick="processCurriculaAssocs(\''.$nid.'\',\''.$faget.'\');return false " /></div>',
    );
    
    
    $form['batch']["ndla_ontopia_connect_grep_tasks_batch_delete_".$faget]['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete associations'),
      '#submit' => array('ndla_ontopia_connect_grep_tasks_confirm'),
      '#attributes' => array('class' => 'resource_delete_submit ochidden'),
    );
    
    $form['batch']['faget'] = array( 
      '#type' => 'hidden',    
    );
  }
  
  return $form;
}

  function ndla_ontopia_connect_grep_tasks_confirm($form,&$form_state) {
    // date_default_timezone_set('Europe/Oslo');
    // $naa = date('j').'/'.date('n').'/'.date('Y').' '.date('H').':'.date('i').':'.date('s');
    // $timestamp = mktime($naa);
    $timestamp = time();
  
    $json = $form['#post']['delete_assocs_ltm_'.$form['#post']['faget']];
    $fagstamp = $form['#post']['faget']."_".$timestamp;
    $data = array();
    $data[$fagstamp] = $json;
  
    variable_set('ndla_ontopia_connect_grep_tasks_delete_data',serialize($data));
    drupal_goto('ndla_ontopia_connect_grep_tasks_confirm_delete_form/'.$fagstamp);
  }
  
  
  function ndla_ontopia_connect_grep_tasks_confirm_delete_form($faget) {
    $var = variable_get('ndla_ontopia_connect_grep_tasks_delete_data',serialize($data)); 
    $json = '';
    if($var != '') {
      $var_arr = unserialize($var);
      $json = $var_arr[arg(1)];
    }
  
   
    $jarr = json_decode($json);
    $formen = array();
     $formen['assocs'] = array(
      '#prefix' => '<ul>',
      '#suffix' => '</ul>',
      '#tree' => TRUE,
    );
    
    foreach($jarr->values as $obj) {
      $formen['assocs'][$obj->reifier] = array('#type' => 'hidden', '#value' => $obj->reifier, '#prefix' => '<li><em>'.$obj->aim_title.'</em> for the resource <strong>', '#suffix' => $obj->title . '</strong></li>');
    }
   
    $formen['assocs']['data'] = array(
        '#type' => 'hidden',
        '#value' => $json, 
      );
      //return $formen;
   return confirm_form($formen,t('Are you sure you want to delete these associations?'),'/admin/build/grep',t('This cannot be undone'),t('Delete associations'), t('Cancel'));
  }
  
  
  function ndla_ontopia_connect_grep_tasks_confirm_delete_form_submit($form, &$form_state) {
    $data = $form_state['values']['assocs']['data'];
    $dataarr = json_decode($data);
    $msg = '<ul>';
    foreach ($dataarr->values as $obj) {
      $ltm = "delete ".$obj->reifier;
      $msg .= '<li>'.$obj->aim_title.' for '.$obj->title.'</li>';
      //print "<br>KTM: ".$ltm;
      $tolog = 'delete $assoc from reifies('.$obj->reifier.',$assoc)';
      $res = _update_topic($tolog);
    }
    $msg .= '</li>';
    
    $message = json_decode($res);
    drupal_set_message(t($message->message));
    $form_state['redirect'] = '/admin/build/grep';
  }
  
  function ndla_ontopia_connect_grep_tasks_delete_resources($form,&$form_state) {
    $faget = $form['#post']['faget'];
    $json = $form['#post']['delete_assocs_ltm_'.$faget];
    return drupal_get_form('ndla_ontopia_connect_grep_tasks_confirm_delete_form', $json);
  }
  

function ndla_ontopia_connect_grep_tasks_port_relations() {
  $form = array();
  $faglist = _getFagList(false);
  
  $fagopts = array();
  foreach ($faglist as $nid => $fag){
    $i=0;
    $faget = str_replace(' ','_',trim($fag));
    $faget = str_replace('æ','ae',trim($faget));
    $faget = str_replace('ø','oe',trim($faget));
    $faget = str_replace('å','aa',trim($faget));
    if($nid != ""){
      $fagopts[$nid] = $faget;
    } 
  }
  
  $form['grep_port_subject']['subject'] = array(
     '#type' => 'select',
      '#title' => t('Choose the subject you want to port competence aims for'),
      '#default_value' => t('Choose your subject'),
      '#options' => $fagopts,
      '#description' => t('Choose the subject you want to port competence aims for.'),
      );
 
  $form['grep_port_subject']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Continue to the next step'),
      '#submit' => array('ndla_ontopia_connect_grep_tasks_port_redirect'),
  );
  
  return $form;
}


function ndla_ontopia_connect_grep_tasks_port_redirect($form, $form_state) {
  $id = $form['#post']['subject'];
  drupal_goto('ontopia.portbyid/'.$id);
}

function ndla_ontopia_connect_grep_tasks_port_relations_by_id($form_state,$nid) {
  if(ndla_context()){
    drupal_add_js(drupal_get_path('module', 'ndla_ontopia_connect') . '/js/port_treeview.js');
    drupal_add_css(drupal_get_path('module', 'ndla_ontopia_connect') . '/css/treeview.css');
  }
  $div_id = 'fag_'.$nid;
  
  $endpoint = RELATE_SERVICE."/services/xmlrpc";
  $method_name = 'get.previous_udir_mapping';
  
  $required_args = array($nid,false);
  $xmlrpc_args = array_merge(
      array($endpoint, $method_name),
      $required_args
  );
  
  $prev_mapres = call_user_func_array('xmlrpc', $xmlrpc_args);
  $prev_mapping = json_decode($prev_mapres);
  $udircode_prev = $prev_mapping->code;
  
  
  $method_name2 = 'get.udir_mapping';
  $required_args2 = array($nid,false);
  $xmlrpc_args2 = array_merge(
      array($endpoint, $method_name2),
      $required_args2
  );
  
  $mapres = call_user_func_array('xmlrpc', $xmlrpc_args2);
  $mapping = json_decode($mapres);
  $udircode_curr = $mapping->code;
  
  drupal_set_title(t('Port resources from the old curriculum (@title) to the new curriculum (@title2)', array('@title' => $udircode_prev, '@title2' => $udircode_curr)));
  
  $form = array();
  $form['port']["ndla_ontopia_connect_grep_port_relations_".$div_id]['fetch_nodes_button'.$div_id] = array(
      '#type' => 'markup',
      '#value' => '<div id="fetch_nodes_button_button_'.$div_id.'"><input type="button" value="'.t('Fetch nodes').'" onclick="getCurriculaResourcesBySubject(\''.$nid.'\',\''.$div_id.'\',\'plan\',\'port\');return false " /></div>',
  );
  
  
  
  $form['port']["ndla_ontopia_connect_grep_port_relations_".$div_id]['alter_assocs_div'.$div_id] = array(
      '#type' => 'markup',
      '#value' => '<div id="alter_assocs_div_'.$div_id.'" class="curriculum-wrapper ochidden"></div>',
  );
  
  $form['port']["ndla_ontopia_connect_grep_port_relations_".$div_id]['nid'] = array(
      '#type' => 'hidden',
      '#value' => $div_id,
      '#attributes' => array('class' => 'fag_nid_'.$div_id),
  );
  
  $form['port']["ndla_ontopia_connect_grep_port_relations_".$div_id]['change_assocs_ltm_'.$div_id] = array(
      '#type' => 'hidden',
  );
  
  
  $form['port']["ndla_ontopia_connect_grep_port_relations_".$div_id]['change_assocs_button'.$div_id] = array(
      '#type' => 'markup',
      '#value' => '<div id="process_resource_button_'.$div_id.'" class="resource-process-button ochidden"><input type="button" value="'.t('Process changes').'" onclick="processPortAssocs(\''.$nid.'\',\''.$div_id.'\');return false " /></div>',
  );
  
  
  $form['port']["ndla_ontopia_connect_grep_port_relations_".$div_id]['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
      '#submit' => array('ndla_ontopia_connect_port_resource_save'),
      '#attributes' => array('class' => 'resource_change_submit ochidden'),
  );
 
  $form['port']['faget'] = array(
      '#type' => 'hidden',
      '#value' => '',
  );
  
  
  return $form;
}


function ndla_ontopia_connect_port_resource_save($form, &$form_state) {
  // date_default_timezone_set('Europe/Oslo');
  // $naa = date('j').'/'.date('n').'/'.date('Y').' '.date('H').':'.date('i').':'.date('s');
  // $dato = mktime($naa);
  $dato = time();
  
  $faget = $form['#post']['faget'];
  $jsonstr = $form['#post']['change_assocs_ltm_'.$faget];
  
  $json = json_decode($jsonstr);
  $adds = array();
  print_r($json);
  $teller = 0;
  foreach ($json->values as $elm) {
    $delete_tolog = 'delete $assoc from reifies('.$elm->ressurs_reifier_id.',$assoc)';
    $res = _update_topic($delete_tolog);
    //error_log( "DELETE FROM NEWRES: ".print_r($res,TRUE));
    $reifdel = 'delete '.$elm->ressurs_reifier_id;
    $reifdelres = _update_topic($reifdel);
    //error_log( "DELETE REIF FROM NEWRES: ".print_r($reifdelres,TRUE));
    
    $res2 = _port_update_topic($delete_tolog);
    //error_log( "DELETE FROM OLDRES: ".print_r($res2,TRUE));
    
    
    $reifdelres2 = _port_update_topic($reifdel);
    //error_log( "DELETE REIF FROM OLDRES: ".print_r($reifdelres2,TRUE));
    
    //print "<br>DELETE TOLOG: ".$delete_tolog;
    foreach ($elm->new_assocs as $addelm) {
      $assoc = $addelm->assoctype.'('.$elm->ressurs_topicid.' : laeringsressurs, '.$addelm->new_uuid.' : kompetansemaal) ~ a_'.$dato.'_'.$teller;
      $reifiertopic = '[a_'.$dato.'_'.$teller.' = "reifier for a_'.$dato.'_'.$teller.'" /topic-name] ';
      $reifiertopic .= '{a_'.$dato.'_'.$teller.', fag, [['.substr($elm->ressurs_fag,strpos($elm->ressurs_fag,"_")+1).']] } ';
      $reifiertopic .= '{a_'.$dato.'_'.$teller.', timestamp, [['.$dato.']] } ';
      $reifiertopic .= '{a_'.$dato.'_'.$teller.', bruker-navn, [['.$elm->ressurs_username.']] } ';
      $reifiertopic .= '{a_'.$dato.'_'.($teller++).', bruker-url, [[http://ndla.no/user/'.$elm->ressurs_userurl.']] } ';
      $addltm = $maaltopic.' '.$assoc. ' '.$reifiertopic;
      $adds[] = $addltm;
    }
  }
  error_log("ADDS: ".print_r($adds,TRUE));
  
  foreach($adds as $elm) {
    $res3 = _add_fragment($elm);
    error_log("ADDRES: ".$res3);
  }
  drupal_set_message(t('The selected resources where ported to the chosen competence aims in the new curriculm'));
}

function ndla_ontopia_connect_grep_tasks_batch_add($offset=0) {
  global $user;
  drupal_set_title(t('Batch relate resources to one competence aim'));
  if(ndla_context()){
    drupal_add_js(drupal_get_path('module', 'ndla_ontopia_connect') . "/js/grep_batch.js");
    drupal_add_css(drupal_get_path('module', 'ndla_ontopia_connect') . "/css/grep_batch.css");
    drupal_add_js(drupal_get_path('module', 'ndla_ontopia_connect') . '/js/treeview.js');
    drupal_add_css(drupal_get_path('module', 'ndla_ontopia_connect') . '/css/treeview.css');
  }
  if (!is_numeric($offset)) {
    $offset = 0;
  }
  
  if (arg(2) == 'grep') {
    $header_markup = TRUE;
  }
  else {
    $header_markup = FALSE;
    $return_json = TRUE;
  }
  
  $nodes = array();
  $tmp = array();
  
  
  $users = getOgUsersByUser($user->uid);
  
  $result = db_query("SELECT n.nid, n.vid, n.title, n.type, n.language, n.changed FROM {node} n where n.type != 'person' and n.type != 'fag' and n.type != 'fil' and n.type != 'biblio' and n.type != 'grep' and n.uid in (".implode(',',$users).") ORDER BY n.changed  DESC limit 100 offset $offset");
  
  while ($obj = db_fetch_object($result)) {
    array_push($tmp,$obj);
  }

  foreach($tmp as $node) {
    $node->published = t('No');
    
    if (ndla_publish_workflow_is_published($node)) {
      $node->published = t('Yes');
    }
    $node->timestamp = $node->changed;
    $node->changed = date("d/m/Y - H:i",$node->changed);
    
    if($node->language == '') {
      $node->language = 'Neutr.';
    }
    array_push($nodes,$node);
  }//end foreach
  
  
  if($return_json) {
    $json = json_encode($nodes);  
    $html = theme('ndla_ontopia_connect_grep_tasks_batch', $nodes,($offset+100),0,$json,NULL,$header_markup,TRUE,$user);
    $tmp = array('html' => $html, 'json' => $json, 'count' => count($nodes));
    $out = json_encode($tmp);
    print $out;
  }
  else {
    $json = json_encode($nodes);  
    $out = theme('ndla_ontopia_connect_grep_tasks_batch', $nodes,($offset+100),0,$json,NULL,$header_markup,TRUE,$user);
    return $out;
  } 
}

function ndla_ontopia_connect_grep_tasks_batch_search($search,$type) {
  global $user;
  if (!is_numeric($offset)) {
    $offset = 0;
  }
 
  date_default_timezone_set('Europe/Oslo');
  
  $nodes = array();
  $tmp = array();
  $users = getOgUsersByUser($user->uid);
  
  //("SELECT n.nid, n.vid, n.title, n.type, n.language, n.changed FROM node n where n.type != 'person' and n.type != 'fag' and n.type != 'fil' and n.type != 'biblio' and n.type != 'grep' and n.title LIKE '%$search%' and n.uid in (".implode(',',$users).") ORDER BY n.changed  DESC");
  
  if($type == 'title') {
    $result = db_query("SELECT n.nid, n.vid, n.title, n.type, n.language, n.changed FROM {node} n where n.type != 'person' and n.type != 'fag' and n.type != 'fil' and n.type != 'biblio' and n.type != 'grep' and n.title LIKE '%$search%' and n.uid in (".implode(',',$users).") ORDER BY n.changed  DESC");
  }
  else {
    $result = db_query("SELECT n.nid, n.vid, n.title, n.type, n.language, n.changed FROM {node} n where n.type != 'person' and n.type != 'fag' and n.type != 'fil' and n.type != 'biblio' and n.type != 'grep' and n.nid = $search and n.uid in (".implode(',',$users).") ORDER BY n.changed  DESC");
  }
  
    
  
  
  while ($obj = db_fetch_object($result)) {
    array_push($tmp,$obj);
  }

  foreach($tmp as $node) {
    $node->published = t('No');
    if (ndla_publish_workflow_is_published($node)) {
      $node->published = t('Yes');
    }
    $node->timestamp = $node->changed;
    $node->changed = date("d/m/Y - H:i",$node->changed);
    
    if($node->language == '') {
      $node->language = 'Neutr.';
    }
    
    array_push($nodes,$node);
  }//end foreach
  
  
  $json = json_encode($nodes);  
  $html = theme('ndla_ontopia_connect_grep_tasks_batch', $nodes,0,($offset+100),NULL,$json,FALSE,TRUE,$user);
  $tmp = array('html' => $html, 'json' => $json, 'count' => count($nodes),'search' => $search);
  $out = json_encode($tmp);
  print $out;
}