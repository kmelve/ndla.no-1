<?php
/**
 * @file
 * @ingroup ndla_ontopia_connect
 * @brief
 *  Template file for rendering nodes related to Relate topics.
 */
?>
<?php if ($header):?>
<fieldset class="collapsible collapsed">
  <legend><?php echo t('Search')?></legend>
  <label><?php echo t('Title')?>: </label>
  <input type="text" name="search_title" id ="search_title" value="" />&nbsp;
  <input type="button" name="search_submit" id="search_submit" onclick="searchNodes('','next')" value="<?php echo t('Search')?>" />
  <div id="flush_search"><?php echo t('Flush search')?></div>
</fieldset>
<?php endif; ?>

<?php if ($browse && $header): ?>
  <div id="navigators"><div id="janitor_get_next" onclick="getPage('next')"><?php echo t('Next')?> &gt;&gt;</div></div>
<?php endif; ?>

<?php if ($header): ?>
<table width="635">
  <thead>
    <tr>
    <th class="th_sort_header" id="th_sort_header_node"><?php print t('Node'); ?></th>
    <th  class="th_sort_header" id="th_sort_header_language"><?php print t('Language'); ?></th>
    <th class="th_sort_header" id="th_sort_header_type"><?php print t('Type'); ?></th>
    <th class="th_sort_header" id="th_sort_header_published"><?php print t('Published'); ?></th>
    <th colspan="3" class="th_sort_header"  id="th_sort_header_resource"><?php print t('Relate resource'); ?></th>
    </tr>
  </thead>
  <tbody id="janitor_table_body">
  <?php endif;?>
    <?php
    $counter = 0;
      foreach($nodes as $node) {
        
        if ($counter++&1) {
          $stripes = 'odd';
        }
        else {
          $stripes = 'even';
        }
        
        print '<tr class="'.$stripes.'">';
        print '<td><a href="/node/'.$node->nid.'">'.$node->title.'</a></td>';
        print '<td>'.$node->language.'</td>';
        print '<td>'.$node->type.'</td>';
        print '<td>'.$node->published.'</td>';
        print '<td id="topic-name_'.$node->nid.'">'.$node->topicname.'</td>';
        print '<td><span id="'.$node->nid.'§'.$node->topicid.'§'.$node->server.'" class="edit-link">'.t('Edit').'</span></td>';
        print '<td><span id="'.$node->nid.'" class="delete-link" onclick="updateTopic(\''.$node->nid.'\',\'delete\');">'.t('Delete').'</span></td>';
        print '</tr>';
      }
    ?>
    <tr><td><div id="edit-window"><div id="edit-body"></div></div></td></tr>
    <tr><td>
    <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
    <input type="hidden" name="search_offset" id="search_offset" value="<?php echo $search_offset; ?>" />
    <input type="hidden" name="jdata" id="jdata" value="<?php echo htmlspecialchars($jdata);?>" /></td></tr>
  <?php if ($header):?>
  </tbody>
</table>
<?php endif; ?>