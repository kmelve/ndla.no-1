<?php
/**
 * @file
 * @ingroup ndla_ontopia_connect
 * @brief
 *  Template file for rendering grep competence aim lighbox.
 */
?>
<div id="show_aim_wrapper">
  <div id="show_aim_grep">
    <div id="show_aim_grep_legend"><?php echo t('Learning Goal'); ?></div>
    <div id="show_aim_grep_breadcrumb"><?php echo $grepdata->laereplan_value; ?> > <?php echo $grepdata->hovedomraade_value; ?></div>
    <div id="show_aim_title"><?php echo $grepdata->aim_name; ?></div>
    
    <?php
    $ressursdata = $grepdata->aims_list[0]->content;
    if (is_array($ressursdata)) {
      $ndladata = null;
      $nygivdata = null;
      $delingdata = null;
      ?>
      <div class="ui-tabs">
      <div id="show_aim_resources">
      <ul id="show_aim_tabs">
        <?php
        
        //ndla
        if (count($ressursdata[0]->ndla) > 0) {
          $ndladata = $ressursdata[0]->ndla[0];
          ?>
            <li><a href="#ndla_tab"><span>NDLA (<?php echo $ndladata->counter; ?>)</span></a></li>
          <?php
        }

        //deling
        if (count($ressursdata[0]->deling) > 0 || count($ressursdata[1]->deling) > 0) {
          count($ressursdata[0]->deling) > 0 ? $delingdata = $ressursdata[0]->deling[0] : $delingdata = $ressursdata[1]->deling[0];
          ?>
            <li><a href="#deling_tab"><span>deling.ndla.no (<?php echo $delingdata->counter; ?>)</span></a></li>
          <?php
        }
        
        if (count($ressursdata[0]->nygiv) > 0 || count($ressursdata[1]->nygiv) > 0 || count($ressursdata[2]->nygiv) > 0) {
        count($ressursdata[0]->nygiv) > 0 ? $nygivdata = $ressursdata[0]->nygiv[0] : $nygivdata = $ressursdata[1]->nygiv[0];
        if(count($nygivdata) < 1){
          $nygivdata = $ressursdata[2]->nygiv[0];
        }
          ?>
          <li><a href="#nygiv_tab"><span>nygiv.ndla.no (<?php echo $nygivdata->counter; ?>)</span></a></li>
        <?php
        }
        ?>
        </ul>
        <div id="show_aim_resources_toolbar">
          <div id="show_aim_resources_toolbar_search">
            <input type="text" id="show_aim_resources_toolbar_search_term" name="show_aim_resources_toolbar_search_term" value="<?php echo t("start your search");?>" />
          </div>
          <div id="show_aim_resources_toolbar_sort">
            <div id="show_aim_resources_toolbar_sort_label"><?php echo t("SORT");?></div>
          </div>
          <div id="show_aim_resources_toolbar_sort_popup">
            <ul id="show_aim_resources_toolbar_sort_popup_list">
              <li id="show_aim_resources_toolbar_sort_popup_list_item_coverage" class="show_aim_resources_toolbar_sort_popup_list_item"><a href="javascript:" onclick="show_aim_resources_sort_by('coverage')"><span><?php echo t("By coverage");?></span></a></li>
              <li id="show_aim_resources_toolbar_sort_popup_list_item_contenttype" class="show_aim_resources_toolbar_sort_popup_list_item"><a href="javascript:" onclick="show_aim_resources_sort_by('contenttype')"><span><?php echo t("By content type");?></span></a></li>
              <li id="show_aim_resources_toolbar_sort_popup_list_item_name" class="show_aim_resources_toolbar_sort_popup_list_item"><a href="javascript:" onclick="show_aim_resources_sort_by('name')"><span><?php echo t("By name");?></span></a></li>
            </ul>
          </div>
        </div>
        <?php 
         //ndla
        if (count($ressursdata[0]->ndla) > 0) {
          ?>
            <div id="ndla_tab">
              <ul id="ndla_tab_list">
              <?php
              $rescounter = 0;
              foreach($ndladata as $id => $val) {
                if($id != "counter") {
                  $reltittel = $val->tittel;
                  $psis = $val->psis;
                  $psis = _typeSort($psis);
                  
                  foreach($psis as $type => $res) {
                    foreach ($res as $reselm){
                      ++$rescounter;
                      $class = "res_";
                      ($rescounter%2) ? $class .= 'odd' : $class .= 'even';
                      if($type == "notype"){
                        $reselm->nodetype_name = "NN";
                        $reselm->nodetype = 'nn';
                      }
                      if($rescounter <= 5){
                        ?>
                        <li class="<?php echo $id; ?> <?php echo $class ; ?> <?php echo $reselm->nodetype; ?>" title="<?php echo $reltittel; ?>"><span class="show_aim_linkspan" onclick="show_aim_goTo('<?php echo $reselm->url;?>')"><?php echo trim($reselm->tittel); ?></span> <span class="show_aim_node_type">(<?php echo $reselm->nodetype_name?>, <?php echo $reselm->language; ?>)</span></li>
                      <?php
                      }
                      else{
                        ?>
                        <li class="<?php echo $id; ?> <?php echo $class ; ?> <?php echo $reselm->nodetype; ?> ndla_show_aim_hidden" title="<?php echo $reltittel; ?>"><span class="show_aim_linkspan" onclick="show_aim_goTo('<?php echo $reselm->url;?>')"><?php echo trim($reselm->tittel); ?></span> <span class="show_aim_node_type">(<?php echo $reselm->nodetype_name?>, <?php echo $reselm->language; ?>)</span></li>
                      <?php
                      }  
                    }
                    
                  }
                }
              }
              ?>
              </ul>
              <?php
                if($ndladata->counter > 5) {
                  ?>
                    <div id="ndla_tab_more"><span class="ndla_tab_more_counter"><?php echo ($ndladata->counter - 5)."</span> ".t("ADDITIONAL RESOURCES")?> <span class="tab_view_more" onclick="show_aim_view_more('ndla')"><?php echo t("VIEW MORE")?></span></div>
                  <?php
                }                 
              ?>
            </div>
          <?php
        }
        
        //deling
        if (count($ressursdata[0]->deling) > 0 || count($ressursdata[1]->deling) > 0) {
           ?>
            <div id="deling_tab">
             <ul id="deling_tab_list">
              <?php
              $drescounter = 0;
              
              foreach($delingdata as $did => $dval) {
                if($did != "counter") {
                  $dreltittel = $dval->tittel;
                  $dpsis = $dval->psis;
                  foreach($dpsis as $dres) {
                    $dclass = "res_";
                    ++$drescounter;
                    ($drescounter%2) ? $dclass .= 'odd' : $dclass .= 'even';
                    if($drescounter <= 5){
                    ?>
                      <li class="<?php echo $did; ?> <?php echo $dclass ; ?>" title="<?php echo $dreltittel; ?>"><span class="show_aim_linkspan" onclick="show_aim_goTo('<?php echo $dres->url;?>')"><?php echo $dres->tittel; ?></span></li>
                    <?php
                    }
                    else {
                      ?>
                        <li class="<?php echo $did; ?> <?php echo $dclass ; ?> deling_show_aim_hidden" title="<?php echo $dreltittel; ?>"><span class="show_aim_linkspan" onclick="show_aim_goTo('<?php echo $dres->url;?>')"><?php echo $dres->tittel; ?></span></li>
                      <?php
                    }
                  }
                }
              }
              ?>
              </ul>
              <?php
                if($delingdata->counter > 5){
                  ?>
                  <div id="deling_tab_more"><span class="ndla_tab_more_counter"><?php echo ($delingdata->counter - 5)."</span> ".t("ADDITIONAL RESOURCES")?> <span class="tab_view_more"  onclick="show_aim_view_more('deling')"><?php echo t("VIEW MORE")?></span></div>
                  <?php
                } 
              ?>
            </div>
          <?php
        }
        
        
        if (count($ressursdata[0]->nygiv) > 0 || count($ressursdata[1]->nygiv) > 0 || count($ressursdata[2]->nygiv) > 0) {
         ?>
            <div id="nygiv_tab">
            <ul id="nygiv_tab_list">
              <?php
              $nrescounter = 0;
              
              foreach($nygivdata as $nid => $nval) {
                if($nid != "counter") {
                  $nreltittel = $nval->tittel;
                  $npsis = $nval->psis;
                  foreach($npsis as $nres) {
                    ++$nrescounter;
                    $nclass = "res_";
                    ($nrescounter%2) ? $nclass .= 'odd' : $nclass .= 'even';
                    if($nrescounter <= 5){
                    ?>
                      <li class="<?php echo $nid; ?> <?php echo $nclass ; ?>" title="<?php echo $nreltittel; ?>"><span class="show_aim_linkspan" onclick="show_aim_goTo('<?php echo $nres->url;?>')"><?php echo $nres->tittel; ?></span></li>
                    <?php
                    }
                    else {
                      ?>
                        <li class="<?php echo $nid; ?> <?php echo $nclass ; ?> nygiv_show_aim_hidden" title="<?php echo $nreltittel; ?>"><span class="show_aim_linkspan" onclick="show_aim_goTo('<?php echo $nres->url;?>')"><?php echo $nres->tittel; ?></span></li>
                      <?php
                    }
                  }
                }
              }
              ?>
              </ul>
              <?php
                if($nygivdata->counter > 5){
                  ?>
                  <div id="nygiv_tab_more"><span class="ndla_tab_more_counter"><?php echo ($nygivdata->counter - 5)."</span> ".t("ADDITIONAL RESOURCES")?> <span class="tab_view_more"  onclick="show_aim_view_more('nygiv')"><?php echo t("VIEW MORE")?></span></div>
                  <?php
                } 
              ?>
            </div>
          <?php
        }
        ?>
      </div>
      </div>
      <?php 
    }
    else{
    ?>
      <div id="show_aim_message"><?php echo t('There were no other resources associated with the competence aim');?></div>
    <?php
    } 
    ?>
  </div>
</div>