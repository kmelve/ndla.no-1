<?php
/**
 * @file
 * @brief
 *  Template file for rendering children of a given UUID.
 *
 * @ingroup ndla_ontopia_connect
 */
?>
<div class='grep-leafs-container'>
  <?php
  if(!count($leafs)) {
    print t('Nothing found');
  }
  else {
    $parent_uuid = $leafs[0]->fag_psi;
    foreach($leafs[0]->fag_data as $leaf) {
      print "<div id='parent-leaf-$parent_uuid' style='margin-left: 10px;' class='leaf'>";
      print "<strong>" . $leaf->hovedomraade_tittel . "</strong><br />";
      foreach($leaf->maaldata as $sub_leaf) {
        $checked = isset($searched_uuid[$sub_leaf->uuid]) ? ' checked ' : '';
        print "<input style='margin-left: 20px' $checked type='checkbox' name='searched_uuid[]' value='" . $sub_leaf->uuid . "' /> " . $sub_leaf->aim_title . "<br />";
      }
      print "</div>";
    }
  }
  ?>
</div>