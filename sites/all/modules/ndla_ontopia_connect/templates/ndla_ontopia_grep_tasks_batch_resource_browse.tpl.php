<?php
/**
 * @file
 * @ingroup ndla_ontopia_connect
 * @brief
 *  Template file for rendering nodes related to Relate topics.
 */
?>
<?php if ($header):?>
<fieldset class="collapsible collapsed" id="batch_fieldset">
  <legend><?php echo t('Search')?></legend>
  <label><?php echo t('Search term')?>: </label>
  <input type="text" name="batch_add_search_title" id ="search_title" value="" />&nbsp;
  <input type="radio" name="batch_add_search_type" id="batch_add_search_type_title" value="title" /> <label><?php echo t('Node title')?></label> &nbsp;
  <input type="radio" name="batch_add_search_type" id="batch_add_search_type_nodeid" value="nodeid" /> <label><?php echo t('Node number')?></label> &nbsp;
  <input type="button" name="batch_add_search_submit" id="batch_add_search_submit" onclick="batch_add_searchNodes()" value="<?php echo t('Search')?>" />
  <div id="flush_search"><?php echo t('Flush search')?></div>
</fieldset>
<?php endif; ?>

<?php if ($browse && $header): ?>
    <div id="navigators"><div id="batchadd_get_next" onclick="batchGetPage('next')"><?php echo t('Next')?> &gt;&gt;</div></div>
<?php endif; ?>

<?php if ($header): ?>
<table width="635" id="node_table">
  <thead>
    <tr>
    <th class="th_sort_header" id="th_sort_header_node" colspan="2" onclick="batch_add_sortData('node')"><?php print t('Node'); ?></th>
    <th class="th_sort_header" id="th_sort_header_language" onclick="batch_add_sortData('language')"><?php print t('Language'); ?></th>
    <th class="th_sort_header" id="th_sort_header_type" onclick="batch_add_sortData('type')"><?php print t('Type'); ?></th>
    <th class="th_sort_header" id="th_sort_header_published" onclick="batch_add_sortData('published')"><?php print t('Published'); ?></th>
    <th class="th_sort_header"  id="th_sort_header_changed" onclick="batch_add_sortData('changed')"><?php print t('Changed'); ?></th>
    </tr>
  </thead>
  <tbody id="batchadd_table_body">
  <?php endif;?>
    <?php
    $counter = 0;
      foreach($nodes as $node) {
        
        if ($counter++&1) {
          $stripes = 'odd';
        }
        else {
          $stripes = 'even';
        }
        
        print '<tr class="'.$stripes.'" id="row_'.$node->nid.'">';
        print '<td><div class="add_node_button" id="node_button_'.$node->nid.'" onclick="addNode('.$node->nid.',\''.$node->title.'\')">'.t('Add').'</div></td>';
        print '<td><a href="/node/'.$node->nid.'">'.$node->title.'</a></td>';
        print '<td>'.$node->language.'</td>';
        print '<td>'.$node->type.'</td>';
        print '<td>'.$node->published.'</td>';
        print '<td>'.$node->changed.'</td>';
        print '</tr>';
      }
    ?>
    <tr><td>
    <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
    <input type="hidden" name="jdata" id="jdata" value="<?php echo htmlspecialchars($jdata);?>" /></td></tr>
    <input type="hidden" name="batch_add_search_jdata" id="batch_add_search_jdata" value="<?php echo htmlspecialchars($batch_add_search_jdata);?>" /></td></tr>
    <input type="hidden" id="grep-user-name" name="grep-user-name" value="<?php echo $user->name; ?>" />
    <input type="hidden" id="grep-user-uid" name="grep-user-uid" value="<?php echo $user->uid; ?>" />
    <input type="hidden" id="grep-user-subject" name="grep-user-subject" value="<?php echo $user->subject; ?>" />
  <?php if ($header):?>
  </tbody>
</table>
<?php endif; ?>