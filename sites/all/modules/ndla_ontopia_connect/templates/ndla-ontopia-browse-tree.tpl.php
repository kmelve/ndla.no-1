<?php
/**
 * @file
 * @ingroup ndla_ontopia_connect
 * @brief
 *  Template file for rendering main UUID's on the search page.
 */
?>
<form id='ndla_ontopia_browse_search_form' method='get'>
  <div class='ontopia_browse'>
    <?php foreach($main as $index => $data):?>
      <div class="grep-tree">
        <a class='grep-leaf<?php print isset($open_parents[$data->uuid]) ? ' grep-auto-click'  : '';?>' id='grep-leaf-<?php print $data->uuid;?>' href='javascript:void(0)'>
          <?php print $data->name; ?>
        </a>
      </div><br />
    <?php endforeach; ?>
  </div>
  <input type='submit' value='<?php print t('Search'); ?>' />
</form>