<?php
/**
 * @ingroup ndla_ontopia_connect
 * @file
 * @brief
 *  Template for rendering the search results.
 */
 
?>

<div class='ontopia-searchresults'>
  <?php
  if(count($results)) {
    foreach($results as $result) {
      print "<h2>" . $result->aim_name . "</h2>";
      $temp = array_values((array)($result->resources));
      $relation_title = $temp[0][0]->assoc_title;
      print "<h4>" . $relation_title . "</h4>";
      print "<ul>";
      foreach($result->resources as $resource) {
        foreach($resource as $node) {
          print "<li>";
          print "<a href='" . $node->resource_url . "'>" . $node->resource_title . "</a>";
          print "</li>";
        }
      }
      print "</ul>";
    }
  }
  else {
    print t('No resources found') . ".";
  }
  ?>
</div>