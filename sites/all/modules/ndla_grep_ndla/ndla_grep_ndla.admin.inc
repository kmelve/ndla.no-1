<?php

function ndla_grep_ndla_settings_form() {
  $form = array();

  $form['service'] = array(
    '#type' => 'fieldset',
    '#title' => t('Service'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'ndla_grep_account' => array(
      '#title' => t('Account'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_account', ''),
      '#description' => t('The name of the user account, i.e. ":fyr" or ":ndla".', array(':fyr' => 'fyr', ':ndla' => 'ndla')),
    ),
    'ndla_grep_version' => array(
      '#title' => t('Version'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_version', ''),
      '#description' => t('The UDIR import version. If empty the latest version will be used.'),
    ),
    'ndla_grep_host' => array(
      '#title' => t('Host'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_host', ''),
      '#description' => t('The GREP server host name, i.e. ":url".', array(':url' => 'mycurriculum.test.ndla.no')),
    ),
    'ndla_grep_ssl' => array(
      '#title' => t('Use SSL'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('ndla_grep_ssl', FALSE),
    ),
    'ndla_grep_username' => array(
      '#title' => t('Username'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_username', ''),
      '#description' => t('The server authentication username.'),
    ),
    'ndla_grep_password' => array(
      '#title' => t('Password'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_password', ''),
      '#description' => t('The server authentication password.'),
    ),
    'ndla_grep_psi' => array(
      '#title' => t('Hostname for PSI'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_psi', ''),
      '#description' => t('If not set the current host will be used. End with trailing slash'),
    ),
    'ndla_grep_timeout' => array(
      '#title' => t('Timeout'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_timeout', 30),
    ),
    'ndla_grep_wakeup' => array(
      '#title' => t('Wakeup time'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_grep_wakeup', 5),
      '#description' => t('Graceful wakeup time. In minutes.'),
    ),
  );

  $aim_sets = variable_get('ndla_grep_competence_aim_sets', array());
  $form['ndla_grep_competence_aim_sets'] = array(
    '#type' => 'fieldset',
    '#title' => t('Competence aim sets'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Enter one competence aim set ID per row.'),
    '#tree' => TRUE,
  );
  $result = db_query("SELECT nid, title FROM {node} WHERE type = 'fag' ORDER BY title");
  while ($row = db_fetch_object($result)) {
    $form['ndla_grep_competence_aim_sets'][$row->nid] = array(
      '#type' => 'textarea',
      '#title' => t('Competence aim set IDs for !fag', array('!fag' => $row->title)),
      '#rows' => 3,
      '#default_value' => !empty($aim_sets[$row->nid]) ? $aim_sets[$row->nid] : '',
    );
  }

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enabled content types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'ndla_grep_content_types' => array(
      '#type' => 'checkboxes',
      '#options' => node_get_types('names'),
      '#default_value' => variable_get('ndla_grep_content_types', array()),
    ),
  );

  return system_settings_form($form);
}

function ndla_grep_ndla_course_map_form() {
  $id = arg(1);
  $map = NdlaGrepNdlaClient::get_course_mapping($id);
  $inactive = variable_get('ndla_grep_course_mapping_inactive_' . $id, array());
  $form = array();
  $form['gid'] = array(
    '#type' => 'hidden',
    '#default_value' => $id,
  );
  $form['id'] = array(
    '#type' => 'textfield',
    '#title' => t('Curriculum set ID'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  if(!empty($map)) {
    $default = variable_get('ndla_grep_course_mapping_default_' . $id, '');
    $html = '<table class="ndla-grep-course-map">';
    foreach($map as $id => $name) {
      $class_default = 'fa-star-o';
      if($default == $id) {
        $class_default = 'fa-star';
      }
      $class_inactive = 'fa-check-square-o';
      if(in_array($id, $inactive)) {
        $class_inactive = 'fa-square-o';
      }
      $html .= '<tr id="' . $id . '"><td>' . $name . ' (' . $id . ')</td><td><i class="fa ' . $class_inactive . '"></i></td><td><i class="fa ' . $class_default . '"></i></td><td><i class="fa fa-times"></i></td></tr>';
    }
    $html .= '</table>';
    $form['table'] = array(
      '#type' => 'markup',
      '#value' => $html,
    );
  }
  drupal_add_js(drupal_get_path('module', 'ndla_grep_ndla') . '/js/ndla_grep_ndla.admin.js');
  drupal_add_css(drupal_get_path('module', 'ndla_grep_ndla') . "/css/ndla_grep_ndla.admin.css");
  return $form;
}

function ndla_grep_ndla_course_map_form_submit($form, &$form_state) {
  NdlaGrepNdlaClient::add_course_mapping($form_state['values']['gid'], $form_state['values']['id']);
}

function ndla_grep_node_course_map_remove($id) {
  $gid = arg(1);
  NdlaGrepNdlaClient::remove_course_mapping($gid, $id);
}

function ndla_grep_node_course_map_default($id) {
  $gid = arg(1);
  variable_set('ndla_grep_course_mapping_default_' . $gid, $id);
}

function ndla_grep_node_course_map_active($id) {
  $gid = arg(1);
  $data = variable_get('ndla_grep_course_mapping_inactive_' . $gid, array());
  if(in_array($id, $data)) {
    unset($data[$id]);
  } else {
    $data[$id] = $id;
  }  
  variable_set('ndla_grep_course_mapping_inactive_' . $gid, $data);
}