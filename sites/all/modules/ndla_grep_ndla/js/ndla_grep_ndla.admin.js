Drupal.behaviors.ndla_grep_ndla = function (context) {
  $('.ndla-grep-course-map .fa-times').click(function() {
    $.get(Drupal.settings.basePath + 'node/' + $('#edit-gid').val() + '/ndla_grep_course_map/remove/' + $(this).parents('tr').attr('id'));    
    $(this).parents('tr').remove();
  });
  $('.ndla-grep-course-map .fa-star, .ndla-grep-course-map .fa-star-o').click(function() {
    if($(this).hasClass('fa-star-o')) {
      $.get(Drupal.settings.basePath + 'node/' + $('#edit-gid').val() + '/ndla_grep_course_map/default/' + $(this).parents('tr').attr('id'));
      $('.ndla-grep-course-map .fa-star').removeClass('fa-star').addClass('fa-star-o');;
      $(this).toggleClass('fa-star-o').toggleClass('fa-star');
    }
  });
  $('.ndla-grep-course-map .fa-check-square-o, .ndla-grep-course-map .fa-square-o').click(function() {
    $.get(Drupal.settings.basePath + 'node/' + $('#edit-gid').val() + '/ndla_grep_course_map/active/' + $(this).parents('tr').attr('id'));
    $(this).toggleClass('fa-square-o').toggleClass('fa-check-square-o');
  });
};