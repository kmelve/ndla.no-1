Drupal.behaviors.ndla_grep_ndla = function(context) {
  $('body').bind('click', function(e) {
    if($.browser.mozilla) {
      var $target = $(e.target);
    } else {
      var $target = $(e.toElement);
    }

    if($target.hasClass('aim-list-item')) {
      var endpoint = $target.data('endpoint');
      var callback = $target.data('callback');
      var tree = $target.data('tree');
      var stored = $('#edit-aims').val();
      if($target.data('loading') == true) {
        return;
      }
      if($target.siblings('ul').length) {
        $target.siblings('ul').toggle();
        $target.find('i.fa').toggleClass('fa-angle-down').toggleClass('fa-angle-right');
      } else {
        $target.data('loading', true);
        $target.find('i.fa').removeClass('fa-angle-right').addClass('fa-spinner fa-spin');
          $.ajax({
            url: callback,
            data: { endpoint : endpoint, tree : tree, stored : stored },
            success: function(data) {
              $target.after(data);
              $target.find('i.fa').removeClass('fa-spinner fa-spin').addClass('fa-angle-down');
              $target.data('loading', false);
            }
          });
        }
      }
      if($target.hasClass('aim-select-radio') || $target.hasClass('aim-select-checkbox')) {
        var tree = $target.parents('ul').first().data('tree');
        var value = $target.parents('ul').first().find('input[type=radio]:checked').val();
        var stored = $('#edit-aims').val();
        var apprentice = $target.parents('ul').first().find('input[type=checkbox]').is(':checked');
        $('.ndla-course-selector input').attr('disabled', true);
        $.ajax({
          url: Drupal.settings.basePath + 'ndla_grep/ajax/store',
          data: { stored : stored, tree : tree, value : value, apprentice : apprentice },
          success: function(data) {
            data = $.parseJSON(data);
            $('#edit-aims').val(data.json);
            $('#ndla_grep_selected').html(data.html);
            $('.ndla-course-selector input').attr('disabled', false);
          }
        });
      }
      if($target.hasClass('aim-list-remove')) {
        var id_list = $target.data('tree');
        var stored = $('#edit-aims').val();
        var id = $(id_list).last()[0];
        var radios = $('.aim-select-radio[name=' + id +']');
        radios.each(function() {
          $ul = $(this).parents('ul').first();
          var tree = $ul.data('tree');
          tree.shift();
          for(i in id_list) {
            if(id_list[i] != tree[i]['#id']) {
              return;
            }
          }
          $ul.find('input').attr('checked', false);
          $ul.find('input[value=none]').attr('checked', true);
        });
        $.ajax({
          url: Drupal.settings.basePath + 'ndla_grep/ajax/remove',
          data: { stored : stored, id_list : id_list },
          success: function(data) {
            data = $.parseJSON(data);
            $('#edit-aims').val(data.json);
            $('#ndla_grep_selected').html(data.html);
          }
        });
      }
    });
}