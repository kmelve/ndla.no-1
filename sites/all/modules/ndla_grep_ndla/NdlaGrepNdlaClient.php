<?php

class NdlaGrepNdlaClient {

  private static function _post($path, $data) {
    $url = self::_build_url($path);
    $response = drupal_http_request($url, array('Accept' => 'application/json', 'Content-Type' => 'application/json'), 'POST', json_encode($data));
    if(!in_array($response->code, array(200, 201))) {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  private static function _delete($path, $data = array()) {
    $url = self::_build_url($path);
    $response = drupal_http_request($url, array('Accept' => 'application/json', 'Content-Type' => 'application/json'), 'DELETE', json_encode($data));
    if(!in_array($response->code, array(200, 201))) {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  public static function getAimNames($id) {
    $data = self::_get("competence-aims/$id");
    return self::_extract_name($data->competenceAim->names);
  }

  private static function _get($path, $cache = TRUE) {
    $wakeup = variable_get('ndla_grep_wakeup', 5) * 3600;
    $limit = rand(0, $wakeup) + variable_get('ndla_grep_sleep', 0);
    if($limit > time()) {
      drupal_set_message(t('MyCurriculum is not responding.'), 'error');
      return FALSE;
    }
    $url = self::_build_url($path);
    if($cache) {
      $data = cache_get($url, 'cache_ndla_grep');
      if(!empty($data->data)) {
        return $data->data;
      }
    }

    $response = drupal_http_request($url, array('Accept' => 'application/json'), 'GET', NULL, 3, variable_get('ndla_grep_timeout', 30));
    if(in_array($response->code, array('-1'))) {
      ndla_utils_cache_lifetime(60);
      variable_set('ndla_grep_sleep', time());
      drupal_set_message(t('MyCurriculum is not responding.'), 'error');
      return FALSE;
    }
    if($response->code != 200) {
      return FALSE;
    }
    if(variable_get('ndla_grep_sleep', 0) != 0) {
      variable_set('ndla_grep_sleep', 0);
    }
    $data = json_decode($response->data);
    if($cache) {
      cache_set($url, $data, 'cache_ndla_grep');
    }
    return $data;
  }

  private static function _build_url($path) {
    if(preg_match('/^http/', $path)) {
      return $path;
    }
    $host = variable_get('ndla_grep_host', '');
    $account = variable_get('ndla_grep_account', '');
    $version = variable_get('ndla_grep_version', '');
    $ssl = variable_get('ndla_grep_ssl', FALSE);
    $username = variable_get('ndla_grep_username', '');
    $password = variable_get('ndla_grep_password', '');
    $url = 'http://';
    if($ssl) {
      $url = 'https://';
    }
    if(!empty($username) || !empty($password)) {
      $url .= $username . ':' . $password . '@';
    }
    $url .= $host . '/v1/users/' . $account . '/' . $path;
    if(!empty($version)) {
      if(strpos($url, '?') !== FALSE) {
        $url .= '&';
      } else {
        $url .= '?';
      }
      $url .= $version;
    }
    return $url;
  }

  private static function _extract_name($names) {
    global $language;
    if(empty($names)) {
      return 'N/A';
    }
    $psi = NdlaGrepLanguages::iso639_1_psi($language->language);
    $default = '';
    foreach($names as $name) {
      if(empty($default)) {
        $default = $name->name;
      }
      if(in_array($psi, $name->scopes)) {
        return $name->name;
      }
      if($name->isLanguageNeutral) {
        $default = $name->name;
      }
    }
    return $default;
  }

  public static function save_resource($node) {
    $author = user_load($node->uid);
    $resource = array(
      'psi' => self::get_psi('node/' . $node->nid),
      'status' => 'TRUE',
      'names' => array(
        array(
          'name' => $node->title,
          'languageCode' => NdlaGrepLanguages::iso639_1_iso639_2($node->language),
          'isLanguageNeutral' => ($node->language == 'und'),
        ),
      ),
      'ingress' => (object)array(
        'text' => '',
        'languageCode' => NdlaGrepLanguages::iso639_1_iso639_2($node->language),
        'isLanguageNeutral' => ($node->language == 'und'),
      ),
      'type' => 'ndla-node',
      'licenses' => array(),
      'authors' => array(
        array(
          'name' => !empty($author->name) ? $author->name : '',
          'url' => self::get_psi('user/' . $author->uid),
        )
      ),
    );
    if(!empty($node->field_license[LANGUAGE_NONE][0]['value'])) {
      $resource['licenses'][] = array(
        'name' => $node->field_license[LANGUAGE_NONE][0]['value'],
        'scopes' => array(
          'http://psi.mycurriculum.org/#universal-type',
          'http://psi.oasis-open.org/iso/639/#eng',
        ),
        'isLanguageNeutral' => TRUE,
      );
    }
    return self::_post('resources', $resource);
  }

  public static function load_aims($node) {
    $data = self::_get('resources?psi=' . self::get_psi('node/' . $node->nid) . '&competence-aims=true');
    if($data == FALSE) {
      self::save_resource($node);
    }
    $aims = array();
    if(empty($data->resource->relations)) {
      return $aims;
    }
    foreach($data->resource->relations as $aim) {
      $set_response = self::_get('competence-aim-sets/' . $aim->competenceAimSetId);
      $curriculum_response = self::_get($set_response->competenceAimSet->links->parent);
      /* Curriculum */
      if(empty($aims[$curriculum_response->curriculum->id])) {
        $aims[$curriculum_response->curriculum->id] = array(
          '#name' => self::_extract_name($curriculum_response->curriculum->names),
          '#id' => $curriculum_response->curriculum->id,
        );
      }
      /* Competence aim set */
      if(empty($aims[$curriculum_response->curriculum->id][$set_response->competenceAimSet->id])) {
        $aims[$curriculum_response->curriculum->id][$set_response->competenceAimSet->id] = array(
          '#name' => self::_extract_name($set_response->competenceAimSet->names),
          '#id' => $set_response->competenceAimSet->id,
        );
      }
      /* Competence aim */
      if(empty($aims[$curriculum_response->curriculum->id][$set_response->competenceAimSet->id][$aim->competenceAim->id])) {
        $aims[$curriculum_response->curriculum->id][$set_response->competenceAimSet->id][$aim->competenceAim->id] = array(
          '#id' => $aim->competenceAim->id,
          '#name' => self::_extract_name($aim->competenceAim->names),
          '#value' => 'related',
          '#apprentice' => $aim->apprenticeRelevant,
        );
      }
    }
    return $aims;
  }

  public static function load_aims_with_names($node, $all_names = FALSE) {
    $data = self::_get('resources?psi=' . self::get_psi('node/' . $node->nid) . '&competence-aims=true');
    if($data == FALSE) {
      self::save_resource($node);
    }
    $aims = array();
    if(empty($data->resource->relations)) {
      return $aims;
    }
    foreach($data->resource->relations as $aim) {
      //Dont filter out names based on current language.
      if($all_names) {
        foreach($aim->competenceAim->names as $name) {
          $aims[$aim->competenceAim->id][] = $name->name;
        }
      }
      else {
        $aims[$aim->competenceAim->id] = self::_extract_name($aim->competenceAim->names);
      }
    }
    return $aims;
  }

  public static function save_aims($node, $aims) {
    global $user;
    $data = array(
      'author' => array(
        'name' => $user->name,
        'url' => self::get_psi('user/' . $user->uid),
      ),
      'subjectMatterId' => '',
      'relations' => array(),
    );
    foreach($aims as $id => $aim) {
      $data['relations'][] = array(
        'competenceAimId' => $aim['id'],
        'relationType' => $aim['value'],
        'resourcePsi' => self::get_psi('node/' . $node->nid),
        'apprenticeRelevance' => ($aim['apprentice'] == 'true'),
        'curriculumSetId' => '',
        'level' => '',
        'courseId' => '',
        'courseType' => '',
        'curriculumId' => $aim['curriculum_id'],
        'competenceAimSetId' => $aim['competence_aim_set_id'],
      );
    }
    if(!empty($data['relations'])) {
      $result = self::_post('relations', $data);
      self::_unset_cache($node->nid);
      return $result;
    }
    return TRUE;
  }

  public static function delete_aims($node) {
    $response = self::_get('resources?psi=' . self::get_psi('node/' . $node->nid) . '&competence-aims=true');
    if($response == FALSE) {
      self::save_resource($node);
    }
    $psi = self::get_psi('node/' . $node->nid);
    $data = array('relations' => array());
    if(!empty($response->resource->relations)) {
      foreach($response->resource->relations as $aim) {
        $data['relations'][] = array(
          'resourcePsi' => $psi,
          'humanId' => $aim->humanId,
          'competenceAimId' => $aim->competenceAim->id,
        );
      }
    }
    if(!empty($data)) {
      $result = self::_delete('relations', $data);
      self::_unset_cache($node->nid);
      return $result;
    }
    return TRUE;
  }
  
  private static function _unset_cache($nid) {
    $psi = self::get_psi('node/' . $nid);
    $url = self::_build_url('resources?psi=' . self::get_psi('node/' . $nid) . '&competence-aims=true');
    cache_clear_all($url, 'cache_ndla_grep');
  }

  public static function get_courses($ids) {
    $data = array();
    $account = variable_get('ndla_grep_account', '');
    $response = self::_get('courses?education-group=' . $account);
    //CURRICULUM
    //PROGRAM_AREA
    foreach($response->courses as $course) {
      if(!in_array($course->externalId, $ids)) {
        continue;
      }
      foreach($course->courseStructures as $courseStructure) {
        if($courseStructure->setType == 'PROGRAM_AREA') {
          $response2 = self::_get($courseStructure->link);
          foreach($response2->courseStructure->curriculumSets as $curriculumSet) {
            if($curriculumSet->setType == 'PROGRAM') {
              $response3 = self::_get($curriculumSet->links->self);
              $name = self::_extract_name($response3->curriculumSet->curriculums[0]->names);
              $data[$name . $response3->curriculumSet->curriculums[0]->id . $course->externalId] = array(
                'name' => $name . ' (' . $course->name . ')',
                'id' => $response3->curriculumSet->curriculums[0]->id,
                'endpoint' => $response3->curriculumSet->curriculums[0]->links->self,
                'extra_id' => $course->externalId,
              );
            }
          }
        } elseif($courseStructure->setType == 'CURRICULUM') {
          $response2 = self::_get($courseStructure->link);
          $response3 = self::_get($response2->courseStructure->curriculumSets[0]->links->self);
          $name = self::_extract_name($response3->curriculumSet->curriculums[0]->names);
          $data[$name . $response3->curriculumSet->curriculums[0]->id . $course->externalId] = array(
            'name' => $name . ' (' . $course->name . ')',
            'id' => $response3->curriculumSet->curriculums[0]->id,
            'endpoint' => $response3->curriculumSet->curriculums[0]->links->self,
            'extra_id' => $course->externalId,
          );
        }
      }
    }
    sort($data);
    return $data;
  }

  public static function get_curriculum($endpoint, $set_ids) {
    $data = array();
    $response = self::_get($endpoint);
    foreach($response->curriculum->competenceAimSets as $competenceAimSet) {
      if(!empty($set_ids) && !in_array($competenceAimSet->id, $set_ids)) {
        continue;
      }
      foreach($competenceAimSet->competenceAimSets as $mainGroup){
        $order = str_pad($mainGroup->sortingOrder, '3', 0, STR_PAD_LEFT);
        $name = self::_extract_name($mainGroup->names);
        $id = $competenceAimSet->id;
        $data[$order . $name . $id] = array(
          'name' => $name . ' (' . self::_extract_name($competenceAimSet->names) .  ')',
          'id' => $mainGroup->id,
        );
        foreach($mainGroup->competenceAims as $aim) {
          $data[$order . $name . $id]['aims'][$aim->id] = array(
            'name' => self::_extract_name($aim->names),
            'id' => $aim->id,
          );
        }
        ksort($data[$order . $name . $id]['aims']);
      }
    }
    ksort($data);
    return $data;
  }

  public static function get_main_groups_and_aims($id, $set_ids, $inactive) {
    global $language;
    module_load_include('inc', 'pathauto', 'pathauto');

    $curricula = array();
    $levels = array();
    $groups = array();
    $aims = array();

    /**
     * Fellesfag = Main groups
     * Yrkesfag
     *   vg1 + vg2 = Main groups
     *   vg3 + 0pplæring i bedrift = Main Groups
     *   vg3 = Programfag
     */
    $title = array(
      'type' => '',
      'levels' => array(),
      'business' => FALSE,
    );
    $response = self::_get('courses/' . $id . '?tree=true&resources=false');
    foreach($response->course->courseStructures as $courseStructures) {
      if(in_array($courseStructures->courseStructure->id, $inactive)) {
        continue;
      }
      foreach($courseStructures->courseStructure->curriculumSets as $curriculumSet) {
        $title['type'] = $curriculumSet->setType;
        foreach($curriculumSet->curriculums as $curriculum) {
          $name = self::_extract_name($curriculum->names);
          foreach($curriculum->competenceAimSets as $set) {

            $setName = "";

            if(!empty($set_ids) && !in_array($set->competenceAimSet->id, $set_ids)) {
              continue;
            }
            if(empty($curricula[$curriculum->id])) {
              $curricula[$curriculum->id] = array(
                'name' => $name,
                'id' => $curriculum->id,
              );
            }

            if(empty($set->competenceAimSet->levels)) {
              $level = new stdClass();
              $level->id = $set->competenceAimSet->id;
              $level->names = $set->competenceAimSet->names;
              $set->competenceAimSet->levels[] = $level;
            }

            $psi = NdlaGrepLanguages::iso639_1_psi($language->language);
            $setName = self::_extract_name($set->competenceAimSet->names);
            //foreach($set->competenceAimSet->names as $nameArr) {
            //  if(in_array($psi,$nameArr->scopes) && count($set->competenceAimSet->competenceAims) == 0){
            //    $setName = $nameArr->name;
            //  }
            //}

            foreach($set->competenceAimSet->levels as $level) {
              //$name = trim(self::_extract_name($level->names));
              if(empty($levels[$level->id])) {
                $title['levels'][$level->id] = $level->id;
                $levels[$level->id] = array();
              }
              array_push($levels[$level->id], array(
                'name' => $setName,
                'id' => $level->id,
                'setId' => $set->competenceAimSet->id,
                'classes' => array('curriculum-' . $curriculum->id),
              ));

              foreach($set->competenceAimSet->competenceAimSets as $main_groups) {
                $order = str_pad($main_groups->sortingOrder, '3', 0, STR_PAD_LEFT);
                $name = trim(self::_extract_name($main_groups->names));
                $main_groups_id = pathauto_cleanstring($name);
                if(empty($groups[$order . $main_groups_id])) {
                  $groups[$order . $main_groups_id] = array(
                    'name' => $name,
                    'id' => $main_groups_id,
                    'classes' => array('level-' . $level->id, 'curriculum-' . $curriculum->id),
                  );
                } else {
                  $groups[$order . $main_groups_id]['classes'][] = 'level-' . $level->id;
                  $groups[$order . $main_groups_id]['classes'][] = 'curriculum-' . $curriculum->id;
                  $groups[$order . $main_groups_id]['classes'] = array_unique($groups[$order . $main_groups_id]['classes']);
                }

                foreach($main_groups->competenceAims as $aim) {
                  if(empty($aims[$aim->id])) {
                    $aims[$aim->id] = array(
                      'name' => self::_extract_name($aim->names),
                      'id' => $aim->id,
                      'setId-' => $set->competenceAimSet->id,
                      'groupId' => $main_groups->id,
                      //'classes' => array('setId-' . $set->competenceAimSet->id, 'group-' . $main_groups_id, 'curriculum-' . $curriculum->id, 'level-' . $level->id),
                      'classes' => array('setId-' . $set->competenceAimSet->id, 'group-' . $main_groups_id, 'curriculum-' . $curriculum->id),
                    );
                  } else {
                    $aims[$aim->id]['classes'][] = 'setId-' . $set->competenceAimSet->id;
                    $aims[$aim->id]['classes'][] = 'group-' . $main_groups_id;
                    //$aims[$aim->id]['classes'][] = 'level-' . $level->id;
                    $aims[$aim->id]['classes'][] = 'curriculum-' . $curriculum->id;
                    $aims[$aim->id]['classes'] = array_unique($aims[$aim->id]['classes']);
                  }
                }
                ksort($aims);
              }
              ksort($groups);
            }
          }
        }
      }
    }

    $levels = self::_pruneLevels($levels);
    $groups = self::_pruneGroups($groups,$levels);

    if($title['type'] == 'COMMON' || !in_array('vg3', $title['level'])) {
      $title = t('Main groups');
    } else {
      $title = 'Programfag';
    }

    return array($curricula, $levels, $groups, $aims, $title);
  }

  private static function _pruneLevels($levels) {
    $found = array();
    foreach($levels as $levelId => $levelData){
      foreach($levelData as $levelInfo){
        if(in_array($levelInfo['setId'],$found)){
          unset($levels[$levelId]);
        }
        array_push($found,$levelInfo['setId']);
      }

    }
    return $levels;
  }

  private static function _pruneGroups($groups,$levels) {
    foreach($groups as $groupId => $groupData){
      for($i = 0; $i < count($groupData['classes']);$i++){
        $classId = $groupData['classes'][$i];
        if(strpos($classId,"level") !== false){
          $levelId = substr($classId,strpos($classId,"-")+1);
          if(!array_key_exists($levelId,$levels)){
            unset($groups[$groupId]['classes'][$i]);
          }
        }
      }

    }
    return $groups;
  }

  public static function get_psi($identifier) {
    global $base_url, $base_path, $user;
    $psi = variable_get('ndla_grep_psi', '');
    if(empty($psi)) {
      $psi = $base_url . $base_path;
    }
    return $psi . $identifier;
  }

  public static function save_course($id, $title) {
    $account = variable_get('ndla_grep_account', '');
    $data = array(
      'externalId' => $id,
      'name' => $title,
      'educationGroupId' => $account,
    );
    $url = self::_build_url('courses?education-group=' . $account);
    cache_clear_all($url, 'cache_ndla_grep');
    return self::_post('courses', $data);
  }

  public static function get_course_mapping($id) {
    $account = variable_get('ndla_grep_account', '');
    $response = self::_get('courses?education-group=' . $account);
    foreach($response->courses as $course) {
      if($course->externalId == 'subject_' . $id) {
        $data = array();
        foreach($course->courseStructures as $courseStructure) {
          $id = $courseStructure->id;
          $name = self::_extract_name($courseStructure->names);
          $data[$id] = $name;
        }
        return $data;
      }
    }
    $node = node_load($id);
    return self::save_course('subject_' . $id, $node->title);
  }

  public static function add_course_mapping($gid, $id) {
    $account = variable_get('ndla_grep_account', '');
    $data = array(
      'courseStructureId' => $id,
    );
    $result = self::_post('courses/subject_' . $gid . '/mappings', $data);
    $url = self::_build_url('courses?education-group=' . $account);
    cache_clear_all($url, 'cache_ndla_grep');
    return $result;
  }

  public static function remove_course_mapping($gid, $id) {
    $account = variable_get('ndla_grep_account', '');
    $result = self::_delete('courses/subject_' . $gid . '/mappings/' . $id);
    $url = self::_build_url('courses?education-group=' . $account);
    cache_clear_all($url, 'cache_ndla_grep');
    return $result;
  }

}