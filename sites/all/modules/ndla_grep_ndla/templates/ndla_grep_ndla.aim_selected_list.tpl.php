<ul>
<?php foreach($data as $curriculum): ?>
  <li>
    <span><?php print $curriculum['#name']; ?></span>
    <ul>
    <?php foreach(array_intersect_key($curriculum, array_flip(element_children($curriculum))) as $main_group): ?>
      <li>
        <span><?php print $main_group['#name']; ?></span>
        <ul>
          <?php foreach(array_intersect_key($main_group, array_flip(element_children($main_group))) as $aim): ?>
          <li>
            <span><?php print $aim['#name']; ?> (<?php print $types[$aim['#value']]; ?>)<?php if($aim['#apprentice'] == 'true') print " (" . t('For apprentices') . ")";
            ?></span>
            <?php
            $id_list = array(
              $curriculum['#id'],
              $main_group['#id'],
              $aim['#id'],
            );
            ?>
            <i class="fa fa-times aim-list-remove" data-tree='<?php echo(json_encode($id_list)); ?>'></i>
          </li>
          <?php endforeach; ?>
        </ul>
      </li>
    <?php endforeach; ?>
    </ul>
  </li>
<?php endforeach; ?>
</ul>