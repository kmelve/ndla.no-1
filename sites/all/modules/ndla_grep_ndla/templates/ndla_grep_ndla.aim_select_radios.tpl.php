<ul class='hidden' data-tree='<?php print json_encode($tree); ?>'>
  <?php foreach($types as $key => $name): ?>
    <li>
      <input<?php if($value == $key) print " checked"; ?> class='aim-select-radio' type='radio' name='<?php print $aim_id; ?>' id='<?php print $aim_id; ?>-<?php echo $key; ?>' value='<?php echo $key; ?>'><label for='<?php print $aim_id; ?>-<?php echo $key; ?>'><?php echo $name; ?></label>
   </li>
  <?php endforeach; ?>
  <li>
    <input <?php if($apprentice) print " checked"; ?> type='checkbox' id='<?php print $aim_id; ?>-apprentices' class='aim-select-checkbox'><label for='<?php print $aim_id; ?>-apprentices'><?php print t('For apprentices'); ?></label>
  </li>
</ul>