<ul <?php if($hidden) print 'class="hidden"' ?>>
  <?php foreach($items as $item): ?>
    <?php
      $json_tree = $tree;
      if(!empty($item['extra_id'])) {
        if(!is_array($item['extra_id'])) {
          $item['extra_id'] = array($item['extra_id']);
        }
        foreach($item['extra_id'] as $id) {
          $json_tree[] = array(
            '#id' => $id,
          );
        }
      }
      $json_tree[] = array(
        '#name' => $item['name'],
        '#id' => $item['id'],
      );
      $json_tree = json_encode($json_tree);
    ?>
  <li>
    <span class='aim-list-item<?php if(!empty($item['marked'])) print ' marked'; ?>' data-endpoint='<?php print $item['endpoint'] ?>' data-callback='<?php print $callback ?>' data-tree='<?php print $json_tree ?>'><i class='fa fa-angle-right'></i><?php print $item['name']?></span>
    <?php if(!empty($item['children'])): ?>
      <?php print $item['children'] ?>
    <?php endif ?>
  </li>
<?php endforeach ?>
</ul>