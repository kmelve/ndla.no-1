<?php

/**
 * Implements hook_apachesolr_update_index
 */
function ndla_grep_ndla_apachesolr_update_index(&$document, $node) {
  $aims = NdlaGrepNdlaClient::load_aims_with_names($node, TRUE);
  
  $ids = array_keys($aims);
  $names = array();
  foreach($aims as $aim) {
    foreach($aim as $name) {
      $names[] = $name;
    }
  }

  $document->setField(ndla_grep_ndla_aims_key(), $names);
  $document->setField(ndla_grep_ndla_aims_facet_key(), $names);
  $document->setField(ndla_grep_ndla_aims_id_key(), $ids);
}

/**
 * Implements hook_apachesolr_modify_query
 */
function ndla_grep_ndla_apachesolr_modify_query(&$query, &$params, $caller) {
  $params['fl'] .= ',' . ndla_grep_ndla_aims_key() . ',' . ndla_grep_ndla_aims_id_key();
  $params['qf'][] = ndla_grep_ndla_aims_key();
  $params['qf'][] = ndla_grep_ndla_aims_id_key();
}

/**
 * Creates the proper name for the aims field.
 *  @return
 *  String.
 */
function ndla_grep_ndla_aims_key() {
  $field = array(
    'name'       => 'ndla_grep_competence_aims',
    'multiple'   => TRUE,
    'index_type' => 'text',
  );
  return apachesolr_index_key($field);
}

function ndla_grep_ndla_aims_facet_key() {
  $field = array(
    'name'       => 'ndla_grep_competence_aims_facet',
    'multiple'   => TRUE,
    'index_type' => 'string',
  );
  return apachesolr_index_key($field);
}

function ndla_grep_ndla_aims_id_key() {
  $field = array(
    'name'       => 'ndla_grep_competence_aims_id',
    'multiple'   => TRUE,
    'index_type' => 'string',
  );
  return apachesolr_index_key($field);
}