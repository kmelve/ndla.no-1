var old_height = 0;
function getAndSendHeight() {
  var new_height = $('#main').height();
  var height = {'height': new_height};
  
  if(old_height != new_height) {
    old_height = new_height;
    parent.postMessage(height, '*');
  }
}

$(document).ready(function() {
  $('html').addClass('oembed');
  getAndSendHeight();
  setInterval(getAndSendHeight, 100);
});

$(window).resize(function() {
    getAndSendHeight();
});
