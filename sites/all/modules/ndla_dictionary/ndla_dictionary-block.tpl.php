<div id="ndla_dictionary_wrapper">
	<div id="ndla_dictionary_searchbox">
		<input type="text" id="ndla_dictionary_search" name="ndla_dictionary_search" />
		<div id="ndla_dictionary_clear_button">
		  &nbsp;
		</div>
		<br class="clear" />
		
		<?php
		  if(sizeof($types) != 1) {
		    echo '<select name="ndla_dictionary_type" id="ndla_dictionary_type">';
        foreach($types as $type) {
          echo "<option value='$type'>" . t($type) . "</option>";
        }
        echo '</select>';
      }else{
        echo "<input type='hidden' value='{$types[0]}' id='ndla_dictionary_type'/>";
      }		
		?>
		
	</div>
	<div id="ndla_dictionary_result">
		<ul id="termlist">
		</ul>
	</div>
	<div id="ndla_dictionary_wait" class="hidden">&nbsp;</div><div id="ndla_dictionary_wait_text" class="hidden"><?php print t('Please wait...'); ?></div>
</div>
<div id="ndla_dictionary_detail" class="hidden">
</div>