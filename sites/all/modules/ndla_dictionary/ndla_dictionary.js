if (Drupal.jsEnabled) {
	$('#ndla_dictionary_search').ready(function() {
    $('#ndla_dictionary_search').val('');
    $('#ndla_dictionary_clear_button').click(function() {
      $('#ndla_dictionary_search').val('');
      $('#ndla_dictionary_wrapper').removeClass('expanded');
      ndla_dictionary_onkeyup();
    });

    $('#ndla_dictionary_search').keyup(ndla_dictionary_onkeyup);
    $('#ndla_dictionary_search').focus(function () {
      $('#ndla_dictionary_wrapper').addClass('expanded');
    });
        
    $('#ndla_dictionary_type').change(function () {
      if ($('#ndla_dictionary_search').val() != '') {
        getData($('#ndla_dictionary_search').val().slice(0, 1));
      } else {
        Drupal.settings.ndla_dictionary.current_letter = '';
      }
    });
  });// end ready
}//end if enabled

function ndla_dictionary_onkeyup() {
	first_letter = $('#ndla_dictionary_search').val().slice(0, 1);
	if (first_letter == '') {
		$('#termlist').html('');
	} else {
		if (Drupal.settings.ndla_dictionary.current_letter == '' || Drupal.settings.ndla_dictionary.current_letter != first_letter) {
			Drupal.settings.ndla_dictionary.current_letter = first_letter;
			getData(first_letter);			
		} else {
			filterData();
		}
	}
}

function stripVowelAccent(str) {
	var s = str;

	var rExps = [ /[\xC0-\xC2]/g, /[\xE0-\xE2]/g,
	              /[\xC8-\xCA]/g, /[\xE8-\xEB]/g,
	              /[\xCC-\xCE]/g, /[\xEC-\xEE]/g,
	              /[\xD2-\xD4]/g, /[\xF2-\xF4]/g,
	              /[\xD9-\xDB]/g, /[\xF9-\xFB]/g ];

	var repChar = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u'];

	for (var i = 0; i < rExps.length; i++) {
		s = s.replace(rExps[i], repChar[i]);
	}

	return trim(s);
}


function filterData(jdata) {
	var searchterm = stripVowelAccent($('#ndla_dictionary_search').val());
	var termlist = $('#termlist')[0];

	if (jdata) {
		filterData.jdata = jdata;
	} else {
		if (typeof filterData.jdata == 'undefined') {
			return;
		}
		jdata = filterData.jdata;
	}

	termlist.innerHTML = '';
	
	var hit = false;
	var counter = 0;
	for(i in jdata.terms) {
		if (stripVowelAccent(jdata.terms[i].term.toLowerCase().substr(0,searchterm.length)) == searchterm.toLowerCase()){
			hit = true;
			var li = document.createElement('li');
			li.setAttribute('id', 'term_' + jdata.terms[i].id);
			if (jdata.terms[i].additional_information != '') {
				wd_elm = document.createElement('span');
				wd_elm.setAttribute('class', 'additional_information');
				wd_elm.appendChild(document.createTextNode(' (' + jdata.terms[i].additional_information + ')'));
			}
			li.appendChild(document.createTextNode(jdata.terms[i].term));
			if (jdata.terms[i].additional_information != '') {
				li.appendChild(wd_elm);
			}
			$(li).click(getDetails);

			termlist.appendChild(li);
			
			if(++counter == Drupal.settings.ndla_dictionary.list_length) {
				break;
			}
		} else {
			if (hit) {
				break;
			}
		}
	}
}

function getDetails(e, attemptNum) {
	var node = (e.target.tagName == 'LI') ? e.target : e.target.parentNode;
	var termId = node.getAttribute('id');
	var id = termId.split('_')[1];

	jasonUrl = Drupal.settings.ndla_dictionary.json_details + '/' + id;
	var timeoutCount = (attemptNum) ? attemptNum : 0;
	var ajaxTimeout = parseInt(Drupal.settings.ndla_dictionary.ajax_timeout);
	var ajaxRetries = parseInt(Drupal.settings.ndla_dictionary.ajax_retries);

	$.ajax({
		type: 'GET',
		url: jasonUrl,
		dataType: 'json',
		timeout: ajaxTimeout,
		success: function(data) {
			showDetails(data.details.data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			if ((textStatus != 'timeout') || 
			    ((textStatus == 'timeout') && (++timeoutCount == ajaxRetries))) {
				showDetails('<p>An error occured. Unable to fetch data.</p>');
			} else {
				getDetails(e, timeoutCount);
			}
		}
	});
}

function showDetails (details) {
	$('#ndla_dictionary_detail').html('');
	backButton = document.createElement('div');
	backButton.setAttribute('id','backButton');
	backButton.innerHTML = '&nbsp;';
	backButton.onclick = function () {
		$('#ndla_dictionary_detail').addClass('hidden');
		$('#ndla_dictionary_wrapper').removeClass('hidden');
	};
	$('#ndla_dictionary_detail')[0].appendChild(backButton);
	dl_elm = document.createElement('dl');
	dl_elm.innerHTML = details;
	$('#ndla_dictionary_detail')[0].appendChild(dl_elm);
	$('#ndla_dictionary_detail').removeClass('hidden');
	$('#ndla_dictionary_wrapper').addClass('hidden');
	
}

function getData(letter, attemptNum) {
	$('#ndla_dictionary_wait').removeClass('hidden');
	$('#ndla_dictionary_wait_text').removeClass('hidden');
	$('#ndla_dictionary_result').addClass('hidden');
	$('#ndla_dictionary_type').attr('disabled', 'disabled');
	url = Drupal.settings.ndla_dictionary.json_list + '/' + letter + '/' + $('#ndla_dictionary_type').val(); 
	$.ajax({
		type: 'GET',
		url: url,
		dataType: 'json',
		success: function(data) {
				$('#ndla_dictionary_wait').addClass('hidden');
				$('#ndla_dictionary_wait_text').addClass('hidden');
				$('#ndla_dictionary_result').removeClass('hidden');
				$('#ndla_dictionary_type').removeAttr('disabled');
				filterData(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
					$('#ndla_dictionary_result').html('<p>An error occured. Unable to fetch data.</p>');
		},
	});
}

function trim (str) {
  str = str.replace(/^\s+/, '');
  for (var i = str.length - 1; i >= 0; i--) {
    if (/\S/.test(str.charAt(i))) {
      str = str.substring(0, i + 1);
      break;
    }
  }
  return str;
}