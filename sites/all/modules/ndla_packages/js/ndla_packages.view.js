Drupal.behaviors.ndla_packages = function (context) {

  width = $('.packages-container').width() - 20;
  var limit = Math.ceil((width-28)/50);
  var height = ((Math.ceil(Drupal.settings.ndla_packages_pages.length/limit) - 1) * 50) + 48;
  
  var out = [[]];
  var x = 0;
  var lx = 0;
  var ly = 0;
  $(Drupal.settings.ndla_packages_pages).each(function(key, value) {
    if(out[x].length == limit) {
      x++;
      out[x] = [];
    }
    value['y'] = x;
    value['lx'] = lx;
    value['ly'] = ly;
    if(x%2 == 0) {
      value['x'] = out[x].length;
      out[x].push(value);
    } else {
      value['x'] = limit - out[x].length - 1;
      out[x].unshift(value);
    }
    lx = value['x'];
    ly = value['y'];
  });
  
  var r = Raphael("packages-canvas", width, height);
  var marked = null;
  
  /* Print lines */
  $(out).each(function(a, row) {
    $(row).each(function(x, value) {
      var x = (value['x'] * 50) + 10;
      var y = (value['y'] * 50) + 10;
      var lx = (value['lx'] * 50) + 10;
      var ly = (value['ly'] * 50) + 10;
      var image_marked = r.rect(x - 5, y - 5, 38, 38).attr({fill: "#20588F", "stroke-width": 0});
      var path = r.path("M" + (lx + 14) + " " + (ly + 14) + "L" + (x + 14)  + " " + (y + 14)).attr({stroke: "#757677", "stroke-width": 2});
      r.circle(x + 14, y + 14, 13).attr({fill: "#fff", "stroke-width": 0});
      var image = r.image('/sites/all/modules/ndla_packages/images/' + value['type'] + '.gray.png', x, y, 27, 28);
      //var image_marked = r.image('/sites/all/modules/ndla_packages/images/' + value['type'] + '.blue.png', x, y, 27, 28);
      image_marked.hide();
      image.click(function(){
        //this.hide();
        this.prev.prev.show();
        if(marked != null) {
          //marked.show();
          marked.prev.prev.hide();
        }
        $('#packages-text').html(value['title']);
        marked = this;
      });
      path.toBack();
    });
  });
};