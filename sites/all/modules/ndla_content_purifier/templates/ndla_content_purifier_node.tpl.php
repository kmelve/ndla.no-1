<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html version="XHTML+RDFa 1.0" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" xmlns:og="http://opengraphprotocol.org/schema/" dir="<?php print $language->dir ?>" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:skos="http://www.w3.org/2004/02/skos/core#">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>

    <style type='text/css'>
      body {
        font-size: 16px;
        font-family: "Times New Roman", serif
      }

      .node {
        margin: 10px auto;
      }

      .w960 .center-layout {
        width: 750px;
        margin: 10px;
      }

      .content {
        margin: 10px auto;
      }

      .node_content {
        width: 620px;
      }

      .contentbrowser_caption {
        display: block;
        border:1px solid #e5e5e5;
        padding: 3px;
        font-size: 0.8em;
      }

      .right .contentbrowser_caption {
        border: none;
      } 

      h2 {
        margin-top: 1em;
        margin-bottom: 1em;
      }

      p {
        margin-bottom: 40px;
        font-size: 1.1em;
        line-height: 1.5em;
      }
    </style>

    <script type="text/javascript">
    setTimeout(function () {
        var startY = 0;
        var endY = 0;
        var b = document.getElementById('container');
        var top = 0;
        var old_top = 0;
        document.body.style.top = "0px";
        document.body.style.position = "absolute";
        b.addEventListener('touchstart', function (event) {
            startY = event.targetTouches[0].pageY;
            old_top = parseInt(document.body.style.top.match(/-?\d+/)[0]);
        });
        b.addEventListener('touchmove', function (event) {
            event.preventDefault();
            endY = event.targetTouches[0].pageY;
            if(old_top > 0) {
              old_top = 0;
              document.body.style.top = "0px";
            } else {
              document.body.style.top = (old_top + (startY - endY) * -1) + "px";
            }
        });
    }, 2000);
    </script>
  </head>

  <body>
    <div id="container">
      <div class="center-layout get-back">
        <div id='rs_1'></div>
        <h1><?php print $title ?></h1>
        <?php print $content ?>
      </div>
    </div>
  </body>
</html>
