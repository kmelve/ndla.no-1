<?php

abstract class NdlaPurifierInjector extends HTMLPurifier_Injector {
  public $name;
  public function handleElement(&$token) {}
  public function handleEnd(&$token) {
    if($token->markForDelete) {
      $token = false;
    }
    if($token->rename) {
      $token->name = $token->new_name;
    }
  }
}

class EasyReaderViewInjector extends NdlaPurifierInjector {
  public $name = 'EasyReaderViewInjector';

  public function handleElement(&$token) {
    if($token->name == 'fieldset') {
      $token->name = 'div';
      while($this->forwardUntilEndToken($i, $current, $nesting)) {
        ;
      }
      $current->rename = true;
      $current->new_name = 'div';
    }
    /* // Remove the a-tags in links and makes them regular text.
    if( $token->name == 'a' && $token->attr['class'] !== 'rsbtn_play') {
      // This whileloop forwards the scope until the end then marks closing
      // tag for removal.
      while($this->forwardUntilEndToken($i, $current, $nesting)) {};
      $current->markForDelete = true;
      $token = false;
    }
    //*/

    if($token->attr['class'] == 'rsbtn_play') {
      $token->armor['ValidateAttributes'] = true;
      // For some reason &lang gets translated to it's html unicode sign that is '〈'.
      // Therefore a str_replace is needed to swich it back to &lang instead.
      $token->attr['href'] = str_replace("\xE2\x8C\xA9", '&lang', $token->attr['href']);
    }
  }
}

class MobileViewInjector extends NdlaPurifierInjector {
  public $name = 'MobileViewInjector';

  public function handleElement(&$token) {
    unset($token->attr['style']);

    if($token->name == 'iframe' && $token->attr['class'] == 'istribute_video') {
      $token->armor['ValidateAttributes'] = true;
    }

    if($token->name == 'iframe' && $token->attr['class'] == 'h5p-iframe') {
      $token->armor['ValidateAttributes'] = true;
    }



    if( $token->attr['class'] == 'contentbrowser_caption') {
      $token = array(new HTMLPurifier_Token_Empty('br'), $token);
    }

    if( $token->name == 'script' ) {
      $token = false;
      while($this->forwardUntilEndToken($i, $current, $nesting)) {
        $current->data = '';
      }
      $current->markForDelete = true;
    }

    if( $token->name == 'a' ) {
      $token->attr['href'] = preg_replace("/\/lightbox/", "", $token->attr['href']);
      $token->attr['rel'] = NULL;
      if($token->attr['class'] === 're-collapse' || $token->attr['class'] === 'read-more') {
        $token = false;
        while($this->forwardUntilEndToken($i, $current, $nesting)) {
          $current->data = '';
        }
        $current->markForDelete = true;
      }
    }

    if( $token->attr['class'] == 'swftools swfobject2') {
      $token->attr['id'] = "";
    }

    //if( $token->attr['id'] == 'readspeaker_button1' ) {
    //  $token = false;
    //  while($this->forwardUntilEndToken($i, $current, $nesting)) {
    //    $current->data = '';
    //  }
    //  $current->markForDelete = true;
    //}

    if($token->name === 'p') {
      $data = '';
      while($this->forwardUntilEndToken($i, $current, $nestin)) {
        $data .= $current->data;
      }

      if(ord($data) === 194) {
        $token = false;
        $current->markForDelete = true;
      }
    }
  }
}

class EmbedViewInjector extends NdlaPurifierInjector {
  public $name = 'EmbedViewInjector';

  public function handleElement(&$token) {
    unset($token->attr['style']);
    if( $token->attr['class'] == 'contentbrowser_caption') {
      $token = array(new HTMLPurifier_Token_Empty('br'), $token);
    }

    if ( $token->attr['class'] == 'right') {
      $token->armor['ValidateAttributes'] = true;
      $token->attr['style'] = 'float:right;width:194px;clear:right;position:relative;margin: 40px;';
    }

    if( $token->name == 'script' ) {
      $token = false;
      while($this->forwardUntilEndToken($i, $current, $nesting)) {
        $current->data = '';
      }
      $current->markForDelete = true;
    }

    if( $token->name == 'a' ) {
      $token->attr['href'] = preg_replace("/\/lightbox/", "", $token->attr['href']);
      $token->attr['rel'] = NULL;
      if($token->attr['class'] === 're-collapse' || $token->attr['class'] === 'read-more') {
        $token = false;
        while($this->forwardUntilEndToken($i, $current, $nesting)) {
          $current->data = '';
        }
        $current->markForDelete = true;
      }
    }

    if($token->attr['class'] === 'contentbrowser contentbrowser') {
      $token->armor['ValidateAttributes'] = true;
      $token->attr['style'] = 'float: left; padding: 10px;';
    }

    if( $token->attr['class'] == 'swftools swfobject2') {
      $token->attr['id'] = "";
    }
  }
}


class PdfViewInjector extends EasyReaderViewInjector {
  public $name = 'PdfViewInjector';
  protected $img_prepend_string = 'file://';

  public function handleElement(&$token) {
    global $base_path, $base_url, $current_ndla_export_node;

    if( $token->name == 'div' ) {
      $classes = explode(' ', $token->attr['class']);
      // Remove TinyMCE and Readspeaker stuff
      // And remove Drupal meta div.
      if($token->attr['class'] == 'details' || $token->attr['class'] == 'hide') {
        $token->attr['class'] = 'hidden_paragraphs';
      }
      if(in_array('mcePaste', $classes) || in_array('rsbtn', $classes) || in_array('meta', $classes)) {
        $token = false;
        while($this->forwardUntilEndToken($i, $current, $nesting)) {
          $current->data = '';
        }
        $current->markForDelete = true;
      }
    }
    
    if($token->attr['class'] == 'qr_image') {
      $token->armor['ValidateAttributes'] = true;
    }

    if($token->name == 'span') {
      //rsimg
      $classes = explode(' ', $token->attr['class']);
      if(in_array('rsimg', $classes) || in_array('rsbtn_text', $classes)) {
        $token = false;
        while($this->forwardUntilEndToken($i, $current, $nesting)) {
          $current->data = '';
        }
        $current->markForDelete = true;
      }
    }

    if ($token->name == 'a') {
       $classes = explode(' ', $token->attr['class']);

       $href = $token->attr['href'];
       $first_char = substr($href, 0, 1);

       if($first_char == '/') {
           $href = $base_url . $href;
           $token->attr['href'] = $href;
       }

       if($href == '#') {
           $token = false;
           while($this->forwardUntilEndToken($i, $current, $nesting)) {
             $current->data = '';
           }
           $current->markForDelete = true;
       }

       $last_chars = substr($href, -3);

       if(in_array(strtolower($last_chars), array('jpg','png','gif'))) {
        $token = false;
       }

       $token->attr['rel'] = NULL;
      // Remove TinyMCE and Readspeaker stuff
      if(in_array('rsbtn_play', $classes) || in_array('re-collapse', $classes)) {
          $token = false;
          while($this->forwardUntilEndToken($i, $current, $nesting)) {
            $current->data = '';
          }
          $current->markForDelete = true;
      }
    }

    if ( $token->attr['class'] == 'frame') {
        $token->armor['ValidateAttributes'] = true;
        $token->attr['style'] = 'border: 1px solid #cccccc';
    }

    if ( $token->attr['class'] == 'quote') {
        $token->armor['ValidateAttributes'] = true;
        $token->attr['style'] = 'font-family: Arial, sans-serif; font-size: 110%; color: #336669; font-style: italic; font-weight: bold; margin: 15px 0px;';
    }

    

    if( $token->name == 'pre') {
      // This whileloop forwards the scope until the end
      // then marks closing tag for removal.
      while($this->forwardUntilEndToken($i, $current, $nesting)) {};
      $current->markForDelete = true;
      $token = false;
    }

    if($token->name === 'p') {
      $data = '';
      while($this->forwardUntilEndToken($i, $current, $nestin)) {
        $data .= $current->data;
      }

      if(ord($data) === 194) {
        $token = false;
        $current->markForDelete = true;
      }
    }
    
    if($token->name === 'iframe') {
      // Fast forward and then mark end tag for deletion.
      while($this->forwardUntilEndToken($i, $current, $nesting)) {}; $current->markForDelete = true;

      $token = array();
      $element = new HTMLPurifier_Token_Text(ndla_export_qrcode($current_ndla_export_node));
      $element->noEscapeHTML = true;
      $token[] = $element;
    }

    if($token->name === 'img' && strpos($token->attr['alt'], '«math') !== FALSE) {
      $replace = array(
        '«' => '<',
        '»' => '>',
        '§' => '&',
        '¨' => '"',
      );

      $math = str_replace(array_keys($replace), array_values($replace), $token->attr['alt']);

      $token = array();
      $token[] = new HTMLPurifier_Token_Text("\n\n");

      $element = new HTMLPurifier_Token_Text($math);
      $element->noEscapeHTML = true;
      $token[] = $element;

      $token[] = new HTMLPurifier_Token_Text("\n\n");
    }


    if($token->name == 'img' && $token->attr['class'] != 'qr_image') {
      $this->handleImage($token);
    }

    if(in_array($token->name, array('h1','h2','h3','h4','h5','h6'))) {
      $title = '';
      while($this->forwardUntilEndToken($i, $current, $nesting)) {
        $title .= $current->data;
        $current->data = "";
      }
      $element = new HTMLPurifier_Token_Text('<span class="headline">' . $title . '</span>');
      $element->noEscapeHTML = true;
      $token = array($element);
    }

    parent::handleElement($token);
  }

  protected function handleImage(&$token) {
    $token->attr['class'] = '';
    global $base_path, $base_url, $current_ndla_export_node;
    static $missing_files = array();

    $url_list = array(
      $base_url . '/nb' . $base_path,
      $base_url . '/nn' . $base_path,
      $base_url . '/en' . $base_path,
      $base_url . $base_path,
      'http://ndla.no/',
    );

    $src = str_replace($url_list, realpath('.') . '/', $token->attr['src']);
    $src = urldecode($src);
    $src = str_replace(" ", "%20", $src);
    $img = str_replace(realpath('.'),'',$src);

    $exists = false;
    if(file_exists($src)) {
      $exists = true;
    }

    if(mb_stripos($src, 'ø') !== FALSE) { // If image filename contains 'ø' always return
      return;
    }

    if(mb_stripos($src, 'å') !== FALSE) { // If image filename contains 'ø' always return
      return;
    }

    if(mb_stripos($src, 'æ') !== FALSE) { // If image filename contains 'ø' always return
      return;
    }

    if(file_exists(urldecode($src))) {
      $exists = true;
      $src = urldecode($src);
    }

    if(!$exists) {
      return; // File not found on filesystem. Just return here.
    }

    if($exists && !in_array($img, $missing_files)) {
      $src_array = pathinfo($src);
      $src = $src_array['dirname'] . '/' . urlencode(utf8_decode($src_array['basename']));

      $src = str_replace('+', '%20', $src);
      // %F8
      $src = str_replace("%F8", "ø", $src);
      $src = str_replace("ø", "ø", $src);

      $token->attr['src'] = $this->img_prepend_string . $src;
    }
  }
}

class ePubViewInjector extends PdfViewInjector {
  public $name = 'ePubViewInjector';
  protected $img_prepend_string = '';

  public function __construct() {
  }

  public function handleElement(&$token) {
    parent::handleElement($token);
  }

  public function handleImage(&$token) {
    parent::handleImage($token);
  }
}
//
// vim: et sw=2 sts=2
