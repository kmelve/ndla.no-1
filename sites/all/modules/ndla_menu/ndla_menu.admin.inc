<?php

/**
 * Remove duplicate leaf nodes in ndla menus.
 */
function ndla_menu_remove_dups() {
  // Find all menus
  $query = "SELECT id FROM {ndla_menu_menus}";
  $result = db_query($query);

  $item_list = array();
  $dup_count = 0;
  while ($menu = db_fetch_object($result)) {
    // Find all leaf nodes in menu
    // Note: The reason for doing this per menu is that we then utilize the existing index
    $query = "SELECT i1.*, t.title, t.language, t.path
              FROM {ndla_menu_items} i1
              INNER JOIN {ndla_menu_translations} t ON i1.id = t.menu_item_id
              WHERE menu_id=%d
                AND i1.parent_id > 0
                AND (SELECT COUNT(*)
                     FROM {ndla_menu_items} i2
                     WHERE menu_id=%d
                       AND i2.parent_id=i1.id
                    ) = 0
              ORDER BY i1.parent_id, i1.id";
    $result2 = db_query($query, $menu->id, $menu->id);
    while ($menu_item = db_fetch_object($result2)) {
      // Items with same parent, title and language are considered duplicates
      $hash = md5($menu_item->parent_id . $menu_item->title . $menu_item->language . $menu_item->path);

      if ($menu_item->language == '') {
        // We do not want to 
        continue;
      }
      if (isset($item_list[$hash])) {
        db_query("DELETE i, t FROM {ndla_menu_items} i JOIN {ndla_menu_translations} t ON i.id = t.menu_item_id WHERE i.id = %d", $menu_item->id);
        $dup_count++;
      }
      else {
        $item_list[$hash] = TRUE;
      }
    }
  }

  print "Removed {$dup_count} duplicates";
}