
var __utils = require('utils');


casper.test.begin('Load page', function suite(test) {
    casper.start("http://red.ndla.l/nb/fag/52234", function() {
        test.assertExists('.ndla2010-tab-center');
        test.assertTitle('Biologi 1 | Nasjonal digital læringsarena', 'Make sure title is correct');
        this.clickLabel('Tema');
    });

    casper.then(function() {
        test.assertExists('.tray-loader');
        
    });

    casper.run(function() {
        test.done();
    });
});
