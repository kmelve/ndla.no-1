<div id="ndla-menu-fadeout"></div>
<div id="ndla-menu-edit">
  <ul class="controls">
    <li>
      <a class="reorder" href="javascript:void(0);"><span class="icon"></span><?php print t('Reorganize') ?></a>
    </li>
    <li>
      <a class="new-item" href="javascript:void(0);"><span class="icon"></span><?php print t('New item') ?></a>
    </li>
  </ul>
  <ul class="ndla-menu-edit">
    <?php print $menu_items ?>
  </ul>
</div>