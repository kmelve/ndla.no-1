<?php
global $language, $base_path;
foreach($items as $item) {
  $expanded = empty($item->expanded) ? '' : ' expanded';
  print '<div>';
  if(!empty($item->children)) {
    print '<div class="ndla_menu_arrow' . $expanded . '">&nbsp;</div>'; 
  }
  if(!empty($item->path)) {
    print '<a href="' . $base_path . $item->path . '">' . $item->title . '</a>';
  } else {
    print $item->title;
  }
  if(!empty($item->children)) {
    print '<div class="ndla_menu_children' . $expanded . '">';
    print theme('ndla_menu_view', $item->children);
    print '</div>';
  }
  print '</div>';
}