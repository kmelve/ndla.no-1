<?php if (count($filters) > 1): ?>
  <ul class="filters">
    <?php foreach ($filters as $tid => &$name): ?>
      <li class="filter-<?php print $tid ?>"><?php print check_plain($name) ?></li>
    <?php endforeach ?>
  </ul>
  <div class="clearfix"></div>
<?php else: ?>
  <div class="emptyrator"></div>
<?php endif ?>
<?php if ($menu_items != ''): ?>
  <ul class="menu" role="tree">
    <?php print $menu_items ?>
  </ul>
<?php endif ?>