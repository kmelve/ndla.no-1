$(document).ready(function () {
  new NdlaMenuEdit();  
});

function NdlaMenuEdit() {
  var that = this;
  this.$wrapper = $('#ndla-menu-edit');
  this.$children = $('.children', this.$wrapper);
  
  var $lis = $('.ndla-menu-edit', this.$wrapper).css('position', 'static');
  $lis.children('li').each(function () {
    that.treeify(this);
  });
  
  $('.reorder', this.$wrapper).click(function () {
    var $this = $(this);
    if ($this.hasClass('reordering')) {
      $this.removeClass('reordering').html('<span class="icon"></span>' + Drupal.t('Reorganize'));
      that.finishReorder();
    }
    else {
      $this.addClass('reordering').html('<span class="icon"></span>' + Drupal.t('Finished'));
      that.initReorder();
    }
  });
  $('#edit-ndla-menu-item-language').change(function(){
    NdlaMenu.change_language($(this).val());
  });
  // Simulated delegte event trigger.
  $('#edit-ndla-menu-item-language').parent().click(function(event){
    if($.browser.mozilla) {
      $target = $(event.originalTarget);
    } else {
      $target = $(event.toElement);
    }

    if($target.attr('id').match(/^link-to-new-lang/)) {
      event.preventDefault();
      selected_lang = $target.attr('id').match(/--(.*)$/)[1];
      var desiredValue = selected_lang;
      var el = $('#edit-ndla-menu-item-language').get(0);
      for(var i=0; i < el.options.length; i++) {
        if ( el.options[i].value == desiredValue ) {
          el.selectedIndex = i;
          $(el).trigger('change');
          break;
        }
      }
    }
  });
  
  $('.cancel-item').click(function(){
    $('#ndla-menu-edit').slideDown();
    $('#ndla-menu-item-form').slideUp();
    $('#ndla-menu-fadeout').fadeOut();
    NdlaMenu.reset_language_selector();
  });
  $('.remove-item').click(function(){
    if (!confirm(Drupal.t('Are you absolutely sure that you wish to delete this menu item? If this menu item has sub-items they will also be deleted.'))) {
      return false;
    }
    var item_id = $('#edit-ndla-menu-item-id').val();
    $('li.menu-item-' + item_id).remove();
    NdlaMenu.remove_item_data(item_id);
    $('#ndla-menu-edit').slideDown();
    $('#ndla-menu-item-form').slideUp();
    $('#ndla-menu-fadeout').fadeOut();
    NdlaMenu.reset_language_selector();
    NdlaMenu.find_term_diff();
  });
  $('.save-item').click(function(){
    NdlaMenu.save();
    $('#ndla-menu-edit').slideDown();
    $('#ndla-menu-item-form').slideUp();
    $('#ndla-menu-fadeout').fadeOut();
    NdlaMenu.reset_language_selector();
    alert("SAVE!");
    NdlaMenu.find_term_diff();
  });
  $('.new-item').click(function(){
    NdlaMenu.new_menu = true;
    NdlaMenu.language = Drupal.settings.ndla_utils.language;
    NdlaMenu.change_language(Drupal.settings.ndla_utils.language);
    $('#edit-ndla-menu-item-id').val(new Date().getTime());
    $('#ndla-menu-item-form input[type=text]').val('');
    $('#ndla-menu-item-form input[type=checkbox]').attr('checked', '');
    $('#ndla-menu-item-form input[type=radio]').attr('checked', '');
    $('#ndla-menu-item-form textarea').val('');
    $('.ndla-menu-image-preview').hide();
    $('.ndla-menu-image-remove').hide();
    $('#ndla-menu-edit').slideUp();
    $('#ndla-menu-item-form').slideDown();
    $('#ndla-menu-fadeout').fadeIn();
  });
  $('#edit-submit,#edit-save-and-continue').click(function() {
    $('#edit-ndla-menu-json').val(JSON.stringify(NdlaMenuData));
    $('#ndla-menu-edit input').remove();
  });
  $('.ndla-menu-image').change(function(){
    element_id = $(this).attr('id');
    NdlaMenu.upload(element_id);
  });
  NdlaMenu.find_term_diff();
}

NdlaMenuEdit.prototype.treeify = function (li) {
  var that = this,
  $li = $(li),
  $ul = $li.children('ul');
  
  if ($ul.length) {
    var $lis = $ul.children('li').each(function () {
      that.treeify(this);
    });
    
    if ($lis.filter('.open, .place').length) {
      $li.addClass('open');
    }
    else if (!$li.hasClass('open')) {
      $ul.hide();
    }
    
    $li.click(function (e) {
      if (that.$wrapper.hasClass('reorder') || $li.hasClass('place')) {
        return;
      }
      var $this = $(this),
      $ul = $this.children('ul');
      if ($this.hasClass('open')) {
        $this.removeClass('open');
        $ul.animate({height: 0}, 200, function () {
          $ul.hide().css({height: 'auto'});
        });
      }
      else {
        $this.addClass('open');
        $ul.show();
        var height = $ul.height();
        $ul.css({height: 0}).animate({height: height}, 200, function () {
          $ul.css({height: 'auto'});
        });
      }
      e.stopPropagation();
    });
  }
  else {
    $li.click(function (e) {
      e.stopPropagation();
    });
  }
  $li.children('.item').children('.edit').click(function (e) {
    e.stopPropagation();
  }).end().children('.arrow').click(function (e) {
    e.preventDefault();
  });
}

NdlaMenuEdit.prototype.initReorder = function () {
  var that = this;
  
  this.$wrapper.addClass('reorder').prev().stop().fadeIn(200);
  
  $('.item', this.$wrapper).mousedown(function (e) {
    that.initMove(this, e.pageX, e.pageY);
  });
  
  this.enableHovering();
}

NdlaMenuEdit.prototype.enableHovering = function () {
  this.$children.hover(function () {
    $(this).parent().addClass('select');
  }, function () {
    $(this).parent().removeClass('select');
  });
}
NdlaMenuEdit.prototype.disableHovering = function () {
  this.$children.unbind('mouseenter').unbind('mouseleave');
}

var mouse_y;
var in_scroll = false;

doScroll = function(x, y) {
  if(y < 0) {
    y = 0;
  }
  if(y > $(window).height()) {
    y = $(window).height();
  }
  in_scroll = true;
  if(mouse_y < 100) {
    y = y - 20;
    window.scrollTo(x, y);
  }
  if(mouse_y > $(window).height() - 100) {
    y = y + 20;
    window.scrollTo(x, y);
  }
  setTimeout(function(x, y) {
    return function() {
      if(in_scroll) {
        doScroll(x, y);
      }
    }
  }(x, y), 50);
}

NdlaMenuEdit.prototype.initMove = function (item, x, y) {
  $(document).mousemove(function(e){
    mouse_y = e.clientY;
    if(!in_scroll) {
      doScroll(e.clientX, e.clientY);
    }
  });
  this.disableHovering();

  var that = this;
  this.$li = $(item).parent().addClass('moving');

  var offset = this.$li.offset(),
  wOffset = this.$wrapper.length ? this.$wrapper.offset() : this.$base.offset();
  
  this.lastX = x;
  
  this.x = x - offset.left;
  this.y = y - offset.top;
  
  this.handleX = wOffset.left + this.x;
  this.handleY = wOffset.top + this.y;
  
  this.height = this.$li.height();

  this.$li.css({left: x - this.handleX, top: y - this.handleY});
  this.$prev = $('<li style="height:' + this.height + 'px"></li>').insertBefore(this.$li);
  
  this.$wrapper.addClass('moving')
  $('body').mouseup(function() {
    that.finishMove();
    $(document).unbind('mousemove');
    in_scroll = false;
  }).attr('unselectable', 'on').css({'-moz-user-select': 'none', '-webkit-user-select': 'none', 'user-select': 'none', '-ms-user-select': 'none'}).mousemove(function (e) {
    that.$li.css({left: e.pageX - that.handleX, top: e.pageY - that.handleY});
    
    var x = e.pageX - that.x,
    y = e.pageY - that.y,
    scroll = $(document).scrollTop();
    
    if (that.moveUp(x, y, scroll)) {
      return;
    }
    else if (that.moveDown(x, y, scroll)) {
      return;
    }
    else if (that.moveLeft(e.pageX)) {
      return;
    }
    else if (that.moveRight(e.pageX)) {
      return;
    }
  })[0].onselectstart = function () {
    return false;
  };
}

NdlaMenuEdit.prototype.moveUp = function (x, y, scroll) {
  var $li = $(document.elementFromPoint(x + 225, y - 1 - scroll));
  if ($li.hasClass('name')) {
    $li = $li.parent().parent();
  }
  else if ($li.hasClass('item')) {
    $li = $li.parent();
  }
  else {
    return 0;
  }

  if (($li.offset().top + (($li.children('.item').height() + 8) / 2) - 2) > y) {
    var $parent = this.$li.parent();
    this.$prev.add(this.$li).insertBefore($li);
    if (!$parent.children('li').length) {
      $parent.prev().removeClass('children').end().remove();
    }
    this.height = this.$li.height();
    this.$prev.css({height: this.height});
    return 1;
  }
  return 0;
}

NdlaMenuEdit.prototype.moveDown = function (x, y, scroll) {
  var $li = $(document.elementFromPoint(x + 225, y + this.height + 1 - scroll));
  if ($li.hasClass('name')) {
    $li = $li.parent().parent();
  }
  else if ($li.hasClass('item')) {
    $li = $li.parent();
  }
  else {
    return 0;
  }

  if (($li.offset().top + (($li.children('.item').height() + 8) / 2) + 2) < y + this.height) {
    var $parent = this.$li.parent(),
    $ul = $li.filter('.open').children('ul');
    if ($ul.length) {
      this.$li.add(this.$prev).prependTo($ul)
    }
    else {
      this.$li.add(this.$prev).insertAfter($li);
    }
    if (!$parent.children('li').length) {
      $parent.prev().removeClass('children').end().remove();
    }
    this.height = this.$li.height();
    this.$prev.css({height: this.height});
    return 1;
  }
  return 0;
}

NdlaMenuEdit.prototype.moveLeft = function (x) {
  if (x < this.lastX - 10 && !this.$li.next().length) {
    var $ul = this.$li.parent(),
    $li = $ul.parent();
    if ($li.is('li')) {
      this.$li.add(this.$prev).insertAfter($li);
      if (!$ul.children('li').length) {
        $li.removeClass('open')
        $ul.prev().removeClass('children').end().remove();
      }
      this.lastX -= 10;
      this.height = this.$li.height();
      this.$prev.css({height: this.height});
      return 1;
    }
  }
  return 0;
}

NdlaMenuEdit.prototype.moveRight = function (x) {
  if (x > this.lastX + 10) {
    var $prev = this.$prev.prev();
    if ($prev.length) {
      var $ul = $prev.children('ul');
      if (!$ul.length) {
        $prev.addClass('open')
        $ul = $('<ul role="group"></ul>').appendTo($prev);
        $ul.prev().addClass('children');
      }
      else if (!$prev.filter('.open').length) {
        $prev.addClass('open').find('> ul').show();
        return 0;
      }
      $ul.append(this.$prev.add(this.$li));
      this.lastX += 10;
      this.height = this.$li.height();
      this.$prev.css({height: this.height});
      return 1;
    }
  }
  return 0;
}

NdlaMenuEdit.prototype.finishMove = function () {
  this.$wrapper.removeClass('moving');
  delete $('body').unbind('mousemove').unbind('mouseup').removeAttr('unselectable').removeAttr('style')[0].onselectstart;
  this.$li.removeClass('moving').removeAttr('style');
  this.$prev.remove();
  
  $pli = this.$li.parent().parent();
  
  /* Set parent id */
  pid = $pli.is('li') ? $pli.attr('class').split(' ')[0].split('-')[2] : 0;
  item_id = this.$li.attr('class').split(' ')[0].split('-')[2];
  NdlaMenuData[item_id]['parent_id'] = pid;
  
  /* Set weight */
  this.$li.parent().children().each(function(key, element){
    item_id = $(element).attr('class').match(/menu-item-([^\s]*)/)[1];
    if(item_id != 'new') {
      NdlaMenuData[item_id]['weight'] = key;
    }
  });
  
  this.enableHovering();
}

NdlaMenuEdit.prototype.finishReorder = function () {
  this.$wrapper.removeClass('reorder');
  $('.item', this.$wrapper).unbind('mousedown');
  this.disableHovering();  
  this.$wrapper.prev().stop().fadeOut(200);
}

function NdlaMenuNodeForm() {
}

NdlaMenu = {};

NdlaMenu.reset_language_selector = function() {
  $('#edit-ndla-menu-item-language').show();
  $('.new-lang-link').remove();
  NdlaMenu.new_menu = false;
  $('#edit-ndla-menu-item-id').val("")
}

NdlaMenu.redraw_language_selector = function() {
  var language_list = Drupal.settings.language_list; //['und', 'nb','nn','en'];
  var used_languages = [];
  var menu_id = $('#edit-ndla-menu-item-id').val();
  if(!menu_id) {
    menu_id = document.current_item_id; // fallback method
  }

  $('.new-lang-link').remove();
  $(language_list).each(function(_, item) {
    if(NdlaMenuData[menu_id][item].title) {
      used_languages.push(item);
    }
    if($('#edit-ndla-menu-item-'+item+'-title').val()) {
      used_languages.push(item); 
    }
  });
  
  current_lang_text = Drupal.t('<p class="new-lang-link"><br /><br />Current language is ' + NdlaMenu.language + '</p>');
  $('#edit-ndla-menu-item-language').after(current_lang_text);

  $(language_list).each(function(_,item) {
    if(item == NdlaMenu.language)
      return;
    if(jQuery.inArray(item, used_languages) !== -1) {
      text = Drupal.t('Go to language ' + item);
      html = '<a class="new-lang-link" id="link-to-new-lang--' + item + '" href="#">' + text + '<br /></a>';
    } else {
      text = Drupal.t('Add new language ' + item);
      html = '<a class="new-lang-link" id="link-to-new-lang--' + item + '" href="#">' + text + '<br /></a>';
    }

    $('#edit-ndla-menu-item-language').after(html);
    $('#edit-ndla-menu-item-language').hide();
  });

}

NdlaMenu.edit = function(element) {
  var item_id = $(element).parents('li').attr('class').split(' ')[0].split('-')[2];
  document.current_item_id = item_id;
  var language_list = Drupal.settings.language_list;
  var menu_id = $('#edit-ndla-menu-item-id').val()

  NdlaMenu.change_language(Drupal.settings.ndla_utils.language);

  $('.remove-item').show();
  $('.ndla-menu-image-preview').hide();
  $('.ndla-menu-image-remove').hide();
  
  /* Transfer fields */
  $(language_list).each(function(key, language) {
    $('input.ndla-menu-terms-' + language).each(function(key, element) {
      var term_id_split = element.id.split('-');
      var term_id = term_id_split[term_id_split.length - 1];
      var value = NdlaMenuData[item_id][language]['terms'][term_id];

      if(value != '0') {
        element.checked = true;
      } else {
        element.checked = false;
      }
    });

    var image_path = NdlaMenuData[item_id][language]['image_path'];
    if(image_path != '' && image_path != undefined) {
      $('.ndla-menu-image-preview.' + language).show().attr('src', Drupal.settings.basePath + image_path);
      $('.ndla-menu-image-remove.' + language).show();
    }

    $('#edit-ndla-menu-item-id').val(item_id);
    
    $(['title',
      'path',
      'image_upload',
      'published',
      'image_path',
      'image_id',
      'image_height',
      'image_width',
      'expanded',
      'description',
      'include_text',
      'has_content'
      ]).each(function(key, field){
        var id_field = field.replace('_', '-');
        $to = $('#edit-ndla-menu-item-' + language + '-' + id_field);
        var type = $to.attr('type');
        var value = NdlaMenuData[item_id][language][field];
        if(value == null) {
          value = "";
        }
        if(field == 'has_content') {
          $('#edit-ndla-menu-item-' + language + '-has-content-' + NdlaMenuData[item_id][language][field]).attr('checked', 'checked');
        }
        switch(type) {
          case 'hidden':
          case 'text':
          case 'textarea':
            $to.val(value);
            break;
          case 'checkbox':
            if(value == 1) {
              $to.attr('checked', 'checked');
            } else {
              $to.attr('checked', null);
            }
            break;
          case undefined:
            break;
          default:
            alert(type);
        }
    });  
  });
  $('#ndla-menu-fadeout').fadeIn();
  $('#ndla-menu-edit').slideUp();
  $('#ndla-menu-item-form').slideDown();
}

NdlaMenu.save = function() {
  var item_id = $('#edit-ndla-menu-item-id').val();
  
  if(NdlaMenuData[item_id] == undefined) {
    NdlaMenuData[item_id] = {
      'id' : item_id,
      'parent_id' : 0,
      'weight' : 10000
    }
    
    var l = Drupal.settings.language_list;
    for(i in l) {
     NdlaMenuData[item_id][l[i]] = {'terms': {}}; 
    }
    $new_item = $('.menu-item-new').clone();
    $new_item.attr('class', $new_item.attr('class').replace('new', item_id));
    $('.menu-item-new').before($new_item);
  }

  var title = 'N/A';
  /* Transfer fields */
  $(Drupal.settings.language_list).each(function(key, language) {
    $('input.ndla-menu-terms-' + language).each(function(key, element) {
      var term_id = element.id.split('-')[6];
      var value = (element.checked) ? term_id : "0";
      NdlaMenuData[item_id][language]['terms'][term_id] = value;
    });

    $(['title',
      'path',
      'image_upload',
      'published',
      'image_path',
      'image_id',
      'image_height',
      'image_width',
      'expanded',
      'description',
      'include_text',
      'has_content'
    ]).each(function(key, field){
      var id_field = field.replace('_', '-');
      $from = $('#edit-ndla-menu-item-' + language + '-' + id_field);
      var type = $from.attr('type');

      if(field == 'title') {
        if($from.val() != '' && (title == 'N/A' || language == Drupal.settings.ndla_utils.language)) {
          title = $from.val();
        }
        if($from.val() != '') {
          $('.menu-item-' + item_id + ' > div > .name > .translations > .' + language).show();
        } else {
          $('.menu-item-' + item_id + ' > div > .name > .translations > .' + language).hide();
        }
      }
      if(field == 'published') {
        if($from.attr('checked') == true) {
          $('.menu-item-' + item_id + ' > div > .name > .translations > .' + language).removeClass('unpublished');
        } else {
          $('.menu-item-' + item_id + ' > div > .name > .translations > .' + language).addClass('unpublished');
        }
      }
      if(field == 'path') {
        if($from.val() == '') {
          $('.menu-item-' + item_id + ' > div > .name > .translations .' + language).next().html('');
        } else {
          $.getJSON(Drupal.settings.basePath + 'ndla-menu/status?path=' + $from.val(), function(data) {
            $('.menu-item-' + item_id + ' > div > .name > .translations .' + language).next().html(data.status);
          });
        }
      }

      if(field == 'has_content') {
        NdlaMenuData[item_id][language][field] = $('input[name="ndla_menu[item][' + language + '][has_content]"]:checked').val();
      }
        
      switch(type) {
        case 'hidden':
        case 'text':
        case 'textarea':
          NdlaMenuData[item_id][language][field] = $from.val();
          break;
        case 'checkbox':
          if($from.attr('checked') == true) {
            NdlaMenuData[item_id][language][field] = 1;
          } else {
            NdlaMenuData[item_id][language][field] = 0;
          }
          break;
        case undefined:
          break;
        default:
          alert(type);
      }
    });
  });
  $('li.menu-item-' + item_id + ' > div.item > div.name > span.title').html(title);
}

NdlaMenu.change_language = function(language) {
  $('#edit-ndla-menu-item-language').val(language);
  $('.menu-item-edit-lanugage').hide();
  $('.menu-item-edit-lanugage.' + language).show();
  
  if(!this.new_menu) {
    this.language = language;
    this.redraw_language_selector();
    return;
  }

  var ids = {};
  var image = {};
  $menu_items = $('.menu-item-edit-lanugage.' + this.language);
  
  $menu_items.find('input[type=text]').each(function(i, el) {
    ids[$(el).attr('id')] = $(el).val();
    $(el).val('')
  });

  $menu_items.find('input[type=hidden]').each(function(i, el) {
    ids[$(el).attr('id')] = $(el).val();
    $(el).val('')
  });

  $menu_items.find('input[type=file]').each(function(i, el) {
    ids[$(el).attr('id')] = $(el).val();
    $(el).val('')
  });


  $menu_items.find('textarea').each(function(_,el) {
    ids[$(el).attr('id')] = $(el).val();
    $(el).val('');
  });  

  $menu_items.find('input[type=checkbox]').each(function(_,el) {
    ids[$(el).attr('id')] = $(el).attr('checked');
    $(el).attr('checked', false);
  });

  $menu_items.find('input[type=radio]').each(function(_,el) {
    ids[$(el).attr('id')] = el.checked;
    $(el).attr('checked', false);
  });

  image.src = $menu_items.find('img').attr('src');

  if(image.src) {
    $('.ndla-menu-image-preview').attr('src', image.src);
    $('.ndla-menu-image-preview').show();
    $('.ndla-menu-image-remove').show();
  }

  //debugger;

  $('#ndla-menu-item-form input[type=checkbox]').attr('checked', '');
    $('#ndla-menu-item-form input[type=radio]').attr('checked', '');
    $('#ndla-menu-item-form textarea').val('');

  for(var propt in ids) {
    new_id = propt.replace(/(edit-ndla-menu-item-)([^-]+)(.+)/, '$1'+language+'$3');
    value = ids[propt];
    if(typeof value === 'boolean') {
      $('#'+new_id).attr('checked', value);
    } else {
      $('#'+new_id).val(ids[propt]);
    }
  }
  this.language = language;
}

NdlaMenu.remove_item_data = function(item_id) {
  delete NdlaMenuData[item_id];
  for (id in NdlaMenuData) {
    if(NdlaMenuData[id].parent_id == item_id) {
      NdlaMenu.remove_item_data(id);
    }
  }
}

NdlaMenu.upload = function(element_id) {
  var form = document.getElementById('node-form');

  /* Create iframe */
  var iframe = document.createElement("iframe");
  iframe.setAttribute("id", "upload_iframe");
  iframe.setAttribute("name", "upload_iframe");
  iframe.setAttribute("width", "0");
  iframe.setAttribute("height", "0");
  iframe.setAttribute("border", "0");
  iframe.setAttribute("style", "width: 0; height: 0; border: none;");

  /* Add iframe to document */
  form.parentNode.appendChild(iframe);
  window.frames['upload_iframe'].name = "upload_iframe";

  iframeId = document.getElementById("upload_iframe");

  /* Add event */
  var eventHandler = function () {

    if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
    else iframeId.removeEventListener("load", eventHandler, false);

    /* Response from server */
    if (iframeId.contentDocument) {
      content = iframeId.contentDocument.body.innerHTML;
    } else if (iframeId.contentWindow) {
      content = iframeId.contentWindow.document.body.innerHTML;
    } else if (iframeId.document) {
      content = iframeId.document.body.innerHTML;
    }

    var response = JSON.parse(content);
          
    if(response.error == 0) {
      $('#edit-ndla-menu-item-' + response.language + '-image-id').val(response.id);
      $('#edit-ndla-menu-item-' + response.language + '-image-height').val(response.height);
      $('#edit-ndla-menu-item-' + response.language + '-image-width').val(response.width);
      $('#edit-ndla-menu-item-' + response.language + '-image-path').val(response.path);
      $('.ndla-menu-image-preview.' + response.language).show().attr('src', Drupal.settings.basePath + response.path);
      $('.ndla-menu-image-remove.' + response.language).show();
    } else {
      alert(response.error); 
    }

    /* Remove the iframe */
    setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
  }

  if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
  if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

  /* Get properties */
  var action = form.getAttribute("action");

  /* Set properties of form */
  form.setAttribute("target", "upload_iframe");
  form.setAttribute("action", Drupal.settings.basePath + 'ndla-menu/upload');

  /* Submit the form */
  form.submit();
  
  form.setAttribute("target", '');
  form.setAttribute("action", action);
}

NdlaMenu.remove_image = function(element) {
  var language = $(element).attr('class').split(' ')[1];
  $('#edit-ndla-menu-item-' + language + '-image-id').val('');
  $('#edit-ndla-menu-item-' + language + '-image-height').val('');
  $('#edit-ndla-menu-item-' + language + '-image-width').val('');
  $('#edit-ndla-menu-item-' + language + '-image-path').val('');
  $('.ndla-menu-image-preview.' + language).hide();
  $('.ndla-menu-image-remove.' + language).hide();
}

NdlaMenu.find_term_diff = function() {
  if(!NdlaMenuTermDiff) {
    return;
  }
  $('.term-diff').removeClass('term-diff');
  $('.term-diff-parent').removeClass('term-diff-parent');
  for(i in NdlaMenuData) {
    var diff = false;
    var terms = false;
    for(j in Drupal.settings.language_list) {
      if(NdlaMenuData[i][Drupal.settings.language_list[j]]['title'] != '') {
        if(terms == false) {
          terms = NdlaMenuData[i][Drupal.settings.language_list[j]]['terms'];
        } else {
          for(k in NdlaMenuData[i][Drupal.settings.language_list[j]]['terms']) {
            if(terms[k] != NdlaMenuData[i][Drupal.settings.language_list[j]]['terms'][k]) {
              diff = true;
            }
          }
        }
      }
    }
    if(diff) {
      NdlaMenu.set_term_diff(NdlaMenuData[i]['id'], NdlaMenuData[i]['parent_id']);
    }
  }
}

NdlaMenu.set_term_diff = function(id, parent_id) {
  $('.menu-item-' + id + ' > div > .name .title').addClass('term-diff');
  NdlaMenu.set_term_diff_parent(parent_id);
}

NdlaMenu.set_term_diff_parent = function(id) {
  $('.menu-item-' + id + ' > div > .name .title').addClass('term-diff-parent');
  if(NdlaMenuData[id]['parent_id'] != 0) {
    NdlaMenu.set_term_diff_parent(NdlaMenuData[id]['parent_id']);
  }
}