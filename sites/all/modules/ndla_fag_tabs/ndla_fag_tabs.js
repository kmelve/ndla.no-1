/**
 * @file
 * @ingroup ndla_fag_tabs
 */
 
var ndla_fag_tabs_map_type_term = "*";
var ndla_fag_tabs_map_result = 0;
var ndla_fag_tabs_map_result_offset = 0;

function ndla_fag_tabs_map_result_get() {
  var url = Drupal.settings.basePath;
  url += "service/ndla_fag_tabs_map/";
  url += ndla_fag_tabs_language;
  url += "/";
  url += Drupal.settings.ndla_utils_disp.context.fag;
  url += "/";
  url += ndla_fag_tabs_map_type_term;
  url += "/";
  url += ndla_fag_tabs_map_result_offset;
  $.getJSON(url, function(data) {
    ndla_fag_tabs_map_result = data;
    ndla_fag_tabs_map_result_render();
  });
}

function ndla_fag_tabs_map_result_prev() {
  if (ndla_fag_tabs_map_result_offset > 0) {
    ndla_fag_tabs_map_result_offset -= ndla_fag_tabs_map_result_limit;
    ndla_fag_tabs_map_result_get();
  }
}

function ndla_fag_tabs_map_result_next() {
  if (ndla_fag_tabs_map_result_offset + ndla_fag_tabs_map_result_limit < ndla_fag_tabs_map_result.count) {
    ndla_fag_tabs_map_result_offset += ndla_fag_tabs_map_result_limit;
    ndla_fag_tabs_map_result_get();
  }
}

function ndla_fag_tabs_map_click(type_term) {
  ndla_fag_tabs_map_type_term = type_term;
  ndla_fag_tabs_map_result_offset = 0;
  ndla_fag_tabs_map_result_get();
  return false;
}

function ndla_fag_tabs_map_term_count(tid) {
  for (var k = 0; k < ndla_fag_tabs_map_result.terms.length; k++) {
    if (ndla_fag_tabs_map_result.terms[k].tid == tid) {
      return ndla_fag_tabs_map_result.terms[k].count;
    }
  }
  return 0;
}

/**
 * Render HTML for node column
 */
function ndla_fag_tabs_map_result_render() {
  
  // render term types
  if (ndla_fag_tabs_map_result.terms != 0) {
    var htm = "<br/>";
    htm += "<div class=\"ndla_fag_tabs_map_item\"><a href=\"#\" onclick=\"ndla_fag_tabs_map_click('*')\">Alt innhold</a> (" + ndla_fag_tabs_map_result.count + ")</div>";
    for (var k = 0; k < ndla_fag_tabs_type_terms.length; k++) {
      htm += "<div class=\"ndla_fag_tabs_map_item\">";
      htm += "<a href=\"#\" onclick=\"ndla_fag_tabs_map_click('" + ndla_fag_tabs_type_terms[k].tid + "')\">";
      htm += ndla_fag_tabs_type_terms[k].name;
      htm += "</a> (" + ndla_fag_tabs_map_term_count(ndla_fag_tabs_type_terms[k].tid) + ")";
      htm += "</div>";
    }
    $("#ndla_fag_tabs_map_type_terms_inner_div").html(htm);
  }

  // render node result pager
  var htm = "";
  htm += "Resultatet viser " + (ndla_fag_tabs_map_result_offset + 1) + " til " + (ndla_fag_tabs_map_result_offset+ndla_fag_tabs_map_result_limit) + " av " + ndla_fag_tabs_map_result.count;
  if (ndla_fag_tabs_map_result_offset > 0) {
    htm += " - <a href=\"#\" onclick=\"ndla_fag_tabs_map_result_prev()\">Forrige " + ndla_fag_tabs_map_result_limit + "</a>";
  }
  if (ndla_fag_tabs_map_result_offset + ndla_fag_tabs_map_result_limit < ndla_fag_tabs_map_result.count) {
    htm += " - <a href=\"#\" onclick=\"ndla_fag_tabs_map_result_next()\">Neste " + ndla_fag_tabs_map_result_limit + "</a>";
  }
  $("#ndla_fag_tabs_map_result_header_div").html(htm);

  // render node result
  var htm = "<br/>";
  for (var k = 0; k < ndla_fag_tabs_map_result.docs.length; k++) {
    var url = Drupal.settings.basePath + ndla_fag_tabs_language + "/node/" + ndla_fag_tabs_map_result.docs[k].nid;
    htm += "<div class=\"ndla_fag_tabs_map_item\">" + (k + ndla_fag_tabs_map_result_offset + 1) + " - ";
    htm += "<a href=\"" + url + "\" target=\"_blank\">" + ndla_fag_tabs_map_result.docs[k].title + "</a>";
    htm += "</div>";
  }


  $("#ndla_fag_tabs_map_result_inner_div").html(htm);
}

/**
 * Render HTML for fag browser
 */
function ndla_fag_tabs_map_render(target_div) {
  var htm = "";
  htm += "<div id=\"ndla_fag_tabs_map\">";

  htm += "<div id=\"ndla_fag_tabs_map_type_terms_div\">";
  htm += "<div id=\"ndla_fag_tabs_map_type_terms_header_div\">Innholdstyper:</div>";
  htm += "<div id=\"ndla_fag_tabs_map_type_terms_inner_div\"></div>";
  htm += "</div>";

  htm += "<div id=\"ndla_fag_tabs_map_result_div\">";
  htm += "<div id=\"ndla_fag_tabs_map_result_header_div\"></div>";
  htm += "<div id=\"ndla_fag_tabs_map_result_inner_div\"></div>";
  htm += "</div>";

  htm += "</div>";

  $(target_div).html(htm);
  
  ndla_fag_tabs_map_result_offset = 0;
  ndla_fag_tabs_map_type_term = "*";
  ndla_fag_tabs_map_result_get();
}
