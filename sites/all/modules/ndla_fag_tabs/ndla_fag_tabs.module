<?php
/** \addtogroup ndla_fag_tabs */

/**
 * @file
 * @ingroup ndla_fag_tabs
 */
 
define('NDLA_FAG_TABS_MAP_RESULT_LIMIT', 25);

/**
 * Implements hook_init()
 */
function ndla_fag_tabs_init() {
  drupal_add_js(drupal_get_path('module', 'ndla_fag_tabs') . '/ndla_fag_tabs.js');
  drupal_add_css(drupal_get_path('module', 'ndla_fag_tabs') . '/ndla_fag_tabs.css', 'module', 'all', FALSE);
}

/**
 * Implements hook_menu()
 */
function ndla_fag_tabs_menu() {
  $items = array();
  $items['service/ndla_fag_tabs_map/%/%/%/%'] = array(
    'page callback' => 'ndla_fag_tabs_map_search_callback',
    'page arguments' => array(2,3,4,5),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * @brief
 *   Implementation of hook_form_FORM_ID_alter()
 */
function ndla_fag_tabs_form_fag_node_form_alter(&$form, &$form_state) {
  $nid = $form['#node']->nid;

  if ($nid) {
    $query = "SELECT enabled_tabs, default_tab FROM {ndla_fag_tabs} WHERE nid=%d";
    $result = db_query($query, $nid);
    $data = db_fetch_object($result);
    $default_enabled = unserialize($data->enabled_tabs);
    $default_tab = $data->default_tab;
  }
  else {
    $default_enabled = array('menu' => 'menu', 'topics' => 'topics', 'grep' => 'grep');
    $default_tab = 'none';
  }

  $form['fag_node_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu Tabs'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fag_node_form']['tabs_enabled'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Enabled menu tabs',
    '#description' => t('Choose which menu tabs that should be enabled.'),
    '#name' => 'menutabs_enabled',
    '#options' => array('menu' => t("Menu"), 'topics' => t("Topics"), 'grep' => t("Curriculum"), 'apps' => t("Applications")),
    '#required' => FALSE,
    '#default_value' => $default_enabled,
  );

  $form['fag_node_form']['default_tab'] = array(
    '#type' => 'radios',
    '#title' => 'Default expanded tab',
    '#description' => t('Choose which a menu tab that should be expanded by default.'),
    '#name' => 'menutabs_default',
    '#options' => array('none' => t("None"), 'menu' => t("Menu"), 'topics' => t("Topics"), 'grep' => t("Curriculum"), 'apps' => t('Applications')),
    '#required' => FALSE,
    '#default_value' => $default_tab,
  );
  $form['fag_node_form']['topics_tab_name_en'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the TOPICS TAB in english'),
    '#description' => t('Here you can override the name of the topic tab, write the name in english.'),
    '#default_value' => isset($form['#node']->topics_tab_name_en) ? $form['#node']->topics_tab_name_en : 'Topics',
  );
  $form['fag_node_form']['topics_tab_name_nn'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the TOPICS TAB in norwegian nynorsk'),
    '#description' => t('Here you can override the name of the topic tab, write the name in norwegian nynorsk.'),
    '#default_value' => isset($form['#node']->topics_tab_name_nn) ? $form['#node']->topics_tab_name_nn : 'Tema',
  );
  $form['fag_node_form']['topics_tab_name_nb'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the TOPICS TAB in norwegian bokmål'),
    '#description' => t('Here you can override the name of the topic tab, write the name in norwegian bokmål.'),
    '#default_value' => isset($form['#node']->topics_tab_name_nb) ? $form['#node']->topics_tab_name_nb : 'Tema',
  );

  $form['fag_node_form']['topic_menu_in_tray'] = array(
    '#type' => 'checkbox',
    '#title' => t('Topic menu in tray'),
    '#description' => t('Indicate whether the topic menu should be loaded into the topic tray'),
    '#default_value' => isset($form['#node']->topic_menu_in_tray) ? $form['#node']->topic_menu_in_tray : 0,
  );
  
  $form['fag_node_form']['topic_menu_tab_name_en'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the TOPIC MENU TAB in english'),
    '#description' => t('Here you can override the name of the topic menu tab, write the name in english.'),
    '#default_value' => isset($form['#node']->topic_menu_tab_name_en) ? $form['#node']->topic_menu_tab_name_en : 'Sequence',
  );
  $form['fag_node_form']['topic_menu_tab_name_nn'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the TOPIC MENU TAB in norwegian nynorsk'),
    '#description' => t('Here you can override the name of the topic menu tab, write the name in norwegian nynorsk.'),
    '#default_value' => isset($form['#node']->topic_menu_tab_name_nn) ? $form['#node']->topic_menu_tab_name_nn : 'Sekvens',
  );
  $form['fag_node_form']['topic_menu_tab_name_nb'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the TOPIC MENU TAB in norwegian bokmål'),
    '#description' => t('Here you can override the name of the topic menu tab, write the name in norwegian bokmål.'),
    '#default_value' => isset($form['#node']->topic_menu_tab_name_nb) ? $form['#node']->topic_menu_tab_name_nb : 'Sekvens',
  );

  $form['fag_node_form']['menu_tab_name_en'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the MENU TAB in english'),
    '#description' => t('Here you can override the name of the menu tab, write the name in english.'),
    '#default_value' => isset($form['#node']->menu_tab_name_en) ? $form['#node']->menu_tab_name_en : 'Material',
  );
  $form['fag_node_form']['menu_tab_name_nn'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the MENU TAB in norwegian nynorsk'),
    '#description' => t('Here you can override the name of the menu tab, write the name in norwegian nynorsk.'),
    '#default_value' => isset($form['#node']->menu_tab_name_nn) ? $form['#node']->menu_tab_name_nn : 'Fagstoff',
  );
  $form['fag_node_form']['menu_tab_name_nb'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the MENU TAB in norwegian bokmål'),
    '#description' => t('Here you can override the name of the menu tab, write the name in norwegian bokmål.'),
    '#default_value' => isset($form['#node']->menu_tab_name_nb) ? $form['#node']->menu_tab_name_nb : 'Fagstoff',
  );
}

/**
 * @brief
 *   Implementation of hook_nodeapi()
 *
 * @param object $node
 *   The node the action is being performed on.
 * @param string $op
 *   What kind of action is being performed.
 * @param bool/array $a3
 *   - For "view", passes in the $teaser parameter from node_view().
 *   - For "validate", passes in the $form parameter from node_validate(). 
 * @param bool $a4
 *   For "view", passes in the $page parameter from node_view(). 
 * 
 * @see
 *   http://api.drupal.org/api/function/hook_nodeapi/6
 */
function ndla_fag_tabs_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  global $language;
  static $ndla_fag_tabs_js = FALSE;
  if (!$ndla_fag_tabs_js) {
    $ndla_fag_tabs_js = TRUE;
    // find all node types
    $type_terms = array();
    $dbh = db_query("SELECT tid, name FROM term_data WHERE vid = 100004 ORDER BY name ASC");
    while ($term = db_fetch_object($dbh)) {
      $type_terms[] = $term;
    }
    // write js
    global $language;
    $js = "var ndla_fag_tabs_type_terms = " . drupal_to_js($type_terms) . ";\n";
    $js .= "var ndla_fag_tabs_language = " . drupal_to_js($language->language) . ";\n";
    $js .= "var ndla_fag_tabs_map_result_limit = " . NDLA_FAG_TABS_MAP_RESULT_LIMIT . ";\n";
    drupal_add_js($js, 'inline');
  }
  switch ($op) {
    case 'validate':
      if (($node->default_tab != 'none') && ($node->tabs_enabled[$node->default_tab] == '0')) {
        form_set_error('default_tab', t('You can not set a hidden tab as default expanded'));
      }
      break;
    case 'insert':
      if (isset($node->tabs_enabled)) {
        $query = "INSERT INTO {ndla_fag_tabs} 
                  (nid, enabled_tabs, default_tab, 
                  topics_tab_name_en, topics_tab_name_nn, topics_tab_name_nb,
                  topic_menu_tab_name_en, topic_menu_tab_name_nn, topic_menu_tab_name_nb,
                  menu_tab_name_en, menu_tab_name_nn, menu_tab_name_nb,
                  topic_menu_in_tray)
                  VALUES (%d, '%s', '%s', 
                  '%s', '%s', '%s',
                  '%s', '%s', '%s',
                  '%s', '%s', '%s',
                  %d)";
        $result = db_query(
          $query,
          $node->nid, serialize($node->tabs_enabled), $node->default_tab,
          $node->topics_tab_name_en, $node->topics_tab_name_nn, $node->topics_tab_name_nb,
          $node->topic_menu_tab_name_en, $node->topic_menu_tab_name_nn, $node->topic_menu_tab_name_nb,
          $node->menu_tab_name_en, $node->menu_tab_name_nn, $node->menu_tab_name_nb,
          $node->topic_menu_in_tray
        );
      }
      break;
    case 'update':
      if (isset($node->tabs_enabled)) {
        $query = "UPDATE {ndla_fag_tabs} SET 
                  enabled_tabs='%s', default_tab='%s',
                  topics_tab_name_en='%s', topics_tab_name_nn='%s', topics_tab_name_nb='%s',
                  topic_menu_tab_name_en='%s', topic_menu_tab_name_nn='%s', topic_menu_tab_name_nb='%s',
                  menu_tab_name_en='%s', menu_tab_name_nn='%s', menu_tab_name_nb='%s',
                  topic_menu_in_tray=%d
                  WHERE nid=%d";
        $result = db_query(
          $query,
          serialize($node->tabs_enabled), $node->default_tab,
          $node->topics_tab_name_en, $node->topics_tab_name_nn, $node->topics_tab_name_nb,
          $node->topic_menu_tab_name_en, $node->topic_menu_tab_name_nn, $node->topic_menu_tab_name_nb,
          $node->menu_tab_name_en, $node->menu_tab_name_nn, $node->menu_tab_name_nb,
          $node->topic_menu_in_tray,
          $node->nid
        );
      }
      break;
    case 'load':
      if ($node->type == 'fag') {
        $query = "SELECT enabled_tabs, default_tab, 
                  topics_tab_name_en, topics_tab_name_nn, topics_tab_name_nb,
                  topic_menu_tab_name_en, topic_menu_tab_name_nn, topic_menu_tab_name_nb,
                  menu_tab_name_en, menu_tab_name_nn, menu_tab_name_nb,
                  topic_menu_in_tray
                  FROM {ndla_fag_tabs} WHERE nid=%d";
        $result = db_query($query, $node->nid);
        $data = db_fetch_object($result);
        $node->tabs_enabled = unserialize($data->enabled_tabs);
        $node->default_tab = $data->default_tab;
        $node->topics_tab_name_en = $data->topics_tab_name_en;
        $node->topics_tab_name_nn = $data->topics_tab_name_nn;
        $node->topics_tab_name_nb = $data->topics_tab_name_nb;
        $node->topic_menu_tab_name_en = $data->topic_menu_tab_name_en;
        $node->topic_menu_tab_name_nn = $data->topic_menu_tab_name_nn;
        $node->topic_menu_tab_name_nb = $data->topic_menu_tab_name_nb;
        $node->menu_tab_name_en = $data->menu_tab_name_en;
        $node->menu_tab_name_nn = $data->menu_tab_name_nn;
        $node->menu_tab_name_nb = $data->menu_tab_name_nb;
        $node->topic_menu_in_tray = $data->topic_menu_in_tray;
      }
  }
}

/**
 * @brief
 *   Implementation of hook_preprocess_page()
 */
function ndla_fag_tabs_preprocess_page(&$variables) {
  global $language;
  $fag = ndla_utils_disp_get_course();
  $args = arg();

  if(empty($fag) && $args[0] == 'node' && is_numeric($args[1])) {
    $g_nid = db_fetch_object(db_query("SELECT group_nid FROM {og_ancestry} WHERE nid = %d LIMIT 1", $args[1]))->group_nid;
    if(!empty($g_nid)) {
      $fag = node_load($g_nid);
    }
  }
  
  if (isset($fag)) {
    // Retrive course name from taxonomy
    $fag_term_id = ndla_fag_taxonomy_get_term_from_og($fag->nid);

    // Make sure name is localized
    // Note! i18ntaxonomy_localize_terms needs term inside an array to work
    $fag_term = i18ntaxonomy_localize_terms(array(taxonomy_get_term($fag_term_id)));
    $tabs = array();
    if (isset($fag->tabs_enabled)) {
      // Make sure we get the tabs in the right order...
      foreach ($fag->tabs_enabled as $key => $tab) {
        $tabs[$key] = !empty($tab) ? 1 : 0;
      }
    }
    $settings = array(
      'ndla_fag_tabs' => array(
        'fagNid' => $fag->nid,
        'fagTitle' => !empty($fag_term[0]->name) ? $fag_term[0]->name : NULL,
        'topicsTabName' => isset($fag->{"topics_tab_name_$language->prefix"}) ? $fag->{"topics_tab_name_$language->prefix"} : t('Topics'),
        'topicMenuTabName' => isset($fag->{"topic_menu_tab_name_$language->prefix"}) ? $fag->{"topic_menu_tab_name_$language->prefix"} : t('Sequence'),
        'tabs' => $tabs,
      )
    );
    drupal_add_js($settings, 'setting');
  }

  if (function_exists("ndla_utils_disp_get_course")) {
    // Fetch current course, if any
    $context = ndla_utils_disp_get_course();
  }

  // Set enabled tabs  
  if (!empty($context->nid)) {
    $query = "SELECT enabled_tabs FROM {ndla_fag_tabs} WHERE nid=%d";
    $result = db_query($query, $context->nid);
    $data = db_fetch_object($result);
    if(!empty($data->enabled_tabs)) {
      $variables ['tab_enabled'] = unserialize($data->enabled_tabs);
    }
  }
}

/**
 * Return node results for fag map
 */
function ndla_fag_tabs_map_search_callback($language, $og_groups, $type_terms, $offset) {
  try {
    $query_str = "";
    $query_filter = "";

    if ($language != '') {
      $query_filter = "(language:" . $language . ")";
    }
    
    // limit to organic group
    $og_groups_arr = array();
    if ($og_groups != '*') {
      foreach (explode(',', $og_groups) as $gid) {
        if (is_numeric($gid)) {
          $og_groups_arr[] = "im_og_gid:" . $gid;
        }
      }
      if (count($og_groups_arr) > 0) {
        if ($query_filter != '') {
          $query_filter .= ' AND ';
        }
        $query_filter .= '(' . implode(' OR ', $og_groups_arr) . ')';
      }
    }

    // limit to vocabulary type
    $type_terms_arr = array();
    if ($type_terms != '*') {
      foreach (explode(',', $type_terms) as $tid) {
        if (is_numeric($tid)) {
          $type_terms_arr[] = "im_vid_100004:" . $tid;
        }
      }
      if (count($type_terms_arr) > 0) {
        if ($query_str != '') {
          $query_filter .= ' AND ';
        }
        $query_filter .= '(' . implode(' OR ', $type_terms_arr) . ')';
      }
    }

    // do search
    $query_str .= $query_filter;
    error_log("query_str = " . $query_str);
    $params = array(
      'fl' => 'title,nid',
      'qt' => 'standard',
      'sort' => 'sort_title asc',
      'facet' => 'true',
      'facet.sort' => 'true',
      'facet.field' => 'im_vid_100004',
      'start' => $offset,
      'rows' => NDLA_FAG_TABS_MAP_RESULT_LIMIT,
    );
    $solr = apachesolr_get_solr();
    $response = $solr->search(htmlspecialchars($query_str, ENT_NOQUOTES, 'UTF-8'), $params['start'], $params['rows'], $params);
    /*
    error_log(print_r($response->response, TRUE));
    error_log(print_r($response->facet_counts, TRUE));
    */
    $result = new stdClass();
    $result->count = $response->response->numFound;
    $result->docs = $response->response->docs;
    if (count($type_terms_arr) == 0) {
      $result->terms = array();
      foreach ($response->facet_counts->facet_fields->im_vid_100004 as $tid => $count) {
        $term = new stdClass();
        $term->tid = $tid;
        $term->count = $count;
        $result->terms[] = $term;
      }
    } else {
      $result->terms = 0;
    }
    print drupal_to_js($result);
  }
  catch ( Exception $e ) {
    $host = variable_get('apachesolr_host', 'localhost');
    $port = variable_get('apachesolr_port', '8983');
    $path = variable_get('apachesolr_path', '/solr');
    print '<error>search service: http://' . $host . ':' . $port . '/' . $path . ' unavailable!</error>';
    print '<exception>search service unavailable!</exception>';
  }
  exit();
}