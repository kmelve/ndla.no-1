<?php
/**
 * @file
 * @ingroup ndla_authors
 */
 
/**
 * Theme file for theming a list of links to authors in a block.
 */
 
 $authors = ndla_authors_get_authors($node, TRUE);

 $utdanning_rdf = (module_exists('utdanning_rdf')) ? TRUE : FALSE;
 if(!empty($node->additional_license_informaiton['checkbox']) && $node->additional_license_informaiton['checkbox'] == 1) {
    print "<h3>" . $node->additional_license_informaiton['text'] . "</h3>"; 
 }
 foreach($authors as $author) {
   print "<h3>" . $author['term_name'] . "</h3>";

   print "<ul>";
   foreach($author['authors'] as $author_node) {
     print "<li>";
     if($utdanning_rdf) {
       print l($author_node['title'], "node/".$author_node['nid']."/lightbox", array('attributes' => array('rel' => 'lightmodal')));
     }
     else {
       print l($author_node['title'], "node/".$author_node['nid'], array('attributes' => array('rel' => 'lightmodal')));
     }
     print "</li>";
   }
   print "</ul>";
 }
 