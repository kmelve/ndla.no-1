<?php
/**
 * @file
 * @brief
 *  Install file
 * @ingroup ndla_authors
 */
 
function ndla_authors_schema() {
  $schema['ndla_authors'] = array(
    'description' => t('Table for storing content types which should show the copyright under the title'),
    'fields' => array(
      'nid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'vid' => array(
        'description' => t('The version ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
	    'tid' => array(
        'description' => t('Column for storing the term id'),
        'type' => 'int',
        'not null' => FALSE,
      ),
      'person_nid' => array(
        'description' => t('Column for storing the related node id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'nid_and_vid' => array('nid', 'vid'),
    ),
  );

  $schema['ndla_authors_show_author'] = array(
    'description' => t('Table for storing content types which should show the copyright under the title'),
    'fields' => array(
      'nid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'vid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
	  'content_types' => array(
        'description' => t('Column for storing the content types as serialized data'),
        'type' => 'text',
        'default' => '',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('vid'),
  );
  
  $schema['ndla_authors_show_author_per_node'] = array(
    'description' => t('Table for storing data wether or not to display author info under a node'),
    'fields' => array(
      'nid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'vid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
	  'show' => array(
        'description' => t('Column for storing the value'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('vid'),
  );
  return $schema;
}

function ndla_authors_update_6101() {
  $schema['ndla_authors_show_author'] = array(
    'description' => t('Table for storing content types which should show the copyright under the title'),
    'fields' => array(
      'nid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'vid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
	  'content_types' => array(
        'description' => t('Column for storing the content types as serialized data'),
        'type' => 'text',
        'default' => '',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('vid'),
  );
  
  $schema['ndla_authors_show_author_per_node'] = array(
    'description' => t('Table for storing data wether or not to display author info under a node'),
    'fields' => array(
      'nid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'vid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
	  'show' => array(
        'description' => t('Column for storing the value'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('vid'),
  );
  
  $ret = array();
  db_create_table($ret, 'ndla_authors_show_author', $schema['ndla_authors_show_author']);
  db_create_table($ret, 'ndla_authors_show_author_per_node', $schema['ndla_authors_show_author_per_node']);
  
  return $ret;
}

function ndla_authors_update_6102() {
  $ret = array();
  db_add_index($ret, 'ndla_authors', 'person_nid', array('person_nid'));
  return $ret;
}

function ndla_authors_update_6103() {
  $ret = array();
  db_add_index($ret, 'ndla_authors', 'vid', array('vid'));
  return $ret;
}

function ndla_authors_install() {
  $result = drupal_install_schema('ndla_authors');

  if (count($result) > 0) {
    drupal_set_message(t('Module ndla_authors installed successfully'));
  } else {
    drupal_set_message(t('Installation failed: ndla_authors'));
  }
}

function ndla_authors_uninstall() {
	drupal_uninstall_schema('ndla_authors');
}
