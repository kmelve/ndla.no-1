<?php

/**
 * @file
 *  amendor_electure.module php file
 *  Drupal module amendor_electure.
 */

/**
 * Implementation of hook_info().
 */
function amendor_electure_node_info() {
  return array(
    'amendor_electure' => array(
      'name' => t('eLecture'),
      'module' => 'amendor_electure',
      'description' => t('Makes it possible to display electures in Drupal.'),
    )
  );
}

/**
 * Implementation of hook_perm().
 */
function amendor_electure_perm() {
  return array('create amendor_electure', 'access amendor_electure');
}

/**
 * Implementation of hook_access().
 */
function amendor_electure_access($op, $node, $account) {
  if (!user_access('access amendor_electure', $account)) {
    return FALSE;
  }
  if ($op == 'create' || $op == 'update' || $op == 'delete') {
    return user_access('create amendor_electure', $account);
  }
}

/**
 * Implementation of hook_menu
 *
 * @return array of menu items
 */
function amendor_electure_menu() {
  return array(
    'amendor-electure/iframe/%' => array(
      'title' => 'Amendor eLecture iFrame',
      'page callback' => 'amendor_electure_page',
      'page arguments' => array(FALSE, NULL, 2),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    ),
    'amendor-electure/fullscreen' => array(
      'title' => 'Amendor eLecture Fullscreen',
      'page callback' => 'amendor_electure_page',
      'page arguments' => array(TRUE),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    ),
    // Deprecated
    'eForelesningFullskjerm' => array(
      'page callback' => 'amendor_electure_redirect_deprecated',
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    )
  );
}

/**
 * Implementation of hook_view().
 */
function amendor_electure_view($node, $teaser, $page) {
  if ($teaser || !$page) {
    $node->content['flash'] = array(
      '#value' => $node->subtitles
    );
  }
  else {
    $node->content['flash'] = array(
      '#value' => theme('amendor_electure', $node->nid),
    );
  }
  return $node;
}

/**
 * Implementation of hook_form().
 */
function amendor_electure_form(&$node) {
  unset($_SESSION['new_electure_swf']);

  //Get array with all possible frameworks:
  $frameworks = _amendor_electure_existing_frameworks();
  $frameworks[0] = t('Velg rammeverk');

  //if we are updating an existing node:
  if (is_numeric($node->nid)) {
    $result = db_query("SELECT sid FROM {amendor_electure_sequence} WHERE nid = %d", $node->nid);
    $topicNodeId = db_fetch_object($result)->sid;
    //Dersom dette er en oversatt node må rammeverk settes til null, eller til det oversatte rammeverket
    //dersom det finnes:
    if ($node->tnid != 0 && $node->nid != $node->tnid) {
      $result = db_query("SELECT sid FROM {amendor_electure_sequence} WHERE nid = %d", $node->tnid);
      $originalFrameworkId = db_fetch_object($result)->sid;
      if ($topicNodeId == $originalFrameworkId) {
        $frameWork_tnid = db_fetch_object(db_query("SELECT tnid FROM {node} WHERE nid = %d", $originalFrameworkId))->tnid;
        $result = db_query("SELECT nid FROM {node} WHERE tnid = %d AND language = '%s'", $frameWork_tnid, $node->language);
        $numrows = db_result(db_query("SELECT COUNT(*) FROM {node} WHERE tnid = %d AND language = '%s'", $frameWork_tnid, $node->language));
        if ($numrows > 0) {
          $topicNodeId = db_fetch_object($result)->nid;
        }
        else {
          $topicNodeId = $frameworks[0];
        }
      }
    }
    $form['framework'] = array(
      '#type' => 'hidden',
      '#value' => $topicNodeId
    );
  }
  else {
    $topicNodeId = $frameworks[0];
  }

  $form['#attributes'] = array('enctype' => "multipart/form-data");

  //Adding a javascript to communicate with flash objects in the form.
  //The flash objects consecutively updates hidden fields in the form.
  $form['helper']['#theme'] = 'amendor_electure_form_js';

  $form['existing_framework'] = array(
    '#type' => 'select',
    '#title' => t('Velg rammeverk'),
    '#required' => TRUE,
    '#default_value' => $topicNodeId,
    '#options' => $frameworks,
    '#description' => t('Velg hvilket rammeverk eForelesningen skal høre til.'),
    '#attributes' => array('onChange' => 'updateSelectedFramework(this.options[this.options.selectedIndex].value)'),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Tittel'),
    '#required' => TRUE,
    '#description' => t('Skriv inn tittelen til eForelesningen.'),
    '#default_value' => $node->title,
    '#id' => 'titleTxt',
    '#attributes' => array('onkeyup' => 'updateTitle(document.getElementById("titleTxt").value)',
      'onmouseout' => 'updateTitle(document.getElementById("titleTxt").value)',),
  );

  //path_dir is needed because of the use of clean urls.
  $start_path = base_path();
  $path_dir = $start_path . drupal_get_path('module', 'amendor_electure');

  $flashvars = 'flashComPage=' . base_path() . 'index.php?q=amendor_electure_flash'
    . '&amp;nid=' . ((is_numeric($node->nid)) ? $topicNodeId : $node->existing_framework)
    . '&amp;flashComPage=' . base_path() . 'index.php?q=amendor_electure_flash'
    . '&amp;sNid=' . $node->nid
    . '&amp;mode=1';

  $embedMenu = '<object height="400" width="300" id="flashcontent" type="application/x-shockwave-flash" data="' . $path_dir . '/flash/index.swf">
  <param name="movie" value="' . $path_dir . '/flash/index.swf" />
  <param name="wmode" value="transparent" />
  <param name="menu" value="false" />
  <param name="quality" value="high" />
  <param name="allowFullScreen" value="true" />
  <param name="scale" value="exactfit" />
  <param name="FlashVars" value="' . $flashvars . '" />
  <embed height="400" width="300" flashvars="' . $flashvars . '" src="' . $path_dir . '/flash/index.swf"></embed>
  </object>';

  $form['topic_menu'] = array(
    '#value' => $embedMenu
  );

  //the flash menu will update this hidden field:
  $form['xmlContent'] = array(
    '#type' => 'hidden',
    '#title' => t('XML'),
    '#id' => 'xmlText',
  );

  $form['imported_file'] = array(
    '#type' => 'file',
    '#title' => t('Last opp eForelesning(Amendor)'),
    '#description' => t('Trykk på "Browse..." for å velge eForelesning som skal lastes opp.'),
  );
  $form['subtitle_file'] = array(
    '#type' => 'file',
    '#title' => t('Last opp undertekster'),
    '#description' => t('Trykk på "Browse..." for å velge xml fil med undertekster som skal lastes opp.'),
  );

  return $form;
}

/**
 * Implementation of hook_insert().
 */
function amendor_electure_insert($node) {
  //if we ar uploading a swf.
  if (isset($_SESSION['new_electure_swf'])) {
    $swf = $_SESSION['new_electure_swf'];
    unset($_SESSION['new_electure_swf']);
    file_set_status($swf, 1);
  }
  else {
    $swf = $node->swf;
  }
  db_query("INSERT INTO {amendor_electure_sequence} (nid, fid, sid) VALUES (%d, %d, %d)", $node->nid, $swf->fid, $node->existing_framework);

  //receiving the new menu(xml)
  if ($newXML = amendor_electure_makeDOM($node, false)) {
    //updates db
    db_query("UPDATE {amendor_electure_topic} SET xmlContent = '%s' WHERE nid = %d", $newXML, $node->existing_framework);
  }

  $subString = '';
  $validators = array(
    'amendor_electure_file_validate_ext' => array('xml')
  );
  $file2 = file_save_upload('subtitle_file', $validators);
  if (!is_int($file2) || $file2 != 0) {
    file_set_status($file2, 1);
    $subString = file_get_contents($file2->filepath);
  }
  elseif (isset($node->subtitles)) {
    $subString = $node->subtitles;
  }
  db_query("UPDATE {amendor_electure_sequence} SET subtitles = '%s' WHERE nid = %d", $subString, $node->nid);
}

/**
 * Implementation of hook_update().
 */
function amendor_electure_update($node) {
  //if framework has been changed:
  if ($node->framework != $node->existing_framework && $node->framework != 0 && is_numeric($node->framework)) {
    amendor_electure_framework_remove_el($node->framework, $node->nid);
  }

  $node->new_swf = $_SESSION['new_electure_swf'];
  unset($_SESSION['new_electure_swf']);
  //If new swf has been uploaded:
  if ($node->new_swf) {
    file_set_status($node->new_swf, 1);

    // We delete the old swf:
    $old_fid = db_result(db_query("SELECT fid FROM {amendor_electure_sequence} WHERE nid=%d", $node->nid));
    $old_path = db_result(db_query("SELECT filepath FROM {files} WHERE fid = %d", $old_fid));
    $path = $node->new_swf->filepath;
    //if we have a new filepath:
    if ($old_path != $path) {
      // Delete the file and information about the file and electure from the db and filesystem
      file_delete(file_create_path($old_path));
      db_query("DELETE FROM {files} WHERE fid = %d", $old_fid);
      db_query("UPDATE {amendor_electure_sequence} SET fid = %d WHERE nid = %d", $node->new_swf->fid, $node->nid);
    }
  }
  //drupal function to expire data from cache
  cache_clear_all('*', 'cache_filter', true);
  //updating the menu:
  if ($theXML = amendor_electure_makeDOM($node, false)) {
    db_query("UPDATE {amendor_electure_topic} SET xmlContent = '%s' WHERE nid = %d", $theXML, $node->existing_framework);
    db_query("UPDATE {amendor_electure_sequence} SET sid = %d WHERE nid = %d", $node->existing_framework, $node->nid);
    db_query("UPDATE {amendor_electure_questions} SET nid = %d WHERE sid=%d", $node->existing_framework, $node->nid);
  }
  //updating subtitles:
  $validators = array(
    'amendor_electure_file_validate_ext' => array('xml')
  );
  $file2 = file_save_upload('subtitle_file', $validators);
  if (!is_int($file2) || $file2 != 0) {
    file_set_status($file2, 1);
    $subString = file_get_contents($file2->filepath);
    db_query("UPDATE {amendor_electure_sequence} SET subtitles = '%s' WHERE nid = %d", $subString, $node->nid);
  }
}

/**
 * Implementation of hook_delete
 *
 * @param node $node - node to be deleted
 */
function amendor_electure_delete($node) {
  $fid = db_fetch_object(db_query("SELECT fid FROM {amendor_electure_sequence} WHERE nid=%d", $node->nid));
  file_delete(file_create_path(db_result(db_query("SELECT filepath FROM {files} WHERE fid=%d", $fid->fid))));
  $framework = db_fetch_object(db_query("SELECT sid FROM {amendor_electure_sequence} WHERE nid=%d", $node->nid))->sid;
  $result = db_query("SELECT xmlContent from {amendor_electure_topic} WHERE nid = %d", $framework);
  $numrows = db_result(db_query("SELECT COUNT(*) FROM {amendor_electure_topic} WHERE nid = %d", $framework));
  //If the eLecture didn't belong to a framework:
  if ($numrows < 1) {
    db_query("DELETE FROM {files} WHERE fid=%d", $fid->fid);
    db_query("DELETE FROM {amendor_electure_sequence} WHERE nid=%d", $node->nid);
  }
  //The electure did belong to a framework:
  else {
    $dbXML = db_fetch_object($result)->xmlContent;
    $theXML = amendor_electure_updateDOM($node, $dbXML, true);
    ;
    db_query("UPDATE {amendor_electure_topic} SET xmlContent = '%s' WHERE nid = %d", $theXML, $framework);
    db_query("DELETE FROM {files} WHERE fid=%d", $fid->fid);
    db_query("DELETE FROM {amendor_electure_sequence} WHERE nid=%d", $node->nid);
  }
  //delete all the relevant questions:
  db_query("DELETE FROM {amendor_electure_questions} WHERE sid=%d", $node->nid);
}

/**
 * Implementation of hook_load().
 */
function amendor_electure_load($node) {
  $additions = db_fetch_object(db_query("SELECT fid AS swf, sid AS framework, subtitles FROM {amendor_electure_sequence} WHERE nid = %d", $node->nid));
  $additions->swf = db_fetch_object(db_query("SELECT * FROM {files} WHERE fid = %d", $additions->swf));
  $additions->xmlContent = db_result(db_query("SELECT xmlContent FROM {amendor_electure_topic} WHERE nid = %d", $additions->framework));
  return $additions;
}

/**
 * Implementation of hook_validate().
 */
function amendor_electure_validate(&$node) {
  $doc = new DOMDocument();
  set_error_handler('_amendor_electure_xml_error_handler');
  //Validates xml:
  try {
    $doc->loadXML($node->xmlContent);
    $topics = $doc->getElementsByTagName('topic');
    if ($topics->length < 1) {
      form_set_error('', t('The eLecture menu you are trying to save is empty. This is probably due to a missing flash file. Contact your site administrator if this problem continues.'));
    }
  }
  catch (Exception $e) {
    form_set_error('', t('Det oppstod en feil. Vennligst pr�v igjen. Kontakt Amendor om problemet fortsetter. Feilkode er 2819.'));
  }
  restore_error_handler();

  // if the directory does not exist, create it
  $electure_directory = file_create_path(file_directory_path() . '/electure');
  file_check_directory($electure_directory, FILE_CREATE_DIRECTORY, 'imported_file');

  $validators = array(
    'amendor_electure_file_validate_ext' => array('swf')
  );

  if ($node->new_swf = file_save_upload('imported_file', $validators, $electure_directory)) {
    $_SESSION['new_electure_swf'] = $node->new_swf;
  }
  else {
    $numrows = db_result(db_query("SELECT COUNT(*) FROM {amendor_electure_sequence} WHERE nid = %d", $node->nid));
    if ($numrows < 1) {
      form_set_error('no_file', t('eForelesningen må ha en flash(.swf) fil å vise'));
    }
  }

  $validators = array(
    'amendor_electure_file_validate_ext' => array('xml')
  );
  $file = file_save_upload('subtitle_file', $validators);
  if (!is_int($file) || $file != 0) {
    // Checks the file extension of the file:
    $splitted = split("\.", $file->filename);
    if (count($splitted) < 2 || !eregi('xml', $splitted[count($splitted) - 1])) {
      form_set_error('subtitle_file', t('Filen du vil laste opp må være en xml-fil'));
    }
    else {
      try {
        $_SESSION['uploaded_subtitles'] = _amendor_electure_extract_subtitles(file_get_contents($file->filepath));
      }
      catch (Exception $e) {
        form_set_error('subtitle_file', t('Innholdet i xml-filen var korrupt'));
      }
    }
  }
}

function amendor_electure_file_validate_ext($file, $exts) {
  $errors = array();
  $regex = '/\.(' . ereg_replace(' +', '|', preg_quote($exts)) . ')$/i';
  if (!preg_match($regex, $file->filename)) {
    $errors[] = t('Bare filer med følgende etternavn er lovlige: %files-allowed.', array('%files-allowed' => $exts));
  }
  return $errors;
}

/**
 * Implementation of hook_help().
 *
 * @param section
 */
function amendor_electure_help($path, $arg) {
  switch ($path) {
    case 'admin/help#amendor_electure':
      return t('Denne modulen er laget av Amendor AS. Kontakt Amendor for support.');
      break;
  }
}

/**
 * Implementation of hook_alter().
 *
 * @param form_id
 * @param &$form
 */
function amendor_electure_form_alter(&$form, &$form_state, $form_id) {
  if (($form_id == "amendor_electure_node_form")) {
    //remove the preview button:
    unset($form['buttons']['preview']);
  }
}

/**
 * Implementation of hook_theme().
 */
function amendor_electure_theme($existing, $type, $theme, $path) {
  return array(
    'amendor_electure' => array(
      'arguments' => array(
        'nid' => NULL,
        'width' => 720,
        'fullscreen' => FALSE,
        'dynamic_size' => FALSE,
        'framework_nid' => NULL,
        'start_frame' => '1',
        'stop_frame' => '',
        'language' => 'bm'
      ),
      'path' => $path . '/templates',
      'template' => 'amendor-electure'
    ),
    'amendor_electure_page' => array(
      'arguments' => array(
        'electure' => NULL,
        'nid' => NULL,
        'fullscreen' => FALSE
      ),
      'path' => $path . '/templates',
      'template' => 'amendor-electure-page',
    ),
    'amendor_electure_external_embed' => array(
      'arguments' => array('node' => NULL),
    ),
    'amendor_electure_form_js' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * implementation of the method _amendor_electure_existing_frameworks
 *
 * @return the existing frameworks nodeid and title
 */
function _amendor_electure_existing_frameworks() {
  $res = db_query("SELECT nid, title FROM {amendor_electure_topic} ORDER BY title");
  $rows = db_result(db_query("SELECT COUNT(*) FROM {amendor_electure_topic}"));
  if ($rows > 0) {
    $frameworks[0] = t('Velg rammeverk');
    for ($i = 0; $i < $rows; $i++) {
      $res_o = db_fetch_object($res);
      $nid = $res_o->nid;
      $title = $res_o->title;
      $frameworks[$nid] = $title;
    }
    return $frameworks;
  }
  else {
    form_set_error('no exisiting frameworks', t('Det finnes ingen eForelesning rammeverk.'));
  }
}

/**
 * Internal error handler used in extracting xml. (Purpose is to ignore warning messages.)
 */
function _amendor_electure_xml_error_handler($errno, $errstr, $errfile, $errline) {
  if ($errno == E_WARNING && (substr_count($errstr, 'DOMDocument::loadXML()') > 0)) {
    throw new DOMException($errstr);
  }
  else {
    return false;
  }
}

/**
 * Check the xml(framework menu) and if any eLecture are removed,
 * the relation to the framework is then set to be 0.
 *
 * @param $node
 * @param isFramework is true if the function is called from framework
 *
 * @return xml for the eLecture menu in the framework
 */
function amendor_electure_makeDOM($node, $isFramework) {
  //If the node is a framework og has a framework.
  if (($node->existing_framework != 0) || ($isFramework)) {
    if ($isFramework) {
      $result = db_query("SELECT xmlContent from {amendor_electure_topic} WHERE nid = %d", $node->nid);
    }
    else {
      $result = db_query("SELECT xmlContent from {amendor_electure_topic} WHERE nid = %d", $node->existing_framework);
    }
    $dbXML = db_fetch_object($result)->xmlContent;
    /*
     * Verifying xml:
     */
    set_error_handler('_amendor_electure_xml_error_handler');
    $oldDoc = new DOMDocument();
    try {
      $oldDoc->loadXML($dbXML);
    }
    catch (Exception $e) {
      form_set_error('', 'Ugyldig xml');
      watchdog('eLecture', 'Ugyldig xml i amendor_electure_makeDOM 1');
      restore_error_handler();
      return false;
    }
    $eLecturCounter = 0;
    //Traversing xml:
    foreach ($oldDoc->getElementsByTagName('eLecture') as $electure) {
      $eNid = $electure->getAttribute('nid');
      $oldELectureNid[$eLecturCounter] = $eNid;
      $eLecturCounter++;
    }
    $doc = new DOMDocument();
    try {
      $doc->loadXML($node->xmlContent);
    }
    catch (Exception $e) {
      form_set_error('', 'Ugyldig xml');
      watchdog('eLecture', 'Ugyldig xml i amendor_electure_makeDOM 2');
      restore_error_handler();
      return false;
    }
    restore_error_handler();

    $newELectureNid = array();
    foreach ($doc->getElementsByTagName('eLecture') as $electure) {
      $eNid = $electure->getAttribute('nid');
      //placing the nid in an array
      $newELectureNid[$eLecturCounter] = $eNid;
      $eLecturCounter++;
    }

    //need a check if some sequences and topics are deleted, if so the sequence is set without relation to a framework
    for ($i = 0; $i < count($oldELectureNid); $i++) {
      if (!in_array($oldELectureNid[$i], $newELectureNid)) {
        db_query("UPDATE {amendor_electure_sequence} SET sid = %d WHERE nid = %d and sid=%d", 0, $oldELectureNid[$i], $node->nid);
        db_query("UPDATE {amendor_electure_questions} SET nid = %d WHERE sid=%d", 0, $oldELectureNid[$i]);
      }
    }
    return amendor_electure_updateDOM($node, $node->xmlContent, false);
  }
  else {
    return $node->xmlContent;
  }
}

/*
 * This function takes care of:
 *
 * -Validating the xml
 * -updating node titles of other nodes if they have been changed through the menu system
 * -If an eLecture is deleted this function removes it from the menu
 * -If we are creating a new eLecture node this function updates the menu with the correct nid
 * -Adds the nid to the original node for nynorsk translations.
 *
 * @param $node
 * @param theXML
 * @param deletingELecture is true if the node is being deleted
 * @return the new xml
 */

function amendor_electure_updateDOM($node, $theXML, $deletingELecture) {
  set_error_handler('_amendor_electure_xml_error_handler');
  $doc = new DOMDocument();
  $newDoc = new DOMDocument('1.0', 'utf-8');
  //validating:
  try {
    $doc->loadXML($theXML);
  }
  catch (Exception $e) {
    form_set_error('', 'Ugyldig xml');
    watchdog('eLecture', 'Ugyldig xml i amendor_electure_updateDOM');
    restore_error_handler();
    return false;
  }
  $eLecturCounter = 0;

  foreach ($doc->getElementsByTagName('menu') as $test) {
    $newMenu = $newDoc->createElement('menu');
    $newDoc->appendChild($newMenu);
    foreach ($test->getElementsByTagName('topic') as $topic) {
      $tName = $topic->getAttribute('name');
      $newTopic = $newDoc->createElement('topic');
      $newTopic->setAttributeNode(new DOMAttr('name', $tName));
      $newMenu->appendChild($newTopic);
      foreach ($topic->getElementsByTagName('eLecture') as $electure) {
        $eName = $electure->getAttribute('name');
        $eNid = $electure->getAttribute('nid');
        //update node title if name has been changed in the menu:
        $result = db_query("SELECT title from {node} WHERE nid = %d", $eNid);
        $rows = db_result(db_query("SELECT COUNT(*) FROM {node} WHERE nid = %d", $eNid));
        if ($rows > 0) {
          $oldName = db_fetch_object($result)->title;
        }

        if (($rows > 0) && ($eName != $oldName)) {
          //loading node, changing tilte and saving it(with new title)
          $tempNode = node_load(array('nid' => $eNid));
          $tempNode->title = $eName;
          node_save($tempNode);
        }
        $ePath = $electure->getAttribute('path');
        $eStatus = $electure->getAttribute('status');
        //If the current eLecture in the xml is to be deleted:
        if (($eNid == $node->nid) && $deletingELecture) {
          
        }
        //If the current eLecture in the xml isn't to be deleted:
        else {
          $newELecture = $newDoc->createElement('eLecture');
          $newELecture->setAttributeNode(new DOMAttr('name', $eName));
          //If we are creating a new node:
          if ($eNid == "undefined") {
            $newELecture->setAttributeNode(new DOMAttr('nid', $node->nid));
            $newELecture->setAttributeNode(new DOMAttr('status', $node->status));
            //If the node is a nynorsk translation of the original node:
            if ($node->tnid != 0 && $node->nid != $node->tnid && $node->language == "nn") {
              $newELecture->setAttributeNode(new DOMAttr('qNid', $node->localizer_pid));
            }
            //the filepath to the electure
            $fid = db_fetch_object(db_query("SELECT fid FROM {amendor_electure_sequence} WHERE nid=%d", $node->nid));
            $result = db_query("SELECT filepath FROM {files} WHERE fid = %d", $fid->fid);
            $number = db_result(db_query("SELECT COUNT(*) FROM {files} WHERE fid = %d", $fid->fid));
            if ($number > 0) {
              $path = db_fetch_object($result)->filepath;
              $newELecture->setAttributeNode(new DOMAttr('path', base_path() . $path));
            }
            else {
              $newELecture->setAttributeNode(new DOMAttr('path', base_path() . $ePath));
            }
          }
          //If we are updating an existing node:
          else {
            $newELecture->setAttributeNode(new DOMAttr('nid', $eNid));
            $fid = db_fetch_object(db_query("SELECT fid FROM {amendor_electure_sequence} WHERE nid = %d", $eNid));
            $newELecture->setAttributeNode(new DOMAttr('path', base_path() . db_fetch_object(db_query("SELECT filepath from {files} WHERE fid = %d", $fid->fid))->filepath));
            //TODO: FIX localization
            //$pidRes = db_query("SELECT pid,language FROM {node} WHERE nid = %d",$eNid);
            //$rows = db_result(db_query("SELECT COUNT(*) FROM {node} WHERE nid = %d",$eNid));
            $pidObj = db_fetch_object(db_query("SELECT tnid,language FROM {node} WHERE nid = %d", $eNid));

            //Adding the nid of the original node:
            if ($pidObj->tnid != 0) {
              $pid = $pidObj->tnid;
              $language = $pidObj->language;
              if ($eNid != $pid && $language == "nn") {
                $newELecture->setAttributeNode(new DOMAttr('qNid', $pid));
              }
            }
            if ($eNid == $node->nid) {
              $newELecture->setAttributeNode(new DOMAttr('status', $node->status));
            }
            else {
              $newELecture->setAttributeNode(new DOMAttr('status', $eStatus));
            }
          }
          $newTopic->appendChild($newELecture);
        }
      }
    }
  }
  $toReturn = $newDoc->saveXML();
  //Removing the header from the xml:
  $xmlTemp = spliti(">\n", $toReturn, 2);
  $toReturn = $xmlTemp[1];
  restore_error_handler();
  return $toReturn;
}

/*
 * Implementation of _amendor_electure_extract_subtitles.
 *
 * @param $xmlString - the xml of the subtitles
 * @return subtitles
 */

function _amendor_electure_extract_subtitles($xmlString) {
  set_error_handler('_amendor_electure_xml_error_handler'); // This is to ignore PHP-warnings
  $doc = new DOMDocument('1.0', 'utf-8');
  //Verifying xml:
  try {
    $doc->loadXml($xmlString);
  }
  catch (Exception $e) {
    form_set_error('', 'Ugyldig xml');
    watchdog('eLecture', 'Ugyldig xml i amendor_extract_subtitles');
  }
  restore_error_handler();
  return $doc->getElementsByTagName("st");
}

/**
 * Generates code for embedding electures on external sites
 * 
 * @param $node
 *  The electure node
 * @return 
 *  Themed html
 */
function theme_amendor_electure_external_embed($node, $width = '720', $height = '568') {
  $nid = $node->nid;
  $language = t('bm');
  $us = variable_get('amendor_electure_url_start', '');
  $url_start = $us . base_path();
  $url_flash = $url_start . drupal_get_path('module', 'amendor_electure') . '/flash/START.swf';
  $url_framework = $url_start . drupal_get_path('module', 'amendor_electure') . '/flash/mainSubs.swf?y=6';
  $url_flash_com = $url_start . '?q=amendor_electure_flash';
  $flash_vars = 'flashCom=' . $url_flash_com
    . '&amp;nid=' . $node->framework
    . '&amp;lang=' . $language
    . '&amp;l=' . $_SERVER['HTTP_HOST'] . base_path() . 'index.php?q=eForelesningFullskjerm'
    . '&amp;playerSwf=' . $url_framework
    . '&amp;e=' . $node->nid
    . '&amp;bF=1'
    . '&amp;ev=1'
    . '&amp;us=' . $us;
  if (!is_numeric($node->framework) || $node->framework == 0) {
    $fid = db_fetch_object(db_query("SELECT fid FROM {amendor_electure_sequence} WHERE nid = %d", $node->nid));
    $electure_file = $url_start . db_fetch_object(db_query("SELECT filepath from {files} WHERE fid = %d", $fid->fid))->filepath;
    $flash_vars .= '&amp;eLF=' . $electure_file;
  }
  return '<object height="' . $height . '" width="' . $width . '" type="application/x-shockwave-flash" data="' . $url_flash . '">
  <param name="movie" value="' . $url_flash . '" />
  <param name="wmode" value="transparent" />
  <param name="menu" value="false" />
  <param name="quality" value="high" />
  <param name="allowFullScreen" value="true" />
  <param name="scale" value="exactfit" />
  <param name="FlashVars" value="' . $flash_vars . '" />
  <embed height="' . $height . '" width="' . $width . '" flashvars="' . $flash_vars . '"'
    . ' src="' . $url_flash . '"></embed>
  </object>';
}

/**
 * Add js to the node form
 * 
 * We use a theme function for this to make sure that the js is added when the form is fetched from cache(for instance if it doesn't
 * pass validation)
 */
function theme_amendor_electure_form_js($form) {
  drupal_add_js(drupal_get_path('module', 'amendor_electure') . '/javascripts/amendor_electure_communication.js');
}

/**
 * Implementation of hook_preprocess_amendor_electure().
 */
function amendor_electure_preprocess_amendor_electure(&$vars) {
  global $user;

  $node = node_load($vars['nid']);
  $base_path = base_path();
  $module_path = drupal_get_path('module', 'amendor_electure');
  drupal_add_js($module_path . '/javascripts/swfobject.js');
  $module_path = $base_path . drupal_get_path('module', 'amendor_electure');

  $vars['player_swf_url'] = $module_path . '/flash/mainSubs.swf?y=6';
  $vars['flash_com_url'] = $base_path . 'index.php?q=amendor_electure_flash';
  $vars['fullscreen_url'] = explode('?', substr(url('amendor-electure/fullscreen', array('absolute' => TRUE)), 7), 2);
  $vars['fullscreen_url'] = $vars['fullscreen_url'][0];
  if (isset($node->framework)) {
    $vars['framework_nid'] = $node->framework;
  }
  $vars['start_swf_url'] = $module_path . '/flash/START.swf';
  if ($vars['dynamic_size']) {
    $vars['width'] = '100%';
    $vars['height'] = '100%';
  }
  else {
    $vars['height'] = round((568 / 720) * $vars['width']);
  }
  $vars['express_install_swf_url'] = $module_path . '/flash/expressInstall.swf';
  $vars['fullscreen'] = $vars['fullscreen'] ? '' : '1';
  if (!is_numeric($vars['framework_nid']) || $vars['framework_nid'] == 0) {
    $vars['swf_url'] = $base_path . $node->swf->filepath;
  }
  $vars['user_id'] = $user->uid;
}

function amendor_electure_page($fullscreen, $framework_nid, $electure_nid, $start_frame = NULL, $stop_frame = NULL, $language = NULL) {
  if ($stop_frame && !is_numeric($stop_frame)) {
    $language = $stop_frame;
    $stop_frame = '';
  }
  $allowed_hosts = variable_get('amendor_electure_hosts_allowed_embed', array());
  if (isset($_SERVER['HTTP_REFERER']) && count($allowed_hosts)) {
    $matches = array();
    $allowed_hosts[] = $_SERVER['SERVER_NAME'];
    preg_match('/^http:\/\/([a-z0-9.-]+)\//i', $_SERVER['HTTP_REFERER'], $matches);
    if (!isset($matches[1]) || !in_array($matches[1], $allowed_hosts)) {
      drupal_not_found();
      exit;
    }
  }

  // Get HTML alternative
  $node = node_load($electure_nid);
  $html_video = theme('istribute_player', $node /*, 720, 468 */);
  
  print theme('amendor_electure_page', theme('amendor_electure', $electure_nid, 720, $fullscreen, TRUE, $framework_nid, $start_frame, $stop_frame, $language) . $html_video, $electure_nid, $fullscreen);
}

function amendor_electure_redirect_deprecated($framework_nid, $electure_nid, $start_frame = NULL, $stop_frame = NULL, $language = NULL) {
  drupal_goto(url('amendor-electure/fullscreen', array('absolute' => TRUE)) . '/' . $framework_nid . '/' . $electure_nid . '/' . ($start_frame == NULL ? '' : $start_frame) . '/' . ($stop_frame == NULL ? '' : $stop_frame) . '/' . ($language == NULL ? '' : $language), NULL, NULL, 301);
}