function thisMovie(txtFieldName) {
    if (navigator.appName.indexOf("Microsoft") != -1) {
        return window[txtFieldName]
    }
    else {
        return document[txtFieldName]
    }
}

/*
* set the xml string to be the updated xml from flash
*/
function callTextField(returnXML) {
    //alert("Flash is speaking:"+returnXML);
    document.getElementById("xmlText").value=returnXML;
}

/*
* flash calls js: the xml is updated in flash
*/
function updateFromFlash(returnXML) {
	callTextField(returnXML);
}


/*
* set the new title to be the updated title from flash
*/
function callTextFieldTitle(newTitle) {
	if(newTitle == "null"){
		document.getElementById("titleTxt").value="";
	} else {
    	document.getElementById("titleTxt").value=newTitle;
    }
}


/*
* flash calls js: the title is updated in flash
*/
function updateNewTitleFromFlash(newTitle) {
	callTextFieldTitle(newTitle);
}

function askForTitle(){
	updateTitle(document.getElementById("titleTxt").value);
}
function getXMLfromForm(){
   return document.getElementById("xmlText").value;
   //alert(document.getElementById("xmlText").value);
} 
/*
* function the update flash with the new title,
*/
function updateTitle(title) {
  document.getElementById("flashcontent").sendTitleToFlash(title);
} 


/*
* function the update flash with new selected framework,
* need to get all the relevant subjects and eLectures for the menu
*/
function updateSelectedFramework(nid) {
  document.getElementById("flashcontent").sendSelectedFrameworkToFlash(nid);
} 


/*
* set the new mails to be the updated mails from flash
*/
function updateMail(newMails) {
	if(newMails == "null"){
		document.getElementById("fb").value="";
	} else {
    	document.getElementById("fb").value=newMails;
    }
}

/*
* set the repetition to be the updated repetition from flash
*/
function updateRep(newRep) {
  document.getElementById("fb_rep").value=newRep;
}
