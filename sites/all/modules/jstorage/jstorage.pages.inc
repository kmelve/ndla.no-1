<?php

/**
 * Ajax callback returning all data in a given namespace
 *
 * Namespace is specified in $_POST
 */
function jstorage_get_namespace() {
  global $user;
  $to_return = array();
  if (isset($_POST['namespace'])) {
    $result = db_query("SELECT name, value FROM {jstorage} WHERE uid = %d AND namespace = '%s'", $user->uid, $_POST['namespace']);
    while ($result_o = db_fetch_object($result)) {
      $to_return[$result_o->name] = $result_o->value;
    }
  }
  drupal_json($to_return);
}

/**
 * Ajax callback setting a value for a given name and namespace
 *
 * Attributes are specified in $_POST
 */
function jstorage_set_value() {
  global $user;
  if (isset($_POST['namespace'], $_POST['name'], $_POST['value'])) {
    db_query("REPLACE INTO {jstorage} (uid, namespace, name, value) VALUES (%d, '%s', '%s', '%s')",
    $user->uid, $_POST['namespace'], $_POST['name'], $_POST['value']);
    drupal_json(array('success' => 1));
  }
  else {
    drupal_json(array('success' => 0));
  }
}

/**
 * Ajax callback deleting a value for a given name and namespace
 *
 * Attributes are specified in $_POST
 */
function jstorage_delete_value() {
  global $user;
  if (isset($_POST['namespace'], $_POST['name'])) {
    db_query("DELETE FROM {jstorage} WHERE uid = %d AND namespace = '%s' AND name = '%s'",
    $user->uid, $_POST['namespace'], $_POST['name']);
    drupal_json(array('success' => 1));
  }
  else {
    drupal_json(array('success' => 0));
  }
}