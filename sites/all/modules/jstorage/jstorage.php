<?php
/*
 * NOTE: To use this file you need to have your module in sites/all/modules/. You can't put it in a subfolder.
 * AND:  You have to define $base_url in settings.php to use this module.
 */
include_once 'jstorage.pages.inc';
chdir('../../../../');
include_once "includes/bootstrap.inc";
include_once "includes/common.inc";

drupal_bootstrap(DRUPAL_BOOTSTRAP_SESSION);

if (!isset($GLOBALS['user'], $GLOBALS['user']->uid)) {
  exit;
}
$user = $GLOBALS['user'];
if ($user->uid != 1) {
  $result = db_query("SELECT p.perm FROM {role} r INNER JOIN {permission} p ON p.rid = r.rid WHERE r.rid IN (". db_placeholders($user->roles) .")", array_keys($user->roles));
  $perms = array();
  while ($row = db_fetch_object($result)) {
    $perms += array_flip(explode(', ', $row->perm));
  }
  if (!isset($perms['access jstorage'])) {
    exit;
  }
}
switch ($_POST['jstorage_op']) {
  case 'set':
    jstorage_set_value();
    break;
  case 'get':
    jstorage_get_namespace();
    break;
  case 'delete':
    jstorage_delete_value();
    break;
}
?>
