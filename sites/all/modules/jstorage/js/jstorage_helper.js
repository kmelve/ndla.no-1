JStorage.prototype.dbSetHelper = function(name, value) {
  if (name in this.dbValues && this.dbValues[name] === value) {
    return;
  }
  var url;
  if (Drupal.settings.jstorage.method == 0) {
    url  = Drupal.settings.basePath + 'jstorage/set';
  }
  else {
    url = Drupal.settings.basePath + Drupal.settings.jstorage.modulePath + '/jstorage.php';
  }
  $.ajax({
    type: "POST",
    timeout: 3000,
    url: url,
    data: {namespace: this.namespace, name: name, value: value, jstorage_op: 'set'}
  });
  this.dbValues[name] = value;
};

JStorage.prototype.dbGetHelper = function(name) {
  if (this.dbGetRun) {
    if (name in this.dbValues) {
      return this.dbValues[name];
    }
    return null;
  }
  var url;
  if (Drupal.settings.jstorage.method == 0) {
    url  = Drupal.settings.basePath + 'jstorage/get';
  }
  else {
    url = Drupal.settings.basePath + Drupal.settings.jstorage.modulePath + '/jstorage.php';
  }
  var data = $.ajax({
    async: false,
    type: "POST",
    url: url,
    timeout: 3000,
    data: {namespace: this.namespace, name: name, jstorage_op: 'get'}
  }).responseText;
  try {
    eval('data = ' + data + ';');
  }
  catch (e) {
    return null;
  }
  $.extend(this.dbValues, data);
  this.dbGetRun = true;
  if (name in data) {
    return data[name];
  }
  else {
    return null;
  }
};

JStorage.prototype.dbDeleteHelper = function(name) {
  var url;
  if (Drupal.settings.jstorage.method == 0) {
    url  = Drupal.settings.basePath + 'jstorage/delete';
  }
  else {
    url = Drupal.settings.basePath + Drupal.settings.jstorage.modulePath + '/jstorage.php';
  }
  $.ajax({
    type: "POST",
    url: url,
    timeout: 3000,
    data: {namespace: this.namespace, name: name, jstorage_op: 'delete'}
  });
  this.dbValues[name] == null;
};