var JStorage = JStorage || function (namespace, callback, options) {
  var that = this;
  
  this.namespace = namespace;
  this.defaultOptions = this.prepareOptions(options);
  this.values = {};
  this.toStore = {};
  this.collecting = false;
  this.dbGetRun = false;
  this.dbValues = {};
  
  if (Drupal.settings.jstorage.method == 2 && window[Drupal.settings.jstorage.object] != undefined) {
    eval(Drupal.settings.jstorage.object + '(namespace, function (propertyListObject) { that.propertyListObject = propertyListObject; callback(that); });');
  }
  else {
    callback(this);
  }
};

JStorage.hasLocalStorage = JStorage.hasSessionStorage = false;

try {
  JStorage.hasLocalStorage = typeof localStorage != 'undefined';
} catch (e) {}
try {
  JStorage.hasSessionStorage = typeof sessionStorage != 'undefined';
} catch (e) {}

JStorage.prototype.del = function (name, options) {
  options = this.prepareOptions(options);
  var storageName = options.group ? options.group : name;
  if (options.group) {
    var oldGroup = this.get(options.group);
    if (typeof oldGroup == 'undefined' || oldGroup === null) {
      return;
    }
    delete oldGroup[name];
    var oldGroupId = options.group;
    delete options.group;
    this.set(oldGroupId, oldGroup, options);
  }
  else {
    JStorage.cookie(this.namespace + '-' + storageName, null);
    if (JStorage.hasLocalStorage) {
      window.localStorage.removeItem(this.namespace + '-' + storageName);
    }
    if (JStorage.hasSessionStorage) {
      window.sessionStorage.removeItem(this.namespace + '-' + storageName);
    }
    if (typeof this.dbDelete != 'undefined') {
      if (this.collecting) {
        this.toStore[storageName] = null;
      }
      else {
        this.dbDelete(storageName);
      }
    }
    this.values[storageName] = null;
  }
};

JStorage.prototype.set = function (name, value, options) {
  options = this.prepareOptions(options);
  var storageName = options.group ? options.group : name;
  var toStore = $.toJSON(value);
  if (options.group) {
    var oldGroup = this.get(options.group);
    if (oldGroup === null || typeof oldGroup != 'object') {
      oldGroup = {};
    }
    oldGroup[name] = toStore;
    this.values[storageName] = oldGroup;
    toStore = $.toJSON(oldGroup);
  }
  else {
    this.values[storageName] = value;
  }
  if (options.profile && typeof this.dbSet != 'undefined') {
    if (this.collecting) {
      this.toStore[storageName] = toStore;
    }
    else {
      this.dbSet(storageName, toStore);
    }
  }
  if (options.browser) {
    if (options.session) {
      if (JStorage.hasSessionStorage) {
        try {
          window.sessionStorage[this.namespace + '-' + storageName] = toStore;
        } catch(e) {}
      }
      else {
        JStorage.cookie(this.namespace + '-' + storageName, toStore);
      }
    }
    else {
      if (JStorage.hasLocalStorage) {
        try {
          window.localStorage[this.namespace + '-' + storageName] = toStore;
        } catch (e) {}
      }
      else {
        JStorage.cookie(this.namespace + '-' + storageName, toStore, {expires: 9999});
      }
    }
  }
};

JStorage.prototype.get = function (name, options) {
  options = this.prepareOptions(options);
  var storageName = options.group ? options.group : name;
  if (storageName in this.values) {
    if (this.values[storageName] === null) {
      return null;
    }
    if (options.group) {
      return name in this.values[storageName] ? JStorage.stringToBool(this.values[storageName][name]) : null;
    }
    return this.values[storageName];
  }
  var toReturn = undefined;
  if (options.profile && typeof this.dbGet != 'undefined') {
    toReturn = this.dbGet(storageName);
    if (typeof toReturn == 'string') {
      toReturn = $.secureEvalJSON(toReturn);
      if (options.group) {
        this.values[storageName] = JStorage.stringToBool(toReturn);
        if (typeof toReturn == 'object' && toReturn !== null && name in toReturn) {
          return JStorage.stringToBool(toReturn[name]);
        }
        else {
          return null;
        }
      }
      else {
        this.values[storageName] = JStorage.stringToBool(toReturn);
        return JStorage.stringToBool(toReturn);
      }
    }
  }
  if (options.browser) {
    if (JStorage.hasLocalStorage) {
      toReturn = window.localStorage.getItem(this.namespace + '-' + storageName);
    }
    if (typeof toReturn == 'undefined' || toReturn == 'undefined') {
      toReturn = JStorage.cookie(this.namespace + '-' + storageName);
    }
    if ((typeof toReturn == 'undefined' || toReturn == 'undefined') && JStorage.hasSessionStorage) {
      toReturn = window.sessionStorage.getItem(this.namespace + '-' + storageName);
    }
    if (typeof toReturn == 'string' && toReturn != 'undefined') {
      toReturn = $.secureEvalJSON(toReturn);
      if (options.group) {
        if (typeof toReturn == 'object' && toReturn !== null && name in toReturn) {
          this.values[storageName] = JStorage.stringToBool(toReturn);
          return JStorage.stringToBool(toReturn[name]);
        }
        else {
          this.values[storageName] = null;
          return null;
        }
      }
      else {
        this.values[storageName] = JStorage.stringToBool(toReturn);
        return JStorage.stringToBool(toReturn);
      }
    }
  }
  this.values[storageName] = null;
  return null;
};

JStorage.prototype.startCollecting = function () {
  this.collecting = true;
}

JStorage.prototype.stopCollecting = function () {
  this.collecting = false;
  if (typeof this.dbSet != 'undefined') {
    for (var i in this.toStore) {
      if (this.toStore[i] === null) {
        this.dbDelete(i);
      }
      else {
        this.dbSet(i, this.toStore[i]);
      }
    }
  }
  this.toStore = {};
}

JStorage.prototype.prepareOptions = function (options) {
  if (typeof options == 'undefined') {
    options = {};
  }
  if (typeof this.defaultOptions == 'undefined') {
    this.defaultOptions = {profile: true, browser: true, session: false, group: false};
  }
  return {
    profile: 'profile' in options ? options.profile : this.defaultOptions.profile,
    browser: 'browser' in options ? options.browser : this.defaultOptions.browser,
    session: 'session' in options ? options.session : this.defaultOptions.session,
    group: 'group' in options ? options.group : this.defaultOptions.group
  };
};

JStorage.stringToBool = function (toReturn) {
  if (toReturn == 'false')
    return false;
  if (toReturn == 'true')
    return true;
  if (toReturn == 'undefined')
    return undefined;
  if (toReturn == 'null')
    return null;
  return toReturn;
};

JStorage.prototype.dbSet = function (name, value) {
  if (Drupal.settings.jstorage.method == 2) {
    try {
      this.propertyListObject.set(name, value);
    }
    catch (err) {}
  }
  else {
    this.dbSetHelper(name, value);
  }
};

JStorage.prototype.dbGet = function (name) {
  if (Drupal.settings.jstorage.method == 2) {
    try {
      return this.propertyListObject.get(name);
    }
    catch (err) {}
  }
  else {
    return this.dbGetHelper(name);
  }
};

JStorage.prototype.dbDelete = function (name) {
  if (Drupal.settings.jstorage.method == 2) {
    try {
      this.propertyListObject.unset(name);
    }
    catch (err) {}
  }
  else {
    this.dbDeleteHelper(name);
  }
};

JStorage.prototype.getUserName = function () {
  if (Drupal.settings.jstorage.method == 2) {
    try {
      return this.propertyListObject.getDisplayName();
    }
    catch (err) {
      return Drupal.t('Anonymous');
    }
  }
  return Drupal.settings.jstorage.username;
};

JStorage.prototype.isLoggedIn = function () {
  if (Drupal.settings.jstorage.method == 2) {
    try {
      return this.propertyListObject.getDisplayName() ? true : false;
    }
    catch (err) {
      return false;
    }
  }
  return Drupal.settings.jstorage.logged_in;
}

JStorage.cookie = function (name, value, options) {
  // @author Klaus Hartl/klaus.hartl@stilbuero.de
  if (typeof value != 'undefined') { // name and value given, set cookie
    options = options || {};
    if (value === null) {
      value = '';
      options.expires = -1;
    }
    var expires = '';
    if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
      var date;
      if (typeof options.expires == 'number') {
        date = new Date();
        date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
      } else {
        date = options.expires;
      }
      expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
    }
    var path = '; path=/';
    var domain = options.domain ? '; domain=' + (options.domain) : '';
    var secure = options.secure ? '; secure' : '';
    document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
  }
  else { // only name given, get cookie
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) == (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
}