<?php

/**
 * Admin settings form.
 */
function jstorage_admin() {
  $form = array();
  $form['jstorage_method'] = array(
    '#title' => t('Storage method'),
    '#description' => t('Select your storage method. "%fast_drupal" requires that you have your jstorage module in sites/all/modules and $base_path defined in settings.php', array('%fast_drupal' => t('Fast Drupal'))),
    '#type' => 'radios',
    '#options' => array(t('Normal Drupal'), t('Fast Drupal'), t('External')),
    '#default_value' => variable_get('jstorage_method', 0),
  );
  $form['external_storage'] = array(
    '#title' => t('External storage'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#Collapsed' => variable_get('jstorage_method', 0) != 2,
  );
  $form['external_storage']['jstorage_external_object'] = array(
    '#title' => t('create storage object function'),
    '#type' => 'textfield',
    '#description' => t('Enter the name of a javascript function that will create a storage object. It must take the following parameters: "namespace", function (propertyListObject) { /* Callback code */ }'),
    '#default_value' => variable_get('jstorage_external_object', 'createCachedUserPropertiesObject'),
  );
  return system_settings_form($form);
}