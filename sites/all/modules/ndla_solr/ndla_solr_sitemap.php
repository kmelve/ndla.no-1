<?php

require_once('sites/all/modules/apachesolr/SolrPhpClient/Apache/Solr/Service.php');

class NDLA_Solr_Sitemap extends Apache_Solr_Service {
  
  function __construct() {
    $host = variable_get('apachesolr_host', 'localhost');
    $port = variable_get('apachesolr_port', '8983');
    $path = variable_get('apachesolr_path', '/solr');
    parent::__construct($host, $port, $path);
  }
  
  function get_sitemap($tid) {
    global $language;
    
    $params = array(
      'version' => self::SOLR_VERSION,
      'wt' => self::SOLR_WRITER,
      'json.nl' => $this->_namedListTreatment,
      //'facet.field' => 'ts_vid_100004_names',
      'facet.field' => 'im_vid_100004',
      'facet' => 'true',
      'rows' => '0',
      'fq' => '',
    );
    if(!empty($tid))
      $params['fq'] = 'tid:' . $tid;
    $params['fq'] .= "language:" . $language->language;
    
    $queryString = http_build_query($params, null, $this->_queryStringDelimiter);
    $result = $this->_sendRawGet($this->_searchUrl . $this->_queryDelimiter . $queryString);
    $data = $this->parseResult($result);
    return $data;
  }
  
  function parseResult($results) {
    if (!$results) {
      return array();
    }
    $content_types = $this->_get_content_types();
    $data = array();
    $response = json_decode($results->getRawResponse());
    if(isset($response->facet_counts)) {
      $hits = $response->facet_counts->facet_fields->im_vid_100004;
      foreach($hits as $tid => $count) {
        if(!empty($content_types[$tid])) {
          $data[$tid] = array($content_types[$tid], $count);
        }
      }
    }
    return $data;
  }
  
  function _get_content_types() {
    $content_types = array();
    $ct_vid = ndla_utils_get_content_type_vocab();
    $tree = taxonomy_get_tree($ct_vid);
    foreach($tree as $content_type) {
      if(!count(array_filter($content_type->parents))) {
        $content_types[$content_type->tid] = $content_type->name;
      }
    }
    return $content_types;
  }
  
}