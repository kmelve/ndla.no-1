var clickedid; // div id for the wrapper for the browser button that was clicked
var cached=new Array();
var jsondata;
var drupalpath='/';
var utdanningrdf="FALSE";

// function to populate the noderefernce field
function runkode(text){
	$('#'+clickedid+' input').val(text);
	Lightbox.end('forceClose');
}

function klikklogg(i){
		clickedid=i;
	}

// function to pupulate the browser
function populate(){
	urlen=clickedid.replace(/wrapper/,'autocomplete');			
	urlen=$('#'+urlen).val();				
	urlen=String(urlen);
	urlen=urlen.split('/');
	fieldname=urlen.pop();
	text=$('#dynsearch').val();
	page=$('#dynspage').val();			
	user=$('#dynsuser').val();
	tax=$('#dynstax').val();
	sort=$('#dynssort').val();
	sortby=$('#dynssortby').val();
	view=$('#dynsview').val();
	type=$('#dynstype').val();
	lang = $('#dynslanguage').val();
  $('#dynsearch').attr("readonly", "true");
	$('#dynsearch').removeClass('enabled');
	$('#dynsearch').addClass('disabled');
	
	if(view=='thumb'){
		hits=9;
	}else{
		hits=15;
	}
	if(parseInt($('#dynstotalcount').val())<(hits*page)){
	
	}

  data_type = 'html';
  //Cookbook hack
  if(clickedid == 'edit-cookbook-recipe-images-wrapper') {
    geturl=Drupal.settings.basePath+'?q=ole_noderef_old/'+fieldname+'/'+text+'/&page='+page+'&hits='+hits+'&view='+view+'&user='+user+'&tax='+tax+'&sort='+sort+'&type='+type+'&sortby='+sortby+'&lang='+lang;
    data_type = 'json';
  }
	else if(clickedid=="edit-utdanning-rdf-table-title-wrapper"){
		utdanningrdf="TRUE";
		geturl=Drupal.settings.basePath+'?q=ole_noderef/'+fieldname+'/'+text+'/&from=rdf&page='+page+'&hits='+hits+'&view='+view+'&user='+user+'&tax='+tax+'&sort='+sort+'&type='+type+'&sortby='+sortby+'&lang='+lang;
	}else if(clickedid=="edit-link-wrapper"){
		fieldname="mmenu";
		geturl=Drupal.settings.basePath+'?q=ole_noderef/'+fieldname+'/'+text+'/&page='+page+'&hits='+hits+'&view='+view+'&user='+user+'&tax='+tax+'&sort='+sort+'&type='+type+'&sortby='+sortby+'&lang='+lang;
	}else{
		geturl=Drupal.settings.basePath+'?q=ole_noderef/'+fieldname+'/'+text+'/&page='+page+'&hits='+hits+'&view='+view+'&user='+user+'&tax='+tax+'&sort='+sort+'&type='+type+'&sortby='+sortby+'&lang='+lang;
	}

	if(cached[geturl] && 0) {
		parsedata(cached[geturl]);
	}
	else {
		$.ajax({
			url: geturl,
			async: false,
			dataType: data_type,
			success: function(html){ 
				cached[geturl]=html;
        parsedata(html);
      },
      error: function(html, textStatus, error) {
        alert("textStatus: " + textStatus + " error: " + error);
		  	alert("Error calling content browser backend." + geturl + " " + html);
		  	$('dynsearch').attr('readonly', false);
		  }
		});
    }
    page=parseInt(page)+1;
    geturl=Drupal.settings.basePath+'?q=ole_noderef/'+fieldname+'/'+text+'/&page='+page+'&hits='+hits+'&view='+view+'&user='+user+'&tax='+tax+'&sort='+sort+'&type='+type+'&sortby='+sortby+'&lang='+lang;
	setTimeout('fetchdata(geturl)', 500);
	
}
function fetchdata(geturl){
	$.ajax({
		url: geturl,
		dataType: 'json',
		success: function(html){
			cached[geturl]=html;					    
		}
	});
}
function parsedata(data){
	$('#dynsearch').removeAttr("readonly"); 
	$('#dynsearch').removeClass('disabled');
	$('#dynsearch').addClass('enabled');
	
	$('#thelist').html('');
	view=$('#dynsview').val();
	
	if(parseInt(data.total)==0){
		$('#thelist').html('<div class="nohits"><span>'+Drupal.t('No results.')+'</span></div>');
		$('#pager').html('');
		return;
	}
	
	if(view=='thumb'){ //this is thumb view
		//$('#outerImageContainer').attr('style', 'background-color: rgb(255, 255, 255); color: rgb(0, 0, 0); width: 620px; display: block; height: 440px;');
		$('#thelist').html('<div id=\"thelist-container\"></div>')
		$.each(data.nodes, function(field,value){		
			runnkode=value.title+' [nid:'+value.nid+']';			
			if(value.image){
				tdiv='<div class=\"thelist-item\" onclick=\'runkode(\"'+runnkode+'\");\'><img src=\"'+Drupal.settings.basePath+value.image+'\" /><span>'+value.title.substr(0, 25)+'</span</div>';
				
			}else{
				tdiv='<div class=\"thelist-item\" onclick=\'runkode(\"'+runnkode+'\");\'><h4>'+value.type+'</h4><span>'+value.title.substr(0, 25)+'</span</div>';
			}
			
			$(tdiv).appendTo('#thelist-container');

		});
		$('#thelist .thelist-item').mouseover(function(){
			$(this).addClass('hover');
		}).mouseout(function(){
			$(this).removeClass('hover');
		});

	}
	else{ //this is list view
		$('#thelist').html('<table class=\'noderef_table\'><thead></thead><tbody></tbody></table>');
		$('#thelist thead').html('<tr><th name=\'title\' id=\'title\'>'+Drupal.t('Title')+'</th><th name=\'type\' id=\'type\'>'+Drupal.t('Type')+'</th><th name=\'language\'>'+Drupal.t('Language')+'</th><th name=\'user\' id=\'user\'>'+Drupal.t('User')+'</th><th name=\'changed\' id=\'changed\'>'+Drupal.t('Changed')+'</th></tr>');
			
		$.each(data.nodes, function(field,value) {
      runnkode=value.title+' [nid:'+value.nid+']';
      tblrow='<tr><td class=\'title\'><a href=\'javascript://\' onclick=\'runkode(\"'+runnkode+'\");\'>'+value.title.substr(0, 45)+'</a></td><td>'+value.type+'</td><td>'+value.language+'</td><td>'+value.user+'</td><td>'+value.updated+'</td><tr>';
      $(tblrow).appendTo('#thelist tbody');
    });

    // sort on press handeling
    $('#thelist #title, #thelist #changed').click(function (e){
			if($('#dynssort').val()==$(this).attr("name")){
				if($('#dynssortby').val()=="desc"){
					$('#dynssortby').val("asc")
				}else{
					$('#dynssortby').val("desc");
				}

			}
			else{
				$('#dynssort').val($(this).attr("name"));
			}
			
			$('#dynspage').val(1);
			populate();

		});	            
        
    // sort header merking
    $('#thelist table thead th').removeClass('down');
    $('#thelist table thead th').removeClass('up');
            	
    if(data.sort_type=='desc'){
      $('#'+data.sort).addClass('down');
    }
    else {
      $('#'+data.sort).addClass('up');
    }	
	}	
  
  // type filter
  $('#add_links').html('');
	$('#dynstypechoice').html("<option value='all'>all</a>");

	$.each(data.type, function(content_type, content_type_name){
	  $('#add_links').html($('#add_links').html() + "<a target='_blank' class='lenke' href='"+Drupal.settings.basePath+"?q=node/add/"+content_type+">"+content_type_name+"</a>&nbsp;&nbsp;");            
		if(data.type_c==content_type){
			$('#dynstypechoice').append('<option value=\"'+content_type+'\" selected=\"selected\">'+content_type_name+'</a>');
		}else{
			$('#dynstypechoice').append('<option value=\"'+content_type+'\">'+content_type_name+'</a>');
    }                	
  });
  
  //Populate the language-box
  $('#dynslanguage').html('');
  $.each(data.languages, function(lang_code, lang_name) {
    if(data.selected_language == lang_code) {
      $('#dynslanguage').append('<option selected value=\"'+lang_code+'\">'+lang_name+'</a>');
    }
    else {
      $('#dynslanguage').append('<option value=\"'+lang_code+'\">'+lang_name+'</a>');
    }
  });
  
  if($('#add_links').html() != '') {
    $('#add_links').html("<p>Legg til: " + $('#add_links').html() + "</p>");
  }
  
  // pager stuff
  prev='<a class=\'pager-button\' href=\'javascript://\' id=\'dynsprev\'>'+Drupal.t('Previus')+'</a> ';
  next='<a class=\'pager-button\' href=\'javascript://\' id=\'dynsnext\'>'+Drupal.t('Next')+'</a> ';
  first='<a class=\'pager-button\' href=\'javascript://\' id=\'dynsfirst\'>'+Drupal.t('First')+'</a> ';
  last=' <a class=\'pager-button\' href=\'javascript://\' id=\'dynslast\'>'+Drupal.t('Last')+'</a> ';
  i=0;
    	
  //add previous button if not first page
  var pager='';
  if(data.page==1){   		
    pager='<span class="pager-button">'+Drupal.t('First')+'</span> <span class="pager-button">'+Drupal.t('Previous')+'</span> '; //add disabled buttons
  }
  else{
    pager=first+' '+prev;
  }
    	
  //make pager items    	
  if(data.pages < 10){ // if less than 10 pages, list links to all
    while(data.pages>i){
      if(parseInt(data.page)==(i+1)){
        thtml='<a class=\'pager-item active\' href=\'javascript://\'>'+(i+1)+'</a> ';
      }
      else{
        thtml='<a class=\'pager-item\' href=\'javascript://\'>'+(i+1)+'</a> ';
      }				
      
      pager=pager+thtml;
      i++;
      
      if(10==i) {
        break;
      }
    }
  }
  
  else { //if more than 10 pages
    i=0;
    pagerlength=10;    		
    		
    if(data.page>(data.pages-(pagerlength/2))){
      curpage=data.pages-(pagerlength-1);
    }
    
    else if(data.page<(pagerlength/2)){
      curpage=1;    			
    }
    
    else {
      curpage=data.page-((pagerlength/2)-1);    			
    }
    		
    while(pagerlength>i){
      if(parseInt(data.page)==(curpage)){
        thtml='<a class=\'pager-item active\' href=\'javascript://\'>'+(curpage)+'</a> ';
      }
      else{
        thtml='<a class=\'pager-item\' href=\'javascript://\'>'+(curpage)+'</a> ';
      }
      
      pager=pager+thtml;				
      i++;
      curpage++;
    }
  }
    	
  // add next page if not last page
  if(parseInt(data.page)!=parseInt(data.pages)){
    pager=pager+next+last;
  } 
  else {
    pager=pager+' <span class="pager-button">'+Drupal.t('Next')+'</span> <span class="pager-button">'+Drupal.t('Last')+'</span> '; //add disabled buttons
  }

  $('#pager').html('');
  $(pager).appendTo('#pager');
    	
  // adding onclick handlers
  $('#pager .pager-item').click(function(){
    $('#dynspage').val($(this).text());
    populate();
  });
  // previous page
  $('#dynsprev').click(function(){
    $('#dynspage').val(parseInt($('#dynspage').val())-1);
    populate();
  });
  // next page		
  $('#dynsnext').click(function(){
    $('#dynspage').val(parseInt($('#dynspage').val())+1);
    populate();
  });
  // first page	
  $('#dynsfirst').click(function(){
    $('#dynspage').val(1);
    populate();
  });
  // last page
  $('#dynslast').click(function(){
    $('#dynspage').val(data.pages);
    populate();
  });

	if(i<2){
		$('#pager .selected').html('');
	}						

	$('#pager').append(' <span id=\"total_count\">('+Drupal.t('Pages')+': '+data.pages+')('+Drupal.t('Count')+': '+data.total+')</span>');
	$('#dynstotalcount').val(data.total);
}

function te(){
	$('#pager').html(cached);
}
// adds browser buttons to nodereference field
function make_browser_buttons(field){
	$('#'+field+'-items div.form-item, #edit-'+field+'-0-nid-nid-wrapper').each(function(index) {
		var url = Drupal.settings.basePath+modulepath + '/browse.php';
		var rel = 'lightmodal';
		if(Drupal.settings.has_contentbrowser) {
	    url = Drupal.settings.basePath + "contentbrowser?output=drupal&nodereference=" + field;
	    rel = 'lightframe[|width:1000px;height:800px]';
	  }
		var link='<a href=\"' + url + '\" rel=\"' + rel + '\">'+Drupal.t('Browse')+'</a>';
		if($(this).find("a").html()==Drupal.t('Browse') || $('input', $(this)).length == 0){

		}else{
			$(this).append(link);
		}
	});
	Lightbox.initList();
	

	$('#'+field+'-items div.form-item a, #edit-'+field+'-0-nid-nid-wrapper a').click(function(e){		
		clickedid=$(this).parent().attr('id');
		
		//$('#lightbox').unbind('click');

		//if (e.preventDefault) { e.preventDefault(); }
		//return false;
	});
	
}

