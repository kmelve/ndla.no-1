
<style>
input.disabled {
	background-color: #eee;
}
input.enabled {
	background-color: #fff;
}
</style>
<div id="controller">
<div id='add_links'>
</div>
<form id="dyns" >
	<div class="form-item">
		<label id="dynsearchlanguage" for="dynslanguage">Language</label>
		<select id="dynslanguage" name="dynslanguage">
		</select>				
	</div>
	<div style="clear: both"></div>
	<div class="form-item">
		<label id="dynsearchlabel" for="dynsearch">Search</label>
		<input type="text" id="dynsearch" name="dynsearch" size="20">		
	</div>
	<div class="form-item">
		<label id="dynsearchtype" for="dynstype">Type</label>
		<select id="dynstypechoice" name="dynstype">
		</select>				
	</div>


	<input type="hidden" id="dynspage" value="1" />
	<input type="hidden" id="dynsuser" value="0" />
	<input type="hidden" id="dynstax" value="0" />
	<input type="hidden" id="dynssort" value="changed" />
	<input type="hidden" id="dynssortby" value="desc" />
	<input type="hidden" id="dynstype" value="all" />
	<input type="hidden" id="dynstotalcount" value="0" />

</div>
<div id="thelist">

</div>
<div id="pager">
</div>
	<div class="form-item">
		<label id="dynsearchview" for="dynsview">View</label>
		<select id="dynsview" name="dynsview">
			<option value="list">List</option>
			<option value="thumb">Thumb</option>
		</select>				
	</div>
</form>	
<script type="text/javascript">
  var timer = 0;
	populate();
	$('#dynsearch').keyup(function (e) {
		if(!$('#dynsearch').attr("readonly")) {	
			clearTimeout(timer);
			timer = setTimeout ("populate()", 500); 
		}
	});
	$(document).ready(function() {
	$("#dynsearch").bind("keypress", function(e) {
		if (e.keyCode == 13){  
			$('#dynspage').val(1);
      if(!$('#dynsearch').attr("readonly")) {	
			  clearTimeout(0);
			  timer = setTimeout ("populate()", 500); 
		  }
			else {
			  populate(); 
			}
			return false;
		}
		});
	});
	$('#pager .pager-item').click(function(){
		$('#dynspage').val($(this).text());
		populate();
	});

	$('#dynsearchhits').change(function (){		
		$('#dynshits').val($('#dynsearchhits :selected').text());
		populate();
	});

	$('#dynsview').change(function (){		
		$('#dynsview').val($('#dynsview :selected').text());
		$('#dynspage').val(1);
		populate();	
	});

	$('#dynstypechoice').change(function (){		
		$('#dynstype').val($('#dynstypechoice :selected').val());
		$('#dynspage').val(1);
		populate();	
	});

	$('#dynslanguage').change(function (){		
		$('#dynslanguage').val($('#dynslanguage :selected').val());
		$('#dynspage').val(1);
		populate();	
	});
</script>
