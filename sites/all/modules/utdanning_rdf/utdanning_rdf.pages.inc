<?php
/**
 * @file
 * @ingroup utdanning_rdf
 * Utdanning_rdf page callback file for the utdanning_rdf module.
 */

function utdanning_rdf_lightbox() {
  if (!is_numeric(arg(1))) {
    return;
  }
  $nid = arg(1);
  $node = node_load($nid);
  $title = check_plain($node->title);
  $content = node_view($node, TRUE, TRUE, TRUE); //check_markup($node->teaser, $node->format);
  
  switch ($node->type) {
    case 'image':
      $size = getimagesize($node->images['fullbredde']);
      $media = '<img src="' . base_path() . $node->images['fullbredde'] . '"' . $size[3] . ' alt="' . $title . '" />';
      $content = ''; //check_markup($node->teaser, $node->format);
      break;
    case 'video':
    case 'flashnode':
      $media = '';
      //$content = check_markup($node->teaser, $node->format);
      break;
    case 'audio':
      $media = '<div id="audioplayer">' . audio_get_node_player($node) . '</div>';
      //$content = check_markup($node->teaser, $node->format);
      break;
    case 'person':
      //This is a block, pick up the data from the block_right module
      if(module_exists('utdanning_block_right')) {
        $block = utdanning_block_right_block('view', 'author_to');
        //Remove the /lightbox from urls
        $block['content'] = str_replace("/lightbox", "", $block['content']);
        //Append the content to the content.
        $content .= $block['content'];
      }
    default:
      $title = '';
      $media = '';
  }
  
  $link = l(t('Go to @title', array('@title' => $node->title)) . " &raquo;", 'node/' . $node->nid, array('html' => TRUE));
  
  print theme('utdanning_rdf_lightbox', $media, $title, $content, $link);
}

function template_preprocess_utdanning_rdf_lightbox(&$variables) {
  $variables['styles'] = drupal_get_css();
  $variables['scripts'] = drupal_get_js();
  $variables['head'] = drupal_get_html_head();
  $variables['closure'] = theme('closure');
}

/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for nodes to relate to.
 */
function utdanning_rdf_node_title_autocomplete($node = NULL, $type='', $string = '') {
  if(!module_exists('og')) {
    die(t("Missing OG module"));
  }
  $matches = array();

  // if nothing is typed in the textfield skip everyting
  if (!empty($string)) {
    $grader_vid = 14; // The id of the "vanskelighetsgrad" vocabulary

    // Populate the array grader_tids with the term names keyed by the tid
    $grader = taxonomy_get_tree($grader_vid);
    $grader_tids = array();
    foreach ($grader as $vid => $temp){
      $grader_tids[$temp->tid] = $temp->name;
    }

    // Get the id of the content type vocabulary
    if (function_exists('ndla_utils_get_content_type_vocab')) {
      $ct_vid = ndla_utils_get_content_type_vocab();
    }

    $types_where = "";
    //Pickup the current nid
    $nid = check_plain(arg(1));
    $perform_ct_filter = FALSE;

    /* Get allowed link types */
    $link_types = array();
    if(module_exists('ndla_group_access') && module_exists('ndla_utils') && is_numeric($nid)) {
      $nids = array();
      $result = db_query("SELECT og.group_nid FROM {node} n INNER JOIN {og_ancestry} og ON og.nid = n.nid WHERE n.nid = %d", $nid);
      while($row = db_fetch_object($result)) {
        $nids[] = $row->group_nid;
      }
      
      foreach($nids as $node_id) {
        $node = ndla_utils_load_node($node_id);
        $group_access = _ndla_group_access_load($node);        
        $link_types = array_merge($group_access['ndla_group_access']['enabled_relations'], $link_types);
      }
    }
    
    $remove_cts_where = "";
    if($remove_cts = utdannig_rdf_from_ct_to_cts($type, $link_types)) {
      $remove_cts_where = " AND n.type IN('" . implode("','", array_keys($remove_cts)) . "') ";
    }
    else {
      //We should never get here because the form shouldnt even be visible when
      //no content types are selected at admin/settings/rdf/utdanning_rdf
      $remove_cts_where = " AND n.nid IS NULL ";
    }

    // Retrieve the 15 first nodes matching the search string.
  	$result = db_query("SELECT nid, vid, title, language FROM {node} n WHERE title LIKE '%s%' $remove_cts_where ORDER BY title ASC LIMIT 15", $string);
    
    while ($node = db_fetch_object($result)) {
      $ctype_name = '';
      $csubtype_name = '';
      $node_grader_tid = 0;

      // Load all terms for the node.
      $terms = taxonomy_node_get_terms($node);

      // Fetch content type from taxonomy
      foreach ($terms as $tid => $tax) {
        if ($ct_vid && ($tax->vid == $ct_vid)) {
          if (count(taxonomy_get_parents($tid)) == 0) {
            $ctype_name = $tax->name;
          }
          else {
            $csubtype_name = ': ' . $tax->name;
          }
        }
        else if ($tax->vid == $grader_vid) {
          $node_grader_tid = $tid;
        }
      }
      // TODO: filter out already related nodes.
      $output = check_plain($node->title) . ' (' . $node->nid . ')' . ' ['. $ctype_name . $csubtype_name . ']';
      // Add language, if any
      $output .= $node->language ? ' ['. $node->language. ']' : '';	
      // Add difficulty level, if any
      $output .= $node_grader_tid ? ' [' . $grader_tids[$node_grader_tid] . ']' : '';

      $matches[$node->title . ' ' . '[nid:' . $node->nid . ']'] = $output;
    }
  }
  drupal_json($matches);  
}

/**
 * Retrive a node based on title
 *
 * @param string $title
 * @return object node
 */
function utdanning_rdf_node_title_lookup($title) {
  return node_load(array('title' => $title));
}

/**
 * Store a new relation when a user pushes the "Add relation" button.
 * And return json to the page, so the new relation gets visable in the form
 *
 */
function utdanning_rdf_node_relation_create($node_title = '') {
 // The form is generated in an include file which we need to include manually.
  require_once drupal_get_path('module', 'node') . '/node.pages.inc';
  // We're starting in step #3, preparing for #4.
  $form_state = array('storage' => NULL, 'rebuild' => TRUE);
  $form_build_id = $_POST['form_build_id'];

  // Step #4.
  $form = form_get_cache($form_build_id, $form_state);

  $args = $form['#parameters'];

  //The post thing is a fix for auto selection of relation when there is only one relation. The form hasnt been rebuilt so the correct title isnt in the form yet.
  $node_title = !empty($_POST['utdanning_rdf_table']['title']) ? $_POST['utdanning_rdf_table']['title'] : check_plain($form_state['post']['utdanning_rdf_table']['title']);
  $type = check_plain($form_state['post']['utdanning_rdf_table']['nodetype']);

  preg_match("/\[nid:([0-9]{0,9})\]/", $node_title, $matches); // parse the autocomplete field and get the nid

  // Get node data the fast way
  $related_node = ndla_utils_load_node($matches[1]);

  /**
   * End of our changes.
   */

  // Preparing for #5.
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  utdanning_rdf_rebuild_form_state($form, $form_state);
  if($related_node->nid) {
    $form_state['post']['utdanning_rdf_table']['existing'][$related_node->nid] = array('nid' => $related_node->nid, 'title' => $node_title, 'weight' => 100);
    //Make the within checkbox default to checked.
    $form_state['utdanning_rdf_within'][] = $related_node->nid;
  }
  // Step #5.
  drupal_process_form($form_id, $form, $form_state);
  // Step #6 and #7 and #8.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  // Step #9.
  $utdanning_rdf_form = $form['utdanning_rdf']['utdanning_rdf_table'];
  unset($utdanning_rdf_form['#prefix'], $utdanning_rdf_form['#suffix']);
  $output = drupal_render($utdanning_rdf_form);

  //pick up validation errors. Dont show them - they are not of interest to us (at this point)
  $errs = theme('status_messages');
  
  // Final rendering callback.
  print drupal_to_js(array('status' => TRUE, 'data' => $output));	
  exit();
}

function utdanning_rdf_rebuild_form_state($form, &$form_state) {
  $form_state['utdanning_rdf_within'] = array();
  if(is_array($form_state['post']['utdanning_rdf_within'])) {
    foreach($form_state['post']['utdanning_rdf_within'] as $key => $value) {
      $form_state['utdanning_rdf_within'][] = $value;
    }
  }
}

/**
 * Removes a relation in the node edit form. 
 *
 * @param $related_node
 *  The node id which we no longer want to be related to.
 */
function utdanning_rdf_node_relation_delete($related_node) {
  // The form is generated in an include file which we need to include manually.
  require_once drupal_get_path('module', 'node') . '/node.pages.inc';
  // We're starting in step #3, preparing for #4.
  $form_state = array('storage' => NULL, 'rebuild' => TRUE);
  $form_build_id = $_POST['form_build_id'];

  // Step #4.
  $form = form_get_cache($form_build_id, $form_state);

  // Preparing for #5.
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form_state['deleted'] = array($related_node => $related_node);
  $form['#programmed'] = $form['#redirect'] = FALSE;
  utdanning_rdf_rebuild_form_state($form, $form_state);
  unset($form_state['post']['utdanning_rdf_table']['existing'][$related_node->nid]);
  // Step #5.
  drupal_process_form($form_id, $form, $form_state);
  // Step #6 and #7 and #8.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  // Step #9.
  $utdanning_rdf_form = $form['utdanning_rdf']['utdanning_rdf_table'];
  unset($utdanning_rdf_form['#prefix'], $utdanning_rdf_form['#suffix']);
  $output = drupal_render($utdanning_rdf_form);
  //pick up validation errors. Dont show them - they are not of interest to us (at this point)
  $errs = theme('status_messages');
  
  // Final rendering callback.
  drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}

function _utdanning_rdf_get_reverse_for_form($node) {
  if(!module_exists('og')) {
    die(t("Missing OG module"));
  }
  //jonas
  $data = array();
  $results_reverse = rdf_query(NULL, NULL, "nid:".$node->nid, utdanning_rdf_rdf_options())->to_array();
  $counter = 0;
  $disabled = FALSE;
  if(!og_is_group_post_type($node->type) && !og_is_group_type($node->type)) {
    $disabled = TRUE;
  }

  foreach($results_reverse as $index => $triplet) {
    $vid = str_replace("vid:", "", $triplet[0]);
    $row = db_fetch_object(db_query("SELECT n.nid, n.vid, n.title, n.language, n2.title as fag_name FROM {node_revisions} nr INNER JOIN {node} n ON n.nid = nr.nid LEFT JOIN {og_ancestry} oga ON oga.nid = n.nid
LEFT JOIN {node} n2 ON oga.group_nid = n2.nid WHERE n.vid = %d LIMIT 1", $vid));
    if(!$row->nid) {
      continue;
    }
    $triplet[0] = "nid:".$row->nid;
    $relation = md5($triplet[0].utdanning_rdf_get_inverse($triplet[1]).$triplet[2]);
    $within = utdanning_rdf_get_within($node->nid, $relation, $row->nid, TRUE);

    if(!isset($within['weight']) || !is_numeric($within['weight'])) {
      $within['weight'] = 100;
    }

    // Retrieve content type and sub content type from the related node's taxonomy
    $ctype_name = '';
    $csubtype_name = '';
    if (function_exists('ndla_utils_get_content_type_vocab')) {
      $ct_vid = ndla_utils_get_content_type_vocab();
      if ($ct_vid) {
        $terms = taxonomy_node_get_terms_by_vocabulary($row, $ct_vid);
        // Fetch content type from taxonomy
        foreach ($terms as $tid => $tax) {
          // Retrieve the name of the parent vocabulary for the content type tax
          if ($tax->vid == $ct_vid) {
            if (count(taxonomy_get_parents($tid)) == 0) {
                $ctype_name = $tax->name;
            }
            else {
              $csubtype_name = $tax->name;
            }
          }
        }
      }
    }

    $data[$counter] = $triplet;
    $data[$counter]['incoming'] = 1;
    $data[$counter]['within'] = $within['within'];
    $data[$counter]['relation'] = $relation;
    $data[$counter]['language'] = $row->language;
    $data[$counter]['relation_name'] = utdanning_retrive_label(utdanning_rdf_get_inverse($triplet[1]));
    $data[$counter]['node_type'] = $ctype_name;
    $data[$counter]['node_subtype'] = $csubtype_name;
    $data[$counter]['fag_name'] = $row->fag_name;
    $data[$counter]['title'] = l($row->title, 'node/' . $row->nid, array('attributes' => array('target' => '_blank')));
    $data[$counter++]['weight'] = $within['weight'];
  }
  
  unset($results_reverse);
  return $data;
}

/**
 * Node form elements used to relate nodes
 *
 * @param array $form_state
 * @param object $node
 * @param boolean $is_embedded
 * @return array Form elements
 */
function utdanning_rdf_node_form($form_state, $node, $is_embedded = FALSE) {
  $can_manage = FALSE;
  if(!is_array($node->utdanning_rdf)) {
    $node->utdanning_rdf = array();
  }
  
  $path = ($node->nid) ? $node->nid : 'add';
  $form['#node'] = $node;
  $form['#prefix'] = "<center><h3>".t('Relations')."<h3></center>";
  $form['#prefix'] .= '<div class="form-item" id="utdanning_rdf-table">';
  $form['#suffix'] = $can_manage ? '<div class="description">' . utdanning_rdf_help('node/%/utdanning_rdf#table') . '</div></div>' : '</div>';
  $form['#theme'] = 'utdanning_rdf_node_form';
  $form['#tree'] = TRUE;
  $form['title'] = array(
    '#type'   => 'textfield',
    '#size'   => 32,
    '#maxlength' => 255,
    '#autocomplete_path' => 'node/' . $path . '/utdanning_rdf/autocomplete/'.$node->type,
      '#ahah'   => array(
        'path'    => 'node/add/utdanning_rdf/create',
        'wrapper' => 'utdanning_rdf-table',
        'method'  => 'replace',
        'effect'  => 'fade',
        'event'   => 'blur'
      ),
    );

	$form['nodetype'] = array(
		"#type" => 'hidden',
		"#value" => $node->type
	);

  $form['nid'] = array(
    '#type'   => 'hidden',
    '#value'  => !empty($node->nid) ? $node->nid : '',
  );
    
  //Populate the form with existing relations.
  if(!$form['existing']) {
    $form['existing'] = array('#tree' => TRUE);
    if(count($form_state['post'])) {
      _utdanning_rdf_form_state_sort($form_state);
      foreach($form_state['post']['utdanning_rdf_table']['existing'] as $data) {
        $nid = isset($data['nid']) ? $data['nid'] : "incoming_" . $data['relation'];
        //Outgoing relations - deletable
        if(strpos($nid, "incoming_") === FALSE && empty($data['incoming'])) {
          utdanning_rdf_create_row($form, $form_state, ndla_utils_load_node($nid), $data['type'], $data['within'], $data['weight']);
        }
        //Incoming relations, cant delete these.
        else {
          utdanning_rdf_create_reverse_row($form, $form_state, $data, $data['within'], $data['weight']); 
        }
      }
    }
    else {
      $reverse_relations = _utdanning_rdf_get_reverse_for_form($node);
      $node->utdanning_rdf = _utdanning_rdf_sort_relations(array_merge($node->utdanning_rdf, $reverse_relations));
      foreach ($node->utdanning_rdf as $index => $related) {
        if(isset($related['incoming'])) {
          utdanning_rdf_create_reverse_row($form, $form_state, $related, $related['within'], $related['weight']); 
        }
        else {
          $within = utdanning_rdf_get_within($node->nid, md5($related[0].$related[1].$related[2]));
          $n = ndla_utils_load_node(str_replace("nid:", "", $related[2]));
          utdanning_rdf_create_row($form, $form_state, $n, $related[1], $within, $weight);
        }
      }
    }
  }
  
  if(isset($form_state['utdanning_rdf_within'])) {
    foreach($form_state['utdanning_rdf_within'] as $key => $value) {
      $form['utdanning_rdf_within'][$value] = array(
        '#type' => 'hidden',
        '#value' => $value
        );
    }
  }
  
  return $form;
}

function utdanning_rdf_create_row(&$form, $form_state, $related_node, $relation, $within, $weight) {
  $node = $form['#node'];
  
  $form['existing'][$related_node->nid] = array(
    '#tree' => TRUE,
    'nid' => array(
      '#type' => 'hidden',
      '#value' => $related_node->nid
    ),
    'title' => array(
      '#type' => 'hidden',
      '#value' => $related_node->title,
    ),
    'type' => array(
      '#type' => 'select',
      '#options' => array_filter(utdanning_rdf_get_predicates(TRUE)),
      '#default_value' => $relation,
    ),
    'weight' => array(
      '#type' => 'weight',
      '#attributes' => array(
        'class' => 'my-elements-weight',
      ),
      '#delta' => 100,
      '#default_value' => $weight,
    ),
    'within' => array(
      '#type' => 'hidden',
      '#value' => ($within > 0) ? $related_node->nid : 0,
      )
  );
  
  if(module_exists('ndla_group_access')) {
    $form['existing'][$related_node->nid]['type']['#options'] = ndla_group_access_filter_relations($form['existing'][$related_node->nid]['type']['#options'], $node->type, $related_node->type, $relation, $node);
  }
}

function utdanning_rdf_create_reverse_row(&$form, $form_state, $related, $within = 1, $weight = 100) {
  $node = $form['#node'];
  $nid = str_replace("nid:", "", $related[0]);
  $form['existing']['incoming_' . $related['relation']] = array(
    '#tree' => TRUE,
    'relation' => array(
      '#type' => 'hidden',
      '#value' => $related['relation'],
    ),
    'relation_name' => array(
      '#type' => 'hidden',
      '#value' => $related['relation_name'],
    ),
    'title' => array(
      '#type' => 'hidden',
      '#value' => $related['title'],
    ),
    'type' => array(
      '#type' => '#value',
      '#value' => $related['relation_name'],
    ),
    'weight' => array(
      '#type' => 'weight',
      '#attributes' => array(
        'class' => 'my-elements-weight',
      ),
      '#default_value' => $weight,
    ),
    'language' => array(
      '#type' => 'value',
      '#value' => $related['language'],
    ),
    
    'within' => array(
      '#type' => 'hidden',
      '#value' => $within,
      ),
    'node_type' => array(
      '#type' => 'hidden',
      '#value' => $related['node_type'],
    ),
    'node_subtype' => array(
      '#type' => 'hidden',
      '#value' => $related['node_subtype'],
    ),
    'nid' => array(
      '#type' => 'hidden',
      '#value' => $nid,
    ),
    'incoming' => array(
      '#type' => 'hidden',
      '#value' => TRUE,
    ),
  );

}

/**
 * Validate the related values that are submited
 *
 * @param array $form
 * @param array $form_state
 */
function utdanning_rdf_node_form_validate($form, &$form_state) {
  // @todo need to revisit this function
  
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  if (empty($title)) {
    return; // when editing a node and using the embedded utdanning_rdfhips form
  }
  else if (valid_url($title, TRUE)) {
    //$title = preg_replace('!https://!', '!http://!', $title);
    if (strpos($title, $GLOBALS['base_url']) !== 0) { // the URL should begin with $base_url
      form_set_error('title', t('Not a valid local URL: %url.', array('%url' => $title)));
    }

    $url = drupal_get_normal_path(substr($title, strlen($GLOBALS['base_url']) + 1));
    if (!preg_match('!node/(\d+)!', $url, $matches) || !($node2 = node_load((int)$matches[1]))) {
      form_set_error('title', t('Not a valid content URL: %url.', array('%url' => $url)));
    }
  }
  else if (!($node2 = utdanning_rdf_node_title_lookup($title))) {
    form_set_error('title', t('No matching content titled %title found.', array('%title' => $title)));
  }
  else if (!node_access('update', node_load($nid)) && !node_access('update', $node2)) {
    form_set_error('title', t('Your current access privileges prohibit the creation of this utdanning_rdfhip.'));
  }
}

/**
 * Adds js and css to node edit form
 *
 * @param array $form
 */
function theme_utdanning_rdf_node_form($form) {
  drupal_add_css(drupal_get_path('module', 'utdanning_rdf') . '/utdanning_rdf.css', 'module', 'all', FALSE);
  drupal_add_js(drupal_get_path('module', 'utdanning_rdf') . '/js/utdanning_rdf.js');
  drupal_add_js("Drupal.behaviors.utdanning_rdf = function (context) {make_utdanning_rdf_browser_button(); };", "inline");
  return theme('utdanning_rdf_node_table', $form, FALSE);
}

function _utdanning_rdf_get_difficulty($node) {
  $difficulty = '';
  
  static $grader = array();
  static $grader_tids = array();
  
  if(!count($grader)) {
    $grader = taxonomy_get_tree(14); // loads "vanskelighetsgrad" taxonomy (vid=14)
    $grader_tids = array(); // all tids and term names

    foreach ($grader as $vid => $temp){
    	$grader_tids[$temp->tid] = $temp->name;
    }
  }
  
  $node_taxonomy = taxonomy_node_get_terms($node);
  foreach ($grader_tids as $tid => $name){
    if(array_key_exists($tid, $node_taxonomy)){
      $difficulty = $name;
      break;
    }
  }
  
  return $difficulty;
}

/**
 * This function builds the table with the related nodes
 *
 * @param array $form
 */
function theme_utdanning_rdf_node_table($form) {
  global $user;
  $is_new_node = empty($form['nid']['#value']);
  $is_embedded = !empty($form['embedded']['#value']);
  $nids= array();
  $head = array(t('Within subject'), t('Title'), t('Nid'), t('Type'),t('Subtype'),t('Language'), t('Difficulty'), t('Related'), t('Operations'));
  $rows = array();

  $related_nodes = array();
  $counter = 0;
  //Pick up the existing relations.
  if($form['existing'] && count($form['existing']) > 0) {
    foreach(element_children($form['existing']) as $rel_nid) {
      $data = $form['existing'][$rel_nid];
      $checked = $data['within']['#value'] > 0 ? ' checked ' : '';
      if(isset($form['utdanning_rdf_within'])) {
        $checked = isset($form['utdanning_rdf_within'][$rel_nid]) ? ' checked ' : '';
      }

      if(is_numeric($rel_nid)) {
        $related = ndla_utils_load_node($rel_nid);
        $type_data = ndla_utils_get_node_content_type(NULL, $related->nid, $related->vid);
        $ctype_name = $type_data['main']['name'];
        $csubtype_name = $type_data['sub']['name'];
        
        $related_nodes[$rel_nid] = array(
          'type' =>  $form['existing'][$rel_nid]['type']['#value'], 
          'title' => l($related->title, 'node/' . $related->nid, array('attributes' => array('target' => '_blank'))),
          'nid' => $rel_nid,
          'within' => $form['existing'][$rel_nid]['within']['#value'],
          'weight' => $form['existing'][$rel_nid]['weight']['#value'],
          'language' => $related->language,
          'node_type' => $ctype_name,
          'node_subtype' => $csubtype_name,
          'difficulty' => _utdanning_rdf_get_difficulty($related),
          //$disable_within
          'checkbox' => "<input name='utdanning_rdf_within[]' value='".$related->nid."' $checked type='checkbox' />",
          'relation_name' => drupal_render($form['existing'][$related->nid]['type']),
          'delete_link' => theme_utdanning_rdf_delete_link(NULL, $related->nid, $is_embedded)
        );
      }
      else if(strpos($rel_nid, 'incoming_') !== FALSE) {
        $related_nodes[$rel_nid] = array(
          'title' => $data['title']['#value'],
          'nid' => $data['nid']['#value'],
          'language' => $data['language']['#value'],
          'weight' => $data['weight']['#value'],
          'within' => $data['language']['#value'],
          'relation_name' => $data['relation_name']['#value'],
          'node_type' => $data['node_type']['#value'],
          'node_subtype' => $data['node_subtype']['#value'],
          'language' => $data['language']['#value'],
          //$disable_within
          'checkbox' => "<input maxlength='128' name='utdanning_rdf_within[]' value='$rel_nid' $checked type='checkbox' />",
          'delete_link' => '',
        );
      }
    }
  }

  //Populate the rows with relations.
	if($related_nodes) {
    foreach ($related_nodes as $nid => $data) {     
      $rows[] = array(
        'data' => array(
          $data['checkbox'],
          $data['title'],
          $data['nid'],
          $data['node_type'],
          $data['node_subtype'],
          $data['language'],
          $data['difficulty'],
          $data['relation_name'],
          drupal_render($form['existing'][$nid]['weight']),
          $data['delete_link'],
        ),
        'class' => 'draggable',
      );
    }
  }
  
  unset($related_nodes);
  drupal_add_tabledrag('utdanning_rdf_draggable', 'order', 'sibling', 'my-elements-weight');
  return drupal_render($form) . theme('table', $head, $rows, array('class' => 'utdanning_rdf', 'id' => 'utdanning_rdf_draggable'));
}

/**
 * Adds the delete link to each relationship in the node edit form
 *
 * @param string $nid1
 * @param string $nid2
 * @param boolean $is_embedded
 */
function theme_utdanning_rdf_delete_link($nid1, $nid2, $is_embedded = TRUE) {
  $path = 'node/add/utdanning_rdf/delete/' . $nid2;
  return l('<i class="fa fa-times"></i>', $path, array(
    'html' => TRUE,
    'attributes' => array(
      'id'      => 'edit-utdanning_rdf-delete-' . $nid2,
      'onclick' => 'return Drupal.utdanning_rdfTriggerAHAH(this);',
      'title' => t('Delete'),
    ),
  ));
}

/**
 * Themes the block that displays referances
 *
 * @param object $node
 * @param array $options
 */
function theme_utdanning_rdf_node_block($blocks = array()) {
	drupal_add_css(drupal_get_path('module', 'utdanning_rdf') .'/utdanning_rdf.css', 'module', 'all', FALSE);
  $content = '';
  foreach($blocks as $key => $data) {
    if ($key != 'subject') {
      $content .= $data;
    }
  }
  return $content;
}

/**
* Returns formated HTML for the RSS block
*
* @param $nodes
*   The nodes
* @return
*   Formatted HTML.
*/
function theme_utdanning_rdf_node_rss($node) {
  //if utdanning_feedapi_item isnt available return empty string
  if(!module_exists('utdanning_feedapi_item')) {
    return '';
  }

  $output = _utdanning_feedapi_item_load($node['node'], FALSE, FALSE, 5);
  return $output;
}

/**
 * Themes the node listing in the block.
 *
 * @param array $nodes
 * @return string HTML
 */
function theme_utdanning_rdf_node_list($nodes, $context = NULL) {
  $sub_theme = array();
  $context_id = ndla_utils_disp_get_context_value('course');
  if (variable_get("ndla_utils_show_relations_thumbnails_" . $context_id, 0)) {
    $sub_theme['flashnode'] = "utdanning_rdf_node_media";
    $sub_theme['video'] = "utdanning_rdf_node_media";
    $sub_theme['image'] = "utdanning_rdf_node_media";
    $sub_theme['audio'] = "utdanning_rdf_node_media";
  }
  $sub_theme['rss'] = "utdanning_rdf_node_rss";
  $relations = array();
  	
  $out_data = _utdanning_rdf_group_relations($nodes);

  if (empty($out_data)) {
    return '';
  }
  
  ksort($out_data);
  $relations = array();
  $counter = 0;
  foreach ($out_data as $content_type => $holder) {
    ksort($holder);
    foreach ($holder as $rel_name => $data) {
      $rel_name = check_plain($rel_name);
      // We add node title as key to make sorting simple
      foreach ($data as $key => $node) {
        $data[] = $node;
        unset($data[$key]);
      }
      
      ksort($data);
      foreach ($data as $node) {        
        if(!isset($sub_theme[$node['node']->type])) {
          $relations[$rel_name][$counter]['content'] = theme('utdanning_rdf_standard_block_node', $node, $context, ($counter % 2));
          $relations[$rel_name][$counter]['weight'] = $node['node']->weight;
        }
        else {
          $relations[$rel_name][$counter]['content'] .= theme($sub_theme[$node['node']->type], $node, $context, ($counter % 2));
          $relations[$rel_name][$counter]['weight'] = $node['node']->weight;
        }
        $counter += 1;
      }
    }
  }

  $output = '<div class="relatedContent">';
  $first = ' class="first"';
  $related_nodes = '';
  foreach($relations as $name => $nodes) {
    _utdanning_rdf_sort_for_block($nodes);
    //The labels isnt translated via Drupal
    if($name == 'Related' || $name == 'Relatert') {
      //$related_nodes .= "<h3$first>$name</h3><ul>".implode("\n", $nodes)."</ul>";
      $related_nodes .= "<h3$first>" . t('General') . "</h3>";
      $related_nodes .= "<ul>".implode("\n", $nodes)."</ul>";
    }
    else {
      $output .= "<h3$first>$name</h3><ul>".implode("\n", $nodes)."</ul>";
    }
    $first = '';
  }
  $output .= $related_nodes . '<div class="relation_separator"></div></div>';

  return $output;
}

function _utdanning_rdf_sort_for_block(&$data) {
  $data = array_values($data);
  for($x = 0; $x < count($data); $x++) {
    for($y = 0; $y < count($data); $y++) {
      $data[$x]['weight'] = (!isset($data[$x]['weight']) || !is_numeric($data[$x]['weight'])) ? 100 : $data[$x]['weight'];
      $data[$y]['weight'] = (!isset($data[$y]['weight']) || !is_numeric($data[$y]['weight'])) ? 100 : $data[$y]['weight'];
      if($data[$x]['weight'] < $data[$y]['weight']) {
        $hold = $data[$x];
        $data[$x] = $data[$y];
        $data[$y] = $hold;
      }
    }
  }
  $new = array();
  foreach($data as $d) {
    $new[] = $d['content'];
  }
  
  $data = $new;
}

function theme_utdanning_rdf_standard_block_node($node, $context = NULL, $odd = TRUE) {
  static $types;
  if(!$types) {
    $types = node_get_types();
  } 
  $sub_type = ($node["node"]->taxonomy_sub_type ? ': ' . $node["node"]->taxonomy_sub_type : ($node["node"]->sub_content_type ? ': ' . $node["node"]->sub_content_type : ''));
  $out = '';
  $thumbnail = '';
  $context_id = ndla_utils_disp_get_context_value('course');
  if ($node['node']->iid && (variable_get("ndla_utils_show_relations_thumbnails_" . $context_id, 0))) { // Hvis noden har et illustrajonsbilde, vis det.
    $bildenode = node_load($node['node']->iid);
    $thumbnail = $bildenode->images['preview'];
  }

  if (empty($thumbnail)) {
    $out .= '<li><a href="' . url('node/' . $node['node']->nid) . '">';
  }
  else {
    $out .= '<li class="image"><a href="' . url('node/' . $node['node']->nid) . '"><img src="' . base_path() . $thumbnail . '" alt=""/>';
  }
  $out .= check_plain($node['node']->title) . '</a> ';
  

  $type = $node['node']->taxonomy_type;
  $tax_type = $node['node']->taxonomy_type;
  $tax_sub_type = $node['node']->taxonomy_sub_type;
  
  if(in_array($tax_type, array('Ekstern ressurs', 'External resource')) && in_array($tax_sub_type, array('Oppgave','Oppgåve','Task'))) {
    $out .= '(' . $tax_type . ')';
  }
  else if(empty($type) && $node['node']->type != 'oppgave') {
    $type = $types[$node['node']->type]->name;
    $out .= '(' . $type . $sub_type . ')';
  }
  else if($node['node']->type != 'oppgave' && $type != 'Oppgave') {
    $out .= '(' . $type . $sub_type . ')';
  }
  else if(!empty($sub_type)) {
    $out .= '(' . str_replace(': ', '', $sub_type) . ')';
  }
  $out .= "</li>";
  return $out;
}

/**
 * Themes media node types in related block.
 *
 * @param array $nodes
 * @return string HTML
 */
function theme_utdanning_rdf_node_media($node, $context = NULL, $odd = TRUE) {
 //Pick up all the types so we can print the normal name of the content type
  $types = node_get_types();
  $sub_type = $node["node"]->sub_content_type ? ': ' . $node["node"]->sub_content_type : '';
  $output = '';
  $query = array();
  if($context) {
    $query['fag'] = $context;
  }
  $duration = '';
  $img_link = base_path() . $node['node']->images['fullbredde'];
  $rel_type = 'lightbox[related]';
  if ($node['node']->iid) { // Hvis noden har et illustrajonsbilde, vis det.
    $bildenode = node_load($node['node']->iid);
    $thumbnail = $bildenode->images['preview'];
  }
  else {
    switch ($node['node']->type) {
      case 'audio':
        $thumbnail = drupal_get_path('module', 'utdanning_rdf') . '/img/soundThumb.gif';
        break;
      case 'flashnode':
        $thumbnail = drupal_get_path('module', 'utdanning_rdf') . '/img/flashThumb.gif';
        break;
      case 'image':
        $thumbnail = $node['node']->images['preview'];
        $link_href = base_path().$node['node']->images['fullbredde'];
        break;
      case 'video':
        //If we have an attached image
        if($node['node']->iid) {
          $img_node = node_load($node['node']->iid);
          $thumbnail = $img_node->images['preview'];
        }
        else {
          if ($node['node']->files) {
            foreach($node['node']->files as $f) {
              if($f->filemime == 'jpg')  {
                $thumbnail = $f->filepath . '" width="58';
                break;
              }
            }
          }
        }
        $link_href = url('node/' . $node['node']->nid, array('query' => $query));
        break;
      default:
        break;
    }
  }
  
  if(empty($thumbnail)) {
    if(empty($thumbnail)) {
      //Pickup the CCK fields and look for ingressbilde
      content_load($node['node']);
      if(isset($node['node']->field_ingress_bilde) && count($node['node']->field_ingress_bilde) && is_numeric($node['node']->field_ingress_bilde[0]['nid'])) {
        $image_node->nid = $node['node']->field_ingress_bilde[0]['nid'];
        image_load($image_node);
        if(isset($image_node->images) && !empty($image_node->images['preview'])) {
          $thumbnail = $image_node->images['preview'];
        }
      }
    }
  }
  
  // Image
  if(empty($thumbnail)) {
    $output .= '<li><a href="' . url('node/' . $node['node']->nid) . '">';
  }
  else {
    $output .= '<li class="image"><a href="' . url('node/' . $node['node']->nid) . '"><img src="' . base_path() . $thumbnail . '" alt=""/>';
  }
  
  $type = $node['node']->taxonomy_type;
  if(empty($type)) {
    $type = $types[$node['node']->type]->name;
  }
  // Heading
  if(empty($node['node']->taxonomy[70936])) { /* Oppgave */
    $output .= check_plain($node['node']->title) . '</a> (' . $type . $sub_type . ')';
  } else {
    $output .= check_plain($node['node']->title) . '</a> (' . str_replace(': ', '', $sub_type) . ')';
  }
  
  if ($duration) {
    $output .= $duration;
  }
  // Description
  $teaser = check_markup($node['node']->teaser);
  if($teaser == 'n/a') {
    $teaser = '';
  }
  $output .= substr($teaser, 0, 60) . (strlen($teaser) > 60 ? '...' : '');
  
  $output .= '</li>';
  return $output;
}

function _utdanning_rdf_group_relations($nodes) {
	if(empty($nodes)) {
	  return;
	}

	global $language;
	$node_lang = $language->language;
	
	//Get the current node language
	if(is_numeric(arg(1))) {
	  //Do a query instead of node_load
	  $sql = "SELECT language FROM {node} WHERE nid = %d";
	  $result = db_query($sql, check_plain(arg(1)));
	  $row = db_fetch_object($result);
	  $node_lang = $row->language;
    //If bokmål - dont show nynorsk nodes
    if($node_lang == 'nb') {
      $dont_show_lang = array('nn');
    }
    //If nynorsk - dont show bokmål
    else if($node_lang == 'nn') {
      $dont_show_lang = array('nb');
    }
    
    //If language neutral
    else if(empty($node_lang)) {
      //english only english and lang. neutral
      //bokmål bare bokmål and lang. neutral
      //nynorsk bare nynorsk and lang. neutral
      if($language->language == 'nn') {
        $dont_show_lang = array('nb', 'en');
      }
      else if($language->language == 'nb') {
        $dont_show_lang = array('nn', 'en');
      }
      else if($language->language == 'en') {
        $dont_show_lang = array('nb', 'nn');
      }
    }
	}
  
  $picked = array();
  $out_data = array();
  foreach($nodes as $node) {
    //Dont show the node if it has the same language as $dont_show_lang
    if(!empty($dont_show_lang) && in_array($node["node"]->language, $dont_show_lang)) {
      continue;
    }
    
    if(!isset($picked[$node['type']])) {
      $label = utdanning_retrive_label($node['type'], $node['node']->language);
      //No label found, try with the default language
      if(empty($label)) {
        $label = utdanning_retrive_label($node['type']);
      }
      $picked[$node['type']] = $label;
    }

    if (function_exists('ndla_utils_get_content_type_vocab')) {
      $ct_vid = ndla_utils_get_content_type_vocab();
      if ($ct_vid) {
        foreach ($node['node']->taxonomy as $tid => $tax) {
          $vid = $tax->vid;
          if (($vid == $ct_vid) && (count(taxonomy_get_parents($tax->tid)) > 0)) {
            $node['node']->sub_content_type = $tax->name;
          }
        }
      }
    }

    $out_data[ $node['node']->type] [$picked[$node['type']]] [] = $node;
  }

  /* Making things even more interesting, this is the way we are going to sort the content types
  00  Veiledning - veiledning
  01  Oppgave - oppgave
  02  Flervalg - multichoice
  03  Quiz - quiz
  04  Test - test
  05  Interaktiv oppgave (amendor) - amendor-ios-task
  06  Interaktivt oppgavesett (amendor) - amendor-ios
  07  Fagstoff - fagstoff
  08  Lenke - lenke
  09  RSS - rss
  10  Vedlegg - fil
  11  eforelsesning - amendor-electure
  12  Bilde - image
  13  Video - video
  14  Flash - flashvideo
  15  Lyd - audio
  */ 

  $sorting = array(
    'veiledning' => array(),
    'oppgave' => array(),
    'multichoice' => array(),
    'quiz' => array(),
    'test' => array(),
    'amendor-ios-task' => array(),
    'amendor-ios' => array(),
    'fagstoff' => array(),
    'lenke' => array(),
    'rss' => array(),
    'fil' => array(),
    'amendor-electure' => array(),
    'image' => array(),
    'video' => array(),
    'flashnode' => array(),
    'audio' => array()
  );

  foreach($out_data as $type => $data) {
    //Sort the nodes based on their relation
    ksort($data);
    $sorting[$type] = $data;
  }
 
  //Unset all empty arrays
  foreach($sorting as $key => $data) {
    if(!count($data)) {
      unset($sorting[$key]);
    }
  }


  return $sorting;
}

function _utdanning_rdf_form_state_sort(&$form_state) {
  $new_data = array_values($form_state['post']['utdanning_rdf_table']['existing']);
  $form_state['post']['utdanning_rdf_table']['existing'] = _utdanning_rdf_sort_relations($new_data);
}
