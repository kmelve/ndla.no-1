/**
 * @file
 * @ingroup utdanning_rdf
 */
Drupal.utdanning_rdfTriggerAHAH = function(link) {
  $('#' + link.id).attr('disabled', true);
  if (!$('#' + link.id + '.ahah-processed').size()) {
    $('#' + link.id).bind('click', function() { return false; });
    var ahah = new Drupal.ahah(link.id, {
      'url':      link.href,
      'event':    'click',
      'keypress': true,
      'wrapper':  'utdanning_rdf-table',
      'selector': '#' + link.id,
      'effect':   'fade',
      'method':   'replace',
      'progress': { 'type': 'throbber' },
      'button':   { 'op': link.innerText },
      'element':  link
    });
    $('#' + link.id).addClass('ahah-processed');
    setTimeout(function() { $('#' + link.id).trigger('click'); }, 100);
  }

  return false;
};

function make_utdanning_rdf_browser_button(){
  if($('#edit-utdanning-rdf-table-title-wrapper :last-child').html()==Drupal.t('Browse')){
    return false;
  }
  var url = '';
  if(typeof modulepath != 'undefined') {
    url = Drupal.settings.basePath + modulepath + '/browse.php';
  }
  var rel = 'lightmodal';
  if(Drupal.settings.has_contentbrowser) {
    url = Drupal.settings.basePath + "contentbrowser?output=drupal&nodereference=utdanning-rdf-table&oncomplete=runkode_rdf";
    rel = 'lightframe[|width:1000px;height:800px]';
  }
  var link='<a href=\"' + url + '\" rel=\"' + rel + '\">'+Drupal.t('Browse')+'</a>';
  $('#edit-utdanning-rdf-table-title-wrapper').append(link);

  Lightbox.initList();

  $('#edit-utdanning-rdf-table-title-wrapper a').click(function(e){    
    clickedid=$(this).parent().attr('id');

    //$('#lightbox').unbind('click');
    //Lightbox.start(this, false, false, false, true);
    //if (e.preventDefault) { e.preventDefault(); }
    //return false;
  });

}

function runkode_rdf(text) {
  $('#'+clickedid+' input').val(text);
  Lightbox.end('forceClose');
  $("#edit-utdanning-rdf-table-title").trigger("blur");
}

$(document).ready(function() {
  if ($('input:file').length != 0) {
    Drupal.ahah.prototype.beforeSubmitFirst = Drupal.ahah.prototype.beforeSubmit;
    Drupal.ahah.prototype.beforeSubmit = function (form_values, element, options) {
      this.beforeSubmitFirst(form_values, element, options);
      var $el = $(this.element);
      if ($el.is('#edit-utdanning-rdf-table-title')) {
        $el.attr('disabled', false);
      }
    }
  }
});
