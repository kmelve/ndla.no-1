<?php if(!(empty($within) && empty($outside))): ?>
<fieldset class='utdanning_rdf_fieldset <?php print $class; ?>'>
  <legend class='utdanning_rdf_legend'><?php print $title; ?></legend>
  <?php if($context): ?>
    <?php if(!empty($within)): ?>
      <h2><?php print t('Within subject'); ?></h2>
      <div class='within'>
        <?php print $within; ?>
      </div>
    <?php endif; // end if within?>
    <?php if(!empty($outside)): ?>
      <h2><?php print t('Interdisciplinary'); ?></h2>
      <div class='outside'>
        <?php print $outside; ?>
      </div>
    <?php endif; //end outside ?>
  <?php else: ?>
    <div class='within'>
      <?php print $within; ?>
    </div>
  <?php endif; //end if context?>
</fieldset>
<?php endif; ?>