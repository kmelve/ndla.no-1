<?php
/**
 * @file
 * @ingroup utdanning_rdf
 */
?>
<?php
  print "<head>";
  print $head;
  print $styles;
  print $scripts;
  print "</head>";
?>
<div id="lightboxContent">
<?php print $media; ?>
<!--<h1><?php print $title; ?></h1>-->
<div class="content">
<?php print $content; ?>
<p><?php print $link; ?></p>
</div>
</div>
<script type="text/javascript">
$(function() {
  //alert($("#lightboxContent").height());
  var innerHeight = parseInt($("#lightboxContent").height());
  if (innerHeight == 0) {
    return; //Something failed... This fixes bug in issue TGP-2413
  }
  var newHeight = parseInt($("#lightboxContent").height()) + parseInt($("#modalContainer").css("padding-top")) + parseInt($("#modalContainer").css("padding-bottom"));
  if ($("#audioplayer")[0]) {
    innerHeight += 20;
    newHeight += 20;
  }
  if (newHeight > 550) {
    newHeight = 550;
    innerHeight = 550 - parseInt($("#modalContainer").css("padding-top")) - parseInt($("#modalContainer").css("padding-bottom"));
  }
  $("#outerImageContainer").animate({height: newHeight + "px"}, 'fast');
  $("#modalContainer").animate({height: innerHeight + "px"}, 'fast');
});
</script>