<?php
/**
 * @file
 * @ingroup utdanning_expander
 */
?>
<?php $path = $_GET['path'] . "sites/all/libraries/tinymce/jscripts/tiny_mce/"; ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Insert</title>
	<script language="javascript" type="text/javascript" src="<?php print $path; ?>tiny_mce_popup.js"></script>
	<script language="javascript" type="text/javascript" src="<?php print $path; ?>utils/mctabs.js"></script>
	<script language="javascript" type="text/javascript" src="<?php print $path; ?>utils/form_utils.js"></script>
	<script language="javascript" type="text/javascript" src="<?php print $path; ?>utils/validate.js"></script>
	<script language="javascript" type="text/javascript" src="jscripts/functions.js"></script>
	<base target="_self" />
</head>
<form onsubmit="insertAction();return false;" action="#">

					<table border="0" cellpadding="0" cellspacing="4">
						<tr>
							<td nowrap="nowrap"><label for="synlig">Hva er synlig i utgangspunktet?</label>&nbsp;</td>
							<td>
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><textarea name="synlig" id="synlig" rows="3" cols="30" /></textarea></td>										
									</tr>
								</table>
							</td>		
						</tr>
						<tr>
							<td nowrap="nowrap"><label for="usynlig">Hva blir synlig etter klikking?</label>&nbsp;</td>
							<td>
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><textarea name="usynlig" id="usynlig" rows="5" cols="30" /></textarea></td>										
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap"><label for="lenktekst">Lenketekst for les mer</label>&nbsp;</td>
							<td>
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><input type="text" name="expand" id="expand" value="les mer"  /></td>										
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap"><label for="coltext">Lenketekst for skjul igjen</label>&nbsp;</td>
							<td>
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><input type="text" name="hide" id="hide" value="skjul"  /></td>										
									</tr>
								</table>
							</td>
						</tr>
					</table>

		<input type="button" id="insert" name="insert" value="Insert" onclick="insertAction();" />
</form>


</html>
