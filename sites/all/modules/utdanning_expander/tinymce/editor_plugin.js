/**
 * @file
 * @ingroup utdanning_expander
 */

(function() {

	tinymce.create('tinymce.plugins.UtdanningExpander', {
	/**
	 * Returns information about the plugin as a name/value array.
	 * The current keys are longname, author, authorurl, infourl and version.
	 *
	 * @returns Name/value array containing information about the plugin.
	 * @type Array 
	 */
	getInfo : function() {
		return {
			longname : 'Utdanning Expander',
			author : 'ole',
			authorurl : 'http://www.utdanning.no',
			infourl : 'http://www.utdanning.no',
			version : "1.0"
		};
	},

	init : function(editor, url) {
		// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceUtdanningExpander');
		editor.addCommand('mceUtdanningExpander', function() {
			editor.windowManager.open({
				file : url + '/pop.php?path=' + Drupal.settings.basePath,
				width : 380 + parseInt(editor.getLang('example.delta_width', 0)),
				height : 250 + parseInt(editor.getLang('example.delta_height', 0)),
				inline : 1
			}, {
				plugin_url : url, // Plugin absolute URL
				some_custom_arg : 'custom arg' // Custom argument
				});
		});
			
		editor.addButton('utdanning_expander', {
			title : 'Utdanning Expander',
			cmd : 'mceUtdanningExpander',
			image : url + "/img/utdanning_expander.gif",
		});
	},


	/**
	 * Executes a specific command, this function handles plugin commands.
	 *
	 * @param {string} editor_id TinyMCE editor instance id that issued the command.
	 * @param {HTMLElement} element Body or root element for the editor instance.
	 * @param {string} command Command name to be executed.
	 * @param {string} user_interface True/false if a user interface should be presented.
	 * @param {mixed} value Custom value argument, can be anything.
	 * @return true/false if the command was executed by this plugin or not.
	 * @type
	 */
	execCommand : function(editor_id, element, command, user_interface, value) {
		function insertDiv() {
			var html = '<div class="hide">Dette vises i feltet <a href="#" class="read-more"> Vis mer</a> <div class="details"><p>dermed kommer den skjulte teksten</p> <a class="re-collapse" href="#">Skjul</a></div></div> <p>&nbsp;</p>';
			tinyMCE.openWindow({
					file : '../../plugins/utdanning_expander/pop.html',
					width : 480 + tinyMCE.getLang('lang_advlink_delta_width', 0),
					height : 400 + tinyMCE.getLang('lang_advlink_delta_height', 0)
				},
				{
					editor_id : editor_id,
					inline : "yes",
					path : Drupal.settings.basePath
				});

				return true;
		}
		
		switch (command) {
			// Remember to have the "mce" prefix for commands so they don't intersect with built in ones in the browser.
			case "mceExpander":
				insertDiv();
				return true;
		}

		// Pass to next handler in chain
		return false;
	}
});

// Adds the plugin class to the list of available TinyMCE plugins
tinymce.PluginManager.add('utdanning_expander', tinymce.plugins.UtdanningExpander);
})();
