<?php
/**
 * @file
 * @brief
 * 	This module was used to convert old Mediabrowser to the new Content Browser format.
 *
 */

/**
 * Implementation of hook_menu().
 */
function contentbrowser_migrate_menu() {
  $items = array();

  $items['admin/settings/ndla/contentbrowser/migrate'] = array(
    'title' => t('Content Browser Migrate'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('contentbrowser_migrate_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/settings/ndla/contentbrowser/migrate/status'] = array(
    'page callback' => 'contentbrowser_migrate_status',
    'page arguments' => array(),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/ndla/contentbrowser/migrate/process'] = array(
    'page callback' => 'contentbrowser_migrate_process',
    'page arguments' => array(),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/ndla/contentbrowser/migrate/process_rmf'] = array(
    'page callback' => 'contentbrowser_migrate_process_rmf',
    'page arguments' => array(),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_form().
 */
function contentbrowser_migrate_form() {
  $form = array();
  
  drupal_add_js('misc/progress.js');

  $htm = '';
  $htm .= '<script type="text/javascript">';
  $htm .= '
function start_process(mode) {
  $("#progress").empty();
  pb = new Drupal.progressBar(\'myProgressBar\');
  pb.setProgress("0", "processing...");
  $("#progress").append(pb.element);
  uri = \'' . base_path() . 'admin/settings/ndla/contentbrowser/migrate/\' + mode;
  $.get(uri , \'\', function(data) {
    pb.setProgress("100","");
    pb.stopMonitoring();
  });
  pb.startMonitoring(\'' . base_path() . 'admin/settings/ndla/contentbrowser/migrate/status\', 500);
  return false;
}
';
  $htm .= '</script>';
  $htm .= '<br/><br/><br/>';
  $htm .= '<input type="button" value="Migrate node teaser and body" onclick="start_process(\'process\')" /><br/>';
  $htm .= '<input type="button" value="Migrate node teaser and body for Restaurant- og matfag" onclick="start_process(\'process_rmf\')" /><br/>';
  $htm .= '<div id="progress"></div>';
  $htm .= '<br/>';

  $form['content_browser_migrate'] = array('#value' => $htm );

  
  $form['#redirect'] = 'admin/settings/ndla/contentbrowser/migrate';
  return $form;
}

/**
 * Implementation of hook_form_submit().
 */
function contentbrowser_migrate_form_submit($form_id, $form) {
  
}

/**
 * Callback function to show process status.
 */
function contentbrowser_migrate_status() {
  $res = explode(";", variable_get('contentbrowser_migrate_progress', '0;'));
  print drupal_to_js(array('status' => TRUE, 'percentage' => number_format($res[0], 2), 'message' => $res[1]));
  exit();
}

/**
 * Callback function to start process for Restaurant- og matfag.
 */
function contentbrowser_migrate_process_rmf() {
  $query = "SELECT COUNT(n.nid) FROM node n INNER JOIN og_ancestry oa ON n.nid=oa.nid WHERE n.type IN ('fagstoff', 'lenke', 'oppgave', 'test', 'veiledning') AND oa.group_nid=37 AND n.nid<59777 ORDER BY n.nid DESC";
  $cc = db_result($query);
  $progress = 0.0;
  $progress_step = 100.0 / $cc;
  variable_set('contentbrowser_migrate_progress', $progress . ';starting migration of RMF nodes');

  $query = "SELECT DISTINCT n.nid FROM node n INNER JOIN og_ancestry oa ON n.nid=oa.nid WHERE n.type IN ('fagstoff', 'lenke', 'oppgave', 'test', 'veiledning') AND oa.group_nid=37 AND n.nid<59777 ORDER BY n.nid DESC";
  $dbh_nid = db_query($query);
  $count = 0;

  set_time_limit(0);
  while ($node_nid = db_result($dbh_nid)) {
    $node = node_load($node_nid);

    if ($node->status == 1) {
      // Node is published. We need to make a draft. Save current
      // vid for later
      $old_vid = $node->vid;
    }
    $node->revision = 1;
    node_save($node);
    $new_vid = $node->vid;

    if ($node->status == 1) {
      // Revert the vid in the node table to the latest one, to 
      // signal this is a draft
      db_query("UPDATE {node} SET vid=%d WHERE nid=%d", $old_vid, $node->nid);
    }
    
    // In this special case, we need to remove any connections to the current node,
    // as this info was not removed after an earlier run of the script.
    db_query("DELETE FROM {contentbrowser_relations} WHERE used_by_nid=%d", $node->nid);

    contentbrowser_migrate_process_node_revision($node_nid, $new_vid);

    variable_set('contentbrowser_migrate_progress', $progress . ';migrating RMF nodes...');
    $progress += $progress_step;
    
  }
  variable_set('contentbrowser_migrate_progress', '100.0;done migrating RMF nodes');
  exit();
}


/**
 * Callback function to start process.
 */
function contentbrowser_migrate_process() {
  $lim = " WHERE n.type IN ('fagstoff', 'lenke', 'oppgave', 'test', 'veiledning')";
  $lim .= " AND oa.group_nid != 37"; // Exclude Restaurant- og matfag
  //$lim = " WHERE nid=53029";
  $cc = db_result(db_query("SELECT COUNT(n.nid) FROM node n INNER JOIN og_ancestry oa ON n.nid=oa.nid" . $lim));
  $progress = 0.0;
  $progress_step = 100.0 / $cc;
  variable_set('contentbrowser_migrate_progress', $progress . ';starting synchronizing og nodes');
  $dbh_nid = db_query("SELECT DISTINCT n.nid FROM node n INNER JOIN og_ancestry oa ON n.nid=oa.nid " . $lim . " ORDER BY n.nid DESC");
  $count = 0;
  while ($node_nid = db_result($dbh_nid)) {
    $dbh_vid = db_query("SELECT vid FROM node_revisions WHERE nid = %d ORDER BY vid ASC", $node_nid);
    while ($node_vid = db_result($dbh_vid)) {
      set_time_limit(0);
      contentbrowser_migrate_process_node_revision($node_nid, $node_vid);
    }
    variable_set('contentbrowser_migrate_progress', $progress . ';synchronizing og nodes...');
    $progress += $progress_step;
  }
  variable_set('contentbrowser_migrate_progress', $progress . ';done synchronizing og nodes');
  exit();
}

/**
 * Function for processing a node revision.
 */
function contentbrowser_migrate_process_node_revision($node_nid, $node_vid) {
  $node = db_fetch_object(
    db_query(
      "SELECT nr.nid, nr.vid, n.type, nr.format, nr.title, nr.teaser, nr.body FROM node_revisions nr LEFT JOIN node n ON n.nid = nr.nid WHERE nr.nid = %d AND nr.vid = %d", $node_nid, $node_vid
    )
  );
  
  $debug = FALSE;
  $p_vid = db_result(db_query("SELECT vid FROM node WHERE nid = %d", $node_nid));
  
  if ($debug == TRUE) {
    if ($p_vid == $node_vid) {
      $dumpfile = '/home/karl/ndla/temp/node_' . $node_nid . '_' . $node_vid;
      if (!file_exists($dumpfile . '.html')) {
        file_put_contents($dumpfile . '.html', $node->body);
      } else {
        $node->body = file_get_contents($dumpfile . '.html');
      }
    }
  }
  
  contentbrowser_migrate_process_umb_tags($node);
  contentbrowser_migrate_process_umb_subtext_tags($node);
  //contentbrowser_migrate_process_stray_img_tags($node);
  contentbrowser_migrate_process_content_browser_inserts($node);
  
  // update teaser and write to db
  $node->teaser = node_teaser($node->body, $node->format);
  db_query("UPDATE node_revisions SET teaser = '%s', body = '%s' WHERE nid = %d AND vid = %d", $node->teaser, $node->body, $node_nid, $node_vid);

  _contentbrowser_save_relationship($node);

  if ($debug == TRUE && $p_vid == $node_vid) {
    file_put_contents($dumpfile . '.phtml', $node->body);
  }
  
  error_log('contentbrowser_migrate_process_node updated node nid=' . $node_nid . ', vid=' . $node_vid);
}

/**
 * Function for processing umb tags
 */
function contentbrowser_migrate_process_umb_tags(&$node) {
  // Map old image sizes to new image cache sizes
  static $img_size_map = array(
    'Thumbnail' => 'Liten',
    'normal' => 'Hoyrespalte',
    'fullbredde' => 'Fullbredde',
    'hovedspalte' => 'Hovedspalte',
  );

  $pattern = '/<div([^>]+class="(?:UMB_(?!subtext)\w+\s*[^\"]*)"[^>]*)>\s*(?:<(i|b|p)>)?\s*<a[^>]*><\/a>.*?(?:<\/\2>)?/si';
  $pma = preg_match_all($pattern, $node->body, $matches);
  if ($pma) {
    $cc = count($matches[0]);
    for ($k = 0; $k < $cc; $k++) {
      $node->body = str_replace($matches[0][$k], '<div>', $node->body);
    }
  }

  $matches = array();
  $pattern = '/<div([^>]+class="(?:UMB_(?!subtext)\w+\s*[^\"]*)"[^>]*)>\s*(?:<(i|b|p)>)?\s*((?:<a.*?><img.*?<\/a>|<input.*?<\/script><\/span>).*?(?:<\/\2>)?)/si';

  $pma = preg_match_all($pattern, $node->body, $matches);
  if ($pma === FALSE || $pma === 0) {
    return;
  }

  $cc = count($matches[0]);
  for ($k = 0; $k < $cc; $k++) {
    // prevent double migration
    if (strpos($matches[3][$k], '[contentbrowser') === 0 || strpos($matches[3][$k], '[node') === 0) {
      continue;
    }
    
    // find type and id from tag
    $id_pattern = '/id="(.+)_([0-9]+)"/';
    preg_match($id_pattern, $matches[1][$k], $id_matches);
    if (count($id_matches) != 3) {
      continue;
    }
    $target_type = $id_matches[1];
    $target_nid = $id_matches[2];

    $atts = array();
    $atts['nid'] = $target_nid;

    $atts['link_text'] = '';

    // find text align
    if (strpos($matches[1][$k], 'UMB_float_left') !== FALSE) {
      $atts['text_align'] = 'left';
    }
    if (strpos($matches[1][$k], 'UMB_float_right') !== FALSE) {
      $atts['text_align'] = 'right';
    }

    switch ($target_type) {
      case 'image':
        // find image alt, width and height and preset size
        if (preg_match('/<img\s([^>]+)>/i', $matches[3][$k], $match_image) > 0) {
          if (preg_match('/alt="([^"]*)"/i', $match_image[1], $match_a) > 0) {
            $atts['alt'] = $match_a[1];
          }
          if (preg_match('/name="image\]:(?:\[[^\]]*\]:){4}\[([^"]*)"/i', $match_image[1], $match_n) > 0) {
            $size = $match_n[1];
            if ($size) {
              $atts['imagecache'] = $img_size_map[$size];
            }
          }
          if (preg_match('/width="([0-9]*)[px]*"/i', $match_image[1], $match_w) > 0) {
            if (is_numeric($match_w[1])) {
              $atts['width'] = intval($match_w[1]);
            }
          }
          if (preg_match('/height="([0-9]*)[px]*"/i', $match_image[1], $match_h) > 0) {
            if (is_numeric($match_h[1])) {
              $atts['height'] = intval($match_h[1]);
            }
          }
        }
        break;

      case 'video':
      case 'flashnode':
        // find video width and height
        if (preg_match('/' . $target_type . '\]:(?:\[[^\]]*\]:){5}\[([0-9\.]*)\]:\[([0-9\.]*)/si', $matches[3][$k], $match_content) > 0) {
          if (is_numeric($match_content[1])) {
            $atts['width'] = intval($match_content[1]);
          }
          if (is_numeric($match_content[2])) {
            $atts['height'] = intval($match_content[2]);
          }
        }
        break;

      case 'audio':
        // Nothing special to do
        break;
        
      case 'amendor_electure':
        // find preset size
        if (preg_match('/<img\s([^>]+)>/i', $matches[3][$k], $match_image) > 0) {
          if (preg_match('/name="([^"]*)"/i', $match_image[1], $match_n) > 0) {
            $size = $match_n[1];
            if ($size) {
              $atts['imagecache'] = $img_size_map[$size];
            }
          }
        }
        break;

      default:
        // And ignore everything else
        continue;
    }

    $atts['css_class'] = 'contentbrowser';

    $attarr = array();
    foreach ($atts as $key => $value) {
      $attarr[] = $key . '=' . $value;
    }

    // Keep style on enclosing div
    $style = '';
    $style_pattern = '/style="[^"]+"/';
    preg_match($style_pattern, $matches[1][$k], $style_matches);
    if (count($style_matches) == 1) {
      $style = ' ' . $style_matches[0];
    }

    $markup = '<div' . $style . '>[contentbrowser ==' . implode('==', $attarr) . ']';

    // replace match with markup
    $node->body = str_replace($matches[0][$k], $markup, $node->body);
  }
}

/**
 * Function for processing umb subtext tags
 */
function contentbrowser_migrate_process_umb_subtext_tags(&$node) {
  $matches = array();
  $pattern = '/(\[contentbrowser ==[^\]]+\])\s*(<div[^>]+class="UMB_subtext[^>]+>(.*?)<\/div>)/si';

  $pma = preg_match_all($pattern, $node->body, $matches);
  if ($pma === FALSE || $pma === 0) {
    return;
  }

  $cc = count($matches[0]);
  for ($k = 0; $k < $cc; $k++) {
    // Prevent removal of node inserts inside first UMB_subtext div
    if (strpos($matches[3][$k], '[contentbrowser') > 0 || strpos($matches[3][$k], '[node') > 0) {
      continue;
    }
 
    $link_text = $matches[3][$k];
    if ($link_text != "") {
      // Note: The extra . in the pattern is to not match any starting tag
      $pattern = '/(..*?)<(em|i)>/si';
      if (preg_match($pattern, $link_text, $m)) {
        $link_text = $m[1];
      }
    }

    $link_text = trim(strip_tags($link_text));
    
    $cb_tag = str_replace("link_text=", "link_text=$link_text", $matches[1][$k]);

    // replace match with markup
    $node->body = str_replace($matches[0][$k], $cb_tag, $node->body);
  }
}


/**
 * Function for processing img tags with name="[blah]:[blah]:[blah]" attribute
 */
function contentbrowser_migrate_process_stray_img_tags(&$node) {
  $matches = array();
  $pattern = '/<a.*?><img .*?<\/a>/si';
  $pma = preg_match_all($pattern, $node->body, $matches);
  if ($pma === FALSE || $pma === 0) {
    return;
  }
  $cc = count($matches[0]);
  for ($k = 0; $k < $cc; $k++) {
    if (preg_match('/name="image\]:\[.*?\]:\[(.*?)\]:\[([0-9]*)\]:\[(.*?)\]:\[([a-z]*)"(.*?)>/si', $matches[0][$k], $match_sub) > 0) {
      if (is_numeric($match_sub[2])) {
        $atts = array();
        $atts['nid'] = $match_sub[2];
        $atts['link_text'] = htmlentities(trim($match_sub[1]));
        //$atts['link_type'] = 'link_to_content';
        
        if (preg_match('/width="([0-9]*)[px]*"/si', $match_sub[5], $match_w) > 0) {
          if (is_numeric($match_w[1])) {
            $atts['width'] = intval($match_w[1]);
          }
        }
        if (preg_match('/height="([0-9]*)[px]*"/si', $match_sub[5], $match_h) > 0) {
          if (is_numeric($match_h[1])) {
            $atts['height'] = intval($match_h[1]);
          }
        }
        
        $attarr = array();
        foreach ($atts as $key => $value) {
          $attarr[] = $key . '=' . $value;
        }
        $markup = '[contentbrowser ==' . implode('==', $attarr) . ']';
        $node->body = str_replace($matches[0][$k], $markup, $node->body);
      }
    }
  }
}

/**
 * Function for converting old content browser inserts ([node:123 xxxxxx])
 */
function contentbrowser_migrate_process_content_browser_inserts(&$node) {
  $matches = array();
  $pattern = '/(?:<a href="\/utdanning_content_browser_node\/(\d+)"\s*rel="lightframe([^"]*)"[^>]*>)?\[node:(\d+)\s+(\w+)(.*?)\](?:<\/a>)?/si';
  if (!preg_match_all($pattern, $node->body, $matches)) {
    return;
  }

  $cc = count($matches[0]);
  for ($k = 0; $k < $cc; $k++) {
    $nid = $matches[3][$k];
    $type = $matches[4][$k];

    if (!in_array($type, array('lightbox', 'link', 'collapsed', 'title'))) {
      // Not all old inserts are compatible with the new content browser.
      // Skip the unsupported ones for now.
      continue;
    }

    $link_text = db_result(db_query("SELECT title FROM node WHERE nid = %d", $nid));
    if ($link_text === FALSE) {
      // No result from the DB. Skip conversion.
      continue; 
    }

    $atts = array();

    $atts['nid'] = $nid;
    $atts['link_text'] = $link_text;

    if ($type == 'lightbox') {
      if (preg_match_all('/(width|height);(\d+)/si', $matches[5][$k], $m)) {
        if (count($m[0]) == 2) {
          // Height and width has been specified
          $atts[$m[1][0]] = intval($m[2][0]);
          $atts[$m[1][1]] = intval($m[2][1]);
          $type = 'lightbox_custom';
        }
      }
    }
    else if ($matches[2][$k]) {
      if (preg_match_all('/(width|height):(\d+)/si', $matches[2][$k], $m)) {
        if (count($m[0]) == 2) {
          // Height and width has been specified
          $atts[$m[1][0]] = intval($m[2][0]);
          $atts[$m[1][1]] = intval($m[2][1]);
          $type = 'lightbox_custom';
        }
      }
    }

    if ($type == 'collapsed') {
      $type = 'collapsed_body';
    }
    else if (($type == 'title') || ($type == 'lightbox')) {
      $type = 'lightbox_large';
    }

    $atts['insertion'] = $type;

    $atts['css_class'] = 'contentbrowser';
    $attarr = array();
    foreach ($atts as $key => $value) {
      $attarr[] = $key . '=' . $value;
    }
    $markup = '[contentbrowser ==' . implode('==', $attarr) . ']';

    // replace match with markup
    $node->body = str_replace($matches[0][$k], $markup, $node->body);
  }  

  $pattern = '/<a href="\/utdanning_content_browser_node\/(\d+)"\s*rel="lightframe([^"]*)"[^>]*>([^<]+)<\/a>/si';
  if (!preg_match_all($pattern, $node->body, $matches)) {
    return;
  }

  $cc = count($matches[0]);
  for ($k = 0; $k < $cc; $k++) {
    $nid = $matches[1][$k];
    $link_text = $matches[3][$k];

    if ($link_text === FALSE) {
      // No title. Skip conversion.
      continue; 
    }

    $atts = array();

    $atts['nid'] = $nid;
    $atts['link_text'] = $link_text;

    if ($matches[2][$k]) {
      if (preg_match_all('/(width|height):(\d+)/si', $matches[2][$k], $m)) {
        if (count($m[0]) == 2) {
          // Height and width has been specified
          $atts[$m[1][0]] = intval($m[2][0]);
          $atts[$m[1][1]] = intval($m[2][1]);
          $type = 'lightbox_custom';
        }
      }
    }

    if ($type == 'collapsed') {
      $type = 'collapsed_body';
    }
    else if (($type == 'title') || ($type == 'lightbox')) {
      $type = 'lightbox_large';
    }

    $atts['insertion'] = $type;
    $atts['css_class'] = 'contentbrowser';
    $attarr = array();
    foreach ($atts as $key => $value) {
      $attarr[] = $key . '=' . $value;
    }
    $markup = '[contentbrowser ==' . implode('==', $attarr) . ']';

    // replace match with markup
    $node->body = str_replace($matches[0][$k], $markup, $node->body);
  }
}
