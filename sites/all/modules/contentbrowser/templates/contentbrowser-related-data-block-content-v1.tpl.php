<?php
$types = node_get_types();
if(module_exists('creativecommons_lite')) {
  $cc_types = creativecommons_lite_get_license_array(variable_get('creativecommons_lite_license_options', NULL));
}
else {
  $cc_types = array();
}
$counter = 1;
print '<div id="mediabrowser-rel-node-list">';
foreach($nids as $nid_object) {
  $extra = $counter > 1 ? ' style="display:none;"' : '';
  print "<div class='mediabrowser-rel-node'>";
  $node = node_load($nid_object->nid);
  print '<div id="contentbrowser-node-title">' . l($node->title, "node/".$node->nid)." (".$types[$node->type]->name. ")</div>";
  
  
  $bearbeidet = @$node->field_bearbeidetav[0]['nid'];
  $copyright = @$node->field_copyright[0]['nid'];

  $creative_commons = @$cc_types[$node->cc_lite_license];

  print theme('ndla_authors_block_view', $node);

  //No need to check if the creativecommons module is enabled - if the cc_lite_license
  //variable is set it clearly must be enabled
  if(isset($node->cc_lite_license) && !empty($node->cc_lite_license)) {
    print strip_tags(get_license_html($node->cc_lite_license,$node->nid), "<a><img>");
  }
  $counter++;
  print '</div>';
}
print '</div>';
?>
