<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Image template file for the content browser.
 *
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $from_editor
 *  - $caption_data: The text under the image (not CCK fields)
 */

global $base_url;

if(   in_array(arg(2), array('embed')) || in_array(arg(0), array('print', 'ndla_node_export')) || !empty($GLOBALS['enable_qrcode']) || !empty($GLOBALS['fag_export'])) {
    print theme('ndla2010_qrcode', $node);
    return;
}

$out = '';
$link_title = '';
if(isset($tag['link_title_text']) && !empty($tag['link_title_text'])) {
  $link_title = "title=\"" . $tag['link_title_text'] . "\"";
}
else if(isset($tag['link_text']) && !empty($tag['link_text'])) {
  $link_title = "title=\"" . $tag['link_text'] . "\"";
}

$title = $node->title;
if(!empty($tag['link_text'])) {
  $title = $tag['link_text'];
}
$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
$alt = t('Image showing the thumbnail for content named \"@name\"', array('@name' => $node->title));

$preset_name = (isset($tag['imagecache']) && !empty($tag['imagecache'])) ? $tag['imagecache'] : NULL;
$preset = NULL;
if(!empty($preset_name) && $tag['insertion'] != 'link') {
  $width = contentbrowser_get_imagecache_width($preset_name);
  if($width && !is_numeric($tag['width'])) {
    $tag['width'] = $width;
  }
}

//If we are coming from the editor AND insertion type is not lightbox (lightbox generates a link which is safe to show in TinyMCE)
if($from_editor && !in_array($tag['insertion'], array('link', 'lightbox_small', 'lightbox_large', 'lightbox_custom'))) {
  $image = contentbrowser_get_thumbnail($tag['nid'], 'flashnode');
  // override flashnode width and height from tag
  $img_dim = '';
  if (is_numeric($tag['width'])) {
    $img_dim .= 'width="' . intval($tag['width']). '"';
  }
  if (is_numeric($tag['height'])) {
    $img_dim .= ' height="' . intval($tag['height']). '"';
  }
  
  if(!empty($preset_name)) {
    $image = $base_url . '/' . imagecache_create_path($preset_name, $image);
  }
  
  $out = "<img src='" . $image . "' $img_dim />";
}
else {
  // override flashnode width and height from tag
  if (is_numeric($tag['width'])) {
    //If this is a flashpakke we need to let utdanning_flashpakke extract width and height.
    if(module_exists('utdanning_flashpakke')) {
      utdanning_flashpakke_prepare_node($node->flashnode);
    }

    //Make sure the flashnode object contains width and height else we will see an error (div. by 0)
    if($node->flashnode['width'] && $node->flashnode['height']) {
      $dimensions = _contentbrowser_resize($node->flashnode['width'], $node->flashnode['height'], $tag['width']);
      $node->flashnode['width'] = $dimensions['width'];
      $node->flashnode['height'] = $dimensions['height'];
    }
  }

  if(in_array($tag['insertion'], array('lightbox_small', 'lightbox_large', 'lightbox_custom'))) {
    $image = _contentbrowser_get_image_thumbnail($node->nid);
    if(empty($image)) {
      $image = NULL;
    }
    else {
      $base_url . '/' . imagecache_create_path($tag['imagecache'], $image);
      $image = "<img alt = '$alt' $link_title src='" . $base_url . '/' . imagecache_create_path($tag['imagecache'], $image) . "' />";
    }
    $out = theme('contentbrowser_insertion', $node, $tag, NULL, NULL, $image);
  }
  else if($tag['insertion'] == 'link') {
    $out = l($title, 'node/' . $node->nid, array('html' => TRUE, 'attributes' => array('title' => $tag['link_title_text'])));
  }
  else {
    $out = theme('flashnode', $node->flashnode, FALSE);
  }

  if(count($caption_data) && $tag['insertion'] != 'link') {
    $out .= "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
  }
}


print $out;
?>
