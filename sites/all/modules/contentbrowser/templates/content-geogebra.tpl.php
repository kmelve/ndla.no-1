<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Template for lenke nodes, used by the content browser.
 *
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $from_editor: TRUE if we are called from an editor.
 *  - $caption_data: The text under the image (not CCK fields)
 */

global $base_path;
$link_title = $node->title;

if(isset($tag['link_title_text']) && !empty($tag['link_title_text'])) {
  $link_title = "title=\"" . $tag['link_title_text'] . "\"";
}
else if(isset($tag['link_text']) && !empty($tag['link_text'])) {
  $link_title = "title=\"" . $tag['link_text'] . "\"";
}

$out = "";

if($from_editor && (empty($tag['insertion']) || $tag['insertion'] == 'inline')) {
  $out = t('Geogebra content with nid# @nid will be inserted here.', array('@nid' => $node->nid));
}

else if(empty($tag['insertion']) || $tag['insertion'] == 'inline') {
  $element['#node'] = $node;
  $out = theme('ndla_geogebra_formatter_ndla_geogebra_file', $element);
  $caption_data = array_filter(array_merge(array($caption_title), $caption_data));
  if(count($caption_data)) {
    $out .= "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
  }
}

else if($tag['insertion'] == 'link') {
  $url = url('node/' . $node->nid);
  $link_text = (empty($tag['link_text'])) ? $node->title : $tag['link_text'];
  $out = "<a $link_title title='".$tag['title']."' href='" . $url . "'>$link_text</a>";
}
else {
  $out .= t('This node does not support %type_of_insertion insertion', array('%type_of_insertion' => $tag['insertion']));
}

print $out;
?>