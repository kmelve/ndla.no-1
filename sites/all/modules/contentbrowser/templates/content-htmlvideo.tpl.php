<?php
global $base_url;
$out = '';
$preset_name = (isset($tag['imagecache']) && !empty($tag['imagecache'])) ? $tag['imagecache'] : NULL;
$image = NULL;
$external = FALSE;

$caption = "";
$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
if(count($caption_data)) {
  $caption = "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
}

//Pick up the poster image. Only of intrest while in TinyMCE.
if($from_editor) {
  if(isset($node->htmlvideo_poster) && !empty($node->htmlvideo_poster)) {
    $image = $node->htmlvideo_poster;
  }
  else if(isset($node->htmlvideo_poster_external_url) && !empty($node->htmlvideo_poster_external_url)) {
    $external = TRUE;
    $image = $node->htmlvideo_poster_external_url;
  }
}

if(!empty($preset_name)) {
  $width = contentbrowser_get_imagecache_width($preset_name);
  if($width && !is_numeric($tag['width'])) {
    $tag['width'] = $width;
  }
}

if(!isset($tag['width'])) {
  $tag['width'] = $node->htmlvideo_width;
}

$dimensions = _contentbrowser_resize($node->htmlvideo_width, $node->htmlvideo_height, $tag['width']);
$node->htmlvideo_width = $dimensions['width'];
$node->htmlvideo_height = $dimensions['height'];

//Make the video look pretty in TinyMCE
if($image && $from_editor) {
  if($preset_name && !$external) {
    $out = "<img src='" . $base_url . '/' . imagecache_create_path($tag['imagecache'], $image) . "' width='" . $dimensions['width'] . "' height='" . $dimensions['height'] . "'/>";
  }
  else {
    $out = "<img src='" . $image . "' width='" . $dimensions['width'] . "' height='" . $dimensions['height'] . "'/>";
  }
}


if($from_editor) {
  if($out) {
    print $out;
  }
  else {
    print t('The selected video with nid# @nid does not have a poster image. It will work anyway and will be inserted here.', array('@nid' => $node->nid));
  }
}
else {
  if(in_array($tag['insertion'], array('lightbox_small', 'lightbox_large', 'lightbox_auto', 'lightbox_custom', 'link'))) {
    $extra = array();
    //If we are using lightbox_auto we need to calculate the size of the lightbox automatically
    if($tag['insertion'] != 'link' && !$from_editor) {
      $extra['dimensions'] = _contentbrowser_resize($node->htmlvideo_width, $node->htmlvideo_height, $tag['width']);
      if($tag['insertion'] == 'lightbox_auto') {
        $tag['insertion'] = 'lightbox_custom';
        $tag['lightbox_size'] = ($extra['dimensions']['width']+50) . "x" . ($extra['dimensions']['height']+100);
      }
    }

    //Link for video is the same thing as lightbox_large except that we show the complete noden instead
    //of only the video in the lightbox
    if($tag['insertion'] == 'link') {
      $tag['insertion'] = 'lightbox_large';
      $extra['fullnode'] = TRUE;
    }
    $out = theme('contentbrowser_insertion', $node, $tag, NULL, $from_editor, NULL, $extra);
    print $out;
    return;
  }
  //print theme('htmlvideo', $node);  
  print htmlvideo_display($node);
}

print $caption;
?>