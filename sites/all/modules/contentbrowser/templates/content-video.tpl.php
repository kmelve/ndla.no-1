<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Template for video nodes, used by the content browser.
 *
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $from_editor: TRUE if we are called from an editor.
 *  - $caption_data: The text under the image (not CCK fields)
 */

if(in_array(arg(0), array('print', 'ndla_node_export')) || !empty($GLOBALS['fag_export'])) {
    $url = url('node/' . $node->nid, array('absolute' => true));
    $out = "<img src='http://chart.apis.google.com/chart?chs=150x150&cht=qr&chl=$url'/><br/>$url";
    print $out;
    return;
}

$out = '';

if (module_exists('istribute')) {
  $is_istribute = istribute_has_video($node);
  $preset_name = (isset($tag['imagecache']) && !empty($tag['imagecache'])) ? $tag['imagecache'] : NULL;
  $preset = NULL;
  if(!empty($preset_name)) {
    $width = contentbrowser_get_imagecache_width($preset_name);
    if($width && !is_numeric($tag['width'])) {
      $tag['width'] = $width;
    }
  }

  if(in_array($tag['insertion'], array('lightbox_small', 'lightbox_large', 'lightbox_auto', 'lightbox_custom', 'link')) && !$from_editor) {
    $extra = array();
    //If we are using lightbox_auto we need to calculate the size of the lightbox automatically
    if($tag['insertion'] != 'link' && !$from_editor) {
      if($is_istribute) {
        if($tag['insertion'] == 'lightbox_auto') {
          if(!is_numeric($tag['width'])) {
            $tag['width'] = variable_get('istribute_playerwidth', 426);
          }
          $tag['insertion'] = 'lightbox_custom';
          $aspect = 16/9;
          $metadata = istribute_get_metadata($node->istribute_video);
          if(isset($metadata->aspect)) {
            $aspect = $metadata->aspect;
          }
          $tag['lightbox_size'] = ($tag['width']+10) . "x" . (ceil(($tag['width']/$aspect))+25);
          $extra['dimensions'] = array('width' => $tag['width'], 'height' => ceil($tag['width']/$aspect));
        }
      }
    }
    //Link for video is the same thing as lightbox_large except that we show the complete noden instead
    //of only the video in the lightbox
    if($tag['insertion'] == 'link') {
      $tag['insertion'] = 'lightbox_large';
      $extra['fullnode'] = TRUE;
    }
    $out = theme('contentbrowser_insertion', $node, $tag, NULL, $from_editor, NULL, $extra);
    print $out;
    return;
  }

  if($from_editor) {
    if(in_array($tag['insertion'], array('lightbox_small', 'lightbox_large', 'lightbox_auto', 'lightbox_custom', 'link'))) {
      //Break here.
      print theme('contentbrowser_insertion', $node, $tag, NULL, $from_editor, NULL, $extra);
      return;
    }
    else {
      $image = NULL;
      if($is_istribute) {
       $image = istribute_get_thumbnail($node, 600, 600);
      }
      else {
        $image = _contentbrowser_get_image_thumbnail($tag['nid']);
      }
       
      // override video width and height from tag
      $img_dim = '';
      if (is_numeric($tag['width'])) {
        $img_dim .= 'width="' . intval($tag['width']). '"';
      }
      if (is_numeric($tag['height'])) {
        $img_dim .= ' height="' . intval($tag['height']). '"';
      }
    
      $im_preset = variable_get('contentbrowser_video_imagecache' ,'');
      if(!empty($preset_name)) {
        $im_preset = $preset_name;
      }
    
      if(!empty($im_preset) && !$is_istribute) {
        $image = $base_url . '/' . imagecache_create_path($im_preset, $image);
      }
      $out = "<img src='" . $image . "' $img_dim />";
    }
  }
  else {
    // override video width and height from tag
    $params = array();

    // Override preview image if one has been selected
    if ($node->iids && count($node->iids)) {
      $img_node = node_load($node->iids[0]);
      $img_url = base_path() . file_create_path($img_node->images['_original']);
      $params['flashvars'] = array('image' => $img_url);
    }

    if($is_istribute) {
      $width = is_numeric($tag['width']) ? $tag['width'] : NULL;
      $start_time = !empty($tag['start_time']) ? $tag['start_time'] : 0;
      $out = theme('istribute_player', $node, $width, NULL, NULL, $start_time);
    }
  }
  $caption_data = array_filter(array_merge(array($caption_title), $caption_data));
  if(count($caption_data)) {
    $style = "";
    if(is_numeric($tag['width'])) {
      $style = "style='width: " . $tag['width'] . "px;'";
    }
    $out .= "<span class='contentbrowser_caption' $style>" . implode("<br />", array_filter($caption_data)) . "</span>";
  }

  if(!empty($tag['download_link'])) {
    $urls = istribute_get_downloadurls($node);
    $out .= theme('ndla_utils_istribute_download', $urls);
  }
} // endif module_exists('istribute')

print $out;
?>
