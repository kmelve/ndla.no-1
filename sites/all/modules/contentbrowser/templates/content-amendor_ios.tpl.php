<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief 
 *  Template file for amendor_ios. Used by the content browser.
 *
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $from_editor: TRUE if we are called from an editor.
 *  - $caption_data: The text under the image (not CCK fields)
 */
global $base_url;
$default_height = 1170;
$default_width = 720;
$out = '';
$link_title = '';
if(isset($tag['link_title_text']) && !empty($tag['link_title_text'])) {
  $link_title = "title=\"" . $tag['link_title_text'] . "\"";
}
else if(isset($tag['link_text']) && !empty($tag['link_text'])) {
  $link_title = "title=\"" . $tag['link_text'] . "\"";
}

$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
$alt = t('Image showing the thumbnail for content named \"@name\"', array('@name' => $node->title));

if($from_editor) {
  if($tag['insertion'] != 'inline') {
    if($tag['insertion'] == 'thumbnail') {
      $thumbnail = _contentbrowser_get_image_thumbnail($node->nid);
    }
    if(empty($thumbnail)) {
      $out .= l($node->title, 'node/' . $node->nid);
      $caption_data = array(); //No caption while rendering links
    } else {
      if($tag['imagecache'] && !empty($tag['imagecache'])) {
        $out .= '<a ' . $link_title . ' href="' . url('node/' . $node->nid) . '">' . theme('image', imagecache_create_path($tag['imagecache'], $thumbnail), $alt, '', NULL, FALSE) . '</a>';
      } else {
        $out .= '<a ' . $link_title . ' href="' . url('node/' . $node->nid) . '">' . theme('image', imagecache_create_path('Liten', $thumbnail), $alt, '', NULL, FALSE) . '</a>';
      }
    }
  }
  else {
    $out = _contentbrowser_render_editor_image($tag);
  }
}
//We only support inline and link right now, make everything which isnt 'line' go this way.
elseif (!isset($tag['insertion']) || $tag['insertion'] != 'inline') {
  if($tag['insertion'] == 'thumbnail') {
    $thumbnail = _contentbrowser_get_image_thumbnail($node->nid);
  }
  if(empty($thumbnail)) {
    $out .= l($node->title, 'node/' . $node->nid, array('attributes' => array('title' => $tag['link_title_text'])));
    $caption_data = array(); //No caption while rendering links
  } else {
    if($tag['imagecache'] && !empty($tag['imagecache'])) {
      $out .= '<a ' . $link_title . ' href="' . url('node/' . $node->nid) . '">' . theme('image', imagecache_create_path($tag['imagecache'], $thumbnail), $alt, '', NULL, FALSE) . '</a>';
    } else {
      $out .= '<a ' . $link_title . ' href="' . url('node/' . $node->nid) . '">' . theme('image', imagecache_create_path('Liten', $thumbnail), $alt, '', NULL, FALSE) . '</a>';
    }
  }
}
//Show the node in the body field.
elseif ($tag['insertion'] == 'inline') {
  $width = (isset($tag['width']) && $tag['width'] > 0) ? $tag['width'] : NULL;
  if($width) {
    $size = _contentbrowser_resize($default_width, $default_height, $tag['width']);
    $out = amendor_ios_pupil($node, $size['width']);
  }
  else {
    $out = amendor_ios_pupil($node, 635);
  }
}
if(count($caption_data)) {
  $out .= "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
}

print $out;
?>
