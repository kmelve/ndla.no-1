<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 */
 
$full = (arg(3) == 'full') ? TRUE : FALSE;

/* Translated title */
if(module_exists('ndla_content_translation')) {
  if(ndla_content_translation_enabled($node->type)) {
    ndla_content_translation_view($node, FALSE, FALSE, TRUE);
    $title = $node->title;
  }
}

/**
 * Create content before drupal_get_html_head()
 * because wiris (and probably other) adds javascrips
 * in filters/node_view.
 */
$content = "";
if(($node->type == 'video' || $node->type == 'flashnode') && !$full) {
  $params = NULL;
  if(arg(3) && arg(4)) {
    $params['width'] = check_plain(arg(3));
    $params['height'] = check_plain(arg(4));
  }
  if($node->type == 'video') {
    if(module_exists('istribute') && istribute_has_video($node)) {
      $content .= "<br />" . theme('istribute_player', $node, $params['width']);
    }
  }
  else if($node->type == 'flashnode') {
    if(!empty($params['width']) && !empty($params['height'])) {
      $node->flashnode['width'] = $params['width'];
      $node->flashnode['height'] = $params['height'];
    }
    $content .= theme('flashnode', $node->flashnode, FALSE, array('no_max' => TRUE));
  }
}
else {
  $content .= node_view($node, FALSE, TRUE, FALSE);
}

?>

<html>
<head>
<title><?php print $title; ?></title>
<?php 
drupal_add_css(drupal_get_path('theme', 'ndla2010') . "/css/default.css");
drupal_add_css(drupal_get_path('theme', 'ndla2010') . "/css/ui-lightness/jquery-ui-1.8.1.custom.css");
drupal_add_css(drupal_get_path('theme', 'ndla2010') . "/css/drupal_styles.css");
drupal_add_css(drupal_get_path('theme', 'ndla2010') . "/css/ndla_tinymce_commons.css");
drupal_add_css(drupal_get_path('theme', 'ndla2010') . "/css/ndla_styles.css");
print drupal_get_html_head();
print drupal_get_css(); ?>
<?php print drupal_get_js('header'); ?>
</head>

<body class="lightboxContent">
<?php if(($node->type != 'flashnode' && $node->type != 'video') || $full): ?>
<h1><?php print $title; ?></h1>
<?php endif; ?>
<div id="content">
<div id="body">
<?php print $content; ?>
</div>
</div>
</body>
</html>
