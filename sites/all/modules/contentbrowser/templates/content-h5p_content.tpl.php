<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Template file for audio nodes, used by the content browser
 *
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $from_editor: TRUE if we are called from an editor.
 *  - $caption_data: The text under the image (not CCK fields)
 */

if(in_array(arg(0), array('print', 'ndla_node_export')) || !empty($GLOBALS['fag_export'])) {
    $url = url('node/' . $node->nid, array('absolute' => true));
    $out = "<img src='http://chart.apis.google.com/chart?chs=150x150&cht=qr&chl=$url'/><br/>$url";
    print $out;
    return;
}

if(!empty($tag['imagecache']) && empty($tag['width'])) {
  $tag['width'] = contentbrowser_get_imagecache_width($tag['imagecache']);
}

if($tag['insertion'] == 'inline') {
  if($from_editor) {
    print t('The H5P with node id %nid will be inserted here', array('%nid' => $node->nid));
  } else {

    /**
     * In h5p_view() the javascript settings array is added
     * to drupal, but since the nodeword module uses node_view
     * to get the node content for the description field
     * this might be added twice. Therefor this code only loads
     * h5p once (in the node content and not in description metatag).
     */
    global $cb_added_h5p;
    if(isset($cb_added_h5p[$node->nid]))
      return;
    $cb_added_h5p[$node->nid] = $node->nid;

    $node = h5p_view($node);
    $out = $node->content['h5p']['#value'];
    // Wrap H5P inside div making JS able to find out forced fullscreen is enabled
    if(!empty($tag['fullscreen']) && $tag['fullscreen'] == 1 && $node->main_library['fullscreen'] == 1) {
      $out = '<div class="h5p-ndla-force-fullscreen">' . $out . '</div>';
    }
    if(!empty($tag['width'])) {
      $out = '<div style="width:' . $tag['width'] .  'px;">' . $out . '</div>';
    }
  }
} else if($tag['insertion'] == 'link') {
  $title = (!empty($tag['link_text'])) ? $tag['link_text'] : $node->title;
  $out = l($title, 'node/' . $node->nid, array('html' => TRUE, 'attributes' => array('title' => $tag['link_title_text'])));
} else {
  $out = t('This node does not support insertion type %type', array('%type' => $tag['insertion']));
}

print $out;
