<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Template for lenke nodes, used by the content browser.
 *
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $from_editor: TRUE if we are called from an editor.
 *  - $caption_data: The text under the image (not CCK fields)
 */
global $base_path;
$out = "";
if(isset($node->field_url) && $tag['insertion'] != 'inline') {
  $title = $node->title;

  if(isset($node->field_url[0]['title']) && !empty($node->field_url[0]['title'])) {
    $title = $node->field_url[0]['title'];
  }

  $title = isset($tag['link_text']) && !empty($tag['link_text']) ? $tag['link_text'] : $title;
  $url = $node->field_url[0]['url'];
  $url = str_replace("&amp;", "TRUEAMP", $url);
  $url = str_replace("&", "&amp;", $url);
  $url = str_replace("TRUEAMP", "&amp;", $url);
  if(isset($tag['link_anchor'])) {
    $url .= '#'. $tag['link_anchor'];
  }
  
  $attributes = array();
  if($tag['insertion'] == 'link') {
    $attributes[] = "target='_new'";
  }
  else {
    $warning = t("By following this link you are outside the scope of ndla.no. NDLA does not take any responsibility for the content or the technical quality").".";
    
    /* Add 40px width and height due to margin */
    if(!empty($tag['lightbox_size'])) {
      $parts = explode('x', $tag['lightbox_size']);
      foreach($parts as $key => $part) {
        $parts[$key] = $part + 40;
      }
      $tag['lightbox_size'] = implode('x', $parts);
    }

    $modal = "rel='lightframe[|" . _contentbrowser_get_lightbox_dimensions($tag) ."scrolling:auto;][$warning]'";
    $attributes[] = $modal;
  }
  $attributes[] = "title='" . $tag['link_title_text'] . "'";
  
  /**
   * Link to http://www.crwflags.com/fotw/flags/np'.html#
   * destroys html since ' is used as container for href
   */
  $url = str_replace("'", "&#39;", $url);
  $out .= "<a " . implode(" ", $attributes) . " href='" .$url . "'><span class='contentbrowser_link_text'>".$title."</span></a>";

  if(empty($node->field_url[0]['url'])) {
    $out = "<i>" . t('The author requested a link to the field URL in node/@nid, but the field is empty.', array('@nid' => $node->nid)) . "</i>";
  }
}
else if($tag['insertion'] == 'inline') {
  $preset_name = (isset($tag['imagecache']) && !empty($tag['imagecache'])) ? $tag['imagecache'] : NULL;
  $width = 600;
  $height = 600;
  if(!empty($preset_name)) {
    $width = contentbrowser_get_imagecache_width($preset_name);
  }
  
  if(isset($tag['width']) && is_numeric($tag['width']) && $tag['width'] > 10) {
    $width = $tag['width'];
  }
  
  if(isset($node->field_embed_code) && !empty($node->field_embed_code[0]['value'])) {
    if($from_editor) {
      $dimensions = _contentbrowser_alter_embed_code_size($node->field_embed_code[0]['value'], $width, $height, TRUE);
      $out .= "<img style='width: " . $dimensions['width'] . "px; height: " . $dimensions['height'] . "px;' src='" . $base_path . drupal_get_path('module', 'contentbrowser') . "/images/embed_thumb.jpg' />";
    }
    else {
      $out .= _contentbrowser_alter_embed_code_size($node->field_embed_code[0]['value'], $width, $height);
    }
  }
  else {
    $out .= t('This node does not support inline insertion');
  }
}
else {
  $out .= t('This node does not support %type_of_insertion insertion', array('%type_of_insertion' => $tag['insertion']));
}

print $out;
?>