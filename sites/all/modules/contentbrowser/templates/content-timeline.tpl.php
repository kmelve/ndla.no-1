<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 * @brief
 *  Template file for rendering timeline content
 *  Implemented insertions: inline, link, title, lightbox
 * Parameters available:
 *  - $node
 *  - $tag
 *  - $from_editor
 */

global $base_url, $theme_path;
//We need to prepare the node for viewing
//$node = node_build_content($node);

//Check if this content type can choose insertion.
$enabled_cts = array_filter(variable_get('contentbrowser_choose_insertion', array()));
if(!isset($tag['insertion']) || empty($tag['insertion']) || !in_array($node->type, $enabled_cts)) {
  return;
}

//Dont allow body/teaser insertion in the TinyMCE editor.
if($from_editor && $tag['insertion'] == 'inline') {
  print t('The timeline with node id %nid will be inserted here', array('%nid' => $node->nid));
  return;
}

$link_to_content = "node/".$node->nid;
$lightbox_link_to_content = url("contentbrowser/node/".$node->nid);
$teaser_type = FALSE;
$out = '';

if(module_exists('ndla_content_translation')) {
  if(ndla_content_translation_enabled($node->type)) {
    ndla_content_translation_view($node, $teaser_type, FALSE, TRUE);
    $node->body = $node->content['body']['#value'];
    $node->teaser = $node->field_ingress[0]['safe'];
    $tag['link_text'] = $node->title;
  }
}

$title = $lightbox_title = (isset($tag['link_text']) && !empty($tag['link_text'])) ? $tag['link_text'] : $node->title;

//The template content-flashnode sends a custom title to be used. Make a check and see if it exists.
if(isset($custom_title) && !empty($custom_title)) {
  //We have a winner. Override the title
  $title = $custom_title;
}

if($tag['insertion'] == 'title' && !$attachment) {
  $out = $node->title;
}
else if(strpos($tag['insertion'], 'lightbox') !== FALSE || $tag['link_type'] == 'lightbox') {
  $caption = array();
  $caption_title = '';
  $lightbox_data = _contentbrowser_get_fields($node, array_filter(variable_get('contentbrowser_lightbox_'. $node->type .'_fields', array())), $tag['remove_fields_lightbox'], 'lightbox');
  $lightbox_data = str_replace("\n", "", implode(" | ", array_filter($lightbox_data)));
  $lightbox_data = htmlentities($lightbox_data, ENT_COMPAT, "UTF-8");
  
  /* Add 40px width and height due to margin */
  if(!empty($tag['lightbox_size'])) {
    $parts = explode('x', $tag['lightbox_size']);
    foreach($parts as $key => $part) {
      $parts[$key] = $part + 40;
    }
    $tag['lightbox_size'] = implode('x', $parts);
  }
  
  $width_height = _contentbrowser_get_lightbox_dimensions($tag);
  $more_info = '<p class="lightbox-more-info">' . t('More information') . ': ' . t('Go to the page') . ' ' . l(htmlentities($node->title, ENT_QUOTES, 'UTF-8'), 'node/' . $node->nid) . '</p>';
  $out = "<a title='" . $tag['link_title_text'] . "' href='".$lightbox_link_to_content."' rel='lightframe[|$width_height][$lightbox_title<br />$lightbox_data" . "<br />$more_info]'>$title</a>";
}
else if($tag['insertion'] == 'link' || $attachment) {
  $caption = array();
  $caption_title = '';
  $out = l($title, $link_to_content, array('attributes' => array('title' => $tag['link_title_text'])));
}
else if($tag['insertion'] == 'inline') {
  if(module_exists('ndla_timeline')) {
    $node2 = ndla_timeline_nodeapi($node, 'view', FALSE, FALSE);
    $out = $node2->content['ndla_timeline']['#value'];
  }
  else {
   $out = node_view($node, FALSE, TRUE, FALSE); 
  }
}
else {
  $out = t('This node does not support insertion type %type', array('%type' => $tag['insertion']));
}

$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
if(count($caption_data)) {
  $out .= "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
}
//Remove div class = full. It messes up our layout
$out = str_replace("class=\"full\"", "", $out);
print $out;
?>
