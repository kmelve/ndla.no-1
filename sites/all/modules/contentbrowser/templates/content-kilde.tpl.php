<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 * @brief
 *  Generic template file.
 * Params available:
 *  - $node - The node object
 *  - $tag - The decoded tag.
 *  - $attachment - FALSE or Array with file information.
 *  - $from_editor - TRUE if we are called from an editor. It is not safe to render embed-code, javascript etc...
 *  - $caption_data - The text under the image (not CCK fields)
 */

global $base_path;
global $language;
//$link_to_pdf = url($node->field_pdf[0]['filepath'], array('absolute' => TRUE));
//$link_to_pdf = str_replace(base_path() . $language->language . "/", "/", $link_to_pdf);
$link_to_pdf = url('node/' . $node->nid, array('absolute' => TRUE));
if(!empty($node->field_pdf[0]) && $tag['insertion'] == 'link') {
  $out = l($tag['link_text'], $link_to_pdf, array('attributes' => array('title' => $tag['link_title_text'], 'target' => '_blank')));
  $caption_data = array();
}
else if($tag['insertion'] == 'thumbnail') {
  $caption_width = $img_dim = '';
  $preset = variable_get('ndla_utils_kilde_pdf_imagecache', 'Fullbredde');
  if(!empty($tag['imagecache'])) {
    $preset = $tag['imagecache'];
  }
  
  if (is_numeric($tag['width'])) {
    $img_dim .= 'width="' . intval($tag['width']). '"';
    $caption_width = intval($tag['width'])."px";
  }
  
  $image_url = url($node->field_pdf_thumbnail[0]['filepath'], array('absolute' => TRUE));
  if(!empty($preset)) {
    $image_url = url(imagecache_create_path($preset, $node->field_pdf_thumbnail[0]['filepath']), array('absolute' => TRUE));
  }
  $image_url = str_replace($base_url . $language->language . "/", "", $image_url);
  $image = "<img $img_dim src='$image_url' alt='" . t('Frontpage of file %file', array('%file' => $node->field_pdf_thumbnail[0]['filename'])) . "' />";
  
  $out = "<a href='" . $link_to_pdf . "' title='" . $tag['link_title_text'] . "' target='_blank'>" . $image . "</a>";
}

if((isset($tag['insertion']) && !empty($tag['insertion']) && preg_match("/(inline|link|collapsed|lightbox)/", $tag['insertion'], $matches))) {
 $caption_title = '';
}

if(empty($tag['insertion']) && empty($out)) {
  //Act as a default handler for content which has no insertion type
  //This is rather lame, it really should exist a better template.
  $out =  t('This node does not support %type_of_insertion insertion', array('%type_of_insertion' => $tag['insertion']));
}

$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
if(count($caption_data)) {
  $out .= "<span style='width: " . $caption_width . "' class='contentbrowser_caption'>" . implode("<br />", $caption_data) . "</span>";
}

print $out;
?>
