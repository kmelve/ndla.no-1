<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Image template for the content browser.
 *
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $from_editor: TRUE if we are called from an editor.
 *  - $caption_data: The text under the image (not CCK fields)
 */
global $base_url;
$image = '';
$alt = '';
$link_title = '';
$link = '';
$lightbox_data = _contentbrowser_get_fields($node, array_filter(variable_get('contentbrowser_lightbox_image_fields', array())), $tag['remove_fields_lightbox'], 'lightbox');
$css = 'margin_right';
$caption_data = array_filter(array_merge(array($caption_title), $caption_data));

if($tag['imagecache'] && !empty($tag['imagecache'])) {
  $image = $base_url . '/' . imagecache_create_path($tag['imagecache'], $node->images['_original']);
}
else {
  $image = $base_url."/".$node->images['normal'];
}

if(isset($tag['alt']) && !empty($tag['alt'])) {
  $alt = "alt=\"" . $tag['alt'] . "\"";
}

$out = '';

if(isset($tag['link_title_text']) && !empty($tag['link_title_text'])) {
  $link_title = "title=\"" . $tag['link_title_text'] . "\"";
}
else if(isset($tag['link_text']) && !empty($tag['link_text'])) {
  $link_title = "title=\"" . $tag['link_text'] . "\"";
}

$url = $base_url . '/' . $node->images["_original"];
$lightbox_type = "lightbox";

if($tag['link']) {
  if(strtolower(substr($tag['link'], 0, 4)) == 'http') {
    $lightbox_type = 'lightframe';
    $url = $tag['link'];
    $css = 'double_underline';
  }
  else {
    $lightbox_type = 'lightmodal';
    $css = 'single_underline';
  }

  $url = url($tag['link'], array('absolute' => TRUE));
  if(isset($tag['link_anchor']) && !empty($tag['link_anchor'])) {
    $url .= '#'.$tag['link_anchor'];
  }
}

// get <img/> width and height
$img_dim = '';
$caption_width = "";
if (is_numeric($tag['width'])) {
  $img_dim .= 'width="' . intval($tag['width']). '"';
  $caption_width = intval($tag['width'])."px";
}
else {
  // $image is an URL = problem on *.dev.ndla.no as it wants authorization. Transform it back to a path.
  global $base_url;
  $i = str_replace($base_url."/", "", $image);
  if (!file_exists($i) && $tag['imagecache'] && !empty($tag['imagecache'])) {
    // The file is a imagecache-generated file which is missing. Use curl to
    // retrieve the file, as this forces imagecache to generate the image
    $img_components = explode("/", $i);
    $filename = $img_components[count($img_components) - 1];
    $url_filename = urlencode($filename);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, str_replace($filename, $url_filename, $image));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $tmp = curl_exec($ch);
  }

  if (file_exists($i)) {
    $img_details = getimagesize($i);
    $caption_width = $img_details[0]."px";
  }
}
if (!empty($tag['height']) && is_numeric($tag['height'])) {
  $img_dim .= ' height="' . intval($tag['height']). '"';
}

if((!isset($tag['link_type']) || empty($tag['link_type'])) || in_array($tag['link_type'], array('lightbox_small', 'lightbox_large', 'lightbox_custom')) || empty($tag['link'])) {
  $more_info = '<p class="lightbox-more-info">' . t('More information') . ': ' . t('Go to the page') . ' ' . l(htmlentities($node->title, ENT_QUOTES, 'UTF-8'), 'node/' . $node->nid) . '</p>';
  $commons = $lightbox_data['creativecommons'];
  unset($lightbox_data['creativecommons']);
  $lightbox_caption = str_replace("\n", '', '<p>' . implode(", ", $caption_data) . '</p>');
  $lightbox_data = str_replace("\n", '', $lightbox_caption . '<p>' . implode(' | ', array_filter($lightbox_data)) . '</p>' . ($commons ? '<p>' . $commons . '</p>' : NULL) . $more_info);
  $lightbox_data = htmlentities($lightbox_data, ENT_COMPAT, "UTF-8");
  $width_height = '';
  $dimensions = explode("x", $tag['lightbox_size']);
  if(count($dimensions) == 2 && is_numeric($dimensions[0]) && is_numeric($dimensions[1])) {
    $width_height = "|width:" . $dimensions[0] . "px;height:" . $dimensions[1] . "px;";
  }

  $out  = '<a ' . $link_title . ' rel="'.$lightbox_type.'[' . $width_height . ']['. $lightbox_data .']" href="' . $url . '"><img class="' . $css . '" src="'. $image .'" '. $alt . $img_dim . ' /></a>';
}
else {
  $target = '';
  if($tag['link_type'] == 'link_in_new_tab') {
    $target = 'target="_new"';
  }
  $out = "<a $link_title title='".$tag['title']."' href='" . $url . "' $target><img class='border_bottom' src='$image' $alt $img_dim/></a>";
}

if(count($caption_data)) {
  $out .= "<span style='width: " . $caption_width . "' class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
}

print $out;
?>
