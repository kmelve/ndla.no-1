<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Template for Biblio nodes. The Contentbrowser filter MUST come before Biblio in the filters order for this to work.
 *
 * Params available:
 *  $node: The node object
 *  $tag: The decoded tag.
 *  $from_editor: TRUE if we are called from an editor.
 *  $caption_data: The text under the image (not CCK fields)
 */
if($from_editor) {
  print "(". t('Reference to (%title, #%nid)', array('%title' => $node->title, '%nid' => $node->nid)) . ")";
}
else {
  print "[bib]".$node->biblio_citekey."[/bib]";  
}
?>