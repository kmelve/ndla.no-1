<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser 
 * @brief
 *  The template for the detailed node view.
 *
 * Parameters available:
 *  - $thumbnail
 *  - $node
 *  - $form
 */
$types = node_get_types();
?>

<style>
p.not_published {
  color: red;
  font-weight: bold;
}
p.workflow {
  margin-bottom: 5px;
}
p.current {
  font-style: italic;
}
</style>

<div id='contentbrowser_wrapper'>
  <div id='contentbrowser_node_details'>
    <?php if($thumbnail): ?>
    <p><?php print $thumbnail;?></p>
    <?php endif; ?>
    <p><?php print t('Title') . ": " . $node->title; ?></p>
    <p><?php print t('Nid') . ": " . $node->nid; ?></p>
    <p><?php print t('Nodetype') .  ": " . $types[$node->type]->name; ?></p>
    <p><?php print t('Author') . ": " . $node->name; ?></p>
    <p><?php print t('Created') . ": " . format_date($node->created, 'small'); ?></p>
    <?php
    if(module_exists('ndla_publish_workflow')):
      $status = ndla_publish_workflow_get_current_state($node);
      $states = ndla_publish_workflow_get_states();
      if($status != PUBLISH_WORKFLOW_PUBLISHED):
        print "<p class='workflow not_published'>" . t('This node is not published!') . "</p>";
        print "<p class='workflow current'>" . t("Current state: @state", array('@state' => $states[$status])) . "</p>";
      endif;
    endif;
    ?>
  </div>
  
  <div id='contentbrowser_details_form'>
    <?php print theme('status_messages'); ?>
    <?php print $form; ?>
  </div>
  <div style='clear: both'></div>
</div>

<script type='text/javascript'>
  Drupal.behaviors.autocomplete();
  Drupal.behaviors.collapse();
</script>
