// $Id$
/**
 * @file
 * @ingroup contentbrowser
 */

/**
 * Loops through all items in the form which has the postable class
 * and creates a tag from tag.
 */
function contentbrowser_get_tag() {
  if(contentbrowser_validate_form()) {
    var data = new Array();
    $(".postable").each( function() {      
      if($(this).is('input:checkbox')) {
        var check_value = $(this).attr('checked') ? 1 : 0;
        data.push($(this).attr('name') + "=" + check_value);
      }
      else {
        try {
          data_value = escape($(this).val().replace(/=/g, "\\="));
          data.push($(this).attr('name') + "=" + data_value);
        }
        catch(err) {
          ; //$(this).val() can be empty if CB is misconfigured
        }
      }
    });
    instance = $('#edit-instanceId').val();
    tag = "[contentbrowser ==" + data.join("==") + "]";
    send_to_editor(tag, instance);
  }
}

/**
 * Returns the tag to the TinyMCE editor.
 */
function send_to_editor(tag, instance) {
  tag = new Array(tag);
  //Send an ajax request and get the nice looking HTML.
  $.ajax({
    url: Drupal.settings.basePath + "contentbrowser/decode/editor",
    type: "POST",
    data: {'tags[]': tag},
    dataType: 'json',
    cache: 1,
    async: 0,
    success: function(encoded_tags){
      var html = encoded_tags[0];
      html += "&nbsp;"
      window.opener.tinymce.activeEditor.execCommand('mceInsertRawHTML', false, html);
    	//window.opener.Drupal.wysiwyg.instances[instance].insert(html);
    	window.close();
    }
  });
}

/**
 * Function for validating the form. Returns true on success, false otherwise.
 */
function contentbrowser_validate_form() {
  var error = false;
  var error_message = "";
  
  if($('#edit-fid').val()) {
    var i = $('#edit-insertion').val();
    if(!(i == 'link' || i == 'lightbox_small' || i == 'lightbox_large' || i == 'default')) {
      error_message += (Drupal.t('Only lightbox, link or default are valid insertion options while using attachments.')) + "\n";
      error = true;
    }
  }
  
  if((typeof $('#edit-width').val() != 'undefined') && $('#edit-width').val() != '' && isNaN($('#edit-width').val())) {
    error = true;
    error_message += Drupal.t('Width does not contain a valid value') + "\n";
  }
  if((typeof $('#edit-height').val() != 'undefined') && $('#edit-height').val() != '' && isNaN($('#edit-height').val())) {
    error = true;
    error_message += Drupal.t('Height does not contain a valid value') + "\n";
  }
  
  if($('#edit-insertion').val() == 'lightbox_custom' || $('#edit-link-type').val() == 'lightbox_custom') {
    var size = $('#lightbox_size').val();
    size = size.split("x");
    try {
      width = size[0];
      height = size[1];

      if(isNaN(width) || isNaN(height) || width <= 0 || height <= 0) {
        error = true;
        error_message += Drupal.t("The lightbox size is invalid");
      }
    }
    catch(err) {
      error = true;
      error_message += Drupal.t("The lightbox size is invalid");
    }
  }
  else {
    //Remove any weird things the user might have inserted
    $('#lightbox_size').val('');
  }

  //Validate any items marked with required...
  $(".error").removeClass('error');
  $(".required").each( function() {
      if($(this).val() == '') {
        //Add the error class.
        $(this).addClass('error');
        error = true;
      }
    }
  );

  if($('#edit-link').length) {
    if($('#edit-link').val()) {
      var link = $('#edit-link').val();
      var not_allowed = new Array('red.test.ndla.no', 'red.ndla.no', 'cm.test.ndla.no', 'cm.ndla.no', 'red.beta.ndla.no', 'red.ndla.l', 'ndla.l');
      for(i in not_allowed) {
        if(link.indexOf(not_allowed[i]) != -1) {
          error = true;
          error_message += Drupal.t('The link is invalid. Remove the hostname.');
          $('#edit-link').addClass('error');
        }
      }
    }
  }
  
  if(error && error_message != '') {
    alert(error_message);
  }
  
  return (!error);
}

function contentbrowser_return(link) {
  $(link).unbind('click');
  $(link).attr('href' , 'javascript:void(0);');
  var complete_function = contentbrowser_parse_query("oncomplete");
  if(complete_function == "") {
    complete_function = "runkode";
  }
  var output = '';
  var nid = $(link).attr('id').replace('node_', '');
  $.ajax({
    url: Drupal.settings.basePath + "contentbrowser/get_object/" + nid,
    type: "POST",
    data: {nid: nid},
    dataType: 'json',
    cache: 1,
    async: 0,
    success: function(data){
      out_data = data.title + " [nid:" + data.nid +"]";
      if($(link).hasClass('json-output')) {
        out_data = data;
      } else if($(link).hasClass('nid-output')) {
        out_data = data.nid;
      } else if($(link).hasClass('path-output')) {
        out_data = '/node/' + data.nid;
      }
      if(complete_function == 'postMessage') {
        window.opener.postMessage(out_data, '*');
        window.close();
      }
      try {        
        //If we are opened in an iframe
        eval("window.parent." + complete_function + "(out_data);");
        if(window.parent.$('#bottomNavClose').length > 0) {
          window.parent.$('#bottomNavClose').click();
        }
        window.close();
      }
      //window.open - anybody can do this
      catch(err) {
        try {
          eval("window.opener." + complete_function + "(out_data);");
          window.close();
        }
        catch(err) {
          alert(Drupal.t("Unable to return data to caller"));
        }
      }
    }
  });

  return false;
}

function contentbrowser_parse_query(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split('&');
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    if (decodeURIComponent(pair[0]) == variable) {
      return decodeURIComponent(pair[1]);
    }
  }

  return "";
}
