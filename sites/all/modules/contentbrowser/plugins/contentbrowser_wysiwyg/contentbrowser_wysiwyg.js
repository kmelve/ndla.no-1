// $Id$
(function ($) {
Drupal.wysiwyg.plugins['contentbrowser_wysiwyg'] = {	
  /**
   * Return whether the passed node belongs to this plugin.
   */
  isNode: function(node) {
    return ($(node).is('.contentbrowser'));
  },

  /**
   * Execute the button.
   */
  invoke: function(data, settings, instanceId) {
    if (data.format == 'html') {
      var url = Drupal.settings.basePath + "contentbrowser";
      var attributes = new Array();
      var cb_id = "";
      try {
        //CKEditor
        cb_id = data['node']['parentNode'].getAttribute('contentbrowser_id');
      }
      catch(err) {
        ;
      }
      if(!cb_id) {
        try {
          //TinyMCE
          cb_id = data['node'].getAttribute('contentbrowser_id');
        }
        catch(err) {
          ;
        }
      }

      if (cb_id) {
  		  matches = cb_id.match(/nid=([0-9]+)/);
  		  if(matches && matches.length == 2) {
  		    url += "/select";
  		    attributes.push("tag=" + encodeURI(escape(cb_id.replace(/\\/g, ''))));
  		  }
  		}
  		if(Drupal.settings.node_id) {
  		  attributes.push("current_nid=" + Drupal.settings.node_id);
  		}
  		
  		attributes.push("instanceId=" + instanceId);
  		
  		url += "?" + attributes.join("&");
      window.open (url, Drupal.t("Content Browser"),"status=1,width=1000,height=771,scrollbars=1");
    }
  },

  /**
   * Replace [contentbrowser ==nid=123==foo=bar] with rendered content
   */
  attach: function(content, settings, instanceId) {
    //alert("attach!");
    var matches = content.match(/\[contentbrowser\s?==(?:[^\]\[]*(?:\[[^\]]*\])*)*\]/g);
    var add_empty = content.match(/\[contentbrowser[^\]]*\]$/);
    var add_last = content.match(/<p>\[contentbrowser[^\]]*\]<\/p>$/);
    if(matches) {
      $.ajax({
        url: Drupal.settings.basePath + "contentbrowser/decode/editor",
        data: ({ 'tags[]': matches }),
        dataType: "json",
        type: "POST",
        async: 0,
        cache: 1,
        success: function(encoded_tags) {
          for (i in encoded_tags) {
            content = content.replace(matches[i], encoded_tags[i]);
          }
        }
      });
      if(add_empty) {
        content += "<p>&nbsp;</p>";
      }
      if(add_last) {
        content = content.replace(/<\/p>$/, '&nbsp;</p>');
      }
    }
    
    return content;
  },

  /**
   * Replace images with <!--break--> tags in content upon detaching editor.
   */
  detach: function(content, settings, instanceId) {
    var $content = $('<div>' + content + '</div>'); // No .outerHTML() in jQuery :(
    $.each($('.contentbrowser', $content), function (i, elem) {
      if($(elem).attr('contentbrowser_id')) {
        var css_class = $(elem).attr('class');
  		  var contentbrowser_id = $(elem).attr('contentbrowser_id');
  	    if($(elem).find(".contentbrowser_link_text").html()) {
  	      link_text = $(elem).find(".contentbrowser_link_text").html().replace(/=/g, "\\=");
          //Remove any tags from link_text.
          link_text = link_text.replace(/<\/?(?!\!)[^>]*>/gi, '');
          contentbrowser_id = contentbrowser_id.replace(/==link_text=.*?==/, "==link_text=" + link_text + "==");
  	    }
        //We could have a problem here while expecting css_class to always be last.
  		  contentbrowser_id = contentbrowser_id.replace(/==css_class=[a-zA-Z_0-9 ]+/, "") + "==css_class=" + css_class;
  		  $(elem).before("[contentbrowser ==" + contentbrowser_id + "]").remove();
      }
    });

    return $content.html();
  },
};

})(jQuery);
