// $Id$
/**
 * @file
 * @ingroup contentbrowser
 * @brief
 *  The TinyMCE plugin for ContentBrowser
 */
(function() {
	tinymce.create("tinymce.plugins.ContentBrowser", {
	/**
	 * Returns information about the plugin as a name/value array.
	 * The current keys are longname, author, authorurl, infourl and version.
	 *
	 * @returns Name/value array containing information about the plugin.
	 * @type Array 
	 */
	getInfo : function() {
		return {
			longname : "Content Browser",
			author : "Cerpus",
			authorurl : "http://www.cerpus.com",
			infourl : "http://www.cerpus.com",
		};
	},
	
	decodeElement : function(n) {
	  if($(n).attr('contentbrowser_id')) {
	    var css_class = $(n).attr('class');
		  var contentbrowser_id = $(n).attr('contentbrowser_id');
	    if($(n).find(".contentbrowser_link_text").html()) {
	      link_text = $(n).find(".contentbrowser_link_text").html().replace(/=/g, "\\=");
        //Remove any tags from link_text.
        link_text = link_text.replace(/<\/?(?!\!)[^>]*>/gi, '');
        contentbrowser_id = contentbrowser_id.replace(/==link_text=.*?==/, "==link_text=" + link_text + "==");
	    }
      //We could have a problem here while expecting css_class to always be last.
		  contentbrowser_id = contentbrowser_id.replace(/==css_class=[a-zA-Z_0-9 ]+/, "") + "==css_class=" + css_class;
		  $(n).before("[contentbrowser ==" + contentbrowser_id + "]").remove();
	  }
	},
	
	init : function(editor, url) {
	  var o = this;
		editor.addCommand('mceContentBrowser', function() { o.showContentBrowser(editor, url) });

		/********** Just before we save we need to make some changes to the html ***********/
		editor.onSaveContent.add(function(editor, data) {
			tinymce.each(editor.dom.select('div'), function(n) {
        o.decodeElement(n);
			});
			tinymce.each(editor.dom.select('span'), function(n) {
        o.decodeElement(n);
			});
			data.content = editor.getContent();
		});
					
		/********** Before we populate TinyMCE with content we need to convert the tags **********/
		editor.onBeforeSetContent.add(function(editor, data) {
      var matches = data.content.match(/\[contentbrowser\s?==(?:[^\]\[]*(?:\[[^\]]*\])*)*\]/g);
      if(matches) {
        $.ajax({
          url: tinyMCE.activeEditor.documentBaseURI.getURI() + "/contentbrowser/decode/editor",
          data: ({ 'tags[]': matches }),
          dataType: 'json',
          type: "POST",
          async: 0,
          cache: 1,
          success: function(encoded_tags) {
            for (i in encoded_tags) {
              data.content = data.content.replace(matches[i], encoded_tags[i]);
            }
          }
        });
        if(!data.content.match(/<p>&nbsp;<\/p>/)) {
          data.content += "<p>&nbsp;</p>";
        }
      }
		});		

		
		editor.addButton('contentbrowser', {
			title : 'Content Browser',
			cmd : 'mceContentBrowser',
			image : url + "/img/contentbrowser.gif",
		});
		
	},
	
	
	showContentBrowser: function(editor, url) {
		var drupal_path = url.split("sites/all")[0];
		var template = new Array();
		var var_sign = "?";
		template['file']   = drupal_path + "contentbrowser";
		template['width']  = 1000;
		template['height'] = 771;
		
		var selection = editor.selection.getNode();
		if($(selection).attr('contentbrowser_id')) {
		  matches = $(selection).attr('contentbrowser_id').match(/nid=([0-9]+)/);
		  if(matches && matches.length == 2) {
		    template['file'] += "/select?tag=" + encodeURI(escape($(selection).attr('contentbrowser_id').replace(/\\/g, '')));
		    var_sign = "&";
		  }
		}
		if(Drupal.settings.node_id) {
		  template['file'] += var_sign + "current_nid=" + Drupal.settings.node_id;
		}
		
		editor.windowManager.open({
      file: template['file'],
      width : template['width'] + parseInt(editor.getLang('amadeo_draft.delta_width', 0)),
      height : template['height'] + parseInt(editor.getLang('amadeo_draft.delta_height', 0)),
      inline : true,
      scrollbars : "yes"
    });

    return true;
	},
});

// Adds the plugin class to the list of available TinyMCE plugins
tinymce.PluginManager.add('contentbrowser', tinymce.plugins.ContentBrowser);
})();
