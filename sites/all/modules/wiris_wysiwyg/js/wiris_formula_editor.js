(function ($) {
  $(document).ready(function () {
  	var mathml = tinyMCEPopup.getWindowArg('mathml');
    var applet = document.getElementById('applet');
    var editExisting = false;

    function setAppletMathml() {
      try {
        if (applet.isActive && applet.isActive()) {
          applet.setContent(mathml);
        }
        else {
          setTimeout(setAppletMathml, 50);
        }
      }
      catch (e) {
        if (applet.isActive()) {
          applet.setContent(mathml);
        }
        else {
          setTimeout(setAppletMathml, 50);
        }
      }
    }

    if (mathml) {
      editExisting = true;
      $('#insert').val(Drupal.t('Update'));
      setAppletMathml();
    }
    else {
      $('#insert').val(Drupal.t('Insert'));
    }

    $('#insert').click(function() {
      var mathml = applet.getContent();

      if (editExisting) {
        var node = $(tinyMCEPopup.editor.selection.getNode());
        if (!node.is('span.Wirisformula')) {
          node = node.parents('span.Wirisformula');
        }
        if (node.length == 1) {
          // Select the <math> node, and replace it with new content
          mathNode = node.children().get(0);
          tinyMCEPopup.editor.execCommand('mceSelectNode', false, mathNode);
          tinyMCEPopup.editor.execCommand('mceReplaceContent', false, mathml);
          tinyMCEPopup.editor.selection.getSel().collapseToEnd();
        }
      }
      else {
        tinyMCEPopup.editor.execCommand('mceInsertContent', false, '<span class="Wirisformula">' + mathml + '</span>');
      }
      tinyMCEPopup.close();
      return false;
    });

    $('#cancel').click(function() {
      tinyMCEPopup.close();
      return false;
    });
  });
})(jQuery);
