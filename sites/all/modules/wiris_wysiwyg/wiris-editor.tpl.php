<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <?php print $scripts; ?>

    <title>WIRIS Formula Editor</title>
  </head>
  <body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
    <div style="height: 360px">
      <applet id="applet" codebase="<?php echo $wirisformulaeditorcodebase; ?>" archive="<?php echo $wirisformulaeditorarchive; ?>" code="<?php echo $wirisformulaeditorcode; ?>" height="100%" width="100%">
        <param name="lang" value="<?php echo $wirisformulaeditorlang; ?>" />
        <param name="menuBar" value="false"/>
      </applet>
    </div>
    <div style="margin-top: 3px">
      <input type="button" id="insert" value="" />
      <input type="button" id="cancel" value="<?php print t('Cancel'); ?>" />
    </div>
  </body>
</html>
