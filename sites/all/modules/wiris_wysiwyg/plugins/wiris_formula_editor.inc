<?php
// $Id$
/**
 * @file
 * @ingroup wiris_wysiwyg
 * @brief
 *  Include file for specifying the WYSIWYG plugin.
 */
/**
 * Implementation of hook_wysiwyg_plugin().
 */
function wiris_wiris_formula_editor_plugin() {
  global $base_url;

  $plugins['wiris_formula_editor'] = array(
    'title' => t('WIRIS Formula Editor'),
    'vendor url' => 'http://www.wiris.com/',
    'icon path' => drupal_get_path('module', 'wiris') . '/plugins/wiris_wysiwyg_plugins/images',
    'icon file' => 'wiris-formula.gif',
    'icon title' => 'Formula editor',
    'js path' => drupal_get_path('module', 'wiris') . '/plugins/wiris_wysiwyg_plugins',
    'js file' => 'wiris_wysiwyg.js',
    'css path' => drupal_get_path('module', 'wiris') . '/plugins/wiris_wysiwyg_plugins',
    'css file' => 'wiris_wysiwyg.css',
    'settings' => array(
      'wiris' => array(
        'baseurl' => base_path() . drupal_get_path('module', 'wiris'),
        'callback' => base_path() . 'index.php?q=/wiris',
      ),
      'dialog' => array(
        'url' => base_path() . 'index.php?q=wiris/editor',
        'width' => 500,
        'height' => 400,
        'scroll' => 'no',
        'resizable' => 'yes',
      ),
    ),
    //TinyMCE specific
    'extended_valid_elements' => array(
      "img[id|class|style]", 
      "math[xmlns]",
    ),
  );

  return $plugins;
}
