(function ($) {
  Drupal.wysiwyg.plugins['wiris_formula_editor'] = {
    /**
     * Return whether the passed node belongs to this plugin.
     */
    isNode: function(node) {
      // This returns true if cursor is inside a formula
      return ($(node).is('span.Wirisformula') || ($(node).parents('span.Wirisformula').length == 1));
    },

    /**
     * Execute the button.
     */
    invoke: function(data, settings, instanceId) {
      var t = this;
      if (data.format == 'html') {
        var options = {mathml: ''}

        if (t.wrapper) {
          options['mathml'] = t.wrapper.html();
        }

        Drupal.wysiwyg.instances[instanceId].openDialog(settings.dialog, options);
      }
    },

    /**
     * Event callback for fomulas
     *
     * This is called when events occur on an active MathML element.
     * Makes MathML formulas non-editable from TinyMCE. Also makes navigating
     * over formulas with the keypad slick.
     */
    _handleClicks : function(ed, e) {
      var k = e.keyCode;
      var Event = tinymce.dom.Event;

      if ((k == 37) || (k ==39)) {
        // If key left or right

        // Find the formula element
        var node = ed.selection.getNode();
        var w = $(node);
        if (!w.is('span.Wirisformula')) {
          w = $(node).parents('span.Wirisformula');
        }

        if (w.length && (k == 37)) {
          // Key left pressed

          // Add dummy spans to the left of the formula
          w.before('<span class="wiris_dummy"></span><span class="wiris_dummy"></span>');
        
          // This if _must_ be here! Probably related to timings
          // in event firing
          if (w.prev().attr('class') == 'wiris_dummy') {
            // Navigate to the leftmost of the dummy spans to move the cursor
            // to the left of the formula. These dummy spans are cleaned up
            // in the detach() function.
            ed.selection.select(w.prev().prev()[0]);
          }
        }

        if (w.length && (k == 39)) {
          // Key right pressed

          // Add dummy span to the right of the formula
          w.after('<span class="wiris_dummy"></span>');
        
          // This if _must_ be here! Probably related to timings
          // in event firing
          if (w.next().attr('class') == 'wiris_dummy') {
            // Move cursor inside dummy and remove it. This makes sure the
            // cursor is positioned to the right of the formula. These dummy
            // spans are cleaned up in the detach() function.
            ed.selection.select(w.next()[0]);
          }
        }
      }

      // Don't block arrow keys, pg up/down, and F1-F12
      if ((k > 32 && k < 41) || (k > 111 && k < 124))
        return;

      return Event.cancel(e);
    },


    /**
     * Event callback for node change
     *
     * Make sure the formula is not editable directly in TinyMCE
     */
    _handleChange: function(ed, cm, n) {
      var t = this;
      var isInside = t.isNode(n);

      if(isInside && !t.locked) {
        t.wrapper = $(n);
        if (!t.wrapper.is('span.Wirisformula')) {
          t.wrapper = $(n).parents('span.Wirisformula');
        }
        t.wrapper.addClass('wiris_active');

        ed.onKeyDown.addToTop(t._handleClicks);
        ed.onKeyPress.addToTop(t._handleClicks);
        ed.onKeyUp.addToTop(t._handleClicks);
        ed.onPaste.addToTop(t._handleClicks);

        t.locked = true;
      }
      else if (isInside && t.locked) {
        if (t.wrapper) {
          var w = $(n);
          if (!w.is('span.Wirisformula')) {
            w = $(n).parents('span.Wirisformula');
          }

          if (t.wrapper != w) {
            // The user has clicked directly from one formula to the next
            t.wrapper.removeClass('wiris_active');
            w.addClass('wiris_active');
            t.wrapper = w;
          }
        }
      }
      else if (!isInside && t.locked) {
        ed.onKeyDown.remove(t._handleClicks);
        ed.onKeyPress.remove(t._handleClicks);
        ed.onKeyUp.remove(t._handleClicks);
        ed.onPaste.remove(t._handleClicks);

        if (t.wrapper) {
          t.wrapper.removeClass('wiris_active');
          t.wrapper = null;
        }

        t.locked = false;
      }
    },


    /**
     * attach handler
     *
     * Make sure the formula is not editable directly in TinyMCE
     */
    attach: function(content, settings, instanceId) {
      // We need a pointer to 'this' object inside anonymous functions,
      // but when inside, 'this' has been redefined
      var t = this;

      // Protect formulas from being edited directly in TinyMCE
      t.editor = tinyMCE.editors[instanceId];

      if (typeof(t.locked) == 'undefined') {
        t.locked = false;
      }
      t.editor.onNodeChange.addToTop(function(ed, cm, n) {
        // Hack to let _handleChange access 'this'
        t._handleChange(ed, cm, n);
      });

      // Just return the untouched content
      return content;
    },

    /**
     * detach handler
     *
     * Remove markup used for editorial behaviour
     */
    detach: function(content, settings, instanceId) {
      content = content.replace(/ wiris_active/g, '');
      content = content.replace(/<span class="wiris_dummy">.*?<\/span>/g, '');

      return content;
    },
  };
})(jQuery);
