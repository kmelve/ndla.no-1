<?php
/**
 * Created by PhpStorm.
 * User: rolfguescini
 * Date: 19.10.14
 * Time: 15:24
 */ 
 
function ndla_fagontologi_admin_content_types_settings() {
  $options = node_get_types('names');
  $form = array();
  $form['ndla_fagontologi_enabled_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled content types'),
    '#options' => $options,
    '#default_value' => variable_get('ndla_fagontologi_enabled_content_types', array()),
  );

  return system_settings_form($form);
}

function ndla_fagontologi_admin_solr_settings() {
  $boosts = array(
    0 => t('No boost'),
    10 => 10,
    25 => 25,
    50 => 50,
    75 => 75,
    100 => 100,
    125 => 125,
    150 => 150,
    200 => 200,
  );
  
  $form = array(
    'ndla_fagontologi_search_solr' => array(
      '#title' => t('Search the topic names'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('ndla_fagontologi_search_solr', 0),
    ),
    'ndla_fagontologi_boost_topic_names' => array(
      '#title' => t('Amount of boost to give hits on topics'),
      '#type' => 'select',
      '#options' => $boosts,
      '#default_value' => variable_get('ndla_fagontologi_boost_topic_names', 0),
    ),
  );

  return system_settings_form($form);
}