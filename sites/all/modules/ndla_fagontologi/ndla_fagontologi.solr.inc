<?php

/**
 * Creates the proper name for the topics field.
 *  @return
 *  String.
 */
function ndla_fagontologi_topics_solr_key() {
  $topics = array(
    'name'       => 'topics',
    'multiple'   => TRUE,
    'index_type' => 'text',
  );

  return apachesolr_index_key($topics);
}

/**
 * Creates the proper name for the topics field.
 *  @return
 *  String.
 */
function ndla_fagontologi_topics_ids_solr_key() {
  $topics_ids = array(
    'name'       => 'topics_ids',
    'multiple'   => TRUE,
    'index_type' => 'string',
  );

  return apachesolr_index_key($topics_ids);
}

/**
 * Implementation of hook_apachesolr_update_index
 */
function ndla_fagontologi_apachesolr_update_index(&$document, $node) {
  $topics = ndla_fagontologi_get_raw_topics($node);

  if(!empty($topics)) {
    $ids = array();
    $names = array();
    foreach($topics as $id => $data) {
      $ids[] = $id;
      $names = array_merge($data, $names);
    }

    $document->setField(ndla_fagontologi_topics_solr_key(), $names);
    $document->setField(ndla_fagontologi_topics_ids_solr_key(), $ids);
  }
}

function ndla_fagontologi_apachesolr_modify_query(&$query, &$params, $caller) {
  if(variable_get('ndla_fagontologi_search_solr', 0)) {
    $params['qf'][] = ndla_fagontologi_topics_ids_solr_key();
    $topic_field = ndla_fagontologi_topics_solr_key();
    $boost = variable_get('ndla_fagontologi_boost_topic_names', 0);
    $boost = ($boost > 0) ? '^' . $boost : '';
    $params['qf'][] = $topic_field . $boost;
  }
}
