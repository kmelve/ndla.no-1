<?php
/**
 * Created by PhpStorm.
 * User: rolfguescini
 * Date: 19.10.14
 * Time: 15:26
 */

function ndla_fagontologi_form() {
  global $language;
  $node = null;

  if (isset($_GET['translation']) && arg(1) == "add"){
    $node = node_load($_GET['translation']);
  }
  else if(isset($node->clone_from_original_nid)){
    $node = node_load($node->clone_from_original_nid);
  }
  else{
    $node = node_load(arg(1));
  }

  $fagId = $_GET['fag'];
  $subject_ontology_mapping = 0;

  $subject_ontology_mapping_vars = variable_get('ndla_fagontologi_subjectOntology_activation',  array());

  if(is_numeric($fagId)){
    if (count($subject_ontology_mapping_vars[$fagId]) > 0){
      $subject_ontology_mapping = $subject_ontology_mapping_vars[$fagId];
    }
  }
  

  if($subject_ontology_mapping != 0){
    $ids = ndla_fagontologi_get_fag_ids(FALSE);
    $currentTopicIds = array();
    $currentTopicNames = array();
    $chosenCourse = "";
    if(isset($_GET['fag'])){
      if(in_array("subject_".$fagId,$ids)){
        $nodeTopics = null;
        $chosenCourse = $fagId;
        if(isset($node->nodeTopics)) {
          $nodeTopics = $node->nodeTopics;
          $currentTopicNames = array();
          $nameLanguage = 'http://psi.oasis-open.org/iso/639/#'.NdlaGrepLanguages::iso639_1_iso639_2($language->language);
          $neutral = 'http://psi.topic.ndla.no/#language-neutral';

          foreach($nodeTopics as $topicId => $topicData) {
            $name = '';
            if($topicId != "subject_matter"){
              foreach($topicData['names'] as $nameObject) {
                if(property_exists($nameObject,$nameLanguage)) {
                  $nameArray = get_object_vars($nameObject);
                  if($nameArray[$nameLanguage] != ""){
                    $name = utf8_decode($nameArray[$nameLanguage]);
                  }
                  else{
                    $name = utf8_decode($nameArray[$neutral]);
                  }
                }
              }

              $currentTopicNames[$topicId] = array("name" => $name);
              array_push($currentTopicIds,$topicId);
            }
          }

          $currentTopicIds = json_encode($currentTopicIds);
          $currentTopicNames = json_encode($currentTopicNames);
        }//end if set NodeTopics
      }
    }
    else{
      if(isset($node->nodeTopics)) {
        $nodeTopics = $node->nodeTopics;
        $currentTopicNames = array();
        $nameLanguage = 'http://psi.oasis-open.org/iso/639/#'.NdlaGrepLanguages::iso639_1_iso639_2($language->language);
        $neutral = 'http://psi.topic.ndla.no/#language-neutral';
        foreach($nodeTopics as $topicId => $topicData) {
          $name = '';
          if($topicId != "subject_matter"){
            if($topicData['names'][$nameLanguage] != ""){
              $name = utf8_decode($topicData['names'][$nameLanguage]);
            }
            else{
              $name = utf8_decode($topicData['names'][$neutral]);
            }
            $currentTopicNames[$topicId] = array("name" => $name);
            array_push($currentTopicIds,$topicId);
          }
        }

        $currentTopicIds = json_encode($currentTopicIds);
        $currentTopicNames = json_encode($currentTopicNames);
      }//end if set NodeTopics
    }


    $form['fagontologi'] = array(
      '#title' => t('Subject ontology'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['fagontologi']['kompetansemaalontologi'] = array(
      '#type' => 'textfield',
      '#title' => t('Node topics'),
      '#attributes' => array('class' => 'ndla-fagontologi-nodetopics'),
      '#default_value' => '',
      '#required' => FALSE,
      '#maxlength' => 4096,
      '#prefix' => '<span id="fagontologi-input-wrapper" class="tagger-plugin">',
      '#suffix' => '<a href="javascript:" onclick="showCurriculaOntology(\'/'.$language->language.'/ndla_fagontologi_get_curriculumOntology/'.$fagId.'/0\',\''.$language->language.'\');return false;" id="fagOntologiOpener">'.t("Competence aim ontology").'</a>'
    );





    $form['fagontologi']['chosenCurriculaTopics'] = array(
      '#type' => 'hidden',
      '#default_value' => $currentTopicIds,
    );

    $form['fagontologi']['chosenCourse'] = array(
      '#type' => 'hidden',
      '#default_value' => $chosenCourse,
    );

    $form['fagontologi']['chosenCurriculaTopicNames'] = array(
      '#type' => 'hidden',
      '#default_value' => $currentTopicNames,
    );

    $form['#after_build'][] = 'ndla_fagontologi_addScripts';
  }//end if Subject ontology

   return $form;
}

function ndla_fagontologi_addScripts($form, &$form_state){
  drupal_add_css(drupal_get_path('module', 'ndla_fagontologi') . "/css/bootstrap.css");
  drupal_add_css(drupal_get_path('module', 'ndla_fagontologi') . "/css/ndla_fagontologi_form.css");
  drupal_add_js(drupal_get_path('module', 'ndla_fagontologi') .'/js/jquery.eventspolyfill.js');
  drupal_add_js(drupal_get_path('module', 'ndla_fagontologi') .'/js/ndla_fagontologi.form.js');
  jquery_ui_add(array('ui.tabs'));
  return $form;
}

function ndla_fagontologi_get_CurriculumOntology($fagId,$curriculumCount){
  global $language;
  $courses = NdlaGrepNdlaClient::get_courses(array("subject_".$fagId));
  $set_ids = array(variable_get('ndla_grep_competence_aim_sets', array()));

  if(!empty($set_ids[0][$fagId])) {
    $set_ids = explode("\r\n", $set_ids[0][$fagId]);
  } else {
    $set_ids = array();
  }

  $udir_id = $courses[$curriculumCount]['id'];
  $mycurriculumEndpoint = $courses[$curriculumCount]['endpoint'];
  $curriculumName = $courses[$curriculumCount]['name'];

  $tree = array(
    'subjectId' => 'subject_'.$fagId,
    'curriculumName' => $curriculumName,
    'udirId' => $udir_id
  );

  $items = ndla_fagontologi_get_curriculum($mycurriculumEndpoint,$tree,$set_ids,array());
  $aimTopics = NDLAFagontologiClient::load_aimTopics("subject_".$fagId);
  error_log("aimTopics: ".print_r($aimTopics,1));
  print theme("ndla_fagontologi_aimTopics",$items,$aimTopics,NdlaGrepLanguages::iso639_1_iso639_2($language->language),$courses,$curriculumCount);
}




function ndla_fagontologi_get_curriculum($endpoint,$tree,$set_ids) {
  $items = array();
  $curriculum = array();
  $items['metadata'] = $tree;
  $curriculumTemp = NdlaGrepNdlaClient::get_curriculum($endpoint, $set_ids);
  foreach($curriculumTemp as $mainId => $mainData) {
    $mainName = $mainData['name'];
    $levelString = $mainName;
    $parenthesisPos = strpos($mainName,"(");
    if($parenthesisPos !== false){
      $mainName = substr($mainName,0,strpos($mainName,"("));
      $levelData = substr($levelString,($parenthesisPos+1),-1);
      $mainData['name'] = $mainName;
      $mainData['levelData'] = $levelData;

      $levelString = "";
      if(strpos($levelData,"Vg1") !== false && strpos($levelData,"Vg2") === false){
        $levelString = "VG1";
      }
      else if(strpos($levelData,"Vg2") !== false && strpos($levelData,"Vg1") === false){
        $levelString = "VG2";
      }
      else if(strpos($levelData,"Vg2") !== false && strpos($levelData,"Vg1") !== false){
        $levelString = "VG1/VG2";
      }
      else if(strpos($levelData,"Vg3") !== false){
        $levelString = "VG3";
      }
      $mainData['levelString'] = $levelString;
    }

    $curriculum[$mainData['id']] = $mainData;
  }
  ksort($curriculum);
  $items['curriculum'] = $curriculum;
  return $items;
}


function ndla_fagontologi_get_relation_types() {
  return array(
    'none' => t('None'),
    'related' => t('Related'),
  );
}

function ndla_fagontologi_form_alter(&$form, &$form_state, $form_id) {
  if (!empty($form['type']) && $form_id == $form['#node']->type . "_node_form") {
    if (ndla_fagontologi_is_type_enabled($form['#node']->type)) {
      $form['ndla_fagontologi'] = ndla_fagontologi_form($form_state, $form['#node']);
    }
  }

  if("fag_node_form" == $form_id && (arg(2) == 'edit' || arg(1) == 'add')) {

    $subject_ontology_mapping_vars = variable_get('ndla_fagontologi_subjectOntology_activation',  array());
    $subject_ontology_mapping = null;
    if(is_numeric(arg(1))){
      if (count($subject_ontology_mapping_vars[arg(1)]) > 0){
        $subject_ontology_mapping = $subject_ontology_mapping_vars[arg(1)];
      }
    }


    $form['ndla_fagontologi_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Subject ontology setings'),
      '#attributes' => array('class'=>'attachments'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );

    $form['ndla_fagontologi_settings']['activation_toggle'] = array(
      '#type' => 'radios',
      '#title' => t('Use Subject Ontologies'),
      '#options' => array("No","Yes"),
      '#required' => TRUE,
      '#default_value' => isset($subject_ontology_mapping)? $subject_ontology_mapping : 0,
    );

  }
}

function ndla_fagontologi_get_fag_ids($withName = FALSE) {
  $ids = array();
  if($withName){
    $result = db_query("SELECT nid, title FROM {node} WHERE type = 'fag' AND status = 1");
    while ($row = db_fetch_object($result)) {
      $ids[$row->nid] = $row->title;
    }
    print json_encode($ids);
  }
  else{
    $result = db_query("SELECT nid FROM {node} WHERE type = 'fag' AND status = 1");
    while ($row = db_fetch_object($result)) {
      $ids[] = 'subject_' . $row->nid;
    }
    return $ids;
  }
}


function _ndla_fagontologi_validate($node) {
}