<?php
/**
 * Created by PhpStorm.
 * User: rolfguescini
 * Date: 19.10.14
 * Time: 10:17
 */

class NDLAFagontologiClient {
  private static function _post($nodeId, $data) {
    $url = self::_build_url("POST", $nodeId);
    $response = drupal_http_request($url, array(
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
      ), 'POST', json_encode($data));
    if (!in_array($response->code, array(200, 201))) {
      return FALSE;
    }
    else {
      self::flush_cache_by_cid(self::_build_url("GET",$nodeId));
      return TRUE;
    }
  }

  private static function _delete($nodeId, $data) {
    if(is_array($data)){
      $url = self::_build_url("DELETE", $nodeId);
    }
    else{
      if(strpos($data,"ndlanode_") !== false){
        $url = self::_build_url("DELETE", $data.$nodeId);
        $data = array();
      }
    }

    $response = drupal_http_request($url, array(
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
      ), 'DELETE', json_encode($data));
    if (!in_array($response->code, array(200, 201))) {
      return FALSE;
    }
    else {
      self::flush_cache_by_cid(self::_build_url("GET",$nodeId));
      return TRUE;
    }
  }

  private static function _get($nodeId, $cache = TRUE) {
    $url = self::_build_url("GET", $nodeId);
error_log("URL: ".$url);
    if ($cache) {
      $data = cache_get($url, 'cache_ndla_fagontologi');
      if (!empty($data->data)) {
        return $data->data;
      }
    }

    $response = drupal_http_request($url, array('Accept' => 'application/json'));
    if ($response->code != 200) {
      return FALSE;
    }
    $data = json_decode(utf8_encode($response->data));
    if ($cache) {
      cache_set($url, $data, 'cache_ndla_fagontologi');
    }
    return $data;
  }

  private static function _build_url($method = 'GET', $nodeId) {
    $site_string = variable_get('ndla_ord_topic_site_string', 'ndla');
    $service_url = variable_get('ndla_ord_topic_service_url', 'http://topics.ndla.no');
    $url = "";

    switch ($method) {
      case "GET":
        if (isset($nodeId)) {
          if(is_numeric($nodeId)){
            $url = $service_url . '/rest/v1/topics/?filter[node]=ndlanode_' . $nodeId;
          }
          else{
            if(strpos($nodeId,"subject_") !== false){
              $url = $service_url . '/rest/v1/aimtopicsbysubject?filter[subject]='. $nodeId;
            }
            else if(strpos($nodeId,"ndlanode_") !== false){
              $url = $service_url . '/rest/v1/keywords/?filter[node]='. $nodeId;
            }
            else if(strpos($nodeId,"topic_") !== false){
              $topicIdArray = explode("_",$nodeId);
              $url = $service_url . '/rest/v1/topics/'. $topicIdArray[1];
            }
            else if(strpos($nodeId,"getnode") !== false){
              $nodeId = str_replace("getnode","ndlanode",$nodeId);
              $url = $service_url . '/rest/v1/nodes/'. $nodeId;
            }

          }

        }
        else{
          $url = $service_url . '/rest/v1/topics';
        }
        break;
      case "POST":

        if (isset($nodeId)) {
          $url = $service_url . '/rest/v1/sites/ndla/nodes/' . $nodeId . '/topics';
        }
        break;

      case "DELETE":

        if (isset($nodeId)) {
          if(is_numeric($nodeId)){
            $url = $service_url . '/rest/v1/sites/ndla/nodes/' . $nodeId . '/topics';
          }
          else{
            if(strpos($nodeId,"ndlanode_") !== false){
              $idArr = explode("_",$nodeId);
              $url = $service_url . '/rest/v1/sites/ndla/nodes/'. $idArr[1];
            }
         }

        }

        break;
    }

    return $url;
  }

  public static function get_cache_cid($nodeId) {
    return self::_build_url("GET", $nodeId);
  }

  public static function save_topics($nodeId, $topics) {
    if (!empty($topics)) {
      return self::_post($nodeId, $topics);
    }
    return TRUE;
  }

  public static function delete_topics($nodeId, $topics) {
    if (!empty($topics)) {
      return self::_delete($nodeId, $topics);
    }
    return TRUE;
  }

  public static function delete_nodeObject($nodeId) {
    if (!empty($nodeId)) {
      return self::_delete($nodeId,'ndlanode_');
    }
    return TRUE;
  }

  public static function checkForKeywords($nodeId) {
    $shouldDelete = false;
    $data = self::_get('ndlanode_'.$nodeId);
    if(count($data) < 1 && count($data->keyword) < 1) {
      $shouldDelete = true;
    }
    return $shouldDelete;
  }

  public static function nodeExists($nodeId,$isNewNode,$allReadySaved) {
    $nodeExists = FALSE;
    if(!$isNewNode && !$allReadySaved){
      $data = self::_get('getnode_'.$nodeId);
      $callers=debug_backtrace();
      if($data->topicId != "" && strpos($data->topicId,'ndlanode_') !== false){
        $nodeExists = TRUE;
      }
    }

    return $nodeExists;
  }

  public static function load_aimTopics($subjectId = NULL) {
    $data = self::_get($subjectId);
    $relations = array();
    foreach($data->topicbyaim->associations as $assoccationObject){
      $relation = array();
      $relation['associationType'] = $assoccationObject->associationType;
      $aimId = "";
      $topicId = "";
      $names = array();
      foreach($assoccationObject->rolePlayers as $rolePlayer) {
          if($rolePlayer->roleType == "http://psi.udir.no/ontologi/lkt/#kompetansemaal"){
            $aimIdString  = $rolePlayer->rolePlayer;
            $aimId = substr($aimIdString,strrpos($aimIdString,"/")+1);
          }
          else{
            $topicId = $rolePlayer->rolePlayer;
            foreach($rolePlayer->rolePlayerNames as $topicName) {
              $nameData = get_object_vars ($topicName);
              $nameLang = $nameData['language'];
              $names[$nameLang] = utf8_decode($nameData['name']);
            }
          }

        if(!isset($relations[$aimId])){
          if($aimId != ""){
            $relations[$aimId] = array();
          }

        }
        $relation['topicId'] = $topicId;
        $relation['names'] = $names;
        if($aimId != "" && $topicId != "") {
          array_push($relations[$aimId], $relation);
        }
      }


    }

    return $relations;
  }

  public static function load_Topic($topicId) {
    $topic = array();
    if(isset($topicId)) {
      $data = self::_get('topic_'.$topicId);

      $topic['topicId'] = $data->topicId;
      $topic['approved'] = $data->approved;
      $topic['processState'] = $data->processState;
      $names = array();

      for($i = 0; $i < count($data->names); $i++){
        $topicName = $data->names[$i];

        $nameArray = array();
        for($j = 0; $j < count($topicName->data); $j++){
          $nameData = (array) $topicName->data[$j];
          $languageKeys = array_keys($nameData);
          $nameArray[$languageKeys[0]] = $nameData[$languageKeys[0]];
        }
        $names[$topicName->wordclass] = $nameArray;
      }//end for names

      $topic['names'] = $names;
    }
    return $topic;
  }


  public static function load_nodeAimTopics($nodeId = NULL,$language) {

    $topics = NULL;

    if(isset($nodeId)) {
      $data = self::_get($nodeId);
      $topics = array();
      if(count($data->topic) > 0){
        foreach($data->topic as $topic) {
          $topics[$topic->topicId] = array(
            'psi' => $topic->psi,
            'approved' => $topic->approved,
            'processState' => $topic->processState,
            'descriptions' => array(),
            'images' => array(),
            'names' => array(),
            'types' => array(),
          );
          if(count($topic->descriptions) > 0) {
            foreach($topic->descriptions as $descriptionLanguage => $description) {
              $topics[$topic->topicId]['descriptions'][$descriptionLanguage] = $description;
            }
          }
          if(count($topic->images) > 0) {
            foreach($topic->images as $imageLanguage => $image) {
              $topics[$topic->topicId]['images'][$imageLanguage] = $image;
            }
          }
          if(count($topic->types) > 0) {
            foreach($topic->types as $typeObject) {
              $typeNames = array();
              foreach($typeObject->names as $typeNameLanguage => $typeName){
                $typeNames[$typeNameLanguage] = $typeName;
              }
              $topics[$topic->topicId]['types'][$typeObject->typeId] = $typeNames;
            }
          }
          if(count($topic->names) > 0) {
            foreach($topic->names as $nameObject)
            {
              foreach($nameObject->data as $nameLanguage => $name)
              $topics[$topic->topicId]['names'][$nameLanguage] = $name;
            }
          }
        }
      }
    }
    else{
      $topics = self::_get($nodeId);
    }

    return $topics;
  }


  public static function getNameLanguage($names,$language) {
    $nameLanguage = '';

    foreach($names as $nameObject){
      if(array_key_exists('http://psi.oasis-open.org/iso/639/#'.$language,$nameObject)){
        $nameLanguage = 'http://psi.oasis-open.org/iso/639/#'.$language;
        break;
      }
      else if(array_key_exists('http://psi.oasis-open.org/iso/639/#nob',$nameObject) && $nameLanguage == ""){
        $nameLanguage = 'http://psi.oasis-open.org/iso/639/#nob';
      }
      else if(array_key_exists('http://psi.oasis-open.org/iso/639/#nno',$nameObject) && $nameLanguage == ""){
        $nameLanguage = 'http://psi.oasis-open.org/iso/639/#nno';
      }
      else if(array_key_exists('http://psi.oasis-open.org/iso/639/#eng',$nameObject  ) && $nameLanguage == ""){
        $nameLanguage = 'http://psi.oasis-open.org/iso/639/#eng';
      }
      else if(array_key_exists('http://psi.topic.ndla.no/#language-neutral',$nameObject  ) && $nameLanguage == ""){
        $nameLanguage = 'http://psi.topic.ndla.no/#language-neutral';
      }
    }//end foreach

    return $nameLanguage;
  }

  public static function flush_cache_by_cid($cid) {
    cache_clear_all($cid,'cache_ndla_fagontologi',FALSE);
  }
}