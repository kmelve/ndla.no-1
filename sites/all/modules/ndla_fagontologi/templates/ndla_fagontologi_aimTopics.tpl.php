<?php
/**
 * Created by PhpStorm.
 * User: rolfguescini
 * Date: 24.10.14
 * Time: 11.54
 */
if(count($courses) > 1){
  ?>
  <div class="courseList">
    <?php
      for($i = 0; $i < count($courses); $i++) {
        $course = $courses[$i];
        if($i == $chosenCurriculum){
          ?>
          <div class="courseListActive" id="course_<?php echo $i?>"><i class="fa fa-book"></i> <?php echo $course['name'];?></div>
          <?php
        }
        else{
          ?>
          <div class="courseListInActive"  id="course_<?php echo $i;?>"><i class="fa fa-book"></i> <?php echo $course['name'];?></div>
        <?php
        }
      }
    ?>
  </div>
<?php
}
?>
<div class="tabbable tabs-left">
  <ul class="nav nav-tabs">
    <?php
    $mainAreaIdCounter = 0;
    foreach($aimTopics['curriculum'] as $mainAreaId => $mainAreaData) {
      if($mainAreaIdCounter == 0) {
        ?>
        <li class="active"><a class="mainLink"  data-main-toggle="<?php echo $mainAreaId ?>" href="javascript:"><?php echo $mainAreaData['name']; ?><span class="smallLevel">(<?php echo $mainAreaData['levelString']; ?>)</span></a><span class="mainAreaLevelPopUp" id="mainAreaLevelPopUp_<?php echo $mainAreaId ?>"><?php echo $mainAreaData['levelData']; ?></span></span></li>
      <?php
      }
      else{
        ?>
        <li><a class="mainLink"  data-main-toggle="<?php echo $mainAreaId ?>" href="javascript:"><?php echo $mainAreaData['name']; ?><span class="smallLevel">(<?php echo $mainAreaData['levelString']; ?>)</span></a><span class="mainAreaLevelPopUp"  id="mainAreaLevelPopUp_<?php echo $mainAreaId ?>"><?php echo $mainAreaData['levelData']; ?></span></span></li>
      <?php
      }
      $mainAreaIdCounter++;
    }
    ?>
  </ul>

  <div class="tab-content">
    <?php
    $aimCounter = 0;
    foreach($aimTopics['curriculum'] as $mainAreaId => $mainAreaData) {
    if($aimCounter == 0) {
    ?>
    <div class="tab-pane active" id="<?php echo $mainAreaId ?>">
      <?php
      }
      else{
      ?>
      <div class="tab-pane" id="<?php echo $mainAreaId ?>">
        <?php
        }
        ?>
        <div class="accordion" id="<?php echo $mainAreaId ?>_aimAccordion">
          <?php
          foreach ($mainAreaData['aims'] as $aimId => $aimData) {

            ?>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-aim-toggle="<?php echo $aimId ?>"  href="javascript:" data-parent="#<?php echo $mainAreaId ?>_aimAccordion">
                  <?php echo $aimData['name'] ?>
                </a>
              </div>

              <div id="<?php echo $aimId ?>_aimCollapse" class="accordion-body collapse" style="height: 0px;">
                <div class="accordion-inner" id="inner_<?php echo $aimId ?>">
                  <ul class="nav nav-pills">
                  <?php
                  if(count($topics[$aimId]) > 0){
                    foreach($topics[$aimId] as $topicData){
                      if($topicData['associationType'] == 'http://psi.topic.ndla.no/aim_has_topic' || $topicData['associationType'] == 'http://psi.topic.ndla.no/#aim_has_topic'){
                        $nameLanguage = 'http://psi.oasis-open.org/iso/639/#'.$language;
                        $neutral = 'http://psi.topic.ndla.no/#language-neutral';
                        $name = '';
                        if($topicData['names'][$nameLanguage] != ""){
                          $name = $topicData['names'][$nameLanguage];
                        }
                        else if(array_key_exists('http://psi.oasis-open.org/iso/639/#nob', $topicData['names'])){
                          $name = $topicData['names']['http://psi.oasis-open.org/iso/639/#nob'];
                        }
                        else if(array_key_exists('http://psi.oasis-open.org/iso/639/#nno', $topicData['names'])){
                          $name = $topicData['names']['http://psi.oasis-open.org/iso/639/#nno'];
                        }
                        else if(array_key_exists('http://psi.oasis-open.org/iso/639/#eng', $topicData['names'])){
                          $name = $topicData['names']['http://psi.oasis-open.org/iso/639/#eng'];
                        }
                        else if(array_key_exists($neutral, $topicData['names'])){
                          $name = $topicData['names'][$neutral];
                        }
                        ?>
                        <li class="active">
                          <a href="javascript:" data-topic-id="<?php echo $topicData['topicId'];?>"><?php echo $name;?></a>
                        </li>
                        <?php
                      }
                    }
                  }
                  ?>
                  </ul>
                </div>
              </div>

            </div>
          <?php
          }
          ?>
        </div>
      </div>
      <?php
      $aimCounter++;
      }//end foreach
      ?>
    </div>

  </div>