<?php
/**
 * @file
 * @ingroup utdanning_browsertest 
 */
?>
<html>
<head>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>

<body class="lightboxContent">
<h1>Browser test</h1>
<div id="content">
<div id="body">
<?php print $content; ?>
</div>
</div>
</body>
</html>

