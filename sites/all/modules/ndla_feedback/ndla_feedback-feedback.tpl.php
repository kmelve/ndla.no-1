<?php if(!empty($feedbacks)): ?>
<ul>
  <?php for ($i = 0, $s = count($feedbacks); $i < $s; $i++): ?>
    <li<?php $i == 0 ? print ' class="first"' : '' ?>>
      <h4>
        <a href="<?php print $feedbacks[$i]->href ?>">
          <?php print check_plain($feedbacks[$i]->title) ?>
        </a>
        <?php if (isset($feedbacks[$i]->content_type)): ?>
          (<?php print $feedbacks[$i]->content_type ?>)
        <?php endif ?>
      </h4>
      <?php if (isset($feedbacks[$i]->teaser)): ?>
        <p class="teaser">
          <?php
            print check_plain($feedbacks[$i]->teaser);
        ?>
        </p>
        <p class="date">
          <?php print check_plain($feedbacks[$i]->author) ?><?php if (isset($feedbacks[$i]->node)): ?>
            <?php print t('on') ?>
            <a href="<?php print $feedbacks[$i]->node->href ?>"><?php print check_plain($feedbacks[$i]->node->title) ?></a><?php endif ?>,
          <?php print $feedbacks[$i]->date ?>
        </p>
      <?php endif ?>
    </li>
  <?php endfor ?>
</ul>
<?php endif; ?>
