<?php

function ndla_feedback_drush_command() {
  $items = array();

  $items['update-likes-mark-update'] = array(
    'description' => dt('Sets all nodes to be updated.'),
  );

  $items['update-likes'] = array(
    'description' => dt('Updates the facebook likes table.'),
    'arguments' => array(
      'limit' => 'A select limit',
    ),
  );

  return $items;
}

function drush_ndla_feedback_update_likes($limit) {
  if(empty($limit)) {
    $limit = 500; 
  }
  $query = "SELECT n.nid FROM {node} n LEFT JOIN {ndla_feedback_likes} nf ON (n.nid = nf.nid) WHERE (marked_for_update IS NULL OR marked_for_update = 1) LIMIT %d";
  $result = db_query($query, $limit);
  $nids = array();
  while($item = db_fetch_object($result))
  {
    $nids[] = $item->nid;
  }
  if(empty($nids)) {
    return;
  }
  $groups = array_chunk($nids, 100);
  foreach($groups as $nids) {
    $data = ndla_feedback_get_facebook_like_counts($nids);
    foreach($data as $nid => $likes) {
      db_query('INSERT INTO {ndla_feedback_likes} (nid, marked_for_update, facebook_likes)
                VALUES (%d, 0, %d) ON DUPLICATE KEY UPDATE marked_for_update = 0, facebook_likes = %d',
                $nid, $likes, $likes);
    }
    $data = ndla_feedback_get_vote_counts($nids);
    foreach($data as $nid => $likes) {
      db_query('INSERT INTO {ndla_feedback_likes} (nid, marked_for_update, seria_likes)
                VALUES (%d, 0, %d) ON DUPLICATE KEY UPDATE marked_for_update = 0, seria_likes = %d',
                $nid, $likes, $likes);
    }
    
    $query = "SELECT count(*) number FROM {node} n LEFT JOIN {ndla_feedback_likes} nf ON (n.nid = nf.nid) WHERE (marked_for_update IS NULL OR marked_for_update = 1)";
    $result = db_query($query);
    $item = db_fetch_object($result);
    echo('Nodes to be updated: ' . $item->number . "\n");
  }
  drush_log($limit . ' nodes was updated.', 'success');
}

function drush_ndla_feedback_update_likes_mark_update() {
  $result = db_query('UPDATE {ndla_feedback_likes} SET marked_for_update = 1');
  drush_log('All nodes marked for update', 'success');
}