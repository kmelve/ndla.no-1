Drupal.behaviors['ndla_paragraphs.weight'] = -1;

Drupal.behaviors.ndla_paragraphs = function() {
 ndla_paragraph_events();
}

function ndla_paragraph_events() {
  $('#add-paragraph-button').mousedown(function (event) {
    tinyMCE.triggerSave();
    //Fix for bug when moving paragraphs
    $(tinyMCE.editors).each(function () {
        var id = this.id;
        if (id.match(/^edit-paragraphs-[\d]*-(right|left)-field/)) {
            tinyMCE.execCommand('mceRemoveControl', true, id);
        }
    });
  });

  $('.paragraph-delete').click(function(e) {
    e.preventDefault();
    var element_weight = $('.weight-value', $(this).parents('.fieldset-wrapper')).val();
    $('#ndla-paragraph-ahah-action').val('delete');
    $('#ndla-paragraph-ahah-element').val(element_weight);
    $('#add-paragraph-button').trigger('mousedown');
  });

  $('.paragraph-move-down').click(function(e) {
    e.preventDefault();
    var element_weight = $('.weight-value', $(this).parents('.fieldset-wrapper')).val();
    $('#ndla-paragraph-ahah-action').val('down');
    $('#ndla-paragraph-ahah-element').val(element_weight);
    $('#add-paragraph-button').trigger('mousedown');
  });

  $('.paragraph-move-up').click(function(e) {
    e.preventDefault();
    var element_weight = $('.weight-value', $(this).parents('.fieldset-wrapper')).val();
    $('#ndla-paragraph-ahah-action').val('up');
    $('#ndla-paragraph-ahah-element').val(element_weight);
    $('#add-paragraph-button').trigger('mousedown');
  });

}

ndla_paragraphs_preview = function() {
  var data = {};
  data['ingress_use'] = $('#edit-field-ingressvispaasiden-value').attr('checked');
  data['ingress_image'] = $('#edit-field-ingress-bilde-0-nid-nid').val();
  $(tinyMCE.editors).each(function(key, editor){
    if(editor.editorId == 'edit-field-ingress-0-value') {
      data['ingress_text'] = encodeURI(tinyMCE.editors[key].getContent());
    } else if(editor.editorId.match(/^edit-paragraphs-[\d]*-(right|left)-field/)) {
      data[editor.editorId] = encodeURI(tinyMCE.editors[key].getContent());
    }
  });
  var style = $('#modalContainer').attr('style');
  $('#modalContainer').attr('style', style + 'background-color: white;');
  $.ajax({
    type: 'POST',
    url: Drupal.settings.basePath + 'ndla_paragraphs/preview/build',
    data: data,
    success: function(data) {
      var url = $('#hidden-lightbox').attr('href');
      url = url.replace(/\/[^\/]*$/, '/' + data);
      $('#hidden-lightbox').attr('href', url);
      $('#hidden-lightbox').click();
    },
  });
}