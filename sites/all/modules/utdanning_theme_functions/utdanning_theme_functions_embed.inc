<?php

/**
 * @file
 * @ingroup utdanning_theme_functions
 */

/**
 * 
 */
function get_node_embed_by_type($node_type, $variables, $textarea) {
  // Insert textbox
 
  if ($textarea) {
$code = <<<EOF
    <div id="media-embed">
      <strong>
EOF;
    $code .= t('Embed');
$code .= <<<EOF
    </strong>
    <br />
    <textarea rows="1" cols="30"  name="embed-text" id="embed-text" onclick="this.focus();this.select();">
EOF;
  }
  
  // Insert the embedding for each node type
  switch ($node_type) {
    case 'image':
      $filepath = $variables['filepath'];
      $alt = $variables['alt'];
      $code .= <<<EOF
<img src="http://ndla.no/$filepath" alt="$alt" />

EOF;
      break;

    case 'audio':
      $filepath = $variables['filepath'];
      $code .= <<<EOF
<object height="24" width="290" data="http://ndla.no/sites/all/modules/audio/players/1pixelout.swf" type="application/x-shockwave-flash">
  <param value="http://ndla.no/sites/all/modules/audio/players/1pixelout.swf" name="movie" />    
  <param value="transparent" name="wmode" />
  <param value="false" name="menu" />
  <param value="high" name="quality" />
  <param value="soundFile=http://ndla.no/$filepath" name="FlashVars" />
  <embed height="24" width="290" flashvars="soundFile=http://ndla.no/$filepath" src="http://ndla.no/sites/all/modules/audio/players/1pixelout.swf"></embed>
</object>

EOF;
      break;

    case 'flashnode':
      $filepath = $variables['filepath'];
      $height = $variables['height'];
      $base = $variables['base'];
      $code .= <<<EOF
<object data="http://ndla.no/$filepath" id="swf12573292991" type="application/x-shockwave-flash" height="$height" width="575">
  <param value="default" name="swliveconnect">
  <param value="true" name="play">
  <param value="false" name="loop">
  <param value="false" name="menu">
  <param value="autohigh" name="quality">
  <param value="showall" name="scale">
  <param value="l" name="align">
  <param value="tl" name="salign">
  <param value="opaque" name="wmode">
  <param value="#FFFFFF" name="bgcolor">
  <param value="7" name="version">
  <param value="true" name="allowfullscreen">
  <param value="sameDomain" name="allowscriptaccess">
  <param value="575" name="width">
  <param value="$height" name="height">
  <param value="http://ndla.no/$base" name="base">
  <param value="http://ndla.no/$filepath" name="src">
  <param value="width=575&amp;height=$height" name="flashvars">
</object>

EOF;
      break;
      
      case 'video':
       $filepath = $variables['filepath'];
       $imagepath = $variables['imagepath'];
       $code .= <<<EOF
<object id="flashvideo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="428" height="337" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab">
  <param name="movie" value="http://ndla.no/sites/default/files/Player.swf" />
  <param name="FlashVars" value="file=$filepath&#38;image=http://ndla.no/$imagepath&#38;rotatetime=3&#38;autostart=false&#38;streamer=rtmp://stream.ndla.no/streamingvideo/" />
  <param name="quality" value="high" />
  <param name="wmode" value="window" />
  <param name="allowfullscreen" value="true" />    
  <embed name="flashvideo" allowScriptAccess="always"   src="http://ndla.no/sites/default/files/Player.swf" width="428" height="337" border="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="window" allowfullscreen="true" quality="high" flashvars="file=$filepath&#38;image=$imagepath&#38;rotatetime=3&#38;autostart=false&#38;streamer=rtmp://stream.ndla.no/streamingvideo/" />
  </embed>
</object>

EOF;
    break;

    case 'amendor_ios':
    case 'amendor_ios_task':
      $code .= theme('amendor_ios_external_embed', $variables['node']);
      break;
      
    case 'amendor_electure':
      $code .= theme('amendor_electure_external_embed', $variables['node']);
    break;
  }
  
  $code .= get_node_embed_copyright($variables);
  
  if($textarea) {
      // Close textbox
$code .= <<<EOF
      </textarea><br />
      <a href="#" onclick="$(':input[name=embed-text]').focus();$(':input[name=embed-text]').select(); return false;">
EOF;
    $code .= t('Select embed code');
    
$code .= <<<EOF
    </a>
  </div><!-- END media-embed -->

EOF;
    
  }

  return $code;
}

/**
 * 
 */
function get_node_embed_copyright($embed_vars) {
  $imgURL = ndla_utils_generate_url();
  $code = '';
  if ($embed_vars['opphav'] != null) {
    $code .= <<<EOF

<div style="border: 1px solid rgb(245, 245, 245); padding: 5px 5px 0; font-size: 80%; background-color: rgb(249, 249, 249); color: rgb(102, 102, 102); height: 30px; width: 45.5em; border: solid 1px #ebebeb">
  <div style="float: left;">
    <strong style="float:left">
EOF;
     $code .= t('Originator / usufruct');
     $code .= <<<EOF
    :</strong>
    <ul style="list-style-type: none; float: left; margin: 0; padding:0; display: inline;">

EOF;
    foreach($embed_vars['opphav'] as $nid =>  $title) {
      $code .= '<li style="display: inline; padding: 0 2px 0 2px"><a href="';
      $code .= url(NULL,array('absolute' => TRUE)) . '/aktor/' . $nid;
      $code .= '" target="_blank" style="color: #666666">'.$title.'</a></li>';  
      $code .= "\n";
    }
    $code .= <<<EOF
    </ul>
  </div>

EOF;
  }
  else {
    $code .=<<<EOF
      <div style="border: 1px solid rgb(245, 245, 245); padding: 5px 5px 0; font-size: 80%; background-color: rgb(249, 249, 249); color: rgb(102, 102, 102); height: 30px; width: 45.5em; border: solid 1px #ebebeb">
        <div style="float: left;">
          <strong style="float:left">
EOF;
          $code .= t('Originator / usufruct');
          $code .= <<<EOF
          :</strong><span>
EOF;
          $code .= t('No originator / usufruct');
          $code .= <<<EOF
        </span></div>
EOF;
  }
  $code .= <<<EOF
  <div style="float: right; margin-right: 5px; margin-top: 5px;max-width: 600px;">

EOF;
  if (!empty($embed_vars["cc"])) {
    if (($embed_vars["cc"] != 'copyrighted') && ($embed_vars["cc"] != 'gnu') && ($embed_vars["cc"] != 'publicdomain')  && ($embed_vars["cc"] != '')) {
      $code .= '  <a title="';
      $code .= $embed_vars["cc_title"];
      $code .= '" href="http://creativecommons.org/licenses/';
      $code .= $embed_vars["cc"];
      $code .= '/3.0/no/"><img style="border: none" src="'.$imgURL.'/sites/all/modules/creativecommons_lite/images/buttons_small/';
      $code .= $embed_vars["cc"];
      $code .= '.png" alt="Creative Commons license icon" /></a> ';
    }
    else {
       $code .= '<strong>' . $embed_vars["cc_title"] . '</strong> ';  
    }
  }
  else {
      $code .= '<strong>'.t('Not Licensed').'</strong> ';
  }
  
  $nodeid = $embed_vars['nid'] ? $embed_vars['nid'] : $embed_vars['node']->nid;
    
  $code .= ' <a title="Les på NDLA" href="'.url(NULL,array('absolute' => TRUE)).'/node/'.$nodeid.'" target="_blank"><img style="border: none; height: 15px" alt="Nasjonal Digital Læringsarena" src="'.$imgURL.'/sites/all/themes/ndla/img/ndlarektangular16h.png" /></a></div></div>';
  
  return $code;
}

