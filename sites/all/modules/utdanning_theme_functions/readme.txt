Name
==========
Theme Functions



Description
============
Defines AJAX callback functions used in the tabs on the top of organic group nodes. Implements a hook_form_alter to customize some forms to fit the design. 

This Module is made and maintained by http://utdanniong.no as a part of the dopler(Drupal OPen Learning Resources) project.

Authored by Max-Erik Hansen 



Instalation
============

How to install this, or things do remenber when installing.



Integration/dependencies/requirements
==============



Credits
============
Authored by Max-Erik Hansen
Contributors: ...


Support
===========
http://utdanning.no/dopler
dopler[at]utdanning.no
