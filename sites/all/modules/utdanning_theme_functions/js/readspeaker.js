/**
 * @file
 * @ingroup utdanning_theme_functions
 */

/* The selected text function */

var selectedString="";

function getSelectedHTML() {
  selectedString="";
  var rng=undefined;
  if (window.getSelection) {
    selobj = window.getSelection();
    if (!selobj.isCollapsed) {
      if (selobj.getRangeAt) {
        rng=selobj.getRangeAt(0);
      }
      else {
        rng = document.createRange();
        rng.setStart(selobj.anchorNode,selobj.anchorOffset);
        rng.setEnd(selobj.focusNode,selobj.focusOffset);
      }
      if (rng) {
        var DOM = rng.cloneContents();
        object = document.createElement('div');
        object.appendChild(DOM.cloneNode(true));
        selectedString=object.innerHTML;
      }
      else {
        selectedString=selobj;
      }
    }
  }
  else if (document.selection) {
    selobj = document.selection;
    rng = selobj.createRange();
    if (rng && rng.htmlText) {
      selectedString = rng.htmlText;
    }
    else if (rng && rng.text) {
      selectedString = rng.text;
    }
  }
  else if (document.getSelection) {
    selectedString=document.getSelection();
  }
}

function copyselected()
{
  setTimeout("getSelectedHTML()",50);
  return true;
}

function openAndRead() {
  window.open('','rs','width=380,height=180,toolbar=0');
  setTimeout("document.rs_form.submit();",500);
}

document.onmouseup = copyselected;
document.onkeyup = copyselected;


/* The expanding function */

function readspeaker(rs_call, rs_file_name)
{
  if (selectedString.length>0) {
    rs_call=rs_call.replace("/cgi-bin/rsent?","/enterprise/rsent_wrapper.php?");
  }
  savelink=rs_call+"&save=1&audiofilename="+rs_file_name;
  rs_call=rs_call+"&output=audio";
  rs_call=escape(rs_call);
  start_rs_table="<table><tr><td>";
  rs_embed="<object type='application/x-shockwave-flash' data='http://media.readspeaker.com/flash/readspeaker20.swf?mp3="+rs_call+"&autoplay=0&rskin=bump' height='20' width='250'><param name='movie' value='http://media.readspeaker.com/flash/readspeaker20.swf?mp3="+rs_call+"&autoplay=1&rskin=bump' /><param name='quality' value='high' /><param name='SCALE' value='exactfit' /><param name='wmode' value='transparent' /><embed wmode='transparent' src='http://media.readspeaker.com/flash/readspeaker20.swf?mp3="+rs_call+"&autoplay=1&rskin=bump' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwaveflash' scale='exactfit' height='20' width='250' /></embed></object>";
  rs_downloadlink="<br />" + Drupal.t("Speech-enabled by") + " <a href='http://www.readspeaker.com'>ReadSpeaker</a><br /><a href='"+savelink+"'>" + Drupal.t("Download audio") + "</a>";
  close_rs="<br /><a href='#' onclick='close_rs_div(); return false;'>" + Drupal.t("Close window") + "</a>";
  end_rs_table="</td></tr></table>";

  var x=document.getElementById('rs_div');

  x.innerHTML=start_rs_table+rs_embed+rs_downloadlink+end_rs_table;
}

function close_rs_div()
{
  var x=document.getElementById('rs_div');
  x.innerHTML="";
}

