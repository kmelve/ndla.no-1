jQuery_timeline(function() {
  function NDLA_Timeline(element_id) {
    this.element_id = element_id;
    this.wrapper = jQuery_timeline('#' + element_id);
    this.direction = 'newest';
    
    this.getData = function() {
      var exp = /\d+/;
      timeline = this;
      $.ajax({
        url: Drupal.settings.basePath + "ndla-timeline/"  + this.element_id.match(exp)[0],
        dataType: 'json',
        async: false,
        success: function(data) {
          timeline.setupTimeline(data);
        }
      });
    };
    
    /*
  	 * Keep the actual line from extending beyond the last item's date tab
  	 */
  	this.adjustLine = function() {
  		var $lastItem = jQuery_timeline('.item.last', this.wrapper);
  		var itemPosition = $lastItem.data('isotope-item-position', this.wrapper);
  		var dateHeight = $lastItem.find('.date').height();
  		var dateOffset = $lastItem.find('.date').position();
  		var innerMargin = parseInt($lastItem.find('.inner').css('marginTop'));
      var dateOffsetHeight = 0;
      try {
        dateOffsetHeight = dateOffset.top;
      }
      catch(e) {
        ;
      }
  		var lineHeight = itemPosition.y + innerMargin + dateOffsetHeight + (dateHeight / 2);
  		jQuery_timeline('.line', this.wrapper).height(lineHeight);
  	};
  	
    /*
  	 * Set the timestamp of the year markers to either the beginning or end of
  	 * the year
  	 */
  	this.updateYearMarkers = function(beginning, wrapper) {
  		jQuery_timeline('.year-marker', wrapper).each(function(){
  			var $this = jQuery_timeline(this);
  			var year = parseInt($this.find('.year').text());
  			var timestamp = getTimestamp(year, beginning);
  			$this.find('.timestamp').text(timestamp);
  		});
  	};
  	
    /**
  	 * Load the data into Isotope
  	 */
  	this.setupTimeline = function(data) {
      wrapper = this.wrapper;
      timeline = this;
  	  this.setupEvents();
  		// load the Handlebar templates into memory
  		var sourcePost = jQuery_timeline('.post-template', wrapper).html();
  		var postTemplate  = Handlebars.compile(sourcePost);
  		var sourceYear = jQuery_timeline('.year-marker-template', wrapper).html();
  		var yearMarkerTemplate  = Handlebars.compile(sourceYear);
  		var years = [];
  		jQuery_timeline.each(data, function(i, val){
  			// save the years so we can create year markers
  			var year = new Date(val.timestamp).getFullYear();
  			if (years.indexOf(year) < 0)
  				years[years.length] = year;

  			// combine data & templqate
  			var html = postTemplate(val);
  			jQuery_timeline(".ndla-timeline", wrapper).append(html);
  		});

  		// add a year marker for each year that has a post
  		jQuery_timeline.each(years, function(i, val){
  			var timestamp;
  			if (this.direction == 'newest')
  				timestamp = getTimestamp(val, false);
  			else
  				timestamp = getTimestamp(val, true);
  			var context = {year: val, timestamp: timestamp};
  			var html = yearMarkerTemplate(context);
  			jQuery_timeline('.ndla-timeline', wrapper).append(html);
  		});

  		jQuery_timeline('.ndla-timeline', wrapper).imagesLoaded(function(){
  			jQuery_timeline(this).isotope({
  				itemSelector : '.item',
  				transformsEnabled: true,
  				layoutMode: 'spineAlign',
  				spineAlign:{
  					gutterWidth: 56
  				},
  				getSortData: {
  					timestamp: function($elem){
  						return parseFloat($elem.find('.timestamp').text());
  					}
  				},
  				sortBy: 'timestamp',
  				sortAscending: false,
  				itemPositionDataEnabled: true
  			});
  		});

  		// add open/close buttons to each post
  		jQuery_timeline('.item.post', wrapper).each(function(){
  			jQuery_timeline(this).find('.inner').append('<a href="#" class="open-close"></a>');
  		});

  		jQuery_timeline('.item a.open-close', wrapper).click(function(e){
  			jQuery_timeline(this).siblings('.body').slideToggle(function(){
  				jQuery_timeline(timeline.element_id).isotope('reLayout');
  			});
  			jQuery_timeline(this).parents('.post').toggleClass('closed');
  			jQuery_timeline('.expand-collapse-buttons a', wrapper).removeClass('active');
  			e.preventDefault();
  		});

  		jQuery_timeline('.buttons a.expand-all').click(function(e){
  		  wrapper = jQuery_timeline(this).parents('.timeline-wrapper').first();
  			jQuery_timeline('.post .body', wrapper).slideDown(function(){
  				jQuery_timeline('.ndla-timeline', wrapper).isotope('reLayout');
  			});
  			jQuery_timeline('.post', wrapper).removeClass('closed');
  			jQuery_timeline('.expand-collapse-buttons a', wrapper).removeClass('active');
  			jQuery_timeline(this).addClass('active');
  			e.preventDefault();
  		});

  		jQuery_timeline('.buttons a.collapse-all').click(function(e) {
  		  wrapper = jQuery_timeline(this).parents('.timeline-wrapper').first();
  			jQuery_timeline('.post .body', wrapper).slideUp(function() {
  				jQuery_timeline('.ndla-timeline', wrapper).isotope('reLayout');
  			});
  			jQuery_timeline('.post', wrapper).addClass('closed');
  			jQuery_timeline('.expand-collapse-buttons a', wrapper).removeClass('active');
  			jQuery_timeline(this).addClass('active');
  			e.preventDefault();
  		});
  	};

  	this.setupEvents = function() {
  	  timeline = this;
  	  jQuery_timeline('.sort-buttons a').click(function(e){
    		// don't proceed if already selected
    		if (jQuery_timeline(this).hasClass('active')) {
    			return false;
    		}
        wrapper = jQuery_timeline(this).parents('.timeline-wrapper').first();
    		jQuery_timeline('.sort-buttons a', wrapper).removeClass('active');
    		jQuery_timeline(this).addClass('active');
    		var $yearMarkers = jQuery_timeline('.year-markers', wrapper);
    		if (jQuery_timeline(this).hasClass('sort-newest')){
    			wrapper = jQuery_timeline(this).parents('.timeline-wrapper').first();
    			timeline.updateYearMarkers(false, wrapper);
    			jQuery_timeline('.ndla-timeline', wrapper).isotope('reloadItems').isotope({sortAscending: false});
    		}
    		else{
    		  wrapper = jQuery_timeline(this).parents('.timeline-wrapper').first();
    			timeline.updateYearMarkers(true, wrapper);
    			jQuery_timeline('.ndla-timeline', wrapper).isotope('reloadItems').isotope({sortAscending: true});
    		}
    		e.preventDefault();
    	});

    	jQuery_timeline('.ndla-timeline', wrapper).resize(function(){ // uses "jQuery resize event" plugin
    	timeline.adjustLine();
    	});
  	};
  }
  

	for(i in Drupal.settings.ndla_timeline) {
    window[i] = new NDLA_Timeline(i); //.setupTimeline(Drupal.settings.ndla_timeline[i]);
    window[i].getData();
	}
});

	/**
	 * Get the timestamp (milliseconds) given the year.
	 * If beginning is true, timestamp is 1/1/year 12:00:00:000
	 * If beginning is false, timestamp is 12/31/year 23:59:59:999
	 */
	function getTimestamp(year, beginning){
		if (beginning)
			return Date.parse('January 1, ' + year);
		else
			return Date.parse('December 31, ' + year) + 86400000 - 1 ; // plus 1 day, minus 1 millisecond
	}

/*
 * Isotope custom layout mode spineAlign
 */

jQuery_timeline.Isotope.prototype._spineAlignReset = function() {
	this.spineAlign = {
		colA: 0,
		colB: 0,
		lastY: -60
	};
};
jQuery_timeline.Isotope.prototype._spineAlignLayout = function( $elems ) {
	var instance = this,
		props = this.spineAlign,
		gutterWidth = Math.round( this.options.spineAlign && this.options.spineAlign.gutterWidth ) || 0,
		centerX = Math.round(this.element.width() / 2);

	$elems.each(function(i, val){
		var $this = jQuery_timeline(this);
		$this.removeClass('last').removeClass('top');
		if (i == $elems.length - 1)
			$this.addClass('last');
		var x, y;
		if ($this.hasClass('year-marker')){
			var width = $this.width();
			x = centerX - (width / 2);
			if (props.colA >= props.colB){
				y = props.colA;
				if (y == 0) $this.addClass('top');
				props.colA += $this.outerHeight(true);
				props.colB = props.colA;
			}
			else{
				y = props.colB;
				if (y == 0) $this.addClass('top');
				props.colB += $this.outerHeight(true);
				props.colA = props.colB;
			}
		}
		else{
			$this.removeClass('left').removeClass('right');
			var isColA = props.colB >= props.colA;
			if (isColA)
				$this.addClass('left');
			else
				$this.addClass('right');
			x = isColA ?
				centerX - ( $this.outerWidth(true) + gutterWidth / 2 ) : // left side
				centerX + (gutterWidth / 2); // right side
			y = isColA ? props.colA : props.colB;
			if (y - props.lastY <= 60){
				var extraSpacing = 60 - Math.abs(y - props.lastY);
				$this.find('.inner').css('marginTop', extraSpacing);
				props.lastY = y + extraSpacing;
			}
			else{
				$this.find('.inner').css('marginTop', 0);
				props.lastY = y;
			}
			props[( isColA ? 'colA' : 'colB' )] += $this.outerHeight(true);
		}
		instance._pushPosition( $this, x, y );
	});
};

jQuery_timeline.Isotope.prototype._spineAlignGetContainerSize = function() {
	var size = {};
	size.height = this.spineAlign[( this.spineAlign.colB > this.spineAlign.colA ? 'colB' : 'colA' )];
	return size;
};
jQuery_timeline.Isotope.prototype._spineAlignResizeChanged = function() {
	return true;
};

// Add indexOf to browsers that lack them. (IEs)
if(!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(needle) {
    for(var i = 0; i < this.length; i++) {
      if(this[i] === needle) {
        return i;
      }
    }
    return -1;
  };
}

