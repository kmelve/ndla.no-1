<div class="timeline-wrapper" id="timeline-block-<?php print $nid; ?>">
<div class="page">
  <div role="main" class="clearfix">
    <div class="buttons">
      <div class="expand-collapse-buttons">
        <a class="expand-all active" href="javascript:void(0);"><span><?php print t('Expand all'); ?></span></a>
        <a class="collapse-all" href="javascript:void(0);"><span><?php print t('Collapse all'); ?></span></a>
      </div>
      <div class="sort-buttons">
        <a class="sort-newest active" href="javascript:void(0);"><span><?php print t('Newest first'); ?></span></a>
        <a class="sort-oldest" href="javascript:void(0);"><span><?php print t('Oldest first'); ?></span></a>
      </div>
    </div>
    <div class="ndla-timeline">
      <div class="line-container">
        <div class="line"></div>
      </div>
    </div>
  </div>
</div>

<script class="year-marker-template" type="text/x-handlebars-template">
  <div class="item year-marker">
    <div class="inner">
      <div class="inner2">
        <div class="timestamp">{{timestamp}}</div>
        <div class="year">{{year}}</div>
      </div>
    </div>
  </div>
</script>

<script class="post-template" type="text/x-handlebars-template">
  <div class="item post">
    <div class="inner">
      <div class="timestamp">{{timestamp}}</div>
      <div class="title"><h3>{{title}}</h3></div>
      <div class="date">{{display_date}}</div>
      <div class="body">
        {{#if photo_url}}
          <img src="{{photo_url}}" alt="">
        {{/if}}
        {{#if caption}}
          <div class="caption">{{caption}}</div>
        {{/if}}
        {{#if body}}
          <div class="text">{{body}}</div>
        {{/if}}
        <div class="clearfix read-more">
        {{#if read_more_url}}
          <?php print t('Read more'); ?>: <a target="_blank" class="more" href="{{read_more_url}}">{{more}}</a>
        {{/if}}
        </div>
      </div>
    </div>
  </div>
</script>
</div>