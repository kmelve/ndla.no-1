<?php 
// $Id$

/**
 * Implementation of hook_menu
 */
function quiz_ty_menu() {
  $items = array();
  // AJAX Callback for loading questions
  $items['quiz_ty/ajax/%node/load_questions'] = array( 
    'page callback' => 'quiz_ty_load_questions_ajax',
    'type' => MENU_CALLBACK,
    'page arguments' => array(2),
    'access callback' => 'quiz_take_access',
    'access arguments' => array(2),
    'file' => 'quiz_ty.pages.inc',
  );
  return $items;
}

/**
 * Implementation of hook_theme
 */
function quiz_ty_theme() {
  return array(
    'quiz_ty_navigation' => array(
      'path' => drupal_get_path('module', 'quiz_ty') .'/theme',
      'file' => 'quiz_ty.theme.inc',
    ),
    'quiz_ty_taking' => array(
      'arguments' => array('first_question_node' => NULL, 'question_ids' => array(), 'quiz' => NULL),
      'path' => drupal_get_path('module', 'quiz_ty') .'/theme',
      'file' => 'quiz_ty.theme.inc',
    ),
    'quiz_ty_multichoice_report' => array(
      'path' => drupal_get_path('module', 'quiz_ty') .'/theme',
      'template' => 'quiz-ty-multichoice-report',
    ),
    'quiz_ty_report' => array(
      'path' => drupal_get_path('module', 'quiz_ty') .'/theme',
      'template' => 'quiz-ty-report',
    ),
    'quiz_ty_multichoice_alternatives' => array(
      'arguments' => array(
        'form' => NULL,
      ),
      'path' => drupal_get_path('module', 'quiz_ty') .'/theme',
      'template' => 'quiz-ty-multichoice-alternatives',
    ),
    'quiz_ty_multichoice_answering_form' => array(
      'arguments' => array(
        'form' => NULL,
      ),
      'path' => drupal_get_path('module', 'quiz_ty') .'/theme',
      'template' => 'quiz-ty-multichoice-answering-form',
    ),
    'quiz_ty_progress' => array(
      'arguments' => array('question_number' => NULL, 'num_questions' => NULL),
      'file' => 'quiz_ty.theme.inc',
      'path' => drupal_get_path('module', 'quiz_ty') .'/theme',
    ),
  );
}
/**
 * Implementation of hook_form_FORM_ID_alter
 * 
 * Adding options on how to take a quiz to the quiz node form.
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function quiz_ty_form_quiz_node_form_alter(&$form, &$form_state) {
  $form['taking']['quiz_ty'] = array(
    '#type' => variable_get('quiz_ty_all', 0) == 0 ? 'checkbox' : 'hidden',
    '#title' => t('Self test'),
    '#description' => t('By choosing this option you make this quiz a self test. The users responses will not be stored, but taking the quiz will be a lot faster.'),
    '#weight' => -1,
    '#default_value' => isset($form['#node']->quiz_ty) ? $form['#node']->quiz_ty : 0,
  );
}

/**
 * Implementation of hook_form_FORM_ID_alter
 * 
 * Adding extra data to the answering forms
 * 
 * @param $form
 * @param $form_state
 */
function quiz_ty_form_quiz_question_answering_form_alter(&$form, &$form_state) {
  // The third parameter tells us to add nid to the form id.
  if ($form['#parameters'][3] === TRUE) {
    $node = $form['#parameters'][2];
    $form['#id'] = 'quiz-question-answering-form-' . $node->nid;
    unset($form['navigation']);
    
    require_once drupal_get_path('module', 'quiz_ty') . '/quiz_ty.pages.inc';
    switch ($node->type) {
      case 'multichoice':
        $form['#theme'] = 'quiz_ty_multichoice_answering_form';
        $form['tries[answer]']['#theme'] = 'quiz_ty_multichoice_alternatives';
        _quiz_ty_add_multichoice_data($form);
        break;
      default:
        drupal_set_message(t('Only multichoice questions can be used in self-tests'), 'error');
        break;
    }
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter
 * 
 * Adding option for making all quizzes self tests to the quiz configuration form
 * 
 * @param $form
 * @param $form_state
 */
function quiz_ty_form_quiz_admin_settings_alter(&$form, &$form_state) {
  $form['quiz_addons']['quiz_ty_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make all quizzes self tests'),
    '#description' => t('Enabling this option will make all quizzes self tests. The self test option for each quiz will be hidden.'),
    '#default_value' => variable_get('quiz_ty_all', 0),
  );
}

/**
 * Implementation of hook_nodeapi
 */
function quiz_ty_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if ($node->type == 'quiz') {
    switch ($op) {
      case 'delete':
        db_query('DELETE FROM {quiz_ty} WHERE nid = %d', $node->nid);
        break;
      case 'delete revision':
        db_query('DELETE FROM {quiz_ty} WHERE vid = %d', $node->vid);
        break;
      case 'insert':
        db_query('INSERT INTO {quiz_ty}(nid, vid, quiz_ty)
                  VALUES(%d, %d, %d)', $node->nid, $node->vid, $node->quiz_ty);
        break;
      case 'load':
        $node->quiz_ty = db_result(db_query('SELECT quiz_ty FROM {quiz_ty} WHERE vid = %d', $node->vid));
        break;
      case 'update':
        if (isset($node->revision) && $node->revision == 1) {
          db_query('INSERT INTO {quiz_ty}(nid, vid, quiz_ty)
                    VALUES(%d, %d, %d)', $node->nid, $node->vid, $node->quiz_ty);
        } else {
          db_query('UPDATE {quiz_ty}
                    SET quiz_ty = %d
                    WHERE vid = %d', $node->quiz_ty, $node->vid);
        }
        break;
      case 'view':
        break;
    }
  }
}

/**
 * Implementation of hook_menu_alter
 */
function quiz_ty_quiz_take_alter(&$quiz) {
  if (isset($quiz->quiz_ty) && $quiz->quiz_ty == 1 || variable_get('quiz_ty_all', 0) == 1) {
    $path = drupal_get_path('module', 'quiz_ty') . '/quiz_ty.pages.inc';
    require_once $path;
    $quiz->rendered_content = quiz_ty_take($quiz);
  }
}