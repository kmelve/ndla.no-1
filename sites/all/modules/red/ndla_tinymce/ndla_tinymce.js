Drupal.behaviors.ndla_tinymce = function() {
  $('.ndla-form-step').click(ndla_tinymce_set_height);
  setTimeout("ndla_tinymce_set_height();", 1000);
}

ndla_tinymce_set_height = function() {
  if(typeof tinymce != 'undefined') {
    jQuery.each(tinymce.editors, function(_, editor) {
      editor.execCommand('mceAutoResize');
    });
  }
}

/* TGP-4817, tinyMCE autoactivate paste as plain text */
Drupal.behaviors.tinymce_pastetext_fix = function(context) {
  if(typeof Drupal.settings.wysiwyg != 'undefined') {
    jQuery.each(Drupal.settings.wysiwyg.configs.tinymce, function(_, instance) {
      // Set a callback for when the tinyMCE instance is called
      instance.init_instance_callback = function(inst) {
        // Activate paste plain text function in the "paste"
        // plugin
        inst.pasteAsPlainText = true; // Activate paste plain text 
        // Activate the button in tinyMCE UI
        inst.controlManager.setActive("pastetext", true); // Activate the button in UI
      };
    });
  }
};

