<?php

/***
 * Module is designed to separate customized code and general code.
 * Manipulate this file to add programmatically changes that are specific for single site and content type.
 *
 * If varibles need to be stored and fetched from database you can use:
 * utdanning_create_course_variable_set($name, $value)
 * utdanning_create_course_variable_get($name, $default)
 * utdanning_create_course_variable_del($name)
 *
 * See utdanning_create_course.inc for details.
 */


/**
 * Return array with content types where simplified mode is accessible.
 * @return array
 */
function utdanning_create_course_profiles_content_types() {
    return array('fag' => 'fag');
}


/**
 * Function that is called during node preparation in nodeapi.
 * @param string $type Content type.
 * @param string $field Field in node object.
 * @param mixed $oldvalue Original value, not in use.
 * @return mixed New value, or NULL.
 */
function utdanning_create_course_profiles_node_prepare($type, $field, $oldvalue = FALSE) {
    $data = array(
        'fag' => array(
            'language' => '',
            'status' => '0',
            'created' => '1',
            'comment' => '0',
            'promote' => '0',
            'moderate' => '0',
            'sticky' => '0',
            'tnid' => '0',
            'translate' => '0',
            'title' => '',
            'body' => '',
            'log' => '',
            'format' => '0',

            'field_vis_i_lister' => array(
                0 => array('value' => '0'),
            ),

            'og_selective' => '3',
            'og_theme' => NULL,
            'og_register' => '1',
            'og_directory' => '1',
            'og_notification' => '0',
            'og_language' => '',
            'og_private' => '0',

            'print_mail_display' => 0,
            'print_mail_display_comment' => 0,
            'print_mail_display_urllist' => 0,
            'print_pdf_display' => 0,
            'print_pdf_display_comment' => 0,
            'print_pdf_display_urllist' => 0,
            'print_display' => 1,
            'print_display_comment' => 0,
            'print_display_urllist' => 1,

        ),
    );
    if ($data[$type][$field]){
        return $data[$type][$field];
    }
    return NULL;
}



/**
 * Programmatically updates fields that show up in hook_form_alter.
 *
 * @param string $type Content type.
 * @param string $field Field id/name.
 * @param array $oldvalue
 * @return array Returns an updated "old value" or unmodified oldvalue.
 */
function utdanning_create_course_profiles_form_alter($type, $field, $oldvalue) {

    if (mb_substr($field, 0, 1) == '#') {
        return $oldvalue;
    }

    if (is_array($oldvalue) && $oldvalue['#type'] == 'value' || $oldvalue['#type'] == 'hidden') {
        return $oldvalue;
    }

    switch($type) {
        case 'fag' :
            switch ($field) {
                case 'title':
                    $oldvalue['#weight'] = -1;
                    break;
                case 'og_description':
                    $oldvalue['#prefix'] = '<div style="display:none">';
                    $oldvalue['#default_value'] = "Ett fag";
                    $oldvalue['#suffix'] = '</div>';
                    break;
                case 'field_contactperson':
                    $oldvalue['#weight'] = 8;
                    break;
                case 'group_ingressboks':
                    $oldvalue['#weight'] = 9;
                    break;
                case 'buttons':
                    $oldvalue['#weight'] = 999;
                    break;
                case 'language':
                    $oldvalue['#default_value'] = '';
                    $oldvalue['#prefix'] = '<div style="display:none">';
                    $oldvalue['#suffix'] = '</div>';
                    break;
                case 'utdanning_gajax_fag':
                    $oldvalue['#weight'] = 10;
                    break;
                default :
                    $oldvalue['#prefix'] = '<div style="display:none">';
                    $oldvalue['#suffix'] = '</div>';
                    break;
            }
            break;
    }


    return $oldvalue;
}



/**
 * Function that handles what will happen after node is created.
 *
 * @param object $node Newly created node object.
 * @return string Information to post on "finalize" page.
 */
function utdanning_create_course_profiles_finalize($node) {

    $success = TRUE;

    $form = array();
    $form['node'] = array(
        '#type' => 'item',
        '#value' => t("Created new node !link of type %type.", array('!link' => l($node->title, 'node/' . $node->nid), '%type' => $node->type)),
        '#weight' => 1
    );

    switch ($node->type) {
        case 'fag' :

            $name = 'Tema ' . $node->title;
            $tmp = array();
            $tmp['name'] = $name;
            $tmp['description'] = '';
            $tmp['help'] = '';

            $tmp['relations'] = '1';
            $tmp['hierarchy'] = '1';
            $tmp['multiple'] = '1';
            $tmp['required'] = '0';
            $tmp['tags'] = '0';
            $tmp['module'] = 'utdanning_create_course';
            $tmp['weight'] = '0';
            $tmp['language'] = '';
            $tmp['nodes'] =  array(
                'biblio' => 'biblio',
                'fagstoff' => 'fagstoff',
                'mmenu' => 'mmenu',
                'oppgave' => 'oppgave',
                'quiz' => 'quiz',
                'test' => 'test',
                'veiledning' => 'veiledning',
            );

            $status = taxonomy_save_vocabulary($tmp);
            $db = db_query("SELECT * FROM {vocabulary} WHERE module = '%s' ORDER BY vid DESC LIMIT 1", 'utdanning_create_course');
            $data = db_fetch_object($db);
            if (!$data || intval($status) < SAVED_NEW || intval($status) > SAVED_UPDATED) {
                $success = FALSE;
            }
            else {
                og_vocab_write_record($node->nid, $data->vid);

                // Store a reference so we can track what actually is considered "tema".
                $variable = utdanning_create_course_variable_get($node->type . '_og_vocab_vid', array());
                $variable[$data->vid] = $data->vid;
                utdanning_create_course_variable_set($node->type . '_og_vocab_vid', $variable);

                // Taxonomy super select store things in variable table.
                $ss = array(
                    'types' => array(
                        'fagstoff' => 'fagstoff',
                        'mmenu' => 'mmenu',
                        'multichoice' => 'multichoice',
                        'oppgave' => 'oppgave',
                        'quiz' => 'quiz',
                        'biblio' => 'biblio',
                        'test' => 'test',
                        'veiledning' => 'veiledning',
                    ),
                    'parents' => 1,
                    'image' => 0,
                );
                variable_set('taxonomy_super_select_vid_'. $data->vid, $ss);


                $form['tema'] = array(
                    '#type' => 'item',
                    '#value' => t("Created vocabulary !tema and assosiated it with node !link.", array('!tema' => l($name, 'node/' . $node->nid . '/og/vocab'), '!link' => l($node->title, 'node/' . $node->nid))),
                    '#weight' => count($form) + 1,
                );
                if (defined('I18N_TAXONOMY_TRANSLATE') && function_exists('i18ntaxonomy_vocabulary')) {
                    i18ntaxonomy_vocabulary($data->vid, I18N_TAXONOMY_TRANSLATE);
                }
            }

            break;
    }

    $form['summary'] = array(
            '#type' => 'item',
            '#value' => t("Creating of %type appear to have been successfull.", array('%type' => $node->type)),
            '#weight' => count($form) + 1,
    );

    if (!$success) {
        $form['tema']['#value'] = t("Creating of %type didn't succeed with all steps. Please contact site administrator.", array('%type' => $node->type));
    }

    return drupal_render($form);
}
