<?php


/**
 * @file utdanning_create_course.module
 *
 * Short description of this module:
 * Module to simplify creating of content.
 *
 * Longer description of this module:
 * Module to simplify creating of content. Adds a tab to create content on selected content types,
 * where users have access to a simplified GUI for creating content. How this module behaves are
 * programmatically controlled within utdanning_create_courses_profiles.inc file.
 * Customization of module is suitable for advanced users only.
 *
 * @author Jan Henrik Hasselberg
 *
 * @version 6.x-1.0
 *
 * @todo See todo in utdanning_create_course_nodeapi function.
 *
 * @warning Recommended to apply on stable solution only since module use many customized tweaks.
 *
 * @license
 * Copyright (C) Utdanning.no
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LISENCE.txt file or the
 * GNU General Public License for more details.
 */


/**
 * Implementation of hook_init().
 * Includes necisary files.
 */
function utdanning_create_course_init() {
    include_once drupal_get_path('module', 'utdanning_create_course') . '/utdanning_create_course.inc';
    include_once drupal_get_path('module', 'utdanning_create_course') . '/utdanning_create_course_profiles.inc';
}


/**
 *Implementation of hook_menu().
 * Adds menu item for each content type configured in utdanning_create_course_profiles_content_types.
 * @see utdanning_create_course_profiles_content_types()
 * @return array Array or void.
 */
function utdanning_create_course_menu() {
    
    // File not included when running update.php, so a check is necisary.
    if (!function_exists('utdanning_create_course_profiles_content_types')) {
        include_once drupal_get_path('module', 'utdanning_create_course') . '/utdanning_create_course_profiles.inc';
    }

    $courses = utdanning_create_course_profiles_content_types();

    foreach ($courses as $c) {
        $basepath = 'node/add/' . $c . '/simplified';
        $items[$basepath] = array(
            'title' => ucfirst($c) .  " (simplified mode)",
            'description' => 'Simplifies creating of ' . ucfirst($c) . ' content.',
            'access arguments' => array('create ' . $c . ' content'),
            'page callback' => 'utdanning_create_course_wizard',
            'type' => MENU_LOCAL_TASK,
        );
    }

    if (isset($items)) {
        return $items;
    }
}


/**
 *Implementation of hook_menu_alter().
 * Alter menu item for content types so we get a tabbed interface.
 * @param array $items
 */
function utdanning_create_course_menu_alter(&$items) {
    $courses = utdanning_create_course_profiles_content_types();
    foreach ($courses as $c) {
        $items['node/add/' . $c . '/standard'] = $items['node/add/' . $c];
        $items['node/add/' . $c . '/standard']['type'] = MENU_DEFAULT_LOCAL_TASK;
    }
}


/**
 *Function that is called when user visits node/add/ <content type> /simplified
 * @global boolean $utdanning_create_course_wizard Variable if we are on path for simplified form.
 * @param string $step Parameter from url.
 * @return string
 */
function utdanning_create_course_wizard($step = FALSE) {
    $courses = utdanning_create_course_profiles_content_types();
    foreach ($courses as $c) {
        if (arg(2) == $c) {
            define('utdanning_create_course_name', $c);
        }
        continue ;
    }

    if (defined('utdanning_create_course_name')) {

        if ($step == 'finalize') {
            drupal_set_title(t("Create") . ' ' . ucfirst(utdanning_create_course_name));
            return utdanning_create_course_finalize();
        }

        if (!$_GET['destination']) {
            drupal_goto($_GET['q'] , 'destination=' . $_GET['q'] . '/finalize');
        }

        if (!$step && $_GET['destination'] == $_GET['q'] . '/finalize') {
            $types = node_get_types();
            if (!isset($types[utdanning_create_course_name])) {
                return t('Error! Module or drupal site is not configured properly. Please contact system administrator.');
            }

            global $utdanning_create_course_wizard;
            $utdanning_create_course_wizard = TRUE;
            include_once drupal_get_path('module', 'node') . '/node.pages.inc';
            return node_add(utdanning_create_course_name);
        }
    }
    return t("Wrong path.");
}



/**
 *Implementation of hook_nodeapi().
 * Updates node data with default values found in utdanning_create_course_profiles_node_prepare()
 * @see utdanning_create_course_profiles_node_prepare()
 * @global boolean $utdanning_create_course_wizard
 * @param $node
 * @param $op
 * @param $a3
 * @param $a4
 * @return void
 */
function utdanning_create_course_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

    if ($op == 'prepare' && !isset($node->nid)) {

        global $utdanning_create_course_wizard;
        if ($utdanning_create_course_wizard == TRUE) {

            // Find sample node.
            $db = db_query("SELECT nid FROM {node} WHERE type = '%s' LIMIT 1", utdanning_create_course_name);
            $data = db_fetch_object($db);
            if (!$data) {

                //TODO: Might need to create a temporally node unless there are simpler method. Investigate this.

                return;
            }
            else {
                $samplenode = node_load($data->nid);
            }

            foreach ($samplenode as $field => $v) {
                $node = _utdanning_create_course_prepare($node, $field);
            }
        }
    }
}



/**
 *Private function that is called when preparing node from  utdanning_create_course_nodeapi().
 * Returns updated node object if applyable.
 * @see utdanning_create_course_nodeapi()
 * @see utdanning_create_course_profiles_node_prepare()
 * @param object $node
 * @param string $field
 * @return object Node object.
 */
function _utdanning_create_course_prepare($node, $field) {
    include_once drupal_get_path('module', 'utdanning_create_course') . '/utdanning_create_course_profiles.inc';
    $hardcoded = utdanning_create_course_profiles_node_prepare(utdanning_create_course_name, $field);
    $default = "This is a unique default saying utdanning_create_course_variable_get didn't find anything.";
    if (!empty($hardcoded)) {
        $node->$field = $hardcoded;
    }
    return $node;
}

/**
 * Implementation of hook_form_alter().
 * Function that alters form on node/add/ <content type> /simplified
 *
 * @see utdanning_create_course_profiles_form_alter
 * @global boolean $utdanning_create_course_wizard
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function utdanning_create_course_form_alter(&$form, $form_state, $form_id) {
    if (defined('utdanning_create_course_name') && $form_id == utdanning_create_course_name . '_node_form') {
        global $utdanning_create_course_wizard;
        if ($utdanning_create_course_wizard == TRUE) {
            foreach ($form as $field => $value) {
                $form = _utdanning_create_course_manipulate_form($form, $field, $value);
            }
        }
    }
}




/**
 *Private helper function to utdanning_create_course_form_alter.
 * Updates individual fields as changed in utdanning_create_course_profiles_form_alter.
 * @see utdanning_create_course_profiles_form_alter
 * @param array $form Form array
 * @param string $field Field
 * @param array $value Original value
 * @return array Updated form array
 */
function _utdanning_create_course_manipulate_form($form, $field, $value) {

    if (mb_substr($field, 0, 1) == '#') {
        return $form;
    }

    if (is_array($value) && $value['#type'] == 'value' || $value['#type'] == 'hidden') {
        return $form;
    }

    $form[$field] = utdanning_create_course_profiles_form_alter(utdanning_create_course_name, $field, $value);

    return $form;
}


/**
 * Function that will be called when node/add/ <content type> /simplified/finalize is called.
 * Usually redirected to this page after submission of new node.
 *
 * @return string Final page.
 */
function utdanning_create_course_finalize() {

    $result = t("Nothing to do");
    $nid = _utdanning_create_course_latestnode();
    $node = node_load($nid);

    if ($node) {
        $result = utdanning_create_course_profiles_finalize($node);
    }
    return $result;
}


/**
 * Private function that determine latest node.
 * @global object $user
 * @return boolean False if not found. Otherwise Node id is returned.
 */
function _utdanning_create_course_latestnode() {
    global $user;
    $db = db_query("SELECT * FROM node WHERE type = '%s' AND uid = %d ORDER BY created DESC LIMIT 1", utdanning_create_course_name, $user->uid);
    $data = db_fetch_object($db);
    // Determinate if it's resonable to believe this is the newly created node.
    if (isset($data) && $data->created == $data->changed && $data->created + 30 >= time()) {
        return $data->nid;
    }
    return FALSE;
}

