<?php
/**
 * @file
 * @ingroup ndla_node_merge
 */
if(count($rows) > 0):
  print "<p>";
  print t("This is a list of list person nodes with the same name. Click the merge button to merge the duplicates. Node references, NDLA Mediabrowser tags etc will automatically be corrected.");
  print "</p>";
  print "<table>";
  print "<thead>";
  print "<tr><th>Node #1 " . t('title') . "</th><th>Node #1 " . t('title') . "</th><th>&nbsp;</th>";
  print "</thead>";
  print "<tbody>";
  foreach($rows as $row):
    $merge_link = l(t('Merge'), "ndla_node_merge/".$row->nid1."/".$row->nid2, array('query' => array('destination' => 'ndla_node_merge')));
  ?>
    <tr><td><?= l($row->node1_title, "node/" . $row->nid1) ?></td><td><?= l($row->node1_title, "node/" . $row->nid2) ?></td><td><?= $merge_link ?></td></tr>
  
  <?php
  endforeach;
  print "</tbody>";
  print "</table>";
endif;
?>