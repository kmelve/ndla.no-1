<?php


/**
 * PUBLIC API FUNCTIONS
 */


/**
 * Function for importing a single RDF source into database.
 * @param string $modeluri
 * @param string $path
 * @return boolean True if succeeded import, false if failure.
 */
function utdanning_rap_import($modeluri, $path) {

    // Reuse existing database connection.
    if (func_get_arg(2) instanceof DbStore) {
        $rdf_database = func_get_arg(2);
    }
    else {
        // New connection.
        $rdf_database = _utdanning_rap_dbmodel();
    }

    // Parse source into memory.
    $rdf_mem = ModelFactory::getDefaultModel();
    $rdf_mem->load($path);

    // Check if exist, then check if update is necissary, eventually drop old version.
    if ($rdf_database->modelExists($modeluri)) {
        $dbmodel = $rdf_database -> getModel($modeluri);
        if ($dbmodel->equals($rdf_mem)) {
            return TRUE;
        }
        $rdf_database->removeNamedGraphDb($modeluri);
    }

    // Store to new data into database.
    $ok = $rdf_database->putModel($rdf_mem, $modeluri);

    // Finnish up.
    if ($ok !== FALSE) {
        // Store a timestamp for import.
        _utdanning_rap_set_time($modeluri);
        $ok = TRUE;
    }
    return $ok;
}


/**
 * Import multiple rdf sources into database.
 * @param array $modeluris_and_paths Modeluris and path in format "array(<modeluri> => <path>)"
 * @return array An array with separate list of succeded and failed modeluri + paths.
 */
function utdanning_rap_import_multiple($modeluris_and_paths) {
    $rdf_database = _utdanning_rap_dbmodel();
    $failed = array();
    $succeeded = array();
    foreach ($modeluris_and_paths as $modeluri => $path) {
        // This can get time consuming.. reset clock
        set_time_limit(30);
        $ok = utdanning_rap_import($modeluri, $path, $rdf_database);
        if ($ok !== TRUE) {
            $failed[$modeluri] = $path;
        }
        else {
            $succeeded[$modeluri] = $path;
        }
    }
    return array('succeed' => $succeeded, 'failed' => $failed);
}


/**
 * Returns a list of modeluris in database.
 * @return array
 */
function utdanning_rap_get_modeluris() {
    $rdf_database = _utdanning_rap_dbmodel();
    $list = $rdf_database->listModels();
    return $list;
}


/**
 * Do a SPARQL query on selected modeluri in database.
 * @param string $modeluri Modeluri to run query on.
 * @param string $query SPARQL query.
 * @return array Result.
 */
function utdanning_rap_sparql($modeluri, $query) {
    $rdf_database = _utdanning_rap_dbmodel();
    if ($rdf_database->modelExists($modeluri)) {
        $dbModel = $rdf_database->getModel($modeluri);
        $res = $dbModel->sparqlQuery($query);
        return $res;
    }
    return FALSE;
}


/**
 *Function to check if modeluri exists in database.
 * @param string $modeluri
 * @return boolean Returns true if found, false if not found.
 */
function utdanning_rap_modeluri_exists($modeluri) {
    $rdf_database = _utdanning_rap_dbmodel();
    if ($rdf_database->modelExists($modeluri)) {
        $res = TRUE;
    } else {
        $res = FALSE;
    }
    return $res;
}


/**
 * Function to create a new modeluri.
 * @param string $modeluri Unique name, required
 * @param string $baseURI Optinal
 * @return boolean True if success, false on failure.
 */
function utdanning_rap_create_modeluri($modeluri,$baseURI=NULL) {
    $rdf_database = _utdanning_rap_dbmodel();
    $dbModel = $rdf_database->getNewModel($modeluri, $baseURI);
    if ($dbModel !== FALSE) {
        // Store a timestamp for creation.
        _utdanning_rap_set_time($modeluri);
        return TRUE;
    }
    return FALSE;
}


/**
 * Drop a specific modeluri.
 * @param string $modeluri Name of modeluri to delete.
 * @return boolean  True if success, false if failure.
 */
function utdanning_rap_drop_modeluri($modeluri) {
    $rdf_database = _utdanning_rap_dbmodel();
    $ok = $rdf_database->removeNamedGraphDb($modeluri);
    if ($ok) {
        // Also remove timestamp.
        _utdanning_rap_remove_time($modeluri);
        return TRUE;
    }
    return FALSE;
}


/**
 * Function to insert statement/triplet into given modeluri/repository with object as literal.
 *
 * @param string $modeluri Modeluri in database to insert statement into.
 * @param string $subject Required.
 * @param string $subject_localname
 * @param string $predicat Required
 * @param string $predicat_localname
 * @param string $object Required
 * @param string $object_language
 * @param string $object_type
 * @return boolean TRUE if succeeded, FALSE if not successfull insert.
 */
function utdanning_rap_add_statement_object_is_literal($modeluri, $subject = FALSE, $subject_localname = FALSE, $predicat = FALSE, $predicat_localname = FALSE, $object = FALSE, $object_language = FALSE, $object_type = FALSE) {

    if ($subject && $predicat && $object) {
        $spo = _utdanning_rap_spo($subject, $subject_localname, $predicat, $predicat_localname, array('type' => 'literal', 'object' => $object, 'object_language' => $object_language, 'object_type' => $object_type));
    } else {
        return FALSE;
    }

    $rdf_database = _utdanning_rap_dbmodel();

    if ($rdf_database->modelExists($modeluri) && $spo) {
        $dbmodel = $rdf_database->getModel($modeluri);
        $statement = new Statement($spo['subject'], $spo['predicat'], $spo['object']);
        $res = $dbmodel->add($statement);
    } else {
        $res = FALSE;
    }

    if ($res !== FALSE) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}

/**
 * Warning: Not fully tested in production.
 *
 *Function to insert statement/triplet into given modeluri/repository with object as resource.
 *
 * @param string $modeluri Modeluri in database to insert statement into
 * @param string $subject
 * @param string $subject_localname
 * @param string $predicat
 * @param string $predicat_localname
 * @param string $object
 * @param string $object_localname
 * @return boolean TRUE if succeeded, FALSE if not successfull insert.
 */
function utdanning_rap_add_statement_object_is_resource($modeluri, $subject = FALSE, $subject_localname = FALSE, $predicat = FALSE, $predicat_localname = FALSE, $object = FALSE, $object_localname = FALSE) {

    if ($subject && $predicat && $object) {
        $spo = _utdanning_rap_spo($subject, $subject_localname, $predicat, $predicat_localname, array('type' => 'resource', 'object' => $object, 'object_localname' => $object_localname));
    } else {
        return FALSE;
    }

    $rdf_database = _utdanning_rap_dbmodel();

    if ($rdf_database->modelExists($modeluri) && $spo) {
        $dbmodel = $rdf_database->getModel($modeluri);
        $statement = new Statement($spo['subject'], $spo['predicat'], $spo['object']);
        $res = $dbmodel->add($statement);
    } else {
        $res = FALSE;
    }

    if ($res !== FALSE) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}

/**
 * Warning: Not fully tested in production.
 *
 * Function for removing statements (triplets) from given modeluri.
 * Can be used to remove many statements, eg. all subjects = "test".
 * Optinal object is literal.
 *
 * @param string $modeluri Modeluri in database to remove a statement from.
 * @param string $subject
 * @param string $subject_localname
 * @param string $predicat
 * @param string $predicat_localname
 * @param string $object
 * @param string $object_language
 * @param string $object_type
 * @return boolean TRUE if succeeded, FALSE if not successfull remove.
 */
function utdanning_rap_remove_statement_object_is_literal($modeluri, $subject = FALSE, $subject_localname = FALSE, $predicat = FALSE, $predicat_localname = FALSE, $object = FALSE, $object_language = FALSE, $object_type = FALSE) {

    if ($subject || $predicat || $object) {
        $spo = _utdanning_rap_spo($subject, $subject_localname, $predicat, $predicat_localname, array('type' => 'literal', 'object' => $object, 'object_language' => $object_language, 'object_type' => $object_type));
    } else {
        return FALSE;
    }

    $rdf_database = _utdanning_rap_dbmodel();

    if ($rdf_database->modelExists($modeluri) && $spo) {
        $dbmodel = $rdf_database->getModel($modeluri);
        $statement = new Statement($spo['subject'], $spo['predicat'], $spo['object']);
        $res = $dbmodel->remove($statement);
    } else {
        $res = FALSE;
    }

    if ($res !== FALSE) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}

/**
 * Warning: Not fully tested in production.
 *
 * Function for removing statements (triplets) from given modeluri.
 * Can be used to remove many statements, eg. all subjects = "test".
 * Optinal object is resource.
 *
 * @param string $modeluri
 * @param string $subject
 * @param string $subject_localname
 * @param string $predicat
 * @param string $predicat_localname
 * @param string $object
 * @param string $object_localname
 * @return boolean TRUE if succeeded, FALSE if not successfull remove.
 */
function utdanning_rap_remove_statement_object_is_resource($modeluri, $subject = FALSE, $subject_localname = FALSE, $predicat = FALSE, $predicat_localname = FALSE, $object = FALSE, $object_localname = FALSE) {

    if ($subject || $predicat || $object) {
        $spo = _utdanning_rap_spo($subject, $subject_localname, $predicat, $predicat_localname, array('type' => 'resource', 'object' => $object, 'object_localname' => $object_localname));
    } else {
        return FALSE;
    }

    $rdf_database = _utdanning_rap_dbmodel();

    if ($rdf_database->modelExists($modeluri) && $spo) {
        $dbmodel = $rdf_database->getModel($modeluri);
        $statement = new Statement($spo['subject'], $spo['predicat'], $spo['object']);
        $res = $dbmodel->remove($statement);
    } else {
        $res = FALSE;
    }

    if ($res !== FALSE) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}



/**
 * Search RDF triplets in database.
 *
 * @param string $modeluri If not set, search all modeluris (WARNING, IT'LL BE SLOWER). If set, search specific modeluri (recommended).
 * @param string $subject
 * @param string $subject_localname
 * @param string $predicat
 * @param string $predicat_localname
 * @param string $object
 * @param string $object_language
 * @param string $object_type
 * @return array Results.
 */
function utdanning_rap_search($modeluri = FALSE, $subject = FALSE, $subject_localname = FALSE, $predicat = FALSE, $predicat_localname = FALSE, $object = FALSE, $object_language = FALSE, $object_type = FALSE) {

    $res = array();

    $spo = _utdanning_rap_spo($subject, $subject_localname, $predicat, $predicat_localname, $object, $object_language, $object_type);

    if (!$spo) {
        return $res;
    }

    $rdf_database = _utdanning_rap_dbmodel();

    // Search..
    if ($modeluri) {
        // Check if modeluri exists first.
        if (!$rdf_database->modelExists($modeluri)) {
            return $res;
        }
        $dbmodel = $rdf_database->getModel($modeluri);
        $res[$modeluri] = $dbmodel->find($spo['subject'], $spo['predicat'], $spo['object']);
    } else {
        $list = $rdf_database->listModels();
        foreach ($list as $model) {
            $modeluri = $model['modelURI'];
            $dbmodel = $rdf_database->getModel($modeluri);
            $res[$modeluri] = $dbmodel->find($spo['subject'], $spo['predicat'], $spo['object']);
        }
    }

    return $res;
}


/**
 *Function that present a stored modeluri as a html -page.
 * @param string $modeluri
 * @return boolean FALSE if failure, otherwise void.
 */
function utdanning_rap_modeluri2html($modeluri) {
    $rdf_database = _utdanning_rap_dbmodel();
    if ($rdf_database->modelExists($modeluri)) {
        $dbmodel = $rdf_database->getModel($modeluri);
        $dbmodel->writeAsHtmlTable();
        die();
    }
    return FALSE;
}


/**
 *Function that present a stored modeluri as a rdf+xml -page.
 * @param string $modeluri
 * @return boolean FALSE if failure, otherwise void.
 */
function utdanning_rap_modeluri2rdfxml($modeluri)  {
    $rdf_database = _utdanning_rap_dbmodel();
    if ($rdf_database->modelExists($modeluri)) {
        $dbmodel = $rdf_database -> getModel($modeluri);
        header('Content-Type: text/xml; charset=utf-8');
        header('Cache-Control: no-cache, must-revalidate'); // HTTP/1.1
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        echo $dbmodel -> writeRdfToString();
        die();
    }
    return FALSE;
}

/**
 *Function that returns UNIX timestamp for when a modelURI got created.
 * @param string $modeluri
 * @return mixed Boolean FALSE if no timestamp, otherwise int for UNIX timestamp.
 */
function utdanning_rap_get_time($modeluri) {
    return _utdanning_rap_get_time($modeluri);
}

/**
 * UNOFFICAL API FUNCTIONS
 */

/**
 *It's recommended to use utdanning_rap_remove_statement_object_is_resource
 * or utdanning_rap_remove_statement_object_is_literal instead.
 *
 * This function require direct use of rap library or private functions in
 * utdanning_rap and is therfore not suited for official API calls.
 *
 * Occurred in utdanning_gajax.
 *
 * @param unknown $model
 * @param unknown $s
 * @param unknown $p
 * @param unknown $o
 */
function utdanning_rap_remove_triplet($model, $s, $p, $o) {
    $rdf_database = _utdanning_rap_dbmodel();
    $dbmodel = $rdf_database -> getModel($model);
    $dbmodel->remove(new Statement($s, $p, $o));
}
