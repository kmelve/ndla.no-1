<?php

function utdanning_rap_ui() {
    // Get extra parameters from url.
    $args = func_get_args();

    if (!$args) {
        $modeluris = utdanning_rap_get_modeluris();
        return drupal_get_form('utdanning_rap_ui_overview', $modeluris);
    }

    if ($args && count($args) > 0) {

        // modeluris with '/' gets splitted in args, so we have to rebuild it on our own.
        $tmp = count($args) > 1 ? '/' : '';
        $modeluri = current($args) . $tmp;
        while ($tmp = next($args)) {
            if (key($args) < count($args)-1) {
                $tmp .= '/';
            }
            $modeluri .= $tmp;
            $tmp = "";
        }

        // Function print out result directly and then do "die()".
        if (!utdanning_rap_modeluri2html($modeluri)) {
            return t("Can't recognize given modeluri %modeluri.", array('%modeluri' => $modeluri));
        }
    }

    return '';
}

function utdanning_rap_ui_overview() {
    $modeluris = func_get_arg(1);

    //        var_dump($modeluris);die();

    $form['tabletitle'] = array(
        '#type' => 'item',
        '#value' => t('Available rdf sources:'),
        '#description' => t('Select the items you want to delete, or click on an item to view content.'),
    );

    foreach ($modeluris as $v) {
        $form['modeluris']['checkbox-' . $v['modelURI']] = array(
            '#type' => 'checkbox',
            '#title' => t('Delete'),
            '#attributes' => array('class' => 'select-all'),
        );
        $form['modeluris']['modeluri-' . $v['modelURI']] = array(
            '#title' => t('modelURI'),
            '#type' => 'item',
            '#value' => l($v['modelURI'], 'admin/content/rap/administer/' . urlencode($v['modelURI'])),
        );

        // Trim the size down a few inches.
        if (strlen($v['baseURI']) > 64) {
            $baseuri = mb_substr($v['baseURI'], 0, 30) . '....' . mb_substr($v['baseURI'], strlen($v['baseURI']) - 30, strlen($v['baseURI']));
        }
        else {
            $baseuri = $v['baseURI'];
        }

        $form['modeluris']['baseuri-' . $v['modelURI']] = array(
            '#title' => t('baseURI'),
            '#type' => 'item',
            '#value' => $baseuri,
        );
    }

    if (!$form['modeluris']) {
        $form['tabletitle']['#description'] = t('No modeluris found.');
    }

    $form['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
    );

    return $form;

}


function theme_utdanning_rap_ui_overview($form) {



    $list = $form['modeluris'];

    //        var_dump($list);die();
    if (isset($list) && is_array($list) && count($list) > 0) {

        // We don't want to render table-content twice so we remove list from $form.
        unset($form['modeluris']);

        $rows = array();

        $header = array();


        $row = array();

        while ( $item = current($list)) {

            // Store header info..
            if ($item['#type'] != 'checkbox' && !isset($header[$item['#title']])) {
                $header[$item['#title']] = $item['#title'];
            }

            // Remove title info..
            if (isset($item['#title'])) {
                unset($item['#title']);
            }

            $row[] = drupal_render($item);

            // Store the row.
            if (count($row) == 3) {
                $rows[] = $row;
                $row = array();
            }

            // Move pointer to next item.
            next($list);

        }

        // Prerender and remove stuff from form so we get things in correct order.
        $tabletitle = drupal_render($form['tabletitle']);
        unset($form['tabletitle']);

        // Return stuff and render the remaining of the form, so submit and other functionality works.
        $header = array_merge(array(theme('table_select_header_cell')), $header);
        return $tabletitle  . theme('table', $header, $rows) . drupal_render($form);
    }
}

function utdanning_rap_ui_overview_submit($form, $form_state) {

    foreach($form_state['values'] as $k => $v) {
        if ($v == 1) {
            $modeluri = substr($k, strlen('checkbox-'), strlen($k));
            $ok = utdanning_rap_drop_modeluri($modeluri);
            if ($ok) {
                drupal_set_message(t('Modeluri %modeluri dropped successfully.', array('%modeluri' => $modeluri)));
            } else {
                drupal_set_message(t('Failed to drop modeluri %modeluri.', array('%modeluri' => $modeluri)), 'error');
            }
        }
    }

    if (!isset($modeluri)) {
        drupal_set_message(t('Nothing to do.'));
    }
}
