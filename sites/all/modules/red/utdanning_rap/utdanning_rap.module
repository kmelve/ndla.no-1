<?php

/*
 * @file utdanning_rap.module
 *
 * Short description of this module:
 * Module to bridge RAP library and Drupal in a friendly API.
 *
 * Longer description of this module:
 * Due to lack of good RDF support in Drupal 6 we made an API to RAP library V0.9.6.
 * RAP library have a long track record and have been in development since year 2002.
 * http://www.seasr.org/wp-content/plugins/meandre/rdfapi-php/doc/
 * With this module developers have a easy way to create modules that can
 * create, store, present and manipulate RDF data without knowing any details about
 * RAP API calls. The module also support and follows w3c standard for querying
 * stored RDF data with SPARQL.
 *
 * @author Jan Henrik Hasselberg
 *
 * @version 6.x-1.0
 *
 * @todo There will never be enough testing unfortunately.
 *
 * @warning RAP library access database outside Drupal and module is expecting mysql database.
 *
 * @license
 * Copyright (C) Utdanning.no
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LISENCE.txt file or the
 * GNU General Public License for more details.
 */


/**
 * Implementation of hook_init().
 */
function utdanning_rap_init() {

/**
 * THINGS TO DEFINE
 */
    define("RDFAPI_INCLUDE_DIR", drupal_get_path('module', 'utdanning_rap') . "/rap/rdfapi-php/api/");
    define("UTDANNING_RAP_INIT", TRUE);

/**
 * THINGS TO INCLUDE
 */
    require_once (RDFAPI_INCLUDE_DIR . "RdfAPI.php");
    require_once drupal_get_path('module', 'utdanning_rap') . '/utdanning_rap.inc';
    require_once drupal_get_path('module', 'utdanning_rap') . '/rap/version.php';
}



/**
 * ADMIN  UI RELATED
 */

/**
 *Implementation of hook_perm().
 * @return array
 */
function utdanning_rap_perm() {
    return array('administer rap modeluris');
}

/**
 *Implementation of hook_menu().
 * @return array
 */
function utdanning_rap_menu() {
    // Page for viewing or deleting RDF modeluris.
    $items['admin/content/rap/administer'] = array(
        'title' => 'Administer RDF modeluris (RAP library)',
        'page callback' => 'utdanning_rap_ui',
        'access arguments' => array('administer rap modeluris'),
        'file' => 'utdanning_rap_ui.inc',
        'description' => t('Administer rdf data that is imported into database with the RAP library.'),
    );
    return $items;
}


function utdanning_rap_theme() {
    $themereg = array ();
    $themereg ['utdanning_rap_ui_overview'] = array ('arguments' => array ('form' => NULL ), 'file' => 'utdanning_rap_ui.inc' );
    return $themereg;
}


/**
 * HELPER FUNCTIONS
 */


/**
 * Function that give rap library a way to talk with Drupal database.
 *
 * WARNING, will not work on all setups!
 *
 * @global string $db_url
 * @return array Database configuration for RAP.
 */
function _utdanning_rap_dbconfig() {

    // In rare cases where things aren't defined and included on beforehand, we have to run init.
    if (! UTDANNING_RAP_INIT ) {
        utdanning_rap_init();
    }

    /*
     * What we must pass into rap:
      * @param   string   $dbDriver
      * @param   string   $host
      * @param   string   $dbName
      * @param   string   $user
      * @param   string   $password
      */

    $dbconfig = array();
    global $db_url;
    $url = parse_url($db_url);

        /**
         * RAP supports a few more databasetypes, but we are currently only interested in mysql.
         */

    if ($url['scheme'] == 'mysqli' || $url['scheme'] == 'mysql') {
        // @param   string   $dbDriver
        $dbconfig['dbDriver'] = 'MySQL';
    }
    else {
        return FALSE;
    }

    // @param   string   $host
    $dbconfig['host'] = urldecode($url['host']);

    // @param   string   $dbName
    $dbconfig['dbName'] = str_replace("/", "", urldecode($url['path'])) ;

    // @param   string   $user
    $dbconfig['user'] = urldecode($url['user']);

    // @param   string   $password
    $dbconfig['password'] = isset($url['pass']) ? urldecode($url['pass']) : '';

    return $dbconfig;
}

/**
 * Create a database class used by other RAP functions.
 * @param array $config
 * @return object
 */
function _utdanning_rap_dbmodel($config = FALSE) {
    static $db_connection = NULL;

    if (!$db_connection) {
        if ($config == FALSE) {
            $config = _utdanning_rap_dbconfig();
        }
        $db_connection = ModelFactory::getDbStore($config['dbDriver'], $config['host'], $config['dbName'], $config['user'], $config['password']);
    }
    return $db_connection;
}


/**
 *Function that creates necisary Resource and Literal classes for entering subject, predicat and object into RAP.
 * @param string $subject
 * @param string $subject_localname
 * @param string $predicat
 * @param string $predicat_localname
 * @param string $object
 * @param string $object_language
 * @param string $object_type
 * @return array Returns array with subject, predicat and object - objects. If s-p-o is not set, function will return FALSE.
 */
function _utdanning_rap_spo($subject = FALSE, $subject_localname = FALSE, $predicat = FALSE, $predicat_localname = FALSE, $object = FALSE) {

    // No need to continue if s, p and o is not set..
    if (!$subject && !$predicat && !$object) {
        return false;
    }

    // Create the s, o and p's.

    /**
     * SUBJECT
     */
    if ($subject) {
        $s = new Resource($subject, $subject_localname ? $subject_localname : NULL);
    }
    else {
        $s = NULL;
    }

    /**
     * PREDICAT
     */
    if ($predicat) {
        $p = new Resource($predicat, $predicat_localname ? $predicat_localname : NULL);
    }
    else {
        $p = NULL;
    }

    /**
     * OBJECT
     */
    if ($object && $object['type'] == 'literal' && $object['object']) {
        $o = new Literal($object['object'], $object['object_language'] ? $object['object_language'] : NULL, $object['object_type'] ? $object['object_type'] : NULL);
    }
    else if ($object && $object['type'] == 'resource' && $object['object']) {
        $o = new Resource($object['object'], $object['object_localname'] ? $object['object_localname'] : NULL);
    }
    else {
        $o = NULL;
    }

    return array('subject' => $s, 'predicat' => $p, 'object' => $o);
}

/**
 *Function that set time for given modeluri.
 * @param string $modeluri
 */
function _utdanning_rap_set_time($modeluri) {
    $time = time();
    $exists = _utdanning_rap_get_time($modeluri);
    if ($exists !== FALSE) {
        db_query("UPDATE {utdanning_rap_time} SET time = %d WHERE modeluri = '%s'", $time, $modeluri);
    }
    else {
        db_query("INSERT INTO {utdanning_rap_time} (modeluri, time) VALUES ('%s', %d)", $modeluri, $time);
    }
}

/**
 *Function that fetches stored UNIX time for when given $modeluri was created.
 * @param string $modeluri
 * @return mixed Boolean FALSE if not stored, int if UNIX time stamp.
 */
function _utdanning_rap_get_time($modeluri) {
    $db = db_query("SELECT * FROM {utdanning_rap_time} WHERE modeluri = '%s'", $modeluri);
    $data = db_fetch_object($db);
    if ($data) {
        return $data->time;
    }
    return FALSE;
}

/**
 *Function that remove timestamp for a given modeluri.
 * @param string $modeluri
 */
function _utdanning_rap_remove_time($modeluri) {
    db_query("DELETE FROM {utdanning_rap_time} WHERE modeluri = '%s'", $modeluri);
}
