<?php

 /**
  * Implementation of hook_menu()
  */
function ndla_notify_menu() {
  $items = array();
  
  //Menu callback for #ahah add-more function
  $items['ndla_notify/add'] = array(
    'page callback' => 'ndla_notify_add',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  
  return $items;
}

/**
 * Called via #ahah. Rebuilds the author part of the node form.
 */
function ndla_notify_add() {
  // The form is generated in an include file which we need to include manually.
  include_once 'modules/node/node.pages.inc';
  // We're starting in step #3, preparing for #4.
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  // Step #4.
  $form = form_get_cache($form_build_id, $form_state);

  // Preparing for #5.
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  // Step #5.
  drupal_process_form($form_id, $form, $form_state);
  // Step #6 and #7 and #8.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  //NDLA specifics...
  $dummy = theme('status_messages');
  // Step #9.
  
  $notify_form = $form['ndla_notify']['notifications'];
  unset($notify_form['#prefix'], $notify_form['#suffix']);

  $output = drupal_render($notify_form);
  global $base_path;
  $output .= "<script type='text/javascript' src='" . $base_path . drupal_get_path('module', 'ndla_notify') . "/js/ndla_notify.js'></script>";
  // Final rendering callback.
  print drupal_to_js(array('data' => $output, 'status' => TRUE));
  exit();
}

/**
 * NDLA Notify -
 *  Sends email when the node hasn't been updated.
 *
 * @author jonas.seffel@cerpus.com
 */
 
 function ndla_notify_form_alter(&$form, &$form_state, $form_id) {
  if(!empty($form['#node']) && $form_id == $form['#node']->type."_node_form") {
    $prev_added = array();
    //AHAH has made its magic
    if(isset($form_state['post']['ndla_notify'])) {
      foreach($form_state['post']['ndla_notify']['notifications'] as $index => $notifcation) {
        $prev_added[$index] = array(
          'notify_message' => $notifcation['notification']['notify_message'], 
          'reoccur' => $notifcation['notification']['reoccur'],
          'notify_date' => $notifcation['notification']['notify_date']['date'],
        );
      }
    }
    //Pickup default values
    else if(!empty($form['#node']->ndla_notify)) {
      ndla_notify_load_and_update($form['#node']);
      foreach($form['#node']->ndla_notify['notifications'] as $index => $notifcation) {
        $prev_added[$index] = array(
          'notify_message' => $notifcation['notification']['notify_message'], 
          'reoccur' => $notifcation['notification']['reoccur'],
          'notify_date' => $notifcation['notification']['notify_date'],
        );
      }
    }

    $notify_form = array(
      '#type' => 'fieldset',
      '#title' => t('Notifications'),
      '#collapsed' => !($the_time > time()),
      '#collapsible' => TRUE,
      '#tree' => TRUE,
      '#weight' => 0,
    );
    
    foreach($prev_added as $index => $added) {
      $notify_form['notifications'][] = ndla_notify_add_row($added, ($index+1), FALSE);
    }
    
    $notify_form['notifications'][] = ndla_notify_add_row(array(), (count($prev_added)+1), TRUE);
    $notify_form['notifications']['#prefix'] = '<div id="ndla_notify_wrapper">';
    $notify_form['notifications']['#suffix'] = '</div>';

    $notify_form['create'] = array(
      '#type'   => 'button',
      '#value'  => t('Add more'),
      '#ahah'   => array(
        'path'    => 'ndla_notify/add',
        'wrapper' => 'ndla_notify_wrapper',
        'method'  => 'replace',
        'effect'  => 'fade',
      ),
    );
    $form['ndla_notify'] = $notify_form;

    $validate = TRUE;
    if(isset($_GET['novalidate'])) {
      $validate = FALSE;
    }
    
    //Add the validation function.
    if(!in_array($ndla_notify_validate, $form['#validate']) && $validate) {
      $form['#validate'][count($form['#validate'])] = 'ndla_notify_validate';
    }
  }
}

function ndla_notify_add_row($added = array(), $num = 1, $is_last = TRUE) {
  $format = 'Y-m-d';
  $date = date($format, time());
  $date = isset($added['notify_date']) ? $added['notify_date'] : '';
  $notify_message = isset($added['notify_message']) ? $added['notify_message'] : '';
  $reoccur = isset($added['reoccur']) ? $added['reoccur'] : '';
  
  if(!empty($date) && is_numeric($date)) {
    $date = date($format, $date);
  }
  
  $row = array(
      'notification' => array(
      '#title' => t('Notification') . ' #' . $num,
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => !$is_last,

      'notify_date' =>  array(
        '#type' => 'date_popup', // types 'date_text' and 'date_timezone' are also supported. See .inc file.
        '#title' => t('Notification date'),
        '#default_value' => $date,
        '#date_format' => $format,
        '#date_label_position' => 'within', // See other available attributes and what they do in date_api_elements.inc
        '#date_year_range' => '0:+1', // Optional, used to set the year range (back 3 years and forward 3 years is the default).
        '#attributes' => array('class' => 'notify_date'),
      ),

      'notify_message' => array(
        '#type' => 'textarea',
        '#title' => t('Notify message'),
        '#description' => t('If you want you can write a small reminder to yourself. HTML characters will be stripped.'),
        '#default_value' => $notify_message,
      ),
  
      'reoccur' => array(
        '#type' => 'textfield',
        '#title' => t('Send an notification every i:th day'),
        '#default_value' => $reoccur,
        '#description' => t('If set, the notification date will add the number of days you enter here to the date when you recieve a notification.'),
        '#size' => 2,
        '#maxlength' => 2,
      ),
    ),
  );
  
  return $row;
}

/**
 * Function for validating the node_form.
 */
function ndla_notify_validate($form, &$form_state) {
  //form_set_error('ndla_notify][notifications][0][notification][notify_date][date', t('Date not valid'));
  foreach($form_state['values']['ndla_notify']['notifications'] as $index => $notification) {
    if($form_state['post']['op'] == t('Add more')) {
      return;
    }
    $msg = check_plain(trim($notification['notification']['notify_message']));
    if(!is_array($notification['notification']['notify_date'])) {
      $date = check_plain($notification['notification']['notify_date']);
    } else {
      $date = check_plain($notification['notification']['notify_date']['date']);
    }
    $reoccur = check_plain($notification['notification']['reoccur']);
    
    //Handle the reoccur value
    if(!empty($reoccur) && !is_numeric($reoccur)) {
      form_set_error('ndla_notify][notifications][' . $index . '][notification][reoccur', t('Reoccur value not valid. Leave empty or enter a positive number.'));
    }
    
    //Validate the time
    $update_date = strtotime($date);
    if(!empty($update_date) && is_numeric($update_date) && ($update_date <= time())) {
      form_set_error('ndla_notify][notifications][' . $index . '][notification][notify_date][date', t('Date must be greater than current date'));
    }
    
    if(!empty($update_date) && is_numeric($update_date) && empty($msg)) {
      form_set_error('ndla_notify][notifications][' . $index . '][notification][notify_message', t('Notification message can not be empty.'));
    }
  }
}

/**
 * Implementation of hook_nodeapi()
 */
function ndla_notify_nodeapi(&$node, $op, $a3 = NULL, $a4  = NULL) {
  switch($op) {
    case 'load':
      ndla_notify_load($node);
      break;
    case 'insert':
    case 'update':
      ndla_notify_save($node);
      break;
    case 'delete':
      ndla_notify_delete($node);
      break;
  }
}

/**
 * Loads a node.
 */
function ndla_notify_load(&$node) {
  $result = db_query("SELECT mid, notify_date, reoccur, notify_message FROM {ndla_notify} WHERE nid = %d", $node->nid);
  while($row = db_fetch_object($result)) {
    $node->ndla_notify['notifications'][] = array(
      'notification' => array(
        'notify_date' => $row->notify_date,
        'reoccur' => $row->reoccur,
        'notify_message' => $row->notify_message,
        'mid' => $row->mid, 
      ),
    );
  }
}

function ndla_notify_load_and_update(&$node) {
  $current_time = strtotime(date('Y-m-d H:m:s', time()));
  //We will get duplicates if we dont unset the previously loaded notifications.
  unset($node->ndla_notify);
  ndla_notify_load($node);
  if(isset($node->ndla_notify) && count($node->ndla_notify['notifications'])) {
    foreach($node->ndla_notify['notifications'] as $index => $notification) {
      $prev_time = $notification['notification']['notify_date'];
      $reoccur = (int)$notification['notification']['reoccur'];
      if($reoccur > 0) {
        while($prev_time <= $current_time) {
          $prev_time = $prev_time + ($reoccur * 24 * 60 * 60);
        }
      }
      $node->ndla_notify['notifications'][$index]['notification']['notify_date'] = $prev_time;
    }
  
    ndla_notify_save($node);
  }
}

/**
 * Saves a node.
 */
function ndla_notify_save(&$node) {
  if(!empty($node->ndla_notify)) {
    //Delete all old rows
    ndla_notify_delete($node);
    foreach($node->ndla_notify['notifications'] as $index => $data) {
      if(!empty($data['notification']['notify_date']) && !empty($data['notification']['notify_message'])) {
        $date = $data['notification']['notify_date'];
        if(!is_numeric($date)) {
          $date = strtotime(check_plain($data['notification']['notify_date']));
        }
        $msg = check_plain($data['notification']['notify_message']);
        $reoccur = (int)$data['notification']['reoccur'];
        ndla_notify_save_notification($node->nid, array('notify_date' => $date, 'notify_message' => $msg, 'reoccur' => $reoccur));
      }
    }
  }
  else {
    ndla_notify_delete($node);
  }
}

function ndla_notify_save_notification($nid, $row) {
  db_query("INSERT INTO {ndla_notify} (nid, notify_date, reoccur, notify_message) VALUES(%d, %d, %d, '%s')", $nid, $row['notify_date'], $row['reoccur'], $row['notify_message']);
}

function ndla_notify_delete($node) {
  db_query("DELETE FROM {ndla_notify} WHERE nid = %d", $node->nid);
}

/**
 * Returns the nodes which is marked for notification
 *
 * @param $time
 *  Timestamp or NULL for current time.
 * @param $coming
 *  If true return notifications which are set in the future.
 */
function ndla_notify_get_nodes($uid, $coming = FALSE) {
  $time = time();
  
  $nodes = array();
  
  $result = FALSE;
  if($coming) {
    $result = db_query("SELECT n.nid, n.title, notify.notify_date FROM {node} n
            INNER JOIN {ndla_notify} notify ON n.nid = notify.nid
            WHERE notify.notify_date > %d AND n.uid = %d GROUP BY n.nid", $time, $uid);
  }
  else {
    $result = db_query("SELECT n.nid, n.title, notify.notify_date FROM {node} n
            INNER JOIN {ndla_notify} notify ON n.nid = notify.nid
            WHERE notify.notify_date <= %d AND n.uid = %d GROUP BY n.nid", $time, $uid);
  }
  while($row = db_fetch_object($result)) {
    $nodes[$row->nid] = $row;
  }

  return $nodes;
}

function ndla_notify_delete_mid($mid) {
  db_query("DELETE FROM {ndla_notify} WHERE mid = %d", $mid);
}

/**
 * Implementation of hook_cron()
 */
function ndla_notify_cron() {
  watchdog('ndla_notify', 'NDLA Notify: cron running (' . date('Y-m-d H:i:s', time()) .')', array(), WATCHDOG_INFO);
  global $base_url;
  $notifications = array();
  $result = db_query("SELECT n.nid, n.title, n.uid, n.changed, u.mail, u.name, nn.mid, nn.reoccur, nn.notify_date, nn.notify_message FROM {node} n 
            INNER JOIN {ndla_notify} nn ON nn.nid = n.nid
            INNER JOIN {users} u ON n.uid = u.uid
            WHERE nn.notify_date <= %d 
            AND u.mail <> '' AND u.mail IS NOT NULL 
            ORDER BY u.uid, n.title", time());
            
  while($row = db_fetch_object($result)) {
    $notifications[$row->name]['nodes'][] = array(
      'nid' => $row->nid,
      'title' => $row->title,
      'created' => $row->created,
      'changed' => $row->changed,
      'notify_date' => $row->notify_date,
      'reoccur' => (int)$row->reoccur,
      'notify_message' => $row->notify_message,
      'mid' => (int)$row->mid,
    );
    
    $notifications[$row->name]['email'] = $row->mail;
  }

  foreach($notifications as $user_name => $user_notifications) {
    $account = user_load(array('name' => $user_name));
    $lang = user_preferred_language($account);
    $mail_message = t('Hi, you have requested to be informed when it is time to update some of your nodes. That time has come, the nodes are:', array(), $lang->language)."\n\n";
    foreach($user_notifications['nodes'] as $notifiable_node) {
      $mail_message .= $notifiable_node['title'] . " (" . $base_url.url('node/'.$notifiable_node['nid']) . ") - " . t('Last change', array(),  $lang->language) . ": " . format_date($notifiable_node['changed'], 'short') . "\n";
      $notify_message = trim($notifiable_node['notify_message']);
      if(!empty($notify_message)) {
        $mail_message .= "\n".t("Notify message").":\n".$notify_message."\n\n";
      }
      
      if(!$notifiable_node['reoccur']) {
        ndla_notify_delete_mid($notifiable_node['mid']);
      }

      watchdog('ndla_notify', 'Notified user ' . $user_name . ' about node with nid#' . $notifiable_node['nid'], array(), WATCHDOG_INFO);
      //Update the notifications
      $node->nid = $notifiable_node['nid'];
      ndla_notify_load_and_update($node);
      ndla_notify_save($node);
      
    }
    $mail_message .= "\n\n";
    $mail_message .= t('Please do not reply to this message', array(), $lang->language).".";
    
    $params['account'] = $account;
    $params['message'] = $mail_message;

    
    
    drupal_mail('ndla_notify', 'notice', $account->mail, $lang, $params);
  }
}

function ndla_notify_mail($key, &$message, $params) {
  $language = $message['language'];
  $variables = user_mail_tokens($params['account'], $language);
  switch($key) {
    case 'notice':
      $message['subject'] = t('Reminder about updating your web-pages', array(), $language->language);
      $message['body'][] = $params['message'];
      break;
  }
}

/**
 * Implementation of hook_views_data()
 */
function ndla_notify_views_data() {
  $data = array();
  $data['ndla_notify']['table']['group'] = t('Node');
       
  /* Make a join to the node table possible */
  $data['ndla_notify']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
    'type' => 'LEFT',
  );

  $data['ndla_notify']['notify_date'] = array(
    'title' => t('NDLA Notification date'),
    'help' => t('The data when a notification will be sent'), // The help that appears on the UI,

    // Information for displaying the guuid
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),

    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),

    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
 
  $data['ndla_notify']['reoccur'] = array(
    'title' => t('NDLA Notification reoccurance'),
    'help' => t('Reoccuring days.'), // The help that appears on the UI,

    // Information for displaying the guuid
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),

    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),

    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
 
  return $data;
}

/**
 * Implementation of hook_content_extra_fields().
 * Allows the admin to change the order of fields in the node edit form.
 *
 * @param $type_name
 *  The content type
 *
 * @return
 *  Array of fields
 */
function ndla_notify_content_extra_fields($type_name) {
  $weight = 5;
  $extra_fields = array();
  
  $extra_fields['ndla_notify'] = array(
    'label' => t('NDLA Notify'),
    'description' => t('Utdanning RDF module form'),
    'weight' => $weight,
  );


  return $extra_fields;
}