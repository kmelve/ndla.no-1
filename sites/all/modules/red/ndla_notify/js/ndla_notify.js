Drupal.behaviors.ndla_notify = function() {
   $('.notify_date').each(
     function( intIndex ){
       if (Drupal.settings.datePopup[this.id] == undefined) { // not bound yet!
         // look for valid 'settings' in Drupal.settings.datePopup 
         var settingsIndex;
         for (settings in Drupal.settings.datePopup) {
           var size = this.id.indexOf("-", this.id.indexOf("-")+1) +1; // part of string we can match to
           if(settings.substr(0, size) == this.id.substr(0,size)) {
             settingsIndex = settings;
             break;
           }//end if
         } //end for
         if (settingsIndex != undefined) {
           $('#'+ this.id).bind('focus', Drupal.settings.datePopup[settingsIndex], function(e) {
             if (!$(this).hasClass('date-popup-init')) {
               var datePopup = e.data;
               // Explicitely filter the methods we accept.
               switch (datePopup.func) {
                 case 'datepicker':
                  $(this).datepicker(datePopup.settings).addClass('date-popup-init').focus();
                  break;
                } //end switch
              } //end if
            }); //end bind
          }//end if settingsIndex
        } //end unbound
      } //end function(intIndex)
    );//end each
}//end behaviors