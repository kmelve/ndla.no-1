Drupal.behaviors.ndla_editor_portal_events = function(context) {
  $('.user-edit-button').unbind('click');
  $('.user-edit-button').bind('click', function() {
    current = $('.edit_wrapper').css('display');
    if(current == 'none') {
      $('.user-edit-button').css('background-position', '0px -32px');
      $('.edit_wrapper').slideDown('fast');
    }
    else {
      $('.user-edit-button').css('background-position', '0px 0px');
      $('.edit_wrapper').slideUp('fast');
    }
  });
  
  $('#save_search_button').bind('keydown', function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
     if(code == 13) { //Enter keycode
       $('#edit-save-search').click();
     }
  });
  
  if($('#block-ndla_editor_portal_photo').length) {
    height = $('#portal-left').height();
    if(height > 50) {
      $('#block-ndla_editor_portal_changelog').height(height);
    }
  }
}

function ask_overwrite() {
  current_name = $('#current-search').val();
  new_name = $('#save_search_button').val();
  if(current_name.length > 0 && current_name != new_name) {
    question = Drupal.t('Replace search \"TOKEN1\" with \"TOKEN2\"?'); 
    question = question.replace("TOKEN1", current_name).replace("TOKEN2", new_name);
    if(!confirm(question)) {
      $('#current-search').val('');
    }
  }
}
