/**
 * When links are opened from portal in new windows/tab
 * simple menu will think they are opened in popup
 * and no simplemenu will be printed
 */
if(window.opener != undefined && window.opener.location.href != undefined) {
  if(window.opener.location.href.match(/user\/[\d]*\/ndla_editor_portal/)) {
    window.opener = null;
  }
}