Drupal.behaviors.ndla_editor_portal_changelog_settings = function(){
  $('#ndla-editor-portal-changelog-settings-form').find('input[name=changelog_type], select').change(function(){
    if($(this).val() != 4) {
      $('#ndla-editor-portal-changelog-settings-form').submit();
    } else {
      if($('#edit-changelog-user').val() != 0) {
        $('#ndla-editor-portal-changelog-settings-form').submit();
      }
    }
  });
}