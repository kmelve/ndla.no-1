<div class='notifications'>
  <h2><?php print('Nodes which are due for update');?></h2>
  <?php if(count($notifications['expired'])): ?>
    <?php foreach($notifications['expired'] as $index => $expired): ?>
      <?php print '<p>' . l($expired->title, 'node/' . $expired->nid, array('attributes' => array('target' => '_blank'))) . ' (' . t('expired') . ' ' . date('Y-m-d') . ')</p>'; ?>
    <?php endforeach; ?>
    <?php print "<p>" . t('Note! Each node may have several notifcations.') . "</p>"; ?> 
  <?php else: ?>
    <?php print '<p>' . t('Yay! No nodes have expired!') . '</p>'; ?>
  <?php endif; ?>
</div>