<?php
/**
 * Variables available
 *  $user (stdObject)
 *  $roles (array)
 */
 
 global $user;
?>

<div class='user_wrapper'>
  <div class='user-edit-button'>&nbsp;</div>
  <h1 class='user-edit'><?php print $name_full; ?></h1>
  <div class='edit_wrapper'>
    <?php print l(t('Edit'), 'user/' . $user->uid . '/edit', array('attributes' => array('class' => 'edit_link'), 'query' => array('destination' => ndla_editor_portal_get_url()))); ?>
    <?php if(user_access('change subject photo') || user_access('administer site configuration')) {
      $base = ndla_editor_portal_get_portal_url();
      print '<br>' . l(t('Change subject photo'), $base . '/photo');
    } ?>
  </div>
  <?php print implode(", ", $roles); ?>
</div>