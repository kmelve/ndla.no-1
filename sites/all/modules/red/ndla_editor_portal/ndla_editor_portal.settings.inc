<?php

function ndla_editor_portal_settings_set($uid, $name, $value) {
  $settings = _ndla_editor_portal_settings_load($uid);
  $settings[$name] = $value;
  _ndla_editor_portal_settings_insert($uid, $settings);
}

function ndla_editor_portal_settings_get($uid, $name, $default) {
  $settings = _ndla_editor_portal_settings_load($uid);
  if(isset($settings[$name])) {
    return $settings[$name];
  }
  return $default;
}

function _ndla_editor_portal_settings_load($uid) {
  $query = "SELECT settings FROM {ndla_editor_portal_settings} WHERE uid = %d";
  $data = db_fetch_array(db_query($query, $uid));
  if($data == false) {
    return array(); 
  }
  return unserialize($data['settings']);
}

function _ndla_editor_portal_settings_insert($uid, $settings) {
  $settings = serialize($settings);
  db_query("DELETE FROM {ndla_editor_portal_settings} WHERE uid = %d", $uid);
  db_query("INSERT INTO {ndla_editor_portal_settings} (uid, settings) VALUES (%d, '%s')", $uid, $settings);
}