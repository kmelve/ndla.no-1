<?php

/**
 * Admin form for special operations
 */
function ndla_convert_ios_to_h5p_admin_form() {
  $form = array();

  $form['radio_fix'] = array(
    '#type' => 'submit',
    '#value' => t('Re-convert radio button tasks'),
    '#suffix' => '<p>' . t('Warning! This will overwrite converted H5Ps that should have had radio buttons. Any changes made to this content will be overwritten!') . '</p>',
    '#disabled' => variable_get('ndla_convert_ios_to_h5p_radio_fix_ran', FALSE)
  );

  $form['translate'] = array(
    '#type' => 'submit',
    '#value' => t('Update translations'),
    '#suffix' => '<p>' . t('Go through tasks with different language and replace default values with translated values.') . '</p>',
  );

  $form['replace'] = array(
    '#type' => 'submit',
    '#value' => t('Replace special chars'),
    '#suffix' => '<p>' . t('Replace all special characters in converted tasks..') . '</p>',
  );

  $form['one_fix'] = array(
    '#type' => 'submit',
    '#value' => t('Only one'),
    '#suffix' => '<p>' . t('Convert all converted Question Sets with only one question to Multi Choice.') . '</p>',
  );

  return $form;
}

/**
 * Handles admin form submission
 */
function ndla_convert_ios_to_h5p_admin_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] === t('Re-convert radio button tasks')) {
    variable_set('ndla_convert_ios_to_h5p_radio_fix_ran', TRUE);

    // Create a batch job
    $batch['title'] = t('Re-converting radio button tasks');
    $batch['file'] = drupal_get_path('module', 'ndla_convert') . '/ndla_convert.admin.inc';
    $batch['progress_message'] = t('Completed @current out of @total jobs.');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_fix_radios_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_fix_radios', array());

    batch_set($batch);
    batch_process();
  }
  elseif ($form_state['values']['op'] === t('Update translations')) {

    // Create a batch job
    $batch['title'] = t('Updating translations');
    $batch['file'] = drupal_get_path('module', 'ndla_convert') . '/ndla_convert.admin.inc';
    $batch['progress_message'] = t('Completed @current out of @total jobs.');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_translation_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_translation', array());

    batch_set($batch);
    batch_process();
  }
  elseif ($form_state['values']['op'] === t('Replace special chars')) {
    // Create a batch job
    $batch['title'] = t('Replacing special chars');
    $batch['file'] = drupal_get_path('module', 'ndla_convert') . '/ndla_convert.admin.inc';
    $batch['progress_message'] = t('Completed @current out of @total jobs.');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_replacechars_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_replacechars', array());

    batch_set($batch);
    batch_process();
  }
  elseif ($form_state['values']['op'] === t('Only one')) {
    // Create a batch job
    $batch['title'] = t('Converting Question Sets');
    $batch['file'] = drupal_get_path('module', 'ndla_convert') . '/ndla_convert.admin.inc';
    $batch['progress_message'] = t('Completed @current out of @total jobs.');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_onlyone_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_onlyone', array());

    batch_set($batch);
    batch_process();
  }
}

/**
 * Process batch job
 */
function ndla_convert_ios_to_h5p_fix_radios(&$context) {
  if (!isset($context['sandbox']['current'])) {
    // Initial setup
    $context['results']['done'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['tasks'] = array();

    // Find tasks
    $result = db_query("SELECT n.ios_nid, n.h5p_nid FROM {amendor_ios_task} a JOIN {ndla_convert_ios_to_h5p} n ON n.ios_nid = a.nid WHERE xmlContent LIKE '<MCSTask%' AND xmlContent LIKE '%radiobuttons=%'");
    while ($task = db_fetch_object($result)) {
      $context['sandbox']['tasks'][] = $task;
    }
  }

  $number = count($context['sandbox']['tasks']);
  if (!$number) {
    return; // Nothing to fix
  }

  // Process
  $task = $context['sandbox']['tasks'][$context['sandbox']['current']];

  $xmlContent = db_result(db_query("SELECT xmlContent FROM {amendor_ios_task} WHERE nid = %d", $task->ios_nid));
  $h5p_node = new stdClass();
  ndla_convert_xmlContent_to_h5p($xmlContent, $h5p_node);
  db_query("UPDATE {h5p_nodes} SET filtered = '', json_content = '%s' WHERE nid = %d", $h5p_node->json_content, $task->h5p_nid);

  // Track progress
  $context['results']['done']++;
  $context['sandbox']['current']++;
  $context['message'] = t('Re-converted %current out of %number tasks.', array('%current' => $context['sandbox']['current'], '%number' => $number));

  if ($context['sandbox']['current'] !== $number) {
    $context['finished'] = $context['sandbox']['current'] / $number;
  }
}

/**
 * Batch job completed
 */
function ndla_convert_ios_to_h5p_fix_radios_finished($success, $results) {
  drupal_set_message(t('Successfully re-converted %num tasks.', array('%num' => $results['done'])));
}

/**
 * Process batch job
 */
function ndla_convert_ios_to_h5p_translation(&$context) {
  if (!isset($context['sandbox']['current'])) {
    // Initial setup
    $context['results']['updated'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['tasks'] = array();

    // Find tasks
    $result = db_query("SELECT n.nid, n.language FROM {ndla_convert_ios_to_h5p} nc JOIN node n ON n.nid = nc.h5p_nid WHERE language IN ('nb', 'nn')");
    while ($task = db_fetch_object($result)) {
      $context['sandbox']['tasks'][] = $task;
    }
  }

  $number = count($context['sandbox']['tasks']);
  if (!$number) {
    return; // Nothing to fix
  }

  // Process
  $task = $context['sandbox']['tasks'][$context['sandbox']['current']];

  $h5p = db_fetch_object(db_query("SELECT hn.json_content, hl.machine_name FROM {h5p_nodes} hn JOIN {h5p_libraries} hl ON hl.library_id = hn.main_library_id WHERE hn.nid = %d", $task->nid));
  $params = json_decode($h5p->json_content);
  switch ($h5p->machine_name) {
    case 'H5P.QuestionSet':
      ndla_convert_ios_to_h5p_translate_questionset($params, $task->language, $context['results']['updated']);
      break;

    case 'H5P.MultiChoice':
      ndla_convert_ios_to_h5p_translate_multichoice($params, $task->language, $context['results']['updated']);
      break;

    case 'H5P.DragQuestion':
      ndla_convert_ios_to_h5p_translate_dragquestion($params, $task->language, $context['results']['updated']);
      break;
  }
  db_query("UPDATE {h5p_nodes} SET filtered = '', json_content = '%s' WHERE nid = %d", json_encode($params), $task->nid);

  // Track progress
  $context['sandbox']['current']++;
  $context['message'] = t('Translated %current out of %number tasks.', array('%current' => $context['sandbox']['current'], '%number' => $number));

  if ($context['sandbox']['current'] !== $number) {
    $context['finished'] = $context['sandbox']['current'] / $number;
  }
}

/**
 * Update question set translations
 */
function ndla_convert_ios_to_h5p_translate_questionset(&$params, $language, &$counter) {
  if ($params->introPage->startButtonText === 'Start Quiz') {
    $params->introPage->startButtonText = 'Start';
    $counter++;
  }
  if ($params->texts->prevButton === 'Previous') {
    $params->texts->prevButton = 'Forrige';
    $counter++;
  }
  if ($params->texts->nextButton === 'Next') {
    $params->texts->nextButton = 'Neste';
    $counter++;
  }
  if ($params->texts->finishButton === 'Finish') {
    $params->texts->finishButton = 'Avslutt';
    $counter++;
  }
  if ($params->texts->textualProgress === 'Question: @current of @total questions') {
    $params->texts->textualProgress = ($language === 'nb' ? 'Deloppgave @current av @total' : 'Deloppgåve @current av @total');
    $counter++;
  }
  if (!isset($params->endGame->scoreString)) {
    $params->endGame->scoreString = 'Du fikk @score poeng av @total mulige.';
    $counter++;
  }
  if ($params->endGame->successGreeting === 'Congratulations!') {
    $params->endGame->successGreeting = 'Gratulerer!';
    $counter++;
  }
  if ($params->endGame->successComment === 'You did very well!') {
    $params->endGame->successComment = 'Du presterte meget godt!';
    $counter++;
  }
  if ($params->endGame->failGreeting === 'Oh, no!') {
    $params->endGame->failGreeting = ($language === 'nb' ? 'Ikke bestått' : 'Ikkje bestått');
    $counter++;
  }
  if ($params->endGame->failComment === "This didn't go so well.") {
    $params->endGame->failComment = 'Det er litt mange feil her.';
    $counter++;
  }
  if ($params->endGame->solutionButtonText === 'Show solution') {
    $params->endGame->solutionButtonText = 'Vis fasit';
    $counter++;
  }
  if ($params->endGame->finishButtonText === 'Finish') {
    $params->endGame->finishButtonText = 'Avslutt';
    $counter++;
  }
  if ($params->endGame->skipButtonText === 'Skip video') {
    $params->endGame->skipButtonText = 'Hopp over';
    $counter++;
  }

  // Update questions
  foreach ($params->questions as &$question) {
    if ($question->library === 'H5P.MultiChoice 1.0') {
      ndla_convert_ios_to_h5p_translate_multichoice($question->params, $language, $counter);
    }
    elseif ($question->library === 'H5P.DragQuestion 1.0') {
      ndla_convert_ios_to_h5p_translate_dragquestion($question->params, $language, $counter);
    }
  }
}

/**
 * Update multi choice translations
 */
function ndla_convert_ios_to_h5p_translate_multichoice(&$params, $language, &$counter) {
  if ($params->UI->showSolutionButton === 'Show solution') {
    $params->UI->showSolutionButton = 'Vis svar';
    $counter++;
  }
  if (!isset($params->UI->tryAgainButton) || $params->UI->tryAgainButton === 'Retry') {
    $params->UI->tryAgainButton = 'Prøv igjen';
    $counter++;
  }
  if ($params->UI->correctText === 'Correct!') {
    $params->UI->correctText = 'Rett!';
    $counter++;
  }
  if ($params->UI->almostText === 'Almost!') {
    $params->UI->almostText = 'Nesten!';
    $counter++;
  }
  if ($params->UI->wrongText === 'Wrong!') {
    $params->UI->wrongText = 'Feil';
    $counter++;
  }
}

/**
 * Update drag question translations
 */
function ndla_convert_ios_to_h5p_translate_dragquestion(&$params, $language, &$counter) {
  if ($params->scoreShow === 'Show score') {
    $params->scoreShow = 'Vis svar';
    $counter++;
  }
  if (!isset($params->tryAgain) || $params->tryAgain === 'Retry') {
    $params->tryAgain = 'Prøv igjen';
    $counter++;
  }
  if ($params->correct === 'Solution') {
    $params->correct = 'Korrekt';
    $counter++;
  }

  foreach ($params->question->task->elements as &$element) {
    if ($element->library === 'H5P.Image 1.0') {
      $element->params->contentName = ($language === 'nb' ? 'Bilde' : 'Bilete');
      $counter++;
    }
  }
}

/**
 * Batch job completed
 */
function ndla_convert_ios_to_h5p_translation_finished($success, $results) {
  drupal_set_message(t('Successfully updated %num translations.', array('%num' => $results['updated'])));
}

/**
 * Process batch job
 */
function ndla_convert_ios_to_h5p_replacechars(&$context) {
  if (!isset($context['sandbox']['current'])) {
    // Initial setup
    $context['results']['updated'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['tasks'] = array();

    // Find tasks
    $result = db_query("SELECT hn.nid FROM {ndla_convert_ios_to_h5p} nc JOIN {h5p_nodes} hn ON hn.nid = nc.h5p_nid WHERE hn.json_content LIKE '%qzx%' OR hn.json_content LIKE '%qzy%'");
    while ($task = db_fetch_object($result)) {
      $context['sandbox']['tasks'][] = $task->nid;
    }
  }

  $number = count($context['sandbox']['tasks']);
  if (!$number) {
    return; // Nothing to fix
  }

  // Process
  $task = $context['sandbox']['tasks'][$context['sandbox']['current']];

  $parameters = db_result(db_query("SELECT json_content FROM {h5p_nodes} WHERE nid = %d", $task));
  $counter = 0;
  $parameters = str_replace(array('qzx', 'qzy'), array('%', '+'), $parameters, $counter);
  db_query("UPDATE {h5p_nodes} SET filtered = '', json_content = '%s' WHERE nid = %d", $parameters, $task);
  $context['results']['updated'] += $counter;

  // Track progress
  $context['sandbox']['current']++;
  $context['message'] = t('Replaced chars in %current out of %number tasks.', array('%current' => $context['sandbox']['current'], '%number' => $number));

  if ($context['sandbox']['current'] !== $number) {
    $context['finished'] = $context['sandbox']['current'] / $number;
  }
}

/**
 * Batch job completed
 */
function ndla_convert_ios_to_h5p_replacechars_finished($success, $results) {
  drupal_set_message(t('Successfully replaced %num special chars.', array('%num' => $results['updated'])));
}

/**
 * Process batch job
 */
function ndla_convert_ios_to_h5p_onlyone(&$context) {
  if (!isset($context['sandbox']['current'])) {
    // Initial setup
    $context['results']['done'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['tasks'] = array();
    $context['results']['kalle'] = array();

    // Find all questions sets that has been converted
    $library_id = ndla_convert_get_library_id('H5P.Questionset', 1, 0);
    $result = db_query("SELECT hn.nid FROM {h5p_nodes} hn JOIN {ndla_convert_ios_to_h5p} n ON n.h5p_nid = hn.nid WHERE hn.main_library_id = %d", $library_id);
    while ($nid = db_result($result)) {
      $context['sandbox']['tasks'][] = $nid;
    }
  }

  $number = count($context['sandbox']['tasks']);
  if (!$number) {
    return; // Nothing to fix
  }

  // Process
  $nid = $context['sandbox']['tasks'][$context['sandbox']['current']];
  $set = json_decode(db_result(db_query("SELECT json_content FROM {h5p_nodes} WHERE nid = %d", $nid)));
  if (count($set->questions) === 1) {
    // Let's converte!
    $library_id = ndla_convert_get_library_id('H5P.MultiChoice', 1, 0);
    db_query("UPDATE {h5p_nodes} SET main_library_id = %d, json_content = '%s' WHERE nid = %d", $library_id, json_encode($set->questions[0]->params), $nid);
    $context['results']['done']++;
  }

  // Track progress
  $context['sandbox']['current']++;
  $context['message'] = t('Checking %current out of %number question sets.', array('%current' => $context['sandbox']['current'], '%number' => $number));

  if ($context['sandbox']['current'] !== $number) {
    $context['finished'] = $context['sandbox']['current'] / $number;
  }
}

/**
 * Batch job completed
 */
function ndla_convert_ios_to_h5p_onlyone_finished($success, $results) {
  drupal_set_message(t('Successfully converted %num question sets.', array('%num' => $results['done'])));
}
