<div id='form-step-display'>
  <div id='form-step-canvas'>
    <div class='ndla-form-step ndla-form-step-blue'>
      <div class='ndla-form-step-text'>
        <?php print t('Create') . " " . $type  ?>
      </div>
      <div class='ndla-form-step-right'>&nbsp;</div>
    </div>
    <?php
      
    foreach($steps as $step => $name) {
      $color = ($step === 0) ? 'green' : 'gray';
      if($step === 'save-now') {
        print "<div id='ndla-form-step-save-container'>&nbsp;";
      }
        
      print "<div class='ndla-form-step ndla-form-step-$color' id='ndla-form-step-$step'><div class='ndla-form-step-left'>&nbsp;</div>
      <div class='ndla-form-step-text'>
      $name
      </div>
      <div class='ndla-form-step-right'>&nbsp;</div></div>";
    }
    ?>
    </div>
  </div>
  <div style='clear: both;'>&nbsp;</div>
</div>