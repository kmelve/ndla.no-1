var ndla_form_step_current = 0;

Drupal.behaviors.ndla_form_step = function() {
  
  width = $('#ndla-form-step-save-view').position().left - $('#form-step-display').position().left;
  $('#form-step-display').attr('style', 'width:' + width + 'px;');
  
  $('#ndla-form-step-save-container').hover(function() {
    width = $('#ndla-form-step-save-view').width() + $('#ndla-form-step-save-edit').width();
    $('#form-step-canvas').stop().animate({ right: width }, 'slow');
  }, function() {
    $('#form-step-canvas').stop().animate({ right: 0 }, 'slow');
  });
  
  $('#node-form input, #node-form textarea, #node-form select').change(function(){
    $('#ndla-form-step-save-now, #ndla-form-step-save-edit, #ndla-form-step-save-view').removeClass('ndla-form-step-gray').addClass('ndla-form-step-orange');
  });  
  $('.ndla-form-step').each(function(){
    step = this.id.replace(/[^\d]*/, '');
    if(step == '')
      return;
    var error = $('#form-step-' + step + '-container').find('.error').length;
    if(error != 0) {
      $(this).removeClass('ndla-form-step-gray').removeClass('ndla-form-step-green').addClass('ndla-form-step-pink');
    }
  });
  $('#form-step-next').click(ndla_form_step_next);
  $('#form-step-previous').click(ndla_form_step_previous);
  $('#form-step-cancel').click(ndla_form_step_cancel);
  $('#ndla-form-step-save-view').click(function(event){
    ndla_form_submit_h5p(event);
    $('form#node-form input#edit-submit').click();
  });
  $('#ndla-form-step-save-edit').click(function(){
    ndla_form_submit_h5p(event);
    $('#edit-form-step-save-and-continue').val('1');
    $('form#node-form input#edit-submit').click();
  });
  $('.ndla-form-step').each(function(key, element){
    var id;
    if(id = $(element).attr('id').match(/ndla-form-step-([\d])/)) {
      $(element).click(function() {
        ndla_form_step_change(id[1]);
      });
    }
  });
  ndla_form_step_navigation(0);
}

ndla_form_step_change = function(step) {
  ndla_form_step_current = step;
  $('.ndla-form-step-green').removeClass('ndla-form-step-green').addClass('ndla-form-step-gray');
  $('#ndla-form-step-' + step).removeClass('ndla-form-step-gray').addClass('ndla-form-step-green');
  $('.form-step-container').hide();
  $('#form-step-' + step + '-container').show();
  ndla_form_step_navigation(step);
}

$(window).load(function(){
  tinyMCE.editors.forEach(
    function(ed){
      ed.onChange.add(
        function(){
          $('#ndla-form-step-save-now, #ndla-form-step-save-edit, #ndla-form-step-save-view').removeClass('ndla-form-step-gray').addClass('ndla-form-step-orange');
        }
      );
    }
  );
});

ndla_form_step_navigation = function(step) {
  if(step == 0) {
    $('#form-step-previous').hide();
  } else {
    $('#form-step-previous').show();
  }
  if($('#form-step-' + (step + 1) + '-container').length == 0) {
    $('#form-step-next').hide();
    $('#edit-submit').show();
    $('#edit-submit-and-queue').show();
    $('#edit-delete').show();
  } else {
    $('#form-step-next').show();
    $('#edit-submit').hide();
    $('#edit-submit-and-queue').hide();
    $('#edit-delete').hide();
  }
}

ndla_form_step_next = function() {
  ndla_form_step_current++;
  ndla_form_step_change(ndla_form_step_current);
}

ndla_form_step_previous = function() {
  ndla_form_step_current--;
  ndla_form_step_change(ndla_form_step_current);
}

ndla_form_step_cancel = function() {
  if (confirm(Drupal.t('Are you sure you want to cancel? Any changes will not be saved.'))){
  	var url = Drupal.settings.ndla_utils.base_url + Drupal.settings.basePath + Drupal.settings.ndla_utils.language + '/node/' + Drupal.settings.ndla_utils.nid;
    window.location = url;
  }
}

ndla_form_submit_h5p = function(event) {
  /**
   * h5p needs to call its submit function
   * to save its data. This is a fix to call
   * that anonymous function when save is triggered.
   */
  for(i in $("#node-form").data("events").submit) {
    if($("#node-form").data("events").submit[i].toString().match(/h5p/) != null) {
      $("#node-form").data("events").submit[i](event);
    }
  }
}