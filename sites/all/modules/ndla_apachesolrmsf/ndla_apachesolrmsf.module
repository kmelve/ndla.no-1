<?php

/**
 * @file
 *  ndla_apachesolrmsf.module php file
 *  Drupal module ndla_apachesolrmsf. Adds multi select facets to apachesolr searches.
 */

/**
 * Implementation of hook_init().
 */
function ndla_apachesolrmsf_init() {
  // Make sure we search even if we have no keywords or filters.
  if (arg(0) == 'search') {
    $_GET['filters'] = TRUE;
  }
}

/**
 * Implementation of hook_apachesolr_facets().
 */
function ndla_apachesolrmsf_apachesolr_facets($name = NULL) {
  static $facets;
  if ($facets == NULL) {
    $facets['sm_ndla_cleartext_years'] = array(
      'info' => t('NDLA ASMF: Cleartext trinn'),
      'facet_field' => 'sm_ndla_cleartext_years',
      'block_title' => t('Trinn'),
    );
    $facets['im_approved'] = array(
      'info' => t('NDLA ASMSF: Approved content'),
      'facet_field' => 'im_approved',
      'block_title' => t('Approved content')
    );
    $facets['type'] = array(
      'info' => t('NDLA ASMSF: Content type'),
      'facet_field' => 'type',
      'block_title' => t('Content types')
    );
    $facets['language'] = array(
      'info' => t('NDLA ASMSF: Language'),
      'facet_field' => 'language',
      'block_title' => t('Languages')
    );
    if (module_exists('ndla_authors')) {
      $facets['im_person_nid'] = array(
        'info' => t('NDLA ASMSF: Person id'),
        'facet_field' => 'im_person_nid',
        'block_title' => t('Person')
      );
    }
    if (module_exists('og')) {
      $facets['im_og_gid'] = array(
        'info' => t('NDLA ASMSF: Organic Group'),
        'facet_field' => 'im_og_gid',
        'block_title' => t('Groups')
      );
    }
    if (module_exists('taxonomy')) {
      $vocabularies = taxonomy_get_vocabularies();
      foreach ($vocabularies as $vid => $vocabulary) {
        $field = 'im_vid_' . $vid;
        $facets[$field] = array(
          'info' => t('NDLA ASMSF: Vocabulary "@name"', array('@name' => $vocabulary->name)),
          'facet_field' => $field,
          'block_title' => $vocabulary->name
        );
      }
    }
    if (module_exists('ndla_deling_subjects')) {    
      $facets['sm_ndla_common_courses'] = array(
        'info' => t('NDLA ASMSF: Common courses'),
        'facet_field' => 'sm_ndla_common_courses',
        'block_title' => t('Common courses')
      );
      $facets['sm_ndla_nygiv_common_courses'] = array(
        'info' => t('NDLA ASMSF: NyGIV common courses'),
        'facet_field' => 'sm_ndla_nygiv_common_courses',
        'block_title' => t('Common courses')
      );
      $facets['sm_ndla_years'] = array(
        'info' => t('NDLA ASMSF: School years'),
        'facet_field' => 'sm_ndla_years',
        'block_title' => t('School years')
      );
      $facets['sm_ndla_nygiv_years'] = array(
        'info' => t('NDLA ASMSF: NyGIV school years'),
        'facet_field' => 'sm_ndla_nygiv_years',
        'block_title' => t('School years')
      );
      $facets['sm_ndla_aim_areas'] = array(
        'info' => t('NDLA ASMSF: Aim areas'),
        'facet_field' => 'sm_ndla_aim_areas',
        'block_title' => t('Aim areas')
      );
      $facets['sm_ndla_nygiv_aim_areas'] = array(
        'info' => t('NDLA ASMSF: NyGIV aim areas'),
        'facet_field' => 'sm_ndla_nygiv_aim_areas',
        'block_title' => t('Aim areas')
      );
      $facets['sm_ndla_aims'] = array(
        'info' => t('NDLA ASMSF: Competence aims'),
        'facet_field' => 'sm_ndla_aims',
        'block_title' => t('Competence aims')
      );
      $facets['sm_ndla_nygiv_aims'] = array(
        'info' => t('NDLA ASMSF: NyGIV competence aims'),
        'facet_field' => 'sm_ndla_nygiv_aims',
        'block_title' => t('Competence aims')
      );
      $facets['sm_ndla_vocational'] = array(
        'info' => t('NDLA ASMSF: Vocational programs'),
        'facet_field' => 'sm_ndla_vocational',
        'block_title' => t('Vocational programs')
      );
      $facets['sm_ndla_preparatory'] = array(
        'info' => t('NDLA ASMSF: Preparatory programs'),
        'facet_field' => 'sm_ndla_preparatory',
        'block_title' => t('Preparatory programs')
      );
      $facets['sm_ndla_programme_areas'] = array(
        'info' => t('NDLA ASMSF: Programme areas'),
        'facet_field' => 'sm_ndla_programme_areas',
        'block_title' => t('Programme areas')
      );
      $facets['sm_ndla_nygiv_programme_areas'] = array(
        'info' => t('NDLA ASMSF: NyGIV programme areas'),
        'facet_field' => 'sm_ndla_nygiv_programme_areas',
        'block_title' => t('Programme areas')
      );
      $facets['sm_ndla_curricula'] = array(
        'info' => t('NDLA ASMSF: Curricula'),
        'facet_field' => 'sm_ndla_curricula',
        'block_title' => t('Curricula')
      );
      $facets['sm_ndla_nygiv_curricula'] = array(
        'info' => t('NDLA ASMSF: NyGIV curricula'),
        'facet_field' => 'sm_ndla_nygiv_curricula',
        'block_title' => t('Curricula')
      );
    }
    if(module_exists('ndla_ord')) {
      $facets[ndla_ord_keywords_ids_solr_key()] = array(
        'info' => t('NDLA ASMSF: Keywords'),
        'facet_field' => ndla_ord_keywords_ids_solr_key(),
        'block_title' => t('Keyword')
      );
    }

    if (module_exists('ndla_grep_ndla')) {
      $facets['sm_ndla_grep_competence_aims_id'] = array(
        'info' => t('NDLA ASMSF: Grep Aims'),
        'facet_field' => 'sm_ndla_grep_competence_aims_id',
        'block_title' => t('Competence aims')
      );
    }
    
    if(module_exists('ndla_fagontologi')) {
      $facets[ndla_fagontologi_topics_ids_solr_key()] = array(
        'info' => t('NDLA ASMSF: Topics'),
        'facet_field' => ndla_fagontologi_topics_ids_solr_key(),
        'block_title' => t('Topics'),
      );
    }
  }

  if ($name != NULL) {
    if (isset($facets[$name])) {
      return $facets[$name];
    }
    else {
      return;
    }
  }

  return $facets;
}

/**
 * Implementation of hook_apachesolr_modify_query().
 */
function ndla_apachesolrmsf_apachesolr_modify_query(&$query, &$params, $caller) {
  $enabled_facets = apachesolr_get_enabled_facets('ndla_apachesolrmsf');
  $hierarchy = variable_get('ndla_apachesolrmsf_hierarchy', array());
  $exclude_parent = variable_get('ndla_apachesolrmsf_exclude_parent', array());
  $exclude_children = variable_get('ndla_apachesolrmsf_exclude_children', array());
  $skip_fyr = array('sm_ndla_nygiv_common_courses', 'sm_ndla_vocational');
  foreach ($enabled_facets as $name) {
    if(module_exists('ndla_nygiv')) {
      if(ndla_nygiv_context() && in_array($name, $skip_fyr)) {
        continue;
      }
    }
    if (isset($_GET[$name]) && is_array($_GET[$name])) {
      $facets = $_GET[$name];
      $subquery = apachesolr_drupal_query();
      $taxonomy = substr($name, 0, 7) == 'im_vid_';

      if (isset($hierarchy[$name]) && $hierarchy[$name] && isset($exclude_parent[$name]) && $exclude_parent[$name] && $taxonomy) {
        // Exclude parents from search when a child is selected.
        foreach ($facets as &$id) {
          $parent = db_result(db_query("SELECT parent FROM {term_hierarchy} WHERE tid = %d", $id));
          if ($parent) {
            foreach ($facets as $key => &$id) {
              if ($id == $parent) {
                unset($facets[$key]);
              }
            }
          }
        }
      }

      foreach ($facets as &$id) {
        $subquery->add_filter($name, $id);

        if (isset($hierarchy[$name]) && $hierarchy[$name] && isset($exclude_children[$name]) && $exclude_children[$name] && $taxonomy) {
          $children = ndla_apachesolrmsf_find_children($id);
          if ($children) {
            foreach ($children as &$child) {
              if (!in_array($child, $facets)) {
                $subquery->add_filter($name, $child, TRUE);
              }
            }
          }
        }
      }
      $query->add_subquery($subquery, 'OR');
    }
  }
}

/**
 * Find children.
 * 
 * @param int $parent Parent identifier.
 * @return array Children identifiers. 
 */
function ndla_apachesolrmsf_find_children($parent) {
  $result = db_query("SELECT tid FROM {term_hierarchy} WHERE parent = %d", $parent);
  while ($tid = db_result($result)) {
    $children[$tid] = $tid;
    $grand_children = ndla_apachesolrmsf_find_children($tid);
    if ($grand_children) {
      $children = array_merge($children, $grand_children);
    }
  }
  return isset($children) ? $children : 0;
}

/**
 * Implementation of hook_theme().
 */
function ndla_apachesolrmsf_theme($existing, $type, $theme, $path) {
  return array(
    'ndla_apachesolrmsf_current_search' => array(
      'arguments' => array('fields' => NULL),
      'path' => $path . '/templates',
      'template' => 'ndla-apachesolrmsf-current-search',
    ),
    'ndla_apachesolrmsf_facet' => array(
      'arguments' => array('fields' => NULL, 'form_values' => NULL),
      'path' => $path . '/templates',
      'template' => 'ndla-apachesolrmsf-facet',
    ),
    'ndla_apachesolrmsf_facet_fields' => array(
      'arguments' => array('facet' => NULL, 'fields' => NULL),
      'path' => $path . '/templates',
      'template' => 'ndla-apachesolrmsf-facet-fields',
    ),
    'ndla_apachesolrmsf_selected_facet' => array(
      'arguments' => array('fields' => NULL, 'url' => NULL),
      'path' => $path . '/templates',
      'template' => 'ndla-apachesolrmsf-selected-facet',
    ),
    'ndla_apachesolrmsf_selected_facet_fields' => array(
      'arguments' => array('fields' => NULL, 'facet' => NULL),
      'path' => $path . '/templates',
      'template' => 'ndla-apachesolrmsf-selected-facet-fields',
    )
  );
}

/**
 * Implementation of hook_block().
 */
function ndla_apachesolrmsf_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks['current_search'] = array(
        'info' => t('NDLA ASMSF: Current search'),
        'cache' => BLOCK_CACHE_PER_PAGE,
      );

      $enabled_facets = apachesolr_get_enabled_facets('ndla_apachesolrmsf');
      $names = ndla_apachesolrmsf_apachesolr_facets();
      foreach ($enabled_facets as $key) {
        $blocks[$key] = array(
          'info' => $names[$key]['info'],
          'cache' => BLOCK_CACHE_PER_PAGE,
        );
      }
      return $blocks;

    case 'view':
      // No solr search equals no blocks.
      if (!apachesolr_has_searched() || arg(0) != 'search') {
        return;
      }
      static $js_added;
      if(empty($js_added)) {
        $path = drupal_get_path('module', 'ndla_apachesolrmsf');
        drupal_add_js($path . "/js/ndla_apachesolrmsf.js");
        $js_added = TRUE;
      }

      // Get name of facet.
      $facet = $delta;
      $facet_info = ndla_apachesolrmsf_apachesolr_facets($facet);
      $block['subject'] = $facet_info['block_title'];

      if ($facet == 'current_search') {
        // This isn't really a facet, just the search summary.
        $block['subject'] = t("You're searching");
        $block['content'] = '';
        $keys = search_get_keys();
        if ($keys) {
          $block['content'] .= theme('ndla_apachesolrmsf_selected_facet_fields', array(
            (object) array(
              'name' => search_get_keys(),
              'url' => ndla_apachesolrmsf_rebuild_search_url(NULL, NULL, TRUE)
            )
            ));
        }

        // Add selected fields from enabled facets.
        $enabled_facets = apachesolr_get_enabled_facets('ndla_apachesolrmsf');
        foreach ($enabled_facets as $name) {
          if (isset($_GET[$name])) {
            $block['content'] .= ndla_apachesolrmsf_get_selected_facet_fields($name);
          }
        }

        // TODO: Move GREP hax to another module.
        if (isset($_GET['grep']) && is_array($_GET['grep'])) {
          $greps = _getAimNames($_GET['grep']);
          foreach ($_GET['grep'] as $i => $id) {
            $grep_fields[] = (object) array(
                'id' => $_GET['grep'][$i],
                'name' => $greps[$id],
            );
          }
          $block['content'] .= theme('ndla_apachesolrmsf_selected_facet_fields', $grep_fields, 'grep');
        }

        if ($block['content'] == '') {
          return;
        }
        $block['content'] = theme('ndla_apachesolrmsf_current_search', $block['content']);
        return $block;
      }


      if (isset($_GET[$facet])) {
        // Display selected facets
        $block['content'] = theme('ndla_apachesolrmsf_selected_facet', ndla_apachesolrmsf_get_selected_facet_fields($facet), ndla_apachesolrmsf_rebuild_search_url($facet) . '&browse=1');
        return $block;
      }

      // Get the solr response.
      $response = apachesolr_static_response_cache();
      if (empty($response)) {
        return;
      }
      
      // Get facet fields
      $broken_fields = $response->facet_counts->facet_fields->$facet;

      foreach ($broken_fields as $key => $field) {
        $fields[$key] = $field;
      }
      if (!isset($fields) || count($fields) < 2) {
        return;
      }

      // Get facet field names
      $fields = ndla_apachesolrmsf_get_facet_data($facet, $fields);
      if (!$fields || count($fields) < 2) {
        return;
      }

      $block['content'] = theme('ndla_apachesolrmsf_facet', theme('ndla_apachesolrmsf_facet_fields', $facet, $fields), ndla_apachesolrmsf_get_facet_form_values());
      return $block;

    case 'configure':
      if ($delta != 'current_search') {
        return ndla_apachesolrmsf_block_configure($delta);
      }
      break;

    case 'save':
      if ($delta != 'current_search') {
        ndla_apachesolrmsf_block_save($edit);
      }
      break;
  }
}

/**
 * Get data for the facet fields.
 * 
 * @staticvar static $facet_data Caches the data for all facets.
 * @param string $facet Name of the facet.
 * @param array $fields The keys are the ids of the fields.
 * @return array Facet data. 
 */
function ndla_apachesolrmsf_get_facet_data($facet, $fields, $with_image = FALSE) {
  global $language;
  $facet_data = array();
  $cid = 'ndla_apachesolrmsf_' . $facet . '_' . $language->language . '_' . $with_image;
  $cache = cache_get($cid);
  $keyword_field = $fagontologi_field = 'meep_moop_dummy';
  
  if(module_exists('ndla_fagontologi')) {
    $fagontologi_field = ndla_fagontologi_topics_ids_solr_key();
  }
  if(module_exists('ndla_ord')) {
    $keyword_field = ndla_ord_keywords_ids_solr_key();
  }
  
  if (1 || !$cache) {
    switch ($facet) {
      case 'type':
        $node_types = node_get_types();
        foreach ($node_types as $name => $values) {
          if (module_exists('i18nstrings')) {
            $values->name = i18nstrings('nodetype:type:' . $name . ':name', $values->name);
          }
          $facet_data[$facet][$name] = (object) array('id' => $name, 'name' => $values->name);
        }
        break;

      //Fagontologi ids (Topics)
      case $fagontologi_field:
        foreach($fields as $name => $value) {
          $topic_id = $name;
          $topic_name = ucfirst(ndla_fagontologi_get_name($topic_id));
          $facet_data[$facet][$name] = (object) array('id' => $topic_id, 'name' => $topic_name);
        }
        break;

      //Keyword ids
      case $keyword_field:
        foreach($fields as $name => $value) {
          $keyword_id = $name;
          $keyword_name = ucfirst(ndla_ord_id_to_name($keyword_id));
          $facet_data[$facet][$name] = (object) array('id' => $keyword_id, 'name' => $keyword_name);
        }
        break;
      case 'language':
        $languages = language_list();
        foreach ($languages as $key => &$language) {
          $facet_data[$facet][$key] = (object) array('id' => $key, 'name' => t($language->name));
        }
        $facet_data[$facet]['und'] = (object) array('id' => 'und', 'name' => t('Language neutral'));
        break;

      case 'im_og_gid':
        if (module_exists('og')) {
          $result = db_query('SELECT n.nid, n.title FROM {og} o JOIN {node} n ON n.status = 1 AND n.nid = o.nid;');
          while ($group = db_fetch_object($result)) {
            $facet_data[$facet][$group->nid] = (object) array('id' => $group->nid, 'name' => $group->title);
          }
          $facet_data[$facet]['_empty_'] = (object) array('id' => '_empty_', 'name' => t('No group'));
        }
        break;

      case 'im_person_nid':
        $result = db_query('SELECT na.person_nid, n.title FROM {ndla_authors} na INNER JOIN {node} n ON na.person_nid = n.nid');
        while ($group = db_fetch_object($result)) {
          $facet_data[$facet][$group->person_nid] = (object) array('id' => $group->person_nid, 'name' => $group->title);
        }
        $facet_data[$facet]['_empty_'] = (object) array('id' => '_empty_', 'name' => t('No person'));
        break;
      
      case 'sm_ndla_grep_competence_aims_id':
        global $language;
        $data = array();
        foreach($fields as $id => $hits) {
          $data[] = (object)array(
            'id' => $id,
            'name' => NdlaGrepNdlaClient::getAimNames($id),
            'parent' => 0,
            'hits' => $hits,
          );
        }
        return $data;
        break;
      default:
        $vid = substr($facet, 7);
        if ($vid) {
          $counter = 0;
          $result = db_query('SELECT td.tid, td.name, th.parent FROM {term_data} td JOIN {term_hierarchy} th ON td.vid = %d AND th.tid = td.tid', $vid);
          while ($term = db_fetch_object($result)) {
            // We also cache the subqueries. This is to make things easier for the server when the cache is deleted. 100 users are trying to build the same cache
            $cid_sub = 'ndla_apachesolrmsf_term' . $term->tid . '_' . $tern->name;
            $cache_sub = cache_get($cid_sub);
            if (!$cache_sub) {
              if(module_exists('i18ntaxonomy')) {
                $term->name = i18ntaxonomy_translate_term_name($term->tid, $term->name);
              }
              $facet_element = (object) array('id' => $term->tid, 'name' => $term->name, 'parent' => $term->parent);
              $facet_data[$facet][$term->tid] = $facet_element;
              cache_set($cid_sub, $facet_element);
            } else {
              $facet_data[$facet][$term->tid] = $cache_sub->data;
            }
          }
        }
    }
  
    cache_set($cid, $facet_data);
  }
  else {
    $facet_data = $cache->data;
  }
  
  // Filter out the fields we need
  $fields_data = array();
  foreach ($fields as $id => $hits) {
    if (isset($facet_data[$facet][$id])) {
      $fields_data[$id] = $facet_data[$facet][$id];
      $fields_data[$id]->hits = $hits;
      if (!isset($fields_data[$id]->parent)) {
        $fields_data[$id]->parent = 0;
      }
    }
  }
  
  $hierarchy = variable_get('ndla_apachesolrmsf_hierarchy', array());
  if (isset($hierarchy[$facet]) && $hierarchy[$facet]) {
    $exclude_children = variable_get('ndla_apachesolrmsf_exclude_children', array());
    $exclude_children = isset($exclude_children[$facet]) ? $exclude_children[$facet] : 0;
    $fields_tree = ndla_apachesolrmsf_get_facet_hierarchy($fields_data, 0, $exclude_children);

    if (!$fields_tree) {
      $fields_tree = array();
    }
    
    // Add leftovers
    foreach ($fields_data as &$field) {
      if (!isset($field->treed)) {
        $field->children = ndla_apachesolrmsf_get_facet_hierarchy($fields_data, $field->id, $exclude_children);
        $fields_tree[$field->id] = $field;
      }
    }
  }

  return isset($fields_tree) ? $fields_tree : $fields_data;
}

/**
 * Recursive function that builds a field hierarchy for a facet.
 * 
 * @param array $fields Flat field list.
 * @param int $parent Internal use only.
 * @return array Tree field list.
 */
function ndla_apachesolrmsf_get_facet_hierarchy(&$fields, $parent = '0', $exclude_children = 0) {
  foreach ($fields as &$field) {
    if ($field->parent == $parent) {
      $field->children = ndla_apachesolrmsf_get_facet_hierarchy($fields, $field->id, $exclude_children);
      if ($exclude_children) {
        if ($field->children) {
          $field->children_hits = 0;
          foreach ($field->children as &$child) {
            $field->children_hits += $child->children_hits;
          }

          $field->hits -= $field->children_hits;
          if ($field->hits < 0) {
            $field->hits = 0;
          }
        }
        else {
          $field->children_hits = $field->hits;
        }
      }
      
      $fields_tree[$field->id] = $field;
      $field->treed = 1;
    }
  }
  return isset($fields_tree) ? $fields_tree : 0;
}

/**
 * Return HTML for the selected facet fields.
 * 
 * @staticvar array $facets Caches the html.
 * @param string $facet Name of the facet.
 * @return string HTML. 
 */
function ndla_apachesolrmsf_get_selected_facet_fields($facet) {
  static $facets;

  if (!isset($facets[$facet])) {
    for ($i = 0, $s = count($_GET[$facet]); $i < $s; $i++) {
      $fields[$_GET[$facet][$i]] = 1;
    }
    $fields = ndla_apachesolrmsf_get_facet_data($facet, $fields);
    $facets[$facet] = theme('ndla_apachesolrmsf_selected_facet_fields', $fields, $facet);
  }
  return $facets[$facet];
}

/**
 * Function that caches facet form values.
 * 
 * @staticvar string $values The values.
 * @return string The values. 
 */
function ndla_apachesolrmsf_get_facet_form_values() {
  static $values;
  if ($values == NULL) {
    $values = ndla_apachesolrmsf_build_facet_form_values();
  }
  return $values;
}

/**
 * Recursive functions that builds a string of hiddene input fields to include in the facet form.
 * 
 * @param string $output Internal use only.
 * @param array $query Internal use only.
 * @param string $parent Internal use only.
 * @return string Internal use only.
 */
function ndla_apachesolrmsf_build_facet_form_values($skip = NULL, $output = '', $query = NULL, $parent = NULL) {
  if ($query == NULL) {
    $query = $_GET;
  }
  foreach ($query as $name => $value) {
    if (is_array($value)) {
      $output = ndla_apachesolrmsf_build_facet_form_values($skip, $output, $value, $name);
    }
    elseif ($name . '' != 'page' && (!variable_get('clean_url', 0) || $name . '' != 'q')) {
      if ($parent != NULL) {
        $name = $parent . '[]';
      }
      if ($name != $skip) {
        $output .= '<input type="hidden" name="' . $name . '" value="' . $value . '"/>';
      }
    }
  }
  return $output;
}

/**
 * Custom recursive function for building url query for the search page.
 * Because drupal_query_string_encode doesn't build them the way we need to have them.
 * 
 * @param string $skip_name Name to skip.
 * @param string $skip_value Value to skip.
 * @param array $query Internal use only.
 * @param string $url Internal use only.
 * @param string $parent Internal use only.
 * @param boolean $first Internal use only.
 * @return string The complete URL. 
 */
function ndla_apachesolrmsf_rebuild_search_url($skip_name = NULL, $skip_value = NULL, $drop_keys = FALSE, $query = NULL, $url = NULL, $parent = NULL, &$first = FALSE) {
  if ($url == NULL) {
    $keys = $drop_keys ? '' : search_get_keys();
    $url = url('search/apachesolr_search/' . $keys);
    if (!strpos($url, '?')) {
      $first = TRUE;
    }
  }
  if ($query == NULL) {
    $query = $_GET;
  }
  foreach ($query as $name => $value) {
    if (is_array($value)) {
      $url = ndla_apachesolrmsf_rebuild_search_url($skip_name, $skip_value, $drop_keys, $value, $url, $name, $first);
    }
    elseif ($name . '' != 'page' && $name . '' != 'q' && !(($skip_name == $parent . '' || $skip_name == $name . '') && ($skip_value == NULL || $value == $skip_value))) {
      if ($first) {
        $first = FALSE;
        $url .= '?';
      }
      else {
        $url .= '&amp;';
      }

      if ($parent != NULL) {
        $name = $parent . '[]';
      }
      $url .= $name . '=' . $value;
    }
  }
  return $url;
}

/**
 * Used to configure block settings.
 */
function ndla_apachesolrmsf_block_configure($delta) {
  $limits = variable_get('apachesolr_facet_query_limits', array());
  if (isset($limits['ndla_apachesolrmsf'])) {
    $limits = $limits['ndla_apachesolrmsf'];
  }
  $facet_missing = variable_get('apachesolr_facet_missing', array());
  if (isset($facet_missing['ndla_apachesolrmsf'])) {
    $facet_missing = $facet_missing['ndla_apachesolrmsf'];
  }
  $exclude_parent = variable_get('ndla_apachesolrmsf_exclude_parent', array());
  $exclude_children = variable_get('ndla_apachesolrmsf_exclude_children', array());
  $hierarchy = variable_get('ndla_apachesolrmsf_hierarchy', array());

  $form['ndla_apachesolrmsf_query_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum filter links'),
    '#description' => t('The maximum number of filter links to show in this block.'),
    '#default_value' => isset($limits[$delta]) ? $limits[$delta] : variable_get('apachesolr_facet_query_limit_default', 20),
    '#element_validate' => array('ndla_apachesolrmsf_validate')
  );
  $form['ndla_apachesolrmsf_missing'] = array(
    '#type' => 'radios',
    '#title' => t('Include a facet for missing'),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#description' => t('A facet can be generated corresponding to all documents entirely missing this field.'),
    '#default_value' => isset($facet_missing[$delta]) ? $facet_missing[$delta] : 0,
  );
  $form['ndla_apachesolrmsf_hierarchy'] = array(
    '#type' => 'radios',
    '#title' => t('Enable hierarchy'),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#description' => t('The fields get sortet in a tree view.'),
    '#default_value' => isset($hierarchy[$delta]) ? $hierarchy[$delta] : 0,
  );
  $form['ndla_apachesolrmsf_exclude_parent'] = array(
    '#type' => 'radios',
    '#title' => t('Exclude parent from search'),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#description' => t('The selected parent fields can be excluded from the search when one or more child fields are selected.'),
    '#default_value' => isset($exclude_parent[$delta]) ? $exclude_parent[$delta] : 0,
  );
  $form['ndla_apachesolrmsf_exclude_children'] = array(
    '#type' => 'radios',
    '#title' => t('Exclude children from search'),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#description' => t('The child fields can ble excluded from the search when only their parent is selected.'),
    '#default_value' => isset($exclude_children[$delta]) ? $exclude_children[$delta] : 0,
  );
  return $form;
}

/**
 * Validates that the form element contains a positive number.
 */
function ndla_apachesolrmsf_validate($element, &$form_state) {
  if (!is_numeric($element['#value']) || intval($element['#value']) != $element['#value'] || $element['#value'] < 1) {
    form_error($element, t('Must be a positive number.'));
  }
}

/**
 * Used to save configured block settings.
 */
function ndla_apachesolrmsf_block_save($edit) {
  $limits = variable_get('apachesolr_facet_query_limits', array());
  $limits['ndla_apachesolrmsf'][$edit['delta']] = $edit['ndla_apachesolrmsf_query_limit'];
  variable_set('apachesolr_facet_query_limits', $limits);

  $facet_missing = variable_get('apachesolr_facet_missing', array());
  $facet_missing['ndla_apachesolrmsf'][$edit['delta']] = $edit['ndla_apachesolrmsf_missing'];
  variable_set('apachesolr_facet_missing', $facet_missing);

  $hierarchy = variable_get('ndla_apachesolrmsf_hierarchy', array());
  $hierarchy[$edit['delta']] = $edit['ndla_apachesolrmsf_hierarchy'];
  variable_set('ndla_apachesolrmsf_hierarchy', $hierarchy);

  $exclude_parent = variable_get('ndla_apachesolrmsf_exclude_parent', array());
  $exclude_parent[$edit['delta']] = $edit['ndla_apachesolrmsf_exclude_parent'];
  variable_set('ndla_apachesolrmsf_exclude_parent', $exclude_parent);

  $exclude_children = variable_get('ndla_apachesolrmsf_exclude_children', array());
  $exclude_children[$edit['delta']] = $edit['ndla_apachesolrmsf_exclude_children'];
  variable_set('ndla_apachesolrmsf_exclude_children', $exclude_children);
}

function ndla_apachesolrmsf_preprocess_ndla_apachesolrmsf_facet_fields(&$vars) {
  foreach ($vars['fields'] as &$field) {
    $field->children = $field->children ? theme('ndla_apachesolrmsf_facet_fields', $vars['facet'], $field->children) : '';
    $_GET[$vars['facet']] = array($field->id);
    $field->url = ndla_apachesolrmsf_rebuild_search_url();
    unset($_GET[$vars['facet']]);
  }
}

function ndla_apachesolrmsf_preprocess_ndla_apachesolrmsf_selected_facet_fields(&$vars) {
  foreach ($vars['fields'] as &$field) {
    $field->children = isset($field->children) && $field->children ? theme('ndla_apachesolrmsf_selected_facet_fields', $field->children, $vars['facet']) : '';
    if (!isset($field->url)) {
      $field->url = ndla_apachesolrmsf_rebuild_search_url($vars['facet'], $field->id);
    }
  }
}
