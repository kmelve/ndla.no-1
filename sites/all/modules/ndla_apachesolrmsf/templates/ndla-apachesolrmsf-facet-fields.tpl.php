<ul>
  <?php foreach ($fields as &$field): ?>
    <li>
      <input type="checkbox" name="<?php print $facet ?>[]" value="<?php print $field->id ?>"/>
      <div class="field-wrap">
        <a href="<?php print $field->url ?>"><?php print $field->name ?></a>
        <span class="hits">(<?php print $field->hits ?>)</span>
      </div>
      <?php print $field->children ?>
    </li>
  <?php endforeach ?>
</ul>