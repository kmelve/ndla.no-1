<?php foreach ($fields as &$field): ?>
  <li>
    <a href="<?php print $field->url ?>" class="remove-field"></a>
    <div class="remove-field-wrap"><?php print $field->name == '' ? '&nbsp;' : check_plain($field->name) ?></div>
    <?php if ($field->children != ''): ?>
      <ul>
        <?php print $field->children ?>
      </ul>
    <?php endif ?>
  </li>
<?php endforeach ?>