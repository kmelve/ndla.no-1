/**
 * Removes all individual forms
 * and creates one form for all
 * ndla_apachesolrmsf facet forms
 */
Drupal.behaviors.ndla_apachesolrmsf = function(context) {
  /* Remove all forms and replace with form content */
  $('.region-left form').each(function() {
    $(this).replaceWith($(this).contents());
  });
  /* Add global form */
  $('.region-left').wrap('<form>');
}