<?php


/**
 * @file
 *  cookbook.install php file
 *  Installs the Drupal module cookbook.
 */

/**
 * Implementation of hook_schema
 */
function cookbook_schema() {
  return array(
    'cookbook_recipes' => array(
      'description' => t('Contains recipes.'),
      'fields' => array(
        'nid' => array(
          'description' => t('The node version identifier for a recipe.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'vid' => array(
          'description' => t('The node revision identifier for a recipe.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'ingredients' => array(
          'description' => t('The ingredients needed for this recipe.'),
          'type' => 'text',
          'size' => 'normal',
          'not null' => FALSE,
        ),
        'steps' => array(
          'description' => t('The steps needed to complete the recipe.'),
          'type' => 'text',
          'size' => 'normal',
          'not null' => FALSE,
        ),
        'history' => array(
          'description' => t("The recipe's history."),
          'type' => 'text',
          'size' => 'normal',
          'not null' => FALSE,
        ),
        'video' => array(
          'description' => t("The recipe's video."),
          'type' => 'text',
          'size' => 'normal',
          'not null' => FALSE,
        ),
        'servings' => array(
          'description' => t('Default number of servings for this recipe.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE
        ),
        'partial_nid' => array(
          'description' => t('Node identifier of a partial recipe to include.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE
        ),
        'partial_pos' => array(
          'description' => t('Position of partial recipe.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE
        )
      ),
      'primary key' => array('vid'),
      'indexes' => array(
        'nid' => array('nid'),
      ),
    ),
    'cookbook_recipe_images' => array(
      'description' => t("Contains recipe images."),
      'fields' => array(
        'id' => array(
          'description' => t('The primary identifier for an image that belongs to a recipe.'),
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'recipe_nid' => array(
          'description' => t('The node version identifier of the recipe the image belongs to.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'recipe_vid' => array(
          'description' => t("The node revision identifier of the recipe the image belongs to."),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'source' => array(
          'description' => t('The source to where the image can be located.'),
          'type' => 'varchar',
          'length' => 511,
          'not null' => FALSE,
          'default' => '',
        ),
        'weight' => array(
          'description' => t("The weight which determines the image's position in the image list."),
          'type' => 'int',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('id'),
      'indexes' => array(
        'recipe_nid' => array('recipe_nid'),
        'recipe_vid' => array('recipe_vid'),
      ),
    ),
    'cookbook_recipe_ingredients' => array(
      'description' => t("Contains a recipe's ingredients."),
      'fields' => array(
        'id' => array(
          'description' => t("The primary identifier for a recipe's ingredient."),
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'recipe_nid' => array(
          'description' => t('The node version identifier of the recipe the ingredient belongs to.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'recipe_vid' => array(
          'description' => t("The node revision identifier of the recipe the ingredient belongs to."),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'ingredient_nid' => array(
          'description' => t("The node version identifier of the ingredient used in the recipe."),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'unit_id' => array(
          'description' => t("The primary identifier of the unit the amount is measured in."),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'amount' => array(
          'description' => t("The amount used of the ingredient."),
          'type' => 'float',
          'not null' => TRUE,
        ),
        'display' => array(
          'description' => t("The custom ingredient name to display in the recipe."),
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
        'weight' => array(
          'description' => t("The weight which determines the ingredient's position in the recipe's ingredient list."),
          'type' => 'int',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('id'),
      'indexes' => array(
        'recipe_nid' => array('recipe_nid'),
        'recipe_vid' => array('recipe_vid'),
        'ingredient_nid' => array('ingredient_nid'),
        'unit_id' => array('unit_id'),
      ),
    ),
    'cookbook_ingredients' => array(
      'description' => t("Contains available ingredients for recipes."),
      'fields' => array(
        'nid' => array(
          'description' => t('The node version identifier for an ingredient.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'vid' => array(
          'description' => t('The node revision identifier for an ingredient.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'energy' => array(
          'description' => t('The level of energy in the ingredient, measured in kilocalories.'),
          'type' => 'float',
          'not null' => TRUE,
        ),
        'protein' => array(
          'description' => t('The level of protein in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => TRUE,
        ),
        'fat' => array(
          'description' => t('The level of fat in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => TRUE,
        ),
        'carbohydrate' => array(
          'description' => t('The level of carbohydrates in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => TRUE,
        ),
        'saturedtransfattyacids' => array(
          'description' => t('The level of saturated+trans fatty acids in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'cismonounsaturated' => array(
          'description' => t('The level of cis-monounsaturated in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'cispolyunsaturatedfattyacids' => array(
          'description' => t('The level of cis-polyunsaturated fatty acids in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'cholesterol' => array(
          'description' => t('The level of cholesterol in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'starch' => array(
          'description' => t('The level of starch in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'monodisaccharides' => array(
          'description' => t('The level of mono+disaccharides in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'dietaryfiber' => array(
          'description' => t('The level of dietary fiber in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'addedsugar' => array(
          'description' => t('The level of added sugar in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'alcohol' => array(
          'description' => t('The level of alcohol in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'calcium' => array(
          'description' => t('The level of calcium in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'iron' => array(
          'description' => t('The level of iron in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'sodium' => array(
          'description' => t('The level of sodium in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'potassium' => array(
          'description' => t('The level of potassium in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'magnesium' => array(
          'description' => t('The level of magnesium in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'zinc' => array(
          'description' => t('The level of zinc in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'selenium' => array(
          'description' => t('The level of selenium in the ingredient, measured in micrograms.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'phosphorus' => array(
          'description' => t('The level of phosphorus in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'vitamina' => array(
          'description' => t('The level of vitamin A in the ingredient, measured in retinol activity equivalents.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'vitamind' => array(
          'description' => t('The level of vitamin D in the ingredient, measured in micrograms.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'vitamine' => array(
          'description' => t('The level of vitamin E in the ingredient, measured in alpha-tocopherol equivalents.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'thiamine' => array(
          'description' => t('The level of thiamine (vitamin B1) in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'riboflavin' => array(
          'description' => t('The level of riboflavin (vitamin B2) in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'niacinequivalents' => array(
          'description' => t('The level of niacin equivalents in the ingredient, measured in niacin equivalents.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'vitaminb6' => array(
          'description' => t('The level of vitamin B6 in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'folate' => array(
          'description' => t('The level of folate in the ingredient, measured in micrograms.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'vitaminc' => array(
          'description' => t('The level of vitamin C in the ingredient, measured in milligrams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'water' => array(
          'description' => t('The level of water in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
        'transfattyacids' => array(
          'description' => t('The level of trans fatty acids in the ingredient, measured in grams.'),
          'type' => 'float',
          'not null' => FALSE,
        ),
      ),
      'primary key' => array('vid'),
      'indexes' => array(
        'nid' => array('nid'),
      ),
    ),
    'cookbook_units' => array(
      'description' => t("Contains available units for ingredients."),
      'fields' => array(
        'id' => array(
          'description' => t('The primary identifier for a ingredient unit'),
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'ingredient_nid' => array(
          'description' => t("The node version identifier of the ingredient."),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'ingredient_vid' => array(
          'description' => t("The node revision identifier of the ingredient."),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'name' => array(
          'description' => t("The name of the unit."),
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
        'grams' => array(
          'description' => t('How many grams 1 of this unit is.'),
          'type' => 'float',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('id'),
      'indexes' => array(
        'ingredient_nid' => array('ingredient_nid'),
        'ingredient_vid' => array('ingredient_vid'),
      ),
    ),
    'cookbook_recipe_relations' => array(
      'description' => t("Keeps track of similar recipies."),
      'fields' => array(
        'recipe_1_nid' => array(
          'description' => t('Node identifier for recipe 1.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'recipe_2_nid' => array(
          'description' => t('Node identifier for recipe 2.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('recipe_1_nid', 'recipe_2_nid'),
    ),
  );
}

/**
 * Implementation of hook_install
 */
function cookbook_install() {
  // Install required modules first
  module_enable(array('taxonomy', 'taxonomy_image'));

  // If default language differs from english then force translation
  if (language_default('language') != 'en') {
    if (module_exists("i18nstrings")) {
      $default_language = language_default();
      variable_set('i18nstrings_translate_langcode_' . $default_language->language, TRUE);
      drupal_set_message(t("i18nstrings has been set to translate from english to !language.", array('!language' => strtolower($default_language->name))));
    }
  }

  // Install the database schema
  drupal_install_schema('cookbook');

  // Create directories for taxonomy images
  $taxonomy_image_path = file_directory_path() . '/' . variable_get('taxonomy_image_path', 'category_pictures');
  $directories = array(
    file_directory_path(),
    $taxonomy_image_path,
    $taxonomy_image_path . '/cookbook',
    $taxonomy_image_path . '/cookbook/categories',
    $taxonomy_image_path . '/cookbook/difficulty',
    $taxonomy_image_path . '/cookbook/time-consumption'
  );
  cookbook_create_directories($directories);

  if (!is_writable($taxonomy_image_path . '/cookbook/categories')) {
    drupal_set_message('The cookbook module could not create folders in the files directory. Check your file permissions and then reinstall the module.', 'error');
    return false;
  }

  // Crate a vocabulary to use as the cookbook dictionary
  cookbook_create_vocabulary(
    array(
      'name' => 'Cookbook dictionary',
      'multiple' => 0,
      'required' => 0,
      'hierarchy' => 0,
      'relations' => 0,
      'module' => 'cookbook',
      'weight' => -10,
      'nodes' => array(),
      'localize' => 3,
    )
  );

  // Crate a vocabulary to use for commodities in the kitchen drawer
  cookbook_create_vocabulary(
    array(
      'name' => 'Cookbook commodities',
      'multiple' => 1,
      'required' => 0,
      'hierarchy' => 0,
      'relations' => 0,
      'module' => 'cookbook',
      'weight' => -10,
      'nodes' => array(),
      'localize' => 1,
    ),
    array(
      'Meat & poultry' => array(),
      'Seafood' => array(),
      'Vegetables, fruits & berries' => array(),
      'Potatoes, rice & pasta' => array(),
      'Egg & dairy products' => array(),
      'Grain products' => array(),
    )
  );

  // Crate a vocabulary to use when searching recipes
  cookbook_create_vocabulary(
    array(
      'name' => 'Cookbook search',
      'tags' => 1,
      'multiple' => 0,
      'required' => 0,
      'hierarchy' => 0,
      'relations' => 0,
      'module' => 'cookbook',
      'weight' => -10,
      'nodes' => array('recipe' => 1),
    )
  );

  // Crate a vocabulary to categorize recipes after
  cookbook_create_vocabulary(
    array(
      'name' => 'Cookbook categories',
      'multiple' => 1,
      'required' => 1,
      'hierarchy' => 1,
      'relations' => 0,
      'module' => 'cookbook',
      'weight' => -10,
      'nodes' => array('recipe' => 1),
      'localize' => 1,
    ),
    array(
      'Food for different needs' => array(
        'IMAGE' => 'categories/food-for-different-needs.png',
        'Allergies & food intolerance' => array(
          'Coeliac' => array(),
          'Lactose intolerance' => array(),
          'Milk protein allergy' => array(),
          'Other allergies' => array(),
        ),
        'Eating & appetite problems' => array(
          'Chewing & swallowing difficulties' => array(),
          'Energy & nutrient dense foods' => array(),
        ),
        'Vegetarian' => array(
          'Vegan' => array(),
          'Lacto-ovo-vegetarian' => array(
            'Lacto-vegetarian' => array(),
            'Ovo-vegetarian' => array(),
          ),
        ),
        'Lifestyle Diseases' => array(
          'Heart disease' => array(),
          'Obesity' => array(),
          'Diabetes' => array(),
          'Osteoporosis' => array(),
          'Other' => array(),
        ),
        'Diet & exercise' => array(
          'Athlets' => array(),
        ),
        'Agegroups' => array(
          'Infants' => array(),
          'Children' => array(),
          'Youth' => array(),
          'Adults' => array(),
          'Mature' => array(),
        ),
      ),
      'Commodities' => array(
        'IMAGE' => 'categories/commodities.png',
        'Meat & poultry' => array(
          'Meat' => array(
            'Beef' => array(),
            'Pork' => array(),
            'Mutton' => array(),
            'Minced' => array(),
          ),
          'Game' => array(
            'Reindeer' => array(),
            'Deer' => array(),
            'Moose' => array(),
            'Bird' => array(),
          ),
          'Poultry' => array(
            'Chicken / Fowls' => array(),
            'Duck' => array(),
            'Turky' => array(),
            'Goose' => array(),
          ),
        ),
        'Seafood' => array(
          'Fish' => array(),
          'Shellfish' => array(),
        ),
        'Vegetables, fruits & berries' => array(
          'Vegetables' => array(),
          'Fruits' => array(),
          'Berries' => array(),
        ),
        'Potatoes, rice & pasta' => array(
          'Potatoes' => array(),
          'Rice' => array(),
          'Pasta' => array(),
        ),
        'Egg & dairy products' => array(
          'Egg' => array(),
          'Dairy products' => array(),
        ),
        'Grain products' => array(),
      ),
      'Food & culture' => array(
        'IMAGE' => 'categories/food-and-culture.png',
        'Traditional' => array(
          'Sami' => array(),
          'Norwegian' => array(),
        ),
        'Asian' => array(
          'Chinese' => array(),
          'Indian' => array(),
          'Japanese' => array(),
          'Thai' => array(),
          'Malaysian' => array(),
          'Other countries' => array(),
        ),
        'African' => array(
          'North' => array(),
          'South' => array(),
          'Middle' => array(),
        ),
        'American' => array(
          'USA' => array(),
          'Mexico' => array(),
          'Argentina' => array(),
          'Brazil' => array(),
          'Canada' => array(),
          'Other countries' => array(),
        ),
        'Middle East' => array(
          'Turkish' => array(),
          'Lebanese' => array(),
          'Egyptian' => array(),
          'Israeli' => array(),
          'Other countries' => array(),
        ),
        'European' => array(
          'Western' => array(
            'English' => array(),
            'French' => array(),
            'Dutch' => array(),
            'Belgian' => array(),
            'Others' => array(),
          ),
          'Eastern' => array(
            'Baltic' => array(),
            'Polish' => array(),
            'Bulgarian' => array(),
            'Romanian' => array(),
            'Others' => array(),
          ),
          'Central' => array(
            'German' => array(),
            'Hungarian' => array(),
            'Austrian' => array(),
            'Swiss' => array(),
            'Others' => array(),
          ),
          'Southern' => array(
            'Italian' => array(),
            'Greek' => array(),
            'Spanish' => array(),
            'Portuguese' => array(),
            'Others' => array(),
          ),
          'Northern' => array(
            'Norwegian' => array(),
            'Swedish' => array(),
            'Danish' => array(),
            'Finnish' => array(),
            'Icelandic' => array(),
          ),
        ),
        'Oceania' => array(
          'Australia' => array(),
          'New Zealand' => array(),
          'Other countries' => array(),
        ),
        'Organic/Fair trade' => array(),
      ),
      'Hiking food' => array(
        'IMAGE' => 'categories/hiking-food.png',
        'Small hikes' => array(),
        'Overnight hikes' => array(),
        'Long hikes' => array(),
        'For children' => array(),
      ),
      'Sauces, soup & thickeners' => array(
        'IMAGE' => 'categories/sauce-soup-and-thickeners.png',
        'Sauces' => array(
          'Warm base sauces' => array(
            'Brown' => array(),
            'White' => array(),
            'Velouté' => array(),
            'Butter sauce' => array(),
            'Other' => array(),
          ),
          'Cold base sauces' => array(),
        ),
        'Soups' => array(
          'Clear soups' => array(),
          'Thickened soups' => array(
            'Cream soups' => array(),
            'Velouté soups' => array(),
            'Pure Soups' => array(),
            'Brown Soups' => array(),
          ),
          'National soups' => array(),
          'Other soups' => array(),
        ),
        'Thickeners' => array(
          'Cold thickeners' => array(),
          'Warm thickeners' => array(),
        ),
        'Stock' => array(
          'Meat stock' => array(),
          'Seafood stock' => array(),
          'Vegetable stock' => array(),
        ),
      ),
      'Drinks' => array(
        'IMAGE' => 'categories/drinks.png',
        'Coffee, tea & cocoa' => array(
          'Coffee' => array(),
          'Tea' => array(),
          'Cocoa' => array(),
        ),
        'Wine & liqueur' => array(
          'Red wine' => array(),
          'Rosé wine' => array(),
          'White wine' => array(),
          'Sparkling wine' => array(),
          'Liqueur' => array(),
          'Other' => array(),
        ),
        'Spirits' => array(
          'Cognac' => array(),
          'Whiskey' => array(),
          'Vodka' => array(),
          'Gin' => array(),
          'Other' => array(),
        ),
        'Other drinks' => array(
          'Smoothies' => array(),
        ),
      ),
      'Appetizer, main course, dessert & baking' => array(
        'IMAGE' => 'categories/appetizer-main-course-dessert-and-baking.png',
        'Appetizer' => array(
          'Cold' => array(),
          'Warm' => array(),
          'Soups' => array(),
        ),
        'Main course' => array(
          'Meat' => array(
            'Beef' => array(),
            'Pork' => array(),
            'Mutton' => array(),
            'Minced' => array(),
          ),
          'Game' => array(
            'Reindeer' => array(),
            'Deer' => array(),
            'Moose' => array(),
            'Bird' => array(),
            'Other' => array(),
          ),
          'Poultry' => array(
            'Chicken / Fowls' => array(),
            'Duck' => array(),
            'Turky' => array(),
            'Goose' => array(),
          ),
          'Seafood' => array(
            'Fish' => array(),
            'Shellfish' => array(),
          ),
        ),
        'Dessert' => array(
          'Cold' => array(),
          'Warm' => array(),
          'Ice' => array(),
          'Sauces' => array(),
        ),
        'Baking' => array(
          'Doughs' => array(
            'Sweet' => array(),
            'Coarse' => array(),
            'Fine' => array(),
          ),
          'Masses' => array(
            'Whipped' => array(),
            'Stirred' => array(),
            'Blanched' => array(),
          ),
          'Creams & glazes' => array(),
          'Cakes' => array(),
        ),
      ),
      'Accessories & snacks' => array(
        'IMAGE' => 'categories/accessories-and-snacks.png',
        'Preserving / canning' => array(
          'Jam' => array(),
        ),
        'Dressing / oil / vinegar' => array(),
        'Stews' => array(),
        'Salads' => array(),
        'Snacks' => array(
          'Sandwiches' => array(),
          'Cured meats' => array(),
          'Pies' => array(),
          'Eggs' => array(),
          'Porridge' => array(),
          'Cheese' => array(),
          'Other' => array(),
        ),
      ),
    )
  );

  // Crate a vocabulary for tagging recipes with a time consumption
  cookbook_create_vocabulary(
    array(
      'name' => 'Cookbook time consumption',
      'multiple' => 0,
      'required' => 1,
      'hierarchy' => 0,
      'relations' => 0,
      'module' => 'cookbook',
      'weight' => -10,
      'nodes' => array('recipe' => 1),
      'localize' => 1,
    ),
    array(
      'Under 15 min.' => array(
        'IMAGE' => 'time-consumption/15-min.png',
      ),
      'Under 30 min.' => array(
        'IMAGE' => 'time-consumption/30-min.png',
      ),
      'Under 45 min.' => array(
        'IMAGE' => 'time-consumption/45-min.png',
      ),
      'Under 60 min' => array(
        'IMAGE' => 'time-consumption/60-min.png',
      ),
      'Over 60 min' => array(
        'IMAGE' => 'time-consumption/60-min.png',
      ),
    )
  );

  // Crate a vocabulary for tagging recipes with a difficulty level
  cookbook_create_vocabulary(
    array(
      'name' => 'Cookbook difficulty',
      'multiple' => 0,

      'required' => 1,
      'hierarchy' => 0,
      'relations' => 0,
      'module' => 'cookbook',
      'weight' => -10,
      'nodes' => array('recipe' => 1),
      'localize' => 1,
    ),
    array(
      'Easy' => array(
        'IMAGE' => 'difficulty/easy.png',
      ),
      'Medium' => array(
        'IMAGE' => 'difficulty/medium.png',
      ),
      'Hard' => array(
        'IMAGE' => 'difficulty/hard.png',
      ),
    )
  );
}

/**
 * Function to create necessary directories.
 *
 * @param array $directories
 *  The directories to create.
 */
function cookbook_create_directories($directories) {
  foreach ($directories as $directory) {
    if (!is_dir($directory)) {
      mkdir($directory);
      chmod($directory, 0775);
    }
  }
  return TRUE;
}

/**
 * Function to create vocabularies.
 *
 * @param array $vocabulary
 *  The vocabulary to create.
 * @param array $terms
 *  Terms to insert into vocabulary.
 */
function cookbook_create_vocabulary($vocabulary, $terms = array()) {
  // If the vocabulary allready exists, use it. If not create a new one...
  $sql = "SELECT vid FROM {vocabulary} WHERE name = '%s' AND module = '%s'";
  if (!$vid = db_result(db_query($sql, $vocabulary['name'], $vocabulary['module']))) {
    taxonomy_save_vocabulary($vocabulary);
    $vid = $vocabulary['vid'];
    // If the internationalization module exists, localize the vocabulary by default
    if (module_exists('i18ntaxonomy')) {
      switch ($vocabulary['localize']) {
        case 1:
          i18ntaxonomy_vocabulary($vocabulary['vid'], I18N_TAXONOMY_LOCALIZE);
          break;
        case 3:
          i18ntaxonomy_vocabulary($vocabulary['vid'], I18N_TAXONOMY_TRANSLATE);
          break;
      }
    }

    // Add terms to vocabulary
    cookbook_insert_terms($vocabulary, $terms);
  }

  // Set the new vocabulary as the default vocabulary
  variable_set(str_replace(' ', '_', strtolower($vocabulary['name'])), $vid);
}

/**
 * Function for inserting terms into given vocabulary.
 *
 * @param int $vocabulary
 *  The identifier of the vocabulary to insert the terms into.
 * @param array $terms
 *  A list of terms to insert into the vocabulary.
 * @param int $parent
 *  The identifiter of the parent term.
 */
function cookbook_insert_terms($vocabulary, $terms = array(), $parent = 0, $weight = 0) {
  foreach ($terms as $name => $content) {
    if ($name == 'IMAGE') {
      // This isn't a term, it's the image that belongs to the parent term
      // Copy the image file from the cookbook module to the taxonomy image path
      copy(drupal_get_path('module', 'cookbook') . '/images/' . $content,
        file_directory_path() . '/' . variable_get('taxonomy_image_path', 'category_pictures') . '/cookbook/' . $content);

      // Add the image to the parent term
      taxonomy_image_add($parent, 'cookbook/' . $content);
    }
    else {
      // Increase weight
      $weight++;

      // Insert term
      $term = array(
        'vid' => $vocabulary['vid'],
        'name' => $name,
        'parent' => $parent,
        'weight' => $weight,
        'description' => is_array($content) ? '' : $content
      );
      if (module_exists('i18ntaxonomy') && $vocabulary['localize'] == I18N_TAXONOMY_LANGUAGE) {
        $term['language'] = 'en';
      }
      taxonomy_save_term($term);

      // Add any child terms
      if (is_array($content)) {
        cookbook_insert_terms($vocabulary, $content, $term['tid'], $weight);
      }
    }
  }
}

/**
 * Implementation of hook_uninstall
 */
function cookbook_uninstall() {
  drupal_uninstall_schema('cookbook');
  foreach (array('block', 'categories', 'commodities', 'dictionary', 'difficulty', 'image_node_recipe_size', 'image_node_thumb_size', 'import', 'import_language', 'logo', 'node_image_recipe_size', 'node_image_thumb_size', 'nutrition_data_reference', 'only_node_images', 'search', 'time_consumption', 'top_block', 'use_ingredient_nodes', 'disable_fractions') as $name) {
    variable_del('cookbook_' . $name);
  }
}

/**
 * Implementation of hook_update_N.
 * Add servings column to recipes.
 */
function cookbook_update_6100() {
  $results = array();
  db_add_field($results, 'cookbook_recipes', 'servings', array(
    'description' => t('Default number of servings for this recipe.'),
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE
  ));
  db_add_field($results, 'cookbook_recipes', 'partial_nid', array(
    'description' => t('Node identifier of a partial recipe to include.'),
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => FALSE
  ));
  db_add_field($results, 'cookbook_recipes', 'partial_pos', array(
    'description' => t('Position of partial recipe.'),
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => FALSE
  ));
  return $results;
}

/**
 * Implementation of hook_update_N.
 * Add recipe relations.
 */
function cookbook_update_6101() {
  $results = array();
  
  $schema = cookbook_schema();
  db_create_table($results, 'cookbook_recipe_relations', $schema['cookbook_recipe_relations']);
  
  return $results;
}