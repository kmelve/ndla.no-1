
/**
 * Watermark in textfield/area
 */
jQuery.fn.watermark = function(settings) {
  settings = jQuery.extend({
    color: '#aaa',
    text: 'Enter text.',
    focus: function() {},
    blur: function() {}
  }, settings);
  return this.each(function() {
    $(this).bind('blur', function() {
      if ($(this).val() == '') {
        $(this).val(settings.text).css('color', settings.color);
        $(this).focus(function() {
          $(this).unbind('focus').bind('focus', settings.focus).val('').css('color', '#000');
          settings.focus();
        });
      }
      settings.blur();
    }).trigger('blur');
  });
}

/**
 * Cookbook container
 */
var Cookbook = {
  /**
   * Initialize special features for the recipe node form.
   */
  init: function() {
    CookbookImages.init();

    if (Drupal.settings['cookbook']['ingredientNodes'] == 1) {
      // Init ingredient node system
      CookbookIngredients.init();
    }
    else {
      // Init simple ingredient system
      CookbookSI.init();
    }

    CookbookSteps.init();
    
    Cookbook.relations = new CookbookRelations();

    // Add submit handler
    $('#node-form').submit(Cookbook.submit);

    $(document).mouseup(function() {
      if (Cookbook.$moveObj != undefined) {
        $(document).unbind('mousemove', Cookbook.moveLi).unbind('mousemove', Cookbook.moveHeadline);
        $('body').css('cursor', 'default');
        Cookbook.$moveObj.css({
          'font-weight': 'normal',
          'cursor': 'default'
        });
        if ($.browser.msie) {
          $(document).unbind('selectstart');
        }
        Cookbook.$moveObj = undefined;
      }
    })
  },

  moveImage: function(e) {
    if (Cookbook.$moveObj) {
      $.each(CookbookImages.$container.children().not(Cookbook.$moveObj), function() {
        var offset = $(this).offset(document);
        if (e.pageY > offset.top && e.pageY < offset.top + 75) {
          if (e.pageX > offset.left && e.pageX < offset.left + 100) {
            if (offset.left >= Cookbook.$moveObj.offset(document).left) {
              Cookbook.$moveObj.insertAfter(this);
            }
            else {
              Cookbook.$moveObj.insertBefore(this);
            }
          }
        }
      });
    }
  },

  /**
   * Move list item up or down.
   */
  moveLi: function(e) {
    if (e.pageY < Cookbook.$moveObj.offset(document).top - 5) {
      // Move up
      if (Cookbook.$moveObj.prev()[0] != undefined) {
        Cookbook.$moveObj.insertBefore(Cookbook.$moveObj.prev());
      }
      else if (Cookbook.$moveObj.parent().prev()[0] != undefined) {
        Cookbook.$moveObj.appendTo(Cookbook.$moveObj.parent().prev().prev());
      }
    }
    else if (e.pageY > Cookbook.$moveObj.offset(document).top + Cookbook.$moveObj.height() + 5) {
      // Move down
      if (Cookbook.$moveObj.next()[0] != undefined) {
        Cookbook.$moveObj.insertAfter(Cookbook.$moveObj.next());
      }
      else if (Cookbook.$moveObj.parent().next()[0] != undefined) {
        Cookbook.$moveObj.prependTo(Cookbook.$moveObj.parent().next().next());
      }
    }
  },

  /**
   * Move headline up or down.
   */
  moveHeadline: function(e) {
    if (e.pageY < Cookbook.$moveObj.offset(document).top - 5) {
      // Move up
      if (Cookbook.$moveObj.prev().children().length != 0) {
        Cookbook.$moveObj.next().prepend(Cookbook.$moveObj.prev().children(':last'));
        if ($.browser.msie){
          Cookbook.$moveObj.next().addClass('IEH4CK').removeClass('IEH4CK');
        }
      }
      else if (Cookbook.$moveObj.prev().prev()[0] != undefined) {
        Cookbook.$moveObj.prev().prev().insertAfter(Cookbook.$moveObj);
        Cookbook.$moveObj.insertBefore(Cookbook.$moveObj.prev());
      }
    }
    else if (e.pageY > Cookbook.$moveObj.offset(document).top + Cookbook.$moveObj.height() + 5) {
      // Move down
      if (Cookbook.$moveObj.next().children().length != 0) {
        Cookbook.$moveObj.prev().append(Cookbook.$moveObj.next().children(':first'));
        if ($.browser.msie){
          Cookbook.$moveObj.next().addClass('IEH4CK').removeClass('IEH4CK');
        }
      }
      else if (Cookbook.$moveObj.next().next()[0] != undefined) {
        Cookbook.$moveObj.next().next().insertBefore(Cookbook.$moveObj);
        Cookbook.$moveObj.insertAfter(Cookbook.$moveObj.next());
      }
    }
  },

  /**
   * Handles form submit
   */
  submit: function() {
    CookbookImages.store();
    CookbookSteps.store();
    Cookbook.relations.store();

    if (Drupal.settings['cookbook']['ingredientNodes'] == 1) {
      CookbookIngredients.store();
    }
    else {
      CookbookSI.store();
    }

    // desc textarea
    $('#edit-body').val($('#desc').val());
  },

  /**
   * Creates a button.
   *
   * @param text
   *   Text to put on the button.
   * @param id
   *   Id to mark the button with.
   *
   * @return
   *   jQuery object containing the button.
   */
  createButton: function(text, id) {
    return $('<input type="button" class="form-button cb-button" ' + (id ? 'id="edit-cookbook-recipe-' + id + '" ' : '') + 'value="' + text + '"/>');
  },

  /**
   * Disable form submit when enter key is pressed.
   */
  disableSubmit: function(e) {
    if (e.keyCode == CookbookKeys.enter) {
      e.preventDefault();
    }
  },

  /**
   * Check a value to make sure that it isn't blank or contains any illegal chars.
   *
   * @param value
   *   The value to check.
   */
  validate: function(value) {
    if (value == '') {
      return false;
    }
    else if (value.match(/(@|;)/)) {
      alert(Drupal.t('You can not use @ or ; in the field.'));
      return false;
    }

    return true;
  }
}

/**
 * Cookbook images special features container.
 */
var CookbookImages = {
  /**
   * Initialize special features for the images field.
   */
  init: function() {
    CookbookImages.$field = $('#edit-cookbook-recipe-images').keypress(Cookbook.disableSubmit);
    CookbookImages.$container = $('<div></div>').insertBefore(CookbookImages.$field);
    CookbookImages.$default = $('<img src="' + Drupal.settings['basePath'] + Drupal.settings['cookbook']['path'] + '/theme/images/default-recipe.png" alt="' + Drupal.t('Image not found!') + '" width="100" height="75" class="cookbook-recipe-image-default"/>').insertBefore(CookbookImages.$container);

    // Load stored images
    if (CookbookImages.$field.val() != '') {
      var images = CookbookImages.$field.val().split('@');
      for (var i = 1; i < images.length; i++) {
        var image = images[i].split(';');
        CookbookImages.add(image[1], image[0]);
      }
    }

    // Tweak the image field
    CookbookImages.$field.css({
      'width': 'auto',
      'display': 'inline'
    }).val('').watermark({
      text: Drupal.t('Type in image url or node id and click "Add".')
    });

    // Add buttons
    CookbookImages.$add = Cookbook.createButton(Drupal.t('Add'), 'images-add').click(CookbookImages.validate).insertAfter(CookbookImages.$field);
    CookbookImages.$save = Cookbook.createButton(Drupal.t('Save'), 'images-save').click(CookbookImages.save).insertAfter(CookbookImages.$add).hide();
    CookbookImages.$cancel = Cookbook.createButton(Drupal.t('Cancel'), 'images-cancel').click(CookbookImages.reset).insertAfter(CookbookImages.$save).hide();
  },

  /**
   * Add image to container.
   */
  add: function(url, id) {
    if (id == undefined) {
      id = 0;
    }

    var attrs = {
      'id': id,
      'nodeId': undefined
    }

    // If URL is a number assume node id
    if (url == parseInt(url, 10)) {
      attrs['nodeId'] = url;
      url = Drupal.settings['imageUrl'] + '/view/' + url + '/' + Drupal.settings['cookbook']['imageSize'];
    }

    // Insert image with buttons
    var $image = $('<div class="cookbook-recipe-image"></div>').data('attrs', attrs).appendTo(CookbookImages.$container);
    $('<div class="cookbook-recipe-image-delete" title="' + Drupal.t('Delete') + '"></div>').appendTo($image).click(CookbookImages.remove);
    $('<div class="cookbook-recipe-image-move" title="' + Drupal.t('Move') + '"></div>').appendTo($image).mousedown(function() {
      Cookbook.$moveObj = $(this).parent().css('cursor', 'move');
      $(document).mousemove(Cookbook.moveImage);
      $('body').css('cursor', 'move');
      if ($.browser.msie) {
        $(document).bind('selectstart', function() {
          return false;
        });
      }
      return false;
    });
    $('<img class="cookbook-recipe-image-image" src="' + url + '" alt="' + Drupal.t('Image not found!') + '" title="' + Drupal.t('Edit') + '" width="100" height="75"/>').click(CookbookImages.edit).appendTo($image);

    // Hide default image if we have images
    if (CookbookImages.$container.children().length > 0) {
      CookbookImages.$default.hide();
    }
  },

  /**
   * Edit image in container.
   */
  edit: function() {
    // Make sure we're not editing two images at the same time.
    CookbookImages.$container.children('.cookbook-recipe-editing').removeClass('cookbook-recipe-editing');

    var $this = $(this).parent().addClass('cookbook-recipe-editing');
    var attrs = $this.data('attrs');

    if (attrs['nodeId'] != undefined) {
      CookbookImages.$field.focus().val(attrs['nodeId']);
    }
    else {
      CookbookImages.$field.focus().val($(this).attr('src'));
    }
    CookbookImages.$add.hide();
    CookbookImages.$save.show();
    CookbookImages.$cancel.show();
  },

  /**
   * Save image after editing.
   */
  save: function() {
    var image = CookbookImages.$field.focus().val();
    if (Cookbook.validate(image)) {
      var $editing = CookbookImages.$container.children('.cookbook-recipe-editing');
      var attrs = $editing.data('attrs');

      // If URL is a number assume node id
      if (image == parseInt(image, 10)) {
        attrs['nodeId'] = image;
        image = Drupal.settings['imageUrl'] + '/view/' + image;
      }
      else {
        attrs['nodeId'] = undefined;
      }

      $editing.children('img').attr('src', image);
      $editing.data('attrs', attrs);
      CookbookImages.reset();
    }
  },

  /**
   * Remove image from container.
   */
  remove: function() {
    $(this).parent().remove();

    // Show default image if we have no images.
    if (CookbookImages.$container.children().length == 0) {
      CookbookImages.$default.show();
    }
    CookbookImages.reset();
  },

  /**
   * Reset images field.
   */
  reset: function() {
    CookbookImages.$container.children('.cookbook-recipe-editing').removeClass('cookbook-recipe-editing');
    CookbookImages.$field.val('').trigger('blur');
    CookbookImages.$cancel.hide();
    CookbookImages.$save.hide();
    CookbookImages.$add.show();
  },

  /**
   * Validate images field.
   */
  validate: function(e) {
    var image = CookbookImages.$field.focus().val();
    if (Cookbook.validate(image)) {
      CookbookImages.add(image);
      CookbookImages.$field.val('');
    }
  },

  /**
   * Serialize images for storage.
   */
  store: function() {
    CookbookImages.$field.val('');
    CookbookImages.$container.children('div').each(function() {
      var attrs = $(this).data('attrs');
      if (attrs['nodeId'] != undefined) {
        attrs['url'] = attrs['nodeId'];
      }
      else {
        attrs['url'] = $(this).children('img').attr('src');
      }
      CookbookImages.$field.val(CookbookImages.$field.val() + '@' + attrs['id'] + ';' + attrs['url']);
    });
  }
}

/**
 * Container for key codes.
 */
var CookbookKeys = {
  enter: 13,
  up: 38,
  down: 40
}

/**
 * Container for the cookbook's recipe ingredient picker.
 */
var CookbookIngredients = {
  /**
   * Initialize the ingredient picker.
   */
  init: function() {
    // Insert elements and bind triggers.
    CookbookIngredients.$field = $('#edit-cookbook-recipe-ingredients').addClass('loading').attr("disabled", "disabled");
    CookbookIngredients.$ingredientsWrapper = $('<div id="cookbook-recipe-ingredients-wrapper"></div>').insertAfter(CookbookIngredients.$field);
    CookbookIngredients.$ingredients = $('<ul id="cookbook-recipe-ingredients"></ul>').appendTo(CookbookIngredients.$ingredientsWrapper);
    CookbookIngredients.$options = $('<ul id="cookbook-recipe-ingredients-options"></ul>').appendTo(CookbookIngredients.$ingredientsWrapper);
    $('<li><a href="javascript:void(0);">' + Drupal.t('Add ingredient without nutrition') + '</a></li>').click(CookbookIngredients.selectDummyIngredient).appendTo(CookbookIngredients.$options);
    $('<li><a href="javascript:void(0);">' + Drupal.t('Add headline') + '</a></li>').click(CookbookIngredients.selectHeadline).appendTo(CookbookIngredients.$options);
    CookbookIngredients.$container = $('<div></div>').insertBefore(CookbookIngredients.$field);
    $('<ul></ul>').appendTo(CookbookIngredients.$container);
    CookbookIngredients.$amount = $('<input type="text" size="2" title="' + Drupal.t('Amount') + '" class="cb-eml" disabled/>').insertAfter(CookbookIngredients.$field).keyup(CookbookIngredients.validate).keypress(Cookbook.disableSubmit);
    CookbookIngredients.$unit = $('<select title="' + Drupal.t('Unit of measurement') + '" class="cb-eml" disabled></select>').insertAfter(CookbookIngredients.$amount).keypress(Cookbook.disableSubmit);
    CookbookIngredients.$display = $('<input type="text" size="20" title="' + Drupal.t('Display name') + '" class="cb-eml" disabled/>').insertAfter(CookbookIngredients.$unit).keypress(Cookbook.disableSubmit);
    CookbookIngredients.$add = Cookbook.createButton(Drupal.t('Add'), 'ingredients-add').attr('disabled', true).insertAfter(CookbookIngredients.$display);
    CookbookIngredients.$save = Cookbook.createButton(Drupal.t('Save'), 'ingredients-save').attr('disabled', true).insertAfter(CookbookIngredients.$add).hide();
    CookbookIngredients.$cancel = Cookbook.createButton(Drupal.t('Cancel'), 'ingredients-cancel').insertAfter(CookbookIngredients.$save).click(CookbookIngredients.reset).hide();

    // Load available ingredients
    CookbookIngredients.load();

    // Load stored ingredients
    var stored = CookbookIngredients.$field.val().split('@');
    for (var i = 1; i < stored.length; i++) {
      var ingredient = stored[i].split(';');
      if (ingredient[5] < 0) {
        CookbookIngredients.insertHeadline(ingredient[6]);
      }
      else if (ingredient[1] == 0) {
        CookbookIngredients.insert(ingredient[0], null, null, ingredient[5], ingredient[6]);
      }
      else {
        CookbookIngredients.insert(ingredient[0], {
          'id': ingredient[1],
          'name': ingredient[2]
        }, {
          'id': ingredient[3],
          'name': ingredient[4]
        }, ingredient[5], ingredient[6]);
      }
    }

    // Tweak the ingredient field
    CookbookIngredients.$field.css({
      'display': 'inline-block',
      'width': '300px'
    }).val('').watermark({
      text: Drupal.t('Type in ingredient name.'),
      focus: function() {
        if (CookbookIngredients.preventBlur == undefined) {
          $(this).keyup();
        }
        else {
          CookbookIngredients.preventBlur = undefined;
        }
      },
      blur: function(e) {
        if (CookbookIngredients.preventBlur == undefined) {
          CookbookIngredients.$ingredientsWrapper.fadeOut('fast');
        }
      }
    }).keyup(CookbookIngredients.processKey).keypress(Cookbook.disableSubmit);
  },

  /**
   *
   */
  load: function() {
    $.getJSON(Drupal.settings.cookbook.url + '/xhr/ingredients', function(data) {
      $.each(data, function(key, value) {
        $('<li class="cookbook-recipe-ingredient">' + value['title'] + '</li>').data('ingredient', value).appendTo(CookbookIngredients.$ingredients).click(function() {
          CookbookIngredients.select(value['nid'], value['title'], value['unit_ids'], value['units']);
        });
      });
      CookbookIngredients.$field.removeClass('loading').attr("disabled", "");
      CookbookIngredients.$ingredients.children().hide();
    });
  },

  /**
   * Process key strokes for the ingredient field.
   */
  processKey: function(e) {
    if (e.keyCode != undefined || CookbookIngredients.$field.val() == '') {
      // Disable fields
      CookbookIngredients.$amount.attr('disabled', true);
      CookbookIngredients.$unit.attr('disabled', true);
      CookbookIngredients.$display.attr('disabled', true);
      CookbookIngredients.$add.attr('disabled', true);
      CookbookIngredients.$save.attr('disabled', true);
    }

    if (CookbookIngredients.$field.val() != '') {
      if (e.keyCode == CookbookKeys.enter) {
        // Select ingredient
        CookbookIngredients.$ingredients.children('.cookbook-recipe-selected').trigger('click');
        CookbookIngredients.$field.trigger('blur');
        CookbookIngredients.$amount.focus();
      }
      else if (e.keyCode == CookbookKeys.up) {
        // Highlight previous ingredient in result list
        var $current = CookbookIngredients.$ingredients.children('.cookbook-recipe-selected');
        var $prev = $current.prev();
        while ($prev[0] != undefined) {
          if ($prev.css('display') != 'none') {
            $current.removeClass('cookbook-recipe-selected');
            $prev.addClass('cookbook-recipe-selected');
            CookbookIngredients.$ingredients[0].scrollTo($prev[0].offsetTop-40);
            return;
          }
          $prev = $prev.prev();
        }
      }
      else if (e.keyCode == CookbookKeys.down) {
        // Highlight next ingredient in result list
        var $current = CookbookIngredients.$ingredients.children('.cookbook-recipe-selected');
        var $next = $current.next();
        while ($next[0] != undefined) {
          if ($next.css('display') != 'none') {
            $current.removeClass('cookbook-recipe-selected');
            $next.addClass('cookbook-recipe-selected');
            CookbookIngredients.$ingredients[0].scrollTo($next[0].offsetTop-40);
            return;
          }
          $next = $next.next();
        }
      }
      else {
        // Search for the ingredient
        if (CookbookIngredients.$field.val().length > 1) {
          CookbookIngredients.setTimer();
        }
      }
    }
  },

  setTimer: function() {
    clearTimeout(CookbookIngredients.timer);
    CookbookIngredients.timer = setTimeout(CookbookIngredients.search, 200);
  },

  /**
   * Search for the ingredient.
   */
  search: function() {
    var words = CookbookIngredients.$field.val().split(' ');
    var res = [];
    for (var i = 0; i < words.length; i++) {
      words[i] = words[i].toLowerCase();
      res.push(new RegExp(words[i], 'i'));
    }
    var $last;
    var $show = $([]);
    CookbookIngredients.$ingredients.children().hide().each(function(i, e) {
      var $e = $(e);
      var name = $e.html();
      for (var i = 0; i < res.length; i++) {
        if (name.search(res[i]) == -1) {
          return;
        }
      }
      
      if (name.substring(0, words[0].length).toLowerCase() == words[0]) {
        if ($last == undefined) {
          $e.prependTo(CookbookIngredients.$ingredients);
        }
        else {
          $e.insertAfter($last);
        }
        $last = $e;
      }
      $show = $show.add($e);
    });
    $show.show();

    CookbookIngredients.$ingredients.children('.cookbook-recipe-selected').removeClass('cookbook-recipe-selected');
    CookbookIngredients.$ingredients.children(':visible:first').addClass('cookbook-recipe-selected');
    CookbookIngredients.$ingredientsWrapper.fadeIn('fast');
    CookbookIngredients.$ingredients.jScrollPane({
      'dragMinHeight': 15
    });
    $('.jScrollPaneTrack').mousedown(function() {
      CookbookIngredients.preventBlur = true;
    }).mouseup(function() {
      CookbookIngredients.$field.focus();
    });
  },

  /**
   * Load units for the selected ingredient.
   */
  setUnits: function(unit_ids, units, selected) {
    CookbookIngredients.$unit.attr('disabled', false).children().remove();
    $.each(units, function(i, unit) {
      if (unit != '') {
        var select = '';
        if (unit_ids[i] == selected) {
          select = ' selected';
        }
        $('<option value="' + unit_ids[i] + '"' + select + '>' + unit + '</option>').data('attrs', {
          'id': unit_ids[i],
          'name': unit
        }).appendTo(CookbookIngredients.$unit);
      }
    });
  },

  /**
   * Select an ingredient.
   */
  select: function(id, name, unit_ids, units) {
    CookbookIngredients.$field.val(name);

    if (CookbookIngredients.$display.val() == '') {
      CookbookIngredients.$display.val(CookbookIngredients.$field.val().split(',')[0]);
    }

    CookbookIngredients.$display.attr('disabled', false);

    if (CookbookIngredients.$edit != undefined) {
      var unit = CookbookIngredients.$edit.data('attrs')['unit'];
      CookbookIngredients.setUnits(unit_ids, units, unit == undefined ? unit : unit['id']);
      CookbookIngredients.$amount.attr('disabled', false).keyup();

      CookbookIngredients.$add.hide().next().show().attr('disabled', false).unbind('click').click(function() {
        CookbookIngredients.update({
          'id': id,
          'name': name
        }, CookbookIngredients.$unit.children(':selected').data('attrs'), CookbookIngredients.$amount.val(), CookbookIngredients.$display.val().toLowerCase());
        $('li:last', CookbookIngredients.$options).click(CookbookIngredients.selectHeadline).removeClass('greyed-out');
      }).next().show();
    }
    else {
      // Adding a new ingredient
      CookbookIngredients.setUnits(unit_ids, units);
      CookbookIngredients.$amount.attr('disabled', false).keyup();

      CookbookIngredients.$add.unbind('click').click(function() {
        CookbookIngredients.insert(0, {
          'id': id,
          'name': name
        }, CookbookIngredients.$unit.children(':selected').data('attrs'), CookbookIngredients.$amount.val(), CookbookIngredients.$display.val().toLowerCase());
      });
    }
  },

  /**
   * Select a dummy ingredient (no nutrition)
   */
  selectDummyIngredient: function(e, noFocus) {
    if (CookbookIngredients.$display.val() == '') {
      CookbookIngredients.$display.val(CookbookIngredients.$field.val());
    }

    CookbookIngredients.$field.val(Drupal.t('Ingredient without nutrition'));
    CookbookIngredients.$amount.attr('disabled', false).keyup();
    if (noFocus == undefined) {
      CookbookIngredients.$amount.focus();
    }
    CookbookIngredients.$display.attr('disabled', false);
    CookbookIngredients.$unit.attr('disabled', true).children().remove();

    if (CookbookIngredients.$edit != undefined) {
      CookbookIngredients.$unit.attr('disabled', true).children().remove();
      CookbookIngredients.$add.hide().next().show().attr('disabled', false).unbind('click').click(function() {
        CookbookIngredients.update(null, null, CookbookIngredients.$amount.val(), CookbookIngredients.$display.val().toLowerCase());
        $('li:last', CookbookIngredients.$options).click(CookbookIngredients.selectHeadline).removeClass('greyed-out');
      }).next().show();
    }
    else {
      CookbookIngredients.$add.unbind('click').click(function() {
        CookbookIngredients.insert(0, null, null, CookbookIngredients.$amount.val(), CookbookIngredients.$display.val().toLowerCase());
      });
    }
  },

  /**
   * Select a headline
   */
  selectHeadline: function() {
    if (CookbookIngredients.$display.val() == '') {
      CookbookIngredients.$display.val(CookbookIngredients.$field.val());
    }

    CookbookIngredients.$field.val(Drupal.t('Headline'));
    CookbookIngredients.$display.attr('disabled', false).focus();
    CookbookIngredients.$amount.attr('disabled', true);
    CookbookIngredients.$unit.attr('disabled', true).children().remove();

    if (CookbookIngredients.$edit != undefined) {
      CookbookIngredients.$add.hide().next().show().attr('disabled', false).unbind('click').click(CookbookIngredients.updateHeadline).next().show();
    }
    else {
      CookbookIngredients.$add.attr('disabled', false).unbind('click').click(function() {
        CookbookIngredients.insertHeadline(CookbookIngredients.$display.val());
      });
    }
  },

  /**
   * Validate the ingredient amount.
   */
  validate: function() {
    var check = CookbookIngredients.$amount.val().replace(',', '.');
    if (check == parseFloat(check, 10) || (CookbookIngredients.$unit.attr('disabled') && check == '')) {
      CookbookIngredients.$add.attr('disabled', false);
      CookbookIngredients.$save.attr('disabled', false);
    }
    else {
      CookbookIngredients.$add.attr('disabled', true);
      CookbookIngredients.$save.attr('disabled', true);
    }
  },

  /**
   * Insert the new ingredient.
   */
  insert: function(id, ingredient, unit, amount, display) {
    // Validate display
    if (!Cookbook.validate(display)) {
      return false;
    }

    if (amount != '') {
      amount += ' ';
    }
    var liUnit;
    if (unit == null) {
      liUnit = '';
    }
    else {
      liUnit = unit['name'] + ' ';
    }

    // Create list element and append to ingredients
    var $li = $('<li><span>' + amount + liUnit + display + '</span></li>').data('attrs', {
      'id': id,
      'ingredient': ingredient,
      'unit': unit,
      'amount': amount,
      'display': display
    }).appendTo($('ul:last', CookbookIngredients.$container));

    // Add edit and delete buttons
    Cookbook.createButton(Drupal.t('Edit')).click(CookbookIngredients.edit).appendTo($li);
    Cookbook.createButton(Drupal.t('Delete')).click(CookbookIngredients.remove).appendTo($li);
    $('<div class="cookbook-recipe-move-up-down"></div>').appendTo($li).mousedown(function() {
      Cookbook.$moveObj = $(this).parent().css('font-weight', 'bold');
      $(document).mousemove(Cookbook.moveLi);
      $('body').css('cursor', 's-resize');
      if ($.browser.msie) {
        $(document).bind('selectstart', function() {
          return false;
        });
      }
      return false;
    })


    CookbookIngredients.reset();
  },

  insertHeadline: function(headline) {
    // Validate display
    if (!Cookbook.validate(headline)) {
      return false;
    }

    var $headline = $('<h4><span>' + headline + '</span></h4>').appendTo(CookbookIngredients.$container);
    Cookbook.createButton(Drupal.t('Edit')).click(CookbookIngredients.editHeadline).appendTo($headline);
    Cookbook.createButton(Drupal.t('Delete')).click(CookbookIngredients.removeHeadline).appendTo($headline);
    $('<div class="cookbook-recipe-move-up-down"></div>').appendTo($headline).mousedown(function() {
      Cookbook.$moveObj = $(this).parent().css('font-weight', 'bold');
      $(document).mousemove(Cookbook.moveHeadline);
      $('body').css('cursor', 's-resize');
      if ($.browser.msie) {
        $(document).bind('selectstart', function() {
          return false;
        });
      }
      return false;
    });
    $('<ul></ul>').appendTo(CookbookIngredients.$container);

    CookbookIngredients.reset();
  },

  /**
   * Edit an ingredient.
   */
  edit: function() {
    CookbookIngredients.$edit = $(this).parent();
    var attrs = CookbookIngredients.$edit.data('attrs');

    CookbookIngredients.$field.focus();
    if (attrs['ingredient'] != null) {
      // Find units
      var $next = CookbookIngredients.$ingredients.children(':first');
      var ingredient, unit_ids, units;
      while($next[0] != undefined) {
        ingredient = $next.data('ingredient');
        if (ingredient['nid'] == attrs['ingredient']['id']) {
          unit_ids = ingredient['unit_ids'];
          units = ingredient['units'];
          break;
        }
        $next = $next.next();
      }

      if (unit_ids == undefined) {
        return;
      }

      CookbookIngredients.select(attrs['ingredient']['id'], attrs['ingredient']['name'], unit_ids, units);
    }
    else {
      CookbookIngredients.selectDummyIngredient(null, true);
    }
    CookbookIngredients.$amount.val(attrs['amount']);
    CookbookIngredients.$display.val(attrs['display']);
    $('li:last', CookbookIngredients.$options).unbind('click').addClass('greyed-out');
  },

  editHeadline: function() {
    var $this = $(this).parent();
    CookbookIngredients.$edit = $this;
    CookbookIngredients.$field.attr('disabled', true);
    CookbookIngredients.selectHeadline();
    CookbookIngredients.$display.val($this.children(':first').html());
  },

  /**
   * Remove an ingredient.
   */
  remove: function() {
    $(this).parent().remove();
    CookbookIngredients.reset();
  },

  /**
   * Remove headline from list.
   */
  removeHeadline: function() {
    var $headline = $(this).parent();
    var $ul = $headline.next();

    // Take care of this headline's steps.
    $ul.children().appendTo($headline.prev());
    $ul.remove();

    // Remove headline
    $headline.remove();
    CookbookIngredients.reset();
  },

  /**
   * Reset the ingredient field.
   */
  reset: function() {
    $('li:last', CookbookIngredients.$options).unbind('click').click(CookbookIngredients.selectHeadline).removeClass('greyed-out');
    $('.cookbook-recipe-editing', CookbookIngredients.$container).removeClass('cookbook-recipe-editing');
    CookbookIngredients.$add.show().next().hide().next().hide();
    CookbookIngredients.$field.val('').attr('disabled', false).focus().next().val('').next().next().val('').prev().children().remove();
    CookbookIngredients.$edit = undefined;

    // Adjust autocomplete position
    CookbookIngredients.$ingredientsWrapper.css('top', CookbookIngredients.$field[0].offsetTop + CookbookIngredients.$field[0].offsetHeight);
  },

  /**
   * Update an ingredient.
   */
  update: function(ingredient, unit, amount, display) {
    if (!Cookbook.validate(display)) {
      return false;
    }

    if (amount != '') {
      amount += ' ';
    }
    if (unit == null) {
      var liUnit = '';
    }
    else {
      var liUnit = unit['name'] + ' ';
    }

    CookbookIngredients.$edit.data('attrs', {
      'id': CookbookIngredients.$edit.data('attrs')['id'],
      'ingredient': ingredient,
      'unit': unit,
      'amount': amount,
      'display': display
    }).children('span').html(amount + liUnit + display);

    CookbookIngredients.reset();
  },

  /**
   * Update a headline.
   */
  updateHeadline: function() {
    var headline = CookbookIngredients.$display.val();
    if (!Cookbook.validate(headline)) {
      return false;
    }
    CookbookIngredients.$edit.children(':first').html(headline);
    CookbookIngredients.reset();
  },

  /**
   * Serialize the ingredients for storage.
   */
  store: function() {
    CookbookIngredients.$field.val('');
    CookbookIngredients.$container.children().each(function() {
      var $this = $(this);
      if ($this.is('h4')) {
        CookbookIngredients.$field.val(CookbookIngredients.$field.val() + '@0;;;;;-1;' + $this.children('span:first').html());
      }
      else {
        $this.children().each(function() {
          var attrs = $(this).data('attrs');
          if (attrs['ingredient'] == null) {
            CookbookIngredients.$field.val(CookbookIngredients.$field.val() + '@' + attrs['id'] + ';;;;;' + attrs['amount'] + ';' + attrs['display']);
          }
          else {
            CookbookIngredients.$field.val(CookbookIngredients.$field.val() + '@' + attrs['id'] + ';' + attrs['ingredient']['id'] + ';' + attrs['ingredient']['name'] + ';' + attrs['unit']['id'] + ';' + attrs['unit']['name'] + ';' + attrs['amount'] + ';' + attrs['display']);
          }
        });
      }
    });
  }
}

var CookbookSI = {
  /**
   * Initialize simple ingredient system
   */
  init: function() {
    var $wrapper = $('#edit-cookbook-recipe-ingredients-wrapper');
    CookbookSI.$field = $('#edit-cookbook-recipe-ingredients').keypress(Cookbook.disableSubmit);
    CookbookSI.$container = $('<div><ul></ul></div>').insertBefore(CookbookSI.$field);

    // Load ingredients
    var ingredients = CookbookSI.$field.val().split('@');
    for (var i = 1; i < ingredients.length; i++) {
      ingredients[i] = ingredients[i].split(';');
      if (ingredients[i].length == 2) {
        CookbookSI.add(ingredients[i][1], ingredients[i][0] == '1');
      }
      else {
        CookbookSI.add(ingredients[i][0]);
      }
    }

    CookbookSI.$field.val('').watermark({
      text: Drupal.t('Type in ingredient and click "Add".')
    });

    CookbookSI.$add = Cookbook.createButton(Drupal.t('Add'), 'ingredients-add').click(CookbookSI.validate).appendTo($wrapper);
    CookbookSI.$addHeadline = Cookbook.createButton(Drupal.t('Add headline'), 'ingredients-add-headline').click(CookbookSI.validate).appendTo($wrapper);
    CookbookSI.$save = Cookbook.createButton(Drupal.t('Save'), 'ingredients-save').click(CookbookSI.save).appendTo($wrapper).hide();
    CookbookSI.$cancel = Cookbook.createButton(Drupal.t('Cancel'), 'ingredients-cancel').click(CookbookSI.reset).appendTo($wrapper).hide();
  },

  /**
   * Validate the ingredient before putting it in the list.
   */
  validate: function() {
    var ingredient = CookbookSI.$field.focus().val();
    if (Cookbook.validate(ingredient)) {
      CookbookSI.add(ingredient, $(this).attr('id') == 'edit-cookbook-recipe-ingredients-add-headline');
      CookbookSI.$field.val('');
    }
  },

  /**
   * Add ingredient to the list.
   */
  add: function(ingredient, headline) {
    var $e = headline ? $('<h4><span>' + ingredient + '</span></h4>').appendTo(CookbookSI.$container) : $('<li><span>' + ingredient + '</span></li>').appendTo(CookbookSI.$container.children(':last'));

    Cookbook.createButton(Drupal.t('Edit')).click(CookbookSI.edit).appendTo($e);
    Cookbook.createButton(Drupal.t('Delete')).click(CookbookSI.remove).appendTo($e);
    $('<div class="cookbook-recipe-move-up-down"></div>').appendTo($e).mousedown(function() {
      Cookbook.$moveObj = $(this).parent().css('font-weight', 'bold');
      (headline) ? $(document).mousemove(Cookbook.moveHeadline) : $(document).mousemove(Cookbook.moveLi);
      $('body').css('cursor', 's-resize');
      if ($.browser.msie) {
        $(document).bind('selectstart', function() {
          return false;
        });
      }
      return false;
    });
    if (headline) {
      $('<ul></ul>').insertAfter($e);
    }
  },

  /**
   * Remove ingredient from the list.
   */
  remove: function() {
    var $e = $(this).parent();
    if ($e.parent().is('h4')) {
      $e.next().children().appendTo($e.prev());
      $e.next().remove();
    }

    // Remove headline
    $e.remove();
    CookbookSI.reset();
  },

  /**
   * Edit ingredient/headline.
   */
  edit: function() {
    // Make sure we're not editing more than one element at the same time.
    $('.cookbook-recipe-editing', CookbookSI.$container).removeClass('cookbook-recipe-editing');

    var $this = $(this);
    $this.parent().addClass('cookbook-recipe-editing');
    CookbookSI.$field.focus().val(($this.prev().html()));
    CookbookSI.$add.hide();
    CookbookSI.$addHeadline.hide();
    CookbookSI.$save.show();
    CookbookSI.$cancel.show();
  },

  /**
   * Reset the ingredient field.
   */
  reset: function() {
    $('.cookbook-recipe-editing', CookbookSI.$container).removeClass('cookbook-recipe-editing');
    CookbookSI.$field.val('').trigger('blur');
    CookbookSI.$cancel.hide();
    CookbookSI.$save.hide();
    CookbookSI.$add.show();
    CookbookSI.$addHeadline.show();
  },

  /**
   * Put the changed ingredient back in the list.
   */
  save: function() {
    var ingredient = CookbookSI.$field.focus().val();
    if (Cookbook.validate(ingredient)) {
      $('.cookbook-recipe-editing span', CookbookSI.$container).html(ingredient);
      CookbookSI.reset();
    }
  },

  /**
   * Serialize the ingredients for storage.
   */
  store: function() {
    CookbookSI.$field.val('');
    CookbookSI.$container.children().each(function() {
      var $this = $(this);
      if ($this.is('h4')) {
        CookbookSI.$field.val(CookbookSI.$field.val() + '@1;' + $this.children('span:first').html());
      }
      else {
        $this.children().each(function() {
          CookbookSI.$field.val(CookbookSI.$field.val() + '@0;' + $(this).children('span:first').html());
        });
      }
    });
  }
}

/**
 * Container for special steps features.
 */
var CookbookSteps = {
  /**
   * Initialize special features for the steps field.
   */
  init: function() {
    var $wrapper = $('#edit-cookbook-recipe-steps-wrapper');
    CookbookSteps.$field = $('#edit-cookbook-recipe-steps');
    CookbookSteps.$container = $('<div><ol></ol></div>').insertBefore($wrapper.children(':first'));

    // Load steps
    var steps = CookbookSteps.$field.val().split('@');
    for (var i = 1; i < steps.length; i++) {
      steps[i] = steps[i].split(';');
      if (steps[i].length == 2) {
        CookbookSteps.add(steps[i][1], steps[i][0] == '1');
      }
      else {
        CookbookSteps.add(steps[i][0]);
      }
    }

    CookbookSteps.$field.val('').watermark({
      text: Drupal.t('Type in step and click "Add".')
    });

    // Add buttons
    CookbookSteps.$add = Cookbook.createButton(Drupal.t('Add as step'), 'steps-add').click(CookbookSteps.validate).appendTo($wrapper);
    CookbookSteps.$addHeadline = Cookbook.createButton(Drupal.t('Add as headline'), 'steps-add-headline').click(CookbookSteps.validate).appendTo($wrapper);
    CookbookSteps.$save = Cookbook.createButton(Drupal.t('Save'), 'steps-save').click(CookbookSteps.save).appendTo($wrapper).hide();
    CookbookSteps.$cancel = Cookbook.createButton(Drupal.t('Cancel'), 'steps-cancel').click(CookbookSteps.reset).appendTo($wrapper).hide();
  },

  /**
   * Add element to list.
   *
   * @param content
   *   Content of the element
   * @param headline
   *   Is it a headline?
   */
  add: function(content, headline) {
    // Convert line endings
    content = content.replace("\n", '<br>');
    var $e = headline ? $('<h4><span>' + content + '</span></h4>').appendTo(CookbookSteps.$container) : $('<li><span>' + content + '</span></li>').appendTo(CookbookSteps.$container.children(':last'));

    Cookbook.createButton(Drupal.t('Edit')).click(CookbookSteps.edit).appendTo($e);
    Cookbook.createButton(Drupal.t('Delete')).click(CookbookSteps.remove).appendTo($e);
    $('<div class="cookbook-recipe-move-up-down"></div>').appendTo($e).mousedown(function() {
      Cookbook.$moveObj = $(this).parent().css('font-weight', 'bold');
      $(document).mousemove(headline ? Cookbook.moveHeadline : Cookbook.moveLi);
      $('body').css('cursor', 's-resize');
      if ($.browser.msie) {
        $(document).bind('selectstart', function() {
          return false;
        });
      }
      return false;
    });
    if (headline) {
      $('<ol></ol>').insertAfter($e);
    }
  },

  /**
   * Edit step in list.
   */
  edit: function() {
    // Make sure we're not editing two steps at the same time.
    $('.cookbook-recipe-editing', CookbookSteps.$container).removeClass('cookbook-recipe-editing');
    var $this = $(this);
    $this.parent().addClass('cookbook-recipe-editing');
    CookbookSteps.$field.focus().val($this.prev().html().replace('<br>', '\n'));
    CookbookSteps.$add.hide();
    CookbookSteps.$addHeadline.hide();
    CookbookSteps.$save.show();
    CookbookSteps.$cancel.show();
  },

  /**
   * Remove step or headline from the list.
   */
  remove: function() {
    var $e = $(this).parent();
    if ($e.is('h4')) {
      // Move children and remove next list
      var $next = $e.next();
      $next.children().appendTo($e.prev());
      $next.remove();
    }

    // Remove element
    $e.remove();
    CookbookSteps.reset();
  },

  /**
   * Reset the step field.
   */
  reset: function() {
    $('.cookbook-recipe-editing', CookbookSteps.$container).removeClass('cookbook-recipe-editing');
    CookbookSteps.$field.val('').trigger('blur');
    CookbookSteps.$cancel.hide();
    CookbookSteps.$save.hide();
    CookbookSteps.$add.show();
    CookbookSteps.$addHeadline.show();
  },

  /**
   * Save the step to the previous list item.
   */
  save: function() {
    var step = CookbookSteps.$field.focus().val();
    if (Cookbook.validate(step)) {
      $('.cookbook-recipe-editing span', CookbookSteps.$container).html(step);
      CookbookSteps.reset();
    }
  },

  /**
   * Validate the content before adding it.
   */
  validate: function() {
    var content = CookbookSteps.$field.focus().val();
    if (Cookbook.validate(content)) {
      CookbookSteps.add(content, $(this).attr('id') == 'edit-cookbook-recipe-steps-add-headline');
      CookbookSteps.$field.val('');
    }
  },

  /**
   * Serialize the steps for storage.
   */
  store: function() {
    CookbookSteps.$field.val('');
    CookbookSteps.$container.children().each(function() {
      var $this = $(this);
      if ($this.is('h4')) {
        CookbookSteps.$field.val(CookbookSteps.$field.val() + '@1;' + $this.children('span:first').html());
      }
      else {
        $this.children().each(function() {
          CookbookSteps.$field.val(CookbookSteps.$field.val() + '@0;' + $(this).children('span:first').html());
        });
      }
    });
  }
}

function CookbookRelations() {
  // Init
  var $relatedInput = $('#edit-cookbook-recipe-relations-json');
  var $addInput = $('#edit-cookbook-recipe-relations-search');
  var $addButton = $('<button>' + Drupal.t('Add recipe') + '</button>').insertAfter('#edit-cookbook-recipe-relations-search-autocomplete');
  var $list = $('<ul></ul>');
  var related = JSON.parse($relatedInput.val());
  
  /**
   * Private
   */
  var addToList = function (name) {
    var $li = $('<li>' + name + '</li>');
    Cookbook.createButton(Drupal.t('Delete')).click(function () {
      $li.remove();
    }).appendTo($li);
    $li.appendTo($list);
  };
  
  /**
   * Public
   */
  this.store = function () {
    var related = [];
    $list.children().each(function () {
      var nodeParts = $(this).text().match(/^(\d+) \((.*)\)$/);
      if (nodeParts.length === 3) {
        related.push({
          nid: nodeParts[1],
          title: nodeParts[2]
        });
      }
    });
    $relatedInput.val(JSON.stringify(related));
  };
  
  // Add current relations to list
  for (var i = 0; i < related.length; i++) {
    addToList(related[i].nid + ' (' + related[i].title + ')');
  }
  $list.insertBefore($relatedInput);
  
  // Bind add button click
  $addButton.click(function () {
    addToList($addInput.val());
    $addInput.val('');
    return false;
  });
};

// Start initilizing special features when the document is ready
$(document).ready(Cookbook.init);