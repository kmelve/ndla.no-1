
/**
 * Changes the random recipe on the front page.
 */
var CookbookFront = {
  /**
   * Initialize the front page.
   */
  init: function() {
    var $recipes = $('#recipes');
    CookbookFront.$overlay = $('.overlay', $recipes);
    CookbookFront.$image = $('.image', $recipes);
    CookbookFront.$header = $('h3', CookbookFront.$overlay);
    CookbookFront.$link = $('a', CookbookFront.$overlay);
    CookbookFront.$image.children().error(CookbookFront.imageError);
    CookbookFront.$overlay.click(function() {
      window.location = CookbookFront.$link.attr('href');
    });

    CookbookFront.recipe = CookbookFront.$link.attr('href').match(/(\/)(\d+)(\?|$)/)[2];
    CookbookFront.fetch();
  },

  /**
   * Fetch random recipes.
   */
  fetch: function() {
    $.getJSON(Drupal.settings['cookbook']['url'] + '/xhr/random', function(data) {
      CookbookFront.recipes = data;
      if (data.length > 1) {
        setInterval(CookbookFront.changeRecipe, 10000);
      }
    });
  },

  /**
   * Change to random recipe.
   */
  changeRecipe: function() {
    // Pick random recipe
    CookbookFront.i = Math.floor(Math.random() * CookbookFront.recipes.length);

    // Make sure it's not the same as last time.
    if (CookbookFront.recipes[CookbookFront.i]['nid'] == CookbookFront.recipe) {
      CookbookFront.changeRecipe();
      return;
    }
    CookbookFront.recipe = CookbookFront.recipes[CookbookFront.i]['nid'];
    var $newImage = CookbookFront.$image.children().clone().error(CookbookFront.imageError).load(CookbookFront.imageLoaded).attr('src', CookbookFront.recipes[CookbookFront.i]['image']).appendTo(CookbookFront.$image).hide();
    if ($newImage.complete) {
      $newImage.trigger('load');
    }
  },
  
  /**
   * Image has loaded, fade in.
   */
  imageLoaded: function() {
    var $this = $(this);
    CookbookFront.$overlay.fadeOut(200, function() {
      $this.prev().fadeOut(200, function() {
        $(this).remove();
        CookbookFront.$header.html(CookbookFront.recipes[CookbookFront.i]['title']);
        CookbookFront.$link.attr('href', CookbookFront.$link.attr('href').replace(/(\/)(\d+)(\?|$)/, "$1" + CookbookFront.recipes[CookbookFront.i]['nid'] + "$3"));
        $this.fadeIn(600, function() {
          CookbookFront.$overlay.fadeIn(200);
        });
      });
    });
  },
  
  /**
   * If we have an error while loading an image, display default image.
   */
  imageError: function() {
    $(this).attr('src', Drupal.settings['cookbook']['path'] + '/theme/images/default-recipe-big.png');
  }
}

$(document).ready(CookbookFront.init);