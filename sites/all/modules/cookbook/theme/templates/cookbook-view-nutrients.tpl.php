<?php
?>
<h3><?php print t('Nutrients') ?></h3>
<dl>
  <?php foreach (cookbook_get_nutrients (FALSE) as $key => $values):
    if ($nutrients[$key] != -1): ?>
    <dt><?php print $values[0] ?></dt>
    <dd><?php print $nutrients[$key] . ' ' . $values[1] ?></dd>
    <?php endif;
  endforeach ?>
</dl>