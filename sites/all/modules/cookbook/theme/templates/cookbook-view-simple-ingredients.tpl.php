<?php
?>
<h3><?php print t('Ingredients for !servings', array('!servings' => format_plural($servings, '1 serving', '@count servings'))) ?></h3>
<?php $new_list = TRUE;
foreach (explode('@', $ingredients) as $ingredient):
  if ($ingredient != ''):
    $ingredient = explode(';', $ingredient);
    if (count($ingredient) == 2):
      if ($ingredient[0] == '1'):
        if (!$new_list): ?>
          </ul>
        <?php endif;
        $new_list = TRUE; ?>
        <h4><?php print check_plain($ingredient['1']) ?></h4>
      <?php elseif ($ingredient[0] == '2'): ?>
        </ul><ul>
      <?php else:
        if ($new_list):
          $new_list = FALSE ?>
          <ul>
        <?php endif ?>
        <li><?php print check_plain($ingredient['1']) ?></li>
      <?php endif;
    else:
      if ($new_list):
        $new_list = FALSE ?>
        <ul>
      <?php endif ?>
      <li><?php print check_plain($ingredient['0']) ?></li>
    <?php endif;
  endif;
endforeach;
if (!$new_list): ?>
  </ul>
<?php endif ?>