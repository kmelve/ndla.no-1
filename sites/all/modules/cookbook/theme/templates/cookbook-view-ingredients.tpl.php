<?php
?>
<h3><?php print t('Ingredients for !servings', array('!servings' => format_plural($servings, '1 serving', '@count servings'))) ?></h3>
<?php $new_list = TRUE;
foreach ($ingredients as $ingredient):
  if ($ingredient->amount < 0):
    if (!$new_list): ?>
      </ul>
    <?php endif;
    $new_list = TRUE; ?>
    <h4><?php print check_plain($ingredient->display) ?></h4>
  <?php else:
    if ($ingredient->display == '@'):
      if (!$new_list): ?>
        </ul><ul>
      <?php endif;
      continue;
    endif;
    if ($new_list):
      $new_list = FALSE ?>
      <ul>
    <?php endif ?>
    <li>
      <?php if ($ingredient->ingredient_nid != '0'):
        print round($ingredient->amount * $servings, 2) . ' ' . (is_null($ingredient->name) ? 'g' : check_plain($ingredient->name)). ' ';
      elseif ($ingredient->amount != '0'):
        print round($ingredient->amount * $servings, 2) . ' ';
      endif;
      print check_plain(strtolower($ingredient->display)) ?>
    </li>
  <?php endif;
endforeach;
if (!$new_list): ?>
  </ul>
<?php endif ?>