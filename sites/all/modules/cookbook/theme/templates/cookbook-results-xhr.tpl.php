<?php
if (isset($recipes)):
  foreach ($recipes as $recipe): ?>
    <div class="result-item shadow-bottom-right">
      <div class="result-image">
        <img src="<?php print check_url($recipe->image) ?>" width="116" height="87" alt=""/>
      </div>
      <div class="result-difficulty">
        <?php print taxonomy_image_display($recipe->diff_tid) ?><br/>
        <?php print check_plain($recipe->diff) ?>
      </div>
      <div class="result-time-consumption">
        <?php print taxonomy_image_display($recipe->time_tid) ?><br/>
        <?php print check_plain($recipe->time) ?>
      </div>
      <div class="result-text">
        <h3>
          <a href="<?php print url('cookbook/recipe/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $recipe->nid) ?>" class="reverse-fading"><?php print $recipe->title ?></a>
        </h3>
        <p><?php print check_plain($recipe->body) ?></p>
      </div>
    </div>
  <?php endforeach ?>
<?php endif ?>
<div id="hits" style="display: none"><?php print $hits ?></div>
<div id="page" style="display: none"><?php print $page ?></div>