<?php
?>
<h3><?php print t('Images') ?></h3>
<p>
  <?php foreach ($images as $image):
    if (is_numeric($image->source)):
      $image->source = file_create_url(db_result(db_query("SELECT f.filepath FROM {files} f JOIN {image} i ON i.nid = %d AND i.image_size = '%s' AND f.fid = i.fid", $image->source, variable_get('cookbook_image_node_thumb_size', 'thumbnail'))));
    endif ?>
    <img class="image-<?php print $image->id ?>" src="<?php print check_url($image->source) ?>" alt="<?php print t('Not available') ?>" width="100" height="75"/>
  <?php endforeach ?>
</p>