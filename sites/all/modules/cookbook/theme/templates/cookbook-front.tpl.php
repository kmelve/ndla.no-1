<?php
$path = base_path() . drupal_get_path('module', 'cookbook');
?>
<div class="block" id="recipes">
  <h2><?php print t('Recipes') ?></h2>
  <div class="content">
    <div class="overlay">
      <div>
        <h3><?php print check_plain($recipe->title) ?></h3>
        <p>
          <a href="<?php print url('cookbook/recipe/0/0/0/0/' . $recipe->nid) ?>" class="reverse-fading"><?php print t('To recipe') ?>
            <span>&nbsp;&gt;&gt;</span>
          </a>
        </p>
      </div>
    </div>
    <div class="image">
      <img src="<?php print check_url($recipe->image) ?>" alt="" width="576" height="432"/>
    </div>
  </div>
</div>
<div class="block" id="vertical-categories">
  <h2><?php print t('Categories') ?></h2>
  <div class="content">
    <table>
      <?php foreach ($categories as $category): ?>
        <tr>
          <td>
            <a href="<?php print url('cookbook/results/' . $category->tid .'/0/0/0') ?>" class="reverse-fading">
              <?php print taxonomy_image_display($category->tid) ?>
              <span><?php print check_plain($category->name) ?></span>
            </a>
          </td>
        </tr>
      <?php endforeach ?>
    </table>
  </div>
</div>