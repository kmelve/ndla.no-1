<?php
?>
<h3><?php print t('Steps') ?></h3>
<?php $new_list = TRUE;
foreach (explode('@', $steps) as $step):
  if ($step != ''):
    $step = explode(';', $step);
    if (count($step) == 2):
      if ($step[0] == '1'):
        if (!$new_list): ?>
          </ol>
        <?php endif;
        $new_list = TRUE; ?>
        <h4><?php print check_plain($step['1']) ?></h4>
      <?php elseif ($step[0] == '2'): ?>
        </ol><ol>
      <?php else:
        if ($new_list):
          $new_list = FALSE ?>
          <ol>
        <?php endif ?>
        <li><?php print check_plain($step['1']) ?></li>
      <?php endif;
    else:
      if ($new_list):
        $new_list = FALSE ?>
        <ol>
      <?php endif ?>
      <li><?php print check_plain($step['0']) ?></li>
    <?php endif;
  endif;
endforeach;
if (!$new_list): ?>
  </ol>
<?php endif ?>