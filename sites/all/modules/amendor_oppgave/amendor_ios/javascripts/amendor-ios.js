var AmendorIOS = AmendorIOS || {};

AmendorIOS.initialize = function() {
  AmendorIOS.$iframeWrapper = $('<div style="display:none"></div>').appendTo('.amendor-ios-wrapper');
  $('<iframe id="iosframe" name="iosframe" src="about:blank" scrolling="auto" width="690" height="1300" style="position:absolute;top:15px;left:15px;border:1px solid #999"></iframe>').appendTo(AmendorIOS.$iframeWrapper).load(function(){AmendorIOS.$loader.hide();});
  $('<a href="#" style="position:absolute;top:20px;right:37px">' + Drupal.t('Cancel') + '</a>').click(AmendorIOS.closePopup).appendTo(AmendorIOS.$iframeWrapper);
  AmendorIOS.$loader = $('<div style="position:absolute;top:20px;left:20px;background:url(' + Drupal.settings['basePath'] + 'misc/throbber.gif) no-repeat 0 -19px;padding-left:16px">' + Drupal.t('Loading') + '</div>').appendTo(AmendorIOS.$iframeWrapper);
}

AmendorIOS.showPopup = function() {
  AmendorIOS.$iframeWrapper.show();
  AmendorIOS.$loader.show();
}

AmendorIOS.closePopup = function(e) {
  if (e != undefined) {
    var id = $(this).parent().prev().attr('id');
    AmendorIOS.flashAbort(id);
  }
  $('#iosframe', AmendorIOS.$iframeWrapper).attr('src', 'about:blank')
  AmendorIOS.$iframeWrapper.hide();
  return false;
}

AmendorIOS.flashAbort = function(id) {
  if(navigator.appName.indexOf("Microsoft") != -1) {
    window[id].abort();
  }
  else {
    document[id].abort();
  }
 }

$(document).ready(AmendorIOS.initialize);