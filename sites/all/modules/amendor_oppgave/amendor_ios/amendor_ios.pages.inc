<?php

/**
 * @file amendor_ios.pages.inc php file
 *   Insert description here.
 */

/**
 * Admin settings form for the module.
 */
function amendor_ios_admin() {
  $form['amendor_ios_include_swfobject'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use included swfobject.js'),
    '#description' => t('This can be unchecked if another module already includes this script.'),
    '#default_value' => variable_get('amendor_ios_include_swfobject', '1')
  );
  $options = array('_original' => 'Original', 'thumbnail' => 'Thumbnail', 'preview' => 'Preview');
  $extra = variable_get('image_sizes', array());
  foreach ($extra as $key => $is) {
    if (!isset($options[$key])) {
      $options[$key] = $is['label'];
    }
  }
  $form['amendor_ios_image_size'] = array(
    '#type' => 'select',
    '#title' => t('Select preset image size'),
    '#description' => t('When inserting an image into a task, the selected image size will be used as a source.'),
    '#options' => $options,
    '#default_value' => variable_get('amendor_ios_image_size', '_original')
  );
  $form['amendor_ios_site_settings'] = array(
    '#type' => 'select',
    '#title' => t('Select predefined site settings'),
    '#default_value' => variable_get('amendor_ios_site_settings', 'ndla'),
    '#options' => array(
      'ndla' => 'NDLA',
      'udir' => 'UDIR',
      'fno' => 'Forelesning.no',
    ),
    '#description' => t('Site settings that is defined in IOS Flash'),
  );
  return system_settings_form($form);
}

/**
 * Shows our modified node forms.
 *
 * @param string $type Node type.
 * @param int $value Id for the waiting flash.
 * @return string HTML.
 */
function amendor_ios_show_form($type, $value = NULL) {
  global $user;
  
  $node = array(
    'uid' => $user->uid,
    'name' => $user->name,
    'type' => $type,
    'amendor_ios' => array(
      'value' => $value,
      'alter' => 1
    )
  );

  module_load_include("inc", "node", "node.pages");
  return drupal_get_form($type . '_node_form', $node);
}

/**
 * shows a page for saving tasks.
 * The method for saving tasks is the same as the method for saving images using
 * index.swf
 *
 * @param int $value
 * @return XHTML
 */
function amendor_ios_show_amendor_ios_task_form($value) {
  global $user;
  $node = array('uid' => $user->uid, 'name' => $user->name, 'type' => 'amendor_ios_task');
  $taskNid = "";
  //Condition: xmlContent has been recieved from index.swf
  if (isset($_POST['c'])) {
    try {
      $taskNid = amendor_ios_extract_nid_from_xml($_POST['c']);
    }
    catch (Exception $e) {
      return t("The task couldn't be saved because of security reasons. Error code @error", array('@error' => 4544));
    }
    //Condition: This is a new node
    if ($taskNid == "undefined") {
      $node = array('uid' => $user->uid, 'name' => $user->name, 'type' => 'amendor_ios_task', 'amendor_code' => 'skippern');
      //Condition: This is an existing node
    }
    else if (is_numeric($taskNid) && is_numeric($value)) {
      $_SESSION['amendorValue'] = $value;
      $_SESSION['taskNid'] = $taskNid;
      $node = node_load($taskNid);
      $node->amendor_code = "skippern";
    }
    else {
      return t("Process aborts because of security reasons. Error code @error", array('@error' => 968));
    }
    //Condition: form is beeing submitted.
  }
  else if ($_SESSION['amendorValue'] == $value) {
    $node = node_load($_SESSION['taskNid']);
    unset($_SESSION['amendorValue']);
    unset($_SESSION['taskNid']);
  }
  $temp = drupal_get_form('amendor_ios_amendor_ios_task_form', $node, $_POST['c'], $value);
  return $temp;
}

/**
 * shows a page for saving sets of tasks
 */
function amendor_ios_show_amendor_ios_form($value) {
  global $user;
  $node = array('uid' => $user->uid, 'name' => $user->name, 'type' => 'amendor_ios');
  $taskNid = "";
  //Condition: xmlContent recieved from index.swf
  if (isset($_POST['c'])) {
    try {
      $taskNid = amendor_ios_extract_nid_from_xml($_POST['c']);
    }
    catch (Exception $e) {
      return t("The task couln't be saved because of security reasons. Error code @error", array('@error' => 4534));
    }
    //Condition: new node
    if ($taskNid == "undefined") {
      $node = array('uid' => $user->uid, 'name' => $user->name, 'type' => 'amendor_ios', 'amendor_code' => 'skippern');
      //Condition: existing node
    }
    else if (is_numeric($taskNid) && is_numeric($value)) {
      $_SESSION['amendorValue'] = $value;
      $_SESSION['taskNid'] = $taskNid;
      $node = node_load($taskNid);
      $node->amendor_code = "skippern";
    }
    else {
      return t("Process aborts because of security reasons. Error code @error", array('@error' => 4535));
    }
    //Condition: form submitted
  }
  else if ($_SESSION['amendorValue'] == $value) {
    $node = node_load($_SESSION['taskNid']);
    unset($_SESSION['amendorValue']);
    unset($_SESSION['taskNid']);
  }
  $temp = drupal_get_form('amendor_ios_amendor_ios_form', $node, $_POST['c'], $value);
  return $temp;
}

/**
 * handles amendor_iosform
 */
function amendor_ios_amendor_ios_form_submit($form, &$form_state) {
  module_load_include("inc", "node", "node.pages");
  node_form_submit($form, $form_state);
  //Finds nid
  $_SESSION["waiting" . $form_state['values']['amendorVerdi']] = $form_state['nid'];
  $form_state['redirect'] = 'amendor-ios/form/complete/' . $temp[1] . '/ios';
  return 'amendor-ios/form/complete/' . $temp[1] . '/ios';
}

/**
 * The page that is displayed after a form was successfully submitted.
 */
function amendor_ios_show_form_complete($nid = NULL, $from = FALSE) {
  if ($from == 'audio') {
    $_SESSION['soundId'] = $nid;
  }
  drupal_add_js('$(document).ready(function(){window.parent.AmendorIOS.closePopup();});', 'inline');
  return t("This box will be closed automatically. If nothing happens within 3 seconds, you may press the cancel link to close it manually.");
}

/**
 * Download the questions as xml in a word document.
 */
function amendor_ios_download() {
  header('Content-Type: application/vnd.word; charset=UTF-8');
  header('Content-Disposition: attachment; filename="' . t('answer.doc') . '"');
  print urldecode($_SESSION['amendor_ios_save_output']);
  exit;
}

/**
 * This method handle all communication with flash.
 */
function amendor_ios_flash_api() {
  switch ($_POST['q']) {
    case "images":
      // We're searching for images. This method is called every time the user shows a new page with search results.
      if (user_access('create amendor_ios')) {
        $page = $_POST['page'];
        // If we're seaching for new keywords:
        if (!is_numeric($_POST['page'])) {
          $page = 0;
          $searchWords = spliti(" ", urldecode($_POST['c']), 4);
          $likes = "";

          for ($i = 0; $i < count($searchWords); $i++) {
            $likes .= " AND CONCAT_WS( ' ', n.title, u.name, t.name, t.description, t2.name, t2.description, t3.name, t3.description ) LIKE '%%%s%'";
          }

          if (count($searchWords) == 1) {
            // We're searching using only one letter.
            if (strlen($searchWords[0]) < 2) {
              // Keywrods are too short
              $likes = "";
            }
          }
          $result = db_query(
            "SELECT DISTINCT n.nid AS nodeid
              FROM {node} n
              LEFT JOIN {users} u ON n.uid = u.uid
              LEFT JOIN {term_node} tn ON n.nid = tn.nid
              LEFT JOIN {term_data} t ON tn.tid = t.tid
              LEFT JOIN {term_hierarchy} th ON t.tid = th.tid
              LEFT JOIN {term_data} t2 ON th.parent = t2.tid
              LEFT JOIN {term_hierarchy} th2 ON t2.tid = th2.tid
              LEFT JOIN {term_data} t3 ON th2.parent = t3.tid
              WHERE n.type = 'image'
              AND n.status =1" . $likes . "
        		  ORDER BY n.nid DESC ", $searchWords[0], $searchWords[1], $searchWords[2], $searchWords[3], $searchWords[4]);

          $imagesToLoad = array();
          while ($nid = db_fetch_object($result)) {
            $imagesToLoad[] = $nid->nodeid;
          }
          $_SESSION['amendorHitNumber'] = count($imagesToLoad);
          $_SESSION['amendorImageHits'] = $imagesToLoad;
        }
        $output = array();
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=images&links=";

        $first = TRUE;
        $start_path = base_path();
        for ($i = $page * 20; $i < min($page * 20 + 20, $_SESSION['amendorHitNumber']); $i++) {

          $temp = node_load(array('nid' => $_SESSION['amendorImageHits'][$i]));
          $imageType = variable_get('amendor_ios_image_size', '_original');
          $imgInfo = image_get_info(file_create_path($temp->images[$imageType]));
          $imgWidth = $imgInfo['width'];
          $imgHeight = $imgInfo['height'];
          //If the image is too big to be handled by Flash:
          if ($imgWidth > 2850 || $imgHeight > 2850) {
            $imageType = 'preview';
          }
          $output[] = $temp;
          if (file_exists($temp->images[$imageType])) {
            if ($first) {
              echo urlencode($start_path . $temp->images['thumbnail'] . "#" . ($start_path . $temp->images[$imageType]) . "#" . $_SESSION['amendorImageHits'][$i] . "#" . $temp->title);
              $first = FALSE;
            }
            else {
              echo urlencode("@" . $start_path . $temp->images['thumbnail'] . "#" . $start_path . $temp->images[$imageType] . "#" . $_SESSION['amendorImageHits'][$i] . "#" . $temp->title);
            }
          }
        }

        if ($first == true) {
          echo "";
        }
        echo "&hitNumber=" . $_SESSION['amendorHitNumber'];
      }
      break;

    case "sounds":
      // We're searching for sounds
      if (user_access('create amendor_ios')) {
        $searchWords = spliti(" ", urldecode($_POST['c']), 4);
        $likes = "";

        for ($i = 0; $i < count($searchWords); $i++) {
          $likes .= " AND CONCAT_WS( ' ', n.title, u.name, t.name, t.description, t2.name, t2.description, t3.name, t3.description ) LIKE '%%%s%'";
        }
        //Condition: there are only entered one letter to search for
        if (count($searchWords) == 1) {
          if (strlen($searchWords[0]) < 2) {
            $likes = "";
          }
        }
        $result = db_query(
          "SELECT DISTINCT n.nid AS nodeid
            FROM {node} n
	     		  LEFT JOIN {users} u ON n.uid = u.uid
	     		  LEFT JOIN {term_node} tn ON n.nid = tn.nid
	     		  LEFT JOIN {term_data} t ON tn.tid = t.tid
	     		  LEFT JOIN {term_hierarchy} th ON t.tid = th.tid
	     		  LEFT JOIN {term_data} t2 ON th.parent = t2.tid
	     		  LEFT JOIN {term_hierarchy} th2 ON t2.tid = th2.tid
	     		  LEFT JOIN {term_data} t3 ON th2.parent = t3.tid
	     		  WHERE n.type = 'audio'
	     		  AND n.status =1" . $likes . " 
	     		  ORDER BY n.nid DESC ", $searchWords[0], $searchWords[1], $searchWords[2], $searchWords[3], $searchWords[4]);

        $output = array();
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=sounds&xmlSounds=";
        $doc = new DOMDocument('1.0', 'utf-8');

        while ($nid = db_fetch_object($result)) {
          $temp = node_load(array('nid' => $nid->nodeid));
          $audioNode = $doc->createElement('ios_audio');
          $audioNode->setAttributeNode(new DOMAttr('nid', $temp->nid));
          $resultVid = db_query("SELECT vid, fid FROM {audio} WHERE nid=%d", $temp->nid . "ORDER BY vid=%d DESC");
          $resVidO = db_fetch_object($resultVid);
          $vid = urlencode($resVidO->vid);
          $fid = $resVidO->fid;
          //getting the audio's filepath
          $res = db_query("SELECT filepath, filename FROM {files} WHERE fid=%d", $fid);
          $filepath = "unknown";
          $filename = NULL;
          while ($item = db_fetch_object($res)) {
            $filepath = base_path() . $item->filepath;
            $filename = $item->filename;
          }

          $audioNode->setAttributeNode(new DOMAttr('soundUrl', $filepath));
          //getting the audio's title
          $resT = db_query("SELECT value FROM {audio_metadata} WHERE vid=%d AND tag='%s'", $vid, "title");
          $title = "unknown";
          while ($item = db_fetch_object($resT)) {
            $title = $item->value;
          }

          //getting the audio's artist
          $resT = db_query("SELECT value FROM {audio_metadata} WHERE vid=%d AND tag='%s'", $vid, "artist");
          $artist = "unknown artist";
          while ($item = db_fetch_object($resT)) {
            $artist = $item->value;
          }

          // if title and artist are unknown, but we have a filename
          if ($title == "unknown" && $artist == "unknown artist" && $filename != NULL) {
            // use filename as title
            $title = $filename;
          }
          $audioNode->setAttributeNode(new DOMAttr('soundTitle', urlencode(str_replace(array("\"", "%"), "", $title))));
          $audioNode->setAttributeNode(new DOMAttr('soundArtist', urlencode(str_replace(array("\"", "%"), "", $artist))));
          //Only add mp3 files to the result:
          if (stripos($filepath, ".mp3") === false) {
            
          }
          else
            $doc->appendChild($audioNode);
        }
        echo urlencode($doc->saveHTML());
      }
      break;

    case "startWaiting":
      // We're starting to wait for image or ios_task to ba saved in drupal.
      // Here we return the id of the node we're waiting for.
      if (user_access('create amendor_ios')) {
        $valueI = variable_get('amendor_ios_image', 0);
        variable_set('amendor_ios_image', $valueI + 1);
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=startWaiting&v=" . $valueI;
      }
      break;
    /*
     * Case: index.swf is starting to wait for sound to be saved in drupal.
     * This method gives index.swf an id for the item it is waiting for.
     */
    case "startWaitingSound":
      // We're starting to wait for a sound to ba saved in drupal.
      // Here we return the id of the node we're waiting for.
      if (user_access('create amendor_ios')) {
        $_SESSION['soundId'] = 'undefined';
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=startWaitingSound";
      }
      break;

    case "waiting":
      // We're waiting for an image or ios_task.
      if (user_access('create amendor_ios')) {
        if (is_numeric($_POST['valueI'])) {
          header('Content-type: application/x-www-urlform-encoded');
          if (isset($_SESSION["waiting" . $_POST['valueI']])) {
            // Node has been saved in drupal
            $urlI = urlencode($_SESSION["waiting" . $_POST['valueI']]);
          }
          else {
            $urlI = "unknown";
          }
          echo "&q=waiting&urlI=" . $urlI;
        }
      }
      break;

    case "waitingSound":
      // We're waiting for a sound.
      if (user_access('create amendor_ios')) {
        if ($_SESSION['soundId'] != 'undefined') {
          $resultVid = db_query("SELECT vid, fid FROM {audio} WHERE nid=%d", $_SESSION['soundId'] . "ORDER BY vid=%d DESC");
          $resVidO = db_fetch_object($resultVid);
          $vid = urlencode($resVidO->vid);
          $fid = $resVidO->fid;
          $res = db_query("SELECT filepath FROM {files} WHERE fid=%d", $fid);
          $rows = db_result(db_query("SELECT COUNT(filepath) FROM {files} WHERE fid=%d", $fid));
          header('Content-type: application/x-www-urlform-encoded');
          //Condition: Element has been saved in drupal
          if ($rows > 0) {
            $resO = db_fetch_object($res);
            $urlI = base_path() . urlencode($resO->filepath);
            //$urlI = $resO->filepath;
            //Condition: Element has not yet been saved in drupal.
          }
          else {
            $urlI = "unknown";
          }
          echo "&q=waitingSound&urlI=" . $urlI . "&soundId=" . $_SESSION['soundId'];
        }
      }
      break;

    case "tasks":
      // We're searching for tasks
      if (user_access('create amendor_ios')) {
        $searchWords = spliti(" ", urldecode($_POST['c']), 4);
        $likes = "";

        for ($i = 0; $i < count($searchWords); $i++) {
          $likes .= " AND CONCAT_WS( ' ', n.title, nr.body, ios.xmlContent, u.name, t.name, t.description, t2.name, t2.description, t3.name, t3.description ) LIKE '%%%s%' ";
        }

        if (count($searchWords) == 1) {
          // The keyword we're searching for is only one letter.
          if (strlen($searchWords[0]) < 2) {
            $likes = "";
          }
        }
        $result = db_query(
          "SELECT DISTINCT n.nid AS nodeid
            FROM {node} n
            LEFT JOIN {node_revisions} nr ON n.nid = nr.nid
            LEFT JOIN {amendor_ios_task} ios ON n.nid = ios.nid
            LEFT JOIN {users} u ON n.uid = u.uid
            LEFT JOIN {term_node} tn ON n.nid = tn.nid
            LEFT JOIN {term_data} t ON tn.tid = t.tid
            LEFT JOIN {term_hierarchy} th ON t.tid = th.tid
            LEFT JOIN {term_data} t2 ON th.parent = t2.tid
            LEFT JOIN {term_hierarchy} th2 ON t2.tid = th2.tid
            LEFT JOIN {term_data} t3 ON th2.parent = t3.tid
            WHERE n.type = 'amendor_ios_task'" . $likes . " 
            ORDER BY n.nid DESC ", $searchWords[0], $searchWords[1], $searchWords[2], $searchWords[3], $searchWords[4]);

        $output = array();
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=tasks&tasks=";

        $doc = new DOMDocument('1.0', 'utf-8');
        while ($nid = db_fetch_object($result)) {
          $temp = node_load(array('nid' => $nid->nodeid));
          $taskNode = $doc->createElement('ios_task');
          $taskNode->setAttributeNode(new DOMAttr('nid', $temp->nid));
          $taskNode->setAttributeNode(new DOMAttr('taskName', urlencode(str_replace(array("\n\r", "\r\n", "\n", "\"", "%", "&", "\r", "<", ">"), " ", strip_tags($temp->title)))));
          $taskNode->setAttributeNode(new DOMAttr('taskDescription', urlencode(str_replace(array("\n\r", "\r\n", "\n", "\"", "%", "&", "\r", "<", ">"), " ", strip_tags($temp->body)))));
          $doc->appendChild($taskNode);
        }
        echo $doc->saveHTML();
      }
      break;

    case "loadTask":
      // We're loading a task.
      if (is_numeric($_POST['nid'])) {
        $res = db_query("SELECT xmlContent FROM {amendor_ios_task} WHERE nid = %d", $_POST['nid']);
        $node = node_load($_POST['nid']);
        $rows = db_result(db_query("SELECT COUNT(nid) FROM {amendor_ios_task} WHERE nid = %d", $_POST['nid']));
        header('Content-type: application/x-www-urlform-encoded');

        if ($rows > 0 && ($node->status == 1 || user_access('create amendor_ios'))) {
          // We are supposed to show this node to the current user
          $resO = db_fetch_object($res);
          $xmlContent = $resO->xmlContent;
          $xmlContent = amendor_ios_fix_image_info($xmlContent);
          $xmlContent = $xmlContent->saveXML();
          $xmlTemp = spliti(">\n", $xmlContent, 2);
          $xmlContent = urlencode($xmlTemp[1]);
        }
        else {
          $xmlContent = "unknown";
        }
        echo "&q=loadTask&status=" . $node->status . "&xmlContent=" . $xmlContent;
      }
      break;

    case "loadImage":
      // We're loading an image.
      if (is_numeric($_POST['nid'])) {
        $start_path = base_path();
        $node = node_load($_POST['nid']);
        $filepath = $start_path . $node->images['thumbnail'];
        $checkpath = $node->images['thumbnail'];
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=loadImage&body=" . urlencode(strip_tags($node->body));
        echo "&title=" . urlencode(strip_tags($node->title));
        echo "&link=" . $start_path . "?q=node/" . $_POST['nid'];
        if (file_exists($checkpath)) {
          echo "&Uri=" . urlencode($filepath);
        }
        else {
          $filepath = $start_path . $node->images[variable_get('amendor_ios_image_size', '_original')];
          echo "&Uri=" . urlencode($filepath);
        }
      }
      break;

    case "saveTask":
      if (user_access('create amendor_ios')) {
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=saveTask";
        try {
          $taskNid = amendor_ios_extract_nid_from_xml($_POST['c']);
        }
        catch (Exception $e) {
          echo "&safe=0";
          return;
        }

        if ($taskNid == "undefined") {
          // This is a new node.
          echo "&safe=0";
        }
        else if (is_numeric($taskNid)) {
          db_query("UPDATE {amendor_ios_task} SET xmlContent = '%s' WHERE nid = %d", $_POST['c'], $taskNid);
          echo "&safe=1";
        }
        else {
          echo "&safe=0";
        }
      }
      break;

    case "saveWritings":
      if (user_access('access amendor_ios')) {
        $node = node_load($_POST['nid']);
        $solution = "<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">";
        $solution .= "<table border=\"0\" bgcolor=\"#339999\"><tr><td><b><font size=12 color=\"#ffffff\">NDLA</font></b></td></tr></table><br>";
        $solution .= "<h1 align=\"left\"><font color=\"#000000\">" . $node->title . "</font></h1><br><hr color=\"#339999\">";
        $solution .= $_POST['output'];

        $_SESSION['amendor_ios_save_output'] = $solution;
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=saveWritings";
      }
      break;
  }
  exit();
}

/**
 * Extracts nid from xml. Throws an exception if xml can't be parsed.
 *
 * @param $xml string The xml as string.
 * @return array Associative array of the extracted questions.
 */
function amendor_ios_extract_nid_from_xml($xml) {
  set_error_handler('amendor_ios_xml_error_handler'); // This is to handle PHP-warnings
  $doc = new DOMDocument();
  $doc->loadXml($xml);
  $xmlNid = $doc->documentElement->getAttribute("nid");
  restore_error_handler();
  return $xmlNid;
}

/**
 * Internal error handler used in extracting xml. (Purpose is to ignore warning messages.)
 */
function amendor_ios_xml_error_handler($errno, $errstr, $errfile, $errline) {
  if ($errno == E_WARNING && (substr_count($errstr, 'DOMDocument::loadXML()') > 0)) {
    throw new DOMException($errstr);
  }
  else {
    return false;
  }
}

/**
 * updates body and uri for images in a task.
 * also uses thumbnail og preview images instead of original where
 * those can be used, and checks to see if image-files exists or not.
 */
function amendor_ios_fix_image_info($xml) {
  //Go through entire XML and put in correct URI and body for each Image node
  $start_path = base_path();

  set_error_handler('amendor_ios_xml_error_handler');
  $doc = new DOMDocument();
  $doc->loadXml(urldecode($xml));
  $items = $doc->getElementsByTagName("Image");
  foreach ($items as $item) {
    $node = node_load($item->getAttribute("nid"));
    $imageHolders = $item->getElementsByTagName("imageHolder");
    $savedWidth = $imageHolders->item(0)->getAttribute("_width");
    $savedHeight = $imageHolders->item(0)->getAttribute("_height");
    $ratio = $savedWidth > 0 ? $savedHeight / $savedWidth : 1;
    $sizes = image_get_sizes(NULL, ($ratio));
    $imageType = variable_get('amendor_ios_image_size', '_original');
    $imgInfo = image_get_info(file_create_path($node->images[$imageType]));
    $imgWidth = $imgInfo['width'];
    $imgHeight = $imgInfo['height'];
    $tooBig = FALSE;
    $currentWidth = $imgWidth;
    if ($imgWidth > 2850 || $imgHeight > 2850) {
      $tooBig = TRUE;
      $imageType = 'preview';
    }
    if (!user_access('create amendor_ios') || $tooBig) {
      foreach ($sizes as $key => $value) {
        $w = $value['width'];
        $h = $value['height'];
        if ($w > 2850 || $h > 2850) {
          continue;
        }
        if ($w > $savedWidth && $w < $currentWidth) {
          $imageType = $key;
          $currentWidth = $w;
        }
      }
    }
    $filepath = $start_path . $node->images[$imageType];
    $checkpath = $node->images[$imageType];
    if (file_exists($checkpath)) {
      $item->setAttribute("body", urlencode(strip_tags($node->body)));
      $item->setAttribute("Uri", urlencode($filepath));
    }
    else {
      $imageType = variable_get('amendor_ios_image_size', '_original');
      $filepath = $start_path . $node->images[$imageType];
      $checkpath = $node->images[$imageType];
      if (file_exists($checkpath)) {
        $item->setAttribute("body", urlencode(strip_tags($node->body)));
        $item->setAttribute("Uri", urlencode($filepath));
      }
      else {
        $item->setAttribute("Uri", "null");
      }
    }
  }
  restore_error_handler();
  return $doc;
}