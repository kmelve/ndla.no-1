<?php
/** \addtogroup utdanning_guid */

/**
* @file
* @ingroup utdanning_guid 
* This module adds the field GUID to nodes making it easier to group nodes.
*
*    
* @author js
*
* @version 1.0
*
*@license
 *Copyright (C) Utdanning.no
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LISENCE.txt file or the
 *   GNU General Public License for more details.
*/

/**
* Implementation of hook_menu()
*/
function utdanning_guid_menu() {
  $items = array();
  
  $items['admin/settings/guid'] = array(
    'title' => t('Utdanning GUID content settings'),
    'description' => t('Which content types should have a GUID field.'),
    'page callback' => 'drupal_get_form',
    'page arguments' =>  array('utdanning_guid_content_form'),
    'access arguments' => array('administer utdanning guid')
  );
  
  return $items;
}
/**
* Implementation of hook_perm()
*/
function utdanning_guid_perm() {
  return array('administer utdanning guid');
}

/**
* The form for the settings page.
*
* @return string 
*/
function utdanning_guid_content_form($node) {
	$type_options = array();
	$node_types = node_get_types();
	foreach($node_types as $type) {
		$type_options[$type->type] = $type->name;
	}
	
	$form['utdanning_guid_types'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Add the GUID field to the following content types'),
		'#default_value' => variable_get('utdanning_guid_types', array()),
		'#options' => $type_options,
	);
		
	return system_settings_form($form);
}

/**
* Returns true/false depending on wether node of $type 
* should use utdanning guid.
*
* @param $type
*	The name of the type which is to be checked.
* @return 
*	boolean
*/
function utdanning_guid_is_valid_content_type($type) {
  $ctypes = variable_get('utdanning_guid_types', array());
  return (!empty($ctypes[$type]));
}

/**
 * Implementation of hook_form_alter().
 */
function utdanning_guid_form_alter(&$form, $form_state, $form_id) {
	if (isset($form['type']) && $form['type']['#value'] .'_node_form' == $form_id) {
		$node = $form['#node'];		
		$form = array_merge($form, utdanning_guid_form($node));
	}
}

/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function utdanning_guid_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'utdanning_guid') . '/views',
    ),
    'handlers' => array(
      'views_handler_filter_guid' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

/**
* Implementation of hook_form().
*
* @param $node
*	The node on which we will add the form.
* @return
*	array
*/
function utdanning_guid_form($node) {

	if(!utdanning_guid_is_valid_content_type($node->type)) {
		return array();
  }
  
	$auto = "";
	/* Try to get the GUID from the node which the user wants to translate */
	if(isset($_GET['translation']) && is_numeric($_GET['translation'])) {
		$t_node = node_load($_GET['translation']);
		if(isset($t_node->utdanning_guid) && !empty($t_node->utdanning_guid)) {
			$node->utdanning_guid = $t_node->utdanning_guid;
		}
	}

	if(!isset($node->utdanning_guid) || empty($node->utdanning_guid)) {
		if(arg(2) == 'clone') {
			drupal_set_message(t('The cloned node does not have a GUID set. Functionality based on GUID will be impossible.'));
		}
		$node->utdanning_guid = _utdanning_guid_create_guid();
		$auto = t('<i>This GUID is auto generated and not inherited from another node.</i><br />');
	}

	$form['utdanning_guid'] = array(
		'#type' => 'fieldset',
		'#title' => t('GUID'),
		'#attributes' => array('class'=>'attachments'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#tree' => TRUE,
	);
	
		
	$description = t('This field is used to keep a reference between cloned nodes. Leave blank or as is. ');
	$form['utdanning_guid'][] = array('#value' => $auto.$description, '#prefix' => '<p>', '#suffix' => '</p>');
	
	
	$form['utdanning_guid']['utdanning_guid'] = array(
		'#type' => 'textfield',
		'#title' => t('GUID'),
		'#default_value' => $node->utdanning_guid,
		'#required' => FALSE,
		'#tree' => FALSE,
	);
	
	return $form;
}


/**
 * Implementation of hook_nodeapi().
 */
function utdanning_guid_nodeapi(&$node, $op, $a3, $a4) {
	switch($op) {
		case 'validate':
			_utdanning_guid_node_validate($node);
			break;

		case 'load':
			_utdanning_guid_node_load($node);
			break;

		case 'insert':
			_utdanning_guid_insert($node);
			break;
			
		case 'update':
			_utdanning_guid_update($node);
			break;
			
		case 'delete':
			_utdanning_guid_delete($node);
			break;
		
		case 'view':
			;
			break;
	}
}

/**
* Lets show ourselves in Views 
*
* @return 
*	The form.
*/
function utdanning_guid_views_data() {
  $data = array();
	
  $data['utdanning_guid']['table']['group'] = t('Node GUID');
	
  /* Make a join to the node table possible */
  $data['utdanning_guid']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
    'type' => 'INNER',
  );
  
  $data['utdanning_guid']['guid'] = array(
    'title' => t('GUID'),
    'help' => t('The GUID.'), // The help that appears on the UI,

    // Information for displaying the guuid
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => FALSE,
    ),
  
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      'name field' => 'guid', // the field to display in the summary.
    ),
  
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_guid',
    ),
  
    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  return $data;
}


/****************************************/
/* Private functions and helpers follow */
/****************************************/

/**
* Creates a unique id.
*
* @return 
*	string $guid
*/
function _utdanning_guid_create_guid()
{
  $microTime = microtime();
  list($a_dec, $a_sec) = explode(" ", $microTime);

  $dec_hex = dechex($a_dec* 1000000);
  $sec_hex = dechex($a_sec);

  _utdanning_guid_ensure_length($dec_hex, 5);
  _utdanning_guid_ensure_length($sec_hex, 6);

  $guid = "";
  $guid .= $dec_hex;
  $guid .= _utdanning_guid_create_guid_section(3);
  $guid .= '-';
  $guid .= _utdanning_guid_create_guid_section(4);
  $guid .= '-';
  $guid .= _utdanning_guid_create_guid_section(4);
  $guid .= '-';
  $guid .= _utdanning_guid_create_guid_section(4);
  $guid .= '-';
  $guid .= $sec_hex;
  $guid .= _utdanning_guid_create_guid_section(6);

  return $guid;

}

/**
* @param $characters
* @return hexadecimal $return
*/
function _utdanning_guid_create_guid_section($characters)
{
  $return = "";
  for($i=0; $i<$characters; $i++)
  {
    $return .= dechex(mt_rand(0,15));
  }
  return $return;
}

/**
* Ensures the length of string.
*
* @param $string
* @param $length
*/
function _utdanning_guid_ensure_length(&$string, $length)
{
  $strlen = strlen($string);
  if($strlen < $length)
  {
    $string = str_pad($string,$length,"0");
  }
  else if($strlen > $length)
  {
    $string = substr($string, 0, $length);
  }
}

/**
*  Function called when a node is deleted.
*
* @param $node
*	The node which is about to be deleted.
*/
function _utdanning_guid_delete(&$node) {
	db_query("DELETE FROM {utdanning_guid} WHERE nid=%d", $node->nid);
}

/**
* Implementation of hook_node_load()
*
* @param $node
*	The node which is loaded.
*/
function _utdanning_guid_node_load(&$node) {
	$result = db_query("SELECT guid FROM {utdanning_guid} WHERE nid=%d",  $node->nid);
	$res = db_fetch_array($result);
	$node->utdanning_guid = $res['guid'];
}

/**
* Checks if the guid looks like a proper GUID. 
* @param $node
*	The node which is to be valided.
*/
function _utdanning_guid_node_validate($node) {
	$node->utdanning_guid = trim($node->utdanning_guid);
	if(!empty($node->utdanning_guid)) {
		if(!(preg_match("/^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{8,12}$/", trim($node->utdanning_guid)))) {
			form_set_error("utdanning_guid", t("Illegal GUID"));
		}
	}
}

/**
* Insert GUID info into the database (saves the node)
*
* @param $node
*	The node to save
*/
function _utdanning_guid_insert(&$node) {
	/* If GUID is blank dont insert the line */
	$node->utdanning_guid = trim($node->utdanning_guid);
	if(!empty($node->utdanning_guid)) {
		$result = db_query("INSERT INTO {utdanning_guid} (nid, guid) VALUES (%d, '%s')", $node->nid, trim($node->utdanning_guid));
	}
}

/**
* Updates the node.
*
* @param @node
*	The node which is to be updated.
*/
function _utdanning_guid_update(&$node) {
	/* If GUID is blank delete the line in the db if any */
	$node->utdanning_guid = trim($node->utdanning_guid);
	if(empty($node->utdanning_guid)) {
		return _utdanning_guid_delete($node);
	}
	$result = db_query("SELECT * FROM {utdanning_guid} WHERE nid = %d",  $node->nid);
	$guid = db_fetch_object($result);
				
	if (!$guid) {  
		return _utdanning_guid_insert($node);
	}
	
	$result = db_query("UPDATE {utdanning_guid} SET 
		guid='%s'
		WHERE nid=%d ", 
		trim($node->utdanning_guid),
		$node->nid);
}

?>
