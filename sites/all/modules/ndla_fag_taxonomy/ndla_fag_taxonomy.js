/**
 * @file
 * @ingroup ndla_fag_taxonomy
 */

function ndla_fag_taxonomy_is_node_group(gid) {
  for (var k = 0; k < ogtax_node_groups.length; k++) {
    if (ogtax_node_groups[k].gid == gid) {
      return true;
    }
  }
  return false;
}

function ndla_fag_taxonomy_is_node_term(tid) {
  for (var k = 0; k < ogtax_node_terms.length; k++) {
    if (ogtax_node_terms[k].tid == tid) {
      return true;
    }
  }
  return false;
}

function ndla_fag_taxonomy_is_user_group(gid) {
  for (var k = 0; k < ogtax_user_terms.length; k++) {
    if (ogtax_user_terms[k].gid == gid) {
      return true;
    }
  }
  return false;
}

function ndla_fag_taxonomy_is_user_term(tid) {
  for (var k = 0; k < ogtax_user_terms.length; k++) {
    if (ogtax_user_terms[k].tid == tid) {
      return true;
    }
  }
  return false;
}

function ndla_fag_taxonomy_edit_render() {
  if ($("#edit-ogtax-selected-groups").val() != undefined && $("#edit-ogtax-selected-terms").val() != undefined) {
    var selected_groups = $("#edit-ogtax-selected-groups").val().split(',');
    var selected_terms = $("#edit-ogtax-selected-terms").val().split(',');
    var out = "";
    out += "<table>";
    
    out += "<tr>";
    out += "<th>";
    out += ogtax_texts.header1;
    out += "</th>";
    if (ogtax_node.og_is_group_post_type) {
      out += "<th>";
      out += ogtax_texts.header2;
      out += "</th>";
    }
    out += "<th>";
    out += ogtax_texts.header3;
    out += "</th>";
    out += "</tr>";
    
    for (var k = 0; k < ogtax_all_terms.length; k++) {
      var name = ogtax_all_terms[k].name;
      var gid = ogtax_all_terms[k].gid;
      var tid = ogtax_all_terms[k].tid;
      if (ndla_fag_taxonomy_is_user_group(gid) || ndla_fag_taxonomy_is_node_group(gid) || ndla_fag_taxonomy_is_node_term(tid)) {
        out += "<tr>";
        out += "<td>";
        out += name;
        out += "</td>";
        if (ogtax_node.og_is_group_post_type) {
          out += "<td>";
          out += '<input type=checkbox name="ogtax@' + gid + '" class="ogtax-check1"';
          if ($.inArray(gid, selected_groups) != -1) {
            out += ' checked=true';
          }
          if (!ndla_fag_taxonomy_is_user_group(gid)) {
            out += ' disabled=true';
          }
          out += '/>';
          out += "</td>";
        }
        out += "<td>";
        out += '<input type=checkbox name="ogtax@' + gid + '@' + tid + '" class="ogtax-check2"';
        if ($.inArray(tid, selected_terms) != -1) {
          out += ' checked=true';
        }
        if (!ndla_fag_taxonomy_is_user_term(tid)) {
          out += ' disabled=true';
        }
        out += '/>';
        out += "</td>";
        out += "</tr>";
      }
    }
    out += "</table>";
    $("#ogtax_edit_div").html(out);

    $(".ogtax-check1").click(function () {
      // toggle term if group is checked
      var name = $(this).attr("name");
      var checked = $(this).attr("checked");
      if (checked) {
        $("input[name*=" + name + "@]").attr("checked", true);
        $("input[name*=" + name + "@]").attr("disabled", true);
      } else {
        $("input[name*=" + name + "@]").attr("checked", false);
        $("input[name*=" + name + "@]").attr("disabled", false);
      }
      // make sure at least one group is checked
      var count = 0;
      $(".ogtax-check1").each(function(index) {
        if ($(this).attr("checked")) {
          count++;
        }
      });
      if (count == 0) {
        $(this).attr("checked", true);
      }
      ndla_fag_taxonomy_edit_assert();
      ndla_fag_taxonomy_edit_update();
    });

    $(".ogtax-check2").click(function () {
      ndla_fag_taxonomy_edit_assert();
      ndla_fag_taxonomy_edit_update();
    });

    ndla_fag_taxonomy_edit_assert();
  }
}

function ndla_fag_taxonomy_edit_update() {
  // stuff selected groups into form textfield
  var selected_groups = '';
  $(".ogtax-check1").each(function(index) {
    var name = $(this).attr("name");
    var checked = $(this).attr("checked");
    if (checked) {
      var gid = name.split('@');
      selected_groups += gid[1] + ',';
    }
  });
  $("#edit-ogtax-selected-groups").val(selected_groups);
  // stuff selected terms into form textfield
  var selected_terms = '';
  $(".ogtax-check2").each(function(index) {
    var name = $(this).attr("name");
    var checked = $(this).attr("checked");
    if (checked) {
      var tid = name.split('@');
      selected_terms += tid[2] + ',';
    }
  });
  $("#edit-ogtax-selected-terms").val(selected_terms);
}

function ndla_fag_taxonomy_edit_assert() {
  $(".ogtax-check1").each(function(index) {
    var name = $(this).attr("name");
    var checked = $(this).attr("checked");
    if (checked) {
      $("input[name*=" + name + "@]").attr("checked", true);
      $("input[name*=" + name + "@]").attr("disabled", true);
    } else {
      $("input[name*=" + name + "@]").attr("disabled", false);
    }
  });
}

function ndla_fag_taxonomy_block_render() {
  if (typeof(ogtax_texts) == 'undefined') {
    return;
  }

  var out = "";
  out += "<table>";
  
  out += "<tr>";
  out += "<th>";
  out += ogtax_texts.header1;
  out += "</th>";
  out += "<th>";
  out += ogtax_texts.header3;
  out += "</th>";
  out += "</tr>";
  
  for (var k = 0; k < ogtax_all_terms.length; k++) {
    var name = ogtax_all_terms[k].name;
    var gid = ogtax_all_terms[k].gid;
    var tid = ogtax_all_terms[k].tid;
    if (ndla_fag_taxonomy_is_user_group(gid) || ndla_fag_taxonomy_is_node_group(gid) || ndla_fag_taxonomy_is_node_term(tid)) {
      out += "<tr>";
      out += "<td>";
      out += name;
      out += "</td>";
      out += "<td>";
      out += '<input type=checkbox name="ogtax@' + tid + '" class="ogtax-check3"';
      if (ndla_fag_taxonomy_is_node_term(tid)) {
        out += ' checked=true';
      }
      if (!ndla_fag_taxonomy_is_user_term(tid)) {
        out += ' disabled=true';
      }
      out += '/>';
      out += "</td>";
      out += "</tr>";
    }
  }
  out += "</table>";

  out += "<tr>";
  out += "<td>";
  out += '<input type="button" onclick="ndla_fag_taxonomy_block_update();" value="Lagre" />';
  out += '&nbsp;';
  out += '<div id="ogtax_block_status" style="float:right;" />';
  out += "</td>";
  out += "</tr>";

  $("#ogtax_block_div").html(out);
}

function ndla_fag_taxonomyRenderCourseBlock() {
  var out = "";
  out += "<ul>";
  var empty = true;
  for (var k = 0; k < ogtax_all_terms.length; k++) {
    var name = ogtax_all_terms[k].name;
    var gid = ogtax_all_terms[k].gid;
    var tid = ogtax_all_terms[k].tid;
    if (ndla_fag_taxonomy_is_node_term(tid) && (ndla_fag_taxonomy_is_user_group(gid) || ndla_fag_taxonomy_is_node_group(gid) || ndla_fag_taxonomy_is_node_term(tid))) {
      out += '<li><a href="' + Drupal.settings.ndla_utils.base_url + '/' + Drupal.settings.ndla_utils.language + '/node/' + gid + '">' + name + '</a></li>';
      empty = false;
    }
  }
  out += "</ul>";
  $("#coure-affiliation").html(out);
  if (empty) {
    $("#block-ndla_fag_taxonomy_1").remove();
  }
}

function ndla_fag_taxonomy_block_update() {
  var terms = "";
  $(".ogtax-check3").each(function(index) {
    if ($(this).attr("checked")) {
      var tid = $(this).attr("name").split('@');
      terms += tid[1] + ","
    }
  });
  $("#ogtax_block_status").html("<img src=\"" + Drupal.settings.basePath + "sites/all/themes/ndla/img/throbber.gif\" width=16 height=16 />");
  var url = Drupal.settings.basePath + "ndla_fag_taxonomy/update/" + ogtax_node.nid + "/" + ogtax_node.vid;
  $.get(url, { terms: terms }, function(data) {
    $("#ogtax_block_status").html("");
  });
}
