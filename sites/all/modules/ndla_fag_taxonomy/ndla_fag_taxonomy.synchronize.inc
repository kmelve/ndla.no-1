<?php

/**
 * @file
 * @ingroup ndla_fag_taxonomy
 */

/**
 */
function ndla_fag_taxonomy_synchronize_form() {
  
  $form = array();

  drupal_add_js('misc/progress.js');

  $htm = '';
  $htm .= '<script type="text/javascript">';
  $htm .= '
function start_process(mode) {
  $("#progress").empty();
  pb = new Drupal.progressBar(\'myProgressBar\');
  pb.setProgress("0", "processing...");
  $("#progress").append(pb.element);
  uri = \'' . base_path() . 'admin/settings/ndla/ndla_fag_taxonomy/\' + mode;
  $.get(uri , \'\', function(data) {
    pb.setProgress("100","");
    pb.stopMonitoring();
  });
  pb.startMonitoring(\'' . base_path() . 'admin/settings/ndla/ndla_fag_taxonomy/synchronize/status\', 500);
  return false;
}
';
  $htm .= '</script>';
  $htm .= '<br/><br/>Synchronize OG Groups and Taxonomy when OG Groups are modified or when term hierarchy is altered:<br/>';
  $htm .= '<input type="button" value="Synchronize OG Groups and Taxonomy" onclick="start_process(\'synchronize/groups\')" />';
  $htm .= '<br/><br/>Synchronize OG Ancestry and Taxonomy when term hierarchy is altered<br/>';
  $htm .= '<input type="button" value="Synchronize OG Ancestry and Taxonomy" onclick="start_process(\'synchronize/nodes\')" />';
  $htm .= '<br/><br/>';
  $htm .= '<div id="progress"></div>';
  $htm .= '<br/>';
  
  $form['ndla_fag_taxonomy_synchronize'] = array('#value' => $htm );

  $form['#redirect'] = 'admin/settings/ndla/ndla_fag_taxonomy';
  return $form;
}

/**
 * Implementation of hook_form_submit().
 */
function ndla_fag_taxonomy_synchronize_form_submit($form_id, $form) {
  error_log('ndla_search_settings_form_submit: ' . print_r($form, TRUE));
}

/**
 * Ajax callback function for updating process status
 */
function ndla_fag_taxonomy_synchronize_status() {
  $res = explode(";", variable_get('ndla_fag_taxonomy_progress', '0;'));
  print drupal_to_js(array('status' => TRUE, 'percentage' => number_format($res[0], 2), 'message' => $res[1]));
  exit();
}

/**
 * Migrate og groups to taxonomy
 */
function ndla_fag_taxonomy_synchronize_groups() {
  $vocabulary = ndla_fag_taxonomy_get_default_vocabulary();
  $cc = db_result(db_query("SELECT count(nid) FROM {og}"));
  $progress = 0.0;
  $progress_step = 100.0 / $cc;
  variable_set('ndla_fag_taxonomy_progress', $progress . ';starting synchronizing og groups');

  $dbh = db_query("SELECT DISTINCT n.nid, n.vid, n.title FROM {og} o INNER JOIN {node} n ON o.nid = n.nid ORDER BY n.title");
  while ($group = db_fetch_object($dbh)) {
    $tid = ndla_fag_taxonomy_get_term_from_og($group->nid);
    db_query("DELETE FROM {term_node} WHERE nid = %d AND vid = %d AND tid IN (SELECT tid FROM {term_data} WHERE vid = %d)", $group->nid, $group->vid, $vocabulary->vid);
    $terms = array();
    foreach (taxonomy_get_parents_all($tid) as $term) {
      $terms[$term->tid] = $term->tid;
    }
    foreach ($terms as $ptid) {
      db_query("INSERT INTO {term_node} (nid, vid, tid) values (%d, %d, %d)", $group->nid, $group->vid, $ptid);
      error_log(sprintf("INSERT INTO {term_node} (nid, vid, tid) values (%d, %d, %d)", $group->nid, $group->vid, $ptid));
    }
    
    variable_set('ndla_fag_taxonomy_progress', $progress . ';synchronizing og groups...');
    $progress += $progress_step;
  }
  variable_set('ndla_fag_taxonomy_progress', $progress . ';done synchronizing og groups');
  exit();
}

/**
 * Migrate og node ancestry to taxonomy
 */
function ndla_fag_taxonomy_synchronize_nodes() {
  $cc = db_result(db_query("SELECT count(DISTINCT nid) FROM {og_ancestry}"));
  $progress = 0.0;
  $progress_step = 100.0 / $cc;
  variable_set('ndla_fag_taxonomy_progress', $progress . ';starting synchronizing og nodes');
  $nid_dbh = db_query("SELECT DISTINCT nid FROM {og_ancestry}");
  while ($nid = db_result($nid_dbh)) {
    set_time_limit(0);
    ndla_fag_taxonomy_synchronize_node($nid);
    variable_set('ndla_fag_taxonomy_progress', $progress . ';synchronizing og nodes...');
    $progress += $progress_step;
  }
  variable_set('ndla_fag_taxonomy_progress', $progress . ';done synchronizing og nodes');
  exit();
}

/**
 * Migrate og node to taxonomy
 */
function ndla_fag_taxonomy_synchronize_node($nid) {
  $vocabulary = ndla_fag_taxonomy_get_default_vocabulary();
  db_query("DELETE FROM {term_node} WHERE nid = %d AND tid IN (SELECT tid FROM {term_data} WHERE vid = %d)", $nid, $vocabulary->vid);
  error_log(sprintf("DELETE FROM {term_node} WHERE nid = %d AND tid IN (SELECT tid FROM {term_data} WHERE vid = %d)", $nid, $vocabulary->vid));
  // add terms for each og group
  $gid_dbh = db_query("SELECT group_nid FROM {og_ancestry} WHERE nid = %d", $nid);
  while ($group_nid = db_result($gid_dbh)) {
    $ptid = ndla_fag_taxonomy_get_term_from_og($group_nid);
    // find term lineage
    $terms = array();
    foreach (taxonomy_get_parents_all($ptid) as $term) {
      $terms[$term->tid] = $term->tid;
    }
    // for each revision
    $vid_dbh = db_query("SELECT vid FROM {node_revisions} WHERE nid = %d", $nid);
    while ($vid = db_result($vid_dbh)) {
      foreach ($terms as $tid => $tull) {
        db_query("INSERT INTO {term_node} (nid, vid, tid) VALUES (%d, %d, %d)", $nid, $vid, $tid);
        error_log(sprintf("INSERT INTO {term_node} (nid, vid, tid) VALUES (%d, %d, %d)", $nid, $vid, $tid));
      }
    }
  }
}

function ndla_fag_taxonomy_modify_content_types($content_types) {
  $vocabulary = ndla_fag_taxonomy_get_default_vocabulary();
  $field = array (
    'label' => $vocabulary->name,
    'field_name' => 'field_fag_taxonomy',
    'type' => 'content_taxonomy',
    'widget_type' => 'content_taxonomy_hs',
    'change' => 'Change basic information',
    'weight' => '8',
    'description' => '',
    'group' => false,
    'required' => 1,
    'multiple' => '1',
    'save_term_node' => 1,
    'vid' => $vocabulary->vid,
    'parent' => '0',
    'parent_php_code' => '',
    'depth' => '',
    'op' => 'Save field settings',
    'module' => 'content_taxonomy',
    'widget_module' => 'hs_content_taxonomy',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'int',
        'not null' => false,
        'sortable' => false,
      ),
    ),
    'rdf_property' => '',
    'display_settings' => 
    array (
      'weight' => '3',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      5 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  );

  module_load_include('inc', 'content', 'includes/content.crud');
  foreach ($content_types as $type) {
    set_time_limit(0);
    $field['type_name'] = $type;
    content_field_instance_create($field);
    set_time_limit(30);
  }
}
