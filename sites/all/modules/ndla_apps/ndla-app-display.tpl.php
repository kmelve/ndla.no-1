<?php
/**
 * @file
 *  Template for viewing a ndla application
 * 
 * Available variables:
 *   $url: The application url
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html version="XHTML+RDFa 1.0" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" xmlns:og="http://opengraphprotocol.org/schema/" dir="<?php print $language->dir ?>">
  <head>
  </head>
  <body style='height: 100%;'>
<div style='height: 150px; background-color: red;'><h1>TOPPEN</h1></div>
<?php
  echo '<iframe src="/' . $url . '" frameborder=0 style="width: 100%; height: 100%;"></iframe>';
?>
</body>
</html>
