<?php
/**
 * @file
 *  Template for listing of ndla applications
 *
 * Available variables:
 *   $apps: Array with application objects. Each object has a title, teaser, promote, sticky and screenshot property
 */
$counter = 0;
?>
<div class="ndla-apps-block-background">
  <?php if(sizeof($apps) > 10): ?>
    <div class="ndla-apps-controls" id="ndla-apps-prev"><i class="fa fa-chevron-left"></i></div>
    <div class="ndla-apps-controls" id="ndla-apps-next"><i class="fa fa-chevron-right"></i></div>
  <?php endif; ?>
  <div class='scroller'>
      <ul id="app-list" class="clear-block">
        <?php foreach ($apps as $app):?>
          <?php
            $classes = array('ndla-app');
            if ($counter == 4) {
              // Split into new div
              $counter = 0;
            }
            if ($app->start_type == 1) {
              $classes[] = 'js-app';
            }
            $lightbox = "";
            if($app->lightbox) {
              $lightbox = 'rel="lightframe';
              if(!empty($app->lightbox_size)) {
                $lightbox .= '[|' . $app->lightbox_size . ']';
              }
              else {
                $lightbox .= '[]';
              }
              $lightbox .= '[' . $app->title . ']"';
            }
            $counter++;
          ?>
          <li<?php if ($app->start_type == 1): ?> id="<?php print $app->start_button_id ?>"<?php endif; if (!empty($classes)):?> class="<?php print implode(' ', $classes)?>"<?php endif ?>>
            <div>
            <?php if ($app->start_type == 0):?>
              <?php
                $url_opts = array();
                if ($app->lang_prefix == 0) {
                  // By sending an empty array in the language key to the url() function
                  // we avoid adding language prefix in front of relative URL's.
                  $url_opts['language'] = array();
                }
                if ($app->auto_context == 0) {
                  $url_opts['ndla_utils_query_set'] = TRUE;
                }
              ?>
              <a href="<?php print str_replace(array('%3F', '%3D'), array('?', '='), url($app->start_url, $url_opts)) ?>" title="<?php print t('Go to application @name', array('@name' => $app->title)) ?>" class="url-app" <?php print $lightbox; ?>>
            <?php endif; ?>
            <img alt='<?php print t($app->title); ?>' src='<?php print url($app->medium_icon->filepath, array('language' => array())); ?>' />

            <?php if ($app->start_type == 0):?>
              </a>
            <?php endif ?>
            </div>
          </li>
        <?php endforeach; ?>
      </ul>
  </div>
</div>
<div class='clear'></div>
