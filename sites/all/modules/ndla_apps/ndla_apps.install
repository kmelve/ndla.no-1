<?php
/**
 * @file
 * @ingroup ndla_apps
 */
 
/**
 * Implementation of hook_schema().
 */
function ndla_apps_schema() {
  $schema['ndla_apps'] = array(
    'description' => 'Stores information about the ndla applications.',
    'fields' => array(
      'nid' => array(
        'description' => 'The node id of the ndla_app node.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'vid' => array(
        'description' => 'Primary Key: The node version id of the ndla_app node.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'global' => array(
        'description' => 'Should this app be available on all pages?',
        'type' => 'int',
        'size' => 'tiny',
        'unsiged' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'protected' => array(
        'description' => 'Should this app only be available to logged in users?',
        'type' => 'int',
        'size' => 'tiny',
        'unsiged' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'start_type' => array(
        'description' => 'How should the application be started? (0 for url, 1 for js)',
        'type' => 'int',
        'size' => 'tiny',
        'unsiged' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'auto_context' => array(
        'description' => 'Should context query be added to the url? (0 for no, 1 for yes)',
        'type' => 'int',
        'size' => 'tiny',
        'unsiged' => TRUE,
        'not null' => TRUE,
        'default' => 1,
      ),
      'start_url' => array(
        'description' => 'Url path where the application will be started',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'alias_start_url' => array(
        'description' => 'Url alias for the application',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'lang_prefix' => array(
        'description' => 'Wether to add language prefix to url or not',
        'type' => 'int',
        'size' => 'tiny',
        'unsiged' => TRUE,
        'not null' => TRUE,
        'default' => 1,
      ),
      'intro_url' => array(
        'description' => 'Url path for the intro resource',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'start_button_id' => array(
        'description' => 'HTML id tag for the start button',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'lightbox' => array(
        'description' => 'If the application should open in a lightbox',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'default' => 0,
      ),
      'lightbox_size' => array(
        'description' => 'Size of the lightbox',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'large_icon' => array(
        'description' => 'The large icon fid.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'medium_icon' => array(
        'description' => 'The medium icon fid.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'small_icon' => array(
        'description' => 'The small icon fid.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'extra_small_icon' => array(
        'description' => 'The extra small icon fid.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('vid'),
  );
  return $schema;
}

/**
 * Implementation of hook_update_N
 *
 * Adds a field for storing whether the app is global or not.
 */
function ndla_apps_update_6000() {
  $results = array();
  db_add_field($results, 'ndla_apps', 'global', array(
    'description' => 'Should this app be available on all pages?',
    'type' => 'int',
    'size' => 'tiny',
    'unsiged' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  ));
  ndla_apps_update_global_and_front();
  return $results;
}

function ndla_apps_update_6001() {
  $results = array();
  db_drop_field($results, 'ndla_apps', 'start_button_text');
  db_add_field($results, 'ndla_apps', 'protected', array(
    'description' => 'Should this app only be available to logged in users?',
    'type' => 'int',
    'size' => 'tiny',
    'unsiged' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  ));
  return $results;
}

function ndla_apps_update_6002() {
  $results = array();
  db_drop_field($results, 'ndla_apps', 'more');
  db_add_field($results, 'ndla_apps', 'intro_url', array(
    'description' => 'Url path for the intro resource',
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
    'default' => '',
  ));
  return $results;
}

function ndla_apps_update_6003() {
  $results = array();
  db_add_field($results, 'ndla_apps', 'lang_prefix', array(
    'description' => 'Wether to add language prefix to url or not',
    'type' => 'int',
    'size' => 'tiny',
    'unsiged' => TRUE,
    'not null' => TRUE,
    'default' => 1,
  ));
  return $results;
}

function ndla_apps_update_6004() {
  $results = array();
  db_add_field($results, 'ndla_apps', 'auto_context', array(
    'description' => 'Should context query be added to the url? (0 for no, 1 for yes)',
    'type' => 'int',
    'size' => 'tiny',
    'unsiged' => TRUE,
    'not null' => TRUE,
    'default' => 1,
  ));
  return $results;
}

function ndla_apps_update_6005() {
  $results = array();
  db_add_field($results, 'ndla_apps', 'lightbox', array(
    'description' => 'If the application should open in a lightbox',
    'type' => 'int',
    'size' => 'tiny',
    'not null' => FALSE,
    'default' => 0,
  ));
  
  db_add_field($results, 'ndla_apps', 'lightbox_size', array(
    'description' => 'Size of the lightbox',
    'type' => 'varchar',
    'length' => 255,
    'not null' => FALSE,
    'default' => '',
  ));
  
  return $results;
}

function ndla_apps_update_6006() {
  $results = array();

  db_add_field($results, 'ndla_apps', 'alias_start_url', array(
    'description' => 'Url alias for the application',
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
    'default' => '',
  ));
    
  return $results;
}

function ndla_apps_update_6007() {
  $results = array();

  db_add_field($results, 'ndla_apps', 'large_icon', array(
    'description' => 'Large icon fid',
    'type' => 'int',
    'unsiged' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  ));
  
  db_add_field($results, 'ndla_apps', 'medium_icon', array(
    'description' => 'Medium icon fid',
    'type' => 'int',
    'unsiged' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  ));
  
  db_add_field($results, 'ndla_apps', 'small_icon', array(
    'description' => 'Small icon fid',
    'type' => 'int',
    'unsiged' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  ));
  
  db_add_field($results, 'ndla_apps', 'extra_small_icon', array(
    'description' => 'Extra small icon fid',
    'type' => 'int',
    'unsiged' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  ));
  
  db_drop_field($results, 'ndla_apps', 'screenshot');
  
  return $results;
}



function ndla_apps_install() {
  drupal_install_schema('ndla_apps');
}

function ndla_apps_uninstall() {
  drupal_uninstall_schema('ndla_apps');
  $variables = array('ndla_apps_thumbnail_size', 'ndla_apps_preview_size', 'ndla_apps_has_front', 'ndla_apps_has_global', 'ndla_apps_has_protected_front', 'ndla_apps_has_protected_global');
  foreach ($variables as $variable) {
    variable_del($variable);
  }
}
