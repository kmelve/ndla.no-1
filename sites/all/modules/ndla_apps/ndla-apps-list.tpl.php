<?php
/**
 * @file
 *  Template for listing of ndla applications
 *
 * Available variables:
 *   $apps: Array with application objects. Each object has a title, teaser, promote, sticky and screenshot property
 */
$counter = 0;
?>
<ul id="app-list" class="clear-block tray-stand-out">
<?php foreach ($apps as $app):?>
  <?php
    $classes = array('ndla-app');
    if ($counter == 4) {
      $classes[] = 'row-first';
      $counter = 0;
    }
    if ($app->start_type == 1) {
      $classes[] = 'js-app';
    }
    $lightbox = "";
    if($app->lightbox) {
      $lightbox = 'rel="lightframe';
      if(!empty($app->lightbox_size)) {
        $lightbox .= '[|' . $app->lightbox_size . ']';
      }
      else {
        $lightbox .= '[]';
      }
      $lightbox .= '[' . $app->title . ']"';
    }
    $counter++;
  ?>
  <li<?php if ($app->start_type == 1): ?> id="<?php print $app->start_button_id ?>"<?php endif; if (!empty($classes)):?> class="<?php print implode(' ', $classes)?>"<?php endif ?>>
    <div>
    <?php if ($app->start_type == 0):?>
      <?php
        $url_opts = array();
        if ($app->lang_prefix == 0) {
          // By sending an empty array in the language key to the url() function
          // we avoid adding language prefix in front of relative URL's.
          $url_opts['language'] = array();
        }
        if ($app->auto_context == 0) {
          $url_opts['ndla_utils_query_set'] = TRUE;
        }
      ?>
      <a href="<?php print url($app->start_url, $url_opts)?>" title="<?php print t('Go to application')?>" class="url-app" <?php print $lightbox; ?>>
    <?php endif; ?>
    
    
    <?php if (!empty($app->intro_url)):?>
      <?php
      if(module_exists('istribute') && preg_match('/contentbrowser\/node\/([\d]*)$/', $app->intro_url, $matches)) {
        $nid = $matches[1];
        $video = node_load($nid);
        $width = variable_get('istribute_playerwidth', 426);
        $aspect = 16/9;
        $metadata = istribute_get_metadata($node->istribute_video);
        if(isset($metadata->aspect)) {
          $aspect = $metadata->aspect;
        }
        $width += 10;
        $height = (ceil(($width/$aspect))+25);
        print l(t('Introduction'), $app->intro_url . "/$width/$height", array('attributes' => array('rel' => 'lightframe[|width:'. $width . 'px;height:' . $height . 'px;]', 'class' => 'app-intro')));
      } else {
        print l(t('Introduction'), $app->intro_url, array('attributes' => array('rel' => 'lightframe', 'class' => 'app-intro')));
      }
      ?>

    <?php endif ?>
    </div>
  </li>
<?php endforeach; ?>
</ul>
