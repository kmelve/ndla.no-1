<?php
/**
 * @file
 * @ingroup ndla_content_sync
 */
 
 
 /**
  * Helper function for making connections to endpoint more robust for timeout errors
  */
 function _ndla_content_sync_call_xmlrpc($args) {
   $retry = 0;
   $xml = FALSE;
   while ($retry++ < variable_get('ndla_content_sync_num_retries', 5)) {
     $xml = call_user_func_array('xmlrpc', $args);
     if ($xml === FALSE) {
       $error = xmlrpc_error();
       if ($error->code == 0) {
         // Timeout. Retry.
         $method = $args[1];
         $wait = $retry * $retry * 10;
         error_log("Timeout for method $method. Will retry in $wait seconds (retry number $retry)");

         if (count($args) > 2) {
           // We need new keys, so we replace the old ones here.
           $tmp = array_splice($args, 2, 4, _ndla_content_sync_xmlrpc_key_args($method));
         }
         
         // Wait before next retry
         sleep($wait);
         continue;
       }
       // Other error. Exit loop.
       break;
     }
     else {
       // Ok. Exit loop.
       break;
     }
   }

   return $xml;
 }
 
/**
 * Abstraction of the service calling code.
 * 
 * Makes an xml-rpc call to the Services module of another Drupal
 * installation, and returns the resulting array. The initial connection and
 * user login calls are made first if needed.
 * 
 * @param $method
 * string telling us what method to call, examples:
 *     - views.get
 *     - file.getNodeFiles
 *     - node.get
 *     - image.getNodeImages
 * @param ...
 * Any additional arguments are passed on to the web
 * service methods - can be anything valid for the method call being
 * invoked.
 *
 * @return
 * array containing results of the call 
 */
function ndla_content_sync_xmlrpc($method) {
  static $login_session_id;
  static $endpoint;

  // Log in to the remote system may be skipped if we already have a login 
  // session, AND the settings allow login to be skipped.
  // Skipping login causes failures for mysterious reasons: @see <http://drupal.org/node/711542>
  if (!isset($login_session_id)) {
    // Get static settings.
    $endpoint = variable_get('ndla_content_sync_endpoint', 'http://red.ndla.no/services/xmlrpc');

    // Build the array of connection arguments.
    $connect_args = array($endpoint, 'system.connect');

    // Connect to the remote system service to get an initial session id to log in with.
    $connect = _ndla_content_sync_call_xmlrpc($connect_args);
    if ($connect === FALSE) {
      return xmlrpc_error();
    }
    $session_id = $connect['sessid'];
    
    // We may want to call only system.connect for testing purposes.
    if ($method == 'system.connect') {
      return $connect; 
    }
  
    // Get the API key-related arguments.
    $key_args = _ndla_content_sync_xmlrpc_key_args('user.login');
    
    // Get account details
    $username = variable_get('ndla_content_sync_webservice_username', '');
    $password = variable_get('ndla_content_sync_webservice_password', '');
  
    // Build the array of connection arguments we need to log in.
    $login_args = array_merge(
      array($endpoint, 'user.login'),
      $key_args,
      array($session_id),
      array($username, $password)
    );
  
    // Call the xmlrpc method with our array of arguments. This accounts for 
    // whether we use a key or not, and the extra parameters to pass to the method.
    $login = _ndla_content_sync_call_xmlrpc($login_args);
    if ($login === FALSE) {
      return xmlrpc_error();
    }
    $login_session_id = $login['sessid'];   
     
    // We may want to call only user.login for testing purposes.
    if ($method == 'user.login') {
      return $login; 
    }
  }

  // Get the API key-related arguments.
  $key_args = _ndla_content_sync_xmlrpc_key_args($method);
  
  // Get all the arguments this function has been passed.
  $function_args  = func_get_args();
  // Slice out the ones that are arguments to the method call: everything past
  // the 4th argument.
  $method_args    = array_slice($function_args, 1);

  // Build the array of connection arguments for the method we want to call.
  $xmlrpc_args = array_merge(
    array($endpoint, $method),
    $key_args,
    array($login_session_id),
    $method_args
  );

  // Call the xmlrpc method with our array of arguments.
  $result = _ndla_content_sync_call_xmlrpc($xmlrpc_args);
  if ($result === FALSE) {
    return xmlrpc_error();
  }

  return $result;
}

/**
 * Helper function to build the xmlrpc arguments needed to use an API key.
 *
 * This should be called to build xmlrpc() arguments regardless of whether a
 * key is actually in use, as this is taken into account here.
 *
 * @param $method
 *  The name of the method we are going to call with these parameters.
 *
 * @return
 *  An array of parameters. If no key is being used, this will be empty.
 */
function _ndla_content_sync_xmlrpc_key_args($method) { 
  $api_key  = variable_get('ndla_content_sync_api_key', '');
  
  // Build the API key arguments - if no key supplied supplied, presume not required
  if ($api_key != '') {
    //use api key to get a hash code for the service.
    $timestamp = (string) strtotime("now");
    if($_SERVER['SERVER_NAME'] == '') {
      // We are being called from drush. Set domain manually
      $domain = 'localhost';
    }
    else {
      $domain = $_SERVER['SERVER_NAME'];
      if ($_SERVER['SERVER_PORT'] != 80) {
        $domain .= ':' . $_SERVER['SERVER_PORT'];        
      }
    }
    
    // HACK: For now, we report localhost as where we are syncing from. This is
    // because we sync both from the browser, and from Drush, and Drush does 
    // not "know" its address.
    $domain = 'localhost';
    
    $nonce = _ndla_content_sync_get_unique_code('10');
    $hash_parameters = array(
      $timestamp,
      $domain,
      $nonce,
      $method,
    );
    $hash = hash_hmac("sha256", implode(';', $hash_parameters), $api_key);
    
    $key_args = array($hash, $domain, $timestamp, $nonce);
  }
  else {
    $key_args = array();
  }
  
  return $key_args;
}

/**
 * Helper function to generate the $nonce for the xmlrpc hash.
 */
function _ndla_content_sync_get_unique_code($length = '') {
  $code = md5(uniqid(rand(), true));
  if ($length != "") return substr($code, 0, $length);
  else return $code;	
}
