<?php

/**
 * @file
 * @ingroup ndla_content_sync
 *   drush integration for the content sync framework.
 */

/**
 * Implementation of hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing the commands.
 */
function ndla_content_sync_drush_command() {
  $items = array();

  $items['csync-connect'] = array(
    'description' => dt('Test the connection settings by calling system.connect on the remote server.'),
  );

  $items['csync-login'] = array(
    'description' => dt('Test the remote user settings by calling user.login on the remote server.'),
  );

  $items['csync-users'] = array(
    'description' => dt('Query the service on the remote server and retrieve all users.'),
  );

  $items['csync-tax'] = array(
    'description' => dt('Query the service on the remote server and retrieve vocabularies.'),
  );

  $items['csync-fagtax'] = array(
    'description' => dt('Query the service on the remote server and retrieve fag taxonomy data.'),
  );

  $items['csync-h5p'] = array(
    'description' => dt('Query the service on the remote server and retrieve h5p support tables.'),
  );

  $items['csync-words'] = array(
    'description' => dt('Query the service on the remote server and retrieve words data.'),
  );

  $items['csync-front-slideshow'] = array(
    'description' => dt('Query the service on the remote server and retrieve slideshow data for the front page.'),
  );

  $items['csync-applications'] = array(
    'description' => dt('Query the service on the remote server and retrieve application data.'),
  );

  $items['csync-front'] = array(
    'description' => dt('Query the service on the remote server and retrieve front page content.'),
  );

  $items['csync-retrieve'] = array(
    'description' => dt('Perform a full retrieval of content. This saves nodes locally and updates the last run time.'),
  );

  $items['csync-reset'] = array(
    'description' => dt('Reset the timestamp for the last sync. This will force a full synchronization from the content production server.'),
  );

  $items['csync-relations'] = array(
    'descriptions' => dt('Perform a retrieval of defined relations.')
  );

  return $items;
}

/**
 * User defined error handler
 *
 * We define our own error handler, to be able to insert information into
 * the drush log at the right place, compared errors occuring in the code.
 * Events logged with drush_log() and drupal_set_message() are output at the
 * end of the drush command output, and it's impossible to get debug output
 * related to internal errors occuring in Drupal.
 * To log through this system, use trigger_error(<message>)
 */
function ndla_content_sync_error_handler($errno, $message, $filename, $line, $context) {
  $error_mask = E_USER_NOTICE | E_USER_WARNING | E_USER_ERROR;
  if ($errno & $error_mask) {
    // Trap user level errors of type (the default error level for trigger_error() is E_USER_NOTICE)
    if ($errno & E_USER_NOTICE) {
      drush_log($message, 'success');
    }
    elseif ($errno & E_USER_WARNING) {
      drush_log($message, 'warning');
    }
    else {
      drush_log($message, 'error');
    }
  }
  else {
    // Let Drush's error handle cope with the other errors
    drush_error_handler($errno, $message, $filename, $line, $context);
  }
}


/**
 * Test connection to remote site
 */
function drush_ndla_content_sync_csync_connect() {
  ini_set('memory_limit', '2048M');
  $connect = ndla_content_sync_xmlrpc('system.connect');

  if (is_array($connect) && isset($connect['user'])) {
    drush_log(t('Sucessfully connected to the remote site.'), 'success');
  }
  elseif (is_object($connect) && $connect->is_error) {
    $error = array(
      '!message' => $connect->message,
      '!code' => $connect->code,
    );
    drush_log(t('Could not connect to the remote site: !message (Code: !code)', $error), 'error');
  }
  else {
    drush_log(t('Could not connect to the remote site.'), 'error');
  }
}


/**
 * Test user login on remote site
 */
function drush_ndla_content_sync_csync_login() {
  ini_set('memory_limit', '2048M');
  $login = ndla_content_sync_xmlrpc('user.login');

  if (is_array($login) && isset($login['user'])) {
    $form_state['storage']['login'] = $login;
    
    $log_args = array(
      '%user' => $login['user']['name'],
      '@uid'  => $login['user']['uid'],
    );
    drush_log(t('Sucessfully logged in to the remote site; got back details for user %user (uid @uid).', $log_args), 'success');
  }
  elseif (is_object($login) && $login->is_error) {
    $error = array(
      '!message' => $login->message,
      '!code' => $login->code,
    );
    drush_log(t('Could not log in to the remote site: !message (Code: !code)', $error), 'error');
  }
  else {
    drush_log(t('Could not log in to the remote site.'), 'warning');
  }
}


/**
 * Retrieve all users from remote site
 *
 * Note: User 1 is not synced. If anything should go wrong while this was in
 * progress, we could possibly end up with no super user on this site!
 */
function drush_ndla_content_sync_csync_users() {
  ini_set('memory_limit', '2048M');
  if (ndla_content_sync_update_users()) {
    drush_log(t('Finished updating users.'), 'success');
  }
  else {
    drush_log(t('Updating users failed.'), 'error');
  }
}


/**
 * Update the taxonomy system
 */
function drush_ndla_content_sync_csync_tax() {
  ini_set('memory_limit', '2048M');
  if (ndla_content_sync_update_taxonomy()) {
    drush_log(t('Finished updating taxonomy system.'), 'success');
  }
  else {
    drush_log(t('Updating taxonomy system failed.'), 'error');
  }
}


/**
 * Update the fag taxonomy tables
 */
function drush_ndla_content_sync_csync_fagtax() {
  ini_set('memory_limit', '2048M');
  if (ndla_content_sync_update_fag_taxonomy()) {
    drush_log(t('Finished updating fag taxonomy tables.'), 'success');
  }
  else {
    drush_log(t('Updating fag taxonomy tables failed.'), 'error');
  }
}


/**
 * Update the h5p system
 */
function drush_ndla_content_sync_csync_h5p() {
  ini_set('memory_limit', '2048M');
  if (ndla_content_sync_update_h5p()) {
    drush_log(t('Finished updating h5p system.'), 'success');
  }
  else {
    drush_log(t('Updating h5p system failed.'), 'error');
  }
}

/**
 * Update the words tables
 */
function drush_ndla_content_sync_csync_words() {
  ini_set('memory_limit', '2048M');
  if (ndla_content_sync_update_words()) {
    drush_log(t('Finished updating words tables.'), 'success');
  }
  else {
    drush_log(t('Updating words system failed.'), 'error');
  }
}

/**
 * Update the slideshow content for the front page
 */
function drush_ndla_content_sync_csync_front_slideshow() {
  ini_set('memory_limit', '2048M');
  if (ndla_content_sync_update_front_slideshow()) {
    drush_log(t('Finished updating slideshow content for the front page.'), 'success');
  }
  else {
    drush_log(t('Updating slideshow content for the front page failed.'), 'error');
  }
}

/**
 * Update the applications
 */
function drush_ndla_content_sync_csync_applications() {
  ini_set('memory_limit', '2048M');
  if (ndla_content_sync_update_applications()) {
    drush_log(t('Finished updating applications.'), 'success');
  }
  else {
    drush_log(t('Updating applications failed.'), 'error');
  }
}

/**
 * Update the front page content
 */
function drush_ndla_content_sync_csync_front() {
  ini_set('memory_limit', '2048M');
  if (ndla_content_sync_update_front_page()) {
    drush_log(t('Finished updating front page content.'), 'success');
  }
  else {
    drush_log(t('Updating front page content failed.'), 'error');
  }
}


/**
 * Retrieve nodes from the remote site
 */
function drush_ndla_content_sync_csync_retrieve() {
  ini_set('memory_limit', '2048M');
  set_error_handler('ndla_content_sync_error_handler');
  $num_retrieved = ndla_content_sync_retrieve_nodes();
  if ($num_retrieved !== FALSE) {
    drush_log(t('Retrieved !num nodes.', array('!num' => $num_retrieved)), 'success');
  }
}


/**
 * Reset information of last sync
 */
function drush_ndla_content_sync_csync_reset() {
  variable_del("ndla_content_sync_last_sync");
  variable_del("ndla_content_sync_pending_node_list");
  drush_log(t('Last sync reset. Next synchronization will retrieve all nodes from the remote server.'), 'success');
}

/**
 * Retrieve all users from remote site
 *
 * Note: User 1 is not synced. If anything should go wrong while this was in
 * progress, we could possibly end up with no super user on this site!
 */
function drush_ndla_content_sync_csync_relations() {
  ini_set('memory_limit', '2048M');
  if (ndla_content_sync_update_relations()) {
    drush_log(t('Finished retrieval of relations.'), 'success');
  }
  else {
    drush_log(t('Failed retrieval of relations.'), 'error');
  }
}
