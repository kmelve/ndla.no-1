<?php
/**
 * @file
 * @ingroup ndla_content_sync
 */

/**
 * Admin settings form.
 * 
 * @return
 *   array containing the form
 */
function ndla_content_sync_admin_settings_form($form_state) {
  $form = array();

  $form['enable_services'] = array(
	  '#title' => t('Enable XMLRPC services'),
	  '#type' => 'checkbox',
	  '#required' => 'true',
	  '#default_value' => variable_get('ndla_content_sync_services_enabled', FALSE),
  );

  $form['endpoint'] = array(
	  '#title' => t('Web service endpoint'),
	  '#type' => 'textfield',
	  '#required' => 'true',
	  '#default_value' => variable_get('ndla_content_sync_endpoint', 'http://red.ndla.no/services/xmlrpc'),
  );

  $form['api_key'] = array(
	  '#title' => t('Web service API key'),
	  '#type' => 'textfield',
	  '#default_value' => variable_get('ndla_content_sync_api_key', ''),
  );

  $form['username'] = array(
	  '#title' => t('Web service user name'),
	  '#type' => 'textfield',
	  '#default_value' => variable_get('ndla_content_sync_webservice_username', ''),
  );

  $form['password'] = array(
	  '#title' => t('Web service password'),
	  '#type' => 'textfield',
	  '#default_value' => variable_get('ndla_content_sync_webservice_password', ''),
  );

  $form['num_per_run'] = array(
	  '#title' => t('Number of nodes to sync per run'),
	  '#type' => 'textfield',
	  '#description' => t('The number of nodes that will be retrieved from the remote site on each sync. 0 = all.'),
	  '#default_value' => variable_get('ndla_content_sync_num_per_run', 0),
  );

  $form['num_retries'] = array(
	  '#title' => t('Number of retries if connection times out'),
	  '#type' => 'textfield',
	  '#description' => t('The number of retries when connection times out.'),
	  '#default_value' => variable_get('ndla_content_sync_num_retries', 5),
  );

  $form['submit'] = array(
    '#value' => 'Save settings',
	  '#type' => 'submit',
  );

  return $form;
}


/**
 * Handle form submit.
 * 
 */
function ndla_content_sync_admin_settings_form_submit($form, &$form_state) {
  variable_set('ndla_content_sync_services_enabled', $form_state['values']['enable_services']);

  // We need to clear the services cache to be able to turn our services on
  // and off.
  services_get_all(TRUE, TRUE);

  variable_set('ndla_content_sync_endpoint', $form_state['values']['endpoint']);
  variable_set('ndla_content_sync_api_key', $form_state['values']['api_key']);
  variable_set('ndla_content_sync_webservice_username', $form_state['values']['username']);
  variable_set('ndla_content_sync_webservice_password', $form_state['values']['password']);
  variable_set('ndla_content_sync_num_per_run', $form_state['values']['num_per_run']);
  variable_set('ndla_content_sync_num_retries', $form_state['values']['num_retries']);

  drupal_set_message(t('The settings have been saved.'));
}


/**
 * Manual control and testing form.
 * 
 * @return
 *   array containing the form
 */
function ndla_content_sync_manual_form($form_state) {
  $form = array();

  $form['notice'] = array(
    '#type' => 'markup', 
    '#value' => '<p>' . t('Use this page to test your connection is set up correctly, and to manually retrieve nodes from the remote site.') . '</p>',
    '#weight' => -10,  
  );

  $form['buttons'] = array(
    '#theme' => 'ndla_content_sync_manual_form_button', 
    '#tree' => TRUE,
  );

  $form['buttons']['connect'] = array(
    '#value' => 'Test connection',
    '#type' => 'submit',
    '#name' => 'connect',
    '#submit' => array('ndla_content_sync_admin_manual_form_connection_submit'),
    '#description' => t('Test the connection settings by calling system.connect on the remote server.'),
  );

  $form['buttons']['login'] = array(
    '#value' => 'Test user login',
    '#type' => 'submit',
    '#name' => 'connection',
    '#submit' => array('ndla_content_sync_admin_manual_form_login_submit'),
    '#description' => t('Test the remote user settings by calling user.login on the remote server.'),
  );
  
  $form['buttons']['vocab'] = array(
    '#value' => 'Refresh taxonomy system',
    '#type' => 'submit',
    '#name' => 'taxonomy',
    '#submit' => array('ndla_content_sync_admin_manual_form_tax_submit'),
    '#description' => t('Query the service on the remote server and retrieve vocabularies.'),
  );

  $form['buttons']['front'] = array(
    '#value' => 'Retrieve front page content',
    '#type' => 'submit',
    '#name' => 'taxonomy',
    '#submit' => array('ndla_content_sync_admin_manual_form_front_page_submit'),
    '#description' => t('Query the service on the remote server and retrieve front page content.'),
  );

  $form['buttons']['retrieve'] = array(
    '#value' => 'Retrieve content now',
    '#type' => 'submit',
    '#name' => 'retrieve',
    '#submit' => array('ndla_content_sync_admin_retrieve_now_form_submit'),
    '#description' => t('Perform a full retrieval of content. This saves nodes locally and updates the last run time.'),
  );

  $form['buttons']['relations'] = array(
     '#value' => 'Retrieve relations now',
     '#type' => 'submit',
     '#name' => 'relations',
     '#submit' => array('ndla_content_sync_admin_retrieve_relations_form_submit'),
     '#description' => t('Perform a retrieval of relations.'),
   );
  return $form;
}


function ndla_content_sync_admin_manual_form_connection_submit($form, &$form_state) {
  $connect = ndla_content_sync_xmlrpc('system.connect');

  if (is_array($connect) && isset($connect['user'])) {
    drupal_set_message(t('Sucessfully connected to the remote site.'));
  }
  elseif (is_object($connect) && $connect->is_error) {
    $error = array(
      '!message' => $connect->message,
      '!code' => $connect->code,
    );
    drupal_set_message(t('Could not connect to the remote site: !message (Code: !code)', $error), 'error');
  }
  else {
    drupal_set_message(t('Could not connect to the remote site.'), 'warning');
  }
}


function ndla_content_sync_admin_manual_form_login_submit($form, &$form_state) {
  $login = ndla_content_sync_xmlrpc('user.login');
  
  if (is_array($login) && isset($login['user'])) {
    $form_state['storage']['login'] = $login;
    
    drupal_set_message(t('Sucessfully logged in to the remote site; got back details for user %user (uid @uid).', array(
      '%user' => $login['user']['name'],
      '@uid'  => $login['user']['uid'],
      )));
  }
  elseif (is_object($login) && $login->is_error) {
    $error = array(
      '!message' => $login->message,
      '!code' => $login->code,
    );
    drupal_set_message(t('Could not log in to the remote site: !message (Code: !code)', $error), 'error');
  }
  else {
    drupal_set_message(t('Could not log in to the remote site.'), 'warning');
  }
}


/**
 * Update the taxonomy system
 */
function ndla_content_sync_admin_manual_form_tax_submit($form, &$form_state) {
  if (ndla_content_sync_update_taxonomy()) {
    drupal_set_message(t('Finished updating taxonomy system.'), 'status');
  }
  else {
    drupal_set_message(t('Updating taxonomy system failed.'), 'error');
  }
}


/**
 * Update the taxonomy system
 */
function ndla_content_sync_admin_manual_form_front_page_submit($form, &$form_state) {
  if (ndla_content_sync_update_front_page()) {
    drupal_set_message(t('Finished updating front page content.'), 'status');
  }
  else {
    drupal_set_message(t('Updating front page content failed.'), 'error');
  }
}


function ndla_content_sync_admin_retrieve_now_form_submit($form, &$form_state) {
  $node = ndla_content_sync_retrieve_nodes();
}

function ndla_content_sync_admin_retrieve_relations_form_submit($form, &$form_state) {
  if (ndla_content_sync_update_relations()) {
    drupal_set_message(t('Finished retrieval of relations.'), 'success');
  }
  else {
    drupal_set_message(t('Failed retrieval of relations.'), 'error');
  }
}

/**
 * Theme the manual connection form buttons.
 */
function theme_ndla_content_sync_manual_form_button($form) {
  foreach (element_children($form) as $e) {
    $button = drupal_render($form[$e]);
    $output .= '<div>' . $button . '<span>' . $form[$e]['#description'] . '</span></div>';
  }

  return $output;
}
