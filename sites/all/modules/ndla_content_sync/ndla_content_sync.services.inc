<?php
/**
 * @file
 * @ingroup ndla_content_sync
 */
 
/**
 * Fetch all vocabularies in the system
 *
 * Callback for the ndla.getNodeList service. 
 * Note: If no vid is specified to the content syncer, the current version will
 * be fetched.
 *
 * @param $last_run
 *   A UNIX timestamp indicating last synchronization
 *
 * @return
 *   A complete list of all vocabularies on the site
 */
function ndla_content_sync_get_node_list($last_run) {

  $syncs = variable_get('ndla_content_sync_timestamps', array());
  array_unshift($syncs , mktime());
  $syncs = array_slice($syncs, 0, 5);
  variable_set('ndla_content_sync_timestamps', $syncs);

  $node_list = array();
  if (module_exists("ndla_publish_workflow")) {
    $node_list = ndla_publish_workflow_get_changed($last_run);
  }

  return base64_encode(gzcompress(serialize($node_list)));
}


/**
 * Fetch a full node
 *
 * Callback for the ndla.getNode service
 *
 * @param $nid
 *   Node id for the node to load
 *
 * @param $vid
 *   Revision id of node to load
 *
 * @return
 *   Object as returned from node_load
 */
function ndla_content_sync_get_nodes($node_list) {
  $return_nodes = array();
  foreach($node_list as $node_to_load) {
    $return_nodes[$node_to_load['nid']] = node_load($node_to_load['nid'], $node_to_load['vid']);
    _ndla_content_sync_load_edit_dates($return_nodes[$node_to_load['nid']]);
  }
  return base64_encode(gzcompress(serialize($return_nodes)));
}


/**
 * Fetch all vocabularies in the system
 *
 * Callback for the ndla.getVocabs service
 *
 * @return
 *   A complete list of all vocabularies on the site
 */
function ndla_content_sync_get_vocabs() {
  return taxonomy_get_vocabularies();
}


/**
 * Fetch all terms for a vocabulary, regardless of language settings
 *
 * Callback for the ndla.getTerm service
 *
 * @param $vid
 *   The id of the vocabulary
 *
 * @return
 *   List of all terms for the given vocabulary
 */
function ndla_content_sync_get_term($vid) {
  ini_set('memory_limit', '2048M');
  $terms = _ndla_content_sync_taxonomy_get_tree($vid);
  return base64_encode(gzcompress(serialize($terms)));
}


/**
 * Helper function to find all terms for a given vocabulary
 *
 * Node: This is a snap-in replacement for the core function
 * sync_taxonomy_get_tree(), but this does not rewrite the SQL to add
 * language filtering. There are also changes to incorporate NDLA
 * specific data into the terms.
 */
function _ndla_content_sync_taxonomy_get_tree($vid, $parent = 0, $depth = -1, $max_depth = NULL) {
  static $children, $parents, $terms, $synonyms;

  $depth++;

  // We cache trees, so it's not CPU-intensive to call get_tree() on a term
  // and its children, too.
  if (!isset($children[$vid])) {
    $children[$vid] = array();
    $parents[$vid] = array();
    $terms[$vid] = array();

    $sql = 'SELECT t.*, h.parent, d.show AS topic_description_on_nodes, v.status AS visible
            FROM {term_data} t
            INNER JOIN {term_hierarchy} h ON t.tid = h.tid 
            LEFT OUTER JOIN {utdanning_hacks_tema_visibility} v ON t.tid=v.tid
            LEFT OUTER JOIN {ndla_utils_show_topic_description} d ON t.tid = d.tid 
            WHERE t.vid = %d
            ORDER BY weight, name';
    $result = db_query($sql, $vid);
    while ($term = db_fetch_object($result)) {
      $children[$vid][$term->parent][] = $term->tid;
      $parents[$vid][$term->tid][] = $term->parent;
      $terms[$vid][$term->tid] = $term;
    }
  }

  // Cache the synonyms
  if (!isset($synonyms)) {
    $synonyms = array();

    $sql = 'SELECT tid, name FROM {term_synonym}';
    $result = db_query($sql);
    while ($syn = db_fetch_object($result)) {
      if(!is_array($synonyms[$syn->tid])) {
        $synonyms[$syn->tid] = array();
      }
      $synonyms[$syn->tid][] = $syn->name;
    }
  }

  $max_depth = (is_null($max_depth)) ? count($children[$vid]) : $max_depth;
  $tree = array();

  // Keeps track of the parents we have to process, the last entry is used
  // for the next processing step.
  $process_parents = array();
  $process_parents[] = $parent;

  // Loops over the parent terms and adds its children to the tree array.
  // Uses a loop instead of a recursion, because it's more efficient.
  while (count($process_parents)) {
    $parent = array_pop($process_parents);
    // The number of parents determines the current depth.
    $depth = count($process_parents);
    if ($max_depth > $depth && !empty($children[$vid][$parent])) {
      $has_children = FALSE;
      $child = current($children[$vid][$parent]);
      do {
        if (empty($child)) {
          break;
        }
        $term = $terms[$vid][$child];
        if (count($parents[$vid][$term->tid]) > 1) {
          // We have a term with multi parents here. Clone the term,
          // so that the depth attribute remains correct.
          $term = clone $term;
        }
        $term->depth = $depth;
        unset($term->parent);
        $term->parents = $parents[$vid][$term->tid];
        $term->synonyms = is_array($synonyms[$term->tid]) ? $synonyms[$term->tid] : array();
        $tree[] = $term;
        if (!empty($children[$vid][$term->tid])) {
          $has_children = TRUE;

          // We have to continue with this parent later.
          $process_parents[] = $parent;
          // Use the current term as parent for the next iteration.
          $process_parents[] = $term->tid;

          // Reset pointers for child lists because we step in there more often
          // with multi parents.
          reset($children[$vid][$term->tid]);
          // Move pointer so that we get the correct term the next time.
          next($children[$vid][$parent]);
          break;
        }
      } while ($child = next($children[$vid][$parent]));

      if (!$has_children) {
        // We processed all terms in this hierarchy-level, reset pointer
        // so that this function works the next time it gets called.
        reset($children[$vid][$parent]);
      }
    }
  }

  return $tree;
}


/**
 * Fetch all taxonomy images
 *
 * Callback for the ndla.getTaxImages service
 *
 * @return
 *   A database object dump of all the taxonomy images stored ob the site. We
 *   do not send any images, as these are synced manually on the side.
 */
function ndla_content_sync_get_taxonomy_images() {
  $images = array();
  $result = db_query('SELECT * FROM {term_image}');
  while ($row = db_fetch_object($result)) {
    $images[] = $row;
  }
  return base64_encode(gzcompress(serialize($images)));
}


/**
 * Fetch fag taxonomy data
 *
 * Callback for the ndla.getFagTaxonomy service
 *
 *
 * @return
 *   Array containing the fag taxonomy structure
 */
function ndla_content_sync_get_fag_taxonomy() {
  $fagtax = array();
  $tables = array(
    "ndla_fag_taxonomy_term",
    "ndla_fag_taxonomy_map",
  );

  foreach ($tables as $table) {
    $fagtax[$table] = array();
    $result = db_query('SELECT * FROM {' . $table . '}');
    while ($row = db_fetch_array($result)) {
      $fagtax[$table][] = $row;
    }
  }

  return base64_encode(gzcompress(serialize($fagtax)));
}


/**
 * Fetch complete h5p library structure
 *
 * Callback for the ndla.getH5p service
 *
 *
 * @return
 *   Array containing the complete h5p structure
 */
function ndla_content_sync_get_h5p() {
  $h5plibs = array();
  $tables = array(
    "h5p_libraries",
    "h5p_libraries_libraries",
    "h5p_libraries_languages",
    "h5p_nodes_libraries",
  );

  foreach ($tables as $table) {
    $h5plibs[$table] = array();
    $result = db_query('SELECT * FROM {' . $table . '}');
    while ($row = db_fetch_array($result)) {
      $h5plibs[$table][] = $row;
    }
  }

  return base64_encode(gzcompress(serialize($h5plibs)));
}


/**
 * Fetch questions for a given quiz
 *
 * Callback for the ndla.getQuizQuestions service
 *
 * @param $nid
 *   The nodeid of the quiz to look up quiestions for
 *
 * @return
 *   List of all questions belonging to the quiz
 */
function ndla_content_sync_get_quiz_questions($nid, $vid) {
  require_once drupal_get_path('module', 'quiz') . '/quiz.module';
  $questions = array();
  $questions = quiz_get_questions($nid, $vid, TRUE, TRUE, TRUE, TRUE);
  
  return base64_encode(gzcompress(serialize($questions)));
}

/**
 * Fetch complete menu structure
 *
 * Callback for the ndla.getMenus service
 *
 *
 * @return
 *   Array containing the complete menu structure
 */
function ndla_content_sync_get_menus() {
  $menus = array();
  $tables = array(
    "ndla_menu_menus",
    "ndla_menu_items",
    "ndla_menu_translations",
    "ndla_menu_terms",
  );

  foreach ($tables as $table) {
    $menus[$table] = array();
    $result = db_query('SELECT * FROM {' . $table . '}');
    while ($row = db_fetch_array($result)) {
      if (($table == 'ndla_menu_translations') && isset($row['image_file_id'])) {
        // Attach file object for ndla_menu_translations rows that has an image
        $file = db_fetch_array(db_query('SELECT * FROM {files} WHERE fid=%d LIMIT 1', $row['image_file_id']));
        if ($file !== FALSE) {
          $row['file'] = $file;
        }
      }
      $menus[$table][] = $row;
    }
  }

  return base64_encode(gzcompress(serialize($menus)));
}

/**
 * Fetch complete words structure
 *
 * Callback for the ndla.getWords service
 *
 *
 * @return
 *   Array containing the complete words structure
 */
function ndla_content_sync_get_words() {
  ini_set('memory_limit', '2048M');
  $words = array();
  $tables = array(
    'ndla_ord_nid_vid_mapping',
  );

  foreach ($tables as $table) {
    $words[$table] = array();

    if ($table == 'ndla_ord_nid_vid_mapping') {
      if (module_exists('ndla_publish_workflow')) {
        $result = db_query('SELECT m.* FROM ndla_ord_nid_vid_mapping m, ndla_publish_workflow_status p WHERE m.nid = p.nid AND m.vid = p.vid AND p.status = %d AND p.timestamp = (SELECT MAX(p2.timestamp) FROM ndla_publish_workflow_status p2 WHERE p2.nid = p.nid AND p2.status = %d)', PUBLISH_WORKFLOW_PUBLISHED, PUBLISH_WORKFLOW_PUBLISHED);
        while ($row = db_fetch_array($result)) {
          $words[$table][] = $row;
        }
      }
    } else {
      $result = db_query('SELECT * FROM {' . $table . '}');
      while ($row = db_fetch_array($result)) {
        $words[$table][] = $row;
      }
    }
  }

  return base64_encode(gzcompress(serialize($words)));
}

/**
 * Fetch all users
 *
 * Callback for the ndla.getUsers service
 *
 *
 * @return
 *   Array containing the complete user list
 */
function ndla_content_sync_get_users() {
  $users = array();

  // Get all users except uid 0.
  $res = db_query("SELECT uid FROM {users} WHERE uid>0 ORDER BY uid");
  
  while ($row = db_fetch_object($res)) {
    $users[] = user_load($row->uid);
  }

  return base64_encode(gzcompress(serialize($users)));
}

/**
 * Fetch all defined relations.
 *
 * Callback for ndla.getRelations service
 *
 * @return
 *  Array containing the list of relations 
 */
function ndla_content_sync_get_relations() {
  $relations = array();
  
  $res = db_query("SELECT * FROM {rdf_resources} WHERE uri LIKE 'http://%' ORDER BY uri");
  while($row = db_fetch_object($res)) {
    $relations['rdf_resources'][$row->rid] = $row;
  }
  
  $res = db_query("SELECT * FROM {rdf_data_ndlaontologi} r WHERE data NOT LIKE 'nid:%' OR data IS NULL");
  while($row = db_fetch_object($res)) {
    $relations['rdf_data_ndlaontologi'][] = $row;
  }
  
  return base64_encode(gzcompress(serialize($relations)));
}

/**
 * Fetch front page content
 *
 * Callback for the ndla.getFrontContent service
 *
 *
 * @return
 *   Array containing the settings for the front page
 */
function ndla_content_sync_get_front_page_content() {
  $content = array(
    'ndla_utils_ndla2010_theme_new_content_first' => variable_get('ndla_utils_ndla2010_theme_new_content_first', ''),
    'ndla_utils_ndla2010_theme_new_content_second' => variable_get('ndla_utils_ndla2010_theme_new_content_second', ''),
    'ndla_utils_ndla2010_theme_user_guide' => variable_get('ndla_utils_ndla2010_theme_user_guide', ''),
  );

  return $content;
}

/**
 * Fetch front page slideshow
 *
 * Callback for the ndla.getFrontSlideshow service
 *
 *
 * @return
 *   Array containing data for the slideshow
 */
function ndla_content_sync_get_front_slideshow() {
  $slideshow = array();
  $tables = array(
    'ndla_utils_slideshow',
  );

  foreach ($tables as $table) {
    $slideshow[$table] = array();

    $result = db_query('SELECT * FROM {' . $table . '}');
    while ($row = db_fetch_array($result)) {
      $slideshow[$table][] = $row;
    }
  }

  return base64_encode(gzcompress(serialize($slideshow)));
}

/**
 * Fetch applications
 *
 * Callback for the ndla.getApplications service
 *
 *
 * @return
 *   Array containing data for the applications
 */
function ndla_content_sync_get_applications() {
  $apps = array(
    'tables' => array(),
    'settings' => array(),
  );

  for ($i = 0; $i < 10; $i++) {
    $apps['settings']['ndla_utils_ndla2010_theme_front_page_application_' . $i] = variable_get('ndla_utils_ndla2010_theme_front_page_application_' . $i, '');
  }

  return base64_encode(gzcompress(serialize($apps)));
}

/**
 * Fetches edit dates from revisions
 */
function _ndla_content_sync_load_edit_dates(&$node) {
  $changes = array();
  if (module_exists('ndla_publish_workflow')) {
    $changes = ndla_publish_workflow_get_published_dates($node->nid);
  }

  if(!empty($changes)) {
    $node->edit_date_created = array_shift($changes);
    if(sizeof($changes) != 0) {
      $node->edit_date_updated = end($changes);
    }
  }
}
