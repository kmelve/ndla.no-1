Drupal.newsFetched = false;
Drupal.behaviors.ndla_ntb = function() {
  if(!Drupal.newsFetched) {
    Drupal.newsFetched = true;
    
    var url = Drupal.settings.basePath;
    url += "ndla_ntb/get_news";
    $.ajax({ 
        url: url, 
          success: function(html) {
            $('#ndla-ntb-ajax-container').replaceWith(html);
          },
          error: function() {
            $('#ndla-ntb-ajax-container').replaceWith(Drupal.t('Unable to load news.'));
          }
    });
  }
}
