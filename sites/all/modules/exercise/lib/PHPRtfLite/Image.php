<?php
/* 
    PHPRtfLite
    Copyright 2007-2008 Denis Slaveckij <info@phprtf.com>
    Copyright 2010 Steffen Zeidler <sigma_z@web.de>

    This file is part of PHPRtfLite.

    PHPRtfLite is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PHPRtfLite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PHPRtfLite.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Class for displaying images in rtf documents.
 * @version     1.0.1
 * @author      Denis Slaveckij <info@phprtf.com>
 * @author      Steffen Zeidler <sigma_z@web.de>
 * @copyright   2007-2008 Denis Slaveckij, 2010 Steffen Zeidler
 * @package     PHPRtfLite
 */
class PHPRtfLite_Image {

    /**
     * @var PHPRtfLite
     */
    protected $_rtf;

    /**
     * @var PHPRtfLite_ParFormat
     */
    protected $_parFormat;

    /**
     * @var string
     */
    protected $_file;

    /**
     * @var string
     */
    protected $_extension;

    /**
     * @var float
     */
    protected $_defaultWidth;

    /**
     * @var float
     */
    protected $_defaultHeight;

    /**
     * @var float
     */
    protected $_height;

    /**
     * @var float
     */
    protected $_width;

    /**
     * @var PHPRtfLite_Border
     */
    protected $_border;

    
    /**
     * Constructor
     * 
     * @param PHPRtfLite    $rtf
     * @param string        $imageFile image file incl. path
     * @param float         $width
     * @param flaot         $height
     */
    public function __construct(PHPRtfLite $rtf,
                                $imageFile,
                                PHPRtfLite_ParFormat $parFormat = null,
                                $width = null,
                                $height = null)
    {
        $this->_rtf = $rtf;
        $this->_parFormat = $parFormat;

        if (is_array($size = @getimagesize($imageFile))) {
            $this->_file = $imageFile;
        
            $type = explode('/', $size['mime']);
            $this->_extension = $type[1];

            list($this->_defaultWidth, $this->_defaultHeight) = $size;

            if ($width !== null) {
                $this->setWidth($width);
            }

            if ($height !== null) {
                $this->setHeight($height);
            }
        }
        else {
            $this->_defaultWidth = 100;
            $this->_defaultHeight = 75;
            $this->_extension = 'png';
        }
    } 

    /**
     * Sets paragraph format for image
     *
     * @param PHPRtfLite_ParFormat $parFormat
     */
    public function setParFormat(PHPRtfLite_ParFormat $parFormat) {
        $this->_parFormat = $parFormat;
    }

    /**
     * Gets paragraph format for image
     * 
     * @return PHPRtfLite_ParFormat
     */
    public function getParFormat() {
        return $this->_parFormat;
    }

    /**
     * Sets image width
     *
     * @param   float   $width  if not defined image is displayed by it's height.
     */
    public function setWidth($width) {
        $this->_width = $width;
    }

    /**
     * Gets image width
     * 
     * @return float
     */
    public function getWidth() {
        return $this->_width;
    }

    /**
     * Sets image height
     *
     * @param   float   $height if not defined image is displayed by it's width.
     */
    public function setHeight($height) {
        $this->_height = $height;
    }

    /**
     * Gets image height
     * 
     * @return float
     */
    public function getHeight() {
        return $this->_height;
    }

    /**
     * Sets border of paragraph
     *
     * @param PHPRtfLite_Border $border
     */
    public function setBorder(PHPRtfLite_Border $border) {
        $this->_border = $border;
    }

    /**
     * Gets border of paragraph
     *
     * @return PHPRtfLite_Border
     */
    public function getBorder() {
        return $this->_border;
    }

    /**
     * Gets rtf image width
     * 
     * @return integer
     */
    private function getImageWidth() {
        if ($this->_width > 0) {
            $width = $this->_width;
        }
        elseif ($this->_height > 0) {
            $width = ($this->_defaultWidth / $this->_defaultHeight) * $this->_height;
        }
        else {
            $width = $this->_defaultWidth * PHPRtfLite::CM_IN_POINTS;
        }

        return round($width * PHPRtfLite::TWIPS_IN_CM);
    }

    /**
     * Gets rtf image height
     *
     * @return integer
     */
    private function getImageHeight() {
        if ($this->_height > 0) {
            $height = $this->_height;
        }
        elseif ($this->_width > 0) {
            $height= ($this->_defaultHeight /$this->_defaultWidth) * $this->_width;
        }
        else {
            $height = $this->_defaultHeight * PHPRtfLite::CM_IN_POINTS;
        }

        return round($height * PHPRtfLite::TWIPS_IN_CM);
    }

    /**
     * Gets file as hex encoded
     *
     * @return string hex code
     */
    private function getFileAsHex() {
        // if file does not exist, show missing image
        if ($this->_file === null) {
            return '89504e470d0a1a0a0000000d49484452000000640000004b0802000000097ab5e00000000774494d4507da061c081a03cdc972b7000000097048597300000b1300000b1301009a9c180000000467414d410000b18f0bfc6105000009674944415478daed9c4b6fdb4614858712255a96ecc491e31470ba898b1449916e8202e9ff078aec8bacd2d459c435ec38366ceb693dd8c339e2d5881269911c5a4aa00b57a146d468e6e3b98f194a757cdf571b5bce4aab1ec0f7641b58296c032b856d60a5b00dac14b68195c2dc550f209745ea1ec7710afdb8ef0c96d0891c0c87c3c16050afd75591c8be0358112ef27877770740c4c4c68ab6e278ad29ac8580c6e3f1c0309e402e8e36e0735dd76cff6161c501a2822822014133df8ea73801e797cbe58246b862580b01d1b360c084c9c7010a4ee6db7ddf2997f1d26834c2f9382888d743c332f397098868c848ce8902f2fd09a0c8e21f6780973e0dbc408a3d58f7c487801517a10510a4940488e29a07647e44d84071954a85d48f45c14a0840e265e6ac4d40fe7280d8123c18afe323b4fefc750ff00901481e6556d1084d4073744c16092dd2adee665cd0269d055811e7523a0091ce52113a5e3edd5eafb6b59500c8991d07df6a8ec4aebe72c1a2e0d56c00ca18a1f5bf636d6eb9fcf7870f171717b55aed8fb76f27a5d3424026c710163b29226c658745c1b7db6d3232279e2142c3be5e5c408d28c19bcde6e5e525921a7afe747cfcebcb974b7ae24cd42b206c6584c5d110963282f4f4d525008db4e1a05aadc26d47c36150588e469d4e677b7bbbdfefe36929ec5a2d11bcf0b1105471612baf56e12318df24586088a311fe144335fed0ee18c64986cffcf00400aa54abe8cad17d61aabf1c1d415978f6f8f163bed1d1679aba9596692ad56a9af46c4489d5c39ac417cc9043955c364b270228dac23963924aedecee9674f15d76dd93931390c2216029e38dce2c1d476a0efde71bb024b1ac052ca52f9d336f7c2d0690394fb752e131a8606edd4e07ca026f042ce4412aebecec6c21a0696f063233b1589795b2523a4c06aa83855a26c787e186412a1844b97cdb6a8df4b20e39f5ecfc9c8b3b3c6db5dbce6c57d3de16b5141ab6b2c38a54de18e25847ebfb73bcb6603e61c2eaf5032be9e36f57570c827c09ed78eff5f535f0ed341af7239b0b5bca5eb5954b591c16af64e041669848ac247d4356f8afd56a95b44f75ba5da445c429b4c8248f3f7fbeb9b9c1c1eb57afbc6a750120a3c5374a53ebd5562e6595b421fd4f83c8bd6ec8ba516b90b3baebf731b15abddeed76c77acf008f2c1d54b84b75707080533dcf9bf6b66099382ddfb99ce6f683c56a2b6fcce2a5134f0cc4751f325f67d2928ee57c8ae3a0ac759c46a3811ae2e6fa7aa44f60b7cf0f0febf5fa3ca085c860887a939e6dc7f88cb0840e6d020ea290c1c5688a93308b58ac69507f4056f4c4c6f676a35e3ffffa95ab6e2acb317b8bd194d282424aad7a5e556b90d583c5b095d7a50516bd72991c8fd32abafe941654f04ff6f6bcb02e45cbd6d616eb2c3c22904d8b10636563d62a20d2eff53aed360e46e1de869add6bcc6f1602bc1326ece8ae4b7c8e47e90912989524449c835a74acd798683163d6eded2d8e11b09c184d61090952c24594b84615fcb48b50598eaebf1d03e254538b5a4a7ae3dc09e70c7d517781b23c0f2aa3b2f0787e7e3ebf93410f6db75abd6e370265540cafecb0a46e9098c5e712e023be33df3209e1a11ea51065b7109778e26038fceff4746ca4b60053bb0dbfe3523c62b2cb68b734b553c10715a94ed54cfc93f6f0e59993d5e214864eb01ec4ccc088f2ac6f6fc3bf081113c6fc519a22b4054e876a2371e937327282b217e3f3c212b1e00a7397ae14ae7b92014d6a77c41aed800030d494a117c995941e6a54a8eb9f4f9f4ebe7cf9fdcd9b6546c53b204eb81768ab34b5a62c5c70f09a96a93180b857418f9b6c19fa3e48dddcdea211f554b55261f18d3510c0414db5ad2d94171f3f7e04825f8e8e20bd25795575b96f7123d002729100b7defb777788b86417d99650e172042cf06a4faffb502560fe8f76779f1d1c800bde889aab52a9ecd4ebd7575770a86089e3fb3f3d7b86b77f393959725403edc2345b612b6fe9a08c3cc89c8d79f6bbdd20ee86c3a58ecaba64c5e4512294c2cdc209e2c1806f470580bfaaa68313f6f7f78328dee9a0b2ffedf5eb6f9797ff1e1fa3a0c752717777375962e8d34c8856c29693933a5d2698671fd3ec412668ec753a43a33214a3d6e4bb1bf2769669b0a13680e39761707c7a7a8a83c3c3432083e8fe7aff1e2e09becd661338fe7cf72e616c8ff6f6f859bc96b27eca6cd6dc9036f9aa81bb58b07a13d48dbc978d8cc1d4209992e0d3a74f710064575757b81efbcd6657ef4c9c9d9d415cc903134ff40dcb33536b3759098b21098b61c82c1bf720336a93cbb0b3b383225ee98470f4e205dcb2f9e4c9cfcf9f435c09fd409bd39b0396b66bf2baa10c0533811be2116e82c6db9b1b3feb2e383a915293be09592142819aebc65edd60c959452ec5bad375b5179beca40ccce389d69425a5a90a2fec9d5edc65304c897b64f450682d2e960b203c46be66340f257f98b7038ba478e9589dba3960a12b008af31a5e09d0a97adebd80a49de2ca19e36d2a8bc8e0359c4b27476ff3a4823e3d2ff032c3c512e8cc77c8ef15e411979d0a5e193951b6563031f3b67e062b07020dc0cb86722a40917646893c61dee6578e26fb7f3af7078ee879196049180221733f231b20d358f7e6f9d2a9e5002f614bbe659d0a9019a7538158e664b9f39ad913ed2b8bd101a5bc805b21a04823f49e27cc5bcb86ca10174a4ad6df0031ea76e5349009c290de6b97adbee43e6d51e3cd312e30324fd3f2774aa538e20e09978a2e2df44a2b01689993b130429d8c91e0e261549eb684b2f6416199610b430c4656a934127f1f921f50443e3d6dc8c808058c09184c556b990ba0b5c886f39e58740012a382b8dd2c80048db4d0f2d4a5f6ddd0097fe0007125ff92260f20740e40c0c4b28e20b87f4f3a926da45a8e6c43ae1e96324a791610194024b41310bf4c4fbd30ed9a0a72e64c19c24ffec495c1c2356780cf0988119a8ea674cdc1d5b5f89400326ff4c67159976c1859f7f4f45d6233a0a6f23b89d3dc3b453fbca72f16712e35271f2b808a82650e8e1ec1ea74493a2afcb9138d4428a2b4806cd1291c96780473a2c08a9b00bd8cbfc850e18e2b6f474782f43ca30700542c2c35abacf93920f0f3c6040e582e9288a757ce91fc25f4d543c9e7e16099618b0b317ef79f68884985991ee7d46ab525f3d7aa00cd7cbaf5aff4ca8f2f4441bcd965de044a883eeb06c8b402dd90c7f2bb52697fc8fc65795e45fcc6c5fc3588d9bf593dafad7c12ac10583faa6dfe5f34296c032b856d60a5b00dac14b68195c236b052d806560adbc04a611b5829ec7f01a476a4eded6b970000000049454e44ae426082';
        }

        $imageData = file_get_contents($this->_file);
        $size = strlen($imageData);

        $hexString = '';

        for ($i = 0; $i < $size; $i++) {
            $hex = dechex(ord($imageData{$i}));

            if (strlen($hex) == 1) {
                $hex = '0' . $hex;
            }

            $hexString .= $hex;
        }

        return $hexString;
    }

    /**
     * Gets rtf code of image
     *
     * @return string rtf code
     */
    public function getContent() {
        $content = '{\pict';

        if ($this->_border) {
            $content .= $this->_border->getContent($this->_rtf);
        }

        $content .= '\picwgoal' . $this->getImageWidth();
        $content .= '\pichgoal' . $this->getImageHeight();

        switch ($this->_extension) {
            case 'jpeg':
            case 'jpg':
                $content .= '\jpegblip ';
                break;

            case 'png':
                $content .= '\pngblip ';
                break;
        }

        $content .= $this->getFileAsHex();
        $content .= '}';
        
        return $content;
    }

    
    //// DEPRECATED FUNCTIONS FOLLOWS HERE ////

    /**
     * @deprecated use setBorder() instead
     * @see PHPRtfLite/PHPRtfLite_Image#setBorder()
     *
     * Sets border
     *
     * @param PHPRtfLite_Border_Format  $borderFormat
     * @param boolean                   $left           if false, left border is not set (default true)
     * @param boolean                   $top            if false, top border is not set (default true)
     * @param boolean                   $right          if false, right border is not set (default true)
     * @param boolean                   $bottom         if false, bottom border is not set (default true)
     * @access public
     */
    public function setBorders(PHPRtfLite_Border_Format $borderFormat,
                               $left = true, $top = true,
                               $right = true, $bottom = true)
    {
        if (!$this->_border) {
            $this->_border = new PHPRtfLite_Border();
        }

        if ($left) {
            $this->_border->setBorderLeft($borderFormat);
        }

        if ($top) {
            $this->_border->setBorderTop($borderFormat);
        }

        if ($right) {
            $this->_border->setBorderRight($borderFormat);
        }

        if ($bottom) {
            $this->_border->setBorderBottom($borderFormat);
        }
    }
    
}