<?php

/**
 * @file
 *  exercise.pages.inc php file
 *  Contains pages for Drupal module exercise.
 */

/**
 * The exercise plan form page
 */
function exercise_plan_form_page() {
  $module_path = drupal_get_path('module', 'exercise');
  $base_path = base_path();
  drupal_add_js($module_path . '/theme/javascripts/json2.min.js');
  $url = explode('?', url('exercise'), 2);
  drupal_add_js(
    array(
      'exercise' => array(
        'url' => $url[0],
        'modulePath' => $base_path . $module_path,
        'weekStart' => variable_get('exercise_week_start', 1),
        'taxonomyImagePath' => $base_path . file_directory_path() . '/' . variable_get('taxonomy_image_path', 'category_pictures') . '/',
        'disableCustomExercise' => variable_get('exercise_disable_custom_exercise', 0),
        'dateFormat' => variable_get('exercise_date_format', 'd.m.y'),
        'examplePeriodicPlan' => variable_get('exercise_example_periodic_plan', ''),
        'exampleWorkoutPlan' => variable_get('exercise_example_workout_plan', ''),
        'jstorageAccessControl' => variable_get('exercise_jstorage_access_control', 0)
      )
    ),
    'setting'
  );
  drupal_add_js($module_path . '/theme/javascripts/exercise.js');
  return drupal_get_form('exercise_plan_form');
}

/**
 * Exercise planner form, used to generate a RTF file for plans.
 */
function exercise_plan_form(&$form_state) {
  // Submit
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Download'
  );

  // JSON reciver
  $form['json'] = array(
    '#type' => 'hidden',
  );

  // Week days
  $week_days = array(
    t('Sunday'),
    t('Monday'),
    t('Tuesday'),
    t('Wednesday'),
    t('Thursday'),
    t('Friday'),
    t('Saturday')
  );

  $week_start = variable_get('exercise_week_start', 1);
  if ($week_start > 0 && $week_start < 7) {
    $week_days = array_merge(array_splice($week_days, $week_start), $week_days);
  }

  $form['week_days']['#value'] = '';
  $week_days_size = count($week_days);
  for ($i = 0; $i < $week_days_size; $i++) {
    $form['week_days']['#value'] .= '<th>' . $week_days[$i] . "</th>";
  }

  return $form;
}

/**
 * Generate RTF file of the submitted plans.
 */
function exercise_plan_form_submit($form, &$form_state) {
  $data = exercise_parse_json($form_state['values']['json']);

  $size = count($data->workouts);
  for ($i = 0; $i < $size; $i++) {
    $date = strtotime($data->workouts[$i]->date);
    $data->workouts[$i]->date = $date;
    if (isset($data->period)) {
      $weeks[date('W', $date)][date('w', $date)][] = $data->workouts[$i]->title;
    }
  }

  // Load RTF lib
  require 'lib/PHPRtfLite.php';

  // Register PHPRtfLite class loader
  PHPRtfLite::registerAutoloader();

  // New RTF
  $rtf = new PHPRtfLite();

  // Paper properties
  $rtf->setLandscape();
  $rtf->setPaperWidth(29);
  $rtf->setPaperHeight(21);
  $rtf->setMargins(1.5, 1.5, 1.5, 1.5);

  // Font
  $times12 = new PHPRtfLite_Font(12, 'Times new Roman');
  $times14 = new PHPRtfLite_Font(14, 'Times new Roman');
  $times16 = new PHPRtfLite_Font(16, 'Times new Roman');
  $center = new PHPRtfLite_ParFormat('center');

  // If we found any content we start filling in the rtf document
  if (isset($weeks)) {
    // Add section(page) for the perodic plan
    $sect = $rtf->addSection();
    $sect->setBorders(new PHPRtfLite_Border_Format(0));

    // Write text    
    $sect->writeText('<b>' . t('Workout schedule') . '</b><br/>', $times16, null);
    $sect->writeText('<b>' . t('Name') . ':</b> <i>' . $data->name . '</i><br/>', $times12, null);
    $sect->writeText('<b>' . t('Objective') . ':</b> <i>' . $data->period->objective . '</i>', $times12, null);

    // Table
    $table = $sect->addTable();
    $table->addColumnsList(array(1.5, 2.6, 2.6, 2.6, 2.6, 2.6, 2.6, 2.6, 5));
    $table->addRows(count($weeks) + 1, 0.5);
    $table->setBordersOfCells(new PHPRtfLite_Border_Format(0.5, '#000', 'single', 0), 1, 1, count($weeks) + 1, 9);

    // Headers
    $week_days = array(
      t('Sunday'),
      t('Monday'),
      t('Tuesday'),
      t('Wednesday'),
      t('Thursday'),
      t('Friday'),
      t('Saturday')
    );

    $week_start = variable_get('exercise_week_start', 1);
    if ($week_start > 0 && $week_start < 7) {
      $week_days = array_merge(array_splice($week_days, $week_start), $week_days);
    }

    $table->writeToCell(1, 1, '<b>' . t('Week') . '</b>', $times12, $center);
    $week_days_size = count($week_days);
    for ($i = 0; $i < $week_days_size; $i++) {
      $table->writeToCell(1, $i + 2, '<b>' . $week_days[$i] . '</b>', $times12, $center);
    }
    $table->writeToCell(1, 9, '<b>' . t('Notes') . '</b>', $times12, $center);

    // Insert data from array
    $i = 0;
    foreach ($weeks as $week => $days) {
      $table->writeToCell($i + 2, 1, $week, $times12, $center);
      for ($day = 0; $day < 7; $day++) {
        $write = '';
        $counter = 0;
        $day_of_week = $day + $week_start;
        if ($day_of_week > 6) {
          $day_of_week -= 7;
        }
        if (isset($days[$day_of_week])) {
          foreach ($days[$day_of_week] as $session) {
            if ($counter > 0) {
              $write .= '<br/><br/>';
            }
            $write .= $session;
            $counter++;
          }
        }
        $table->writeToCell($i + 2, $day + 2, $write, $times12, null);
      }
      $i++;
    }
  }

  // Add exercise plans
  $size = count($data->workouts);
  for ($i = 0; $i < $size; $i++) {
    $plan = $data->workouts[$i];
    $plan->exercises_size = count($plan->exercises);

    // Don't print blank workouts
    if ($plan->exercises_size == '0' && $plan->objective == '') {
      continue;
    }

    // Create new section(page) for the plan
    $sect = $rtf->addSection();
    $sect->setBorders(new PHPRtfLite_Border_Format(0));

    $date = str_replace(array('d', 'm', 'y'), array(date('d', $plan->date), date('m', $plan->date), date('Y', $plan->date)), variable_get('exercise_date_format', 'd.m.y'));

    // Write text.
    $sect->writeText('<b>' . t('Workout plan') . ':</b> ' . $plan->title . '<br/>', $times16, null);
    $sect->writeText('<b>' . t('Name') . ':</b> <i>' . $data->name . '</i><br/>', $times12, null);
    $sect->writeText('<b>' . t('Date') . ':</b> ' . $date . '<br/>', $times12, null);
    $sect->writeText('<b>' . t('Objective') . ':</b> <i>' . $plan->objective . '</i>', $times12, null);

    // Add table
    $table = $sect->addTable();
    $table->addColumnsList(array(3, 3, 3, 7, 5, 4));
    $table->addRows(1, 0.5);
    $table->addRows($plan->exercises_size, 2.5);
    $table->setBordersOfCells(new PHPRtfLite_Border_Format(0.5, '#000', 'single', 0), 1, 1, $plan->exercises_size + 1, 6);
    $table->setVerticalAlignmentOfCells('center', 1, 1, $plan->exercises_size + 1, 5);

    // First headers
    $table->writeToCell(1, 1, '<b>' . t('How long') . '</b>', $times12, $center);
    $table->writeToCell(1, 2, '<b>' . t('What') . '</b>', $times12, $center);
    $table->mergeCells(1, 2, 1, 3);
    $table->writeToCell(1, 4, '<b>' . t('How') . '</b>', $times12, $center);
    $table->writeToCell(1, 5, '<b>' . t('Why') . '</b>', $times12, $center);
    $table->writeToCell(1, 6, '<b>' . t('Notes') . '</b>', $times12, $center);
    
    // Write each exercise from row 2
    for ($j = 0; $j < $plan->exercises_size; $j++) {
      $exercise = $plan->exercises[$j];
      $exercise->image = exercise_remove_base_path($exercise->image);
      $table->writeToCell($j + 2, 2, $exercise->title, $times12);
      $table->addImageToCell($j + 2, 3, $exercise->image, $center, 2.8, 2.1);
      $table->writeToCell($j + 2, 4, $exercise->amount, $times12);
      $table->writeToCell($j + 2, 5, $exercise->why, $times12);
    }
  }

  // Send rtf document
  $rtf->sendRtf(t('exercise_plan.rtf'));
  exit;
}

/**
 * Recursive JSON parser. Inspired by the drupal module json_server.
 * 
 * @param string $json The JSON string to parse.
 * @return array Parsed JSON.
 */
function exercise_parse_json($json) {
  // PHP 5.2 or newer has its own parser.
  if (function_exists('json_decode')) {
    return json_decode($json);
  }

  // Parse the old way
  if ($json{0} == '"') {
    $json = substr($json, 1, -1);
  }
  elseif ($json{0} == '{') {
    $var = explode(",", substr($json, 1, -2));
    $json = array();
    foreach ($var as $value) {
      $va = explode(":", $value);
      $json[$va[0]] = drupal_parse_json($va[1]);
    }
  }
  elseif ($json{0} == '[') {
    $var = explode(",", substr($json, 1, -2));
    $json = array();
    foreach ($var as $value) {
      $json[] = drupal_parse_json($va[0]);
    }
  }
  return $json;
}