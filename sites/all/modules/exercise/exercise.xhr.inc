<?php

/**
 * @file
 *  exercise.xhr.inc php file
 *  Contains XMLHttpRequest responses for Drupal module exercise.
 */

/**
 * Returns a list with all exercises in JSON.
 */
function exercise_xhr_exercises() {
  global $language;

  // Load all exercises with their categories
  $result = db_query("SELECT n.nid, n.title, e.image, e.amount, GROUP_CONCAT(c.tid SEPARATOR ',') as categories FROM {node} n
    JOIN {exercises} e ON n.type = 'exercise' AND n.status = 1 AND n.language IN ('%s', '') AND e.vid = n.vid
    LEFT JOIN (SELECT tn.nid, tn.tid FROM {term_data} td JOIN {term_node} tn ON td.vid = %d AND td.tid = tn.tid) c ON c.nid = n.nid
    GROUP BY n.nid ORDER BY n.title",
      $language->language, variable_get('exercise_category_vocabulary', NULL));

  $exercises = array();
  while ($exercise = db_fetch_object($result)) {
    $tagged_categories = explode(',', $exercise->categories);
    $categories_size = count($tagged_categories);
    $exercise->categories = array();
    for ($i = 0; $i < $categories_size; $i++) {
      $exercise->categories = array_merge($exercise->categories, exercise_find_parents($tagged_categories[$i]));
    }
    $exercise->categories = array_merge($exercise->categories, $tagged_categories);
    $exercise->title = check_plain($exercise->title);
    $exercise->amount = check_plain($exercise->amount);
    $exercise->image = exercise_image_url($exercise->image);
    $exercises[] = $exercise;
  }
  $filters = array();
  exercise_order_filters(exercise_find_parents(0, TRUE), $filters);
  drupal_json(array($filters, $exercises, drupal_get_messages()));
}

/**
 * Recursive function for finding a term's parents. This is a performance
 * enhanced version of taxonomy_get_parents_all(). It loads all the terms from
 * the category vocabulary and translates them(if necessary) in a singel query.
 *
 * @staticvar array $terms All terms cached.
 * @staticvar int $terms_size Number of terms cached.
 * @param int $kid Term id.
 * @param boolean $get_terms Used to return cached terms.
 * @return A term's parents.
 */
function exercise_find_parents($kid, $get_terms = FALSE) {
  static $terms, $terms_size;

  // We need all terms in order to find parents
  if ($terms == NULL) {
    $vid = variable_get('exercise_category_vocabulary', NULL);
    if (module_exists('i18ntaxonomy')) {
      $terms = exercise_get_localized_tree($vid);
    }
    else {
      $terms = taxonomy_get_tree($vid);
    }
    $terms_size = count($terms);
  }

  if ($get_terms) {
    return $terms;
  }

  $parents = array();
  for ($i = 0; $i < $terms_size; $i++) {
    if ($terms[$i]->tid == $kid) {
      $parents_size = count($terms[$i]->parents);
      for ($j = 0; $j < $parents_size; $j++) {
        if ($terms[$i]->parents[$j] != 0) {
          $parents[] = $terms[$i]->parents[$j];
          // Find parent's parents
          $parents = array_merge($parents, exercise_find_parents($terms[$i]->parents[$j]));
        }
      }
    }
  }

  return $parents;
}

/**
 * Recursive function that orders the filters.
 *
 * @param array $terms The terms that's going to be used as filters.
 * @param array $filters The arranged filter terms.
 * @param int $parent Parent, internal use only.
 * @param int $level Current depth in the tree, internal use only.
 */
function exercise_order_filters($terms, &$filters, $parent = 0, $level = 0) {
  foreach ($terms as $term) {
    if ($term->parents[0] == $parent) {
      $filters[$level][] = array(
        'tid' => $term->tid,
        'name' => $term->name,
        'parent' => $parent
      );
      if (variable_get('exercise_disable_filter_hierarchy', 0) != 1) {
        exercise_order_filters($terms, $filters, $term->tid, $level + 1);
      }
    }
  }
}

/**
 * Prints exercise details in JSON for a given node.
 *
 * @param int $nid
 *   Node indentifier
 */
function exercise_xhr_details($nid) {
  $exercise = db_fetch_object(db_query("
    SELECT n.vid, n.title, nr.body, nr.format, e.image, e.illustration, e.illustration_format
    FROM {node} n
    JOIN {node_revisions} nr ON n.nid = %d AND n.vid = nr.vid
    JOIN {exercises} e ON n.vid = e.vid", $nid));
  $exercise->title = check_plain($exercise->title);
  $exercise->body = check_markup($exercise->body, $exercise->format);
  $exercise->illustration = exercise_filter_illustration_tags(check_markup($exercise->illustration, $exercise->illustration_format));
  $exercise->image = exercise_image_url($exercise->image, TRUE);
  $result = db_query("
    SELECT ti.path FROM {term_data} td
    JOIN {term_node} tn ON td.vid = %d AND tn.vid = %d AND td.tid = tn.tid
    JOIN {term_image} ti ON ti.tid = tn.tid", variable_get('exercise_muscle_vocabulary', NULL), $exercise->vid);
  $exercise->muscles = array();
  while ($muscle = db_result($result)) {
    $exercise->muscles[] = $muscle;
  }
  drupal_json($exercise);
}

/**
 * Callback function for the help page
 */
function exercise_help_page($page) {
  print theme('exercise_help_' . $page);
}