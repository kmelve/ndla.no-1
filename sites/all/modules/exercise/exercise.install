<?php

/**
 * @file
 *  exercise.install php file
 *  Installs the Drupal module exercise.
 */

/**
 * Implementation of hook_schema().
 */
function exercise_schema() {
  return array(
    'exercises' => array(
      'description' => t('Contains exercises.'),
      'fields' => array(
        'vid' => array(
          'description' => t('The current version identifier.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'nid' => array(
          'description' => t('Node identifier.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'amount' => array(
          'description' => t('The default value to display in the amount field.'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ),
        'image' => array(
          'description' => t('URL or node ID of an image to associate with the exercise.'),
          'type' => 'varchar',
          'length' => 2047,
          'not null' => FALSE,
          'default' => '',
        ),
        'illustration' => array(
          'description' => t('Illustration of the exercise - could be a flash or a video.'),
          'type' => 'text',
          'size' => 'normal',
          'not null' => FALSE,
          'default' => '',
        ),
        'illustration_format' => array(
          'description' => t('Filter format to use on the exercise illustration.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        )
      ),
      'primary key' => array('vid'),
    ),
  );
}

/**
 * Implementation of hook_install().
 */
function exercise_install() {
  // Enable required modules
  module_enable(array('taxonomy', 'taxonomy_image', 'jstorage'));

  // Install database schema
  drupal_install_schema('exercise');

  // Create a vocabulary with muscles that can be trained.
  $vocabulary = array(
    'name' => 'Exercise muscles',
    'multiple' => 1,
    'required' => 0,
    'hierarchy' => 0,
    'relations' => 0,
    'module' => 'exercise',
    'weight' => -10,
    'nodes' => array('exercise' => 1),
  );

  // Don't add vocabulary if it exists.
  if (!db_result(db_query("SELECT vid FROM {vocabulary} WHERE name = '%s' AND module = '%s'", $vocabulary['name'], $vocabulary['module']))) {
    // Create a directory for muscle images as a sub-directory of the default taxonomy_image folder.
    $directory = file_directory_path() . '/' . variable_get('taxonomy_image_path', 'category_pictures');
    $directories = array(
      $directory,
      $directory . '/exercise',
      $directory . '/exercise/muscles'
    );

    foreach ($directories as $directory) {
      if (!is_dir($directory)) {
        mkdir($directory);
        chmod($directory, 0775);
      }
    }

    if (!is_writable($directory)) {
      drupal_set_message('The exercise module could not create folders in the files directory. Check your file permissions and then reinstall the module.', 'error');
      return false;
    }

    taxonomy_save_vocabulary($vocabulary);
    variable_set('exercise_muscle_vocabulary', $vocabulary['vid']);

    // Localize terms if possible
    if (module_exists('i18ntaxonomy')) {
      i18ntaxonomy_vocabulary($vocabulary['vid'], I18N_TAXONOMY_LOCALIZE);
    }

    // Populate the muscle vocabulary with terms extracted from the muscle images file names.
    $files = scandir(drupal_get_path('module', 'exercise') . '/images/muscles');
    $muscles = array();
    for ($i = 0; $i < count($files); $i++) {
      $file_name_parts = explode('.', $files[$i]);
      if (count($file_name_parts) == 2 && $file_name_parts[1] == 'png') {
        $file_name_parts = explode('-', $file_name_parts[0]);
        $name = '';
        for ($n = 0, $length = count($file_name_parts); $n < $length; $n++) {
          $name = ($name == '' ? '' : ' ') . ucfirst($file_name_parts[$n]);
        }
        $term = array(
          'vid' => $vocabulary['vid'],
          'name' => $name,
        );
        taxonomy_save_term($term);
        copy(drupal_get_path('module', 'exercise') . '/images/muscles/' . $files[$i], $directory . '/' . $files[$i]);
        taxonomy_image_add($term['tid'], 'exercise/muscles/' . $files[$i]);
      }
    }
  }

  // Add a vocabulary to categorize exercises (required for filtering in the exercise library)
  $vocabulary = array(
    'name' => 'Exercise filter',
    'multiple' => 1,
    'required' => 1,
    'hierarchy' => 1,
    'relations' => 0,
    'module' => 'exercise',
    'weight' => -10,
    'nodes' => array('exercise' => 1),
  );

  // Don't add vocabulary if it exists.
  if (!db_result(db_query("SELECT vid FROM {vocabulary} WHERE name = '%s' AND module = '%s'", $vocabulary['name'], $vocabulary['module']))) {
    taxonomy_save_vocabulary($vocabulary);
    variable_set('exercise_category_vocabulary', $vocabulary['vid']);

    // Localize terms if possible
    if (module_exists('i18ntaxonomy')) {
      i18ntaxonomy_vocabulary($vocabulary['vid'], I18N_TAXONOMY_LOCALIZE);
    }

    exercise_add_terms(
      $vocabulary['vid'], array(
      'Warm up' => array(
        'Run' => array(),
        'Run with ball' => array(),
        'Strength, agility and balance drills' => array(),
      ),
      'Endurance' => array(
        'Running' => array(
          'Long distance' => array(),
          'Fast long-run' => array(),
          'Long interval' => array(),
          'Short interval' => array(),
        ),
        'Cycling' => array(
          'Long distance' => array(),
          'Fast long-run' => array(),
          'Long interval' => array(),
          'Short interval' => array(),
        ),
        'Ski' => array(
          'Long distance' => array(),
          'Fast long-run' => array(),
          'Long interval' => array(),
          'Short interval' => array(),
        ),
        'Swimming' => array(
          'Long distance' => array(),
          'Fast long-run' => array(),
          'Long interval' => array(),
          'Short interval' => array(),
        ),
        'Ball games' => array(
          'Speed density' => array(),
        ),
      ),
      'Strenght' => array(
        'Ankle' => array(
          'Calves' => array(),
        ),
        'Knee' => array(
          'Quadriceps' => array(),
          'Hamstrings' => array(),
        ),
        'Hip' => array(
          'Gluteus' => array(),
          'Tensor fasciae latae' => array(),
          'Adductors' => array(),
        ),
        'Belly and back' => array(
          'Abdominal muscles' => array(),
          'Back muscles' => array(),
        ),
        'Chest' => array(
          'Chest muscles' => array(),
        ),
        'Arms' => array(
          'Triceps' => array(),
          'Biceps' => array(),
        ),
        'Shoulders' => array(
          'Deltoids' => array(),
        ),
      ),
      'Movement' => array(
        'Ankle' => array(
          'Calves' => array(),
        ),
        'Knee' => array(
          'Quadriceps' => array(),
          'Hamstrings' => array(),
        ),
        'Hip' => array(
          'Gluteus' => array(),
          'Tensor fasciae latae' => array(),
          'Adductors' => array(),
        ),
        'Belly and back' => array(
          'Abdominal muscles' => array(),
          'Back muscles' => array(),
        ),
        'Chest' => array(
          'Chest muscles' => array(),
        ),
        'Arms' => array(
          'Triceps' => array(),
          'Biceps' => array(),
        ),
        'Shoulders' => array(
          'Deltoids' => array(),
        ),
      ),
      'Speed' => array(
        'Reaction speed and acceleration' => array(),
        'Maximum speed and endurance' => array(),
      ),
      'Jumping' => array(
        'Horizontal jumping' => array(),
        'Vertical jumping' => array(),
      ),
      'Basic training' => array(
        'Coordination exercises' => array(),
        'Stabilization and strengthening exercises' => array(),
      ),
      'Sports' => array(
        'Badminton' => array(
          'Technique exercises' => array(),
          'Play exercises' => array(),
        ),
        'Basketball' => array(
          'Technique exercises' => array(),
          'Play exercises' => array(),
        ),
        'Football' => array(
          'Technique exercises' => array(),
          'Play exercises' => array(),
        ),
        'Athletics' => array(),
        'Frisbee' => array(
          'Technique exercises' => array(),
          'Play exercises' => array(),
        ),
        'Handball' => array(
          'Technique exercises' => array(),
          'Play exercises' => array(),
        ),
        'Floorball' => array(
          'Technique exercises' => array(),
          'Play exercises' => array(),
        ),
        'Volleyball' => array(
          'Technique exercises' => array(),
          'Play exercises' => array(),
        ),
      ),
      )
    );
  }
}

/**
 * Implementation of hook_install().
 */
function exercise_uninstall() {
  drupal_uninstall_schema('exercise');
  variable_del('exercise_category_vocabulary');
  variable_del('exercise_muscle_vocabulary');
  variable_del('exercise_logo');
  variable_del('exercise_disable_filter_hierarchy');
  variable_del('exercise_image_thumbnail_size');
  variable_del('exercise_image_details_size');
  variable_del('exercise_disable_custom_exercise');
  variable_del('exercise_example_periodic_plan');
  variable_del('exercise_example_workout_plan');
}

/**
 * Implementation of hook_update_N().
 */
function exercise_update_6200() {
  return array(
    update_sql("ALTER TABLE {exercises} CHANGE image image VARCHAR(2047)"),
    update_sql("UPDATE {variable} SET name = 'exercise_logo' WHERE name = 'exerciseplan_logo'"),
    update_sql("UPDATE {variable} SET name = 'exercise_disable_custom_exercise' WHERE name = 'exerciseplan_custom_exercise'"),
    update_sql("UPDATE {variable} SET name = 'exercise_example_periodic_plan' WHERE name = 'exerciseplan_perodic_example'"),
    update_sql("UPDATE {variable} SET name = 'exercise_example_workout_plan' WHERE name = 'exerciseplan_workout_example'"),
    update_sql("UPDATE {variable} SET name = 'exercise_category_vocabulary' WHERE name = 'exerciseplan_vocabulary'"),
    update_sql("UPDATE {variable} SET name = 'exercise_disable_filter_hierarchy' WHERE name = 'exerciseplan_disable_filter_hierarchy'"),
    update_sql("UPDATE {variable} SET name = 'exercise_muscle_vocabulary' WHERE name = 'exerciseplan_muscle_vocabulary'"),
    update_sql("UPDATE {variable} SET name = 'exercise_image_thumbnail_size' WHERE name = 'exerciseplan_image_thumbnail_size'"),
    update_sql("UPDATE {variable} SET name = 'exercise_image_details_size' WHERE name = 'exerciseplan_image_details_size'"),
    update_sql("DELETE FROM {variable} WHERE name = 'exerciseplan_skip_first_step'"),
    update_sql("DELETE FROM {system} WHERE name = 'exerciseplan'")
  );
}

/**
 * Implementation of hook_update_N().
 */
function exercise_update_6201() {
  $results = array();
  db_add_field($results, 'exercises', 'illustration_format', array(
    'description' => t('Filter format to use on the exercise illustration.'),
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => FALSE,
  ));
  return $results;
}

/**
 * Implementation of hook_update_N().
 * This update hook will replace old muscle images with new ones.
 */
function exercise_update_6202() {
  $results = array();
  $files = scandir(drupal_get_path('module', 'exercise') . '/images/muscles');
  $path = file_directory_path() . '/' . variable_get('taxonomy_image_path', 'category_pictures');
  $directories = array(
    $path . '/exercise/muscles',
    $path . '/exerciseplan',
    $path . '/exercise'
  );
  $muscle_path = drupal_get_path('module', 'exercise') . '/images/muscles/';
  for ($i = 0, $size = count($files); $i < $size; $i++) {
    $file_name_parts = explode('.', $files[$i]);
    if (count($file_name_parts) == 2 && $file_name_parts[1] == 'png') {
      for ($n = 0, $length = count($directories); $n < $length; $n++) {
        $file = $directories[$n] . '/' . $files[$i];
        if (file_exists($file) && is_writable($file)) {
          break;
        }
        $file = $directories[$n] . '/' . str_replace('-', '_', $files[$i]);
        if (file_exists($file) && is_writable($file)) {
          break;
        }
        $file = NULL;
      }
      if ($file) {
        if (copy($muscle_path . $files[$i], $file)) {
          $results[] = array('success' => TRUE, 'query' => 'Successfully updated muscle image <em>' . $files[$i] . '</em>.');
        }
        else {
          $results[] = array('success' => FALSE, 'query' => 'Could not update muscle image <em>' . $files[$i] . '</em>: File copy failed for reasons unknown.');
        }
      }
      else {
        $results[] = array('success' => FALSE, 'query' => 'Could not update muscle image <em>' . $files[$i] . '</em>: File does not exist or isn\'t writable.');
      }
    }
  }
  return $results;
}

/**
 * Implementation of hook_update_N().
 * Add missing nid field and clean up db.
 */
function exercise_update_6203() {
  $results = array();
  db_add_field($results, 'exercises', 'nid', array(
    'description' => t('Node identifier.'),
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
  ));
  // Clean up exercise records without nid:
  $result = db_query("SELECT e.vid FROM {exercises} e LEFT JOIN {node_revisions} nr ON e.vid = nr.vid WHERE nr.nid IS NULL");
  while($vid = db_result($result)) {
    $vids = ($vids ? $vids . ', ' : '') . $vid;
  }
  if ($vids) {
    db_query("DELETE FROM {exercises} WHERE vid IN (%s)", $vids);
  }
  return $results;
}

/**
 * Add a list of terms to a vocabulary
 *
 * @param int $vocabulary_id
 *  Id of the vocabulary we're adding terms to
 * @param array $terms
 *  Terms to be added to the vocabulary
 * @param int $parent_tid
 *  Term id for the parent term
 */
function exercise_add_terms($vocabulary_id, $terms, $parent_tid = 0) {
  foreach ($terms as $name => $content) {
    $term = array(
      'vid' => $vocabulary_id,
      'name' => $name,
      'parent' => $parent_tid,
    );
    taxonomy_save_term($term);
    if (is_array($content)) {
      exercise_add_terms($vocabulary_id, $content, $term['tid']);
    }
  }
}
