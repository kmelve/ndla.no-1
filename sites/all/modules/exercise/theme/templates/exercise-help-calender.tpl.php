<p><?php print t('Welcome to the exercise planner! Here you can plan a single workout or a period of workouts.') ?></p>
<h5><?php print t('This is how it works') ?></h5>
<h6><?php print t('Add a single workout') ?></h6>
<p><?php print t('To do this you must first find a date in the calender. Press the "Create workout" link that appears when you hover the day. Fill in a suitable name for your workout and press "OK". If you have accidently pressed the wrong date, press the "X" in the upper right corner to abort.') ?></p>
<p><?php print t('Press your newly created workout and then press "Create workout plan" to pick exercises/activities.') ?></p>
<h6><?php print t('Copy workouts') ?></h6>
<p><?php print t('You can copy your workout to any other date. First press your workout and then press "Copy workout". Now you can press on the "Create workout" link on any date and choose "Paste workout".') ?></p>
<h6><?php print t('Periodic plan') ?></h6>
<p><?php print t('To create an exercise period press the button labeled "Create periodic plan". Type in an objective for your period, and select a start date and end date. Press "OK". You can now view the period in the calendar.') ?></p>
<p><?php print t('The next step is to add workouts. See the instructions above under "Add a single workout".') ?></p>
<p><?php print t('When you are done adding exercises you can download your period by pressing the "Download" button.') ?></p>
<p><?php print t('In order to print your period you must first download your periodic plan. When the download is complete you open the file in your favorite text editor and choose print.') ?></p>