<?php
$path = base_path() . drupal_get_path('module', 'exercise');
?>
<div id="start">
  <span><?php print t('You must enable javascript in your browser to view this page.') ?></span>
  <img src="<?php print $path ?>/theme/images/ajax-loader.gif" alt="Loading"/>
</div>
<?php print theme('exercise_login') ?>
<div id="calender">
  <div class="frame">
    <table>
      <thead>
        <tr class="first">
          <?php print drupal_render($form['week_days']) ?>
        </tr>
        <tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
      </thead>
      <tbody>
        <tr><td class="week-start"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td class="week-start"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td class="week-start"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td class="week-start"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td class="week-start"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td class="week-start"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
      </tbody>
    </table>
  </div>
  <div id="calender-buttons">
    <div class="button workout" title="<?php print t('Tip on creating a new workout/warm up plan.') ?>"><div class="start"></div><div class="text"><?php print t('Create workout/warm up plan') ?></div><div class="end"></div></div>
    <div class="button periodic" title="<?php print t('Create a new periodic plan.') ?>"><div class="start"></div><div class="text"><?php print t('Create periodic plan') ?></div><div class="end"></div></div>
    <div class="button download" title="<?php print t('Download a periodic plan.') ?>"><div class="start"></div><div class="text"><?php print t('Download') ?></div><div class="end"></div></div>
  </div>
</div>
<div id="dialog">
  <div class="header"></div>
  <div class="body">
    <div class="close"></div>
    <div id="dialog-buttons">
      <div class="button ok"><div class="start"></div><div class="text"><?php print t('OK') ?></div><div class="end"></div></div>
    </div>
  </div>
  <div class="footer"></div>
</div>
<div id="tip-dialog">
  <div class="header"></div>
  <div class="text"></div>
  <div class="footer"></div>
</div>
<div id="workout" class="page">
  <div class="frame">
    <p><?php print t("Here you can create a workout/warm up plan. You have already given your workout a name and a date. What you need to do now is fill in an objective and start adding exercises to your plan.") ?></p>
    <div class="table-top"><div class="objective"><?php print t('Objective for the workout') ?>: <input type="text"/> <span class="tip"><?php print t('Tip') ?></span><span class="tip-content"><?php print t('Improve shooting technique in basketball.') ?><br/><?php print t('Improve aerobic endurance.') ?></span></div></div>
    <div class="table-wrap">
    <table>
      <thead>
        <tr class="first">
          <th colspan="3" class="what no-border-left"><?php print t('What') ?> <span class="tip"><?php print t('Tip') ?></span><span class="tip-content"><?php print t('Exercise/activity') ?></span></th>
          <th class="how"><?php print t('How') ?> <span class="tip"><?php print t('Tip') ?></span><span class="tip-content"><?php print t('Description of the exercise/activity; execution, method, number of repetitions, series, pauses, intensity etc.') ?></span></th>
          <th class="why"><?php print t('Why') ?> <span class="tip"><?php print t('Tip') ?></span><span class="tip-content"><?php print t('Here you can justify your choice of exercises, e.g. balanse exercise to get better at skateboarding.') ?></span></th>
          <th colspan="2" class="edit no-border-right"><?php print t('Edit') ?></th>
        </tr>
      </thead>
      <tfoot>
        <tr class="last">
          <td colspan="8"><span class="pick-exercises"><?php print t('Pick exercises/activities') ?></span></td>
        </tr>
      </tfoot>
      <tbody>
        <tr class="no-exercises">
          <td colspan="8" class="no-border-left no-border-right"><div class="loading"><div class="bar"></div></div></td>
        </tr>
      </tbody>
    </table>
    </div>
  </div>
  <div id="workout-buttons">
    <div class="content">
      <div class="button cancel" title="<?php print t('Go back to the calender without saving.') ?>"><div class="start"></div><div class="text"><?php print t('Back') ?></div><div class="end"></div></div>
      <div class="button save" title="<?php print t('Save the workout plan and go back to the calender.') ?>"><div class="start"></div><div class="text"><?php print t('Save') ?></div><div class="end"></div></div>
      <div class="button download" title="<?php print t('Download the workout plan.') ?>"><div class="start"></div><div class="text"><?php print t('Download') ?></div><div class="end"></div></div>
    </div>
  </div>
</div>
<div id="library" class="page">
  <div class="frame">
    <div id="library-filters">
      <table><tr><th><?php print t('Choose') ?>:</th><td></td></tr></table>
    </div>
    <div id="library-exercises">
      <div class="loading"><div class="bar"></div></div>
    </div>
  </div>
  <div id="library-buttons">
    <div class="content">
      <div class="button cancel" title="<?php print t('Go back without inserting any exercises.') ?>"><div class="start"></div><div class="text"><?php print t('Back') ?></div><div class="end"></div></div>
      <div class="button insert" title="<?php print t('Insert the selected exercises and go back.') ?>"><div class="start"></div><div class="text"><?php print t('Insert') ?></div><div class="end"></div></div>
    </div>
  </div>
</div>
<?php print drupal_render($form) ?>
