
/**
 * @file
 *  exercise-node-form.js javascript file
 *  The javascript file for the exercise node form.
 */

var Exercise = Exercise || {};
Exercise.image = Exercise.image || {};

/**
 * Initialize image field.
 */
Exercise.image.initialize = function() {
  Exercise.image.preload(Drupal.settings.exercise.moduleUrl + '/theme/images/loading.gif');
  Exercise.image.$field = $('#edit-exercise-image').change(Exercise.image.change);
  Exercise.image.$preview = $('<img src="' + Drupal.settings.exercise.moduleUrl + '/theme/images/exercise-default.png" style="vertical-align: middle" alt="' + Drupal.t('Exercise image') + '" width="100" height="75"/>').error(Exercise.image.error).insertBefore(Exercise.image.$field);
  Exercise.image.$message = $('<span style="color: #f00; vertical-align: middle; display: none">' + Drupal.t('Image not found!') + '</span>').insertAfter(Exercise.image.$preview);
  $('<br/>').insertAfter(Exercise.image.$message);
  Exercise.image.$field.change();
}

/**
 * Image changed.
 */
Exercise.image.change = function() {
  Exercise.image.$message.hide();
  Exercise.image.$preview.attr('src', Drupal.settings.exercise.moduleUrl + '/theme/images/loading.gif')
  var image = Exercise.image.$field.val();
  if (image == '') {
    Exercise.image.$preview.attr('src', Drupal.settings.exercise.moduleUrl + '/theme/images/exercise-default.png');
  }
  else if (image.match(/^\d+$/)) {
    Exercise.image.$preview.attr('src', Drupal.settings.exercise.viewImageUrl + '/' + image + '/' + Drupal.settings.exercise.thumbnailSize);
  }
  else {
    Exercise.image.$preview.attr('src', image);
  }
}

/**
 * Image could not be loaded.
 */
Exercise.image.error = function() {
  $(this).attr('src', Drupal.settings.exercise.moduleUrl + '/theme/images/exercise-default.png');
  Exercise.image.$message.show();
}

/**
 * Preload image.
 */
Exercise.image.preload = function(image) {
  var preload = new Image();
  preload.src = image;
}

$(document).ready(Exercise.image.initialize);