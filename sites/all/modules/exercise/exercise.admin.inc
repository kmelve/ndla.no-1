<?php

/**
 * @file
 *  exercise.pages.inc php file
 *  Contains settings page for Drupal module exercise.
 */

/**
 * Implementation of hook_admin().
 */
function exercise_admin() {
  $form['exercise_logo'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo'),
    '#default_value' => variable_get('exercise_logo', ''),
    '#description' => t('URL for the logo to display at the top of the exercise planner. Maximum resolution is 100 x 45 pixels.'),
  );

  $form['exercise_disable_custom_exercise'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove the "Create exercise" exercise'),
    '#default_value' => variable_get('exercise_disable_custom_exercise', 0),
    '#description' => t('Disable the "Create exercise" exercise in the exercise library.'),
  );

  $form['exercise_jstorage_access_control'] = array(
    '#type' => 'checkbox',
    '#title' => t("Logged in users only"),
    '#default_value' => variable_get('exercise_jstorage_access_control', 0),
    '#description' => t("Use JStorage to determine if a user is logged in or not. (Don't use this as access control, only to customise the ux)"),
  );
  
  $form['exercise_allow_anonymous_users'] = array(
    '#type' => 'checkbox',
    '#title' => t("Allow anonymous users"),
    '#default_value' => variable_get('exercise_allow_anonymous_users', 0),
    '#description' => t('Allow anonymous users to "test" the exercise planner before registering. Nothing is saved.'),
  );

  $form['exercise_image_nodes_only'] = array(
    '#type' => 'checkbox',
    '#title' => t("Only use image nodes for exercise images"),
    '#default_value' => variable_get('exercise_image_nodes_only', 0),
    '#description' => t("Don't allow remote images in the exercises. If not set, allow_url_fopen needs to be enabled in php.ini for remote images to work."),
  );

  $form['exercise_week_start'] = array(
    '#type' => 'select',
    '#title' => t('Week start'),
    '#options' => array(
      t('Sunday'),
      t('Monday'),
      t('Tuesday'),
      t('Wednesday'),
      t('Thursday'),
      t('Friday'),
      t('Saturday')
    ),
    '#default_value' => variable_get('exercise_week_start', 1),
    '#description' => t('The first day of the week.'),
  );

  $form['exercise_date_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Date format'),
    '#default_value' => variable_get('exercise_date_format', 'd.m.y'),
    '#description' => t('The date format to use in the exercise planner. Available variables are: d, m and y.')
  );

  // Example plans
  $form['exercise_example_plans'] = array(
    '#type' => 'fieldset',
    '#title' => t('Example plans'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['exercise_example_plans']['exercise_example_periodic_plan'] = array(
    '#type' => 'textfield',
    '#title' => t('Periodic plan example'),
    '#default_value' => variable_get('exercise_example_periodic_plan', ''),
    '#description' => t('URL to an image that will be used as an example for the periodic plan.'),
  );

  $form['exercise_example_plans']['exercise_example_workout_plan'] = array(
    '#type' => 'textfield',
    '#title' => t('Workout plan example'),
    '#default_value' => variable_get('exercise_example_workout_plan', ''),
    '#description' => t('URL to an image that will be used as an example for the workout plan.'),
  );

  // Vocabularies
  $form['exercise_vocabularies'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vocabularies'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $all_vocabularies = taxonomy_get_vocabularies('exercise');
  $vocabularies = array();
  foreach ($all_vocabularies as $vocabulary) {
    $vocabularies[$vocabulary->vid] = $vocabulary->name;
  }
  $form['exercise_vocabularies']['exercise_category_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Filtering vocabulary'),
    '#default_value' => variable_get('exercise_category_vocabulary', NULL),
    '#options' => $vocabularies,
    '#description' => t("A vocabulary that is used to sort exercises."),
  );

  $form['exercise_vocabularies']['exercise_disable_filter_hierarchy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable filter hierarchy'),
    '#default_value' => variable_get('exercise_disable_filter_hierarchy', 0),
  );

  $form['exercise_vocabularies']['exercise_muscle_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Muscle vocabulary'),
    '#default_value' => variable_get('exercise_muscle_vocabulary', NULL),
    '#options' => $vocabularies,
    '#description' => t("The exercise module will be looking for term images related to a node in this vocabulary."),
  );

  if (module_exists('image')) {
    // IMAGES
    $form['exercise_image_sizes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Image node sizes'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );
    $image_sizes = image_get_sizes();
    foreach ($image_sizes as $key => $image_size) {
      $image_sizes[$key] = $image_size['label'];
    }
    $form['exercise_image_sizes']['exercise_image_thumbnail_size'] = array(
      '#type' => 'select',
      '#title' => t('Thumbnail size of image nodes'),
      '#default_value' => variable_get('exercise_image_thumbnail_size', 'thumbnail'),
      '#options' => $image_sizes,
      '#description' => t("Size of the image nodes to use on thumbnail images. Recommended size is 100 x 75 pixels."),
    );
    $form['exercise_image_sizes']['exercise_image_details_size'] = array(
      '#type' => 'select',
      '#title' => t('Size of image nodes in exercise details'),
      '#default_value' => variable_get('exercise_image_details_size', 'preview'),
      '#options' => $image_sizes,
      '#description' => t("Size of the image nodes to use in the exercise details dialog. Recommended size is 300x240 pixels."),
    );
  }

  return system_settings_form($form);
}