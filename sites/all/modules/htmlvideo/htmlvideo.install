<?php
/**
 * Implementation of hook_schema().
 */
function htmlvideo_schema() {
  $schema['htmlvideo'] = array(
    'description' => 'Stores video files information.',
    'fields' => array(
      'nid' => array(
        'description' => 'Primary Key: The {node}.nid of the HTML5Video node.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'htmlvideo_type' => array(
        'description' => 'The type of file, i.e. mp4, ogv...',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'fid' => array(
        'description' => 'Index: The {files}.fid of the mp4 file.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'external_url' => array(
        'description' => 'External url.',
        'type' => 'varchar',
        'length' => 511,
        'not null' => TRUE,
        'default' => '',
      ),
      'htmlvideo_width' => array(
        'description' => 'The width to be used when this video is embedded',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'htmlvideo_height' => array(
        'description' => 'The height to be used when this video is embedded',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('nid', 'htmlvideo_type'),
  );
  return $schema;
}

/**
 * Implementation of hook_install().
 */
function htmlvideo_install() {
  drupal_install_schema('htmlvideo');
}

/**
 * Implementation of hook_uninstall().
 */
function htmlvideo_uninstall() {
  drupal_uninstall_schema('htmlvideo');
  variable_del('htmlvideo_default_path');
}

/**
 * Implementation of hook_requirements().
 */
function htmlvideo_requirements($phase) {
  $requirements = array();

  if ($phase == 'runtime') {
    // File paths
    $htmlvideo_path = file_create_path(file_directory_path() .'/'. variable_get('htmlvideo_default_path', 'htmlvideos'));
    $temp_path = $htmlvideo_path . '/temp';
    if (!file_check_directory($htmlvideo_path, FILE_CREATE_DIRECTORY)) {
      $requirements['htmlvideo_dirs'] = array(
        'value' => t('Missing directory.'),
        'severity' => REQUIREMENT_ERROR,
        'description' => t("The HTML5 video module's video directory %htmlvideo_path is missing.", array('%htmlvideo_path' => $htmlvideo_path)),
      );
    }
    else if (!file_check_directory($temp_path, FILE_CREATE_DIRECTORY)) {
      $requirements['htmlvideo_dirs'] = array(
        'value' => t('Missing temp directory.'),
        'severity' => REQUIREMENT_ERROR,
        'description' => t("The HTML5 video module's temp directory %temp_dir is missing.", array('%temp_dir' => $temp_path)),
      );
    }
    else {
      $requirements['htmlvideo_dirs'] = array(
        'value' => t('Exists (%path).', array('%path' => $htmlvideo_path)),
        'severity' => REQUIREMENT_OK,
      );
    }
    $requirements['htmlvideo_dirs']['title'] = t('HTML Video module directories');
  }

  return $requirements;
}