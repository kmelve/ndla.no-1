<?php
/**
 * Menu callback; Form builder function for settings.
 */
function htmlvideo_admin_settings() {
  $form['htmlvideo_default_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Default video path'),
    '#default_value' => variable_get('htmlvideo_default_path', 'htmlvideos'),
    '#description' => t('Subdirectory in the directory %dir where files will be stored. Do not include trailing slash.', array('%dir' => file_directory_path())),
  );

  // Make changes to the settings before passing them off to
  // system_settings_form_submit().
  $form['#submit'][] = 'htmlvideo_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Form validation handler for admin settings form.
 */
function htmlvideo_admin_settings_validate($form, &$form_state) {
  // Try to create directories and warn the user of errors.
  $htmlvideo_default_path = $form_state['values']['htmlvideo_default_path'];
  $path = file_create_path(file_directory_path() . '/' . $htmlvideo_default_path);
  $temp_path = $path . '/temp';

  if (!file_check_directory($path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS, 'htmlvideo_default_path')) {
    form_set_error('htmlvideo_default_path', t('You have specified an invalid directory.'));
  }
  if (!file_check_directory($temp_path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS, 'htmlvideo_default_path')) {
    form_set_error('htmlvideo_default_path', t('You have specified an invalid directory.'));
  }
}

/**
 * Form submit handler for htmlvideo admin settings form.
 */
function htmlvideo_admin_settings_submit($form, &$form_state) {
  // Ensure that 'htmlvideo_default_path' variable contains no trailing slash.
  $form_state['values']['htmlvideo_default_path'] = rtrim($form_state['values']['htmlvideo_default_path'], '/');
}