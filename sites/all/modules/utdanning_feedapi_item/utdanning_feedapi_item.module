<?php
/**
 * ($Id modulename, version, date, filled inn by svn)
 *
 *   @file
 * 
 *   @brief
 *   Stores feed items in the database in the same way as the core Feed 
 *   module instead for as nodes as in the default implementation of FeedApi.
 *
 * 
 *   Longer description of this module
 *
 *   @author Max-Erik Hansen
 * 
 *
 *   @version 6.x-1.0-dev
 *   @todo
 *   @warning
 *
 *	 @license
 *	 Copyright (C) Utdanning.no
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LISENCE.txt file or the
 *   GNU General Public License for more details.
 * 
 */



/**
 * Implementation of hook_block().
 *
 * @param string $op
 *   What kind of information to retrieve about the block or blocks.
 * @param string/int $delta
 *   Which block to return (not applicable if $op is 'list').
 * @param array $edit
 *   If $op is 'save', the submitted form data from the configuration form.
 * @return array
 * 
 * @see
 *   http://api.drupal.org/api/function/hook_block/6
 */
function utdanning_feedapi_item_block($op = 'list', $delta = 0, $edit = array()) {
	switch ($op) {
		case 'list':
			$blocks[0]['info'] = t('Feeds related to node');
			$blocks[0]['cache'] = BLOCK_NO_CACHE;
			return $blocks;
		case 'configure':
			$form['ufi_number_items'] = array(
				'#type' => 'select',
				'#title' => 'Number of items to show',
				'#options' => array(5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30),
				'#default_value' => variable_get('ufi_number_items', 10),
			);
			return $form;
		case 'save':
			variable_set('ufi_number_items', (int)$edit['ufi_number_items']);
			break;
		case 'view':
			if (arg(0) == 'node' && is_numeric(arg(1))) {

				if (arg(2) == 'revisions' && is_numeric(arg(3))) {
					$node = node_load(arg(1), arg(3));
					$node = node_prepare($node, $teaser);
					$node = node_build_content($node, false, true);
				}
				else {
					$node = node_load(arg(1));
					$node = node_prepare($node, $teaser);
					$node = node_build_content($node, false, true);
					if(function_exists(publishing_queue_load_most_recent_revision)) {
						publishing_queue_load_most_recent_revision($node, FALSE, TRUE);
					}
				}
				
				if (!node_access('view', $node)) {
					return '';
				}
				
				//return _utdanning_feedapi_item_load($node, false, true);
				$block['subject'] = 'Feeds';
				$block['content'] = '';

				if (is_array($node->field_feeds)) {
					foreach ($node->field_feeds as $feed) {
						$feed_node = node_load($feed['nid']);
						$rss = _utdanning_feedapi_item_load($feed_node, false, true, variable_get('ufi_number_items', 10));
						if ($rss != '') {
							$block['content'] .= '<h3>' . $feed_node->title . '</h3>';
							$block['content'] .= $rss;
						}
					}
				}
				
				return $block;
			}
	}
}

/**
 * @brief
 *   Implementation of hook_theme().
 *
 * @return array
 *   A keyed array of theme hooks.
 * 
 * @see
 *   http://api.drupal.org/api/function/hook_theme/6
 */
function utdanning_feedapi_item_theme() {
	return array(
		'utdanning_feedapi_item' => array(
			'arguments' => array('items' => NULL),
			'template' => 'utdanning-feedapi-item',
		),
	);
}

/**
 * @brief
 *   Implementation of hook_nodeapi().
 *
 * @param object $node
 *   The node the action is being performed on.
 * @param string $op
 *   What kind of action is being performed.
 * @param bool $teaser
 *   - For "view", passes in the $teaser parameter from node_view().
 *   - For "validate", passes in the $form parameter from node_validate(). 
 * @param bool $page
 *   For "view", passes in the $page parameter from node_view(). 
 * 
 * @see
 *   http://api.drupal.org/api/function/hook_nodeapi/6
 */
function utdanning_feedapi_item_nodeapi(&$node, $op, $teaser, $page) {
	if ($page) {
		if ($op == 'view') {
			$rss = _utdanning_feedapi_item_load($node, $teaser, $page);
			if ($rss != '') {
				$node->content['feed_items'] = array(
					'#value' => $rss,
					'#weight' => 10,
				);
			}
		}
	}
}

/**
 * @brief
 *   Implementation of hook_feedapi_item().
 * 
 * Timestamp of each feed_item will be parsed from the URL
 * instead of being taken from the timestamp field.
 *
 * @param string $op
 *   The operation that will be performed on the feed item.
 * @return unknown
 */
function utdanning_feedapi_item_feedapi_item($op, $a2, $a3) {
	if ($op != 'expire') {
		$args = func_get_args();
		$item = $args[1]->rdf;
		if (is_array($item)) {
			$item->title = $item->rss_title[0];
			$item->link = $item->rss_link[0];
			$item->description = $item->rss_description[0];
			$item->timestamp = $args[1]->options->timestamp;
		}
		else {
			$item = $args[1];
			if ($item->link == '') $item->link = $item->options->original_url;
			if ($item->timestamp == '') $item->timestamp = $item->options->timestamp;
		}
	}
	
	if ($op == 'save' || $op == 'update' || $op == 'delete' || $op == 'unique') {
	  $nid = $a3;
	}
	else {
	  $nid = $a2->nid;
	}
	
	switch ($op) {
		case 'type':
			// Return
			return array("XML feed");
		case 'update':
			$sql = "UPDATE {uno_feedapi_item} SET title='%s', link='%s', description='%s', timestamp=%d WHERE fid=%d AND guid='%s'";
			db_query($sql, $item->title, $item->link, $item->description, $item->timestamp, fid, $item->options->guid);
			break;
		case 'save':
			$sql = "INSERT INTO {uno_feedapi_item}(fid, title, link, description, timestamp, guid) VALUES(%d, '%s', '%s', '%s', %d, '%s')";
			db_query($sql, $nid, $item->title, $item->link, $item->description, $item->timestamp, $item->options->guid);
			break;
		case 'expire':
		case 'delete':
			$sql = "DELETE FROM {uno_feedapi_item} WHERE fid=%d AND guid='%s'";
			db_query($sql, $nid, $item->options->guid);
			break;
		case 'load':
			break;
		case 'unique':
			$sql = "SELECT COUNT(iid) as count FROM {uno_feedapi_item} WHERE fid=%d AND link='%s'";
			$result = db_query($sql, $nid, $item->link);
			$info = db_fetch_object($result);
			return $info->count == 0;
		case 'fetch':
			break;
		case 'purge':
			break;
		default:
			if (function_exists('feedapi_node'. $op)) {
				$args = array_slice(func_get_args(), 1);
				return call_user_func_array('feedapi_node'. $op, $args);
			}
	}
}

/**
 * @brief
 *   Load feed items for one RSS feed.
 *
 * @param object $node
 *   The feed node.
 * @param bool $teaser
 *   Value is true when the node is shown as teaser.
 * @param bool $page
 *   Value is true when the node is shown as page.
 * @param int $num
 *   Number of items to load.
 * @return string
 *   Generated HTML code for the feed items.
 */
function _utdanning_feedapi_item_load($node, $teaser, $page, $num = -1) {
	if(!is_numeric($node->nid)) {
	  return "";
	}
	
	$sql = "SELECT title, link, description, timestamp FROM {uno_feedapi_item} WHERE fid=%d ORDER BY timestamp DESC";
	if ($num != -1) {
		$sql .= " LIMIT 0, $num";
	}
	$result = db_query($sql, $node->nid);
	$rss = array();
	while ($item = db_fetch_object($result)) {
		$rss[] = $item;
	}

	if (sizeof($rss) != 0) {
		return theme('utdanning_feedapi_item', $rss);
	}
	else {
		return '';
	}
}
?>
