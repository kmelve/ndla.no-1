<?php
/**
 * Functions shared between NDLA / Deling / Nygiv
 *
 */

function ndla_ord_common_init() {
  drupal_add_css(drupal_get_path('module', 'ndla_ord') . '/css/ndla_ord_disp.css','theme');
}

function ndla_ord_solr_setup() {
  $boosts = array(
    0 => t('No boost'),
    10 => 10,
    25 => 25,
    50 => 50,
    75 => 75,
    100 => 100,
    125 => 125,
    150 => 150,
    200 => 200,
  );
  
  $form = array(
    'ndla_ord_search_solr' => array(
      '#title' => t('Search the keyword names'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('ndla_ord_search_solr', 0),
    ),
    'ndla_ord_boost_keyword_names' => array(
      '#title' => t('Amount of boost to give hits on keywords'),
      '#type' => 'select',
      '#options' => $boosts,
      '#default_value' => variable_get('ndla_ord_boost_keyword_names', 0),
    ),
  );

  return system_settings_form($form);
}

function ndla_ord_get_node_keywords($node) {
  $words = array();
  $topic_ids = array();  

  if(!empty($node->keyword_formdata->keyword)) {
    foreach($node->keyword_formdata->keyword as $topic) {
      $topic_ids[] = $topic->topicId;
      foreach($topic->names as $word_data) {
        foreach($word_data->data as $element) {
          foreach($element as $lang_psi => $clear_text_word) {
            $words[] = $clear_text_word;
          }
        }
      }
    }
  }
  
  $words = array_unique($words);
  return array('ids' => $topic_ids, 'keywords' => $words);
}

/**
 * Creates the proper name for the topics field.
 *  @return
 *  String.
 */
function ndla_ord_keywords_solr_key() {
  $keywords = array(
    'name'       => 'keywords',
    'multiple'   => TRUE,
    'index_type' => 'text',
  );

  return apachesolr_index_key($keywords);
}

/**
 * Creates the proper name for the topics field.
 *  @return
 *  String.
 */
function ndla_ord_keywords_ids_solr_key() {
  $keywords_ids = array(
    'name'       => 'keywords_ids',
    'multiple'   => TRUE,
    'index_type' => 'string',
  );

  return apachesolr_index_key($keywords_ids);
}

/**
 * Implementation of hook_apachesolr_update_index
 */
function ndla_ord_apachesolr_update_index(&$document, $node) {
  if (ndla_ord_is_type_enabled($node->type)) {
    _ndla_ord_load($node, TRUE);

    $keywords = ndla_ord_get_node_keywords($node);

    if(count($keywords['ids'])) {
      //Index the topic ids
      $document->setField(ndla_ord_keywords_ids_solr_key(), $keywords['ids']);
      $document->setField(ndla_ord_keywords_solr_key(), $keywords['keywords']);
    }
  }
}

function ndla_ord_apachesolr_modify_query(&$query, &$params, $caller) {
  if(variable_get('ndla_ord_search_solr', 0)) {
    $params['qf'][] = ndla_ord_keywords_ids_solr_key();
    $keywords_field = ndla_ord_keywords_solr_key();
    $boost = variable_get('ndla_ord_boost_keyword_names', 0);
    $boost = ($boost > 0) ? '^' . $boost : '';
    $params['qf'][] = $keywords_field . $boost;
  }
}

function ndla_ord_is_type_enabled($type) {
  $enabled_types = array_filter(variable_get('ndla_ord_enabled_content_types', array()));  
  return in_array($type, $enabled_types);
}

function ndla_ord_admin_content_types_settings() {
  $options = node_get_types('names');
  $form = array();
  $form['ndla_ord_enabled_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled content types'),
    '#options' => $options,
    '#default_value' => variable_get('ndla_ord_enabled_content_types', array()),
  );
  
  return system_settings_form($form);
}