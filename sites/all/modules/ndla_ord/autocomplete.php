<?php

$limit = 9999;

chdir('../../../../');
include_once "includes/bootstrap.inc";
include_once "includes/common.inc";

drupal_bootstrap(DRUPAL_BOOTSTRAP_LATE_PAGE_CACHE);

$search_domain = variable_get('ndla_ord_search_service_url', 'http://search.ndla.no');
$search = rawurlencode($_GET['search']);
if ($search == '') {
	print json_encode(array());
	exit;
}

$languages = (isset($_GET['lang']) ? $_GET['lang'] : '');
$lang_str = '';
if (is_array($languages)) {
	foreach ($languages as $lang) {
		$lang_str .= 'language[]=' . $lang . '&';
	}
} else if ($languages) {
	$lang_str = 'language=' . $languages . '&';
}

$search_url = $search_domain . '/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/' . rawurlencode($_GET['search']) . '/?' . $lang_str . 'type=start&limit=' . $limit;
$json = file_get_contents($search_url);
$results = json_decode($json);
$mresults = array();
$counter = 0;
foreach ($results as $object) {
	$row = array();
	$counter++;

  if($object->wordclass == null) {
    $row['wordclass'] = 'noun';
  } else {
    $row['wordclass'] = $object->wordclass;
  }
  
  if($object->visible == null) {
    $row['visible'] = '1';
  } else {
  	$row['visible'] = $object->visible;
  }
  
  $row['id'] = $object->id;

  $types = array();
  if (isset($object->type_id) && $object->type_id) {
  	if (!is_array($object->type_id)) $object->type_id = array($object->type_id);
  	foreach ($object->type_id as $type) {
  		$type_tmp = json_decode($type);
  		if (isset($type_tmp->type_id) && $type_tmp->type_id) {
  			$type = new stdClass();
  			$type->type_id = $type_tmp->type_id;
  			$type->names = array();
  			if (isset($type_tmp->names)) {
	  			foreach ($type_tmp->names as $name) {
	  				foreach ($name as $lang => $langname) break;
	  				$type->names[$lang] = $langname;
	  			}
	  		}
  			$types[] = $type;
  		}
  	}
  }

  $row['types'] = $types;

  $names = array();
  $row['title'] = FALSE;
  foreach ($object as $key => $value) {
  	if (strpos($key, 'title_') === 0) {
  		$row[$key] = $value;
  		$lang = substr($key, 6);
  		if ($lang == 'nob') $row['title'] = $value;
  	}
  }
  if ($row['title'] === FALSE) $row['title'] = $value;

  $mresults[] = $row;
  if ($counter == $limit) break;
}

print_r(json_encode($mresults));