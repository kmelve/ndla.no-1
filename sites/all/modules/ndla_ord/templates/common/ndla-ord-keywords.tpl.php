<ul> 
  <?php for ($i = 0, $s = count($keywords); $i < $s; $i++): ?> 
    <li class="<?php if ($i == 0) print 'first' ?><?php if ($i + 1 == $s) print ' last' ?>">
      <?php print $keywords[$i]; ?>
    </li> 
  <?php endfor ?> 
</ul> 
<div style='clear: both;'></div>
