<?php
// $Id$  ndla_ord_admin.inc, v 1.0 2012/11/20 14:45 minimalismore Exp $
/** \addtogroup ndla_ord */

/**
 * @file
 * @ingroup ndla_ord
 * @brief
 *  Provides ndla_ord_admin
 */


function ndla_ord_admin_module_settings(){
  $form = array();

  $form['ndla_ord_search_service_url'] = array(
      '#title' => t('Base url of the search server'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_ord_search_service_url', ''),
      '#description' => t('Enter the search service url for this installation i.e http://search.ndla.no. Omit trailing slash'),
  );

  $form['ndla_ord_topic_service_url'] = array(
      '#title' => t('Base url of the topic server'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_ord_topic_service_url',  ''),
      '#description' => t('Enter the topic service url for this installation i.e http://topics.ndla.no. Omit trailing slash'),
  );
  
  $form['ndla_ord_topic_site_string'] = array(
      '#title' => t('Site argument for the topic server'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_ord_topic_site_string',  ''),
      '#description' => t('Enter the site argument for this installation i.e ndla'),
  );

  $form['ndla_ord_topic_validate_keywords'] = array(
  '#type' => 'radios',
  '#title' => t('Obligatory keywords?'),
  '#required' => TRUE,
  '#default_value' => variable_get('ndla_ord_topic_validate_keywords',  1),
  '#options' => array(t("No"),t("Yes")),
  );

  $form['ndla_ord_topic_writeable'] = array(
    '#type' => 'radios',
    '#title' => t('Write access to api.topics.ndla.no'),
    '#required' => TRUE,
    '#default_value' => variable_get('ndla_ord_topic_writeable',  1),
    '#options' => array(t("No"),t("Yes")),
  );

  $form['ndla_ord_topic_readable'] = array(
    '#type' => 'radios',
    '#title' => t('Read access to api.topics.ndla.no'),
    '#required' => TRUE,
    '#default_value' => variable_get('ndla_ord_topic_readable',  1),
    '#options' => array(t("No"),t("Yes")),
  );

  $form['ndla_ord_topic_errorloggable'] = array(
    '#type' => 'radios',
    '#title' => t('Log errors from api.topics.ndla.no?'),
    '#required' => FALSE,
    '#default_value' => variable_get('ndla_ord_topic_errorloggable',  1),
    '#options' => array(t("No"),t("Yes")),
  );


  
  $form['submit']=array(
      '#type' => 'submit',
      '#value' => t('Save')
  );
  
  return $form;
}

function ndla_ord_admin_language_settings(){
  $form = array();


  $languages = ndla_ord_get_languages("nob");
  $mapvars =  variable_get('ndla_ord_topic_site_language_mappings',$out);
  $sitemapping = $mapvars['sitemapping'];


  $form['language_setting'] = array(
    '#title' => t('Language Settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
    '#attributes' => array("id" => "langsettings")
  );

  $form['language_setting']['ndla_ord_topic_site_languages'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose languages used for keywords on the site'),
    '#required' => FALSE,
    '#default_value' => variable_get('ndla_ord_topic_site_languages',  array()),
    '#options' => $languages,
    '#prefix' => '<table><tr><td>',
    '#suffix' => '</td>',
  );

  $form['language_setting']['ndla_ord_topic_site_obligatory_language'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose obligatory keyword language for the site'),
    '#required' => FALSE,
    '#default_value' => variable_get('ndla_ord_topic_site_obligatory_language',  array()),
    '#options' => $languages,
    '#prefix' => '<td>',
    '#suffix' => '</td></tr></table>',
  );

  $form['language_setting']['submit']=array(
    '#type' => 'submit',
    '#value' => t('Save language settings'),
    '#submit' => array('ndla_ord_admin_language_settings_save'),
  );

  $form['language_mapping'] = array(
    '#title' => t('Language Mapping'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
    '#attributes' => array("id" => "langmap")
  );


  foreach($languages as $langid => $languagestring){
    $topiclang = "http://psi.oasis-open.org/iso/639/#".$langid;
    $form['language_mapping'][$langid] = array(
      '#title' => $languagestring,
      '#type' => 'textfield',
      '#default_value' => $sitemapping[$langid],
      '#description' => t('Enter the site equivalent for this language identificator')
    );
  }

  $form['language_mapping']['submit']=array(
    '#type' => 'submit',
    '#value' => t('Save language mappings'),
    '#submit' => array('ndla_ord_admin_language_mappings_save'),
  );

  return $form;
}

function ndla_ord_admin_language_settings_save($form, $form_state){
  $data = $form['#post']['language_setting'];
  foreach ($data as $key => $value) {
    variable_set($key, $value);
  }
  drupal_set_message(t('The language settings were saved.'), 'status', TRUE);
}

function ndla_ord_admin_language_mappings_save($form, $form_state){
  $data = $form['#post']['language_mapping'];
  $topicmapping = array();
  $sitemapping = array();

  foreach($data as $id => $val) {
      if(strpos($id, "oasis-open") !== false && $val != ""){
        $psi = str_replace("_",".",$id);
        $topicmapping[$val] = $psi;
        $sitemapping[$psi] = $val;
      }
  }
  $out = array(
    "sitemapping" => $sitemapping,
    "topicmapping" => $topicmapping
  );


  variable_set('ndla_ord_topic_site_language_mappings',$out);
  drupal_set_message(t('The language mappings were saved.'), 'status', TRUE);
}

/**
 * Implementation of hook_validate for ndla_ontopia_connect_admin_settings
 */
function ndla_ord_admin_module_settings_validate($form,  &$form_state) {

  if (empty($form_state['values']['ndla_ord_topic_service_url'])) {
    form_set_error('ndla_ord_topic_service_url', t('The topic service url for this installation cannot be empty'));
  }
  else if (empty($form_state['values']['ndla_ord_topic_site_string'])) {
    form_set_error('ndla_ord_topic_site_string', t('The site argument for the topic server cannot be empty'));
  }
}


/**
 * Implementation of hook_submit for ndla_ontopia_connect_admin_settings
 */
function ndla_ord_admin_module_settings_submit($form,  &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    variable_set($key, $value);
  }
  drupal_set_message(t('The settings were saved.'), 'status', TRUE);
}