var tagger;
(function($) {

  if ($.fn.jquery == "1.2.6") { // Just to please Drupal 6.
    $.fn.extra = function (key, value) {
      if (typeof key !== 'undefined') {
        if (typeof value !== 'undefined') {
          $(this).attr('data-' + key, value);
        } else {
          return $(this).attr('data-' + key);
        }
      } else {
        var data = {};
        $(this).each(function() {
          $.each(this.attributes, function() {
            pos = this.name.indexOf('-');
            if (pos != -1 && this.name.substr(0, pos) == 'data') {
              data[this.name.substr(pos + 1)] = this.value;
            }
          });
        });
        return data;
      }
    };
  } else {
    $.fn.extra = $.fn.data;
  }

  $.fn.tagger = function(user_params) {
    var $this = this;

    var params = {
      new_tags: true
    };
    if (typeof user_params === 'undefined') user_params = {};

    $.extend(params, user_params)

    var initPlugin = function() {
      tagger.createNewTagger($this, params);
    };

    $(document).click(function(event) {
      tagger.blurKeyword();
    });

    initPlugin();
    $this.trigger('initialized');
  };

  // ################################################################
  // #################### THIS IS THE MAIN CLASS ####################
  // ################################################################

  function Tagger() {
    var that = this;

    this.checkForOverflow = function($input) {
      if (($input[0].scrollWidth - 1) * window.devicePixelRatio > ($input.width() + 1)) {
        $input.width($input.parent().parent().width() - 1);
      }
    }

    this.init = function() {
      $(document).keydown(function(event) {
        that.keyDown(event);
      });
    };

    this.refresh = function() {
      $('.tagger-plugin-input').each(function() {
        $input = $(this);
        c = tagger.getPluginId($input);
        $keywords = $('.tagger-plugin-keyword-' + c);
        $(this).trigger('save', [$keywords]);
        var value = {};
        var i = 0;
        $keywords.each(function() {
          var obj = $(this).extra();
          obj['name'] = $(this).find('.tagger-plugin-keyword-title-' + c).text();
          value[i++] = obj;
        });
        $input.val(JSON.stringify(value));
      });
    };

    this.keyDown = function(event) {
      $selected = that.getSelectedKeyword();
      if ($selected.length > 0) {
        var status = false;
        if (event.which == 37 || (event.which == 9 && event.shiftKey)) { // arrow left or shift + tab
          status = that.prevKeyword();
          if (!status && event.which == 9) that.blurKeyword(); // Blur if using shift + tab at the first keyword
        } else if (event.which == 39 || (event.which == 9 && !event.shiftKey)) { // arrow right or tab
          status = that.nextKeyword();
        } else if (event.which == 8 || event.which == 46) { // backspace or delete
          status = that.removeKeyword();
        }
        if (status) event.preventDefault();
      }
    };

    this.prevKeyword = function() {
      $selected = that.getSelectedKeyword();
      if ($selected.length > 0) {
        c = tagger.getPluginId($selected);
        $sibling = $selected.parent().prev().find('.tagger-plugin-keyword-' + c);
        if ($sibling.length > 0) {
          that.selectKeyword($sibling);
          return true;
        }
      }
      return false;
    };

    this.nextKeyword = function() {
      $selected = that.getSelectedKeyword();
      if ($selected.length > 0) {
        c = tagger.getPluginId($selected);
        $sibling = $selected.parent().next().find('.tagger-plugin-keyword-' + c);
        if ($sibling.length > 0) {
          that.selectKeyword($sibling);
        } else {
          that.blurKeyword();
          $('#tagger-plugin-textfield-' + c).focus();
        }
        return true;
      }
      return false;
    };

    this.removeKeyword = function() {
      $selected = that.getSelectedKeyword();
      if (!$selected.hasClass('tagger-plugin-keyword-locked')) {
        c = tagger.getPluginId($selected);
        $('#tagger-plugin-textfield-' + c).focus();
        $element = $('#tagger-plugin-' + c);
        $selected.parent().remove();
        that.resetInputField($element);
        that.refresh();
      }
      return true;
    }

    this.getSelectedKeyword = function() {
      return $('.tagger-plugin-selected');
    }


    var plugin_number = 0;

    this.getPlugin = function($element) {
      if (!$element.hasClass('tagger-plugin')) {
        $element = $element.parents('.tagger-plugin');
        if ($element.length > 1) {
          $element = $($element[0]);
        } else if ($element.length == 0) {
          alert('an error');
          return false;
        }
      }

      return $element;
    }

    this.getPluginId = function($element) {
      if (!$element.hasClass('tagger-plugin')) {
        $element = $element.parents('.tagger-plugin');
        if ($element.length > 1) {
          $element = $($element[0]);
        } else if ($element.length == 0) {
          alert('an error');
          return false;
        }
      }

      var id = $element.attr('id');
      pos = id.lastIndexOf('-');
      return id.substr(pos + 1)
    }

    this.lockKeyword = function($keyword) {
      $keyword.addClass('tagger-plugin-keyword-locked');
    }

    this.unlockKeyword = function($keyword) {
      $keyword.removeClass('tagger-plugin-keyword-locked');
    }

    this.createNewTagger = function($elements, params) {

      $elements.each(function() {
        c = ++plugin_number;

        $(this).wrap('<span class="tagger-plugin" data-new-tags="' + (params['new_tags'] ? '1' : '0') + '" id="tagger-plugin-' + c + '"></span>');
        $(this).addClass('tagger-plugin-input');
        $(this).attr('id', 'tagger-plugin-input-' + c);

        $(this).after('<span class="tagger-plugin-content" id="tagger-plugin-content-' + c + '"><span class="tagger-plugin-textfield-wrapper" id="tagger-plugin-textfield-wrapper-' + c + '"><input type="text" class="tagger-plugin-textfield" autocomplete="off" id="tagger-plugin-textfield-' + c + '"></span></span>');

        $('#tagger-plugin-content-' + c).click(function(event) {
          c = tagger.getPluginId($(this));
          $('#tagger-plugin-textfield-' + c).focus();
        });

        $('#tagger-plugin-textfield-' + c).focus(function(event) {
          if ($(this).val() == '') {
            tagger.resetInputField(tagger.getPlugin($(this)));
          }
        });

        $('#tagger-plugin-textfield-' + c).keydown(function(event) {
          c = tagger.getPluginId($(this));
          $plugin = tagger.getPlugin($(this));
          if (event.which == 13 && $plugin.extra('new-tags') == '1') { // hitting enter
            c = tagger.getPluginId($(this));
            $('#tagger-plugin-input-' + c).trigger('keyboard-enter', [$plugin]);
            event.preventDefault();
            return false;
          } else if ((event.which == 37 || event.which == 8 || (event.which == 9 && event.shiftKey)) && $(this).val() == '') { // arrow left, backspace or shift + tab
            $keywords = $('#tagger-plugin-' + c).find('.tagger-plugin-keyword-' + c);
            if ($keywords.length > 0) {
              event.stopPropagation();
              event.preventDefault();
              $(this).blur();
              $keywords[$keywords.length - 1].click();
            }
          } else {
            that.checkForOverflow($(this));
          }
        });

        if ($(this).val()) {
          data = JSON.parse($(this).val());
          for (i in data) {
            that.addKeyword($(this).parents('.tagger-plugin'), data[i].name, {}, data[i]);
          }
        }

      });

    };

    this.addKeyword = function($element, keyword, user_params, extra) {
      var params = {
        locked: false
      };
      if (typeof user_params === 'undefined') user_params = {};

      $.extend(params, user_params)

      c = tagger.getPluginId($element);
      $input = $('#tagger-plugin-textfield-' + c);

      if (!keyword) keyword = $input.val();
      keyword = keyword.trim();

      if (keyword == '') {
        $input.val('');
        return false;
      }

      $span1 = $('<span class="' + (params['locked'] ? 'tagger-plugin-keyword-locked ' : '') + 'tagger-plugin-keyword tagger-plugin-keyword-' + c + '" />');
      $span3 = $('<span class="tagger-plugin-keyword-title tagger-plugin-keyword-title-' + c + '" />');
      $span4 = $('<span class="tagger-plugin-keyword-delete tagger-plugin-keyword-delete-' + c + '" />');

      $span3.text(keyword);

      $span1.click(function(event) {
        that.blurKeyword();
        $(this).addClass('tagger-plugin-selected');
        event.stopPropagation();
      });

      $span4.click(function(event) {
        that.blurKeyword();
        that.selectKeyword($(this).parent());
        that.removeKeyword();
      });

      $input.parent().before($span1);
      $span1.append($span3);
      $span1.append($span4);

      $span2 = $('<span class="tagger-plugin-keyword-wrapper tagger-plugin-keyword-wrapper-' + c + '" />');
      $span1.wrap($span2);

      that.resetInputField($element);

      $span1.setData = function(name, value) {
      };

      // Custom events

      $span1.hover(function(event) {
        c = tagger.getPluginId($(this));
        $('#tagger-plugin-input-' + c).trigger('mouseenterkeyword', [$(this)]);
      }, function(event) {
        c = tagger.getPluginId($(this));
        $('#tagger-plugin-input-' + c).trigger('mouseleavekeyword', [$(this)]);
      });

      $('#tagger-plugin-input-' + c).trigger('addkeyword', [$span1,extra]);

      // End custom events

      that.refresh();

      return $span1;

    };

    this.resetInputField = function($element, text) {
      c = tagger.getPluginId($element);
      if (!text) text = '';
      $input = $('#tagger-plugin-textfield-' + c);
      var width = 0;
      var lastTop = 0;
      $element.find('.tagger-plugin-keyword-wrapper-' + c).each(function() {
        if ($(this).position().top != lastTop) {
          width = 0;
          lastTop = $(this).position().top;
        }
        width += $(this).width();
      });
      $input.width($input.parent().parent().width() - width - 1);
      $input.val(text);
    };

    this.selectKeyword = function($element) {
      that.blurKeyword();
      $element.addClass('tagger-plugin-selected');
    };

    this.blurKeyword = function() {
        $('.tagger-plugin-keyword').removeClass('tagger-plugin-selected');
    }

    this.init();
  };

  $(function() {
    tagger = new Tagger();
  });


}(jQuery));
