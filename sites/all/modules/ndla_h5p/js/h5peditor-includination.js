var H5PEditor = H5PEditor || {};

/**
 * Library selector includination widget module.
 * Makes it possible to include a H5P in another H5P using the contentbrowser.
 * Comes without warranty.
 *
 * @param {jQuery} $
 */
H5PEditor.widgets.library = H5PEditor.Includination = (function ($) {

  /**
   * Initialize widget.
   *
   * @param {Object} parent
   * @param {Object} field
   * @param {Object} params
   * @param {Function} setValue
   * @returns {_L3.C}
   */
  function C(parent, field, params, setValue) {
    var that = this;

    this.field = field;
    this.parent = parent;
    this.params = params;

    this.$button = $('<a href="#" class="h5peditor-contentbrowser">' + window.top.Drupal.t('Browse') + '...</a>').hide().click(function () {
      that.open();
      return false;
    });

    this.widget = new H5PEditor.Library(parent, field, params, function (field, params) {
      that.params = params;
      setValue(field, params);
    });
    this.changes = this.widget.changes;
    this.changes.push(function () {
      that.currentLibrary = that.widget.currentLibrary;
      that.children = that.widget.children;
      if (that.params !== undefined) {
        if (that.params.nodeId !== undefined) {
          that.load(that.params.nodeId);
        }

        if (that.params.library === undefined) {
          return that.$button.hide();
        }
        if (that.params.library === 'H5P.Link 1.0') {
          return that.$button.show();
        }

        // Find out if lib is runnable.
        var runnable = false;
        for (var i = 0; i < that.widget.libraries.length; i++) {
          var lib = that.widget.libraries[i];
          if (lib.uberName === that.params.library) {
            runnable = (lib.runnable === '1');
            break;
          }
        }

        if (!runnable) {
          return that.$button.hide();
        }
        that.$button.show();
      }
    });

    this.base = window.location.protocol + '//' + window.location.host + H5PEditor.baseUrl;
  }

  /**
   * Append widget to the given DOM wrapper.
   *
   * @param {jQuery} $wrapper
   * @returns {undefined}
   */
  C.prototype.appendTo = function ($wrapper) {
    var that = this;

    this.widget.appendTo($wrapper);
    this.$select = this.widget.$select;

    this.$button.insertAfter(this.$select);

    // if (this.params.library !== undefined) {
    //   this.$button.show();
    // }
  };

  C.prototype.forEachChild = function (next) {
    this.widget.forEachChild(next);
  };

  /**
   *
   * @param {Number} nodeId
   * @returns {undefined}
   */
  C.prototype.load = function (nodeId) {
    var that = this;

    this.$button.hide();
    var $loading = $('<div class="h5peditor-loading">' + C.t('loading', {':type': 'node ' + nodeId}) + '</div>')
      .insertBefore(this.widget.$libraryWrapper.hide());

    this.params.nodeId = nodeId;

    $.get(this.base + 'ndla-h5p/node-params/' + nodeId + '/' + (that.params.library === 'H5P.Link 1.0' ? 1 : 0), function (node) {
      if (node.library === that.params.library) {
        that.params.params = JSON.parse(node.params);
        that.widget.$libraryWrapper
          .html('<div class="h5peditor-node">Node: ' + node.title + ' (<a href="#" class="h5peditor-removenode">' + window.top.Drupal.t('Remove') + '</a>)</div>')
          .find('.h5peditor-removenode').click(function () {
            if (confirm(window.top.Drupal.t('Are you sure you wish to remove the connection to this node?'))) {
              delete that.params.nodeId;
              that.widget.loadLibrary(that.params.library, true);
            }
            return false;
          });
      }
      else {
        delete that.params.nodeId;
      }

      that.widget.$libraryWrapper.show();
      $loading.remove();
      that.$button.show();
    });
  };

  /**
   * Opens content searcher.
   *
   * @returns {undefined}
   */
  C.prototype.open = function () {
    var that = this;

    if (that.params.library === undefined) {
      return this.$button.hide(); // A little bit hack, but it gets the job done.
    }

    runkode = function (nodeId) {
      that.load(nodeId);
    };

    var cb = window.open(this.base + 'contentbrowser?output=nid', "_blank", "resizable=yes");

    if (that.params.library === 'H5P.Link 1.0') {
      return; // Allow choosing from all nids.
    }

    $(cb).load(function () {
      var $html = $(cb.document).contents();
      $('#edit-node-type-wrapper', $html).find('option[value="h5p_content"]').prop('selected', true).end().css({
        position: 'absolute',
        right: '100%'
      });
      $('#edit-h5p-type', $html).val(that.params.library);
    });
  };

  /**
   * Ready.
   *
   * @param {type} ready
   */
  C.prototype.ready = function (ready) {
    this.parent.ready(ready);
  };

  /**
   * Always validate.
   *
   * @returns {Boolean} true
   */
  C.prototype.validate = function () {
    return this.params.nodeId !== undefined ? true : this.widget.validate();
  };

  /**
   * Remove field.
   *
   * @returns {undefined}
   */
  C.prototype.remove = function () {
    this.widget.remove();
  };

  /**
   * Forward change callbacks.
   */
  C.prototype.change = function () {
    this.widget.change.apply(this, arguments);
  };

  /**
   * Get localized strings.
   *
   * @param {String} key
   * @param {Object} params
   * @returns {@exp;H5PEditor@call;t}
   */
  C.t = function (key, params) {
    return H5PEditor.t('core', key, params);
  };

  return C;
})(H5P.jQuery);
