(function ($) {
  if (window.MathJax === undefined) {
    return; // Missing MathJax
  }

  // Hide annoying processing messages
  MathJax.Hub.Config({messageStyle: 'none'});

  function ForcedFullscreen($container, instance, H5P) {
    var self = this;

    var $overlay = $('<div>', {
      'class': 'h5p-ndla-fullscreen-overlay',
      click: function () {
        self.goFullscreen();
      }
    }).append($('<div>', {
      'class': 'h5p-ndla-fullscreen-overlay-icon'
    }).append($('<div>', {
      'class': 'h5p-ndla-fullscreen-overlay-text',
      html: 'Start'
    }))).appendTo($container);

    /**
     * Go fullscreen
     * @method goFullscreen
     */
    self.goFullscreen = function () {
      H5P.fullScreen($container, instance);
    };
  };

  $(document).ready(function () {
    $('.h5p-ndla-force-fullscreen').each(function (i, e) {
      // Must handle both div and iframe mode:
      var $iframe  = $(this).find('.h5p-iframe');
      var isIframe = $iframe.length !== 0;
      var win = isIframe ? $iframe.get(0).contentWindow : window;

      function h5pInitialized () {
        var jq = win.H5P.jQuery;
        var $content = isIframe ? jq('.h5p-content') : jq(this).find('.h5p-content');

        // Get content id:
        var contentId = $content.data('content-id');

        // Find library instance:
        var instance;
        for (var i = 0; i < win.H5P.instances.length; i++) {
          if (win.H5P.instances[i].contentId === contentId) {
            new ForcedFullscreen($content.find('.h5p-container'), win.H5P.instances[i], win.H5P);
            break;
          }
        }
      }

      // If iframe, need for it be initialized
      function waitForIframe () {
        setTimeout(function () {
          if (win.H5P !== undefined && win.H5P.instances !== undefined && win.H5P.instances.length !== 0) {
            h5pInitialized();
          }
          else {
            waitForIframe();
          }
        }, 50);
      }

      if (isIframe) {
        waitForIframe();
      }
      else {
        h5pInitialized();
      }
    });

    // Find H5P content
    $('.h5p-content').each(function (i, e) {
      var doJax = function (node) {
        MathJax.Hub.Queue(['Typeset', MathJax.Hub, node]);
      };
      var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
      if (!MutationObserver) {
        var check = function () {
          $('math', e).each(function (j, m) {
            doJax(m.parentNode);
          });
          checkInterval = setTimeout(check, 2000);
        };
        var checkInterval = setTimeout(check, 2000);
      }
      else {
        var running = false;
          var limitedResize = function () {
            if (!running) {
              running = setTimeout(function () {
                $('math', e).each(function (j, m) {
                  doJax(m.parentNode);
                });
                running = null;
              }, 500); // 2 fps cap
            }
          };

        var observer = new MutationObserver(function (mutations) {
          for (var i = 0; i < mutations.length; i++) {
            if (mutations[i].addedNodes.length) {
              limitedResize();
              return;
            }
          }
        });
        observer.observe(e, {
          childList: true,
          subtree: true
        });
      }
    });
  });
})(H5P.jQuery);
