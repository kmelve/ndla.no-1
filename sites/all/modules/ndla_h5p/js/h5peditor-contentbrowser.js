var H5PEditor = H5PEditor || {};

/**
 * Content browser editor widget module.
 *
 * @param {jQuery} $
 */
H5PEditor.widgets.image = H5PEditor.widgets.video = H5PEditor.widgets.audio = H5PEditor.ContentBrowser = (function ($) {

  /**
   * Initialize content browser widget
   *
   * @param {Object} parent
   * @param {Object} field
   * @param {Object} params
   * @param {Function} setValue
   * @returns {_L3.C}
   */
  function C(parent, field, params, setValue) {
    var that = this;

    this.field = field;
    this.setValue = setValue;
    this.parent = parent;
    this.params = params;
    this.cbContent = (params !== undefined && (params.nodeId !== undefined || (params[0] !== undefined && params[0].nodeId !== undefined)));

    var customSetValue = function (field, params) {
      if (that.cbContent) {
        if (field.type === 'image') {
          that.setField('../alt', '', false);
        }
        that.setField('copyright/title', '', false);
        that.setField('copyright/author', '', false);
        that.setField('copyright/year', '', false);
        that.setField('copyright/source', '', false);
        that.setField('copyright/license', 'U', false);

        that.cbContent = false;
      }

      that.params = params;
      setValue(field, params);
    };

    // Create original widget
    if (field.type === 'image' || field.type === 'file') {
      this.widget = new H5PEditor.File(parent, field, this.params, customSetValue);
    }
    else if (field.type === 'video' || field.type === 'audio') {
      this.widget = new H5PEditor.AV(parent, field, this.params, customSetValue);
    }

    this.changes = this.widget.changes;
    this.base = window.location.protocol + '//' + window.location.host + H5PEditor.baseUrl;
  }

  /**
   * Append widget to the given DOM wrapper.
   *
   * @param {jQuery} $wrapper
   * @returns {undefined}
   */
  C.prototype.appendTo = function ($wrapper) {
    var that = this;

    var $dummy = $('<div></div>');
    this.widget.appendTo($dummy);
    this.$file = $dummy.find('.file');

    this.$button = $('<a href="#" class="h5peditor-contentbrowser">' + window.top.Drupal.t('Browse') + '...</a>').insertAfter(this.$file).click(function () {
      that.open();
      return false;
    });

    if (this.field.type === 'video') {
      this.$input = $('<input/>', {
        'class': 'h5peditor-text',
        type: 'text',
        value: this.params !== undefined && this.params[0].nodeId ? this.params[0].nodeId : ''
      }).insertAfter(this.$file).change(function () {
        var val = that.$input.val();
        that.widget.$errors.html('');

        if (/^\d+$/.test(val)) {
          // NodeID
          that.load(val);
          that.cbContent = true;
        }
        else if (/^https?:\/\/(www.youtube.com|youtu.be|y2u.be)\/(.+=)?(\S+)$/i.test(val)) {
          // Update params
          that.params = [{
            path: val,
            mime: 'video/YouTube'
          }];
          that.setValue(that.field, that.params);
          for (var j = 0; j < that.changes.length; j++) {
            that.changes[j](that.params[0]);
          }
        }
        else {
          // Not YT nor Node
          that.widget.$errors.append(H5PEditor.createError('Invalid input.'));
        }
      });
    }

    $wrapper.append($dummy.children());

    if (this.widget.$add !== undefined) {
      // Override file upload callback
      var oldUploadFile = this.widget.uploadFile;
      this.widget.uploadFile = function () {
        oldUploadFile.apply(that.widget);

        var oldChangeCallback = H5PEditor.File.changeCallback;
        H5PEditor.File.changeCallback = function () {
          oldChangeCallback();
          if (that.cbContent) {
            // Remove files from content searcher when adding a new file
            that.$file.children('.thumbnail').remove();
            delete that.params;
            delete that.widget.params;
          }
        };
      };
    }
    else {
      // Disable image browse on thumbnail click
      that.widget.$file.children('.thumbnail').unbind('click').click(function () {
        that.open();
        return false;
      });
    }

    if (this.params !== undefined) {
      if (this.params.nodeId !== undefined) {
        this.parent.ready(function () {
          that.setField('../alt', undefined, true);
        });
      }
      if (this.params.nodeId !== undefined || (this.params[0] !== undefined && this.params[0].nodeId !== undefined)) {
        this.parent.ready(function () {
          that.setField('copyright/title', undefined, true);
          that.setField('copyright/author', undefined, true);
          that.setField('copyright/year', undefined, true);
          that.setField('copyright/source', undefined, true);
          that.setField('copyright/license', undefined, true);
        });
      }
    }
  };

  /**
   *
   * @param {Number} nodeId
   * @returns {undefined}
   */
  C.prototype.load = function (nodeId) {
    var that = this;

    this.$button.hide();
    if (this.$input !== undefined) {
      this.$input.hide();
    }
    this.$file.html('<div class="h5peditor-loading">' + C.t('loading', {':type': 'node ' + nodeId}) + '</div>');

    $.get(this.base + 'ndla-h5p/node-data/' + nodeId, function (nodeData) {
      switch (that.field.type) {
        case 'image':
          if (nodeData.error !== undefined) {
            that.widget.$errors.append(H5PEditor.createError(nodeData.error));
            that.widget.addFile();
            break;
          }

          that.params = that.widget.params = nodeData.data;
          that.setValue(that.field, that.params);

          for (var i = 0; i < that.changes.length; i++) {
            that.changes[i](that.params);
          }
          that.widget.addFile();
          that.widget.$file.children('.thumbnail').unbind('click').click(function () {
            that.open();
            return false;
          });

          // Set alt text
          that.setField('../alt', nodeData.copyright.title, true);
          break;

        case 'video':
        case 'audio':
          if (nodeData.error !== undefined) {
            that.widget.$errors.append(H5PEditor.createError(nodeData.error));
            break;
          }

          that.params = that.widget.params = nodeData.data;
          that.setValue(that.field, that.params);

          that.widget.$add = that.$file.html(ns.AV.createAdd()).children().click(function () {
            that.widget.uploadFile();
            return false;
          });

          for (var i = 0; i < that.params.length; i++) {
            that.widget.addFile(i);
            for (var j = 0; j < that.changes.length; j++) {
              that.changes[j](that.params[i]);
            }
          }

          break;
      }

      that.widget.setCopyright(that.widget.copyright);

      // Set copyright data
      that.setField('copyright/title', nodeData.copyright.title, true);
      that.setField('copyright/author', nodeData.copyright.author, true);
      that.setField('copyright/year', nodeData.copyright.year, true);
      that.setField('copyright/source', nodeData.copyright.source, true);
      that.setField('copyright/license', nodeData.copyright.license, true);

      that.$button.show();
      if (that.$input !== undefined) {
        that.$input.val(nodeId).show();
      }
    });
  };

  /**
   * Update field at the given path with the given value.
   *
   * @param {String} path
   * @param {String} value
   * @returns {undefined}
   */
  C.prototype.setField = function (path, value, disable) {
    disable = disable || false;

    var field = H5PEditor.findField(path, this.widget);
    if (field !== false) {
      if (field instanceof H5PEditor.Text && (H5P.trim(field.$input.val()) === '' || disable)) {
        if (value !== undefined) {
          field.$input.val(value).change();
          field.$errors.html('');
        }
        field.$input.attr('disabled', disable);

      }
      else if (field instanceof H5PEditor.Select) {
        if (value !== undefined) {
          field.$select.val(value).change();
        }
        field.$select.attr('disabled', disable);
      }
    }
  };

  /**
   * Opens content searcher.
   *
   * @returns {undefined}
   */
  C.prototype.open = function () {
    var that = this;

    runkode = function (nodeId) {
      that.widget.$errors.html('');
      that.cbContent = true;
      that.load(nodeId);
    };

    var cb = window.open(this.base + 'contentbrowser?output=nid', "_blank", "resizable=yes");
    $(cb).load(function () {
      var $html = $(cb.document).contents();
      $('#edit-node-type-wrapper', $html).find('option[value="' + that.field.type + '"]').attr('selected', true).end().css({
        position: 'absolute',
        right: '100%'
      });
    });
  };

  /**
   * Always validate.
   *
   * @returns {Boolean} true
   */
  C.prototype.validate = function () {
    return true;
  };

  /**
   * Remove field.
   *
   * @returns {undefined}
   */
  C.prototype.remove = function () {
    this.widget.remove();
  };

  /**
   * Get localized strings.
   *
   * @param {String} key
   * @param {Object} params
   * @returns {@exp;H5PEditor@call;t}
   */
  C.t = function (key, params) {
    return H5PEditor.t('core', key, params);
  };

  return C;
})(H5P.jQuery);
