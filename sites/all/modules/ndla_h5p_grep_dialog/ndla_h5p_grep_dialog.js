/*global H5P*/

if (H5P && H5P.externalDispatcher) {
  H5P.externalDispatcher.on('domChanged', function (event) {
    // Create plugin
    if (this instanceof H5P.DocumentationTool) {
      var plugin = new H5P.DocumentationToolGrepPlugin(this, event.data.$target);
      plugin.attach();
    }
  });
}

H5P.DocumentationToolGrepPlugin = (function ($, GrepDialogBox) {
  var USER_DEFINED_GOAL = 0;

  /**
   * Initialize module
   * @param {H5P.DocumentationTool} documentationTool
   * @constructor
   */
  function DocumentationToolGrepPlugin(documentationTool, $wrapper) {
    this.documentationTool = documentationTool;
    this.$wrapper = $wrapper;
    this.goalPages = [];
  }

  /**
   * Get lists with filtered ids
   * @returns {Array} filterIdList
   */
  DocumentationToolGrepPlugin.prototype.getFilteredIdList = function (goalPage) {
    var filterIdList = [];

    if (goalPage.params.commaSeparatedCurriculumList !== undefined &&
        goalPage.params.commaSeparatedCurriculumList.length) {
      filterIdList = goalPage.params.commaSeparatedCurriculumList.split(',');
      filterIdList.forEach(function (filterId, filterIndex) {
        filterIdList[filterIndex] = filterId.trim();
      });
    }
    return filterIdList;
  };

  /**
   * Create grep dialog box
   */
  DocumentationToolGrepPlugin.prototype.createGrepDialogBox = function (goalPage) {
    var filteredIdList = this.getFilteredIdList(goalPage);
    var dialogInstance = new GrepDialogBox(goalPage.params, filteredIdList);
    dialogInstance.attach(goalPage.$inner.parent().parent().parent());
    dialogInstance.getFinishedButton().on('dialogFinished', function (event, data) {
      data.forEach(function (competenceAim) {
        // Set competence aim type to 0 to prevent specifications of goals
        if (!competenceAim.goalType) {
          competenceAim.goalType = USER_DEFINED_GOAL;
        }
        goalPage.addGoal(competenceAim);
      });
    });
  };

  DocumentationToolGrepPlugin.prototype.addGrepDialogButton = function (goalPage) {
    var self = this;
    var $goalButtonArea = $('.goals-define', goalPage.$inner);

    // Create predefined goal using GREP API
    H5P.JoubelUI.createSimpleRoundedButton(goalPage.params.chooseGoalText)
      .addClass('goals-search')
      .click(function () {
        self.createGrepDialogBox(goalPage);
      }).prependTo($goalButtonArea);
  };

  DocumentationToolGrepPlugin.prototype.getGoalsPages = function () {
    var self = this;
    this.documentationTool.pageInstances.forEach(function (page) {
      if (page instanceof H5P.GoalsPage) {
        self.goalPages.push(page);

        // Set default behavior.
        page.params = $.extend(true, {
          chooseGoalText: 'Choose goal from list',
          specifyGoalText: 'Specification',
          grepDialogDone: 'Done',
          filterGoalsPlaceholder: "Filter on words...",
          commaSeparatedCurriculumList: "",
          noResultsFound: "No matching goals were found..."
        }, page.params);
      }
    });
  };

  DocumentationToolGrepPlugin.prototype.attachGoalPage = function (goalPage) {
    this.addGrepDialogButton(goalPage);

    this.getFilteredIdList(goalPage);
  };

  DocumentationToolGrepPlugin.prototype.attach = function () {
    var self = this;
    this.getGoalsPages();

    this.goalPages.forEach(function (goalPage) {
      self.attachGoalPage(goalPage);
    });
  };

  return DocumentationToolGrepPlugin;

}(H5P.jQuery, H5P.GrepDialogBox));

