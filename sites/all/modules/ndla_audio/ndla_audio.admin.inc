<?php

function ndla_audio_settings_form() {
  $form = array(
    'jplayer_directory' => array(
      '#type' => 'textfield',
      '#title' => t('jPlayer file directory'),
      '#default_value' => variable_get('jplayer_directory', 'sites/all/libraries/jplayer'),
      '#description' => t('Specify the path that contains the jPlayer library. The jplayer.player.min.js file should be in the root of this directory.'),
    ),
    'options' => array(
      '#type' => 'fieldset',
      '#title' => t('NDLA jPlayer options'),
      '#collapsible' => FALSE,
    ),
    'options' => array(
      'jplayer_autoplay' => array(
        '#type' => 'checkbox',
        '#title' => t('Auto-play files on page load'),
        '#description' => t('Use caution when combining this option with multiple players on the same page.'),
        '#default_value' => variable_get('jplayer_autoplay', ''),
      ),
    ),
  );
  $form = system_settings_form($form);
  $form['#validate'][] = 'ndla_audio_settings_form_validate';
  $form['#submit'][] = 'ndla_audio_settings_form_submit';
  return $form;
}

function ndla_audio_settings_form_validate($form, &$form_state) {
  $form_state['jplayer_version'] = ndla_audio_get_version($form_state['values']['jplayer_directory']);
  if (!$form_state['jplayer_version']) {
    form_error($form['jplayer_directory'], t('The directory specified does not seem to contain the jPlayer library. Check to make sure that the jquery.player.min.js file is located within this directory.'));
  }
}

function ndla_audio_settings_form_submit($form, &$form_state) {
  drupal_set_message(t('The jPlayer library (version @version) successfully found in the %directory directory.', array('@version' => $form_state['jplayer_version'], '%directory' => $form_state['values']['jplayer_directory'])));
}

function ndla_audio_get_version($directory = NULL) {
  $version = 0;
  if (!isset($directory)) {
    $directory = variable_get('ndla_jplayer_directory', 'sites/all/libraries/jplayer');
  }
  if (file_exists($directory . '/jquery.jplayer.min.js')) {
    $contents = file_get_contents($directory . '/jquery.jplayer.min.js');
  }
  elseif (file_exists($directory . '/jquery.jplayer.js')) {
    $contents = file_get_contents($directory . '/jquery.jplayer.js');
  }
  $matches = array();
  preg_match('/Version:?[ ]*([\d.]+)/i', $contents, $matches);
  if (isset($matches[1])) {
    $version = $matches[1];
  }
  return $version;
}
