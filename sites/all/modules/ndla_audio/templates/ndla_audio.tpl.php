<?php
//When we build the SolR index - dont render the player
if($node->build_mode == NODE_BUILD_SEARCH_INDEX) {
  return;
}
?>
<!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<div class="jp-jplayer rs_skip" id ="jp-jplayer-<?php print $node->nid; ?>"></div>
<div class="jp-audio" id='ndla-audio-<?php print $node->nid; ?>'>
  <div class="jp-type-single">
    <div class="jp-title">
		  <ul>
			  <li>
			    <?php if($options['downloadable']) print "<a href='" . check_url($options['filepath']) . "' target='_new'>"; ?>
			      <?php print check_plain($node->title); ?>
			    <?php if($options["downloadable"]) print "</a>"; ?>
			    <a href='<?php print check_url($options["filepath"]); ?>' class='jp_url'></a>
			  </li>				
			</ul>
		</div>
		<div class="jp-progress">
			<div class="jp-seek-bar">
				<div class="jp-play-bar"></div>
			</div>
		</div>
	  <div class="jp-gui jp-interface">
		  <ul class="jp-controls">
			  <li>
			    <a href="javascript:;" class="jp-play" tabindex="1">
			      play
			    </a>
			  </li>
				<li>
				  <a href="javascript:;" class="jp-pause" tabindex="1">
				    pause
				  </a>
				</li>
				<li class="jp-mute-container">
				  <a href="javascript:;" class="jp-mute" tabindex="1" title="mute">
				    mute
				  </a>
				</li>
				<li class="jp-unmute-container">
				  <a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">
				    unmute
				  </a>
				</li>
			</ul>
			<div class="jp-volume-bar">
				<div class="jp-volume-bar-value"></div>
			</div>
			<span class="jp-current-time"></span> / <span class="jp-duration"></span>
		</div>
		<div class="jp-no-solution">
		  <span>Update Required</span>
				To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>
