<html>
<head>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>

<body class="lightboxContent">
<h1><?php print $title; ?></h1>
<div id="content">
<div id="body">
<?php print $content; ?>
</div>
</div>
</body>
</html>