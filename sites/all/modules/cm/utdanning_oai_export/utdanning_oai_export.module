<?php
/** \addtogroup utdanning_oai_export */

/**
* @file
* @ingroup utdanning_oai_export
* This module provides an interface to the OAI export.
*
*    
* @author hrm
*
* @version 1.0
*
*@license
 *Copyright (C) Utdanning.no
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LISENCE.txt file or the
 *   GNU General Public License for more details.
*/

require_once 'utdanning_oai_export_public.inc';
require_once 'utdanning_oai_export_private.inc';
define('UTDANNING_OAI_EXPORT_DEFAULT_URI', 'oai');
define('UTDANNING_OAI_EXPORT_NUM_PER_PAGE', 20);
define('UTDANNING_OAI_EXPORT_DEFAULT_LOM_PATH', drupal_get_path('module', 'utdanning_oai_export').'/lomData/');

$lomdir = variable_get('utdanning_oai_export_lom_profile', NULL);
if(file_exists(UTDANNING_OAI_EXPORT_DEFAULT_LOM_PATH.$lomdir)) {
	if(is_file(UTDANNING_OAI_EXPORT_DEFAULT_LOM_PATH.$lomdir)) {
		require_once UTDANNING_OAI_EXPORT_DEFAULT_LOM_PATH.$lomdir;
	}
}

$supportedMetadataPrefixes = array('norlom');

/**
 * Implementation of hook_init
 */
function utdanning_oai_export_init()
{
	drupal_add_css(drupal_get_path('module', 'utdanning_oai_export')."/css/utdanning_oai_export.css");
}

/**
 * Implementation of hook_menu
 */
function utdanning_oai_export_menu() {
	$items = array();
	
	$items['admin/settings/oai_export'] = array(
		'title' => t('Utdanning oai export Admin Page'),
		'description' => t('Change settings for this module.'),
		'access arguments' => array('administer utdanning_oai_export'),
		'page callback' => 'drupal_get_form',
		'page arguments' => array('utdanning_oai_export_admin_form'),		
	);
	
	$items['admin/settings/oai_export/settings'] = array(
		'title' => t('Settings'),
		'weight' => 1,
		'type' => MENU_DEFAULT_LOCAL_TASK,
	);
	
	$items['admin/settings/oai_export/profile'] = array(
		'title' => t('Field mapping'),
		'page callback' => 'drupal_get_form',
		'page arguments' => array('utdanning_oai_export_admin_map_profile'),
		'access arguments' => array('administer utdanning_oai_export'),
		'weight' => 2,
		'type' => MENU_LOCAL_TASK,
	);
	
	$items['admin/settings/oai_export/profile/new'] = array(
		'title' => t('Profile admin'),
		'weight' => 2,
		'type' => MENU_DEFAULT_LOCAL_TASK,
	);
	
	utdanning_oai_export_form_profiles($items);
	
	$items[variable_get("utdanning_oai_export_URI", UTDANNING_OAI_EXPORT_DEFAULT_URI)] = array(
		'type' => MENU_CALLBACK,
		'page callback' => '_utdanning_oai_export_oai',
		'access arguments' => array('access content'),
	);
	
	return $items;
}//hook_menu

/**
 * Implemention of hook_perm
 */
function utdanning_oai_export_perm()
{
	return array('administer utdanning_oai_export');
}//hook_perm

