<?php
/**
* @file
* @ingroup utdanning_oai_export
* Public functions for utdanning_oai_export.
*
* @see utdanning_oai_export.module
*    
* @author hrm
*
* @version 1.0
*
*@license
 *Copyright (C) Utdanning.no
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LISENCE.txt file or the
 *   GNU General Public License for more details.
*/

/**
 * Enter description here...
 *
 * @param string $errorMsg
 * @param object $xml
 * @return boolean
 */
function _utdanning_oai_export_error($errorMsg, &$xml)
{
	$xml->startElement("error");
	$xml->writeAttribute("code", $errorMsg);
	switch($errorMsg) {
		case "idDoesNotExist":
			$xml->text(t('No matching identifier in archive.'));
			break;
		case "badArgument":
			$xml->text(t('The request includes illegal or missing arguments.'));
			break;
		case "noSetHierarchy":
			$xml->text(t('set doesnt exists or includes illegal arguments.'));
			break;
		case "badVerb":
			$xml->text(t('Illegal OAI verb'));
		case "cannotDisseminateFormat":
			$xml->text(t('The value given for the metadataPrefix is not supported by the item or by the repository.'));
	}
	$xml->endElement();
	return TRUE;
}

/**
 * Function to check the request-variables
 *
 * @param object $xml
 * @return boolean
 */
function _utdanning_oai_export_request_check(&$xml)
{	
	if(!isset($_GET['verb']) && !isset($_POST['verb'])) {
		_utdanning_oai_export_error("missing", $xml);
		return FALSE;
	}
	else {
		$verb = check_plain($_REQUEST['verb']);
	}
	
	switch ($verb) {
		case "GetRecord":
			$identifiers = explode(":", $_REQUEST['identifier']);
			if((!isset($_GET['metadataPrefix']) && !isset($_POST['metadataPrefix'])) || preg_match_all("/[\W]+/", $_REQUEST['metadataPrefix'], $matches) != 0) {
				_utdanning_oai_export_error("badArgument", $xml);
				return FALSE;
			}	// In NDLA the identifier is a number and thats why the preg_match_all is used.
			else if((!isset($_GET['identifier']) && !isset($_POST['identifier'])) || preg_match_all("/[\D]+/", $identifiers[2], $matches) != 0 || $identifiers[0] != 'oai') {
				_utdanning_oai_export_error("idDoesNotExist", $xml);
				return FALSE;
			}
			else if($_GET['metadataPrefix'] == "" && $_POST['metadataPrefix'] == "") {
				_utdanning_oai_export_error("badArgument", $xml);
				return FALSE;
			}
			else if($_GET['identifier'] == "" && $_POST['identifier'] == "") {
				_utdanning_oai_export_error("idDoesNotExist", $xml);
				return FALSE;
			}
			
			break;
		case "Identify":
		case "ListMetadataFormats":
		case "ListRecords":
			_utdanning_oai_export_error("badArgument", $xml);
			return FALSE;
		case "ListIdentifiers":
			//metadataPrefix is required and set is optional
			if((!isset($_GET['metadataPrefix']) && !isset($_POST['metadataPrefix']))) {
				_utdanning_oai_export_error("badArgument", $xml);
				return FALSE;
			}
			else if(!in_array($_REQUEST['metadataPrefix'], utdanning_oai_export_metadataPrefixes())) {
				_utdanning_oai_export_error("cannotDisseminateFormat", $xml);
				return FALSE;
			}
			else if(preg_match_all("/[\W]+/", $_REQUEST['set'], $matches) != 0) {
				_utdanning_oai_export_error("noSetHierarchy", $xml);
				return FALSE;
			}
			break;
		case "ListSets": //no errors here
			break;
		default:
			_utdanning_oai_export_error("badVerb", $xml);
			return FALSE;
	}

	return $verb;
}

/**
 * Returns the content types which are set to be exportable in the admin interface.
 */
function _utdanning_oai_export_get_exportable_types() {
  $cts = array();
  $result = db_query("SELECT content_type FROM {utdanning_oai_export_settings}");
  while($row = db_fetch_object($result)) {
    $cts[] = $row->content_type;
  }
  
  return $cts;
}

/**
 * xml main(void)
 *
 * @return XML
 */
function _utdanning_oai_export_oai()
{
	$xml = new XMLWriter();
	$xml->openMemory();
	
	$xml->startDocument('1.0', 'UTF-8');
	$xml->setIndent(TRUE);
	$xml->setIndentString("   ");
	
	$xml->startElement("OAI-PMH");
	$xml->writeAttribute("xmlns", "http://www.openarchives.org/OAI/2.0/");
	$xml->writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	$xml->writeAttribute("xsi:schemaLocation", "http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd");
	
	_utdanning_oai_export_oai_responseHeader($xml);
	$verb = _utdanning_oai_export_request_check($xml);

	
	//Section to determin whether the set type is set and supported
	$setType = 0;
	$idCheck = 0;
	$counter = 0;
	$profileArray = array();
	
	$db_resource = db_query("SELECT id,profile,content_type FROM utdanning_oai_export_settings");
	while($db_array = db_fetch_array($db_resource)) {
		$profileArray[$counter]['id'] = check_plain($db_array['id']);
		$profileArray[$counter]['profile'] = check_plain($db_array['profile']);
		$profileArray[$counter]['content_type'] = check_plain($db_array['content_type']);
		$counter++;
	}
	
	if(isset($_GET['set']) || $_POST['set']) {
		foreach($profileArray AS $key => $value) {
			if(in_array(check_plain($_REQUEST['set']), $value, TRUE)) {
				$setType = $value['content_type'];
			}
		}
		if($setType === 0) {
			_utdanning_oai_export_error("noSetHierarchy", $xml);
			$verb = FALSE;
		}
	}
	else {
		$setType = 1;
	}
	
	if($verb == 'GetRecord') {
		$identifiers = explode(":", $_REQUEST['identifier']);
		$nid = check_plain(strip_tags(end($identifiers)));
		$nodeInfo = db_query("SELECT nid,type FROM node WHERE nid = %d LIMIT 1", $nid);
		$node_array = db_fetch_array($nodeInfo);		
		if(!$node_array) {
			_utdanning_oai_export_error("idDoesNotExist", $xml);
			$verb = FALSE;
		}
		else {
			$db_resource = db_query("SELECT id,profile,content_type FROM utdanning_oai_export_settings");
			while($db_array = db_fetch_array($db_resource)) {
				if($node_array['type'] == $db_array['content_type']) {
					$idCheck = 1;
				}
			}
			if($idCheck == 0) {
				_utdanning_oai_export_error("idDoesNotExist", $xml);
				$verb = FALSE;
			}
		}
	}
	
	/**
	 * This section first determins whether the document have found errors.
	 * If errors occur it is outputted with the corresponding errormessage.
	 * If no errors occur the different metadataPrefixes are executed.
	 */

	if($verb === FALSE) {
		$xml->endDocument();
		header("Content-type: text/xml");
		echo $xml->outputMemory(TRUE);
		die();
	}
	else {
		$xml->startElement($verb);
		//Denne kan v�re med � lage en sp�rring som utf�res under og s� genereres header p� samme m�te...
		switch($verb) {
			case "GetRecord":
				if(module_exists('ndla_utils') && 0) {
				  $node = ndla_utils_load_node($nid);
				}
				else {
				  $node = node_load(array('nid' => $nid));
				}

				$xml->startElement("header");
				$xml->writeElement("identifier", check_plain($_REQUEST['identifier']));
				$xml->writeElement("datestamp", gmstrftime('%Y-%m-%dT%H:%M:%SZ', $node->changed));			
				$xml->endElement();
				
				$xml->startElement('metadata');
				$xml->startElementNs(NULL, 'lom', 'http://ltsc.ieee.org/xsd/LOM');
				$xml->writeAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
				$xml->writeAttribute('xsi:scemaLocation', 'http://ltsc.ieee.org/xsd/LOM http://www.nssl.no/norlom/v1.1/norlom1-1.xsd');
				
				$query = "SELECT data FROM {utdanning_oai_export_settings} WHERE content_type = '".$node->type."' LIMIT 1";
				$db_resource = db_query($query);
				$db_array = db_fetch_array($db_resource);
				
				$profile_data = unserialize($db_array[data]);
				
				global $lom;
				$retur = _utdanning_oai_export_get_values($profile_data, $node);
				$traverseArray = array();
				foreach ($retur AS $lom_id => $values) {
					$ids = explode(".", $lom_id);
					while(!array_key_exists(implode(".", $ids), $traverseArray))
					{
						if(count($ids) > 1) {
							array_pop($ids);
							$traverseArray[implode(".", $ids)] = 0;
							$traverseArray[$lom_id] = $values;							
						}
					}
				}

				_utdanning_oai_export_LOMtraverse($xml, $traverseArray, $node);
				break;
			case "ListIdentifiers":
				$query = "SELECT n.nid AS nid, n.changed AS changed FROM {node} n WHERE n.status = 1";
				$contentType = array_key_exists($setType, node_get_types('names'));
				if(array_key_exists($setType, node_get_types('names'))) {
					$query .=" AND n.type = '".$setType."'";
				}
				else {
				  $query .= " AND n.type IN('" . implode("','", _utdanning_oai_export_get_exportable_types()) . "')";
				}

				$query_result = db_query($query);
				while($query_array = db_fetch_array($query_result)) {
					$xml->startElement("header");
					$xml->writeElement("identifier", "oai:".$_SERVER['HTTP_HOST'].":".$query_array['nid']);
					$xml->writeElement("datestamp", gmstrftime('%Y-%m-%dT%H:%M:%SZ', $query_array['changed']));
					$xml->endElement();
				}
				break;
			case "ListSets":
				$xml->startElement('metadata');
				$xml->startElementNs(NULL, 'lom', 'http://ltsc.ieee.org/xsd/LOM');
				$xml->writeAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
				$xml->writeAttribute('xsi:scemaLocation', 'http://ltsc.ieee.org/xsd/LOM http://svn.utdanning.no/projects/norlom/tags/norlom-1.0.0_beta1/xsd/norlom.xsd');

				foreach($profileArray AS $sequense => $key) {					
					$xml->startElement("set");
					$xml->writeElement("setSpec", $profileArray[$sequense]['id']);
					$xml->writeElement("setName", $profileArray[$sequense]['profile']);
					$xml->endElement();
				}
				break;
		}
	}
	
	$xml->endDocument();
	
	header("Content-type: text/xml");
	echo $xml->outputMemory(TRUE);
}

function _utdanning_oai_export_oai_responseHeader(&$xml)
{
	$xml->writeElement("responseDate", gmstrftime('%Y-%m-%dT%H:%M:%SZ'));
	$xml->startElement("request");
	if((isset($_POST['verb']) && $_POST['verb'] != "") || (isset($_GET['verb']) && $_GET['verb'] != "")) {
		$xml->writeAttribute("verb", check_plain($_REQUEST['verb']));
	}
	if((isset($_POST['identifier']) && $_POST['identifier'] != "") || (isset($_GET['identifier']) && $_GET['identifier'] != "")) {
		$xml->writeAttribute("identifier", check_plain($_REQUEST['identifier']));
	}	
	if((isset($_POST['metadataPrefix']) && $_POST['metadataPrefix'] != "") || (isset($_GET['metadataPrefix']) && $_GET['metadataPrefix'] != "")) {
		$xml->writeAttribute("metadataPrefix", check_plain($_REQUEST['metadataPrefix']));
	}
	$xml->text("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	$xml->endElement();
}

/**
 * Enter description here...
 *
 * @param array $profile_data
 * @param array $return_value
 * @return array
 */
function _utdanning_oai_export_get_values($profile_data, $node = NULL, &$return_value = array()) {	
	foreach ($profile_data AS $key => $value) {
		if (isset($value) && $value != "") {
			$return_value[$key] = _utdanning_oai_export_LOMvalue($value, $key, $node);
		}
	}
	return $return_value;
}


/**
 * Enter description here...
 *
 * @param array $input The form
 * @param array $values The traversed version of the LOM-array
 */
function _utdanning_oai_export_formTraverser(&$input, $values) {
	//func_get_arg(2) produces a warning if argument isnt passed
	if(@func_get_arg(2)) {
    $lom_traverse = func_get_arg(2); 
	}
	else {
		global $lom;
		$lom_traverse = $lom["lom"];
	}

	foreach($lom_traverse AS $id => $value)
	{
		if(is_array($value) && $id != "Vocabulary")
		{
			_utdanning_oai_export_formTraverser($input, $values, $value);
			
			foreach($value AS $key1 => $val1)
			{
				//placeholder to check for child-elements
				//would probl be the same as setting $val1 to the last element of the array
			}
			if(!is_array($val1))
			{
				if($lom_traverse[$id]['presence type'] == 'Required') $required = TRUE;
				else $required = FALSE;
				
				//The periods(.) in the $id-variable have to be replaced by underscore(_) in order for Drupal 
				//to be able to read them and communicate with the $_POST-variable.
				$varname = 'lom_'.str_replace('.', '_', $id);
				$input['utdanning_oai_export_fields'][$varname] = array(
					'#title' => $id.' - '.t($value['name']),
					'#type' => 'textfield',
					'#default_value' => $values[$id],
					'#required' => $required,
				);
			}
		}
	}
}

/**
 * Enter description here...
 *
 * @param XMLWriter-object $xml
 * @param array $values
 * @param object $node
 */
function _utdanning_oai_export_LOMtraverse(&$xml, $formatted_values, $node) {
	global $lom;
	//die("<pre>".print_r($formatted_values, TRUE)."</pre>");
	
	//func_get_arg(3) produces a warning if argument isnt passed
	if(@func_get_arg(3)) $lom_traverse = func_get_arg(3);
	else $lom_traverse = $lom["lom"];

	foreach($lom_traverse AS $id => $value)
	{
	  //error_log("ID: ".$id." => VALUE: ".print_r($value,TRUE));
		if(is_array($value))
		{
			if(array_key_exists($id, $formatted_values)) { //This removes the empty values
				
				$xml->startElement($value['attributeGroup']);
				_utdanning_oai_export_LOMtraverse($xml, $formatted_values, $node, $value);
				
				/*foreach($value AS $key1 => $val1)
				{
					//placeholder to check for child-elements
					//would probl be the same as setting $val1 to the last element of the array
				}*/
				if(!is_array($val1))
				{
					if($id == "9.2.2.1") {
						for($i = 0;$i < count($formatted_values["9.2.2.1"]);$i++) {
							if($i != 0) {
								$xml->endElement();
								$xml->startElement(_utdanning_oai_export_getProperty($lom, '[attributeGroup]', "9.2.2"));
								$xml->startElement(_utdanning_oai_export_getProperty($lom, '[attributeGroup]', "9.2.2.1"));
							}
							_utdanning_oai_export_xmlElement($formatted_values["9.2.2.1"][$i], $xml, $node->language, $lom['version']);
							$xml->endElement();
							$xml->startElement(_utdanning_oai_export_getProperty($lom, '[attributeGroup]', "9.2.2.2"));
							_utdanning_oai_export_xmlElement($formatted_values["9.2.2.2"][$i], $xml, $node->language, $lom['version']);
							$xml->endElement();
						}
					}
					else if($id == "9.2.2.2")
					{
						continue2;
					}
					else
					{
						foreach($formatted_values[$id] AS $key => $valueArray) {
							_utdanning_oai_export_xmlElement($formatted_values[$id][$key], $xml, $node->language, $lom['version']);
						}
					}
				}
				$xml->endElement();
			}
		}
	}
}

/**
 * Enter description here...
 *
 * @param array $valueArray
 * @param XMLWriter-object $xml
 * @param string $language
 * @param string $version
 */
function _utdanning_oai_export_xmlElement($valueArray, &$xml, $language, $version) {
	switch ($valueArray['type']) {
		case 'CharacterString':
		case 'DateTime':
			$xml->text($valueArray['value']);
			break;
		case 'LangString':
			$xml->startElement("string");
			$xml->writeAttribute("language", $language);
			$xml->text($valueArray['value']);
			$xml->endElement();
			break;
		case 'Vocabulary':
			$xml->writeElement('source', $version);
			$xml->writeElement('value', $valueArray['value']);
			break;
	}
}

/**
 * Enter description here...
 *
 * @param string $input
 * @return array
 */
function _utdanning_oai_export_LOMvalue($input, $key, $node = NULL)
{
	global $lom;
	//First section searches for keywords
	$keyword = array_shift(explode("=", $input, 2));
	switch ($keyword) {
		case 'value': //The keyword for values with no computation
			$string = substr($input, strlen($keyword)+1);
			$variables = explode("&", $string);
			foreach($variables AS $sub_string) {				
				$return_array[] = array(
					"type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key),
					"value" => check_plain(strip_tags(substr($sub_string, 1, -1))),
				);
			}
			return $return_array;
		case 'node_uri':
			global $base_url;
			if(variable_get('clean_url', 0) == 0) {
				return array(0 => array("type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key), "value" => $base_url."/?q=node/".$node->nid));
			}
			else {
				return array(0 => array("type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key), "value" => $base_url."/node/".$node->nid));
			}			
		case 'var':
			$string = substr($input, strlen($keyword)+1);
			$string = strip_tags($string);
			$string = str_replace(";", "", $string);
			$variables = explode("&", $string);
			foreach($variables AS $var) {
				eval("\$return_value .= ".$var.";");
				$return_array[] = array(
					"type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key),
					"value" => check_plain(strip_tags($return_value)),
				);
			}
			return $return_array;
		case 'taxonomy':
			$vid = substr(strstr($input, "="), 1);
			foreach($node->taxonomy AS $value) {
				if($value->vid == $vid) {
					$return_array[] = array(
						"type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key),
						"value" => check_plain(strip_tags($value->name)),
					);
				}
			}
			return $return_array;
		case 'vCard':
			$start = "BEGIN:VCARD\\n";
			$end = "\\nEND:VCARD\\n";
			$vCard = substr(strstr($input, "="), 1);
			$email = variable_get("site_mail", FALSE);
			if($email) {
				$vCard .= "\\nEMAIL;TYPE=internet:".$email;
			}
			
			return array(0 => array("type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key), "value" => check_plain(strip_tags($start.$vCard.$end))));
		case 'date':
			return array(0 => array("type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key), "value" => date('Y-m-d', $node->changed)));
		case 'module':
			$module_array = explode('=', $input, 2);
			$module_values = explode(';', $module_array[1], 2);
			if(module_exists(check_plain(strip_tags($module_values[0])))) {
				switch($module_values[0]) {
					case 'creativecommons_lite':
						$license_array = creativecommons_lite_get_license_array();
						return array(0 => array("type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key), "value" => "Lisens: ".check_plain(strip_tags($license_array[$node->cc_lite_license]))));
					case 'ndla_ontopia_connect':
						$grepinfo = ndla_ontopia_connect_getGrepData($node->nid,'http://ndla.no/node/'.$node->nid,'red');
						if ($module_values[1] == "9.2.2.1") {
  						foreach ($grepinfo['instanser'] as $elm) {
  						      $outstr = '';
    						    foreach ($elm['psis'] as $psidata) {
    						      $outstr .= $psidata['psi'].',';
    						    }
    						  $outstr = substr($outstr,0,-1);
  						    $return_array[] = array(
      							"type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key),
      							"value" => $outstr,
      						);
  						}
							return $return_array;
						}
						else if ($module_values[1] == "9.2.2.2")
						$grepinfo = ndla_ontopia_connect_getGrepData($node->nid,'http://ndla.no/node/'.$node->nid,'red');
						foreach ($grepinfo['instanser'] as $elm) {
						      $outstr = $elm['tittel']['nb']. " : ";
  						    foreach ($elm['psis'] as $psidata) {
  						      $outstr .= $psidata['tittel'].',';
  						    }
  						  $outstr = substr($outstr,0,-1);
						    $return_array[] = array(
    							"type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key),
    							"value" => $outstr,
    						);
						}
						return $return_array;
				}
			}
			break;
		case 'vocab':
			$vocab_string = substr(strstr($input, "="), 1);
			$vocab_values = explode(";", $vocab_string);
			foreach($vocab_values AS $vocabs) {
				$return_array[] = array(
					"type" => _utdanning_oai_export_getProperty($lom, '[data type]', $key),
					"value" => check_plain(strip_tags(substr($vocabs, 1, -1))),
				);
			}
			return $return_array;
	}
	return $input;
}

/**
 * Cred to "nick at customdesigns dot ca" who posted the latter part on php.net
 * This function is made to find a property from an array
 *
 * @param array $array
 * @param string $string
 * @param string $key
 * @return string $property
 */
function _utdanning_oai_export_getProperty($array, $string, $key) {
	$path_array = explode(".", $key);
	$first = array_shift($path_array);
	$key_path = "[".$first."]";
	$newPath = $first;
	foreach ($path_array AS $id => $value) {
		$newPath .= ".".$value;
		$key_path .= "[".$newPath."]";
	}

	$string = '[lom]'.$key_path.$string;

	preg_match_all('/\[([^\]]*)\]/', $string, $arr_matches, PREG_PATTERN_ORDER);
	$property = $array;
	foreach($arr_matches[1] as $dimension) {
		$property = $property[$dimension];
	}
	return $property;
}
