<?php
// $Id

/**
 * @file
 * @ingroup ndla_sru
 *
 * Available variables:
 * - $node: The node for this result
 * - $index: Index of this result
 */

global $base_url;
?>
<sru:record>
  <sru:recordSchema>http://purl.org/dc/elements/1.1/</sru:recordSchema>
  <sru:recordPacking>XML</sru:recordPacking>
  <sru:recordData>
    <dc:identifier><?php print $base_url . '/node/' . $node->nid?></dc:identifier>
    <dc:format>text/html</dc:format>
    <dc:type><?php print $node->type?></dc:type>
    <dc:date><?php print date(DATE_ISO8601, $node->created)?></dc:date>
    <dc:title><?php print htmlspecialchars($node->title)?></dc:title>
    <dc:creator><?php print htmlspecialchars($node->name)?></dc:creator>
    <dc:language><?php print $node->language?></dc:language>
    <dc:description><?php print ndla_sru_get_teaser($node, 2)?></dc:description>
    <dc:subject><?php print ndla_sru_get_subject($node)?></dc:subject>
    <dc:contributor><?php print ndla_sru_get_contributor($node)?></dc:contributor>
    <dc:source><?php print ndla_sru_get_source($node)?></dc:source>
    <dc:rights><?php print ndla_sru_get_rights($node)?></dc:rights>
    <dc:coverage><?php print ndla_sru_get_coverage($node)?></dc:coverage>
    <dc:publisher>NDLA</dc:publisher>
  </sru:recordData>
  <sru:extraRecordData>
    <fr:thumbnail><?php print ndla_sru_get_thumbnail($node) ?></fr:thumbnail>
  </sru:extraRecordData>
</sru:record>
