<?php 
// $Id$

/** 
 * @file
 * @ingroup ndla_sru
 */

/**
 * Create representation of node icon
 */
function ndla_sru_get_thumbnail($node) {
  global $base_url;
  return $base_url . '/' . drupal_get_path('module', 'ndla_sru') . '/images/' . $node->type . '.png';
}

/**
 * Create representation of node teaser
 * 
 * @example: http://localhost/ndla/service/sru?query=Kandidatane%20til%20fredsprisen%202009
 * 
 * @param $node - the node to produce teaser for 
 * @param $periods - the number of periods to output, 0 for all.
 */
function ndla_sru_get_teaser($node, $periods = 0) {
  return ndla_sru_strip_tags(node_teaser($node->body, $node->format), $periods);
}

/**
 * Create representation of node rights
 * 
 * @example: http://localhost/ndla/service/sru?query=Spekteranalyse
 */
function ndla_sru_get_rights($node) {
  $arr = array();
  $dbh = db_query("SELECT license FROM {creativecommons_lite} WHERE nid = %d", $node->nid);
  while ($val = db_result($dbh)) {
    $arr[] = $val;
  }
  return htmlspecialchars(implode(', ', $arr));
}

/**
 * Create representation of node coverage
 * 
 * @example: http://localhost/ndla/service/sru?query=Kristin%20B%C3%B8hle
 */
function ndla_sru_get_coverage($node) {
  $arr = array();
  if (isset($node->location) && is_array($node->location)) {
    if (!empty($node->location['name'])) {
      $arr[] = $node->location['name'];
    }
    if (!empty($node->location['street'])) {
      $arr[] = $node->location['street'];
    }
    if (!empty($node->location['additional'])) {
      $arr[] = $node->location['additional'];
    }
    if (!empty($node->location['country'])) {
      $arr[] = strtoupper($node->location['country']);
    }
  }
  return htmlspecialchars(implode(', ', $arr));
}

/**
 * Create representation of node meta
 * 
 * @example: http://localhost/ndla/service/sru?query=Kristin%20B%C3%B8hle
 * @example: http://localhost/ndla/service/sru?query=Sigurd%20Aln%C3%A6s
 * @example: http://localhost/ndla/service/sru?query=Nasjonalmuseet%20for%20kunst
 * @example: http://localhost/ndla/service/sru?query=Kandidatane%20til%20fredsprisen%202009
 */
function ndla_sru_get_meta($node) {
  //error_log('ndla_sru_get_coverage: ' . print_r($node->location, TRUE));
  $out = "";
  if (isset($node->location) && is_array($node->location)) {
    if (!empty($node->location['latitude']) && $node->location['latitude'] > 0) {
      $out .= '<dc:meta name="DC.coverage.x" scheme="UTM32W" content="' . $node->location['latitude'] . '" />'; 
    }
    if (!empty($node->location['longitude']) && $node->location['longitude'] > 0) {
      $out .= '<dc:meta name="DC.coverage.y" scheme="UTM32N" content="' . $node->location['longitude'] . '" />';
    }
  }
  
  $nd = db_fetch_object(db_query("SELECT * FROM {utdanning_negdate} WHERE nid = %d", $node->nid));
  if ($nd) {
    if (!empty($nd->utd_negdate_year) && $nd->utd_negdate_year != 0) {
      $df = mktime($nd->utd_negdate_hour, $nd->utd_negdate_minute, $nd->utd_negdate_second, $nd->utd_negdate_month, $nd->utd_negdate_day, $nd->utd_negdate_year);
      $out .= '<dc:meta name="DC.coverage.t.min" content="' . date(DATE_ISO8601, $df) . '" />';
    }
    if (!empty($nd->utd_negdate_year_to) && $nd->utd_negdate_year_to != 0) {
      $dt = mktime($nd->utd_negdate_hour_to, $nd->utd_negdate_minute_to, $nd->utd_negdate_second_to, $nd->utd_negdate_month_to, $nd->utd_negdate_day_to, $nd->utd_negdate_year_to);
      $out .= '<dc:meta name="DC.coverage.t.max" content="' . date(DATE_ISO8601, $dt) . '" />';
    }
  }
  
  return $out;
}

/**
 * Create representation of node subject
 * 
 * @example: http://localhost/ndla/service/sru?query=myr
 */
function ndla_sru_get_subject($node) {
  //error_log('ndla_sru_get_subject:' . print_r($node->taxonomy, TRUE));
  $arr = array();
  if (isset($node->taxonomy) && is_array($node->taxonomy)) {
    foreach ($node->taxonomy as $item) {
      if ($item->vid == 1) {
        $arr[] = $item->name;
      }
    }
  }
  return htmlspecialchars(implode(', ', $arr));
}

/**
 * Create representation of node contributor
 * 
 * @example: http://localhost/ndla/service/sru?query=Aktivit%C3%B8rfaget
 */
function ndla_sru_get_contributor($node) {
  //error_log('ndla_sru_get_contributor: ' . print_r($node->field_bearbeidetav, TRUE));
  $arr = array();
  if (isset($node->field_bearbeidetav) && is_array($node->field_bearbeidetav)) {
    foreach ($node->field_bearbeidetav as $item) {
      $arr[] = $item['nid'];
    }
  }
  $nid_csv = implode(',', $arr);
  if ($nid_csv != '') {
    $sql = "SELECT u.name FROM {node} n INNER JOIN {users} u ON u.uid = n.uid WHERE n.nid IN (%s)";
    $dbh = db_query($sql, $nid_csv);
    $users = array();
    while ($user = db_result($dbh)) {
      $users[] = $user;
    }
    return htmlspecialchars(implode(', ', $users));
  }
  return '';
}

/**
 * Create representation of node source
 * 
 * @example: http://localhost/ndla/service/sru?query=Gjentaking
 */
function ndla_sru_get_source($node) {
  //error_log('ndla_sru_get_source: ' . print_r($node->field_copyright, TRUE));
  $arr = array();
  if (isset($node->field_copyright) && is_array($node->field_copyright)) {
    foreach ($node->field_copyright as $item) {
      $arr[] = $item['nid'];
    }
  }
  $nid_csv = implode(',', $arr);
  if ($nid_csv != '') {
    $sql = "SELECT title FROM {node} n WHERE n.nid IN (%s)";
    $dbh = db_query($sql, $nid_csv);
    $titles = array();
    while ($title = db_result($dbh)) {
      $titles[] = $title;
    }
    return htmlspecialchars(implode(', ', $titles));
  }
  return '';
}

/**
 * Create representation of node authors
 */
function ndla_sru_get_authors($node) {
  $authors = array();
  foreach($node->authors as $author) {
    $node = ndla_utils_load_node($author['person_nid']);
    $authors[] = $node->title;
  }
  return htmlspecialchars(implode(', ', $authors));
}

/**
 * Create representation of node content type
 */
function ndla_sru_get_content_type($node) {
  $content_type = '';
  $type = ndla_utils_get_node_content_type($node);
  if(!empty($type['main']['name'])) {
    $content_type = $type['main']['name'];
  }
  return $content_type;
}

/**
 * Create representation of node preamble
 */
function ndla_sru_get_preamble($node) {
  $ingress = '';
  if(!empty($node->field_ingress[0]['value'])) {
    $ingress = strip_tags($node->field_ingress[0]['value']);
    $ingress = str_replace('&nbsp;', ' ', $ingress);
  }
  return $ingress;
}

/**
 * Create representation of node preamble picture
 */
function ndla_sru_get_preamble_picture($node) {
  global $base_url;
  $url = '';
  if(!empty($node->field_ingress_bilde[0]['nid'])) {
    $node = node_load($node->field_ingress_bilde[0]['nid']);
    if(!empty($node->images['_original'])) {
      $url = $node->images['_original'];
    }
  }
  return $base_url . '/' . $url;
}

/**
 * Reduce html to plain text
 * 
 * @see: http://nadeausoftware.com/articles/2007/09/
 * 
 * @param $str - the string to be stripped 
 * @param $periods - the number of periods to output, 0 for all.
 */
function ndla_sru_strip_tags($str, $periods = 0) {
  $str = preg_replace(
    array(
      // remove invisible content
      '@<head[^>]*?>.*?</head>@siu',
      '@<style[^>]*?>.*?</style>@siu',
      '@<script[^>]*?.*?</script>@siu',
      '@<object[^>]*?.*?</object>@siu',
      '@<embed[^>]*?.*?</embed>@siu',
      '@<applet[^>]*?.*?</applet>@siu',
      '@<noframes[^>]*?.*?</noframes>@siu',
      '@<noscript[^>]*?.*?</noscript>@siu',
      '@<noembed[^>]*?.*?</noembed>@siu',
      
      // add line breaks before & after blocks
      '@<((br)|(hr))@iu',
      '@</?((address)|(blockquote)|(center)|(del))@iu',
      '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
      '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
      '@</?((table)|(th)|(td)|(caption))@iu',
      '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
      '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
      '@</?((frameset)|(frame)|(iframe))@iu',
    ),
    array(
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
      "\n\$0", "\n\$0",
    ),
    $str
  );
  
  // remove remaining html tags
  $str = strip_tags($str);
  
  // remove whitespace and crop periods
  $str_length = strlen($str);
  $new_str = "";
  $prev_c = ' ';
  $p_count = 0;
  for ($i = 0; $i < $str_length; $i++) {
    $c = $str[$i];
    if ($c == "\n" || $c == "\r" || $c == "\t" || $c == "\0" || $c == "\x0B") {
      $c = ' ';
    }
    if ($prev_c == ' ' && $c != ' ') {
      $new_str .= $c;
    }
    elseif ($prev_c != ' ') {
      $new_str .= $c;
    }
    if ($periods > 0 && $c == '.' && ++$p_count >= $periods) {
      break;
    }
    $prev_c = $c;
  }
  return htmlspecialchars(trim($new_str));
}
