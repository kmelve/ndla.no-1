<?php 
// $Id$
/** \addtogroup ndla_sru */

/** 
 * @file
 * @ingroup ndla_sru
 * @brief
 *  NDLA SRU Service
 */

/* Implementation of hook_menu() */
function ndla_sru_menu() {
  $items = array();
  $items['admin/settings/ndla/ndla_sru'] = array(
    'title' => t('NDLA SRU Service Settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_sru_settings_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['service/sru'] = array(
    'page callback' => 'ndla_sru_service_callback',
    'page arguments' => array(),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/* Implementation of hook_form() */
function ndla_sru_settings_form() { 
  $form = array();
  $allowed_types = variable_get('ndla_sru_allowed_types', array());
  $all_types = ndla_sru_get_node_types();
  $form['ndla_sru_allowed_types'] = array(
    '#title' => t('Allowed node types'),
    '#default_value' => $allowed_types,
    '#options' => $all_types,
    '#type' => 'select',
    '#size' => count($all_types),
    '#multiple' => TRUE,
    '#description' => t('Select the node types this service will expose.')
  );
  $form['ndla_sru_apache_solr'] = array(
    '#value' => variable_get('ndla_sru_apache_solr' , TRUE),
    '#type' => 'checkbox',
    '#description' => t('User Apache Solr Module for search.')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  return $form;
}

/* Get array of all node types */
function ndla_sru_get_node_types() {
  $node_types = array();
  $result = db_query("SELECT type FROM {node_type} ORDER BY type ASC");
  while ($type = db_result($result)) {
    $node_types[$type] = $type;
  }
  return $node_types;
}

/* Implementation of hook_form_submit() */
function ndla_sru_settings_form_submit($form, $form_state) {    
  $allowed_types = $form_state['clicked_button']['#post']['ndla_sru_allowed_types'];
  $apache_solr = isset($form_state['clicked_button']['#post']['ndla_sru_apache_solr']) ? TRUE : FALSE;
  variable_set('ndla_sru_allowed_types', $allowed_types);
  variable_set('ndla_sru_apache_solr', $apache_solr);
}

/**
 * Service callback
 * 
 * @example: http://localhost/ndla/service/sru?version=1.1&operation=searchRetrieve&recordSchema=dc&maximumRecords=500&query=myr
 */
function ndla_sru_service_callback() {
  $use_solr = variable_get('ndla_sru_apache_solr' , TRUE);
  $allowed_types = variable_get('ndla_sru_allowed_types', array());
  /*
    http://www.loc.gov/standards/sru/specs/search-retrieve.html#records
    
    operation         mandatory   The string: 'searchRetrieve'.
    version	          mandatory	  The version of the request, and a statement by the client that it wants the response to be less than, or preferably equal to, that version. See Version.
    query	            mandatory	  Contains a query expressed in CQL to be processed by the server. See CQL.
    startRecord       optional	  The position within the sequence of matched records of the first record to be returned. The first position in the sequence is 1. The value supplied MUST be greater than 0. The default value if not supplied is 1.
    maximumRecords    optional	  The number of records requested to be returned. The value must be 0 or greater. Default value if not supplied is determined by the server. The server MAY return less than this number of records, for example if there are fewer matching records than requested, but MUST NOT return more than this number of records.
    recordPacking     optional	  A string to determine how the record should be escaped in the response. Defined values are 'string' and 'xml'. The default is 'xml'. See Records.
    recordSchema      optional	  The schema in which the records MUST be returned. The value is the URI identifier for the schema or the short name for it published by the server. The default value if not supplied is determined by the server. See Record Schemas.
    resultSetTTL	    optional	  The number of seconds for which the client requests that the result set created should be maintained. The server MAY choose not to fulfil this request, and may respond with a different number of seconds. If resultSetTTL is not supplied then the server will determine the value. See Result Sets.
    stylesheet	      optional  	A URL for a stylesheet. The client requests that the server simply return this URL in the response. See Stylesheets.
  */
  
  $parameters = array(
    'operation' => array(
      'default' => 'searchRetrieve',
      'match' => '/^searchRetrieve$/',
    ),
    'version' => array(
      'default' => '1.2',
      'match' => '/^(1.1|1.2)$/',
    ),
    'query' => array(
      'default' => NULL,
      'match' => '/^.+$/',
    ),
    'startRecord' => array(
      'default' => '0',
      'match' => '/^[0-9][\d]*$/',
    ),
    'maximumRecords' => array(
      'default' => '10',
      'match' => '/^[\d]*$/',
    ),
    'recordPacking' => array(
      'default' => 'xml',
      'match' => '/^(xml|string)$/',
    ),
    'recordSchema' => array(
      'default' => 'dc',
      'match' => '/^dc$/',
    ),
    'resultSetTTL' => array(
      'defaul' => '5',
      'match' => '/^[\d]*$/',
    ),      
    'stylesheet' => array(
      'default' => '',
      'match' => '/^.*$/',
    ),
  );
  
  $input = array();
  $error = array();
  foreach($parameters as $name => $value) {
    $input[$name] = isset($_GET[$name]) ? check_plain($_GET[$name]) : $value['default'];
    if(!preg_match($value['match'], $input[$name])) {
      $error[] = $name . ' "' . $input[$name] . '" is not supported.';
    }
  }
  
  if(!empty($error)) {
    print("NDLA SRU:\n<br>- ". implode("\n# ", $error));
    exit;
  }
  
  drupal_set_header('Content-Type: text/xml; charset=utf-8');
  if($use_solr) {
    $result = ndla_sru_search_retrieve_solr($input['query'], $input['maximumRecords'], $input['startRecord']);
  } else {
    $result = ndla_sru_search_retrieve($input['query'], $input['maximumRecords']);
  }
  $query = "SELECT n.nid, n.vid, n.type, n.status, s.publish_on, s.unpublish_on, s.timezone FROM {node} n LEFT JOIN {scheduler} s ON s.nid = n.nid WHERE n.nid = %d";
  $now = time();
  $items = array();
  foreach ($result as $item) {
    if(!$use_solr) {
      $item->nid = $item->sid;
    }
    $data = db_fetch_object(db_query($query, $item->nid));
    if ($data->publish_on > 0 && $data->publish_on > $now) {
      continue;
    }
    if ($data->unpublish_on > 0 && $data->unpublish_on < $now) {
      continue;
    }
    if ($data->status != 1 || !in_array($data->type, $allowed_types)) {
      continue;
    }
    $node = node_load($data->nid, NULL, FALSE);
    if ($node) {
      $items[] = theme('sru_result', $node);
    } else {
      error_log('ndla_sru_search_retrieve: node_load(' . $n_obj->nid . ') failed!');
    }
  }
  echo theme('sru_results', implode($items), sizeof($items), $input['stylesheet']);
  exit();
}

function ndla_sru_search_retrieve($query, $limit) {
  $result = do_search($query, 'node');
  if(sizeof($result) > $limit) {
    $result = array_chunk($result, $limit);
    return $result[0];
  }
  return $result;
}

function ndla_sru_search_retrieve_solr($query, $limit, $start) {
  $params = array(
    'fl' => 'nid',
    'start' => $start,
    'rows' => $limit,
    'facet' => 'true',
    'facet.mincount' => 1,
    'facet.sort' => 'true'
  );
  
  try {
    $path = drupal_get_path('module', 'ndla_sru');
    require_once($path . '/CqlParser.php');
    $data = CqlParser::parse($query);  
    $query = '';
    if(!is_array($data)) {
      $query = $data;
      $data = array();
    }
    $query = apachesolr_drupal_query($query, NULL, NULL, '');
    if(!empty($data)) {
      $params['fq'][] = urldecode(CqlParser::build_solr_query($data));
    }
    $solr = apachesolr_get_solr();
    apachesolr_search_add_facet_params($params, $query);
    apachesolr_search_add_boost_params($params, $query, $solr);
    $response = $solr->search(htmlspecialchars($query->get_query_basic(), ENT_NOQUOTES, 'UTF-8'), $params['start'], $params['rows'], $params);
    return $response->response->docs;
  } catch( Exception $e ) {
    $host = variable_get('apachesolr_host', 'localhost');
    $port = variable_get('apachesolr_port', '8983');
    $path = variable_get('apachesolr_path', '/solr');
    print '<error>NDLA SRU: search service: http://' . $host . ':' . $port . '/' . $path . ' unavailable!</error>';
    print '<exception>search service unavailable!</exception>';
    exit();
  }
}

/* Implementation of hook_theme() */
function ndla_sru_theme() {
  return array(
    'sru_result' => array(
      'arguments' => array('node' => NULL),
      'file' => 'ndla_sru.pages.inc',
      'template' => '/templates/sru-result'
    ),
    'sru_results' => array(
      'arguments' => array('results_data' => NULL, 'results_count' => NULL, 'stylesheet' => NULL),
      'file' => 'ndla_sru.pages.inc',
      'template' => '/templates/sru-results'
    ),
  );
}