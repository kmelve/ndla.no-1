<?php

class CqlParser {
  
  static $fields = array(
    'title' => 'title',
    'ss_license' => 'license',
    'body' => 'body',
    'nid' => 'nid',
    'type' => 'nodetype',
    'sm_vid_Innholdstype' => 'contenttype',
    'ss_subject' => 'subject',
  );
  
  function parse($line, $first = true) {
    $line = trim($line);
    $line = str_replace(array('&quot;', "'"), '"', $line);
    $line = preg_replace('/ and /i', ' and ', $line);
    $line = preg_replace('/ or /i', ' or ', $line);
    
    /* Remove sort */
    if(preg_match('/^.*([\s]+sortBy[\s]+.*)$/', $line, $matches)) {
      $sort = preg_replace('/^[\s]*sortBy[\s]+/', '', $matches[1]);
      $sorts = explode(' ', $sort);
      $line = str_replace($matches[1], '', $line);
    }
    
    if(self::isOr($line)) {
      return self::parseOr($line);
    }
    if(self::isAnd($line)) {
      return self::parseAnd($line);
    }
    if(self::isParentes($line)) {
      return self::parseParentes($line);
    }
    if(self::isOperation($line)) {
      return self::parseOperation($line);
    }
    if($first) {
      if(self::isText($line)) {
        return $line;
      }
    }
    throw new Exception($line);
  }
  
  function isOr($line) {
    $line = preg_replace('/\([^\)]*\)/', '', $line);
    return preg_match('/ or /', $line);
  }
  
  function isAnd($line) {
    $line = preg_replace('/\([^\)]*\)/', '', $line);
    return preg_match('/ and /', $line);
  }
  
  function isParentes($line) {
    return preg_match('/^\(.*\)$/', $line);
  }
  
  function isOperation($line) {
    $fields = implode('|', self::$fields);
    if(preg_match('/^(' . $fields . ')[\s]*(=|!=)(.*)$/', $line, $matches)) {
      return self::isText(trim($matches[3])); 
    }
    return false;
  }
  
  function isText($line) {
    return true;
  }
  
  function parseOr($line) {
    $data = array();
    $offset = 0;
    $in = 0;
    while($offset < strlen($line)) {
      $offset++;
      if(substr($line, $offset - 1, 1) == '(') {
        $in++;
      }
      if(substr($line, $offset - 1, 1) == ')') {
        $in--;
      }
      if(substr($line, $offset, 4) == ' or ' && $in == 0) {
        $data['#or'][] = self::parse(substr($line, 0, $offset), false);
        $line = substr($line, $offset + 4);
        $offset = 0;
      }
    }
    if(!empty($data['#or'])) {
      $data['#or'][] = self::parse($line, false); 
    }
    return $data;
  }
  
  function parseAnd($line) {
    $data = array();
    $offset = 0;
    $in = 0;
    while($offset < strlen($line)) {
      $offset++;
      if(substr($line, $offset - 1, 1) == '(') {
        $in++;
      }
      if(substr($line, $offset - 1, 1) == ')') {
        $in--;
      }
      if(substr($line, $offset, 5) == ' and ' && $in == 0) {
        $data['#and'][] = self::parse(substr($line, 0, $offset), false);
        $line = substr($line, $offset + 4);
        $offset = 0;
      }
    }
    if(!empty($data['#and'])) {
      $data['#and'][] = self::parse($line, false); 
    }
    return $data;
  }
  
  function parseParentes($line) {
    $line = preg_replace('/^\(/', '', $line);
    $line = preg_replace('/\)$/', '', $line);
    return array(
      '#parentes' => self::parse($line, false),
    ); 
  }
  
  function parseOperation($line) {
    $fields = implode('|', self::$fields);
    preg_match('/^(' . $fields . ')[\s]*(=|!=|any)(.*)$/', $line, $matches);
    return array(
      '#operation' => array(
        '#field' => $matches[1],
        '#operator' => $matches[2],
        '#value' => trim($matches[3]),
      ),
    );
  }
  
  function build_solr_query($data) {
    $string = '';
    foreach($data as $key => $value) {
      switch($key) {
        case '#parentes':
          $string .= '(' . self::build_solr_query($value) . ')';
          break;
        case '#and':
          $parts = array();
          foreach($value as $part) {
            $parts[] = self::build_solr_query($part);
          }
          $string .= implode(' AND ', $parts);
          break;
        case '#or':
          $parts = array();
          foreach($value as $part) {
            $parts[] = self::build_solr_query($part);
          }
          $string .= implode(' OR ', $parts);
          break;
        case '#operation':
          $fields = array_flip(self::$fields);
          if($value['#operator'] == '!=') {
            $string .= '-';
          }
          $string .= $fields[$value['#field']] . ':' . $value['#value'];
          break;
        default:
          throw new Exception($line);
      }
    }
    return $string;
  }
  
}