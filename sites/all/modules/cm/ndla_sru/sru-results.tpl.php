<?php
// $Id

/**
 * @file
 * @ingroup ndla_sru
 *
 * Available variables:
 * - $results_data: String of results rendered by sru-result.tpl.php
 * - $results_count: Number of results
 */

echo("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
?>
<sru:searchRetrieveResponse
  xmlns:sru="http://www.loc.gov/zing/srw/"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:fr="http://fronter.com/fronterlre/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.loc.gov/zing/srw/ http://www.loc.gov/standards/sru/sru1-1archive/xml-files/srw-types.xsd http://purl.org/dc/elements/1.1/ http://dublincore.org/schemas/xmls/simpledc20021212.xsd">
  <sru:version>1.1</sru:version>
  <sru:numberOfRecords><?php print $results_count ?></sru:numberOfRecords>
  <sru:records>
    <?php print $results_data ?>
  </sru:records>
</sru:searchRetrieveResponse>
