function NdlaMyls() {
  var selected = userData.getData('page', 'learningStatus'),
    that = this,
    $block = $('<div class="block my-learning-status"><h2 title="' + Drupal.t('Use the buttons below to indicate how well you understand and master the material on this site in relation to your personal goals') + '">' + Drupal.t('Learning status') + '</h2><p>' + Drupal.t('This page is') + ':</p><ul><li class="green" title="' + Drupal.t('The content on this site are reviewed, and I feel I have good control') + '">' + Drupal.t('Learned and understood') + '</li><li class="yellow" title="' + Drupal.t("The content on this page have been reviewed, but I'm not sure I master the material as well that I want") + '">' + Drupal.t('Perceived, but must be repeated') + '</li><li class="red" title="' + Drupal.t('I have not worked well enough with the content of this page and need more time and / or assistance') + '">' + Drupal.t('Not sufficiently understood') + '</li></ul></div>').appendTo('.region-learning');
  this.$lis = $('li', $block);
  this.store = false;
  this.$lis.click(function () {
    that.select($(this));
  });
  if (selected != undefined && selected) {
    this.select(this.$lis.filter('.' + selected.color));
  }
  this.store = true;
}

NdlaMyls.prototype.select = function ($li) {
  var isSelected = $li.hasClass('selected');
  this.$lis.removeClass('selected').removeClass('not-selected');
  if (!isSelected) {
    if (this.store) {
      userData.saveData('page', 'learningStatus', {
        color: $li.attr('class'), 
        title: $(document).attr('title')
      });
      if($('a.learningcurve').length == 1 && !$('a.learningcurve').hasClass('marked')) {
        $('a.learningcurve').click();
      }
    }
    $li.addClass('selected');
    this.$lis.not($li).addClass('not-selected');
  }
  else if (this.store) {
    userData.removeData('page', 'learningStatus');
  }
}

userData.ready(function () {
  new NdlaMyls();
});