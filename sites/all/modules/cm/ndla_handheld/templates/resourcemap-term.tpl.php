<li>
  <span class="resourcemap__item">
    <?php print l($term->name, 'search/apachesolr_search', $term->url_options) ?>
    <span class='resourcemap'>(<?php print $term->resources; ?>)</span>
  </span>
  <?php if(!empty($children)) print '<ul>' . $children . '</ul>' ?>
</li>
