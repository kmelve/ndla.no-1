<?php global $base_path; ?>

<p><img src="<?php print $base_path; ?>sites/all/themes/ndla_mobile/img/logo.png"></p>
<p>Nettstedet er utarbeidet av <a href="http://om.ndla.no">NDLA</a> som <a href="http://bak.ndla.no">åpen kildekode</a>, basert på <a href="http://drupal.org">Drupal</a></p>
<p>Ansvarlig redaktør: Øivind Høines<br>Webredaktør: Jarle Ragnar Meløy</p>

<ul>
    <li class="first"><?php print l(t('Contact'), variable_get('ndla_handheld_footer_contact_path', '')); ?></li>
    <li><a href="http://om.ndla.no"><?php print t('About NDLA'); ?></a></li>
    <li><?php print l(t('Help'), variable_get('ndla_handheld_footer_help_path', '')); ?></li>
    <li><a href="http://om.ndla.no/nyhetsbrev"><?php print t('Newsletter'); ?></a></li>
    <li><a href="http://ndla.no/<?php print $_GET['q'] ?>"><?php print t('Go to full version'); ?></a></li>
</ul>
