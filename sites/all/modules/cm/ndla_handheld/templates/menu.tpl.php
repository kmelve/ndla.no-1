<?php global $base_path; ?>
<div id="menucontainer">
  <?php foreach($menu as $i => $div): ?>
		<div class='menu-page <?php echo $status; $status = 'next-menu-page' ?>' id='menu-<?php print $i ?>'>
  <div class="menu-header">
    <div class="ndla-logo">
      <a href='<?php echo url("<front>"); ?>'>
        <img src="<?php print $base_path; ?>sites/all/themes/ndla_mobile/img/logo.png" />
      </a>
    </div>
    <div class="topic-name"><?php echo $topic_name ?></div>
  </div>
			<?php foreach($div as $parent_id => $ul): ?>
				<ul id='<?php echo 'parent-node-'.$parent_id; ?>'>
				<?php if($i != 0): ?>
        <li class="back"><i class="icon-arrow-left"></i> <span data-parent-id='<?php echo $parent_id; ?>'>Back</span></li>
				<?php endif; ?>
				<?php foreach($ul as $parent_id => $li): ?>

					<?php if(!empty($li['header'])): ?>
						<h2><?php echo $li['header']; ?></h2>
					<?php endif; ?>

          <li class="link" id="<?php echo $li['menu_id'] ?>" data-name="<?php echo $li['name'] ?>">
						<?php if( ! empty($li['has_children']) ): ?>
							<i class="icon icon-chevron-right"></i>
						<?php endif; ?>
						<?php if(!empty($li['path'])): ?>
						  <?php if(strpos($li['path'], '?') === FALSE): ?>
							  <a href="<?php echo url($li['path']); ?>"><?php echo $li['title']; ?></a>
							<?php else: ?>
							  <a href="<?php echo $li['path']; ?>"><?php echo $li['title']; ?></a>
							<?php endif; ?>
						<?php else: ?>
							<?php echo $li['title']; ?>
						<?php endif; ?>
					</li>
				<?php endforeach ?>
				</ul>
			<?php endforeach ?>
		</div>
	<?php endforeach; ?>
</div>
