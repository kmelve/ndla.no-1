<?php
/** \addtogroup ndla_deling_oppgave */

/**
 * @file
 * @ingroup ndla_deling_oppgave
 * @brief
 *  Deling exercise link module for NDLA
 */


/**
* Implementation of hook_menu_alter()
*
* Override the access callback for node/add 
* so we can redirect non-logged in users to auth 
*/
function ndla_deling_oppgave_menu_alter(&$menu) {
  $menu['node/add/quiz']['access callback'] = 'ndla_deling_oppgave_check_access';
  $menu['node/add/quiz']['access arguments'] = array(2);
  $menu['node/add/artikkel']['access callback'] = 'ndla_deling_oppgave_check_access';
  $menu['node/add/artikkel']['access arguments'] = array(2);
}

function ndla_deling_oppgave_check_access($node_type) {
  global $user;
  global $base_url;

  // Redirect to auth if not logged in
  if (!$user->uid) {
    $seriaurl = $base_url.'/seriaauth/authenticate?continue=' . urlencode('/node/add/' . $node_type . ($_GET['ndla-node'] ? '?ndla-node=' . $_GET['ndla-node'] : ''));
    drupal_goto($seriaurl);
  } else {
    return TRUE;
  }
}

/**
  * Implementation of hook_menu()
  */
function ndla_deling_oppgave_menu(){
  $items = array();
  
  //Menu callback for the settings
  $items['admin/settings/ndla_deling_oppgave'] = array(
    'title' => t('NDLA Deling Oppgave'),
    'description' => t('Configure NDLA Deling Oppgave'),
    'page callback' => 'drupal_get_form',
    'page arguments' =>  array('ndla_deling_oppgave_settings_form'),
    'access arguments' => array('administer ndla_deling_oppgave')
  );

  return $items;
}

/**
 * Implementation of hook_perm()
 */
function ndla_deling_oppgave_perm() {
  return array('administer ndla_deling_oppgave');
}


/**
 * Implementation of hook_theme()
 */
function ndla_deling_oppgave_theme() {
  return array(
    'ndla_deling_oppgave_node_form' => array(
      'arguments' =>array('node' => NULL),
    ),
    'ndla_deling_oppgave_node_table' => array(
      'arguments' => array(),
    ),
    'ndla_deling_oppgave_button' => array(
      'arguments' => array('deling_url' => NULL, 'nid' => NULL),
      'template' => 'ndla-deling-oppgave-button',
    ),
  );
}

function ndla_deling_oppgave_settings_form() {
  $type_options = array();
  $node_types = node_get_types();
  foreach($node_types as $type) {
    $type_options[$type->type] = $type->name;
  }
  
  $form['ndla_deling_oppgave_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show the Deling oppgave icon on the following content types'),
    '#default_value' => variable_get('ndla_deling_oppgave_node_types', array()),
    '#options' => $type_options,
  );
  
  $form['deling_oppgave_assoctypes_context_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Association type context key'),
    '#default_value' => variable_get('deling_oppgave_assoctypes_context_key', ''),
  );
  
  $form['deling_oppgave_deling_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Deling URL'),
    '#default_value' => variable_get('deling_oppgave_deling_url', ''),
  );
  
  $form['deling_oppgave_deling_test_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Deling test URL'),
    '#default_value' => variable_get('deling_oppgave_deling_test_url', ''),
  );
  
  
  $form['deling_oppgave_context'] = array(
    '#type' => 'radios',
    '#title' => t('Set context'),
    '#default_value' => variable_get('deling_oppgave_context', ''),
    '#options' => array('ndla' => t('NDLA'), 'deling' => t('Deling')),
  );
  
  return system_settings_form($form);
}

/**
 * Implementation of hook_nodeapi
 * @param $node
 * @param $op
 * @param $a3
 * @param $a4
 */
function ndla_deling_oppgave_nodeapi(&$node, $op, $a3, $a4) {
  $context = variable_get('deling_oppgave_context', '');
  switch ($op) {
    case 'insert':
      if($context == 'deling'){
        ndla_deling_oppgave_save_relations($node->ndla_deling_oppgave_table['aims'],$node->nid,$node->title,$node->teaser,$node->cc_lite_license,$node->ndla_deling_oppgave_table['ndla_nid']);
      }
      break;
  }
}



/**
 * Implementation of hook_form_alter
 * 
 * @param array $form
 * @param array $form_state
 * @param string $form_id
 */
function ndla_deling_oppgave_form_alter(&$form, $form_state, $form_id) {
  drupal_add_css(drupal_get_path('module', 'ndla_deling_oppgave') . '/ndla_deling_oppgave.css','theme');
  $context = variable_get('deling_oppgave_context', '');
  if($context == 'deling' && ("artikkel_node_form" == $form_id || "quiz_node_form" == $form_id) && arg(1) == 'add' && isset($_GET['ndla-node'])){
    if (isset($form['type']) && isset($form['#node'])) {
      $node = $form['#node'];
    }
    
    $form['ndla_deling_oppgave'] = array(
        '#type'          => 'fieldset',
        '#title'         => t('Competence aims from NDLA'). ' <span class="red">*</span>',
        '#collapsible'   => TRUE,
        '#collapsed'     => FALSE,
        '#prefix'        => '<div class="ndla_deling_oppgave">',
        '#suffix'        => '</div>',
        '#weight'        => 20,
        '#required' => TRUE,
    );

  $form['ndla_deling_oppgave']['ndla_deling_oppgave_table'] = ndla_deling_oppgave_node_form($form_state, $node, TRUE,$_GET['ndla-node']);
  }//end if deling-oppgave
}


function ndla_deling_oppgave_node_form($form_state, $node, $is_embedded = FALSE,$ndlanid) {
  
  if(!is_array($node->ndla_oppgave_aims)) {
    $node->ndla_oppgave_aims = array();
  }
  $form['#node'] = $node;
  $form['#prefix'] = "<center><h3>".t('Competence aims from NDLA')."<h3></center>";
  $form['#prefix'] .= '<div class="form-item" id="ndla_oppgave_aims-table">';
  $form['#suffix'] = '</div>';
  $form['#theme'] = 'ndla_deling_oppgave_node_form';
  $form['#tree'] = TRUE;
  
  
  $endpoint = RELATE_SERVICE."/services/xmlrpc";
  $method_name = 'get.aims_by_resource';
  $nodeurl = "http://ndla.no/node/".$ndlanid;
  
  $required_args = array($nodeurl,'nn','json');
  
  $xmlrpc_args = array_merge(
    array($endpoint, $method_name),
    $required_args
  );
  
  $result = call_user_func_array('xmlrpc', $xmlrpc_args);
 
  if(xmlrpc_errno()) {
    $msg = xmlrpc_error_msg();
    watchdog('ndla_deling_oppgave',t("Error fetching GREP relations from relate.ndla.no: ").$msg,NULL, WATCHDOG_ERROR,$nodeurl);
  }
  
  $resdata = json_decode($result);
  $aims = array();
  foreach($resdata as $associd => $assocobj) {
    foreach($assocobj->aims as $elm) {
     $aims[$elm->topicref] = $elm->title;
    }//end foreach
  }//end foreach
  
  
  $tmp_assoctypes = ndla_ontopia_connect_getAssoctypes();
  $deling_assoctypes = array();
  $deling_assoctypes[''] = t('Choose relation type');
  foreach($tmp_assoctypes as $id => $data) {
    $deling_assoctypes[$id] = $data['kompetansemaal'];
  }
  
  if(!$form['aims']) {
    $form['aims'] = array('#tree' => TRUE);
    foreach($aims as $uuid => $title) {
        ndla_deling_oppgave_create_row($form, $form_state, $uuid, $title,$deling_assoctypes,$ndlanid);
    }
  }
  if(!$form['ndla_nid']) {
    $form['ndla_nid'] = array(
      '#type' => 'hidden',
      '#value' => $ndlanid,
    );
  }
  return $form;
}

function ndla_deling_oppgave_create_row(&$form, $form_state, $aim, $title, $relations) {
  $form['aims'][$aim] = array(
    '#tree' => TRUE,
    'aim' => array(
      '#type' => 'markup',
      '#value' => '<input name="ndla_deling_oppgave_aims[]" value="'.$aim.'" checked="checked" type="checkbox"  class="checkbox_hidden" />'
    ),
    'title' => array(
      '#type' => 'item',
      '#value' => $title,
    ),
    'relationtype' => array(
      '#type' => 'select',
      '#options' => $relations,
      '#default_value' => '',
      "#required" => TRUE,
    ),
  );
}

function theme_ndla_deling_oppgave_node_form($form) {
  $head = array('', t('Competence aim'), t('Relation type'));
  $rows = array();

  if($form['aims'] && count($form['aims']) > 0) {
    foreach($form['aims'] as $uuid => $title) {
      if($form['aims'][$uuid] && is_array($form['aims'][$uuid]) && strpos($uuid,'uuid') !== false){
        $related_nodes[$uuid] = array(
          'aim' => drupal_render($form['aims'][$uuid]['aim']),  
          'title' =>  drupal_render($form['aims'][$uuid]['title']),
          'relationtype' => drupal_render($form['aims'][$uuid]['relationtype']),
        );
      }
      
    }
  }
  
  if($related_nodes) {
    foreach ($related_nodes as $uuid => $data) {     
      $rows[] = array(
        'data' => array(
          $data['aim'],
          $data['title'],
          $data['relationtype']
        ),
      );
    }
  }
  
  unset($related_nodes);
  return drupal_render($form) . theme('table', $head, $rows, array('class' => 'aim_table', 'id' => 'ndla_deling_oppgave_table'));
  
}


function ndla_deling_oppgave_getAssoctypes() {
  
$endpoint = RELATE_SERVICE."/services/xmlrpc";
  $method_name = 'get.assoctypes';
  $context_key = variable_get('deling_oppgave_assoctypes_context_key', '');
  
  $required_args = array('nn',$context_key,'json');
  $xmlrpc_args = array_merge(
    array($endpoint, $method_name),
    $required_args
  );
  
  $result = call_user_func_array('xmlrpc', $xmlrpc_args);
  
  if(xmlrpc_errno()) {
    $msg = xmlrpc_error_msg();
    watchdog('ndla_ontopia_connect:greptray',t("Error fetching GREP curricula for the greptray from relate.ndla.no: ").$msg,NULL, WATCHDOG_ERROR,$nodeurl);
  }
  
  return obj_toArray(json_decode($result));
}

/** 
 * Implementation of hook_block
 * @param $op
 * @param $delta
 * @param $edit
 */
function ndla_deling_oppgave_block($op = 'list', $delta = 0, $edit = array()){
  switch ($op) {
    case 'list':
      $blocks['deling_oppgave_list'] = array(
        'info' => t('Exercises from Deling'),
        'title' => t('Exercises from Deling'),
        'weight' => 0, 
        'cache' => BLOCK_NO_CACHE,
      );
      return $blocks;
      break;
    case 'view':
      $context = variable_get('deling_oppgave_context', '');
      switch($delta) {
        case 'deling_oppgave_list':
         $args = arg();
         if(!module_exists('ndla_nygiv')){
           if ($args[0] == 'node' && $args[1] == intval($args[1]) && $args[2] != "edit"){
             $block['subject'] = t('Exercises from Deling');
             $block['content'] = ndla_deling_oppgave_show_DelingOppgaver(arg(1));
           }
         }
          break; 
      }
      return $block;
      break;
  }
}


function ndla_deling_oppgave_create_icon(){
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $path = drupal_get_path('module', 'ndla_deling_oppgave');
    drupal_add_css($path . '/ndla_deling_oppgave.css');
    $q = "SELECT type FROM {node} WHERE nid = %d";
    $type = db_result(db_query($q, arg(1)));
    $allowed_types = array_values(variable_get('ndla_deling_oppgave_node_types', array()));
    if (in_array($type, $allowed_types)) {
      $deling_url = variable_get('deling_oppgave_deling_url', '');
      $deling_test_url = variable_get('deling_oppgave_deling_test_url', '');
      if ($deling_test_url != '') {
        $deling_url = $deling_test_url;
      }
      $content = theme('ndla_deling_oppgave_button', $deling_url, arg(1));
    }
    else {
       $content = '';
    }
  }
  return $content;
}

function ndla_deling_oppgave_show_DelingOppgaver($nid) {
  global $language;
  $content = '';
  $endpoint = RELATE_SERVICE."/services/xmlrpc";
  $method_name = 'get.deling_exercises';
  $nodeurl = 'http://ndla.no/node/'.$nid;
  $required_args = array($nodeurl,$language->language,'false');
  $xmlrpc_args = array_merge(
    array($endpoint, $method_name),
    $required_args
  );
  
  $result = call_user_func_array('xmlrpc', $xmlrpc_args);
  if(xmlrpc_errno()) {
    $msg = xmlrpc_error_msg();
    watchdog('ndla_ontopia_connect:greptray',t("Error fetching GREP curricula for the greptray from relate.ndla.no: ").$msg,NULL, WATCHDOG_ERROR,$nodeurl);
  }
  if (!$result) {
    $result = t('You can be the first to create an exercise for this page!');
  }
  return $result . ndla_deling_oppgave_create_icon(); 
}

/**
 * Saves the chosen aims to relate
 * @param array $aims
 * @param $nid
 */
function ndla_deling_oppgave_save_relations($aims,$nid,$nodetitle,$ingress,$lisens,$ndlanid){
  global $user;
  date_default_timezone_set('Europe/Oslo');
  $naa = date('j').'/'.date('n').'/'.date('Y').' '.date('H').':'.date('i').':'.date('s');
  $dato = mktime($naa);
  
  if($ndlanid != "") {
    
    $ndla_topicid = 'ndlanode_'.$ndlanid;
    $oppgave_assoctypes = array_keys(ndla_deling_oppgave_getAssoctypes());
    
    $ltm .= '@"utf-8" ';  
    $oppgaveltm = '@"utf-8" ';
    $topicid = 'delingnode_'.$nid;
    
    $dnoderes = _ndla_ontopia_connect_sjekkdata('http://deling.ndla.no/node/'.$nid,'ressurs','','');
    
    if($dnoderes->values[0]->topicname == ''){
      $restopic = '['.$topicid.' : deling-node = "'._ndla_deling_oppgave_unicodeTitle($nodetitle).'" /nb language-neutral @"http://deling.ndla.no/node/'.$nid.'"]';
      $resocc .= ' {'.$topicid.', published, [[true]]}';
      $resocc .= ' {'.$topicid.', resourcetype, "http://psi.topic.ndla.no/#exercise"}';
      $resocc .= ' {'.$topicid.', node-ingress, [['.$ingress.']]}';
      $resocc .= ' {'.$topicid.', node-usufruct, "http://creativecommons.org/licenses/'.$lisens.'"} /en';
      $resocc .= ' {'.$topicid.', node-usufruct, "http://creativecommons.org/licenses/'.$lisens.'/no"} /nb';
      $ltm .= $restopic. ' ' .$resocc;
    }
    
    
    $delingurl = 'http://deling.ndla.no/node/'.$nid;
    $teller = 0;
    $nteller = 0;
    $aimarr = array($delingurl => array());
    $relarr = array($delingurl => array());
    foreach($aims as $uuid => $data) {
      if('' != $data['relationtype']) {
        array_push($aimarr[$delingurl],$uuid);
        array_push($relarr[$delingurl],$data['relationtype']);
        $maalres = _ndla_ontopia_connect_sjekkdata($uuid,'kompetansemaal',$data['relationtype'],'','http://deling.ndla.no/node/'.$nid);
        
        if($maalres[0]->duplikatassoc == 'false'){
          $reifierid = 'a_'.$dato;
        
          $resassoc = $data['relationtype'].'('.$topicid.' : laeringsressurs, '.$uuid.' : kompetansemaal) ~ '.$reifierid.'_'.$teller;
          $ltm .= ' '.$resassoc;
        
        
          $reifier = ' ['.$reifierid.'_'.$teller.' = " reifier for '.$reifierid.'_'.$teller.'" /topic-name ]';
          $reifier .= ' {'.$reifierid.'_'.$teller.', timestamp, [['.$dato.']]}';
          $reifier .= ' {'.$reifierid.'_'.$teller.', bruker-navn, [['.$user->name.']]}';
          $reifier .= ' {'.$reifierid.'_'.$teller++.', bruker-url, "http://deling.ndla.no/user/'.$user->uid.'"}';
          
          $ltm .= ' '.$reifier;
          
          db_query("INSERT INTO {ndla_deling_courses_node_aims} (node_id, aim_uuid, user_id) VALUES (%d, '%s', %d)", $nid, $uuid, $user->uid);
          
        }
      }
    }
    
    $tres = _add_fragment($ltm);
    
      if($oppgave_assoctypes[0]) {
        $nreifierid = 'n_'.$dato;
        $oppgaveltm .= ' '.$oppgave_assoctypes[0].'('.$ndla_topicid.' : ndla-node, '.$topicid.' : deling-node) ~ '.$nreifierid;
    
        $nreifier = ' ['.$nreifierid.' = " reifier for '.$nreifierid.'" /topic-name ]';
        $nreifier .= ' {'.$nreifierid.', timestamp, [['.$dato.']]}';
        $nreifier .= ' {'.$nreifierid.', bruker-navn, [['.$user->name.']]}';
        $nreifier .= ' {'.$nreifierid.', bruker-url, "http://deling.ndla.no/user/'.$user->uid.'"}';
        
        $oppgaveltm .= $nreifier;
        array_push($aimarr[$delingurl],$topicid);
        array_push($relarr[$delingurl],'oppgave-til');
      }
    $nres = _add_fragment($oppgaveltm);
  }
}


/** UTILS **/
 
function _ndla_deling_oppgave_unicodeSpecialchars($fragment) {
  $fragment = str_replace('æ', '\u00E6',$fragment);
  $fragment = str_replace('Æ', '\u00C6',$fragment);
  $fragment = str_replace('ø', '\u00F8',$fragment);
  $fragment = str_replace('Ø', '\u00D8',$fragment);
  $fragment = str_replace('å', '\u00E5',$fragment);
  $fragment = str_replace('Å', '\u00C5',$fragment);
  return $fragment;
}

function _ndla_deling_oppgave_unicodeTitle($title) {
  $title = str_replace('Æ', '\u00C6',$title);
  $title = str_replace('ø', '\u00F8',$title);
  $title = str_replace('Ø', '\u00D8',$title);
  $title = str_replace('å', '\u00E5',$title);
  $title = str_replace('Å', '\u00C5',$title);
  $title = str_replace('"', '\u0022',$title);
  $title = str_replace('\'', '\u2019',$title);
  $title = str_replace('(', '\u0029',$title);
  $title = str_replace(')', '\u0028',$title);
  $title = str_replace('[', '\u005D',$title);
  $title = str_replace(']', '\u005B',$title);
  $title = str_replace(':', '\u003A',$title);
  $title = str_replace('/', '\u2215',$title);
  $title = str_replace('@', '\u0040',$title);
  $title = str_replace('~',$title,$title);
  $title = str_replace('ü', '\u00FC',$title);
  $title = str_replace('Ü', '\u00DC',$title);
  $title = str_replace('ö', '\u00F6',$title);
  $title = str_replace('Ö', '\u00D6',$title);
  return $title;
}

function _ndla_deling_oppgave_simplifySpecialchars($fragment) {
  $fragment = str_replace('æ', 'ae',$fragment);
  $fragment = str_replace('Æ', 'AE',$fragment);
  $fragment = str_replace('ø', 'oe',$fragment);
  $fragment = str_replace('Ø', 'OE',$fragment);
  $fragment = str_replace('å', 'aa',$fragment);
  $fragment = str_replace('Å', 'AA',$fragment);
  $fragment = str_replace(',','',$fragment);
  $fragment = str_replace('(','',$fragment);
  $fragment = str_replace(')','',$fragment);
  return $fragment;
}
