<?php
/**
 * Implementation of hook_menu().
 **/

function ndla_contact_menu() {
  $items = array();
  $items['contact-us'] = array(
    'title' => t('Contact us'),
    'page callback' => 'ndla_contact_form',
    'access arguments' => array('access content'),
  );
  $items['contact-us-feedback'] = array(
    'title' => t('Contact us'),
    'page callback' => 'ndla_contact_form_feedback',
    'access arguments' => array('access content'),
  );
  $items['admin/settings/ndla_contact'] = array(
    'title' => t('NDLA Contact Form'),
    'description' => t('Settings for the issue collector.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_contact_settings_form'),
    'access arguments' => array('administer ndla_contact'),
  );
  return $items;
}

function ndla_contact_form_feedback() {
  $html = "<html><head>";
  drupal_add_css(drupal_get_path('theme', 'ndla2010') . "/css/ndla_styles.css");
  $html .= drupal_get_html_head();
  $html .= drupal_get_css();
  $html .= drupal_get_js('header');
  $html .= "</head><body>
  <div id='content'>
  <div id='body'>";
  $title = t('Thank you for your feedback!');
  $closing = t('This window will close in 4 seconds');
  if(!empty($_REQUEST['mobile'])) {
    $closing = t('This page will redirect within in 4 seconds, if not click !here.', array('!here' => l(t('here'), $_REQUEST['url'])));
  }
  $html .= "<h1>$title</h1><p>$closing</p>";
  if(!empty($_REQUEST['mobile'])) {
    $url = $_REQUEST['uri'];
    $html .= "<script type='text/javascript'>setTimeout(function() {window.location = '" . $url . "';}, 4000);</script>";
  }
  else {
    $html .= "<script type='text/javascript'>setTimeout(function() {parent.Lightbox.end();}, 4000);</script>";
  }
  $html .= "</div>
  </div>
  </body>
  </html>";
  print $html;
}
/**
 * Implementation of hook_init()
 **/
function ndla_contact_init() {
  drupal_add_css(drupal_get_path('module', 'ndla_contact') . '/css/ndla_contact.css');
  drupal_add_js(drupal_get_path('module', 'ndla_contact') . '/js/ndla_contact.js');
}
/**
 * Implementation of settings_form()
 **/
function ndla_contact_settings_form() {
  $form = array();
  $form['radio_btn_options'] = array(
    '#title' => t('Radio button options.'),
    '#description' => t('Eachline in the textbox represents an option presented as a radio button.'), 
    '#type' => 'textarea',
    '#default_value' => variable_get('radio_btn_options',''),
  );
  $form['jira_settings'] = array(
    '#title' => 'JIRA Settings',
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#description' => 'Type in the username and password that will be used to login to JIRA with.
    this is needed in order to be able post to JIRA.',
  );
  $form['jira_settings']['jira_user'] = array(
    '#title' => t('User'),
    '#description' => t('The username for the jira account.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('jira_user',''),
  );
  $form['jira_settings']['jira_password'] = array(
    '#title' => t('Password'),
    '#description' => t('Password for the jira account'),
    '#type' => 'password',
    '#default_value' => variable_get('jira_password',''),
  );
  $form['jira_settings']['jira_project'] = array(
    '#title' => 'Project',
    '#type' => 'textfield',
    '#default_value' => variable_get('jira_project',''),
    '#description' => 'The project key. f.ex. RDM or TGP',
  );
  $form['jira_settings']['jira_issuetype'] = array(
    '#title' => 'Issue type',
    '#description' => '',
    '#default_value' => variable_get('jira_issuetype',''),
    '#type' => 'textfield',
  );
  $form['jira_settings']['jira_reporter'] = array(
    '#title' => 'Reporter',
    '#description' => 'The user that will be set as reporter',
    '#default_value' => variable_get('reporter',''),
    '#type' => 'textfield',
  );
  $form['google_recaptcha'] = array(
    '#title' => 'Google reCaptcha',
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['google_recaptcha']['google_recaptcha_enabled'] = array(
    '#title' => 'Enable Google reCaptcha',
    '#type' => 'checkbox',
    '#default_value' => variable_get('google_recaptcha_enabled',''),
  );
  $form['google_recaptcha']['google_pub_key'] = array(
    '#title' => 'Public key',
    '#description' => 'Google reCaptcha public key',
    '#type' => 'textfield',
    '#default_value' => variable_get('google_pub_key',''),
  );
  $form['google_recaptcha']['google_secret_key'] = array(
    '#title' => 'Secret key',
    '#description' => 'Google reCaptcha secret key',
    '#type' => 'textfield',
    '#default_value' => variable_get('google_secret_key',''),
  );
  return system_settings_form($form); 
}
/**
 * Implementation of hook_form().
 **/
function ndla_contact_jiraform_form($form_state) {
  $sitekey = variable_get('google_pub_key','');
  $is_mobile = !empty($_REQUEST['mobile']);

  //A little rewrite of the array with the values for the concern radio buttons.
  $vals = explode("\n", str_replace("\r", "", variable_get('radio_btn_options','')));
  $concernoptions = array();
  foreach ($vals as $val) {
    $concernoptions[$val] = ($val);
  }
  $form = array();
  $form['email'] = array(
    '#prefix' => '<div id="ndla_contact_form"><h2 class="block-tab">' . t('Leave us some feedback') . '</h1>',
    '#title' => t('Your e-mail.'),
    '#type' => 'textfield',
    '#size' => '30',
    '#required' => FALSE,
    '#description' => t('If you want us to be able to reply to you, it is important that you enter a valid e-mail address'),
  );
  $form['concern'] = array(
    '#type' => 'radios',
    '#title' => t('Your issue concerns?'),
    '#description' => t('Select the most appropiate option'),
    '#required' => FALSE,
    '#default_value' => "Generelt",
    '#label' => FALSE,
    '#options' => array_filter($concernoptions),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#resizable' => FALSE,
    '#title' => t('Description'),
    '#description' => t('Type your issue here'),
    '#required' => TRUE,
    '#rows' => 7,
  );
  $form['url'] = array(
    '#default_value' => $_SERVER['HTTP_REFERER'],
    '#type' => 'hidden',
  );
  $form['is_mobile'] = array(
    '#type' => 'value',
    '#value' => $is_mobile,
  );
  $form['has_flash'] = array(
    '#type' => 'hidden',
    '#default_value' => 'Bruker har ikke flash',
  );
  // Show reCaptcha if enabled
  if(variable_get('google_recaptcha_enabled','')) {
    $form['recaptcha'] = array(
      '#value' => "<div class='g-recaptcha' data-sitekey=$sitekey></div>",
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Send',
    '#suffix' => '</div>',
  );
  return $form;
}
/**
 * Implementation of hook_form()
 **/
function ndla_contact_form() {
  // There is probably a less messy way to do this.
  $form = drupal_get_form('ndla_contact_jiraform_form');
  $html = "<html><head><script src='https://www.google.com/recaptcha/api.js'></script>";
  drupal_add_css(drupal_get_path('theme', 'ndla2010') . "/css/ndla_styles.css");
  $html .= drupal_get_html_head();
  $html .= drupal_get_css();
  $html .= drupal_get_js('header');
  $html .= "</head><body>
  <div id='body'>";

  $all_messages = drupal_get_messages();
  if ($all_messages) {
    $html .= "<div id='messages'>";
    foreach ($all_messages as $type => $messages) {
      foreach ($messages as $message) {
        $html .= '<div class="message message-' . $type . '">' . $message . '</div>';
      }
    }
    $html .= '</div>';
  }
  
  $html .= $form;

  $html .= "</div>
  </body>
  </html>";
  print $html;
}
/**
 * Implementation of form_validate()
 **/
function ndla_contact_jiraform_form_validate($form, &$form_state) {
  $google_recaptcha = variable_get('google_recaptcha_enabled','');
  $privkey = variable_get('google_secret_key','');

  if(isset($_POST['g-recaptcha-response'])){
    $captcha=$_POST['g-recaptcha-response'];
  }

  $url="https://www.google.com/recaptcha/api/siteverify?secret=$privkey&response=$captcha";
  $json = file_get_contents($url);
  $res = json_decode($json, true);
  if(!$google_recaptcha || ($google_recaptcha && $res['success'])) {
    $email = $form_state['values']['email'];
    // Only check if e-mail address is valid if something is entered in field.
    if($email && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
      form_set_error('email',t('Please enter a valid e-mail address.'));
    }
  // The reCaptcha test failed. This smells like robot fingers.
  } else {
    form_set_error('', t('Please complete the reCaptcha so we know you are not a robot.'));
  }
}
/**
 * Implementation of hook_form
 **/
function ndla_contact_jiraform_form_submit($form, &$form_state) {
  $has_flash = $form_state['values']['has_flash'];
  $email = $form_state['values']['email'];
  $url = $form_state['values']['url'];
  $description = $form_state['values']['description'];
  $concern = $form_state['values']['concern'];
  $username = variable_get('jira_user','');
  $password = variable_get('jira_password','');
  $projectkey = variable_get('jira_project','');
  $issuetype = variable_get('jira_issuetype','');
  $reporter = variable_get('jira_reporter','');
  // If description is larger than 50 chars, make substring to use as summary.
  if (strlen($description) > 50) {
    $summary = substr($description,0, 49) . "...";
  } else {
    $summary = $description;
  }
  // Remove newline characters from summary string
  $summary = str_replace(array("\r", "\n"), '', $summary);

  $environment = "URL: " . $url . " USER AGENT: " . $_SERVER['HTTP_USER_AGENT'];
  // The data sent in the curl post.
  $postdata = array(
    "fields" => array(
      "project" => array(
        "key" => $projectkey
      ),
      "summary" => $summary,
      "description" => $description,
      "issuetype" => array(
        "name" => $issuetype
      ),
      "environment" => $environment,
      "customfield_11491" => array(
        "value" => $concern
      ),
      "customfield_11592" => $email,
      "customfield_11993" => $has_flash,
    ),
  );
  $create = jira_rest_createissue($username, $password, $postdata);
    if($create->id) {
      $is_mobile = !empty($form_state['values']['is_mobile']) ? check_plain($form_state['values']['is_mobile']) : 0;
      drupal_goto('contact-us-feedback', array('mobile' => $is_mobile, 'uri' => $form_state['values']['url']));
  }
}

function ndla_contact_button() {
  global $base_root;
  return l(t('Contact us'), 'contact-us', array('attributes' => array('class' => 'ndla-contact-button', 'rel' => 'lightframe[|width:700;height:510;overflow:visible]'), 'query' => array('url' => $base_root . request_uri())));
}
