userData.ready(function (loggedIn) {
  if(!loggedIn) {
    return;
  }
  var $panel = $('<div id="my-learning-panel"></div>');
  var $dropdown = $('<div id="my-learning-panel-dropdown" class="dropdown clearfix activea"><h2></h2><div class="scrollable"><fieldset class="subject_dropdown">' + /*<ul><li><a href="/en/node/9484">English</a></li><li><a href="/nb/node/9484" class="active">Bokmål</a></li><li><a href="/nn/node/9484">Nynorsk</a></li></ul> */ '</fieldset></div></div>');
  $('#main').prepend($panel);
  $panel.after($dropdown);

  // Delay animated opening of the panel
  setTimeout(function () {
    $panel.addClass('is-open');
  }, 50);
});

NdlaPanel = {};

NdlaPanel.id = null;

NdlaPanel.toggle_dropdown = function(title, html, id) {
  if(NdlaPanel.id != id) {
    $('#my-learning-panel-dropdown > h2').html(title);
    $('#my-learning-panel-dropdown > div.scrollable > fieldset.subject_dropdown').html(html);
    $('#my-learning-panel-dropdown').addClass('active');
    NdlaPanel.id = id;
  } else {
    $('#my-learning-panel-dropdown').removeClass('active');
    NdlaPanel.id = null;
  }
};
