NdlaReminders = {};
NdlaReminders.reminders = [];

NdlaReminders.init = function(loggedIn) {
  if(!loggedIn) {
    return;
  }
  var $button = $('<i class="icon-calendar"></i>');
  $('#my-learning-panel').append($button);
  var reminders = userData.getData('page', 'reminder');

  if(reminders != null) {
    NdlaReminders.reminders = reminders;
  }

  $button.click(function() {
    var html = '<div id="my-learning-reminder-form"><div id="my-learning-reminder-calendar"></div><br><textarea></textarea><br><input type="button" value="' + Drupal.t('Add reminder') + '"></div>';
    NdlaPanel.toggle_dropdown(Drupal.t('Reminders'), html, 'reminder');
    g_jsDatePickImagePath = Drupal.settings.basePath + "sites/all/modules/ndla_reminders/libraries/jsdatepick-calendar/img/";
    var calendar = new JsDatePick({
      useMode:1,
    	isStripped:true,
    	target:"my-learning-reminder-calendar",
      imgPath: Drupal.settings.basePath + "sites/all/modules/ndla_reminders/libraries/jsdatepick-calendar/img/"
    });
    $('#my-learning-reminder-form input').click(function(){
      var obj = calendar.getSelectedDay();
      if(obj == false) {
        alert(Drupal.t('No date has been selected.'));
        return;
      }
	    var text = $('#my-learning-reminder-form textarea').val();
      var date = new Date(obj.year, (obj.month - 1), obj.day);
      var timestamp = date.getTime() / 1000;
      var reminder = {
        title: $(document).attr('title'),
        subject: Drupal.settings.ndla_mssclient.fag,
        contenttype: Drupal.settings.ndla_mssclient.contenttype,
        text: text,
        timestamp: timestamp
      };
      NdlaReminders.reminders.push(reminder);
      userData.saveData('page', 'reminder', NdlaReminders.reminders);
      NdlaPanel.toggle_dropdown('', '', 'reminder');
    });
  });
};

userData.ready(function (loggedIn) {
  NdlaReminders.init(loggedIn);
});
