<?php
require_once 'libraries/phpqrcode/qrlib.php';

$str = $_GET['str'];
$qrcode_svg_xml = QRcode::png($str, false, QR_ECLEVEL_L, 4, 3, false, 0xFFFFFF, 0x000000);

header('Content-type: image/png');
echo $qrcode_svg_xml;
