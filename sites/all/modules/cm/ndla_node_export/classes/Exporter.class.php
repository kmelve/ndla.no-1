<?php

// Disable Opengraph generation for epub/pdf files.
// This is using internal functions for testing the opengraph_meta module
// and might break in the future. BE CAREFUL!
class OGMFakeSettings {
  public function get($name, $default) {
    return array('dummy_content_type');
  }
  public function set($name, $value) {}
}

class Exporter {
  public $html_shards;
  public $ignored_nids = array(
    '22199', // Suggested Year Plan (UK - Other Countries - USA) 2009/2010
    '55741', // Suggested Year Plan, SF
    '55747', // Suggested Year Plan, YF
    '20968', // Suggested Year Plan (USA - Other Countries - UK) 2009/2010
    '20528', // English-Speaking Countries - Task
  );
  protected $already_added_nids = array();
  protected $title_html_buffer = "";



  public function __construct($menu_nid) {
    $this->debug = false;
    $this->menu_node_id = $menu_nid;
    $menu_node = node_load($menu_nid);
    $this->menu_structure = ndla_menu_build_tree($menu_node->ndla_menu);
  }

  public function log($message) {
    if($this->debug) {
      drush_log($message, 'ok');
    }
  }

  public function create_html() {
    global $language;
    $menu = $this->build_struct($this->menu_structure, $language->language);
    $html_bits = $this->create_html_from_menu($menu);
    foreach($html_bits as $html) {
      $this->html_shards[] = $html;
    }
  }

  function build_struct($struct, $lang, $level = 0, $menu = array()) {
    $default_types_to_ignore = array(
      "",
      "flashnode",
      "quiz",
      "amendor_ios",
      "amendor_ios_task",
      "lenke",
      "image",
      "biblio",
      "person",
      "multichoice",
      "fil",
      "amendor_electure",
      "mmenu",
      "veiledning",
      "audio",
      "rss",
      "video",
      "h5p_content",
    );

    $NDLA_EXPORT_SKIP_NODE_TYPES = variable_get('ndla_export_ignore_node_types', $default_types_to_ignore);

    foreach($struct as $key=>$item) {
      if(!$item->$lang->published) {
        continue;
      }
      // Reset variables
      $nid = null;
      $type = null;
      $dupe = false;
      $m = array();

      $has_nid = preg_match_all("/^node\/([\d]+)$/", $item->$lang->path, $matches);
      if($has_nid === 1) {
        $nid  = $matches[1][0];
        $node = ndla_utils_load_node($nid);
        $type = $node->type;
      } else {
        $nid     = null;
        $type    = null;
        $matches = null;
      }

      if(in_array($nid, $this->already_added_nids) && $nid !== null) {
        $type = null;
        $nid  = null;
        $dupe = true;
      }

      if(in_array($nid, $this->ignored_nids) && $nid !== null) {
        $type = null;
        $nid  = null;
        $dupe = true;
      }

      if($nid !== null) {
        $this->already_added_nids[] = $nid;
      }

      $m['title'] = $item->$lang->title;
      if(!in_array($type, $NDLA_EXPORT_SKIP_NODE_TYPES)) {
        $m['nid'] = $nid;
        $m['type'] = $type;
        $m['dupe'] = $dupe;
        $m['children'] = array();
      }

      if(is_array($item->children)) {
        $m['children'] = $this->build_struct($item->children, $lang, ++$level);
      }

      if(empty($m['nid']) && empty($m['children'])) {
        continue;
      }

      if(empty($m['children'])) {
        unset($m['children']);
      }

      $menu[] = $m;
    }
    return $menu;
  }

  function create_html_from_menu($menu, $key = "", $html = array(), $depth = 1) {

    if($key === "children") {
      $depth++;
    }

    if($key === "title") {
      $this->title_html_buffer .=  "<h$depth>$menu</h$depth>";
      // Enable to print the titles
      // echo str_pad("", $depth*2) . $this->title_html_buffer. "\n";
    }

    if($key === "nid") {
      $content = $this->create_content($menu);
      $content['html'] = $this->title_html_buffer . ' ' . $content['html'];
      $html[] = $content;
      $this->title_html_buffer = "";
    }

    if(is_array($menu)) {
      foreach($menu as $key => $m) {
        $html = $this->create_html_from_menu($m, $key, $html, $depth);
      }
    }

    return $html;
  }

  public function create_content($nid) {
    global $current_ndla_export_node, $PURIFY_MODE, $language;
    $html = array();
    

    // Disable Opengraph generation for epub/pdf files.
    // This is using internal functions for testing the opengraph_meta module
    // and might break in the future. BE CAREFUL!
    try{
      if(module_exists('opengraph_meta')) {
        $objs = OpenGraphMeta::instance()->__get_objects();
        $settings = new OGMFakeSettings();
        OpenGraphMeta::instance()->__set_objects($objs[0], $settings, $objs[2]);
      }
    } catch(Exception $e) {}

    if($this->debug) {
      $this->log('Parsing node >> ' . $nid);
    }
    $node = node_load($nid);
    $html[] = "<a href='http://ndla.no/node/$nid'>{$node->title} ($nid)</a>";
    // Expose the current node globaly, ndla_content_purifier uses this in NdlaPurifierInjector.php:89
    $current_ndla_export_node = $node;

    $data = null;

    $data = ndla_content_get_clean($node, 'ndla_content_purifier_export', $PURIFY_MODE);
    $html[] = $data;

    // Copyright information display logic
    $og_sql = 'SELECT group_nid FROM {og_ancestry} WHERE nid = %s LIMIT 1';
    $resultset = db_query($og_sql, $nid);


    $course = NULL;
    while($row = db_fetch_array($resultset)) {
      $course = $row['group_nid'];
    }
    
    $copyright = '';
    if (module_exists('ndla_authors') && ndla_authors_show_authors($node->type, $node, $course)) {
      $authors = ndla_authors_get_authors($node, TRUE);
      $shown = array();
      $printed = array();

      foreach($authors as $author) {
       foreach($author['authors'] as $author_node) {
         if(!in_array($author_node['nid'], $printed)) {
           $shown[] = $author_node['title'];
           $printed[] = $author_node['nid'];
         }
       }
      }

      $authors_string = implode(", ", $shown);
      $copyright = '<div class="footer">Forfatter: ' . $authors_string . '</div>';
    } // End copyright logic block
    
    return array('title' => $node->title, 'html' => $copyright . implode(" ", $html), 'nid' => $nid);
  }

  public function saveHTMLtoFile($nid, $html) {
    $handler = fopen($nid.'.html', 'w');
    fwrite($handler, $html);
    fclose($handler);
  }


    /**
   * @return <string> filename + path
   */
  
  public function create_file($format_class, $path) {
    global $language;

    if(empty($this->html_shards)) {
      return '';
    }

    $filepath = $path . '/' . $this->menu_node_id . '_' . $language->language . '.' . $format_class->extension;

    if(file_exists($filepath)) {
      unlink($filepath);
      //return $filepath;
    }

    $menu_nid = $this->menu_node_id;
    
    $metadata = array(
      'language' => $language->language,
      'identifier' => 'http://ndla.no/' . $language->language . '/node/' . $menu_nid,
      'title' => $this->get_title(),
    );

    drush_log($this->menu_node_id . ', ' .$metadata['title'] . ', ' .$this->fag_nid . ', ' . $language->language . ', ' . get_class($format_class), 'ok');

    $cover_html = $this->generate_cover_html();

    $format_class->setup($metadata, $cover_html);
    $format_class->input($this->html_shards);
    $result = $format_class->transform();

    $sql = "INSERT INTO 
              {ndla_export_html_files}
            SET
              created_at = '%s',
              filepath = '%s',
              menu_node = '%s',
              meta_data = '%s',
              language = '%s';";

    db_query($sql, 
      date('U'), 
      $filepath,  
      $this->menu_node_id, 
      json_encode($metadata), 
      $language->language
    );
    
    if(!$result) {
      return FALSE;
    }


    $format_class->output($filepath);
    return $filepath;
  }

  public function get_title() {
    $fag_nid = NULL;

    $sql = "SELECT gid FROM {ndla_menu_courses} WHERE nid = %d LIMIT 1";
    $resultset = db_query($sql, $this->menu_node_id);

    while($row = db_fetch_object($resultset)) {
      $fag_nid = $row->gid;
      $this->fag_nid = $row->gid;
    }
    if($fag_nid) {
      $node  = node_load($fag_nid);
      $name = trim($node->title);
    }
    return $name;
  }

  public function generate_cover_html() {
    global $base_url, $language;

    $pdf_title = '<h1>' . $this->get_title() . '</h1>';

    $description = array();

    $description['nn'] = "<h2>Fagstoff til eksamen</h2>
    <p>Innhald på ndla.no er no tilgjengeleg i PDF- eller ePub-format
    som hjelpemiddel til eksamen. Desse filane kan du lagre på din 
    eigen datamaskin og lese i digitalt format, eller du kan skrive dei 
    ut og ta dei med til eksamen. Dette er automatisk genererte filar 
    som ikkje er tilrettelagde manuelt.</p>
    
    <p>Dette dokumentet er ei tekstutgåve av det digitale læreverket for
    faget slik det låg føre på ndla.no april 2015. For å sjå det komplette 
    læreverket, slik det er sett saman av ulike medietypar og 
    interaktive element, gå til http://ndla.no.</p>

    <p>Ved eksamen vil du ikkje ha tilgang til Internett, og dermed vil i
    hovudsak berre tekst og bilete vere tilgjengelege. Animasjonar, 
    simuleringar, lydfilar og video er interaktive ressursar som krev 
    tilkopling til nett.</p>

    <span class='italics'>
      <p>Sentralt gitt skriftleg eksamen i Kunnskapsløftet følgjer to hovudmodellar 
      for hjelpemiddel. I modell 1 er alle hjelpemiddel tillatne. 
      Unnatak er Internett og andre verktøy som tillèt kommunikasjon. 
      For norsk og framandspråka er heller ikkje omsetjingsprogram tillatne.</p>

      <p>Modell 2 er ein todelt eksamen. Der er det i del 1 tillate med
      skrivesaker, passar, linjal og vinkelmålar. I del 2 er alle hjelpemiddel 
      tillatne med unnatak av Internett eller andre verktøy som tillèt 
      kommunikasjon.</p>

      <p>Desse faga følgjer modell 2 for hjelpemiddelbruk utan førebuingsdel; 
      matematikk i grunnskulen, matematikk i grunnskuleopplæringa for vaksne, 
      matematikk, fysikk, kjemi og biologi i vidaregåande opplæring.</p>
   </span>";

    $description['nb'] = "<h2>Fagstoff til eksamen</h2>
    <p>Innhold på ndla.no er nå tilgjengelig i PDF- eller ePub-format
    som hjelpemidler til eksamen. Disse filene kan lagres på egen 
    datamaskin og leses i digitalt format, eller de kan skrives ut og 
    tas med til eksamen. Dette er automatisk genererte filer som ikke 
    er manuelt bearbeidet.</p>
    
    <p>Dette dokumentet er en tekstutgave av det digitale læreverket for 
    faget slik det forelå på ndla.no april 2015. For å se det komplette 
    læreverket, slik det er sammensatt av ulike medietyper og interaktive 
    elementer, gå til http://ndla.no.</p>

    <p>Ved eksamen vil man ikke ha adgang til Internett, og dermed 
    vil i hovedsak kun tekst og bilder være tilgjengelig. Animasjoner, 
    simuleringer, lydfiler og video er interaktive ressurser som krever 
    tilkobling til nett.</p>

    <span class='italics'>
      <p>Sentralt gitt skriftlig eksamen i Kunnskapsløftet følger to hovedmodeller 
      for hjelpemidler. I modell 1 er alle hjelpemidler tillatt. 
      Unntak er Internett og andre verktøy som tillater kommunikasjon. 
      For norsk og fremmedspråkene er heller ikke oversettelsesprogrammer tillatt.</p>

      <p>Modell 2 er en todelt eksamen. Der er det i del 1 tillatt med skrivesaker, 
      passer, linjal og vinkelmåler. I del 2 er alle hjelpemidler 
      tillatt med unntak av Internett eller andre verktøy som tillater 
      kommunikasjon.</p>

      <p>Disse fagene følger modell 2 for hjelpemiddelbruk uten forberedelsesdel; 
      matematikk i grunnskolen, matematikk i grunnskoleopplæringen for voksne, 
      matematikk, fysikk, kjemi og biologi i videregående opplæring.</p>
   </span>";

    $style = <<<'EOD'
    <style>
      * {
        margin: 0;
        padding: 0;
        font-size: 14px;
      }

      body: {
        margin: 0;
        padding: 0;
        font-size: 14px;
      }

      .logo {
        margin: 1em;
        margin-left: 100px;
        margin-top: 40px;
        float: left;
      }

      .header {
        float: left;
        margin-top: 40px;
      }
      
      .clearfix {
        clear: both;
      }
      
      .header_blue_bg {
        background-color: #20588F;
        width: 100%;
      }

      h1 {
        font-family: Arial;
        font-size: 16px;
        padding: 1em 1em 1em 0;
        color: white;
        margin-left: 100px;
      }

      h2 {
        font-family: Arial;
        font-size: 20px;
        padding: 1em 1em 1em 0;
        color: #5A85BB;
        margin-left: 100px;
      }

      p {
        width: 500px;
        text-align: justify;
        text-justify: inter-word;
        margin-left: 100px;
        font-size: 12px;
        line-height: 1.5;
        margin-top: 20px;
      }

      .italics {
        font-style:italic;
      }

      .footer {
        position: fixed;
        bottom: 0;
        left: 0;
        width: 100%;
      }
    </style>
EOD;
    
    $url = $base_url . '/sites/all/modules/cm/ndla_node_export/templates/';

    $html = '<html><body>';
    $html .= $style;

    $html .= "<img class='logo' src='" . $url . "logo_blue.png' width='40%' />
              <img class='header' src='" . $url . "dandelion.png' width='40%' />
              <div class='clearfix'></div>";
    
    $html .= "<div class='header_blue_bg'>" . $pdf_title . "</div>";
    
    // If language is not nn or nb default to nb
    $lang = $language->language;

    if(!in_array($lang,  array('nb','nn'))) { 
      $lang = 'nb';
    }
    $html .= $description[$lang];
    $html .= "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
    $html .= "<img class='footer' src='" . $url . "footer_bg.png' width='100%' />";
    $html .= '<body></html>';

    return $html;
  }
}
