<?php

class EpubFormat {
  public $extension = 'epub';
  protected $html;

  public function setup($metadata = null) {
    $this->metadata = $metadata;
  }

  public function input($html_shards) {
    $this->html_shards = $html_shards;
  }

  public function transform() {
    require_once drupal_get_path('module', 'ndla_export') . '/libraries/phpepub/EPub.php';

    $title = $this->metadata['title'];

    if(empty($title)) {
      $title = 'NDLA ePub book';
    }

    $bookStart =
      "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
      . "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n"
      . "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n"
      . "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
      . "<head>"
      . "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
      . "<link rel=\"stylesheet\" type=\"text/css\" href=\"styles/style.css\" />\n"
      . "<title>$title</title>\n"
      . "</head>\n"
      . "<body>\n";

    $bookEnd = "</body>\n</html>\n";


    $this->epub = new Epub();
    $this->epub->setTitle($title);
    $this->epub->setIdentifier($this->metadata['identifier'], EPub::IDENTIFIER_URI);
    $this->epub->setLanguage($this->metadata['langugage']);
    $this->epub->setDescription('');
    $this->epub->setAuthor('NDLA', 'NDLA');
    $this->epub->setDate(time());
    $this->epub->setRights('');
    $this->epub->setSourceUrl('http://ndla.no/');

    $drupal_path = realpath('.');
    $css_file = $drupal_path . '/' . drupal_get_path('module', 'ndla_export') . '/templates/epub-page.css';
    $css_data = file_get_contents($css_file);

    $this->epub->addCSSFile('styles/style.css', 'css1', $css_data);

    $chapter_idx = 1;
    $title_idx = 0;

    foreach($this->html_shards as $html_shard) {
      if(is_array($html_shard)) {
        $html_string = $html_shard['html'];
        $html_title = $html_shard['title'];
        $html_nid = $html_shard['nid'];
      } else {
        $html_string = $html_shard;
      }

      $html_string = str_replace('file://', '', $html_string);
      $html_string = str_replace('%20', ' ', $html_string);
      $html_string = str_replace('%25', '%', $html_string);

      $html = $bookStart . $html_string . $bookEnd;
      $this->epub->addChapter($html_title, $chapter_idx++, $html, false, EPub::EXTERNAL_REF_ADD, '/');
    }
    return TRUE;
  }

  public function output($filename) {
    $this->log('>>> ' . __method__ . ' executing');
    $path_info = pathinfo(realpath('.') .'/'. $filename);
    $this->epub->finalize();
    $return = $this->epub->saveBook($path_info['filename'], $path_info['dirname']);

    $this->log('<<< ' . __method__ . ' ending');
    return $return;
  }

  public function log($filename) {
    if(function_exists('drush_log')) {
      drush_log(__class__ . " :: " . $msg, $level);
    } else {
      print __class__ . ' :: ' . $msg;
    }
  }

  public function getTitle() {
    return $this->title;
  }
}
