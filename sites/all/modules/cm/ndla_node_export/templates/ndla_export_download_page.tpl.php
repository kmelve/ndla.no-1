<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html version="XHTML+RDFa 1.0" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" xmlns:og="http://opengraphprotocol.org/schema/" dir="<?php print $language->dir ?>" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:skos="http://www.w3.org/2004/02/skos/core#">
  <head>
    <title><?php print $head_title ?></title>
    <?php print drupal_get_html_head(); ?>
    <?php print drupal_get_css(); ?>
    <?php print drupal_get_js(); ?>
  </head>
  <body class="<?php print $classes ?>">
    <div class="download-frame">
      <h2><?php print $title ?></h2>
      <p class="description">
        <?php print $description ?>
      </p>
      <?php foreach($menu_html as $type => $data):
        $lang = $data['lang'];
        $nid = $data['nid'];
        $menuname = $data['menuname'];
      ?>
        <div class='download-links'>
          <span class="download-menu-name"><?php echo t('Download menu !menuname', array('!menuname' => $menuname)) ?></span>
          <!-- PDF -->
          <div class="download-pdf download-link">
            <a href="<?php print "/$lang/node/$nid/download/pdf"; ?>">
              <i class="fa fa-cloud-download"></i>
              <span>
                <?php echo t('Download as !format', array('!format' => 'PDF')) ?>
              </span>
              <div class='download-button-footer'></div>
            </a>
          </div>
          
        
          <!-- EPUB -->
          <div class="download-epub download-link">
              <a href="<?php print "/$lang/node/$nid/download/epub"; ?>">
                <i class="fa fa-cloud-download"></i>
                <span>
                  <?php echo t('Download as !format', array('!format' => 'ePub')) ?>
                </span>
                <div class='download-button-footer'></div>
              </a>
          </div>
          
        </div>
      <?php endforeach ?>
    </div>
  </body>
</html>
