<?php
require_once 'libraries/phpqrcode/qrlib.php';

/**
 * Implements hook_menu()
 */
function ndla_export_menu() {
  $items = array(
    'admin/settings/ndla_export' => array(
      'title' => t('NDLA PDF/epub Export settings'),
      'description' => t('Control how NDLA export module works.'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('ndla_export_settings_form'),
      'access arguments' => array('administer site configuration'),
    ),
     'node/%node/qrcode' => array(
      'title' => t('QR Code Debug for node'),
      'page callback' => 'ndla_export_qrcode',
      'page arguments' => array(1),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    ),
    'node/%node/download' => array(
      'title' => t('Download menu'),
      'page callback' => 'ndla_export_show_menu',
      'page arguments' => array(1),
      'access callback' => 'ndla_export_download_access',
      'access arguments' => array(1),
      'type' => MENU_CALLBACK,
    ),
    'node/%node/download/pdf' => array(
      'title' => t('Download menu'),
      'page callback' => 'ndla_export_download',
      'page arguments' => array(1, 'pdf'),
      'access callback' => 'ndla_export_download_access',
      'access arguments' => array(1),
      'type' => MENU_CALLBACK,
    ),
    'node/%node/download/epub' => array(
      'title' => t('Download menu'),
      'page callback' => 'ndla_export_download',
      'page arguments' => array(1, 'epub'),
      'access callback' => 'ndla_export_download_access',
      'access arguments' => array(1),
      'type' => MENU_CALLBACK,
    ),
  );

  return $items;
}
function ndla_export_settings_form() {
  $ndla_export_settings = array(
    'title' => array(
      'nb' => 'Export as PDF/ePub',
      'nn' => 'Export as PDF/ePub',
      'en' => 'Export as PDF/ePub',
    ),
    'description' => array(
      'nb' => 'Description',
      'nn' => 'Description',
      'en' => 'Description',
    ),
  );

  $ndla_export_settings = variable_get('ndla_export_settings', $ndla_export_settings);

  $form = array(
    'help' => array(
      '#weight' => 9999,
      '#value' => "",
      '#type' => 'item',
    ),
    'Norwegian Bokmål' => array(
      '#value' => t('Norwegian Bokmål'),
      '#type' => 'item',
    ),
    'title_nb' => array(
      '#label' => t('Title for the download modal popup (nb)'),
      '#description' => t('Please enter a title for the modal popup window (nb)'),
      '#type' => 'textfield',
      '#default_value' => $ndla_export_settings['title']['nb'],
    ),
    'description_nb' => array(
      '#label' => t('Description'),
      '#description' => t('Please enter a description for the modal window'),
      '#type' => 'textarea',
      '#rows' => '15',
      '#default_value' => $ndla_export_settings['description']['nb'],
    ),

    'Nynorsk' => array(
      '#value' => t('Nynorsk'),
      '#type' => 'item',
    ),
    'title_nn' => array(
      '#label' => t('Title for the download modal popup (nn)'),
      '#description' => t('Please enter a title for the modal popup window (nn)'),
      '#type' => 'textfield',
      '#default_value' => $ndla_export_settings['title']['nn'],
    ),
    'description_nn' => array(
      '#label' => t('Description'),
      '#description' => t('Please enter a description for the modal window'),
      '#type' => 'textarea',
      '#rows' => '15',
      '#default_value' => $ndla_export_settings['description']['nn'],
    ),

    'English' => array(
      '#value' => t('English'),
      '#type' => 'item',
    ),
    'title_en' => array(
      '#label' => t('Title for the download modal popup (en)'),
      '#description' => t('Please enter a title for the modal popup window (en)'),
      '#type' => 'textfield',
      '#default_value' => $ndla_export_settings['title']['en'],
    ),
    'description_en' => array(
      '#label' => t('Description'),
      '#description' => t('Please enter a description for the modal window'),
      '#type' => 'textarea',
      '#rows' => '15',
      '#default_value' => $ndla_export_settings['description']['en'],
    ),

    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save settings'),
    )
  );

  return $form;
}

function ndla_export_settings_form_submit($form, &$form_state) {
  $ndla_export_settings = array(
    'title' => array(
      'nb' => $form_state['values']['title_nb'],
      'nn' => $form_state['values']['title_nn'],
      'en' => $form_state['values']['title_en'],
    ),
    'description' => array(
      'nb' => $form_state['values']['description_nb'],
      'nn' => $form_state['values']['description_nn'],
      'en' => $form_state['values']['description_en'],
    ),
  );

  variable_set('ndla_export_settings', $ndla_export_settings);
}

function ndla_export_show_menu($node) {
  global $language;

  $menus = array();
  if($node->type == 'fag') {
    $sql = "SELECT  nmm.nid, nmc.type
          FROM {ndla_menu_courses nmc}
          JOIN node n ON (nmc.nid = n.nid)
          JOIN {ndla_menu_menus} nmm ON (n.vid = nmm.vid)
          WHERE nmc.gid = %d";

  
    $resultset = db_query($sql, $node->nid);


    while($row = db_fetch_object($resultset)) {
      // type:1 = fagstoff
      // type:2 = topic menu
      $generated_menus_sql = "SELECT count(id) AS count FROM {ndla_export_html_files}
                              WHERE language = '%s'
                              AND filepath <> ''
                              AND menu_node = %d LIMIT 1";
      $existing_menus_resultset = db_query($generated_menus_sql, $language->language, $row->nid);
      $e_row = db_fetch_object($existing_menus_resultset);
      if($e_row->count > 0) {
        $menus[$row->type] = $row->nid;
      }
      // d($menus);
    }
  }

  drupal_add_css(drupal_get_path('module', 'ndla_export') . 'templates/ndla_export_buttons.css');
  drupal_add_js(drupal_get_path('module', 'ndla_export') . '/js/ndla_export.js');
  $node_export_settings = variable_get('ndla_export_settings', array());

  // Default settings
  $node_export_settings += array(
    'title' => array(
      'nb' => 'Export as PDF/ePub',
      'nn' => 'Export as PDF/ePub',
      'en' => 'Export as PDF/ePub',
    ),
    'description' => array(
      'nb' => 'Description',
      'nn' => 'Description',
      'en' => 'Description',
    ),
  );

  $title = $node_export_settings['title'][$language->language];
  $description = $node_export_settings['description'][$language->language];

  $options = array(
    array('id' => 'qrcode', 'checked' => 'checked', 'name' => t('With QR Codes')),
  );

  $epubs = array();
  $pdfs = array();
  $menu_html = array();

  // Constants
  $TOPIC_MENU_ID = 2;
  $FAGSTOFF_MENU_ID = 1;

  $menuname_sql = "SELECT * FROM {ndla_fag_tabs} WHERE nid = %d";
  $resultset = db_query($menuname_sql, $node->nid);
  while($row = db_fetch_array($resultset)) {
    $menuname[$TOPIC_MENU_ID] = $row["topics_tab_name_{$language->language}"];
    $menuname[$FAGSTOFF_MENU_ID] = $row["menu_tab_name_{$language->language}"];
  }

  foreach($menus as $type => $menu) {
    $menu_html[$type] = array('nid' => $menu, 'lang' => $language->language, 'menuname' => $menuname[$type]);
  }

  echo theme('ndla_export_download_page', $title, $description, $options, $menu_html, $language);
}

function ndla_export_download($node, $type, $qrcode = null) {
  global $language;

  require_once 'classes/Exporter.class.php';

  $path = array(
    'pdf' => variable_get('ndla_export_pdf_path', 'sites/default/files/export/pdf_files'),
    'epub' => variable_get('ndla_export_epub_path', 'sites/default/files/export/epub_files'),
  );
  
  $file_str = $path[$type] . '/' . $node->nid . '_' . $language->language . '.' . $type;
  if(!file_exists($file_str)) {
    $file_str = $path[$type] . '/' . $node->nid . '_nb.' . $type;
  }
  $exporter = new Exporter($node->nid);
  if(file_exists($file_str)) {
    $filename = _ndla_export_clean_filename($exporter->get_title());
    $file_contents = file_get_contents($file_str);

    if($type === 'pdf') {
      drupal_set_header('Content-type: application/pdf');
    } elseif($type === 'epub') {
      drupal_set_header('Content-type: application/epub+zip');
    }
    drupal_set_header('Content-Disposition: attachment; filename="' . $filename . '.' . $type . '"');
    echo $file_contents;
  } else {
    echo 'file not found';
  }
}

function _ndla_export_clean_filename($name) {
  return preg_replace("#[^a-zA-Z0-9]#", '_'  ,$name);
}

function ndla_export_download_access($node) {
  if($node->type == 'nodemenu' || $node->type == 'fag') {
    return true;
  }
}


/**
 * Implements hook_theme()
 */
function ndla_export_theme($existing, $type, $theme, $module_path) {
  $path =  $module_path . '/templates';
  $template_array =  array(
    'ndla_export_download_page' => array(
      'path' => $path,
      'template' => 'ndla_export_download_page',
      'arguments' => array('title' => NULL, 'description' => NULL, 'options' => array(), 'menu_html' => NULL, 'language' => NULL),
    ),
    'ndla_export_qrcode' => array(
      'arguments' => array('qrcode' => NULL, 'title' => NULL, 'type' => NULL, 'url' => NULL),
      'template' => 'ndla_export_qrcode',
      'path' => $path,
    ),
    'ndla_export_download_menu_buttons' => array(),
  );
  return $template_array;
}

function theme_ndla_export_download_menu_buttons() {
  global $language;
  $url = '/' . $language->language . '/' . implode('/', arg()) . '/download';

  if(isset($_GET['fag'])) {
    $url = '/' . $language->language . '/node/' . $_GET['fag'] . '/download';
  }

  $a_link = "<a href='$url' rel='lightframe[|width:780px;height:700px;]'>";
  $a_link .= t('Download subject material');
  $a_link .= "</a>";

  $html = array(
    '<div class="menu-button">',
      '<span class="ndla2010-shadow-left"></span>',
      '<span class="ndla2010-tab-center">',
        $a_link,
      '</span>',
      '<span class="ndla2010-shadow-right"></span>',
      '<span class="ndla2010-shadow-bottom-left"></span>',
      '<span class="ndla2010-shadow-bottom-right"></span>',
    '</div>',
  );
  return implode('',$html);
}

function ndla_export_init() {
  drupal_add_css(drupal_get_path('module', 'ndla_export') . '/templates/ndla_export_buttons.css');
}

function _ndla_export_get_menu($menu_type = NDLA_MENU_LEFT, $parent_id = 0, $aria_level = 1) {
  global $language;

  if(!module_exists('ndla_utils')) return;
  if(!$parent_id) ndla_menu_url_to_js();

  $group_nid = ndla_utils_disp_get_context_value('course');
  $sql = "SELECT nmm.nid
          FROM {ndla_menu_courses nmc}
          JOIN node n ON (nmc.nid = n.nid)
          JOIN {ndla_menu_menus} nmm ON (n.vid = nmm.vid)
          WHERE nmc.gid = %d
          AND nmc.type = %d";
  $menu_id = db_result(db_query($sql, $group_nid, $menu_type));
  return $menu_id;
}

function ndla_export_qrcode($node) {
  global $lang, $base_url, $PHPQRCODE_FOR_INLINE_USE;
  $PHPQRCODE_FOR_INLINE_USE = true;

  $url = $base_url . url('node/' . $node->nid);
  $qrcode_svg_xml = QRcode::svg($url, false, QR_ECLEVEL_L, 4, 3, false, 0xFFFFFF, 0x000000);
  $base64_svg = base64_encode($qrcode_svg_xml);

  $img_tag = "<img width='100' height='100' class='qr_image' src='data:image/svg+xml;base64,$base64_svg'>";
  return theme('ndla_export_qrcode', $img_tag, $node->title, $node->type, $url);
}
