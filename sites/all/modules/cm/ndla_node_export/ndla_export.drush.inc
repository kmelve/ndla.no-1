<?php

define('NDLA_EXPORT_EXIT_CODE', 100); // Arbitrary number

function ndla_export_drush_command() {
  $items = array();
  $items['clear-export'] = array(
    'description' => 'Clear the HTML/PDF/Epub generated files',
    'callback'    => 'ndla_export_clear'
  );
  $items['export-menus'] = array(
    'description' => 'Create the PDF for one menu nid',
    'callback'    => 'ndla_export_single_menu',
    'arguments' => array(
      'nid' => 'menu node id',
    ),
  );
  $items['export-all-menus'] = array(
    'description' => 'Create the PDF for next menu',
    'callback'    => 'ndla_export_menus',
  );
  return $items;
}
/**
 * Drush callback
 * Generates a single menu into pdf and epub file.
 **/
function ndla_export_single_menu($menu_id) {
  global $language;
  $use_languages = array('nb', 'nn', 'en');
  
  foreach($use_languages as $lang) {
    $languages = language_list();
    $language = $languages[$lang]; // Set the global language before run
    ndla_export_create_file($menu_id);
  }
}

/**
 * Drush callback
 * Generates all menus found in ndla_menu_courses table where type is 1.
 **/
function ndla_export_menus() {
  global $language;
  $sql = "SELECT nid FROM {ndla_menu_courses}";
  $resultset = db_query($sql);

  // Check against what we already have
  $check_sql = "SELECT DISTINCT menu_node FROM {ndla_export_html_files}";
  $check_resultset = db_query($check_sql);

  $done_nids = array();
  while($check_obj = db_fetch_object($check_resultset)) {
    $done_nids[] = $check_obj->menu_node;
  }

  $menu_nids = array();
  while($obj = db_fetch_object($resultset)) {
    $menu_nids[] = $obj->nid;
  }

  $nodes = array_diff($menu_nids, $done_nids);
  $nid = array_pop($nodes);

  // No menus need pdf/epub files to be generated
  // returning and exiting
  if(empty($nid)) {
    return;
  }

  ndla_export_single_menu($nid);

  // Do we still have more menus to generate
  if(!empty($nodes)) { 
    // Custom exit code to tell external scripts to continue running
    exit(NDLA_EXPORT_EXIT_CODE); 
  }
}

function ndla_export_create_file($menu_nid, $mode = PURIFY_FOR_PDF) {
  global $ndla_export_language;
  global $language;
  global $PURIFY_MODE;
  global $pdf_export_running;

  $pdf_export_running = true;

  require_once 'classes/Exporter.class.php';
  require_once 'classes/formats/WkHtmlToPdfFormat.class.php';
  require_once 'classes/formats/EpubFormat.class.php';

  $PURIFY_MODE = $mode;
  $ndla_export_language = $language;
  
  $path = variable_get('ndla_export_html_path', 'sites/default/files/export/html_files');
  $pdf_path = variable_get('ndla_export_pdf_path', 'sites/default/files/export/pdf_files');
  $epub_path = variable_get('ndla_export_epub_path', 'sites/default/files/export/epub_files');

  drush_mkdir($path, TRUE);
  drush_mkdir($pdf_path, TRUE);
  drush_mkdir($epub_path, TRUE);

  $exporter = new Exporter($menu_nid);
  
  $exporter->create_html();
  $epub_file = $exporter->create_file(new EpubFormat(), $epub_path);
  $pdf_file = $exporter->create_file(new WkHtmlToPdfFormat(), $pdf_path);

  /*
  drush_log('PDF: ' . $pdf_file . ';EPUB: ' . $epub_file, 'ok');
  */

  if(empty($pdf_file) && empty($epub_file)) {
    $sql = "INSERT INTO 
              {ndla_export_html_files}
            SET
              created_at = '%s',
              filepath = '%s',
              menu_node = '%s',
              language = '%s';";

    db_query($sql, 
      date('U'), 
      '',  
      $menu_nid, 
      $language->language
    );
  }

  /*
  $size = memory_get_peak_usage();
  $unit = array('b','kb','mb','gb','tb','pb');
  $mem = @round( $size / pow(1024, ( $i = floor( log($size,1024) ) )), 2 ) . ' ' . $unit[$i];
  drush_log('PHP -> max memory used -> ' . $mem, 'ok');
  */
}

function ndla_export_clear() {
  $sql = "TRUNCATE {ndla_export_html_files}";
  db_query($sql);
  $path = variable_get('ndla_export_html_path', 'sites/default/files/export/html_files');
  $pdf_path = variable_get('ndla_export_pdf_path', 'sites/default/files/export/pdf_files');
  $epub_path = variable_get('ndla_export_pdf_path', 'sites/default/files/export/epub_files');

  $files = drush_scan_directory($path, '/.+/');
  $pdf_files = drush_scan_directory($pdf_path, '/.+/');
  $epub_files = drush_scan_directory($epub_path, '/.+/');

  $files = array_merge($files,$pdf_files, $epub_files);

  foreach($files as $filename => $file_info)
    drush_register_file_for_deletion($filename);

  drush_register_file_for_deletion('node_export_missing_files.log');
}
