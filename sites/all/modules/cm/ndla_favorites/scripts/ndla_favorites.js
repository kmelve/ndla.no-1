NdlaFavorites = {};

NdlaFavorites.init = function() {
  var marked = userData.getData('page', 'favorite');
  var mclass = '';
  var text = Drupal.t('Mark as favorite');
  if(marked != null) {
    text = Drupal.t('Marked as favorite');
    mclass = 'marked';
  }
  $block = $('<div class="block my-favorites"><ul><li class="' + mclass + '" title="' + Drupal.t('Mark/unmark as favorite') + '">' + text + '</li></ul></div>').appendTo('.region-learning');
  $('li', $block).click(function () {
    NdlaFavorites.mark($(this));
  });
}

NdlaFavorites.mark = function($li) {
  var isMarked = $li.hasClass('marked');
  if(!isMarked) {
    $li.addClass('marked');
    $li.html(Drupal.t('Marked as favorite'));
    userData.saveData('page', 'favorite', {
      title: $(document).attr('title'),
      subject: Drupal.settings.ndla_mssclient.fag,
      contenttype: Drupal.settings.ndla_mssclient.contenttype
    });
  } else {
    $li.removeClass('marked');
    $li.html(Drupal.t('Mark as favorite'));
    userData.removeData('page', 'favorite');
  }
}

userData.ready(function () {
  NdlaFavorites.init();
});