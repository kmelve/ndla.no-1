if (Drupal.jsEnabled) {
	$('#ndla_matlexicon_search').ready(function() {
    $('#ndla_matlexicon_search').val('');
    $('#ndla_matlexicon_clear_button').click(function() {
      $('#ndla_matlexicon_search').val('');
      $('#ndla_matlexicon_wrapper').removeClass('expanded');
      ndla_matlexicon_onkeyup();
    });

    $('#ndla_matlexicon_search').keyup(ndla_matlexicon_onkeyup);
    $('#ndla_matlexicon_search').focus(function () {
      $('#ndla_matlexicon_wrapper').addClass('expanded');
    });
        
    $('#ndla_matlexicon_type').change(function () {
      if ($('#ndla_matlexicon_search').val() != '') {
        getData($('#ndla_matlexicon_search').val().slice(0, 1));
      } else {
        Drupal.settings.ndla_matlexicon.current_letter = '';
      }
    });
  });// end ready
}//end if enabled

function ndla_matlexicon_onkeyup() {
	first_letter = $('#ndla_matlexicon_search').val().slice(0, 1);
	if (first_letter == '') {
		$('#termlist').html('');
	} else {
		if (Drupal.settings.ndla_matlexicon.current_letter == '' || Drupal.settings.ndla_matlexicon.current_letter != first_letter) {
			Drupal.settings.ndla_matlexicon.current_letter = first_letter;
			getData(first_letter);			
		} else {
			filterData();
		}
	}
}

function stripVowelAccent(str) {
	var s = str;

	var rExps = [ /[\xC0-\xC2]/g, /[\xE0-\xE2]/g,
	              /[\xC8-\xCA]/g, /[\xE8-\xEB]/g,
	              /[\xCC-\xCE]/g, /[\xEC-\xEE]/g,
	              /[\xD2-\xD4]/g, /[\xF2-\xF4]/g,
	              /[\xD9-\xDB]/g, /[\xF9-\xFB]/g ];

	var repChar = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u'];

	for (var i = 0; i < rExps.length; i++) {
		s = s.replace(rExps[i], repChar[i]);
	}

	return trim(s);
}


function filterData(jdata) {
	var searchterm = stripVowelAccent($('#ndla_matlexicon_search').val());
	var termlist = $('#termlist')[0];
	if (jdata) {
		filterData.jdata = jdata;
	} else {
		if (typeof filterData.jdata == 'undefined') {
			return;
		}
		jdata = filterData.jdata;
	}

	termlist.innerHTML = '';
	
	var hit = false;
	var counter = 0;
	for(i in jdata.terms) {
		if (stripVowelAccent(jdata.terms[i].term.toLowerCase().substr(0,searchterm.length)) == searchterm.toLowerCase()){
			hit = true;
			var li = document.createElement('li');
			li.setAttribute('id', 'term_' + jdata.terms[i].id);
			if (jdata.terms[i].additional_information != '') {
				wd_elm = document.createElement('span');
				wd_elm.setAttribute('class', 'additional_information');
				wd_elm.appendChild(document.createTextNode(' (' + jdata.terms[i].additional_information + ')'));
			}
  		li.appendChild(document.createTextNode(jdata.terms[i].term));
			if (jdata.terms[i].additional_information != '') {
				li.appendChild(wd_elm);
			}
			var img = document.createElement('img');
			img.setAttribute('src', Drupal.settings.ndla_utils.base_url + Drupal.settings.basePath + '/sites/all/modules/ndla_matlexicon/ajax-loader-small.gif');
			img.setAttribute('style', 'float: right; display:none;');
			li.appendChild(img);
			$(li).each(function(){
				if($(this).html().indexOf("[") > 0){
					var str = $(this).html();new RegExp(".","gm")," "
					str = str.replaceAll("[", "[<span class='ndla_matlexicon_attribute'>").replaceAll("]", "</span>]");
					$(this).html(str);
				}
			});
			
			$(li).click(getDetails);
			termlist.appendChild(li);
			
			if(++counter == Drupal.settings.ndla_matlexicon.list_length) {
				break;
			}
		} else {
			if (hit) {
				break;
			}
		}
	}
}

String.prototype.replaceAll = function(token, newToken, ignoreCase) {
    var str, i = -1, _token;
    if((str = this.toString()) && typeof token === "string") {
        _token = ignoreCase === true? token.toLowerCase() : undefined;
        while((i = (
            _token !== undefined? 
                str.toLowerCase().indexOf(
                            _token, 
                            i >= 0? i + newToken.length : 0
                ) : str.indexOf(
                            token,
                            i >= 0? i + newToken.length : 0
                )
        )) !== -1 ) {
            str = str.substring(0, i)
                    .concat(newToken)
                    .concat(str.substring(i + token.length));
        }
    }
return str;
};

function getDetails(e, attemptNum) {
	var node = (e.target.tagName == 'LI') ? e.target : e.target.parentNode;
	var termId = node.getAttribute('id');
	var id = termId.split('_')[1];
	
	var loader = $(node).find('img');
	loader.show();

	jasonUrl = Drupal.settings.ndla_matlexicon.json_details + '/' + id;
	var timeoutCount = (attemptNum) ? attemptNum : 0;
	var ajaxTimeout = parseInt(Drupal.settings.ndla_matlexicon.ajax_timeout);
	var ajaxRetries = parseInt(Drupal.settings.ndla_matlexicon.ajax_retries);

	$.ajax({
		type: 'GET',
		url: jasonUrl,
		dataType: 'json',
		timeout: ajaxTimeout,
		success: function(data) {
		  loader.hide();
			showDetails(data.details.data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
		  loader.hide();
			if ((textStatus != 'timeout') || 
			    ((textStatus == 'timeout') && (++timeoutCount == ajaxRetries))) {
				showDetails('<p>An error occured. Unable to fetch data.</p>');
			} else {
				getDetails(e, timeoutCount);
			}
		}
	});
}

function showDetails (details) {
	$('#ndla_matlexicon_detail').html('');
	backButton = document.createElement('div');
	backButton.setAttribute('id','backButton');
	backButton.innerHTML = '&nbsp;';
	backButton.onclick = function () {
		$('#ndla_matlexicon_detail').addClass('hidden');
		$('#ndla_matlexicon_wrapper').removeClass('hidden');
	};
	$('#ndla_matlexicon_detail')[0].appendChild(backButton);
	$('#ndla_matlexicon_detail').append(details);
	$('#ndla_matlexicon_detail').removeClass('hidden');
	$('#ndla_matlexicon_wrapper').addClass('hidden');
	
}

function getData(letter, attemptNum) {
	$('#ndla_matlexicon_wait').removeClass('hidden');
	$('#ndla_matlexicon_wait_text').removeClass('hidden');
	$('#ndla_matlexicon_result').addClass('hidden');
	$('#ndla_matlexicon_type').attr('disabled', 'disabled');
	url = Drupal.settings.ndla_matlexicon.json_list + '/' + letter + '/' + $('#ndla_matlexicon_type').val();
	$.ajax({
		type: 'GET',
		url: url,
		dataType: 'json',
		success: function(data) {
				$('#ndla_matlexicon_wait').addClass('hidden');
				$('#ndla_matlexicon_wait_text').addClass('hidden');
				$('#ndla_matlexicon_result').removeClass('hidden');
				$('#ndla_matlexicon_type').removeAttr('disabled');
				filterData(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
					$('#ndla_matlexicon_result').html('<p>An error occured. Unable to fetch data.</p>');
		},
	});
}

function trim (str) {
  str = str.replace(/^\s+/, '');
  for (var i = str.length - 1; i >= 0; i--) {
    if (/\S/.test(str.charAt(i))) {
      str = str.substring(0, i + 1);
      break;
    }
  }
  return str;
}

function ndla_matlexicon_read_more(wid) {
  $('#ndla_matlexicon_lightbox').attr('href', Drupal.settings.basePath + 'ndla_matlexicon.full/' + wid).click();
}

function ndla_matlexicon_select(wid) {
  if($('#lightbox:visible').length) {
    $('#bottomNavClose').click();
  }
	
	jasonUrl = Drupal.settings.ndla_matlexicon.json_details + '/' + wid;
	var timeoutCount = 0;
	var ajaxTimeout = parseInt(Drupal.settings.ndla_matlexicon.ajax_timeout);
	var ajaxRetries = parseInt(Drupal.settings.ndla_matlexicon.ajax_retries);

	$.ajax({
		type: 'GET',
		url: jasonUrl,
		dataType: 'json',
		timeout: ajaxTimeout,
		success: function(data) {
			showDetails(data.details.data);
		},
	});
}