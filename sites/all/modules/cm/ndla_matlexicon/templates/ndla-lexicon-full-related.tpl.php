<ul>
<?php foreach($words as $word): ?>
  <?php if(!empty($word['title'])): ?>
    <li>
      <?php if($word['more']) print '<a onclick="ndla_matlexicon_select(' . $word['wid'] . ');">'; ?>
      <?php print $word['title']; ?>
      <?php if($word['more']) print '</a>'; ?>
    </li>
    <?php if(!empty($word['related'])) {
      print theme('ndla_matlexicon_full_related', $word['related']);
    } ?>
  <?php endif; ?>
<?php endforeach; ?>
</ul>