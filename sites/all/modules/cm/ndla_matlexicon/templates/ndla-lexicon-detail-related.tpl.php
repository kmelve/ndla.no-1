<?php foreach($words as $word): ?>
  <dd>
    <?php if(!empty($word['title'])): ?>
    <dt>
      <?php if($word['more']) print '<a onclick="ndla_matlexicon_select(' . $word['wid'] . ');">'; ?>
      <?php print $word['title']; ?>
      <?php if($word['more']) print '</a>'; ?>
    </dt>
    <?php endif; ?>
    <?php if(!empty($word['related']['description'])): ?>
      <?php print theme('ndla_matlexicon_detail_related', $word['related']['description']) ?>
    <?php endif; ?>
    <?php if(!empty($word['related'])): ?>
      <?php print theme('ndla_matlexicon_detail_related', $word['related']) ?>
    <?php endif; ?>
  </dd>
<?php endforeach; ?>