<?php
/**
 * @file
 * @brief
 *  Template file for rendering the matelexicon block.
 *
 * @ingroup ndla_matlexicon
 */
?>
<div id="ndla_matlexicon_wrapper">
  <a id="ndla_matlexicon_lightbox" rel="lightmodal[|width:635px; height:500px; scrolling: auto;]" href=""></a>
	<div id="ndla_matlexicon_searchbox">
		<input type="text" id="ndla_matlexicon_search" name="ndla_matlexicon_search" />
		<div id="ndla_matlexicon_clear_button">
		  &nbsp;
		</div>
		<br class="clear" />
		
		<?php
		  if(sizeof($types) != 1) {
		    echo '<select name="ndla_matlexicon_type" id="ndla_matlexicon_type">';
        foreach($types as $type) {
          echo "<option value='$type'>" . t($type) . "</option>";
        }
        echo '</select>';
      }else{
        echo "<input type='hidden' value='{$types[0]}' id='ndla_matlexicon_type'/>";
      }		
		?>

	</div>
	<div id="ndla_matlexicon_result">
		<ul id="termlist">
		</ul>
	</div>
	<div id="ndla_matlexicon_wait" class="hidden">
	  &nbsp;
	</div>
	<div id="ndla_matlexicon_wait_text" class="hidden">
	  <?php print t('Please wait...'); ?>
	</div>
</div>
<div id="ndla_matlexicon_detail" class="hidden">
</div>