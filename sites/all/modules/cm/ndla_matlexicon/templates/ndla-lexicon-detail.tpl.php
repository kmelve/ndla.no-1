<dl>
  <?php if(!empty($word['title'])): ?>
  <dt>
    <span class="detail-title">
    <h2><?php print str_replace("]","</span>]",str_replace("[","[<span class=\"ndla_matlexicon_attribute\">",$word['title'])); ?></h2>
    <?php if(!empty($word['attribute'])): ?>
      <span class="additional_information">(<?php print $word['attribute']; ?>)</span>
    <?php endif; ?>
    </span>
    <?php if(!empty($word['language'])): ?>
      <?php print theme('ndla_matlexicon_language', $word['language']) ?>
    <?php endif; ?>
  </dt>
  <?php endif; ?>
  <?php foreach($word['description'] as $description): ?>
  <dd>
    <p>
      <?php print $description['text']; ?>
      <?php if(!empty($description['related'])): ?>
        <?php print theme('ndla_matlexicon_detail_related', $description['related']) ?>
      <?php endif; ?>
    </p>
  </dd>  
  <?php endforeach; ?>
  <?php if(!empty($word['related'])): ?>
    <?php print theme('ndla_matlexicon_detail_related', $word['related']) ?>
  <?php endif; ?>
</dl>

<?php

//print_r($word);

?>