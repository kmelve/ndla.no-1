<div class='ndla_matlexicon_full'>
<span class="detail-title">
  <h2 class='ndla_matlexicon_header'><?php print str_replace("]","</span>]",str_replace("[","[<span class=\"ndla_matlexicon_attribute\">",$word['title'])); ?></h2>
  <?php if(!empty($word['attribute'])): ?>
    <span class="additional_information">(<?php print $word['attribute']; ?>)</span>
  <?php endif; ?>
  </span>
  <?php if(!empty($word['language'])): ?>
    <?php print theme('ndla_matlexicon_language', $word['language']) ?>
  <?php endif; ?>
  <?php if(!empty($word['description'])): ?>
  <div class='ndla_matlexicon_title'><?php print t('Description'); ?></div>
  <div class='ndla_matlexicon_content'>
    <?php foreach($word['description'] as $description) {
      print "<p>" . $description['text'] . "</p>";
      if(!empty($description['related'])) {
        print theme('ndla_matlexicon_full_related', $description['related']);
      }
    } ?>
  </div>
  <?php endif; ?>
  <?php if(!empty($word['related'])): ?>
  <div class='ndla_matlexicon_title'><?php if(strpos(strtolower($word['related'][0]['title']), 'rett') === false) print t('Related Info'); else print t('Dishes'); ?></div>
  <div class='ndla_matlexicon_content'>
    <?php if(!empty($word['related'])) {
      print theme('ndla_matlexicon_full_related', $word['related']);
    } ?>
  </div>
  <?php endif; ?>
</div>