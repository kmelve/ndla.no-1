<div id="ndla_matlexicon_languages">
<?php foreach($languages as $language => $name) {
  print '<div>';
  if(file_exists(drupal_get_path('module', 'ndla_matlexicon').'/img/'.$language.'.png')){
    print ' <img src="/'.drupal_get_path('module', 'ndla_matlexicon').'/img/'.$language.'.png" alt="'.$name[0].' ('.$language.')" /> ';
  } else if(!empty($language)) {
    print '(nb)';
  } else {
    print ' <img src="/'.drupal_get_path('module', 'ndla_matlexicon').'/img/nb.png" alt="'.$name[0].' (nb)" /> ';
  }
  print $name[0];
  print '</div>';
} ?>
</div>