; $Id: README.txt,v 1.2 2011/02/01 10:49:09 rsp Exp $

; Module name
ReadSpeaker Expanding Player with Embedded Highlighting module

; Drupal Version
Drupal 6.x

; Description
This module integrates ReadSpeaker Expanding Player to your Drupal 6 website

; Prerequisites
An own account at ReadSpeaker and locale.module may be useful

; Installation
To install, copy the readspeaker_hl directory and all its contents to your modules
directory.

; Configuration
To enable this module, visit administer -> modules, and enable ReadSpeaker Expanding Player module.
Configure settings under administer -> content management -> ReadSpeaker Expanding Player.

; Author
Steffen Schlaer (http://www.it-cru.de), Modified by ReadSpeaker
