var rstext = {
	closeplayer:"St&auml;ng spelare",
	fast:"H&ouml;g",
	hide:"D&ouml;lj",
	highlightingoptions:"Markeringsval",
	listentoselectedtext:"Lyssna på markerad text",
	medium:"Normal",
	nohighlighting:"Ingen markering",
	nosound:"Inget ljud?",
	pause:"Paus",
	playerwidth:"285",
	play:"Play",
	popupbutton:"Popup-knapp",
	sentonly:"Enbart meningar",
	settings:"Inst&auml;llningar",
	show:"Visa",
	slow:"L&aring;ngsam",
	speechenabled:"Talsatt av <a href='http://www.readspeaker.com/sv'>ReadSpeaker</a>",
	speed:"L&auml;shastighet",
	stop:"Stopp",
	volume:"Volym",
	wordonly:"Enbart ord",
	wordsent:"Ord och meningar"
};