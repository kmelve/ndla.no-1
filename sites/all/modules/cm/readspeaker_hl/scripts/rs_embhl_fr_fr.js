var rstext = {
	closeplayer:"Fermez le lecteur",
	fast:"Rapide",
	hide:"Cacher",
	highlightingoptions:"Options de surbrillance",
	listentoselectedtext:"Ecouter le texte sélectionné",
	medium:"Moyen",
	nohighlighting:"Pas de surbrillance",
	nosound:"Aucun son?",
	pause:"Pause",
	playerwidth:"305",
	play:"Lecture",
	popupbutton:"Bouton popup",
	sentonly:"Phrase uniquement",
	settings:"Param&egrave;tres",
	show:"Montrer",
	slow:"Lent",
	speechenabled:"Vocalis&eacute; par <a href='http://www.readspeaker.com/fr'>ReadSpeaker</a>",
	speed:"Vitesse",
	stop:"Stop",
	volume:"Volume",
	wordonly:"Mot uniquement",
	wordsent:"Mot et phrase"
};