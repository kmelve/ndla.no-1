<?php
$readspeaker_languages = readspeaker_hl_language_list();
$protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
?>
<div id="readspeaker_button1" class="rs_skip" <?php print $button_style;?>>
  <a accesskey="L" href="<?php print $protocol; ?>://app.readspeaker.com/cgi-bin/rsent?customerid=<?php print $account_id; ?>&amp;audiofilename=<?php print $audiofilename; ?>&amp;lang=<?php print $lang_id; ?>&amp;readid=rs_read_this&amp;url=<?php print urlencode($url); ?>" 
    onclick="readpage(this.href, 'rs_1'); return false;">
      <img src="<?php print $protocol; ?>://media.readspeaker.com/images/buttons/listen_<?php print $lang_id; ?>/listen_<?php print $lang_id; ?>.gif" border="0" title="<?php print $alt_text; ?>" alt="<?php print $alt_text; ?>" /></a></div>
<div id="rs_1" <?php print $player_style; ?>></div>
