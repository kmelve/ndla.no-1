<div class='plugin_item <?php if(!empty($implementation['class'])) print $implementation['class']; ?>'>
  <a href='#' class='<?php print $implementation['function']; ?>'>
    <div class='image_container'>
      <img class='textselect' src='<?php print $implementation['image']; ?>' />
    </div>

    <?php if($implementation['label']): ?>
      <span><?php print $implementation['label']; ?></span>
    <?php endif; ?>
  </a>
</div>