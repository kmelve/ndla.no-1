var last_selection = '';
ndla_textselect_text_selected = function(event) {
  text = ndla_textselect_get_selection();
  if(last_selection != text) {
    last_selection = text;
    text = text.replace(/<[^>]*>/g, '');
    if($.trim(text).length == 0) {
      $('#textselect').hide();
    } else {  
      if(typeof window.orientation === 'undefined'){
        $('#textselect').css('left', event.pageX + 5);
        $('#textselect').css('top', event.pageY + 10);
        if(typeof ndla_textselect_readspeaker_init != "undefined") {
          ndla_textselect_readspeaker_init($('#textselect'));
        }
      }
      $('#textselect').show();
    }
  }
}

ndla_textselect_close = function() {
  $('#textselect').hide();
}


ndla_textselect_get_selection = function() {
  var b = undefined;
  var a = undefined;
  if (window.getSelection) {
    a = window.getSelection();
    if (!a.isCollapsed) {
      if (a.getRangeAt) {
        b = a.getRangeAt(0)
      } else {
        b = document.createRange();
        b.setStart(a.anchorNode, a.anchorOffset);
        b.setEnd(a.focusNode, a.focusOffset)
      }
      if (b) {
        DOM = b.cloneContents();
        object = document.createElement("div");
        object.appendChild(DOM.cloneNode(true));
        return object.innerHTML
      } else {
        return a
      }
    }
  } else {
    if (document.selection) {
      a = document.selection;
      b = a.createRange();
      if (b && b.htmlText) {
        return b.htmlText
      } else {
        if (b && b.text) {
          return b.text
        }
      }
    } else {
      if (document.getSelection) {
        return document.getSelection()
      }
    }
  }
  return "";
}

ndla_textselect_unbind = function() {
  if(document.addEventListener) {
    document.removeEventListener('mouseup', ndla_textselect_text_selected, false);
  }
  else {
    if(document.attachEvent){
      document.detachEvent("onmouseup", ndla_textselect_text_selected);
    } else {
      document.onmouseup = null;
    }
  }
}

if(typeof window.orientation === 'undefined'){
  if(document.addEventListener){
    document.addEventListener("mouseup", ndla_textselect_text_selected, false);
  } else {
    if(document.attachEvent){
      document.attachEvent("onmouseup", ndla_textselect_text_selected);
    } else {
      document.onmouseup = ndla_textselect_text_selected;
    }
  }
} else {
  document.addEventListener("selectionchange", ndla_textselect_text_selected, false);
}

Drupal.behaviors.ndla_textselect = function(){
  if(typeof window.orientation !== 'undefined'){
    $('#textselect').css('position', 'fixed');
    $('#textselect').css('left', '50px');
    $('#textselect').css('bottom', '50px');
    $('#main_1').prepend($('#textselect'));
  }
  else {
    $('#textselect a').each(function(){
      var hammer = new Hammer(this);
      var c = $(this).attr('class');
      hammer.ontap = function(ev) {
        /**
         * Call a function name found in a strin:
         * http://stackoverflow.com/questions/912596/how-to-turn-a-string-into-a-javascript-function-call
         */
        var func = window[c];
        func(ndla_textselect_get_selection(), ev);
        ndla_textselect_close();
      };
    });
  }
}