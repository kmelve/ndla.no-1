NdlaReminders = {};
NdlaReminders.reminders = [];

NdlaReminders.init = function() {
  var reminders = userData.getData('page', 'reminder');
  
  if(reminders != null) {
    NdlaReminders.reminders = reminders;
  }
  
  $block = $('<div class="block reminders"><ul><li title="' + Drupal.t('Add reminder') + '">' + Drupal.t('Add reminder') + '</li></ul><div id="reminder_form"><div id="reminder_calendar"></div><br><textarea></textarea><br><input type="button" value="' + Drupal.t('Add reminder') + '"></div></div>').appendTo('.region-learning');
  $('li', $block).click(function () {
    $('#reminder_form').slideToggle();
  });
  
  g_jsDatePickImagePath = Drupal.settings.basePath + "sites/all/modules/ndla_reminders/libraries/jsdatepick-calendar/img/";
  
  reminder = new JsDatePick({
		useMode:1,
		isStripped:true,
		target:"reminder_calendar",
		imgPath: Drupal.settings.basePath + "sites/all/modules/ndla_reminders/libraries/jsdatepick-calendar/img/"
	});
	
	$('#reminder_form input').click(function(){
	  var obj = reminder.getSelectedDay();
		if(obj == false) {
		  alert(Drupal.t('No date has been selected.'));
		  return;
	  }
	  var text = $('#reminder_form textarea').val();
		var timestamp = Date.parse(obj.year + '-' + obj.month +'-' + obj.day)/1000;
		$('#reminder_form').slideToggle(function(){
		  NdlaReminders.reminders.push({
		    title: $(document).attr('title'),
        subject: Drupal.settings.ndla_mssclient.fag,
        contenttype: Drupal.settings.ndla_mssclient.contenttype,
        text: text,
        timestamp: timestamp
      });
		  userData.saveData('page', 'reminder', NdlaReminders.reminders);
		  $('#reminder_form textarea').val('');
  	});
	});
}

userData.ready(function () {
  NdlaReminders.init();
});