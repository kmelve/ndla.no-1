NdlaNotify = {};

NdlaNotify.notifications = [];
NdlaNotify.html = null;

NdlaNotify.init = function(loggedIn) {
  var $bubble = $('#notification_bubble');

  if (loggedIn) {
    // Set cookie that signals that the browser will not show initial notify bubble
    document.cookie = "ndlaNotifyInitial=1; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";

    var notifications = userData.getData('global', 'notify');

    if (notifications) {
      this.notifications = notifications;
      var count = this.notifications.length;

      // Set notification bubble
      if (count) {
        $bubble.html(count);
        $bubble.removeClass('hidden');
      }
      
      // Generate item for each notifications in pull down
      if($('#notification_container').length > 0) {
        $notification = $('#notification_container');
        for (var i = 0; i < count; i++) {
          var $li = $('<li>');
          var $elem = $('<a>').attr('href', NdlaNotify.notifications[i].url).addClass('notify').html(NdlaNotify.notifications[i].title);
          $elem.click(NdlaNotify.notifications[i], function(ev) {
            userData.saveData('global', 'notify', ev.data, function() {
              document.location = ev.data.url;
            });

            return false;
          });
          $remove = $('<i>').addClass('icon-remove');
          $remove.click(NdlaNotify.notifications[i], function(ev) {
            userData.saveData('global', 'notify', ev.data);
            $(this).parent().slideUp('fast', function() {
              $(this).remove();
              var number = $('#notification_bubble').html();
              if(number > 1) {
                $('#notification_bubble').html(number - 1);
              } else {
                $('#notification_bubble').hide();
              }
            });
            return false;
          });
          $li.append($remove);
          $li.append($elem);
          $li.append($('<p>').addClass('note').html(NdlaNotify.notifications[i].text));
          $li.append($('<p>').addClass('date').html(NdlaNotify.date(NdlaNotify.notifications[i].timestamp)));
          $notification.append($li);
        } // end for
      } // end if notification container...
    }
  }
  else {
    // Show notification bubble with "1" for users that have never logged in
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var found = false;
      if (cookies[i].indexOf('ndlaNotifyInitial=') > -1) {
        found = true;
        break;
      }

    }
    if (!found) {
      ndla_auth(function(auth) {
        $a = $('<a>').attr('href', auth.login_url() + "&new=1").html('1');
        $bubble.append($a);
        $bubble.removeClass('hidden');
      });
    }
  }
}

NdlaNotify.date = function(timestamp) {
  var date = new Date(timestamp * 1000);

  var pad = function(param) {
    return (param < 10 ? '0' : '') + param;
  }
  
  var year = date.getFullYear();
  var month = pad(date.getMonth() + 1);
  var dateOfMonth = pad(date.getDate());
  var hours = pad(date.getHours());
  var minutes = pad(date.getMinutes());

  return dateOfMonth + '.' + month + '.' + year + ' ' + hours + ':' + minutes;
}


userData.ready(function (loggedIn) {
  NdlaNotify.init(loggedIn);
});
