var h5psave = h5psave || {};

h5psave.init = function (loggedIn) {
  var that = this;

  if (typeof(H5P) != 'undefined') {
    var url = Drupal.settings.ndla_utils.base_url + Drupal.settings.basePath;
    var libs = H5P.getTypes();
    var hasData = false;

    this.index = -1;
    for (var i = 0; i < libs.length; i++) {
      if (libs[i] == 'H5P.TaskPage') {
        this.index = i;
        if (userData.getData('page', 'h5p') != null) {
          hasData = true;
        }
        break;
      }
    }

    if (hasData) {
      this.setSaveMessage();
    }

    var buttons = '<div id="taskpage-buttons"></div>';
    $('.h5p-content').after(buttons);

    $('#taskpage-buttons').append('<div class="button" id="btn_save">' + Drupal.t('Save') + '</div>');
    $('#taskpage-buttons').after('<div id="taskpage-message" style="display: none"></div>')

    if (this.getParameterByName('load')) {
      this.showSavedData();
    }

    // Attach events to save button
    $('#btn_save').hover(function(ev) {
      $(this).addClass('hover');
    },
    function(ev) {
      $(this).removeClass('hover');
    }).click(function(ev) {
      if (loggedIn) {
        userData.saveData('page', 'h5p', {
          title: $(document).attr('title'),
          subject: Drupal.settings.ndla_mssclient.fag,
          contenttype: Drupal.settings.ndla_mssclient.contenttype,
          data: H5P.read(that.index)
        });
        $('#taskpage-message').html(Drupal.t('Answers saved to min.ndla.no')).removeAttr('style');
        that.setSaveMessage();
        setTimeout(function() {$('#taskpage-message').fadeOut();}, 3000);
      }
      else {
        alert(Drupal.t('You have to be logged in to use this function'));
      }
    });
  }
};

h5psave.setSaveMessage = function() {
  var that = this;
  if ($('#ndla-h5psave').length == 0) {
    var $block = $(
      '<div class="block my-h5p"><div id="ndla-h5psave">' + 
      Drupal.t('You have answered this task before') + 
      '<br><a href="#learning" id="h5psave-load">' + 
      Drupal.t('Load previous answer') + '</a>' + 
      '</div></block>'
    ).prependTo('.region-learning');

    // Attach events to load text
    $('#h5psave-load').click(function(ev) {
      that.showSavedData();
    });
  }
}

h5psave.showSavedData = function() {
  var data = userData.getData('page', 'h5p');
  H5P.write(this.index, data.data);
}

h5psave.getParameterByName = function(name)
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null ) {
    return "";
  }
  else {
    return decodeURIComponent(results[1].replace(/\+/g, " "));
  }
}

userData.ready(function (loggedIn) {
  h5psave.init(loggedIn);
});

// Functions used by ndla_my_learning for saving h5p data to min.ndla.no
if (typeof(H5P) != 'undefined') {
  H5P.libs = [];
  H5P.getTypes = function() {
    var libs = [];
    for (var i = 0; i < this.libs.length; i++) {
      libs[i] = this.libs[i].machineName;
    }
    return libs;
    };

  H5P.read = function (idx) {
    var data = null;

    if (this.libs[idx] && this.libs[idx].read) {
      data = this.libs[idx].read();
    }

    return data;
  };

  H5P.write = function (idx, data) {
    if (this.libs[idx] && this.libs[idx].write) {
      this.libs[idx].write(data);
    }
  };
}