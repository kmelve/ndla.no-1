function NdlaMyls(loggedIn) {
  var selected = userData.getData('page', 'learningStatus');
  that = this;
  
  $block = $('<div class="block my-learning-status"><ul><li title="' + Drupal.t('Learning status') + '">' + Drupal.t('Learning status') + '</li></ul><div id="learning_form"></div></div>').appendTo('.region-learning');;
  $('li', $block).click(function () {
    $('#learning_form').slideToggle();
  });
  
  $block = $('<ul><li class="green" title="' + Drupal.t('The content on this site are reviewed, and I feel I have good control') + '">' + Drupal.t('Learned and understood') + '</li><li class="yellow" title="' + Drupal.t("The content on this page have been reviewed, but I'm not sure I master the material as well that I want") + '">' + Drupal.t('Perceived, but must be repeated') + '</li><li class="red" title="' + Drupal.t('I have not worked well enough with the content of this page and need more time and / or assistance') + '">' + Drupal.t('Not sufficiently understood') + '</li></ul>').appendTo('#learning_form');
  this.$lis = $('li', $block);
  this.store = false;
  this.$lis.click(function () {
    that.select($(this));
  });
  if (selected != undefined && selected) {
    this.select(this.$lis.filter('.' + selected.color));
  }
  this.store = true;
  
  if (selected !== null && typeof selected.color != 'undefined' && selected.color) {
    $('#learning-title').addClass(selected.color);
  }

  ndla_auth(function(auth) {
    if (auth.is_authenticated()) {
      if(auth.is_teacher()) {
        $('.region-learning > .my-learning-status > ul > li').html(Drupal.t('Teaching status'));
        $('.region-learning > .my-learning-status #learning_form ul > li.green').html(Drupal.t('Top dividend'));
        $('.region-learning > .my-learning-status #learning_form ul > li.yellow').html(Drupal.t('Good dividend'));
        $('.region-learning > .my-learning-status #learning_form ul > li.red').html(Drupal.t('Little dividend'));
      }
    }
  });

  /* client */
  //var color = '';
  //if (selected !== null && typeof selected.color != 'undefined' && selected.color) {
  //  color = ' ' + selected.color;
  //}
  //$mark = $('<li><a href="#" class="learningcurve" title="' + Drupal.t('Add learning status') + '"><span class="icon' + color + '"></span></a></li>')
  //$mark.children('a').click(function () {
  //  $('.js-link.learning').click();
  //  $('#learning_form').show();
  //});
  //$('#node-actions').append($mark);
  /* client */
}

NdlaMyls.prototype.select = function ($li) {
  var isSelected = $li.hasClass('selected');
  this.$lis.removeClass('selected').removeClass('not-selected');
  $('#learning-title').removeClass('red green yellow');
  if (!isSelected) {
    $('#node-actions .learningcurve .icon').removeClass('red yellow green').addClass($li.attr('class'));
    $('#learning-title').addClass($li.attr('class'));
    if (this.store) {
      userData.saveData('page', 'learningStatus', {
        color: $li.attr('class'), 
        title: $(document).attr('title'),
        subject: Drupal.settings.ndla_mssclient.fag,
        contenttype: Drupal.settings.ndla_mssclient.contenttype
      });
      if($('a.learningcurve').length == 1 && !$('a.learningcurve').hasClass('marked')) {
        $('a.learningcurve').click();
      }
    }
    $li.addClass('selected');
    this.$lis.not($li).addClass('not-selected');
  }
  else if (this.store) {
    $('#node-actions .learningcurve .icon').removeClass('red yellow green');
    userData.removeData('page', 'learningStatus');
  }
}

userData.ready(function (loggedIn) {
  new NdlaMyls(loggedIn);
});