function my_courses(loggedIn) {
  var courses = userData.getData('global', 'subjects');
  if(courses) {
    if($('#fag-title .fag-title-wrap').length != 0) {
      $('#fag-title .fag-title-wrap a.fag-title').after('<span class="fa-stack fa-lg"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>');
      var nid = $('#fag-title .fag-title-wrap a.fag-title').attr('href').match(/[\d]+/);
      var title = $('#fag-title .fag-title-wrap a.fag-title').html().replace(/^[\s]*/, '').replace(/[\s]*$/, '');
      var favorite = false;
      for(var i = 0; i < courses.length; i++) {
        if(courses[i].value == 1 && courses[i].id == nid) {
          $('#fag-title .fag-title-wrap .fa-lg').addClass('marked');
          favorite = true;
          break;
        }
      }
      $('#fag-title .fag-title-wrap .no-favorite, #fag-title .fag-title-wrap .fa-lg').click(function() {
        $(this).toggleClass('marked');
        favorite = !favorite;
        for(var i = 0; i < courses.length; i++) {
          if(courses[i].id == nid) {
            courses[i].value = favorite;
            userData.saveData('global', 'subjects', courses);
            return;
          }
        }
        courses.push(
          {
            'id' : nid,
            'title' : title,
            'value' : favorite,
            'guid' : ''
          }
        );
        userData.saveData('global', 'subjects', courses);
      });
    }
  }
}

if(typeof userData != 'undefined') {
  userData.ready(function (loggedIn) {
    my_courses(loggedIn);
  });
}