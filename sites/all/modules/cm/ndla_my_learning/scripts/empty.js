$(window).load(function(){
  new JStorage('ndla_msclient', function (storage) {
    if (storage.isLoggedIn()) {
      return;
    }
    for (var i = 0; i < userData.onReadies.length; i++) {
      userData.onReadies[i](false);
    }
    userData.onReadies = [];
    $('.region-learning li, #node-actions a.learningcurve').unbind().click(function() {
      if(confirm(Drupal.t('You have to be logged in to use this function. Do you want to login?'))) {
        window.location.href = $('#seria_login_link').attr('href');
      }
    });
  });
});