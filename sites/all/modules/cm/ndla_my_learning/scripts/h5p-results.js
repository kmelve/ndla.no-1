$(document).ready(function () {
  if (window.H5P === undefined || window.H5PIntegration === undefined) {
    return; // H5P not present
  }

  /** @private */
  var $ = H5P.jQuery;

  /**
  * Return unix timestamp for the given JS Date.
  *
  * @private
  * @param {Date} date
  * @returns {Number}
  */
  var toUnix = function (date) {
    return Math.round(date.getTime() / 1000);
  };

  /**
  * Log H5P user results.
  *
  * @public
  * @namespace H5P
  * @param {Number} contentId
  * @param {Number} score achieved
  * @param {Number} maxScore that can be achieved
  * @param {Number} time optional reported time usage
  */
  var setFinished = function (contentId, score, maxScore, time) {
    // Get opened and finished in unix time.
    var opened;
    if (H5P.opened !== undefined) {
      opened = toUnix(H5P.opened[contentId]);
    }
    var finished = toUnix(new Date());
    if (time === undefined && opened !== undefined) {
      time  = finished - opened;
    }

    // Save results
    userData.saveData('page', 'h5p_results', {
      title: $('h1:first').text().trim(),
      course: $('.fag-title:first').text().trim(),
      type: $('.nodetype:first').text().trim(),
      score: score,
      max_score: maxScore,
      opened: opened,
      finished: finished,
      time: time
    });
    // TODO: Get props from JS vars?
  };

  // Override user results logging
  H5PIntegration.postUserStatistics = true;
  H5P.setFinished = function () {
    var self = this;
    var result = arguments;

    if (!userData.inited) {
      // Wait for user data to become ready
      userData.ready(function (loggedIn) {
        setFinished.apply(self, result);
      });
      return;
    }
    setFinished.apply(self, result);
  };
});
