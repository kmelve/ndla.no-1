(function() {

  var __hasProp = Object.prototype.hasOwnProperty;
  var __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Annotator.Plugin.NdlaStore = (function(_super) {

    __extends(NdlaStore, _super);

    NdlaStore.prototype.events = {
      'annotationCreated': 'annotationCreated',
      'annotationDeleted': 'annotationDeleted',
      'annotationUpdated': 'annotationUpdated'
    };

    function NdlaStore(element, options) {
      NdlaStore.__super__.constructor.apply(this, arguments);
      this.annotations = [];
      this.notes = [];
    }

    NdlaStore.prototype.pluginInit = function() {
      var _this = this;
      var notes = userData.getData('page', 'ndla_annotation');
      if(notes != null) {
        $(notes).each(function(key, note) {
          if(note.ranges != undefined) {
            _this.notes.push({text : note.text, quote : note.quote, ranges: note.ranges, title: $(document).attr('title'), subject: Drupal.settings.ndla_mssclient.fag, contenttype: Drupal.settings.ndla_mssclient.contenttype});
            _this.annotations.push(note);
          } else {
            /**
             * Special case for version 1 of notes.
             * Save them, but don't show where they
             * are located.
             */
            _this.notes.push(note);
            notes.splice(key, 1);
            _this.deprecated_display(note);
          }
        });
        this.annotator.loadAnnotations(this.annotations);
      }

      if (window.H5P !== undefined) {
        // Reset
        $('.h5p-initialized').html('').removeClass('h5p-initialized');
        // Re-init
        H5P.init();
      }
    };

    NdlaStore.prototype.annotationCreated = function(annotation) {
      this.notes.push({text : annotation.text, quote : annotation.quote, ranges: annotation.ranges, title: $(document).attr('title'), subject: Drupal.settings.ndla_mssclient.fag, contenttype: Drupal.settings.ndla_mssclient.contenttype});
      userData.saveData('page', 'ndla_annotation', this.notes);
    };

    NdlaStore.prototype.annotationUpdated = function(annotation) {
      var index = this.indexOf(annotation);
      if(index != -1) {
        this.notes.splice(index, 1);
      }
      this.notes.push({text : annotation.text, quote : annotation.quote, ranges: annotation.ranges, title: $(document).attr('title'), subject: Drupal.settings.ndla_mssclient.fag, contenttype: Drupal.settings.ndla_mssclient.contenttype});
      userData.saveData('page', 'ndla_annotation', this.notes);
    };

    NdlaStore.prototype.annotationDeleted = function(annotation) {
      var index = this.indexOf(annotation);
      if(index != -1) {
        this.notes.splice(index, 1);
        userData.saveData('page', 'ndla_annotation', this.notes);
      }
    };

    NdlaStore.prototype.indexOf = function(annotation) {
      var range = JSON.stringify(annotation.ranges);
      var result = -1;
      this.notes.forEach(function(note, key) {
        if(range == JSON.stringify(note.ranges)) {
          result = key;
        }
      });
      return result;
    };

    NdlaStore.prototype.deprecated_display = function(note) {
      var _this = this;

      $('.js-link.stickers').parent().show();
      $('.js-link.stickers').click();

      var region = $('.region-stickers .block:first');
      var thread = "<div class='ndla-annotate timestamp" + note[0].timestamp + "'><span class='text'>" + note[0].text + "</span><a><i class='icon-trash'></i>" + Drupal.t('Delete') + "</a><span class='date'>" + note[0].time + "</span></div>";
      region.append(thread);
      $('.timestamp' + note[0].timestamp + ' a').click(function() {
        var timestamp = $(this).parent().attr('class').match(/timestamp([\d]*)/)[1];
        var index = _this.deprecated_indexOf(timestamp);
        if(index != -1) {
          _this.notes.splice(index, 1);
        }
        $(this).parent().remove();
        userData.saveData('page', 'ndla_annotation', _this.notes);
      })
    };

    NdlaStore.prototype.deprecated_indexOf = function(timestamp) {
      var result = -1;
      this.notes.forEach(function(note, key) {
        if(note[0] != undefined && note[0].timestamp == timestamp) {
          result = key;
        }
      });
      return result;
    };

    return NdlaStore;

})(Annotator.Plugin);

}).call(this);
