/**
 * @file
 * @ingroup ndla_msclient
 */
var userData = userData || {};

if(typeof jq172 == "undefined") {
  var jq172 = jQuery;
}

jq172(window).load(function(){
  userData.checkIn();
});


userData.query = function(data, callback) {
  var base = Drupal.settings.ndla_msclient_services_url;
  var url =  base + '/sites/all/modules/ndla_msservices/ndla_msservices.php';
  var jsonData = {success: 0};
  data.msservices_roamauth = getRoamAuthUrl();
  jq172.ajax(url, {
    type: 'POST',
    data: data,
    contentType: 'text/plain',
    xhrFields: {
      withCredentials: true,
    },
    success: function(data) {
      jsonData = jq172.parseJSON(data);
    },
    complete: function() {
      if (callback) {
        callback(jsonData);
      }
    }
  });
}

userData.receivedData = false;

userData.checkIn = function() {
  setTimeout(function() {
    if (!userData.receivedData) {
      userData.checkIn();
    }
  }, 4000); // Ajax timeout - This is done because of a bug in IE9 I can't find a solution for. Seems like the onload-event is triggered before the page is loaded

  new JStorage('ndla_msclient', function (storage) {
    if (!storage.isLoggedIn()) {
      userData.receivedData = true;
      return;
    }
    var queryData = userData.getDefaultRequestData();
    queryData.msservices_op = 'check_in';
    userData.query(queryData, function(data) {
      if (!userData.receivedData) { // Prevent checking in twice
        userData.receivedData = true;
        userData.init(data);
      }
    });
  });
}

userData.consistentScopes = ['page', 'site', 'global'];

userData.init = function(data) {
  for (var i = 0; i < userData.consistentScopes.length; i++) {
    var scope = userData.consistentScopes[i];
    if (scope in data) {
      userData[scope] = eval(data[scope]);
    }
  }
  for (var i = 0; i < userData.onReadies.length; i++) {
    userData.onReadies[i](true);
  }
  userData.onReadies = [];
  userData.inited = true;
}

userData.saveData = function(scope, namespace, data, callback) {
  var queryData = userData.getDefaultRequestData();
  queryData.data = JSON.stringify(data);
  queryData.scope = scope;
  queryData.namespace = namespace;
  queryData.msservices_op = 'set';
  if (!callback) {
    callback = userData.saved;
  }
  userData.query(queryData, callback);
  if (jq172.inArray(scope, userData.consistentScopes) != -1) {
    if (typeof userData[scope] == 'undefined') {
      userData[scope] = {};
    }
    userData[scope][namespace] = data;
  }
}

userData.removeData = function(scope, namespace) {
  var queryData = userData.getDefaultRequestData();
  queryData.scope = scope;
  queryData.namespace = namespace;
  queryData.msservices_op = 'delete';
  userData.query(queryData, userData.saved);
  if (jq172.inArray(scope, userData.consistentScopes) != -1) {
    if (typeof userData[scope] == 'undefined') {
      userData[scope] = {};
    }
    userData[scope][namespace] = {};
  }
}

userData.saved = function(data) {
  // Todo: Handle namespace info etc...
}

userData.getData = function(scope, namespace) {
  if(typeof userData[scope] != 'undefined' && typeof userData[scope][namespace] != 'undefined') {
    return userData[scope][namespace];
  }
  return null;
}

userData.getConditionalData = function(namespace, func) {
  var queryData = userData.getDefaultRequestData();
  queryData.namespace = namespace;
  queryData.msservices_op = 'get';
  userData.query(queryData, func);
}

userData.deleteData = function(scope, namespace) {

}

userData.getDefaultRequestData = function() {
  return {
    site: Drupal.settings.ndla_mssclient.base_url,
    language: Drupal.settings.ndla_mssclient.language,
    page: Drupal.settings.ndla_mssclient.q,
    title: jq172(document).attr('title')
  }
}

userData.onReadies = [];
userData.ready = function(onReady) {
  userData.onReadies.push(onReady);
  if (userData.inited) {
    onReady();
  }
}
