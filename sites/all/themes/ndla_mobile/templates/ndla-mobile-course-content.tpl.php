<?php
extract($vars);
?>


<?php if($fb_js = variable_get('ndla_utils_facebook_js', '')) print $fb_js; ?>
<?php if ($editorial): ?>
<div id="editorial">
  <?php if ($node->nid == 42): ?>
    <h2 class="block-tab"><?php print t('Recommended subject resources') ?></h2>
  <?php endif ?>

  <div id="editorial-content">
    <?php print $editorial ?>
  </div>
</div>
<?php endif ?>
          
<?php if($node->ndla_utils_ndla2010_theme_fag_template == 2): ?>
<?php if(!empty($node->ndla_feeds)): ?>
<div id='ndla_feeds' class='tabbed_content'>
  <?php $li = array(); $content = array(); ?>
  <ul class='tabs block-tab'>
    <?php 
    $num_feeds = count($node->ndla_feeds);
    $classes = array(1 => 'full', 2 => 'fifty', 3 => 'third');
    foreach($node->ndla_feeds as $index => $url) {
      $item = ndla_feeds_parse_url($url); 
      $class = ($index == 0) ? ' active ' : '';
      $class .= !empty($classes[$num_feeds]) ? $classes[$num_feeds] : '';
      $li[] = "<li class='tab_title $class'><h2 class='block-tab'><a class='feed-" . $index . "' href='javascript:void(0)'>" . $item['title'] . "</a></h2></li>";
      $content[] = "<div class='feed-content node-" . $node->vid . " feed-" . $index . " " . $class ."'> .... </div>";
    } 
    print implode("", $li);
  print "</ul>";
  print "<div class='clearfix'></div>";
  print "<div class='content'>" . implode("", $content) . "</div>";
?>
</div>
<?php else: ?>
<div id='ndla_feeds'>
  <h2 class='block-tab'><?php print('Feeds'); ?></h2>
  <div class='content'>
    <?php print t('Feeds template selected, but no feeds are available.'); ?>
  </div>
</div>
<?php endif; ?>
<?php endif; ?>
          
<div id="left-blocks">
  <div id="first-blocks">
    <?php if($node->ndla_utils_ndla2010_theme_fag_template != 2): ?>
    <div class="recommend">
      <h2 class="block-tab"><?php print t('The editor recommends') ?></h2>
      <?php if ($new_content) print $new_content ?>
    </div><!-- end recommend -->
    <?php endif; ?>

    <div class='user-recommends'>
      <h2 class="block-tab"><?php print t('Our users think') ?></h2>
      <?php if (module_exists('ndla_feedback')): ?>
      <div class="first">
        <?php 
        $block = module_invoke('ndla_feedback', 'block', 'view', 'most_liked', 'course_page');
        print '<h3>' . $block['subject'] . '</h3>';
        print '<div id="block-ndla-feedback-most-liked">' . $block['content'] . '</div>'; ?>
      </div><!-- end first -->

      <div class="last">
        <?php 
        $block = module_invoke('ndla_feedback', 'block', 'view', 'recent_comments', 'course_page');
        if(!empty($block['content'])) {
          print '<h3>' . $block['subject'] . '</h3>';
          print '<div id="block-ndla-feedback-recent-comments">' . $block['content'] . '</div>'; 
        }
        ?>
      </div> <!-- end last -->
      <?php else: ?>
      <h3><?php print t("The service isn't available at this site."); ?></h3>
      <?php endif ?>

      <a href="<?php print $feedback_url ?>" class="feedback-icon"><i class='icon-circle-arrow-right'></i><?php print t('More') ?></a>
      <div class='clearfix'></div>
    </div><!-- end user recommends -->
  </div><!-- end first-blocks -->
</div><!-- end left-blocks -->
<?php if($tweets): ?>
<div id="twitter-block">
  <h2 class="block-tab twitter"><i class='icon-twitter'></i><?php print t('Twitter') ?></h2>
  <ul>
    <?php print $tweets ?>
  </ul>
</div> <!-- end twitter-block -->
<?php endif; ?>
<?php
if(function_exists('ndla_utils_get_fb_html')) {
  print ndla_utils_get_fb_html($node->nid);
}
?>
