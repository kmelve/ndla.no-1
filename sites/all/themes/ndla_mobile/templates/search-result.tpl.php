<?php
global $base_url;
$base_url . '/' . imagecache_create_path('Liten', $result['node']->ss_thumbnail);
?>

<li>
  <h2><?php print l($title, $url, array('html' => TRUE)) ?> <span class="type">(<?php print $result['type'] ?><?php if(!empty($result['node']->is_updated)) print ", " . date('d.m.Y', $result['node']->is_updated); ?>)</span></h2>
  <p><?php if(!empty($result['node']->ss_thumbnail)): ?>
    <img src="<?php print $base_url . '/' . imagecache_create_path('Liten', $result['node']->ss_thumbnail); ?>" alt="<?php if(!empty($result['node']->ss_alt_text)) print $result['node']->ss_alt_text; ?>"/>
  <?php endif; ?><?php print $snippet ?></p>
</li>
