<?php global $base_path; ?>
<?php foreach($applications as $application): ?>
<div class='subjects applications'>
  <a href="<?php print $application->href ?>" target="_blank">
    <img src="<?php print str_replace("//", "/", $base_path . $application->image) ?>" alt="<?php print $application->title ?>"/>
  </a>
</div>
<?php endforeach;
