<?php global $user;
if(!module_exists('ndla_auth_js')) {
  return;
}
?>
<div class='right-box'>
  <div class='item login'>
    <span id='notification_bubble' class='hidden'></span>
    <p class='not_logged_in'><a id='seria_login_link' href='#' title='<?php print t('Login'); ?>'><i class='icon-user'></i><span><?php print t('Login'); ?></span></a></p>
    <p class='logged_in'><a id='user_dropdown_toggler' href='javascript:void(0);'><i class='icon-user logged_in_user_img'></i><span class='seria_user_name'>...</span><i class='toggler icon-caret-down'></i></a></p>
    <div class='login-dropdown'>
      <ul id="notification_container"></ul>
      <a id='myfav_link' href='#' target="_blank"><i class='icon-star'></i><?php print t('min.ndla.no'); ?></a>
      <a id='seria_logout_link' href='#' title='<?php print t('Logout'); ?>'><i class='icon-signout'></i><?php print t('Logout'); ?></a>
    </div>
  </div>
</div>
