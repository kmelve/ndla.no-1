<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6 ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <!--<![endif]-->
<head>
  <?php print $head; ?>
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
  <title><?php print $head_title; ?></title>
  <script type="text/javascript" async src="<?php print $base_path.path_to_theme(); ?>/js/modernizr.js"></script>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type='text/javascript' src='<?php print '/' . drupal_get_path('module', 'ndla_oembed') . '/js/ndla_oembed.js'; ?>'></script>
  <!-- IE Fix for HTML5 Tags -->
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <base target="_blank">
</head>

<body class="page-oembed <?php print $body_classes; ?>">
  <section id="main" role="main" class="">
    <div class="content-wrapper">
      <?php if (!empty($title)): ?><h1 class="page-title" id="page-title"><?php print $title ?></h1><?php endif; ?>
      <?php if ($author): ?><div class="author"><?php print $author ?></div><?php endif ?>
      <?php if (module_exists('readspeaker_hl') && $node->type !='fag'): ?><div class='readspeaker_custom_play'><i class="icon-volume-up"></i></div><?php endif; ?>

      <?php print $content; ?>

      <?php
      // The name of the module implementing hook_block.
      $module = 'ndla_handheld';
      $delta = 'handheld_licenses';

      // Get the block as an object and set the module and delta properties.
      $block = (object) module_invoke($module, 'block', 'view', $delta);
      $block->module = $module;
      $block->delta = $delta;

      // Now you can print the themed block.
      $block_content = theme('block', $block);
      ?>
      <?php if (!empty($block_content)): ?>
        <div id="content_bottom">
          <?php print $block_content; ?>
        </div>
      <?php endif; ?>
    </div> <!-- /.content_wrapper -->
  </section>

  <?php print $closure ?>

</body>
</html>
