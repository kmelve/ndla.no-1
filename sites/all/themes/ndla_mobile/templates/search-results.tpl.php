<div class="hits"><?php print format_plural($hits, '1 result', '@count results') ?></div>
<div class='paragraph container'>

  <div class='frontside'>

    <div class="paragraph-actions flip-clicker">
      <a href='javascript:void(0)'>
        <i class='icon-resize-horizontal'>&nbsp;</i>
      </a>
    </div>

    <ol class="search-results <?php print $type ?>-results">
      <?php print $search_results ?>
    </ol>
    <div class='paragraph'></div>

  </div>

  <div class='backside'>
    <div class="paragraph-actions flip-clicker">
      <a href='javascript:void(0)'>
        <i class='icon-resize-horizontal'>&nbsp;</i>
      </a>
    </div>

    <?php echo theme('blocks', 'content_bottom'); ?>

  </div>
</div>
<?php print $pager ?>
