<div class='paragraph container'>

    <?php $column = trim($column); $body = trim($body); ?>
    <?php if(!empty($body)) { ?>
      <?php if(!empty($column)) { ?>
        <div class='frontside'>
        <div class="paragraph-actions flip-clicker">
          <a href='javascript:void(0)'>
            <i class='icon-resize-horizontal'>&nbsp;</i>
          </a>
        </div>
        <?php } else { ?>
          <div class="frontside-full">
          <?php } ?>
            <?php print $body ?>
          </div>
    <?php } ?>
    
    <?php if(!empty($column)) { ?>
    <div class='backside'>
      <div class="paragraph-actions flip-clicker">
        <a href='javascript:void(0)'>
          <i class='icon-resize-horizontal'>&nbsp;</i>
        </a>
      </div>
      <div class='content'>
        <?php print $column ?>
      </div>
    </div>
    <?php } ?>
</div>
