<?php

function ndla_mobile_theme() {
  global $theme_path;
  $reg = array(
    'ndla_mobile_course_content' => array(
      'arguments' => array('vars' => NULL),
      'template' => 'ndla-mobile-course-content',
      'path' => $theme_path . '/templates'
    ),
    'ndla_mobile_courses' => array(
      'arguments' => array('courses' => NULL),
      'template' => 'ndla-mobile-courses',
      'path' => $theme_path . '/templates'
    ),
    'ndla_mobile_page_front' => array(
      'arguments' => array('vars' => NULL),
      'template' => 'ndla-mobile-page-front',
      'path' => $theme_path . '/templates'
    ),
    'ndla_mobile_applications' => array(
      'arguments' => array('applications' => NULL),
      'path' => $theme_path . '/templates',
      'template' => 'ndla-mobile-applications'
    ),
    'ndla_mobile_login' => array(
      'path' => $theme_path . '/templates',
      'template' => 'ndla-mobile-login',
    ),
  );
  return $reg;
}

function ndla_mobile_add_js() {
  global $theme_path;
  
  // Add default js
  drupal_add_js('misc/jquery.js', 'theme');
  drupal_add_js('misc/drupal.js', 'theme');
  drupal_add_js(drupal_get_path('module', 'utdanning_expander') ."/jquery.expander.js", "theme");
  
  // All custom stuff here
  if(arg(0) != 'admin') {
    drupal_add_js($theme_path . '/js/jquery-1-7.js', 'theme'); 
  }
  drupal_add_js($theme_path . '/js/scripts.js', 'theme'); 
  drupal_add_js($theme_path . '/js/jquery.cookie.js', 'theme'); 
  drupal_add_js($theme_path . '/js/hammer.min.js', 'theme'); 
  drupal_add_js($theme_path . '/js/jquery.hammer.min.js', 'theme'); 
  drupal_add_js($theme_path . '/js/jquery.flyin.js', 'theme');
  drupal_add_js($theme_path . '/js/modernizr.js', 'theme'); 
  drupal_add_js($theme_path . '/js/cerpus-slider.js', 'theme'); 
  drupal_add_js($theme_path . '/js/mobile_menu.js', 'theme');
  drupal_add_js($theme_path . '/js/scripts.js', 'theme');

  if(module_exists('ndla_auth_js')) {
    drupal_add_js(drupal_get_path('module', 'ndla_auth_js') . '/js/ndla_auth.js', 'theme');
  }

 

  if(module_exists('ndla_audio')) {
    $directory = variable_get('jplayer_directory', 'sites/all/libraries/jplayer');
    if (file_exists($directory . '/jquery.jplayer.min.js')) {
      $filepath = $directory . '/jquery.jplayer.min.js';
    } elseif (file_exists($directory . '/jquery.jplayer.js')) {
      $filepath = $directory . '/jquery.jplayer.js';
    }
    if (isset($filepath)) {
      $path = drupal_get_path('module', 'ndla_audio');
      drupal_add_js("$path/js/jquery16.min.js", 'theme');
      drupal_add_js($filepath, 'theme');
      drupal_add_js("$path/js/jquery-switch.js", 'theme');
    }
  }

  // Add collapse js for fieldsets  
  drupal_add_js('misc/collapse.js', 'theme');
  
  if(module_exists('ndla_msclient') && module_exists('jstorage')) {
    drupal_add_js(drupal_get_path('module', 'ndla_msclient') . '/jquery-1.7.2.min.js', 'theme');
    drupal_add_js(drupal_get_path('module', 'ndla_msclient') . '/ndla_msclient.js', 'theme');
    drupal_add_js(drupal_get_path('module', 'jstorage') . '/js/jstorage.js', 'theme');
    drupal_add_js(drupal_get_path('module', 'jstorage') . '/js/jquery.json.js', 'theme');
    drupal_add_js(drupal_get_path('module', 'jstorage') . '/js/jstorage_helper.js', 'theme');
  }

  // Only render stuff from the theme scope, cause all other stuff seams abit to much..
  $js = drupal_add_js();

  foreach($js['core'] as $filename => $filedata) {
    // Add to theme scope if matches expression
    if(preg_match("/^sites\/default\/files\/languages\/(.*)/", $filename)) {
      $js['theme'][$filename] = $filedata;
    }
  }

  if(arg(0) != 'admin') {
    // Make exceptions for javascripts:
    $js_exceptions = FALSE;
    foreach ($js['module'] as $jsm=>$j) {
      if(substr($jsm,0,22) != 'sites/all/modules/quiz'
      && substr($jsm,0,25) != 'sites/all/modules/quiz_ty'
      && substr($jsm,0,21) != 'sites/all/modules/h5p'
      && substr($jsm,0,23) != 'sites/default/files/h5p') {
        unset($js['module'][$jsm]);
      } else {
        $js_exceptions = TRUE;
      }
    }
    if(!$js_exceptions) {
      unset($js['core']);
    }
  }
  return $js;
}

function ndla_mobile_preprocess_page(&$vars){

  global $theme_path;

  // Strip duplicate head charset metatag
  $matches = array();
  preg_match_all('/(<meta http-equiv=\"Content-Type\"[^>]*>)/', $vars['head'], $matches);
  if(count($matches) >= 2){
    $vars['head'] = preg_replace('/<meta http-equiv=\"Content-Type\"[^>]*>/', '', $vars['head'], 1); // strip 1 only
  }

  // Unset all css files{
  //unset($vars['css']['all']['module']);

  $vars['styles'] = drupal_get_css($vars['css']);
  if(arg(0) == 'admin') {
    $vars['styles'] = drupal_get_css();
  }


  // Add content browser css back
  $vars['css']['all']['module'] = array();
  $vars['css']['all']['module'][] = 'sites/all/modules/contentbrowser/css/contentbrowser.css';

  $js = ndla_mobile_add_js();
  // Tadaa, all set!
  $vars['scripts'] = drupal_get_js("header", $js);

  if(drupal_is_front_page()) {
    ndla_mobile_preprocess_front($vars);
  }

  if(module_exists('ndla_utils')) {
    /* Get current course */
    if (function_exists("ndla_utils_disp_get_course")) {
      // Try to fetch fag context.
      $course = ndla_utils_disp_get_course();
    }

    if(empty($course)) {
      if(!empty($vars['node']->og_groups)) {
        $og_nid = array_shift($vars['node']->og_groups);
        $course = node_load($og_nid);
      }
      else if(!empty($vars['node']->nid)) {
        $g_nid = db_fetch_object(db_query("SELECT group_nid FROM {og_ancestry} WHERE nid = %d LIMIT 1", $vars['node']->nid))->group_nid;
        if(!empty($g_nid)) {
          $course = node_load($g_nid);
        }
      }
    }
  }

  /* Get course list*/
  $courses_tmp = ndla_utils_get_courses_for_frontpage();
  $courses = '';

  $vars['courses'] = theme('fieldset', array(
    '#value' => theme('ndla_mobile_courses', $courses_tmp[0]->children),
    '#title' => $courses_tmp[0]->name,
    '#attributes' => array(
      'class' => 'subject_dropdown first',
    ),
  ));
  
  $vars['courses'] .= theme('fieldset', array(
    '#value' => theme('ndla_mobile_courses', $courses_tmp[2]->children),
    '#title' => $courses_tmp[2]->name,
    '#attributes' => array(
      'class' => 'subject_dropdown second',
    ),
  ));
  
  $vars['courses'] .= theme('fieldset', array(
    '#value' => theme('ndla_mobile_courses', $courses_tmp[1]->children),
    '#title' => $courses_tmp[1]->name,
    '#attributes' => array(
      'class' => 'subject_dropdown third',
    ),
  ));

  if(empty($course)) {
    $course = new stdClass();
  }
  if(empty($course->title)) {
    $course->title = "Velg fag";
  }
  $vars['ndla_mobile_course_dropdown'] = '<div id="course_selector" class="dropdown clearfix"><h2>'.$course->title.'</h2><div class="scrollable">'.$vars['courses'].'</div></div>';

  // Dropdown search
  $vars['ndla_mobile_search_dropdown'] = '<div id="header_search" class="dropdown clearfix"><h2>Search</h2><div class="scrollable">'.drupal_get_form('search_block_form').'</div></div>';

  // Language switcher
  $path = drupal_is_front_page() ? '<front>' : $_GET['q'];
  $languages = language_list();
  $languages_html = '<ul>';
  foreach($languages as $language) {
    $language->language;
    $languages_html .= '<li>' . l($language->native, $path, array('language' => $language)) . '</li>';
  }
  $languages_html .= '</ul>';
  $vars['ndla_mobile_language_dropdown'] = '<div id="language_selector" class="dropdown clearfix"><h2>'.t('Choose language').'</h2><div class="scrollable"><fieldset class="subject_dropdown">' . $languages_html . '</fieldset></div></div>';

  $vars['author'] = '';
  if ($vars['node'] && module_exists('ndla_authors') && ndla_authors_show_authors($vars['node']->type, $vars['node'], $course)) {
    $vars['author'] = theme('ndla_authors_links', $vars['node']);
  }
  
  if($vars['node']->type == 'fag') {
    unset($vars['context']);
  }
}

function ndla_mobile_preprocess_node(&$vars) {
  unset($vars['links']);
  unset($vars['terms']); //Going to be displayed in a block.
  
  //Remove OG-template if available.
  if(!empty($vars['template_files'])) {
    $index = array_search('node-og-group-post', $vars['template_files']);
    if($index !== FALSE) {
      unset($vars['template_files'][$index]);
    }
  }
  
  //Go out and look for ndla_mobile_preprocess_node_TYPE
  $function = __FUNCTION__ . '_' . $vars['node']->type;
  if (function_exists($function)) {
    $function($vars);
  }

  if ($vars['node']->field_ingress_bilde != '') {
    $vars['image'] = node_load($vars['node']->field_ingress_bilde[0]['nid']);
    $vars['image_author'] = '';
    if (is_array($vars['image']->field_copyright)) {
      foreach ($vars['image']->field_copyright as $author_id) {
        $author = node_load($author_id);
        $vars['image_author'] .= ( $vars['image_author'] != '' ? ', ' : '') . $author->title;
      }
    }
  }
  else if ($vars['teaser']) {
    if ($vars['node']->type == 'image') {
      $vars['image'] = $vars['node'];
    }
    else if ($vars['node']->iid) {
      $vars['image'] = node_load($vars['node']->iid);
    }
  }
  $vars['links'] = !empty($vars['node']->links) ? theme('links', $vars['node']->links, array('class' => 'links inline')) : '';

  if ($vars['node']->type == 'fil' && count($vars['node']->iids) && module_exists('contentbrowser')) {
    $image = contentbrowser_get_thumbnail($vars['node']->iids[0], 'fil', TRUE);
    if (!empty($image)) {
      global $base_url;
      $vars['picture'] = l("<img src='" . $base_url . '/' . imagecache_create_path('Liten', $image) . "' />", "node/" . $vars['node']->iids[0], array('html' => TRUE));
    }
  }

  //Set up ingress image.
  $vars['ingress_image'] = '';
  if ($vars['page'] && !empty($vars['image']->images['normal'])) {
    $preset = 'mobile_fag_preset';
    $vars['ingress_image'] = "<div class=\"ingressImg\">";
    $vars['ingress_image'] .= theme('imagecache', $preset, $vars['image']->images['_original'], $vars['image']->title, '', array("style" => "float:left; padding: 0 0.5em;"));
    $vars['ingress_image'] .= "</div>";
  }
  else if ($vars['teaser'] && isset($vars['image']->images[IMAGE_THUMBNAIL])) {
    $vars['ingress_image'] = '<img src="' . base_path() . $vars['image']->images[IMAGE_THUMBNAIL] . '" style="float:left; padding: 0 0.5em;" alt="" />';
  }

}

function ndla_mobile_preprocess_node_fag(&$vars) {
  if(module_exists('ndla_utils')) {
    global $base_path;
    $vars['base_path'] = $base_path;
    ndla_utils_preprocess_course_page($vars);
    unset($vars['css']['all']['theme'][$vars['directory'] . '/css/course-page.css']);
    $vars['content'] = theme('ndla_mobile_course_content', $vars);
  }
}

function ndla_mobile_preprocess_front(&$vars) {
  $courses_tmp = ndla_utils_get_courses_for_frontpage();
  $courses = '';
  $vars['title'] = NULL;
  $vars['courses'] = theme('fieldset', array(
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#value' => theme('ndla_mobile_courses', $courses_tmp[0]->children),
    '#title' => $courses_tmp[0]->name,
    '#attributes' => array(
      'class' => 'subject_dropdown first',
    ),
  ));
  
  $vars['courses'] .= theme('fieldset', array(
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#value' => theme('ndla_mobile_courses', $courses_tmp[2]->children),
    '#title' => $courses_tmp[2]->name,
    '#attributes' => array(
      'class' => 'subject_dropdown second',
    ),
  ));
  
  $vars['courses'] .= theme('fieldset', array(
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#value' => theme('ndla_mobile_courses', $courses_tmp[1]->children),
    '#title' => $courses_tmp[1]->name,
    '#attributes' => array(
      'class' => 'subject_dropdown third',
    ),
  ));

  // Applications
  $applications = array(
    (object) variable_get('ndla_utils_ndla2010_theme_front_page_application_0', array(
      'title' => '',
      'description' => '',
      'image' => '',
      'href' => ''
      )
    ),
    (object) variable_get('ndla_utils_ndla2010_theme_front_page_application_1', array(
      'title' => '',
      'description' => '',
      'image' => '',
      'href' => ''
      )
    ),
    (object) variable_get('ndla_utils_ndla2010_theme_front_page_application_2', array(
      'title' => '',
      'description' => '',
      'image' => '',
      'href' => ''
      )
    ),
    (object) variable_get('ndla_utils_ndla2010_theme_front_page_application_3', array(
      'title' => '',
      'description' => '',
      'image' => '',
      'href' => ''
      )
    ),
    (object) variable_get('ndla_utils_ndla2010_theme_front_page_application_4', array(
      'title' => '',
      'description' => '',
      'image' => '',
      'href' => ''
      )
    ),
    (object) variable_get('ndla_utils_ndla2010_theme_front_page_application_5', array(
      'title' => '',
      'description' => '',
      'image' => '',
      'href' => ''
      )
    ),
    (object) variable_get('ndla_utils_ndla2010_theme_front_page_application_6', array(
      'title' => '',
      'description' => '',
      'image' => '',
      'href' => ''
      )
    ),
    (object) variable_get('ndla_utils_ndla2010_theme_front_page_application_7', array(
      'title' => '',
      'description' => '',
      'image' => '',
      'href' => ''
      )
    ),
    (object) variable_get('ndla_utils_ndla2010_theme_front_page_application_8', array(
      'title' => '',
      'description' => '',
      'image' => '',
      'href' => ''
      )
    ),
  );

  for ($i = 0; $i < 3; $i++) {
    if (!isset($applications[$i]->title)) {
      $applications[$i]->title = $applications[$i]->{$vars['language']->language . '_title'};
    }
    if (!isset($applications[$i]->description)) {
      $applications[$i]->description = $applications[$i]->{$vars['language']->language . '_description'};
    }
    $applications[$i]->href = url($applications[$i]->href);
  }

  $vars['applications'] = theme('ndla_mobile_applications', $applications);
 
  /*
  // Tweets
  if(module_exists('ndla_tweet')) {
    $vars['tweets'] = _ndla_tweet_get_tweets('#feeds .twitter ul');
  } elseif(module_exists('ndla_twitter')) {
    $vars['tweets'] = _ndla_twitter_get_tweets();
  }


  $vars['tweets'] = theme('fieldset', array(
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#value' => $vars['tweets'],//theme('ndla_mobile_courses', $courses_tmp[0]->children),
    '#title' => t('Twitter'),
    '#attributes' => array(
      'class' => 'subject_dropdown twitter',
    ),
  ));
  */

  /*
  // New content
  foreach (array('first', 'second') as $weight) {
    $new_content[] = ndla_utils_load_course_content(variable_get('ndla_utils_ndla2010_theme_new_content_' . $weight, ''));
  }
  
  if (isset($new_content)) {
    $vars['new_content'] = theme('ndla_utils_course_new_content', $new_content, $vars['base_path'], $vars['directory'], $vars['language']->language);
  }

  $vars['new_content'] = theme('fieldset', array(
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#value' => $vars['new_content'],
    '#title' => t('We recommend'),
    '#attributes' => array(
      'class' => 'subject_dropdown new-content',
    ),
  ));
  */
  // Facebook
  
  /*
  $vars['facebook'] = theme('fieldset', array(
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#value' => ndla_utils_get_fb_html('front'),
    '#title' => t('Facebook'),
    '#attributes' => array(
      'class' => 'subject_dropdown facebook',
    ),
  ));
  */

  $vars['content'] = theme('ndla_mobile_page_front', $vars);
}  
/**
 * Implementation of hook_preprocess_search_results().
 */
function ndla_mobile_preprocess_search_results(&$vars) {
  if (module_exists('apachesolr')) {
    $response = apachesolr_static_response_cache();
  }
  if (isset($response) && !empty($response)) {
    // Get facet fields
    $vars['hits'] = $response->response->numFound;
  }
  else {
    $vars['hits'] = 0;
  }
}