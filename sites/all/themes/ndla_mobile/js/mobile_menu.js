
(function($){
  
	$(function() {
		var selector = 'body';
		$('.menu-page').addClass('enable-transition');

    var point = [];

    if(Modernizr.touch) {
      $(selector).on('touchstart', '.active-menu-page .link', touch_point_saver);
      $(selector).on('touchend', '.active-menu-page .link', function(ev) { scroll_blocker.call(this,ev,event_link) });

      $(selector).on('touchstart', '.active-menu-page .back', touch_point_saver);
      $(selector).on('touchend', '.active-menu-page .back', function(ev) { scroll_blocker.call(this,ev,event_back) });
    } else {
      $(selector).on('click', '.active-menu-page .link', event_link);
      $(selector).on('click', '.active-menu-page .back', event_back);
   }

   function touch_point_saver(ev) {
    var x = ev.originalEvent.touches[0].pageX,
        y = ev.originalEvent.touches[0].pageY;

    point = {X:x,Y:y};
  }

  function scroll_blocker(ev, func) {
    if(ev.originalEvent.changedTouches[0].pageY != point.Y) {
      return;
    }

    func.call(this,ev);
  }

  function event_link(ev) {
    if(!$(ev.target).is('a')) {
      ev.preventDefault();
    } else {
      $(ev.target).css('color', '#366A9A');
    }

    if($('.next-menu-page').last().length == 0) {
      return;
    }

    var $parent = $(this).parents('.active-menu-page');
    var $next = $('.next-menu-page').first();
    var $next_menu = $next.children('#parent-node-'+this.id);


    $next_menu.find('.back span')
      .each(function(_,d) {
        var p = $('#'+$(d).data('parent-id')).data('name');
        $(d).html(p);
      });

    if($next_menu.length == 0) {
      return;
    }

    $next_menu.show();
    $next_menu.siblings().hide();
    $parent.removeClass('active-menu-page').addClass('prev-menu-page');
    $next.removeClass('next-menu-page');
    $next.addClass('active-menu-page');
    resize_menu();
  }

function event_back(event) {
  if($('.prev-menu-page').last().length == 0)
    return;

  $parent = $(this).parents('.active-menu-page');
  $prev = $('.prev-menu-page').last();
  $prev.removeClass('prev-menu-page').addClass('active-menu-page');
  $parent.removeClass('active-menu-page').addClass('next-menu-page');
  resize_menu();
}

resize_menu();
});

resize_menu = function() {
 var height = $('.active-menu-page').height();
 $('#menucontainer').css('height', height + 'px')
}

})(jq17)
