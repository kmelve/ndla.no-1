/*!
 * Various hacks needed to make jquery in drupal work
 */
/*global jQuery*/
(function ($) {
  $.extend(jQuery.fn, {
    /**
     * Absolutely lame substitute for jquery.closest()
     * TODO: implement me properly!
     */
    closest: function( selector ) {
      var matchPOS = /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/;
       var pos = matchPOS.test( selector ) ? jQuery(selector) : null,
         closer = 0;

       return this.map(function(){
         var cur = this;
         while ( cur && cur.ownerDocument ) {
           if ( pos ? pos.index(cur) > -1 : jQuery(cur).is(selector) ) {
             jQuery.data(cur, "closest", closer);
             return cur;
           }
           cur = cur.parentNode;
           closer++;
         }
       });
     }
  });
})(jQuery);
