Drupal.behaviors.tabbed_content = function() {
  $('ul.tabs li a').bind('click', function() {
    var div_parent = $(this).parents('div').first();
    var div_to_show = $(this).attr('class');
    $('ul.tabs li', div_parent).removeClass('active');
    $(this).parents('li').first().addClass('active');
    $("div.active", div_parent).removeClass('active');
    $('div.' + div_to_show).addClass('active');
  });
}