/**
 * @file
 * @ingroup ndla2010
 */

$(document).ready(function () {

  // Allow full screen on HTML videos when inserted in a lightbox
  $(document).bind('DOMNodeInserted', function(e) {
    $("#lightboxFrame").attr({
      allowfullscreen: "true",
      webkitallowfullscreen: "true",
      mozallowfullscreen: "true"
    });
  });

  $(document).ajaxComplete(function(e, r, s) {
    if (typeof MathJax == 'undefined' && typeof r.responseText != 'undefined') {
      
      if (r.responseText.replace(/\\\"/g, '"').indexOf('class="Wirisformula"') != -1) {
        $.getScript("http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML");
      }
    }
  });

  if($('.utdanning_rdf_fieldset li').length) {
    $('.utdanning_rdf_fieldset li:even').addClass('even');
    $('.utdanning_rdf_fieldset li:odd').addClass('odd');
  }

  // Make sure the document doesn't get ready more than once.
  if (window['ndla2010'] != undefined) {
    return;
  }
  ndla2010 = true;

  // Replace lightbox2's useless caption span.
  $('#caption').replaceWith('<div id="caption"></div>');
  $('#bottomNav').insertBefore('#imageDetails');

  // Add ITSL
  if ($.cookie('itslearningembed') && Drupal.settings.ndla_utils.nid) {
    $('<a href="' + Drupal.settings.basePath + 'sites/all/libraries/itslearning/itslearning.php?nodeid=' + Drupal.settings.ndla_utils.nid + '" id="itslearningembed"></a>').appendTo('body');
  }

  // Load theme settings.
  new JStorage('theme', function (storage) {
    storage.startCollecting();

    // Prepare tray system.
    new NdlaTabs(storage);

    // Prepare left menu.
    new NdlaLeft(storage);

    // Adjust page size.
    new NdlaResize(storage);

    // Prepare right sidebar.
    new NdlaRight(storage);

    // Footer
    new NdlaFooter();

    storage.stopCollecting();
  });
});

function NdlaResize(themeStorage) {
  var that = this,
  $window = $(window),
  $container = $('#container'),
  $leftMenuTabs = $('#left-menu-tabs', $container);

  if (!$leftMenuTabs.length) {
    $container.addClass('w960');
    return; // No resizing needed.
  }

  $window.resize(function () {
    var width = $window.width(),
    has_w960 = $container.hasClass('w960');
    if (width < 1260) {
      if (!has_w960) {
        $container.addClass('w960');

        // Close left menu
        $leftMenuTabs.children('.left-menu-tab-selected').click();
      }
    }
    else if (has_w960 && !themeStorage.get('leftMenuHidden')) {
      $container.removeClass('w960');

      // Open left menu
      $leftMenuTabs.children('li:last:not(.left-menu-tab-selected)').click();
    }
  }).resize();
}

function NdlaRight(themeStorage) {
  var that = this;

  this.$wrapper = $('#right-wrapper');

  // Hide empty blocks in right sidebar.
  $('.empty-block', this.$wrapper).each(function (index, element) {
    if ($(this).parent().parent().attr('id') != 'block-ndla_deling_oppgave_deling_oppgave_list') $(this).parent().parent().hide();
  });


  this.$tabs = $('#right-tabs', this.$wrapper);

  $('.related', this.$tabs).click(function () {
    that.show($('.region-right', this.$wrapper), $(this).parent());
    // Executed as a virtual click, dont save default!
    if(arguments[0].originalEvent !== undefined) {
      themeStorage.set('rightTab', 'related');
    }
  });
  $('.tools', this.$tabs).click(function () {
    that.show($('.region-right-tools', this.$wrapper), $(this).parent());
    // Executed as a virtual click, dont save default!
    if(arguments[0].originalEvent !== undefined) {
      themeStorage.set('rightTab', 'tools');
    }
  });

  var $active = $('.' + themeStorage.get('rightTab'), this.$tabs);

  if (Drupal.settings.ndla_utils.site == 'cm') {
    $('<div class="region region-learning"></div>').appendTo(this.$wrapper);
    $('<li><a class="js-link learning" id="learning-title" href="#learning">' + Drupal.t('My learning') + '</a></li>').appendTo(this.$tabs).children('a').click(function () {
      that.show($('.region-learning', that.$wrapper), $(this).parent());
      // Executed as a virtual click, dont save default!
      if(arguments[0].originalEvent !== undefined) {
        themeStorage.set('rightTab', 'learning');
      }
    });

    ndla_auth(function(auth) {
      if (auth.is_authenticated()) {
        var title = auth.is_teacher() ? Drupal.t('My teaching') : Drupal.t('My learning');
        $('#learning-title').html(title);
      }
    });

    if(themeStorage.isLoggedIn()) {
      $('<div class="region region-stickers"><div class="block"></div></div>').appendTo(this.$wrapper);
      $('<li class="li-stickers"><a class="js-link stickers" href="#stickers">' + Drupal.t('Notes') + '</a></li>').appendTo(this.$tabs).children('a').click(function () {
        that.show($('.region-stickers', that.$wrapper), $(this).parent());
        // Executed as a virtual click, dont save default!
        if(arguments[0].originalEvent !== undefined) {
          themeStorage.set('rightTab', 'stickers');
        }
      });
    }

    if (!$active.length) {
      ndla_auth(function(auth) {
        if (auth.is_authenticated()) {
          $active = $('.learning', this.$tabs);
        }
      });
    }
  }

  if (!$active.length) {
    $active = $('a:first', this.$tabs);
    if(Drupal.settings.right_default_tab != undefined) {
      $active = $('.' + Drupal.settings.right_default_tab, this.$tabs);
    }
  }
  $active.click();

  $('.right-tools-button.open', this.$wrapper).click(function () {
    var $this = $(this);
    if ($this.hasClass('open')) {
      $this.removeClass('open').children('ul').hide();
    }
    else {
      $this.addClass('open').children('ul').show();
    }
  }).removeClass('open').children('ul').hide();

  //// Shorten lists
  //$('#block-utdanning_rdf_utdanning_rdf ul').each(function () {
  //  var $excess = $('li:gt(4)', this);
  //  if ($excess.length) {
  //    $('<a href="#" class="less"></div>').insertAfter(this).click(function (e) {
  //      var $this = $(this);
  //      if ($this.hasClass('more')) {
  //        $this.removeClass('more').text(Drupal.t('Show less'));
  //        $excess.show();
  //      }
  //      else {
  //        $this.addClass('more').text(Drupal.t('Show more'));
  //        $excess.hide();
  //      }
  //      e.preventDefault();
  //    }).click();
  //  }
  //});
}



NdlaRight.prototype.show = function ($region, $tab) {
  $('.region:not(.region-right-red)', this.$wrapper).hide();
  $region.show();
  $('.current', this.$tabs).removeClass('current');
  $tab.addClass('current');
}

function NdlaTabs(themeStorage) {
  var that = this,
  $leftMenuTab = $('.left-menu-tab');
  this.themeStorage = themeStorage;

  $leftMenuTab.click(function () {
    that.openMenu(this.id.split('-', 4)[2]);
  });
  $('.tray-tab').click(function () {
    that.openTray(this.id);
  });
  $('#close-tray').click(function () {
    $('#tray-tabs .active, #grep.active').click();
  });
  $('.tray-tab.click').click();

  if (themeStorage.get('leftMenuHidden')) {
    $leftMenuTab.filter('.left-menu-tab-selected').click();
  }
}

NdlaTabs.prototype.openMenu = function (type) {
  var $container = $('#container'),
  $menuTab = $('#left-menu-' + type + '-tab', $container),
  is_open = $menuTab.hasClass('left-menu-tab-selected'),
  is_w960 = $container.hasClass('w960');

  $('.left-menu-tab-selected').removeClass('left-menu-tab-selected');
  $('.left-menu-content-selected').removeClass('left-menu-content-selected');

  if (is_open) {
    $('#left-menu').hide();
    if (!is_w960) {
      this.themeStorage.set('leftMenuHidden', 1);
      $container.addClass('w960');
    }
  }
  else {
    if (is_w960) {
      if ($(window).width() > 1260) {
        $container.removeClass('w960');
      }
      else {
        // Hide trays.
        $('.tray-tab.active').click();
      }
    }
    this.themeStorage.set('leftMenuHidden', 0);
    $menuTab.addClass('left-menu-tab-selected');
    $('#left-menu-' + type + '-content').addClass('left-menu-content-selected');
    $('#left-menu').show();
  }
}

NdlaTabs.prototype.openTray = function (type) {
  var that = this,
  $trayTab = $('#' + type),
  was_open = $trayTab.hasClass('active'),
  $trayInner = $('#tray-inner');
  $trayInner.css('height', $trayInner.height() + 'px'); // Make sure we have a static hight

  $('.tray-tab.active').removeClass('active');
  $trayInner.children('div').hide();

  if ($('#container').hasClass('w960')) {
    $('.left-menu-tab-selected').click(); // Hide any open left menus in 960 mode.
  }

  if (Drupal.settings.context_id == undefined && this.$fader == undefined) {
    // Fade away page content for non front pages.
    var $mainContent = $('#main_content');
    this.$fader = $('<div id="main-content-fader" class="active"></div>').appendTo($mainContent).hide().height($mainContent.height()).click(function () {
      $('#tray-tabs .active').click();
    });
  }

  if (was_open) {
    var $trayInner = $('#tray-inner');
    $trayInner.stop().animate({height: 0}, 400);
    $('#tray').css('zIndex', '-1').stop().animate({height: 0}, 400, function () {
      $(this).hide();
    });
    if (this.$fader != undefined) {
      this.$fader.stop().fadeOut(400);
    }
  }
  else {
    if (this.$fader != undefined) {
      this.$fader.stop().fadeIn(400);
    }
    $trayTab.addClass('active');
    var $content = $('#' + type + '-tray');
    if ($content.length) {
      this.resizeTray($content);
    }
    else {
      // Load tray
      $content = $('<div id="' + type + '-tray"><div class="tray-loader"><img src="' + Drupal.settings.ndla_utils.theme_url + '/img/throbber.gif" alt="' + Drupal.t('Loading') + '"/></div></div>').hide().appendTo('#tray-inner');
      this.resizeTray($content);
      var url = Drupal.settings.ndlaMenu.url;
      switch (type) {
        case 'topics':
          var fag = Drupal.settings.ndla_fag_tabs.fagNid;
          url += '/ndla-menu-display/topics/' + fag;
          break;

        case 'apps':
          url += '/ndla_apps/list/ajax/0/' + (window['SERIA_VARS'] != undefined && SERIA_VARS.IS_LOGGED_IN ? '1' : '0') + '?' + Drupal.settings.ndlaMenu.query;
          break;

        case 'grep':
          url += '/ndla_grep/ajax/tray/' + Drupal.settings.ndla_fag_tabs.fagNid;
          break;

        default:
          url += '404';
      }

      // Exclude the topics menu from this case statement. 
      // And call a seperate function to initalize the whole menu. 
      // Try to keep this code smaller and cleaner .
      if(type == 'topics') {
        NdlaTopicsMenuDisplay(Drupal.settings.ndlaMenu, this, $content);
      } else {
        $.get(url, function (data) {
          var isCurrentTray = $content.is(':visible');
          $content.hide().html(data);

          if (isCurrentTray) {
            that.resizeTray($content);
          }

          switch (type) {
            case 'grep':
            case 'apps':
              Drupal.attachBehaviors($('#' + type + '-tray'));
          }
        });

      }

    }
  }
}

NdlaTabs.prototype.resizeTray = function ($content) {
  var $tray = $('#tray').show(),
  $trayInner = $('#tray-inner'),
  currentHeight = $trayInner.height();
  $content.show();
  var targetHeight = $trayInner.css('height', 'auto').height();
  if ($tray.css('zIndex') < 0) {
    $tray.stop().animate({height: '30px'}, 50, function () {
      $tray.css({zIndex: 0, height: 'auto'});
    });
  }
  $trayInner.css('height', currentHeight).stop().animate({height: targetHeight}, 200, function () {
    $trayInner.css('height', 'auto');
  });
}

function NdlaFooter() {
  var that = this;

  this.$container = $('#footer-container');
  this.$content = $('#footer-container .footer-content');
  this.$footer = $('#footer');

  this.doScroll = true;
  // Add listener to expandable first level footer-elements
  $('#footer-container .footer-tab').click(function () {
    that.openTab($(this));
    return false;
  });

  var $footerTop = $('#footer-top').click(function () {
    that.open();
  });

  if (Drupal.settings.ndla2010 && Drupal.settings.ndla2010.openFooter) {
    that.doScroll = false;
    $footerTop.click();
  }
}

NdlaFooter.prototype.open = function () {
  var that = this;

  if (this.$container.hasClass('footer-collapsed')) {
    this.$content.show();
    $('#footer-left li:first', this.$container).trigger('click');
    this.$footer.parent().height(this.$footer.height());
    this.$container.removeClass('footer-collapsed').addClass('footer-expanded');
    $('body, html').animate({scrollTop: '+=' + (parseInt(this.$content.height()) + 15) + 'px'}, 400);
  }
  else {
    $('body, html').animate({scrollTop: '-=' + parseInt(this.$content.height()) + 'px'}, 400, function () {
      that.$content.hide();
      that.$container.removeClass('footer-expanded').addClass('footer-collapsed');
      that.$footer.parent().height(that.$footer.height());
    });
  }
}

NdlaFooter.prototype.openTab = function ($tab) {
  var that = this,
      loaded = $tab.hasClass('loaded');

  $('.footer-block-selected', this.$content).removeClass('footer-block-selected');
  $('#' + $tab.attr('id') + '-content', this.$content).addClass('footer-block-selected');

  // Load block if it hasn't been loaded
  if (!loaded) {
    url = Drupal.settings.basePath + Drupal.settings.ndla_utils.language +'/ndla_utils_disp/load_block/' + $tab.attr('id').split('-', 2)[1] + '/' + Drupal.settings.ndla_utils.nid + window.location.search;
    $.get(url, function (data) {
      $tab.addClass('loaded');

      data = Drupal.parseJson(data);
      $('#' + $tab.attr('id') + '-content').html(data.data);
      that.$footer.parent().height(that.$footer.height());
      if (that.doScroll) {
        that.scroll();
      }
      else {
        that.doScroll = true;
      }
    });
  }

  // Indicate that it's active
  $('.footer-tab-selected', this.$content).removeClass('footer-tab-selected');
  $tab.addClass('footer-tab-selected');

  this.showInner();

  if (loaded) {
    if (this.doScroll) {
      this.scroll();
    }
    else {
      this.doScroll = true;
    }
  }
}

NdlaFooter.prototype.showInner = function () {
  $('#inner', this.$content).show().css({height: 'auto'});
}

NdlaFooter.prototype.hideInner = function () {
  $('#inner', this.$content).hide('slide', {direction: 'left'}, 200);
}

NdlaFooter.prototype.scroll = function () {
  // Scroll down
  var $BH = $('body, html'),
  footerPos = $('.footer-content').offset().top,
  toScroll = footerPos - $BH.attr('scrollTop') - 40;
  if (toScroll > 0) {
    $("body, html").animate({scrollTop: '+=' + toScroll + 'px'}, 400);
  }
}

function NdlaLeft(storage) {
  var that = this,
  $leftMenu = $('#left-menu-menu-content .menu'),
  $topicsMenu = $('#left-menu-topics-content .menu');

  if (!$leftMenu.length && !$topicsMenu.length) {
    return;
  }

  this.pageFlipper = {current: 0, total: 0};
  this.pathify = new RegExp('(^' + Drupal.settings.basePath.replace(/\//g, '\\/') + '([a-z]{2}\\/)?|\\?.*$)', 'g');

  if ($leftMenu.length) {
    var $menuLis = $leftMenu.children('li').each(function () {
      that.treeify(this);
    });
    $menuLis = $leftMenu.find('li');
    this.filters($menuLis, $leftMenu.prev().prev().children('li'), storage);
  }

  if ($topicsMenu.length) {
    var oldPageFlipper = this.pageFlipper;
    this.pageFlipper = {current: 0, total: 0};
    $menuLis = $topicsMenu.children('li').each(function () {
      that.treeify(this);
    });
    if (!this.pageFlipper.current) {
      this.pageFlipper = oldPageFlipper;
    }
    this.filters($topicsMenu.find('li'), $topicsMenu.prev().prev().children('li'), storage);
  }

  if (this.pageFlipper.current) {
    var topFlipper = '<span>';
    var limit = 33;
    if (this.pageFlipper.previous != undefined) {
      topFlipper += '<a href="' + this.pageFlipper.previous.attr('href') + '" class="bla-previous" title="' + this.pageFlipper.previous.text() + '"><i class="fa fa-angle-left"></i></a>';
      var prev_text = this.pageFlipper.previous.text();
      if(prev_text.length >= limit) {
        prev_text = prev_text.substr(0, limit) + "...";
      }
    }
    topFlipper += "<span class='counter_flip'>" + this.pageFlipper.current + ' / ' + this.pageFlipper.total + "</span>";
    if (this.pageFlipper.next != undefined) {
      topFlipper += '<a href="' + this.pageFlipper.next.attr('href') + '" class="bla-next" title="' + this.pageFlipper.next.text() + '"><i class="fa fa-angle-right"></i></a>';
      var next_text = this.pageFlipper.next.text();
      if(next_text.length >= limit) {
        next_text = next_text.substr(0, limit) + "...";
      }
    }
    $('#top-bla-menu').html(topFlipper + '</span>');

    var bottomFlipper = '';
    
    if (this.pageFlipper.previous != undefined) {
      bottomFlipper += '<a href="' + this.pageFlipper.previous.attr('href') + '" class="prev"><i class="fa fa-angle-left"></i>' + prev_text + '</a>';
    }
    bottomFlipper += '<div class="mmenu-nav-traverse-inactive current">' + this.pageFlipper.current + " " + Drupal.t("of") + " " + this.pageFlipper.total + '</div>';
    if (this.pageFlipper.next != undefined) {
      bottomFlipper += '<a href="' + this.pageFlipper.next.attr('href') + '" class="next">' + next_text + '<i class="fa fa-angle-right"></i></a>';
    }
    $('#block-ndla_menu_0').show().children('.content').children('.articleNav').children('.articlewrap1').prepend(bottomFlipper);
  }
  else {
    $('#top-bla-menu').remove();
  }

  tid = storage.get('menu-filter-' + Drupal.settings.ndla_utils.left_menu);
  if(isFinite(tid)) {
    $('li.filter-' + tid).click();
  }
}

NdlaLeft.prototype.treeify = function (li) {
  var that = this,
  $li = $(li),
  $ul = $li.children('ul'),
  $a = $li.children('.item').children('.name').children('a');

  if ($a.length) {
    this.pageFlipper.total++;
    if (this.pageFlipper.current && this.pageFlipper.next == undefined) {
      this.pageFlipper.next = $a;
    }
    var href = $a.attr('href').replace(this.pathify, '');
    if (href == Drupal.settings.ndlaMenu.path || href == Drupal.settings.ndlaMenu.alias) {
      $li.addClass('open current').children('ul').attr('aria-hidden', false).prev().children('a.arrow').attr('aria-expanded', true);
      this.pageFlipper.current = this.pageFlipper.total;
    }
    if (!this.pageFlipper.current) {
      this.pageFlipper.previous = $a;
    }
  }
  if ($ul.length) {
    var $lis = $ul.children('li').each(function () {
      that.treeify(this);
    }) ;

    if ($lis.filter('.open').length) {
      $li.addClass('open').children('ul').attr('aria-hidden', false).prev().children('a.arrow').attr('aria-expanded', true);
    }

    $li.click(function (e) {
      var $this = $(this);

      if ($this.hasClass('open')) {
        that.closeItem($this);
      }
      else {
        that.openItem($this);
      }

      e.stopPropagation();
    });
  }
  $li.children('.item').children('a').click(function () {
    $(this).parent().parent().click();
    return false;
  }).end().children('.name').children('a').click(function (e) {
    e.stopPropagation();
  });
}

NdlaLeft.prototype.openItem = function ($li) {
  var that = this,
      $ul = $li.children('ul');

  // Close others that are open.
  $li.parent().children('.open').each(function () {
    that.closeItem($(this));
  });

  $ul.slideDown(250, function() {
    $li.addClass('open');
    $ul.attr('aria-hidden', false);
  });
}

NdlaLeft.prototype.closeItem = function ($li) {
  var $ul = $li.children('ul');

  $ul.slideUp(250, function() {
    $li.removeClass('open');
  });
  $ul.attr('aria-hidden', true);
}

NdlaLeft.prototype.filters = function ($menuLis, $filterLis, storage) {
  $filterLis.click(function () {
    if ($('#left-menu .export').length) {
      // Cancel export
      $('#left-menu .cancel-button').click();
    }

    var $this = $(this),
    /* Split on ' ' since class can contain bold */
    tid = $this.attr('class').split('-', 2)[1].split(' ')[0];

    if(isFinite(tid)) {
      storage.set('menu-filter-' + Drupal.settings.ndla_utils.left_menu, tid);
    } else {
      storage.del('menu-filter-' + Drupal.settings.ndla_utils.left_menu);
    }

    if (tid == 'all') {
      $menuLis.show();
    }
    else {
      $menuLis.hide().filter('.tid-' + tid).show();
    }
    $($menuLis).each(function(unused_key,item) {
      if( ! $(item).attr('class').match("tid") ) {
        $(item).show();
      }
    });
    $filterLis.removeClass('bold');
    $this.addClass('bold');
    $('#left-menu .top input').keyup();
  }).eq(0).addClass('bold');
}

function loadEmbedCode(theLink) {
  if($('.embed-wrapper').length == 0) {
    $(theLink).after("<div class='embed-wrapper'><img src='" + Drupal.settings.ndla_utils.theme_url + "/img/throbber.gif' /></div>");
    $.get(Drupal.settings.basePath + 'ndla_utils_disp/load_block/embed_code/' + Drupal.settings.ndla_utils.nid + window.location.search, function (data) {
      data = Drupal.parseJson(data);
      data.data += "<div style='float: right'><a href='javascript:void(0)' onclick='$(\".embed-wrapper\").fadeOut();'>" + Drupal.t('Close') + "</a></div>";
      $('.embed-wrapper').html(data.data);
      $('.embed-wrapper').html()
    });
  }
  else {
    $('.embed-wrapper').show();
  }
}

// If newer version of jQuery is present override Drupal.parseJson,
// since newer versions of jQuery automatically parses JSON in many situations
// where Drupal.parseJson was needed.
if ($.parseJSON) {
  Drupal.parseJson = function(data) {
    // Only parse data if incoming data is string
    if (typeof data === "string") {
      data = $.parseJSON(data);
    }

    return data;
  }
}

Drupal.behaviors.user_dropdown = function(context) {
  if(typeof ndla_auth == 'undefined' || $('#user_dropdown_toggler').length == 0) {
    return;
  }

  /**
   * The behaviour is called twice. Once with
   * document as context and once with body
   * as context.
   */
  if(context == document) {
    $(document).click(function(event) {
      if(event.hasOwnProperty('originalEvent')) { //Check if this is a real click.
        if ($(event.target).closest('.right-box').get(0) == null && $('.right-box .dropdown:visible').length > 0) {
          $('#user_dropdown_toggler').click();
        }
      }
    });
  }

  var orig_width = 0;
  var full_width = 500;

  var clickHandler = function() {
    if (orig_width == 0) {
      // Set orig_width the first time the event is fired
      orig_width = $('.right-box .login').width();
    }

    // Slide to shink/expand to the left
    if($('.right-box .dropdown:visible').length == 0) {
      $('.right-box .login').animate({width: full_width + 'px'}, 100).css('overflow', 'visible');
    }
    else {
      $('.right-box .login').animate({width: (orig_width + 1) + 'px'}, 100).css('overflow', 'visible');
    }

    // Slide up/down
    $('.right-box .dropdown').slideToggle(100, function() {
      if($('.right-box .dropdown:visible').length == 1) {
        $('#user_dropdown_toggler i.toggler').attr('class', 'toggler icon-caret-up');
      }
      else {
        $('#user_dropdown_toggler i.toggler').attr('class', 'toggler icon-caret-down');
      }
    });
  };

  ndla_auth(function(auth) {
    //Set the correct login link
    $('#seria_login_link').attr('href', auth.login_url());
    $('#seria_logout_link').attr('href', auth.logout_url(window.location.href));
    $('#seria_settings_link').attr('href', auth.user_settings_url());
    //User is logged in
    if(auth.authenticated()) {
      $('.logged_in').fadeIn();
      $('#user_dropdown_toggler .seria_user_name').html(auth.display_name());
      var profile_pic = auth.profile_picture(45,45);
      if(profile_pic) {
        $('#user_dropdown_toggler').addClass('has_profile');
        $('#user_dropdown_toggler i.icon-user').remove();
        $('#user_dropdown_toggler').prepend('<img class="profile-image" src="' + profile_pic + '" />');
      }

    // Make sure click is not registered several times.
    $('#user_dropdown_toggler').unbind('click');
    $('#notification_bubble').unbind('click');
    $('#user_dropdown_toggler').click(clickHandler);
    $('#notification_bubble').click(clickHandler);
    $('#notification_bubble').css('cursor', 'pointer');
    }
    else {
      $('.not_logged_in').fadeIn();
    }

    //Link to min.ndla.no
    if(Drupal.settings.ndla_msclient_services_url) {
      $('#myfav_link').attr('href', Drupal.settings.ndla_msclient_services_url);
    }
    else {
      $('#myfav_link').remove();
    }
  });
}



function animate_tray_height($tray, height) {
  $tray.stop();
  $tray.animate({
    height : height
  }, 200);
}

try {
  QuizTY.activateNavigation = function() {
    $('#next-question' + QuizTY.notProcessed).click(QuizTY.nextQuestion).addClass(QuizTY.processed);
    $('#previous-question' + QuizTY.notProcessed).click(QuizTY.prevQuestion).addClass(QuizTY.processed);
    $('#mark-questions' + QuizTY.notProcessed).click(QuizTY.markQuestions).addClass(QuizTY.processed);
    if (typeof MathJax != 'undefined') MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
  };
}
catch(e) {
  ;
}

Drupal.behaviors.feeds_eq_height = function() {
  if($('#ndla_feeds h2').length) {
    height = 0;
    $('#ndla_feeds h2').each(function() {
      if($(this).height() > height) {
        height = $(this).height();
      }
    });
  
    $('#ndla_feeds h2 a').height(height);
  }
}

Drupal.behaviors.tooltip_qtip = function() {

  $('.tooltip').each(function() {
    $(this).qtip({
      style: { 
        classes: 'qtip-youtube qtip-rounded qtip-shadow'
      },
      position: {
          my: 'bottom left',
          at: 'top center',
      },
      hide: {
        fixed: true,
        delay: 300,
      },
      content: {
        button: false,
        title: $(this).attr('title'),
        text: $(this).next('.tooltip-data') // Use the "div" element next to this for the content
      }
    });
  });
}

Drupal.behaviors.ndla_apps = function() {
  if(!$('#ndla-apps-prev').length || !$('#ndla-apps-block').length) {
    return;
  }

  var block_width = $('#ndla-apps-block').width(),
      step_size = block_width / 1.5;
      width = ($('.ndla-apps-block-background .ndla-app').length * 65);
      ndla_apps_enabled = true;
      $scroller = $( ".ndla-apps-block-background .scroller" );

  $scroller.width(width);

  var redraw_buttons = function() {
    if((width + $scroller.position().left) > block_width){
      $('#ndla-apps-next').show();
    } else {
      $('#ndla-apps-next').hide();
    }
    if($scroller.position().left > -10) {
      $('#ndla-apps-prev').hide();
    } else {
      $('#ndla-apps-prev').show();
    }
  }
  redraw_buttons();


  
  //redraw_buttons();
  $('#ndla-apps-prev').click(function(){
    if($scroller.position().left != '0' && ndla_apps_enabled) {
      ndla_apps_enabled = false;

      $scroller.animate({
        left: '+=' + step_size,
      }, 500, function() {
        ndla_apps_enabled = true;
        redraw_buttons();
      });
    }
  });
  $('#ndla-apps-next').click(function(){
    if((width + $scroller.position().left) > block_width && ndla_apps_enabled) {
      ndla_apps_enabled = false;
      $scroller.animate({
        left: '-=' + step_size,
      }, 500, function() {
        ndla_apps_enabled = true;
        redraw_buttons();
      });
    }
  });
}

Drupal.behaviors.ndla_language_switcher = function() {
  
  $('.ndla_language_switcher').each(function() {
    if (!$(this).data("processed") == "1") {
      $(this).data("processed", "1");
      i=0;
      var buttons = [];
      var $container = $(this);
      var salt = $container.attr('data-salt');
      $ul = $('<ul></ul>');
      $container.prepend($ul);
      $(this).find('> div').each(function() {
        ++i;
        $li = $('<li><a href="#lang' + i + '_' + salt + '">' + Drupal.settings.ndla_utils.languages[$(this).attr('lang')] + '</a></li>');
        $ul.append($li);
      });
      $('.ndla_language_switcher').tabs();
    }
  });
}
