subject_selector = function(loggedin) {
  new JStorage('theme', function (storage) {
    storage.startCollecting();
    subject_selector_render(storage);
    storage.stopCollecting();
  });
  
  $(document).click(function(event) {
    if(event.hasOwnProperty('originalEvent')) { //Check if this is a real click.
      if ($(event.target).closest('.coursebrowser').get(0) == null && $('#subjects:visible').length > 0 && !$(event.target).hasClass('coursebrowser_toggler')) {
        $(".coursebrowser").fadeOut('fast');
      }
    }
  });
  
  $(document).keyup(function(e) {
    //Escpae key triggered
    if (e.keyCode == 27) { 
      $('.coursebrowser').fadeOut('fast');
    }
  });
  
  $('.close-subject-selector').bind('click', function() {
    $('.coursebrowser').fadeOut('fast');
  });
}

function subject_selector_render(storage) {
  if(storage.isLoggedIn()) {
    var courses = userData.getData('global', 'subjects');
    if(courses) {
      for(var i = 0; i < courses.length; i++) {
        if(courses[i].value == 1) {
          $('.my_courses input:checkbox[value=' + courses[i].id + ']').attr('checked', 'checked');
        }
      }
    }
    subject_selector_hide_items();

    $('#select_courses_button').bind('click', function() {
      $('.my_courses .subject_link').toggle();
      $('.my_courses .subject_selector').toggle();
      subject_selector_hide_items(true);
      $(this).hide();
      $('#save_courses_button').show();
    });

    $('#save_courses_button').bind('click', function() {
      courses = [];
      $('.my_courses input').each(function() {
        var id = parseInt($(this).val());
        var value = $(this).is(':checked') ? 1 : 0;
        var title = $(this).parent().text();
        if(!isNaN(value)) {
          courses.push(
            {
              'id' : id,
              'title' : title,
              'value' : value,
              'guid' : ''
            }
          );
        }
      });
      userData.saveData('global', 'subjects', courses);

      $('#save_courses_button').hide();
      $('#select_courses_button').show();
      subject_selector_render(storage);
    });
  }
  else {
    link = 'http://auth.ndla.no';
    if($('#authndlaLoginLink').length > 0) {
      link = $('#authndlaLoginLink').attr('href');
    }
    $('div.my_courses').html('<div class="not-logged-in"><a href="' + link + '"><span></span>' + Drupal.t('You must log in to use this function.') + '</a></div>');
  }

}

function subject_selector_hide_items(show) {
  if(!show) {
    $('.my_courses .column a.subject_link').show();
    $('.my_courses .subject_selector').hide();
    //Hide all non-checked items
    $(".my_courses input:checkbox:not(:checked)").each(function() {
      $(this).parents('.course_item:eq(0)').hide();
    });
    
    $('.my_courses .subject_container').each(function() {
      if($('input:checkbox:checked', $(this)).length == 0) {
        $(this).hide();
      }
    });
  }
  //We want to show everything again
  else {
    $('.my_courses .subject_container').show();
    $('.my_courses .course_item').show();
    $('.my_courses .subject_selector').show();
    $('.my_courses .column a.subject_link').hide();
  }
}

if(typeof userData != 'undefined') {
  userData.ready(function (loggedIn) {
    subject_selector(loggedIn);
  });
} else {
  $(function() {
    $('.coursebrowser .tabs').remove();
  });
}
