Drupal.behaviors.eq_height_course = function() {
  var feeds = $('#ndla_feeds').height();
  var rec = $('.user-recommends').height();

  if(rec > feeds) {
    $('#ndla_feeds').height(rec);
  }
  else {
    $('.user-recommends').height(feeds);
  }
}