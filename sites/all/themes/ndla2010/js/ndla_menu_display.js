function NdlaTopicsMenuDisplay(SettingsObject, NdlaTabs, $content_container) {
  var id = Drupal.settings.ndla_fag_tabs.fagNid; // Calculate FAG id.
  var url = Drupal.settings.ndlaMenu.url + '/ndla-menu-display-json/topics/' + id; // Load the menu
  var menuObject = {}, currentMenu = {}, clickTracker = []; // Create empty menu object

  // Load the menu from server
  $.ajax(url, {
    'complete' : function(data) {
      $content_container.html(''); // Remover the spinner.
      menuObject = $.parseJSON(data.responseText); // Store menu data in object
      redraw(filter(menuObject.menu), $content_container, 0); // draw the menu html

      // Code for displaying the current viewed node in the menu.
      var found = false;
      var stack = [];
      var current_nid = Drupal.settings.ndla_utils.nid;
      function traverseAndPrint(item, nid) {
        if(found) return;

        if(typeof(item.children) != "undefined") {
          _.each(item.children, traverseAndPrint);
          if(found) stack.push(item);
        }

        if(typeof(item.raw_path) != "undefined") {
          if(item.raw_path == "node/" + current_nid) {
            stack.push(item);
            found = true;
          }
        }
      }

      _.each(menuObject.menu,traverseAndPrint);
        for(var i = stack.length - 1; i >= 0; i--) {
          $('#' + stack[i].id).click();
        }

    } // end function
  }); // end ajax

  // Eventfunction for clicks. Will be called programmaticly the enable context menus.
  var content_container_event_function = function(event) {

      var id_str = $(this).attr('id');
      var class_str = $(this).attr('class');
      var $id = $('#'+id_str);

      // What will happend if we click the same element twice?
      if( _.contains(clickTracker, id_str)) {
      //  return; // NOT A THING at the moment.
      }

      if(class_str.match(/link/)) {
        event.stopImmediatePropagation();

      }
      // Add this clicked ID to the clickTracker array
      clickTracker.push(id_str);

      if(_.isEmpty(Drupal.settings.currentMenu)) {
        Drupal.settings.currentMenu = menuObject.menu;
      }

      var found_depth = find_depth(menuObject.menu, id_str);

      if(typeof(found_depth) == "undefined") {
        found_depth = 0;
      }

      if(class_str.match(/aria-level-[1-9]/)) {
        $('.menu-title').css({'font-weight': '100'});
        $('#'+id_str + ' .menu-title').css({'font-weight': '600'});
        Drupal.settings.currentMenu = filter(menuObject.menu, id_str);
        redraw(Drupal.settings.currentMenu, $content_container, found_depth);
        $('.loaded-node').parents('.added').find('li').width('940px')
      }
  }



  // Attach click event for all the <li>-tags
  $content_container.on('click', 'li.clickable', function(event) {
      content_container_event_function.call(this, event);
  });

  $content_container.on('click', '.expand-icon', function(event) {
    event.stopImmediatePropagation();
    if($(this).hasClass('active')) {
      $(this).removeClass('active');
      $(this).parent().children('.menu-sublist').children().children().slideUp();
    } else {
      $(this).addClass('active');
      $(this).parent().children('.menu-sublist').children().children().slideDown();
    }
    
  });

  // Give the function an external and a internal name
  var find_depth = function __find_depth(menuObject, menu_id, depth) {
    // set depth variable
    if(typeof(depth) == "undefined") depth = 0; 

    for(var i = 0; i < menuObject.length; i++) {
      // depth is found, return!
      if(menuObject[i].id == menu_id) return depth; 
      // do we have anymore childre to recurse through
      if(!_.isEmpty(menuObject[i].children)) {
        // Recurse
        var found = __find_depth(menuObject[i].children, menu_id, depth + 1);
        // if we found depth, return!
        if(typeof(found) != "undefined") return found; 
      } // if empty
    } // for 
  } // function

  var filter = function(menuObject, menu_id) {
    if(typeof(menu_id) == "undefined" ) return menuObject;
    var filteredMenu = {};

    var selected_menu = _(menuObject).select(function(item) {
      if(item.id == menu_id) return true;
    });

    if(!_.isEmpty(selected_menu)) {
      return _.first(selected_menu).children;
    }

    var m;
    for(var i = 0; i < menuObject.length; i++) {
      if(!_.isEmpty(menuObject[i].children)) {
        m = filter(menuObject[i].children, menu_id);
      }
      if(typeof(m) != "undefined") {
        return m;
      }
    }
  };

  var redraw = function(menu, $content_container, depth) {
    var max_iterations = 20; // max menu depth iterations setting
    for (var i = max_iterations; i >= depth; i--) {
        var current_iteration = i; // for inner scope
        $content_container.find('.added').css({'background-color': '#EDEDED'});
        $content_container.find('.aria-level-' + (i+1).toString()).css({'border': '1px solid #EDEDED'});

        $content_container.find('.aria-level-' + (i+1).toString()).animate({'height': '32px'},400,function() {
          $content_container.find('.aria-level-' + (current_iteration+1).toString()).find('.menu-image').remove();
          $content_container.find('.aria-level-' + (current_iteration+1).toString()).find('.menu-description').remove();
        });
      $content_container.find('.aria-level-' + (i+2).toString()).parent().remove();
    }
    var basepath = Drupal.settings.basePath; // Get drupal basepath for image uri's

    // menu item template
    var list = "<% var counter = 0; _.each(items, function(item) { counter++ %> \
                  <li class='<%= item.class %> <%= (item.clickable == true) ? 'clickable' : 'nonclickable' %>' id='<%= item.id %>'> \
                    <% if(item.class.match(/has-children/)) { %> \
                      <div class='expand-icon'></div> \
                    <% } %> \
                    <span class='menu-title'><%= item.data %></span> \
                    <% if (item.image_file_path != '') { %> \
                      <% if (typeof(item.url) != 'undefined') { %> \
                        <a href='<%= item.url %>'><img class='menu-image' src='<%= basepath + item.image_file_path %>'></a> \
                      <% } else { %> \
                        <img class='menu-image' src='<%= basepath + item.image_file_path %>'>\
                      <% } %> \
                    <% } %> \
                    <span class='menu-description'><%= item.text %></span> \
                    <% if(item.class.match(/has-children/) && item.clickable == false) { %> \
                      <div class='menu-sublist'> \
                          <ul class='submenu-ul <%= item.class.match(/aria-level-[0-9]/) %> <%= item.class.match(/expanded/) %>'> \
                            <%= _.template(_.list_template, {items : item.children, basepath : basepath}) %> \
                          </ul> \
                      </div> \
                    <% } %> \
                  </li> \
                <% }); %>";

    // Assign the template to the _-object for recursive reference within the template.
    _.list_template = list;
    // apply menu item template
    var new_html = _.template(list, {items : menu, basepath : basepath});
    $content_container.append('<ul class="added" style="display:none;">' + new_html + '</ul>');

    // Animate the newly added html with a slideDown effect.
    $content_container.find('ul.added').slideDown();
  };
}
