<div class="<?php print implode(' ', $classes_array) ?>">
  <div class="inner-wrapper">
    <div class="inner-inner-wrapper">
      <?php if (isset($title)): ?>
        <h2><?php print $title ?></h2>
      <?php endif ?>
      <div class="content">
        <?php print $content ?>
      </div>
    </div>
  </div>
</div>