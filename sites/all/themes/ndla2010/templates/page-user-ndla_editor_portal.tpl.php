<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html version="XHTML+RDFa 1.0" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" xmlns:og="http://opengraphprotocol.org/schema/" dir="<?php print $language->dir ?>" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:skos="http://www.w3.org/2004/02/skos/core#">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php global $base_url; ?>
    <meta name="application-name" content="NDLA" />
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage" content="<?php print $base_url; ?>/sites/all/themes/ndla2010/img/ndla2013.png"/>
    <?php print $styles ?>
    <?php print $scripts ?>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print base_path() . drupal_get_path('module', 'ndla_editor_portal') . '/css/ndla_editor_portal.css';?>" />
  </head>
  <body class="<?php print $classes ?>">
    <div class='no-css'>
      <a href='<?php print $front_page; ?>'>NDLA</a><br />
    </div>
    <a accesskey='S' href='#page-content' class='skip'><?php print t('Go to content') ?></a>
    <div id="container">
      <?php print $header ?>
      <div class="center-layout">
        <?php $portal_class = (arg(3) == '') ? 'front' : arg(3); ?>
        <div id="main_content" class="portal clear-block <?php print "portal-$portal_class"; ?>">
          <?php print $left; ?>
          <div id="main_1" class="alpha">
            <?php if (!$node || $node->type != 'fag'): ?>
              <div id="top-bla-menu"></div>
              <div id="page-content" class="clear-block" role="main">
                <a name='page-content'></a>
                <h1><?php print $title ?></h1>
            <?php endif ?>
            <?php print $messages ?>
            <?php if (!$hide_content) print $tabs ?>
            <?php print $links ?>
        
            <?php if(!empty($portal_top)): ?>
              <div id='portal-top'>
                <?php print $portal_top; ?>
              </div>
            <?php endif; ?>
            
            <?php if(!empty($portal_middle_left) || !empty($portal_middle_right)): ?>
            <div id='portal-middle-wrapper'>
              <?php if(!empty($portal_middle_left)): ?>
              <div id='portal-left'>
                <?php print $portal_middle_left; ?>
              </div>
              <?php endif; ?>
              
              <?php if(!empty($portal_middle_right)): ?>
              <div id='portal-right'>
                <?php print $portal_middle_right; ?>
              </div>
              <?php endif; ?>
              <div style='clear: both;'></div>
            </div>
            <?php endif; ?>
            <div id="portal-content" class="<?php print $portal_class; ?>">
              <?php print $content ?>
            </div>
            <?php if(!empty($port_low)): ?>
              <div id='portal-low'>
                <?php print $port_low; ?>
              </div>
            <?php endif; ?>
            <?php if (!$node || $node->type != 'fag'): ?>
              </div>
              <?php if($readspeaker): ?>
                </div>
              <?php endif ?>
            <?php endif ?>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
    <?php print $closure ?>
    <?php if ($ndla_textselect) print $ndla_textselect ?>
    <?php print $jira_button ?>
  </body>
</html>