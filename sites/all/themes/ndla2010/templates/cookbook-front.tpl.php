<script type="text/javascript">
<!--//--><![CDATA[//><!--
$(document).ready(function () {
  var $author = $('.overlay-2');
  CookbookFront.$overlay = CookbookFront.$overlay.add($author);
  CookbookFront.imageLoaded = function () {
    var $this = $(this);
    CookbookFront.$overlay.fadeOut(200, function () {
      $this.prev().fadeOut(200, function () {
        $(this).remove();
        CookbookFront.$header.html(CookbookFront.recipes[CookbookFront.i]['title']);
        $author.html(CookbookFront.recipes[CookbookFront.i]['image_authors'] != undefined ? '<span style="padding: 3px 6px">' + Drupal.t('Copyright, image:') + ' ' + CookbookFront.recipes[CookbookFront.i]['image_authors'] + '</span>' : '');
        CookbookFront.$link.attr('href', CookbookFront.$link.attr('href').replace(/(\/)(\d+)(\?|$)/, "$1" + CookbookFront.recipes[CookbookFront.i]['nid'] + "$3"));
        $this.fadeIn(600, function () {
          CookbookFront.$overlay.fadeIn(200);
        });
      });
    });
  };
});
//--><!]]>
</script>
<div class="block" id="recipes">
  <h2><?php print t('Recipes') ?></h2>
  <div class="content" style="position: relative">
    <div class="overlay">
      <div>
        <h3><?php print check_plain($recipe->title) ?></h3>
        <p>
          <a href="<?php print url('cookbook/recipe/0/0/0/0/' . $recipe->nid) ?>" class="reverse-fading"><?php print t('To recipe') ?>
            <span>&nbsp;&gt;&gt;</span>
          </a>
        </p>
      </div>
    </div>
    <div class="overlay-2" style="font-size: 10px; position: absolute; right:15px; top: 418px; background: #fff; background: rgba(255, 255, 255, 0.75);">
      <?php if (isset($recipe->image_authors)): ?>
        <span style="padding: 3px 6px"><?php print t('Copyright, image:') . ' ' . $recipe->image_authors ?></span>
      <?php endif ?>
    </div>
    <div class="image">
      <img src="<?php print check_url($recipe->image) ?>" alt="" width="576" height="432"/>
    </div>
  </div>
</div>
<div class="block" id="vertical-categories">
  <h2><?php print t('Categories') ?></h2>
  <div class="content">
    <table>
      <?php foreach ($categories as $category): ?>
        <tr>
          <td>
            <a href="<?php print url('cookbook/results/' . $category->tid .'/0/0/0') ?>" class="reverse-fading">
              <?php print taxonomy_image_display($category->tid) ?>
              <span><?php print check_plain($category->name) ?></span>
            </a>
          </td>
        </tr>
      <?php endforeach ?>
    </table>
  </div>
</div>