<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html version="XHTML+RDFa 1.0" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" xmlns:og="http://opengraphprotocol.org/schema/" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head;?>
    <?php global $base_url; ?>
    <meta name="application-name" content="NDLA" />
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage" content="<?php print $base_url; ?>/sites/all/themes/ndla2010/img/ndla2013.png"/>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body class="front">
    <div class='no-css'>
      <a href='<?php print $front_page; ?>'>NDLA</a><br />
    </div>
    <a accesskey='S' href='#page-content' class='skip'><?php print t('Go to content') ?></a>
    <?php if($fb_js = variable_get('ndla_utils_facebook_js', '')) print $fb_js; ?>
    <?php if($readspeaker): print "<div class='hidden'>" . $readspeaker . "</div>"; endif; ?>
    <div id="container">
      <div id="header" class="clearfix">
        <div class="center-layout clearfix">
          <?php if ($logo): ?>
            <h1 class="logo">
              <a href="<?php print $front_page ?>" title="<?php print t('To start page for'); print ' '; print $site_name ?>">
                <img src="<?php print $logo ?>" alt="<?php print t('To start page for'); print ' '; print $site_name ?>"/>
              </a>
            </h1>
          <?php endif ?>
          <ul class="links">
            <li><?php print $language_switcher ?></li>
            <li class='pipe'>|</li>
            <li><a href="<?php print url('node/59982') ?>"><?php print t('Help') ?></a></li>
          </ul>
          <p class="slogan"><?php print $front_page_slogan ?></p>
          <?php print theme('ndla2010_black_boxes'); ?>
          <div class='clearfix'></div>
        </div>
      </div>
      <div id="navigation">
        <div class="center-layout">
          <div id="county"></div>

          <div id="search">
            <?php print $search_box ?>
          </div>
          <a href='javascript:void(0)' class='resourcemap-button'><i class="fa fa-globe"></i><?php print t('Resource map') ?></a>
        </div>
      </div>
      <div id="content">
        <div class="center-layout">
        <?php print $messages ?>
        <div id="subjects" role="navigation">
          <a name='page-content'></a>
          <?php print $courses ?>
        </div>
        <?php print $applications ?>
        <div id="feeds-outer">
          <div id="feeds">
            <div class="recommend">
              <div class="top">
                <?php print $new_content ?>
              </div>

          </div>
            <div class='clear-float'></div>
        </div>
        </div>
      </div>
      </div>
        <div id="footer-wrapper">
          <div id="footer">
            <div class="center-layout">
              <div class="small-blocks about">
                <div class='links'><ul class='help_link'>
                  <li class='first'><a href="<?php print $help_url ?>"><?php print t('Help') ?></a></li>
                  <li><a href="<?php print $about_url ?>"><?php print t('About NDLA') ?></a></li>
                  <li><a href="http://om.ndla.no/nyhetsbrev"><?php print t('Newsletter')?></a></li>
                  <li><a href="http://arkiv.ndla.no/"><?php print t('Archive') ?></a></li>
                </ul>
                </div>
                <p><?php print $footer_message ?></p>
                <p><?php print t('Editor in chief') ?>: <?php print $footer_editor_in_chief_name ?><br/><?php print t('Managing editor') ?>: <?php print $footer_web_editor_name ?></p>
              </div>
              <div class="small-blocks links">
              </div>
              <div class="small-blocks wiki">
                  <ul class='social'>
                    <li>
                        <a href='https://www.facebook.com/ndla.no' target='blank' title='<?php print t('Like us on Facebook'); ?>' class="fa-stack fa-lg">
                          <i class="facebook-color fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-facebook frontpage fa-stack-1x fa-inverse"></i>
                        </a>
                    </li>
                    <li>
                        <a href='https://twitter.com/ndla_no' target='blank' title='<?php print t('Follow us on Twitter'); ?>' class="fa-stack fa-lg">
                          <i class="twitter-color fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-twitter frontpage fa-stack-1x fa-inverse"></i>
                        </a>
                    </li>
                    <li>
                        <a href='https://plus.google.com/+ndlano' target='blank' title='<?php print t('Follow us on Google Plus'); ?>' class="fa-stack fa-lg">
                          <i class="google-color fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-google-plus frontpage fa-stack-1x fa-inverse"></i>
                        </a>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
        </div>
    </div>
    <?php print $page_closure ?>
    <?php print $closure ?>
    <?php if($ndla_textselect) {
        print $ndla_textselect;
    } ?>
    <?php if ($roamauth_url): ?>
    <script type="text/javascript" src="<?php print $roamauth_url; ?>"></script>
    <?php endif; ?>
    <?php print $tns_code ?>
    <?php print $jira_button ?>
  </body>
</html>
