<i class='search-icon fa fa-search'></i>
<input placeholder='<?php print t('Enter search terms'); ?>' accesskey="4" type="text" maxlength="128" name="search_theme_form" id="edit-search-theme-form-1" size="40" class="<?php print $classes ?>" alt="<?php print t('Search') ?>"/>
<input class="autocomplete" type="hidden" id="edit-search-theme-form-1-autocomplete" value="<?php print $autocomplete_path ?>" disabled="disabled" />
<span class="form-submit-border"><?php print $search['submit'] ?></span>
<?php print $search['hidden'] ?>
