<?php
  $url = url('node/' . $node->nid, array('absolute' => true));
  $type = node_get_types('type', $node);
?>

<div style="margin-top: 2em;float: left; border:1px solid grey; width: 100%" class="qrcode-container">
  <div style="float: left;margin: 10px;">
    <img  src='http://chart.apis.google.com/chart?chs=150x150&cht=qr&chld=L&chl=<?php print $url ?>'/><br />
  </div>
  <div style="float: left;line-height: 3em;margin: 20px;">
    <strong><?php print $node->title; ?></strong><br />
    <em><?php print $type->name; ?></em><br />
    <a href="<?php print $url ?>"><?php print $url ?></a><br />
  </div>
</div>
