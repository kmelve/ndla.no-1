<div id="block-<?php print $id ?>" class="<?php print $block_classes . $class; ?>">
  <?php if (!empty($title)): ?>
    <?php if (isset($inner_class)): ?>
      <div class="<?php print $inner_class ?>"<?php if ($collapsible): print ' title="' . t('Expand/collapse') . '"'; endif ?>>
        <?php if ($collapsible): ?> 
          <div class="sprite-image small-expand-collapse"></div>
        <?php endif ?>
        <h2 class="top"><?php print $title ?></h2>
      </div>
    <?php else: ?>
      <h2 class="top"><?php print $title ?></h2>
    <?php endif ?>
  <?php endif ?>
  <div class="<?php print $content_class ?>">
    <?php print $content ?>
  </div>
</div>