<div id="sort-by">
  <form action="" method="get">
    <div>
      <?php print t('Sort search result') ?>:
      <select name="sort_by" onchange="submit()">
        <?php foreach ($sort_bys as $key => $name): ?>
          <option value="<?php print $key ?>"<?php if (ndla_utils_search_get_sort_by() == $key): print ' selected="selected"'; endif ?>><?php print $name ?></option>
        <?php endforeach ?>
      </select>
      <?php print ndla_apachesolrmsf_build_facet_form_values('sort_by') ?>
    </div>
  </form>
</div>
<hr/>
<h1><?php print t('Search results') ?></h1>
<div class="hits"><?php print format_plural($hits, '1 result', '@count results') ?></div>
<ol class="search-results <?php print $type ?>-results">
  <?php print $search_results ?>
</ol>
<?php print $pager ?>
