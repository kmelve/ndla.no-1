<?php if ($left_menu || $topic_menu): ?>
  <div id="left-menu" role="navigation" aria-label="<?php print t('Menu')?>">
    <div id="left-menu-menu-content" class="left-menu-content<?php if (!$topic_menu) print ' left-menu-content-selected' ?>">
      <?php if ($default_tab != 'topics'): ?>
        <div class="top toptoptop"></div>
      <?php endif ?>
      <?php print $left_menu ?>
    </div>
    <div id="left-menu-topics-content" class="left-menu-content<?php if ($topic_menu) print ' left-menu-content-selected' ?>">
      <?php print $topic_menu ?>
    </div>
  </div>
<?php endif ?>

<div id="tray"<?php if ($default_tab != 'topics') print ' class="tray-closed"' ?>>
  <div id="tray-left">
    <div id="tray-right">
      <div id="tray-inner" aria-live="polite"></div>
    </div>
  </div>
  <div id="close-tray" class="fa fa-caret-up"></div>
</div>
