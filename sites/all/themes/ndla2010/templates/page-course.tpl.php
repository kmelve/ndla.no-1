<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html version="XHTML+RDFa 1.0" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" xmlns:og="http://opengraphprotocol.org/schema/" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php global $base_url; ?>
    <meta name="application-name" content="NDLA" />
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage" content="<?php print $base_url; ?>/sites/all/themes/ndla2010/img/ndla2013.png"/>
    <?php print $styles ?>
    <?php print $scripts ?>
    <?php if($node->ndla_utils_ndla2010_theme_fag_template == 2) {
      //ESI-include etc is a bitch - use javascript to create equal heights
      global $theme_path, $base_path;
      print "<script type='text/javascript' src='" . $base_path . $theme_path . "/js/eq-height-course.js'></script>";
    }
    ?>
  </head>
  <body class="<?php print $classes ?>">
    <div class='no-css'>
      <a href='<?php print $front_page; ?>'>NDLA</a><br />
    </div>
    <a accesskey='S' href='#page-content' class='skip'><?php print t('Go to content') ?></a>
    <?php if($fb_js = variable_get('ndla_utils_facebook_js', '')) print $fb_js; ?>
    <?php if($readspeaker) print "<div class='hidden'>" . $readspeaker . "</div>" ?>
    <div id="container">
      <?php print $header ?>
      <div id="content">
      <!--[if IE 7]>
      <div class="center-layout get-back">
      <![endif]-->
      <!--[if !IE 7]><!-->
      <div class="center-layout">
      <!--<![endif]-->
        <?php print $left_menu ?>
        <div id="main_content">
          <a name='page-content'></a>
          <?php print $messages ?>
          <?php if (!$hide_content) print $tabs ?>
          <?php if ($editorial): ?>
            <div id="editorial">
              <div id="editorial-content">
                <?php print $editorial ?>
              </div>
            </div>
          <?php endif ?>
          
          <div class='ndla_feeds_apps_container'>
          <?php if($node->ndla_utils_ndla2010_theme_fag_template == 2): ?>
            <?php if(!empty($node->ndla_feeds)): ?>
              <div id='ndla_feeds' class='tabbed_content'>
                <?php $li = array(); $content = array(); ?>
                <ul class='tabs block-tab'>
                  <?php 
                  $num_feeds = count($node->ndla_feeds);
                  $classes = array(1 => 'full', 2 => 'fifty', 3 => 'third');
                  foreach($node->ndla_feeds as $index => $feed) {
                    $item = ndla_feeds_parse_url($feed['url'], $feed['title']);
                    $class = ($index == 0) ? ' active ' : '';
                    $class .= !empty($classes[$num_feeds]) ? $classes[$num_feeds] : '';
                    $li[] = "<li class='tab_title $class'><h2 class='block-tab'><a class='feed-" . $index . "' href='javascript:void(0)'>" . $item['title'] . "</a></h2></li>";
                    $content[] = "<div class='feed-content node-" . $node->vid . " feed-" . $index . " " . $class ."'> .... </div>";
                  } 
                  print implode("", $li);
                print "</ul>";
                print "<div class='clearfix'></div>";
                print "<div class='content'>" . implode("", $content) . "</div>";
              ?>
              </div>
            <?php else: ?>
              <div id='ndla_feeds'>
                <h2 class='block-tab'><?php print('Feeds'); ?></h2>
                <div class='content'>
                  <?php print t('Feeds template selected, but no feeds are available.'); ?>
                </div>
              </div>
            <?php endif; ?>
          <?php endif; ?>
          


          <div id="left-blocks">
            <div id="first-blocks">
              <?php if($node->ndla_utils_ndla2010_theme_fag_template != 2): ?>
              <div class="recommend">
                <h2 class="block-tab"><?php print t('The editor recommends') ?></h2>
                <?php if ($new_content) print $new_content ?>
              </div>
              <?php endif; ?>

              <div class='user-recommends'>
                <h2 class="block-tab"><?php print t('Our users think') ?></h2>
                  <?php if (module_exists('ndla_feedback')): ?>
                    <div class="first">
                      <?php $block = module_invoke('ndla_feedback', 'block', 'view', 'most_liked', 'course_page');
                        print '<h3>' . $block['subject'] . '</h3>';
                        print '<div id="block-ndla-feedback-most-liked">' . $block['content'] . '</div>'; ?>
                    </div><!-- end first -->

                    <div class="last">
                      <?php $block = module_invoke('ndla_feedback', 'block', 'view', 'recent_comments', 'course_page');
                        print '<h3>' . $block['subject'] . '</h3>';
                        print '<div id="block-ndla-feedback-recent-comments">' . $block['content'] . '</div>'; ?>
                    </div> <!-- end last -->
                  <?php else: ?>
                    <h3><?php print t("The service isn't available at this site."); ?></h3>
                  <?php endif ?>
                  <a href="<?php print $feedback_url ?>" class="feedback-icon"><i class='fa fa-chevron-circle-right'></i><?php print t('More') ?></a>
                  <div class='clearfix'></div>
              </div><!-- end user recommends -->
              
              <?php if($node->ndla_utils_ndla2010_theme_fag_template != 2): ?>
              
                <?php $block = module_invoke('ndla_apps', 'block', 'view', 'ndla_apps_course_page');
                if(!empty($block['content'])): ?>
                <div id="ndla-apps-block">
                  <h2 class="block-tab apps"><?php print t('Tools') ?></h2>
                  <?php print $block['content']; ?>
                </div><!-- end ndla apps -->
                <?php endif; ?>

              <?php endif; ?>

              <div class='clearfix'></div>

            </div>
          </div>
              <?php if($node->ndla_utils_ndla2010_theme_fag_template == 2): ?>
                <?php $block = module_invoke('ndla_apps', 'block', 'view', 'ndla_apps_course_page');
                if(!empty($block['content'])): ?>
                <div id="ndla-apps-block">
                  <h2 class="block-tab apps"><?php print t('Tools') ?></h2>
                  <?php print $block['content']; ?>
                </div><!-- end ndla apps -->
                <?php endif; ?>
              <?php endif; ?>
          </div>
          
          <?php if($tweets): ?>
          <div id="twitter-block">
            <h2 class="block-tab twitter"><i class='fa fa-twitter'></i><?php print t('Twitter') ?></h2>
              <ul>
                <?php print $tweets ?>
              </ul>
          </div>
        <?php endif; ?>
        <?php if(function_exists('ndla_utils_get_fb_html')): ?>
          <div id="facebook-block">
            <h2 class="block-tab facebook"><i class='fa fa-facebook'></i><?php print t('Facebook') ?></h2>
              <ul>
                <?php print ndla_utils_get_fb_html($node->nid); ?>
              </ul>
          </div>
        <?php endif; ?>
        <?php if($google_plus): ?>
        <div class='google_plus'>
          <?php
            if($node->ndla_utils_ndla2010_theme_fag_template != 2) {
              //Default NDLA width is 220px, if we are not using the feeds
              //template, go for 240px.
              $google_plus = str_replace("220", "240", $google_plus);
            }
            print $google_plus;
          ?>
        </div>
        <?php endif; ?>

        
      </div>

      <div class='clearfix'></div>
      <div id="footer-container" class="footer-collapsed" role="contentinfo">
        <div id="footer-top">
          <h2 id="footer-header"><a href='javascript:void(0);'><i class='black-color fa fa-chevron-down'></i><i class='black-color fa fa-chevron-right'></i><?php print t('About this learning resource') ?></a></h2>
        </div>
        <div id="footer-tabs" class="footer-content clear-block">
          <ul id="footer-left">
            <?php if (!$hide_content): ?>
              <?php $footer_blocks = block_list('footer'); $i = 0; foreach ($footer_blocks as $footer_block): ?>
                <li id="footer-<?php print $footer_block->delta ?>" class="footer-tab<?php if (!$i) print ' first footer-tab-selected' ?>"><?php print $footer_block->subject ?></li>
              <?php $i++; endforeach ?>
            <?php endif ?>
          </ul>
          <div id="footer-right" class="clear-block">
            <div id="inner">
              <?php if (!$hide_content): ?>
                <?php $i = 0; foreach ($footer_blocks as $footer_block): ?>
                  <div id="footer-<?php print $footer_block->delta ?>-content" class="footer-block<?php if (!$i) print ' first footer-block-selected' ?>"><?php print $footer_block->content ?></div>
                <?php $i++; endforeach ?>
              <?php endif ?>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
      <div id="footer-wrapper">
        <div id="footer">
          <div class="center-layout">
            <?php print $footer ?>
          </div>
          <div id="footer-bottom">
            <div class="center-layout">
              <?php print $about_footer ?>
              <?php print $page_closure ?>
            </div>
          </div>
        </div>
      </div>
    
    <?php print $closure ?>
    <?php if($ndla_textselect) print $ndla_textselect ?>
    <?php if ($roamauth_url): ?>
      <script type="text/javascript" src="<?php print $roamauth_url; ?>"></script> 
    <?php endif; ?>
    <?php print $tns_code ?>
    <?php print $jira_button ?>
    </div>
  </body>
</html>
