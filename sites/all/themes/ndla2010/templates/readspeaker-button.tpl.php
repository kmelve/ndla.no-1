<?php
global $language;
$l = $language->language;
$readspeaker_languages = readspeaker_language_list();
$protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
$url = url(check_plain($_REQUEST['q']), array('absolute' => TRUE));
$url  = str_replace("$l/$l", $l, $url);
$extra_params = trim(check_plain(variable_get('readspeaker_extra_parameters', '')));
if(!empty($extra_params)) { $extra_params = "&amp;" . $extra_params; }

?>
<a id="readspeaker_wake" class="readspeaker" accesskey="L" title="Title" href="<?php print $protocol; ?>://app.eu.readspeaker.com/cgi-bin/rsent?customerid=<?php print $account_id; ?>&amp;audiofilename=<?php print $audiofilename; ?>&amp;lang=<?php print $lang_id; ?>&amp;readid=rs_read_this&amp;url=<?php print urlencode($url); print $extra_params; ?>">
<i class='fa fa-volume-up'></i> <span><?php print t('Listen') ?></span>
</a>
<script>
  (function () {
    var element = document.getElementById('readspeaker_wake');
    var clickEvent = function (e) {
      var evt = e ? e:window.event;
      if (evt.stopPropagation)
        evt.stopPropagation();
      if (evt.cancelBubble!=null)
        evt.cancelBubble = true;
      if (evt.preventDefault)
        evt.preventDefault();

      var rsButton1 = document.getElementById('readspeaker_button1');
      if (rsButton1) {
        rsButton1.style.display = 'block';
        // Replace link with customized link content from old link. 
        $(rsButton1).find('.rsbtn_play').first().attr('href', $(this).attr('href'));
        $(rsButton1).find('.rsbtn_play').first().trigger('click');
      }

      return false;
    };
    if(document.addEventListener)
      element.addEventListener('click', clickEvent, false);
    else if (document.attachEvent)
      element.attachEvent('onclick', clickEvent);
    else
      element.onclick = clickEvent;
  })();
</script>
