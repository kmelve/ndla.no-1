<?php
// $Id$

/**
 * @file
 * Default print module template
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
  <head>
    <?php print $print['head']; ?>
    <title>Nasjonal digital læringsarena | <?php print $print['title']; ?></title>
    <?php print $print['scripts']; ?>
    <?php print $print['robots_meta']; ?>
    <?php print $print['base_href']; ?>
    <?php print $print['favicon']; ?>
    <?php print $custom_styles; ?>
    <script type="text/javascript">
      $(document).ready(function(){
        window.print();
      });
    </script>
  </head>
  <body<?php print $print['sendtoprinter']; ?>>
  <?php print $test; ?>
    <?php if (!empty($print['message'])) {
      print '<div class="print-message">'. $print['message'] .'</div><br />';
    } ?>
    <div id="main_content">
      <?php if ($node_type): ?>
        <div class="nodetype"><?php print $node_type; ?></div>
      <?php endif; ?>
      <h1 class="print-title"><?php print $print['title']; ?></h1>
      <?php if ($copyright): ?>
         <div class="owner"><?php print $copyright; ?></div>
      <?php endif; ?>
      <div class="print-submitted"><?php print $print['submitted']; ?></div>
      <div class="print-created"><?php print $print['created']; ?></div>
      <div class="print-content">
        <?php print $print['content']; ?>
      </div>
      <div class="print-taxonomy"><?php print $print['taxonomy']; ?></div>
    </div> <!-- End main content -->
    <div class="print-footer"><?php print filter_xss_admin(t(variable_get('site_footer', FALSE))) ."\n". theme('blocks', 'footer'); ?></div>
  </body>
</html>
