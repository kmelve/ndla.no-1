<?php global $base_path; ?>
<ul id="applications">
  <?php for ($i = 0, $size = count($applications); $i < $size; $i++): ?>
    <li<?php print (($i+1) % 3) == 1 ? ' class="clear-float"' : '' ?>>
      <a title="<?php print $applications[$i]->title . ' - ' . $applications[$i]->description ?>" href="<?php print str_replace(array('%3F', '%3D'), array('?', '='), $applications[$i]->href) ?>" target="_blank">
        <img src="<?php print str_replace("//", "/", $base_path . $applications[$i]->image) ?>" alt=""/>
      </a>
    </li>
  <?php endfor ?>
</ul>