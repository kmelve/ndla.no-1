<?php
/**
 * @file
 * @ingroup ndla2010
 */
?>
<div id="page-content" class="clear-block">
<?php
if ($node->field_ingress_bilde != '') {
	$image = node_load($node->field_ingress_bilde[0]['nid']);
}

$preset = 'fag_preset';
print theme('imagecache', $preset, $image->images['_original'], $image->title, '', array("style" => "float:left; padding-right: 1em;"));

?>
<?php if ($title): ?>
<h1><?php print $title; ?></h1>
<?php endif; ?>

<div class="ingress"><?php print $node->field_ingress[0]['safe']; ?></div>
</div>

<?php if ($links): ?>
  <div class="terms terms-inline"><?php print $links ?></div>
<?php endif;?>
