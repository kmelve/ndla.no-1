<div<?php print $class ? ' class="subject_container ' . $class . '"' : ' class="subject_container"' ?>>
  <h2 class="block-tab"><?php print $course_category ?></h2>
  <?php
    $columns = 3;
    $per_column = ceil((count($courses)/$columns));
    $courses = array_chunk($courses, $per_column);
    foreach($courses as $column) {
      print "<div class='column'>";
      print "<ul>";
      foreach($column as $course) {
        $class = "course_item";
        $span = '';
        if(!empty($course->in_dev)) {
          $span = "<span class='beta'><i class='fa fa-exclamation-circle'></i>Beta</span>";
        }
        print "<li class='$class'>";
        
        if($render_options) {
          print "<span class='subject_selector'><input type='checkbox' name='my_subject_selector' value='" . $course->nid ."' />" . trim($course->name) . "</span>";
        }
        print "<a class='subject_link nid-" . $course->nid . "' href='" . $course->url ."'>" .  trim($course->name) . $span . "</a>";
        
        print "</li>";
      }
      print "</ul>";
      print "</div>";
    }
  ?>
<div class='clearfix'></div>
</div>
