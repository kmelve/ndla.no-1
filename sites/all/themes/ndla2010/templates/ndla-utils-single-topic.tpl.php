<?php
/* Available variables:
 * $topic(object)
 *
 * NOTE: We use stored_src instead of src in the image tag because we want to avoid that the browser requests images that aren't to be shown yet.
 * We use JQuery to control when images are to be loaded.
 */
$classes[] = 'topic';
if ($counter % 4 == 0) {
  $classes[] = 'clear-left';
}
?>
<li id="topic-<?php print $topic->tid?>" tid="<?php print $topic->tid?>" class="<?php print implode(' ', $classes) ?>" num_children="<?php print $topic->num_children?>">
  <h3 class="topic-heading"><a href="#"><?php print check_plain($topic->name)?></a></h3>
  <img width="210" stored_src="<?php print check_url($topic->image)?>" alt="<?php print t('Topic image')?>"/>
  <div class="topic-description"><?php print check_plain($topic->description)?></div>
</li>