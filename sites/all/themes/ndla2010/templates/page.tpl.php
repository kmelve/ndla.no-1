<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html version="XHTML+RDFa 1.0" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" xmlns:og="http://opengraphprotocol.org/schema/" dir="<?php print $language->dir ?>" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:skos="http://www.w3.org/2004/02/skos/core#">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php global $base_url; ?>
  <meta name="application-name" content="NDLA" />
  <meta name="msapplication-TileColor" content="#FFFFFF"/>
  <meta name="msapplication-TileImage" content="<?php print $base_url; ?>/sites/all/themes/ndla2010/img/ndla2013.png"/>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>
<body class="<?php print $classes ?>">
  <div class='no-css'>
    <a href='<?php print $front_page; ?>'>NDLA</a><br />
  </div>
  <a accesskey='S' href='#page-content' class='skip'><?php print t('Go to content') ?></a>
  <div id="container">
    <?php print $header ?>
    <div id="content">
      <!--[if IE 7]>
      <div class="center-layout get-back">
      <![endif]-->
      <!--[if !IE 7]><!-->
      <div class="center-layout">
      <!--<![endif]-->
        <?php print $left_menu ?>
        <div id="main_content" class="clear-block">
          <div id="top-bla-menu"></div>
          <div id="main_1" class="alpha">  
            <?php if (!$node || $node->type != 'fag'): ?>
              <?php if ($topic_description) print $topic_description ?>

              <?php if($readspeaker): ?>
              <div id='rs_1'></div> 
              <?php endif ?>
              <div id="page-content" class="clear-block" role="main">
                <?php if (!empty($node_type)): ?>
                  <div class="nodetype"><?php print $node_type ?></div>
                <?php endif ?>
                <a name='page-content'></a>
                <h1><?php print $title ?></h1>
                <?php if (!empty($subtitle)): ?>
                  <h2><?php print $subtitle ?></h2>
                <?php endif ?>
                <?php if ($copyright): ?>
                  <div class="owner rs_skip"><?php print $copyright ?></div>
                <?php endif ?>
                <?php if (!empty($node->show_edit_date)) print $node->edit_dates ?>
              <?php endif ?>
              <?php print $messages ?>
              <?php if (!$hide_content) print $tabs ?>
              <?php print !empty($links) ? $links : NULL ?>
              <?php if (!$hide_content) print $content ?>
              <?php if ($content_low) print $content_low ?>
              <?php if (!isset($node) || (!empty($node->type) && $node->type != 'fag')): ?>
              </div>
            <?php endif ?>
          </div>
          <?php print $right ?>
        </div>
        <div class='clearfix'></div>
        <div id='prefooter'>
          <?php 
          if($prefooter):
            print $prefooter;
          endif;
          ?>
        </div>
        <div id="footer-container" class="footer-collapsed" role="contentinfo">
          <div id="footer-top">
            <?php if(arg(0) == 'node'): ?>
            <h2 id="footer-header"><a href='javascript:void(0);'><i class='black-color fa fa-chevron-down'></i><i class='black-color fa fa-chevron-right'></i><?php print t('About this learning resource') ?></a></h2>
            <?php endif; ?>
          </div>
          <div id="footer-tabs" class="footer-content clear-block">
            <ul id="footer-left">
              <?php if (!$hide_content): ?>
                <?php $footer_blocks = block_list('footer'); $i = 0; foreach ($footer_blocks as $footer_block): ?>
                  <li id="footer-<?php print $footer_block->delta ?>" class="footer-tab<?php if (!$i) print ' first footer-tab-selected' ?>"><?php print $footer_block->subject ?></li>
                <?php $i++; endforeach ?>
              <?php endif ?>
            </ul>
            <div id="footer-right" class="clear-block">
              <div id="inner">
                <?php if (!$hide_content): ?>
                  <?php $i = 0; foreach ($footer_blocks as $footer_block): ?>
                    <div id="footer-<?php print $footer_block->delta ?>-content" class="footer-block<?php if (!$i) print ' first footer-block-selected' ?>"><?php print $footer_block->content ?></div>
                  <?php $i++; endforeach ?>
                <?php endif ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php if($display_footer): ?>
      <div id="footer-wrapper">
        <div id="footer">
          <div class="center-layout">
            <?php print $footer ?>
          </div>
          <div id="footer-bottom">
            <div class="center-layout">
              <?php print $about_footer ?>
              <?php print $page_closure ?>
            </div><!-- end center-layout -->
          </div> <!-- end footer-bottom -->
        </div> <!-- end footer -->
      </div> <!-- end footer-wrapper -->
    <?php endif; ?>
    
    <?php print $closure; ?>
    <?php if (!empty($ndla_textselect)) print $ndla_textselect ?>
    <?php if (!empty($roamauth_url)): ?>
      <script type="text/javascript" src="<?php print $roamauth_url; ?>"></script>
    <?php endif; ?>
    <?php print isset($tns_code) ? $tns_code : NULL ?>
    <?php print isset($jira_button) ? $jira_button : NULL ?>
  </div> <!-- end conainer -->
  </body>
  </html>
