<div id="right-wrapper"<?php print $wrapper_class ?>>
  <?php if ($node_id): ?>
    <ul id="node-actions">
      <?php if ($node_id): ?>
        <?php if($embed): ?>
        <li class='first'>
          <a class='embed' href="javascript:void(0)" onclick="loadEmbedCode(this);" class="embed" title="<?php print t('Embed code for this page');?>"><i class='fa fa-external-link'></i> <?php print t('Embed'); ?></a>
        </li>
        <?php endif; ?>
        <li>
          <a class="easyreader" href="<?php print url('easyreader/' . $node_id) ?>" rel='lightframe'>
            <i class='fa fa-book'></i> <?php print t('Easy Reader') ?>
          </a>
        </li>
        <?php endif ?>
        <?php if ($readspeaker): ?>
          <li>
            <?php print $readspeaker ?>
          </li>
        <li <?php print (!$embed) ? ' class="first"' : ''; ?>>
          <a href="<?php print url('print/' . $node_id) ?>" class="print" title="<?php print t('Print') ?>">
            <i class='fa fa-print'></i> <?php print t('Print') ?>
          </a>
        </li>
      <?php endif ?>
    </ul>
  <?php endif ?>
  <?php if ($related_content || $tools): ?>
    <ul id="right-tabs">
      <?php if ($related_content): ?>
        <li class="current first">
          <a class="js-link related" href="#related"><?php print t('Context') ?></a>
        </li>
      <?php endif ?>
      <?php if ($tools): ?>
        <li<?php if (!$related_content) print ' class="current first"' ?>>
          <a class="js-link tools" href="#tools"><?php print t('Tools') ?></a>
        </li>
      <?php endif ?>
    </ul>
  <?php endif ?>
  <?php if ($related_content): ?>
    <?php print $related_content ?>
  <?php endif ?>
  <?php if ($tools): ?>
    <?php print $tools ?>
  <?php endif ?>
  <?php if ($editor): ?>
    <div id="right-red-folder"></div>
    <?php print $editor ?>
  <?php endif ?>
</div>