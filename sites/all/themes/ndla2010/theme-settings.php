<?php
/**
 * @file
 * @ingroup ndla2010
 */

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array of the additional form widgets.
*/
function phptemplate_settings($saved_settings) {
  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the template.php file.
   */
  $defaults = array(
    'about_url' => 'http://om.ndla.no',
    'contact_url' => '/kontakt',
  	'help_url' => 'http://ndla.no/nb/book/9782',
    'beta_url' => 'http://om.ndla.no/betamerking',
  );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Create the form widgets using Forms API
  $form['about_url'] = array(
    '#type' => 'textfield',
    '#title' => t('About NDLA URL'),
    '#default_value' => $settings['about_url'],
  );
  $form['contact_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact URL'),
    '#default_value' => $settings['contact_url'],
  );
  $form['help_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Help URL'),
    '#default_value' => $settings['help_url'],
  );
  $form['beta_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Beta URL'),
    '#default_value' => $settings['beta_url'],
  );
  
  // Return the additional form widgets
  return $form;
}
?>