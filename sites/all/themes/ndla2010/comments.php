<?php

/**
 *
 * Call this to use this library against another server than feedback.ndla.no
 * @param string $http_root Default is http://feedback.ndla.no
 */
function feedback_set_http_root_url($http_root)
{
	$GLOBALS['feedback_http_root_url'] = $http_root;
}

/**
 *
 * Get the current http-root this library is requesting data from.
 * @return string Default http://feedback.ndla.no
 */
function feedback_get_http_root_url()
{
	if (isset($GLOBALS['feedback_http_root_url']))
		return $GLOBALS['feedback_http_root_url'];
	else
		return 'http://feedback.ndla.no';
}

/**
 *
 * Run a GET-request against
 * @param unknown_type $url
 * @param unknown_type $params
 */
function feedback_json_request($url, $params)
{
	foreach ($params as $name => &$value)
		$value = urlencode($name).'='.urlencode($value);
	unset($value);
	$req = $url.'?'.implode('&', $params);
	$data = file_get_contents($req);
	return json_decode($data);
}

class FeedbackComment {
	/**
	 *
	 * Comment id.
	 * @var string
	 */
	public $id;
	/**
	 *
	 * Comment title.
	 * @var unknown_type
	 */
	public $title;
	/**
	 *
	 * Comment text.
	 * @var string
	 */
	public $text;
	/**
	 *
	 * Comment time (YYYY-MM-DD HH:MM:SS)
	 * @var string
	 */
	public $time;
	/**
	 *
	 * Author name (display-name)
	 * @var string
	 */
	public $author_name;
	/**
	 *
	 * Drupal node-id (WARNING: Unreliable because this is extracted from the url)
	 * @var string
	 */
	public $page_nid;
	/**
	 *
	 * Page url.
	 * @var string
	 */
	public $page_url;
}

/**
 * Get the last comments for a course, or for the entire site
 *
 * @param int $num_comments the number of comments to return
 * @param int $course_id Node id for the course, if no id is specified we retrieve the most recent comments from all courses
 * @returns array consisting of the specified number of comments. Each comments is an object with the properties id, text, time, author_name, page_nid(node id for the page the comment was placed on) and page_url
 */
function feedback_get_recent_comments($num_comments = 5, $course_id = NULL)
{
	$params = array(
		'limit' => $num_comments,
	);
	if ($course_id !== null)
		$params['courseId'] = $course_id;
	$data = feedback_json_request(feedback_get_http_root_url().'/outboard/api/comments.php', $params);
	$objects = array();
	foreach ($data as $comment) {
		$obj = new FeedbackComment();
		$obj->id = $comment->id;
		$obj->title = $comment->title;
		$obj->text = $comment->message;
		$obj->time = $comment->createdDate;
		$obj->author_name = $comment->displayName;
		$obj->page_nid = $comment->nid;
		$obj->page_url = $comment->url;
		$objects[] = $obj;
	}
	return $objects;
}

class FeedbackPage
{
	/**
	 *
	 * Score value on feedback server.
	 * @var integer
	 */
	public $num_likes;
	/**
	 *
	 * Node id in drupal (WARNING: Unreliable because it has to be gathered from the url)
	 * @var string
	 */
	public $page_id;
	/**
	 *
	 * Page url.
	 * @var string
	 */
	public $page_url;
	/**
	 *
	 * Page title.
	 * @var string
	 */
	public $page_title;
	/**
	 *
	 * Comma-separated list of courses.
	 * @var string
	 */
	public $fagkode;
}

/**
 * Get the most liked pages for a course, or for the entire site
 *
 * @param int $num_pages the number of pages to return
 * @param int $course_id Node id for the course, if no id is specified we retrieve the most liked pages for the entire site
 * @returns array consisting of the specified number of pages, ordered by num_likes. Each page is an object with the properties num_likes, page_id(node id for the page the comment was placed on), page_url and if possible page_title.
 */
function feedback_get_most_liked($num_pages = 5, $course_id = NULL)
{
	$params = array(
		'limit' => $num_pages
	);
	if ($course_id !== null)
		$params['courseId'] = $course_id;
	$data = feedback_json_request(feedback_get_http_root_url().'/outboard/api/score.php', $params);
	$objects = array();
	foreach ($data as $page) {
		$obj = new FeedbackPage();
		$obj->num_likes = $page->score;
		$obj->page_id = $page->nid;
		$obj->page_url = $page->url;
		$obj->page_title = $page->title;
		$obj->fagkode = $page->fagkode;
		$objects[] = $obj;
	}
	return $objects;
}
