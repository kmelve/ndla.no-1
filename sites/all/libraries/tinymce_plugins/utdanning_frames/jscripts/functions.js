function init() {
	tinyMCEPopup.resizeToInnerSize();
}


function insertObject(type, title){
	var html = insertDiv(type);
	tinyMCE.execCommand('mceInsertContent', false, html);
	tinyMCEPopup.close();
}
		function insertDiv(class) {

			var selection = tinyMCE.selectedInstance.selection.getSelectedHTML();
			
			innerSelection = removeClass(selection, class);

			if (class && innerSelection == selection) {
				innerSelection = removeDiv(selection);
				content = '<div class="icon '+class+'">' + innerSelection + '</div>';
			} else {
				content = innerSelection;
			}

			return content;

		}
		
		function removeClass(content, class) {

		    var rx = new RegExp("<div class=\"icon "+class+"\">(.*?)</div>","mi");
			
		    rx.exec(content);
		    content = content.replace(rx,RegExp.$1);

		    return content;
			
		}

		function removeDiv(content, class) {

		    var rx = new RegExp("<div .*?>(.*?)</div>","mi");
			
		    rx.exec(content);
		    content = content.replace(rx,RegExp.$1);

		    return content;
		}