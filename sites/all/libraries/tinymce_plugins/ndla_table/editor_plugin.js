(function() {

	tinymce.create("tinymce.plugins.NDLATable", {
	/**
	 * Returns information about the plugin as a name/value array.
	 * The current keys are longname, author, authorurl, infourl and version.
	 *
	 * @returns Name/value array containing information about the plugin.
	 * @type Array 
	 */
	getInfo : function() {
		return {
			longname : "NDLA Table",
			author : "NDLA",
			authorurl : "http://www.ndla.no/",
			infourl : "http://www.ndla.no",
		};
	},
		
	init : function(editor, url) {
	  var o = this;
		editor.addCommand('mceNDLATable', function() { o.showNDLATable(editor, url) });

		editor.addButton('ndla_table', {
			title : 'NDLA Table',
			cmd : 'mceNDLATable',
			image : url + "/img/ndla_table.gif",
		});
		
	},
	
	
	showNDLATable: function(editor, url) {
		var template = new Array();
		template['file']   = url + '/browser.html';
		template['width']  = 650;
		template['height'] = 771;
		
		editor.windowManager.open({
      file: template['file'],
      scrollbars : "yes",
      width : template['width'] + parseInt(editor.getLang('amadeo_draft.delta_width', 0)),
      height : template['height'] + parseInt(editor.getLang('amadeo_draft.delta_height', 0)),
      inline : true
    });

    return true;
	},
});

// Adds the plugin class to the list of available TinyMCE plugins
tinymce.PluginManager.add('ndla_table', tinymce.plugins.NDLATable);
})();
