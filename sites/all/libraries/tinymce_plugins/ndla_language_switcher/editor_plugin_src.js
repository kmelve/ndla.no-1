(function() {
	tinymce.create('tinymce.plugins.NDLALanguageSwitcher', {
		init : function(ed, url) {
			// Register commands
			ed.addCommand('mceNDLALanguageSwitcher', function() {
        var lang1 = 'en';
        var lang2 = 'en';
        if(ed.selection.getNode().className == 'ndla_language_switcher') {
          lang1 = $($(ed.selection.getNode()).find('> *')[0]).attr('lang');
          lang2 = $($(ed.selection.getNode()).find('> *')[1]).attr('lang');
        }
        
				ed.windowManager.open({
					file : url + '/ndla_language_switcher.html',
					width : 200,
					height : 210,
					inline : 1
				},{
					lang1 : lang1,
					lang2 : lang2
				});
			});
			ed.addCommand('mceCreateElementLangSwitcher', function(lang1, lang2) {
        if(ed.selection.getNode().className == 'ndla_language_switcher') {
          $($(ed.selection.getNode()).find('> *')[0]).attr('lang', lang1);
          $($(ed.selection.getNode()).find('> *')[0]).attr('xml:lang', lang1);
          $($(ed.selection.getNode()).find('> *')[1]).attr('lang', lang2);
          $($(ed.selection.getNode()).find('> *')[1]).attr('xml:lang', lang2);
        } else {
          var salt = Math.floor(Math.random()*9999999);
          ed.selection.setContent("<div class='ndla_language_switcher' data-salt='" + salt + "'><div id='lang1_" + salt + "' lang='" + lang1 + "' xml:lang='" + lang1 + "'>Version 1</div><div id='lang2_" + salt + "' lang='" + lang2 + "' xml:lang='" + lang2 + "'>Version 2</div></div>");
        }
				ed.execCommand('mceRepaint');
			});
			ed.addButton('ndla_language_switcher', {title : 'Language Switcher', cmd : 'mceNDLALanguageSwitcher', image: url + '/img/icon.gif'});
		},
		getInfo : function() {
			return {
				longname : 'LanguageSwitcher',
				author : 'Cerpus',
				authorurl : 'http://cerpus.se',
				infourl : 'http://cerpus.se',
				version : '1.0'
			};
		}
	});
	tinymce.PluginManager.add('ndla_language_switcher', tinymce.plugins.NDLALanguageSwitcher);
})();
