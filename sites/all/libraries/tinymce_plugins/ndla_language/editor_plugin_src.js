(function() {
	tinymce.create('tinymce.plugins.NDLALanguage', {
		init : function(ed, url) {
			// Register commands
			ed.addCommand('mceNDLALanguage', function() {

				var applyLangToBlocks = false;
				var blocks = ed.selection.getSelectedBlocks();
				var language = '';
				if (blocks.length === 1) {
		      language = ed.selection.getNode().lang;
				} else {
					tinymce.each(blocks, function(block) {
					  var temp_language = ed.dom.getAttrib(block, 'lang');
					  if(temp_language == '') {
					    language = temp_language;
				    }
				    if(temp_language != language) {
				      language = 'multi';
			      }
					});
					applyLangToBlocks = true;
        }
        if(language == 'multi') {
          language = '';
        }
				ed.windowManager.open({
					file : url + '/ndla_language.html',
					width : 200,
					height : 210,
					inline : 1
				}, {
					applyLangToBlocks : applyLangToBlocks,
					language : language
				});
			});

			ed.addCommand('mceSetElementLang', function(lang, new_section) {
			  if(new_section) {
			    var selection = ed.selection.getContent();
			    ed.selection.setContent("<span lang='" + lang + "' xml:lang='" + lang + "'>" + selection + "</span>");
		    } else {
			    var blocks = ed.selection.getSelectedBlocks();
			    if(blocks.length == 1) {
			      block = ed.selection.getNode();
			      ed.dom.setAttrib(block, 'lang', lang);
    	      ed.dom.setAttrib(block, 'xml:lang', lang);
		      } else {
			      tinymce.each(blocks, function(block) {
			        ed.dom.setAttrib(block, 'lang', lang);
      	      ed.dom.setAttrib(block, 'xml:lang', lang);
		        });
	        }
        }
				ed.execCommand('mceRepaint');
			});
    
		  ed.onNodeChange.add(function(ed, cm, n) {
				cm.setDisabled('ndla_language', n.nodeName === 'BODY');
			});
    
			ed.addButton('ndla_language', {title : 'Language', cmd : 'mceNDLALanguage', image: url + '/img/icon.gif'});
		},

		getInfo : function() {
			return {
				longname : 'Language',
				author : 'Cerpus',
				authorurl : 'http://cerpus.se',
				infourl : 'http://cerpus.se',
				version : '1.0'
			};
		}
	});

	tinymce.PluginManager.add('ndla_language', tinymce.plugins.NDLALanguage);
})();