tinyMCEPopup.requireLangPack();

function init(ed) {
	language = tinyMCEPopup.getWindowArg('language');
  document.getElementById('language').value = language;
}

function updateLanguage() {
  tinyMCEPopup.editor.execCommand('mceSetElementLang', document.getElementById('language').value, document.getElementById('new_section').checked);
  tinyMCEPopup.close();
}

tinyMCEPopup.onInit.add(init);