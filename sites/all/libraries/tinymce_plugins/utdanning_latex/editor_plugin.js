(function() {
	tinymce.create('tinymce.plugins.UtdanningLatex', {
	/**
	 * Returns information about the plugin as a name/value array.
	 * The current keys are longname, author, authorurl, infourl and version.
	 *
	 * @returns Name/value array containing information about the plugin.
	 * @type Array 
	 */
	getInfo : function() {
		return {
			longname : 'Utdanning Insert Latex',
			author : 'js',
			authorurl : 'http://www.utdanning.no',
			infourl : 'http://www.utdanning.no',
			version : "0.1"
		};
	},

	init : function(editor, url) {
		// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceUtdanningLatex');
		editor.addCommand('mceUtdanningLatex', function() {
			/*var selected = tinyMCE.activeEditor.selection.getContent({format : 'text'});
			var nid = '';
			var type = '';
      */
			/** 
			 * We are looking for something which looks like this: [node:123 whatever]
			 * the whatever thing can only contain alphas and must atleast be one character
			 */
		 
		 	/* if(selected) {
				var pattern = /\[node:([0-9]+) ([a-z]+)\]/ig;
				var regexp = new RegExp(pattern);
				var nodeMatch = regexp.exec(selected)

				if(nodeMatch) {
					nid = nodeMatch[1];
					type = nodeMatch[2];
				}
				else {
					alert("For å redigere, merk teksten lik dette: [node:123 collapsed]");
					return;
				}
			}*/
			
			editor.windowManager.open({
					file : url + "/browser.php",
					width : 620,
					height : 590,
					inline : 1
				}, {
					plugin_url : url, // Plugin absolute URL
				}
			);
		});
			
		editor.addButton('utdanning_latex', {
			title : 'Utdanning Insert Latex',
			cmd : 'mceUtdanningLatex',
			image : url + "/img/utdanning_latex.gif",
		});
		
	},

	/**
	 * Executes a specific command, this function handles plugin commands.
	 *
	 * @param {string} editor_id TinyMCE editor instance id that issued the command.
	 * @param {HTMLElement} element Body or root element for the editor instance.
	 * @param {string} command Command name to be executed.
	 * @param {string} user_interface True/false if a user interface should be presented.
	 * @param {mixed} value Custom value argument, can be anything.
	 * @return true/false if the command was executed by this plugin or not.
	 * @type
	 */
	execCommand : function(editor_id, element, command, user_interface, value) {
		function insertDiv() {
			tinyMCE.openWindow({
					file : '../../plugins/utdanning_latex/browser.php',
					width : 480,
					height : 400,
				},
				{
					editor_id : editor_id,
					inline : "yes"
				});

				return true;
		}
	},

});

// Adds the plugin class to the list of available TinyMCE plugins
tinymce.PluginManager.add('utdanning_latex', tinymce.plugins.UtdanningLatex);
})();
