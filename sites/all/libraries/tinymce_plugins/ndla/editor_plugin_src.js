/**
 * $Id: editor_plugin_src.js 201 2007-02-12 15:56:56Z spocke $
 *
 * @author Moxiecode
 * @copyright Copyright � 2004-2007, Moxiecode Systems AB, All rights reserved.
 */

/* Import plugin specific language pack */
tinyMCE.importPluginLanguagePack('ndla', 'en');

var TinyMCE_NDLAPlugin = {
	getInfo : function() {
		return {
			longname : 'ndla ting',
			author : 'VMdata',
			
			
			version : tinyMCE.majorVersion + "." + tinyMCE.minorVersion
		};
	},

	/**
	 * Returns the HTML contents of the column_right and column_full
	 */
	getControlHTML : function(cn) {
		switch (cn) {
			case "columnright":
				return tinyMCE.getButtonHTML(cn, 'lang_right_desc', '{$pluginurl}/images/col-right.gif', 'mceColumnRight');

			case "columnfull":
				return tinyMCE.getButtonHTML(cn, 'lang_full_desc', '{$pluginurl}/images/col-full.gif', 'mceColumnFull');

			case "columndefault":
				return tinyMCE.getButtonHTML(cn, 'lang_full_desc', '{$pluginurl}/images/col-default.gif', 'mceColumnDefault');

			case "class-quote":
				return tinyMCE.getButtonHTML(cn, 'lang_full_desc', '{$pluginurl}/images/quote.gif', 'mceClassQuote');

			case "class-object":
				return tinyMCE.getButtonHTML(cn, 'lang_full_desc', '{$pluginurl}/images/object.gif', 'mceClassObject');

			case "class-image":
				return tinyMCE.getButtonHTML(cn, 'lang_full_desc', '{$pluginurl}/images/image.gif', 'mceClassImage');

			case "class-facts":
				return tinyMCE.getButtonHTML(cn, 'lang_full_desc', '{$pluginurl}/images/facts.gif', 'mceClassFacts');

			case "class-related":
				return tinyMCE.getButtonHTML(cn, 'lang_full_desc', '{$pluginurl}/images/related.gif', 'mceClassRelated');

			case "class-excercise":
				return tinyMCE.getButtonHTML(cn, 'lang_full_desc', '{$pluginurl}/images/excercise.gif', 'mceClassExcercise');

			case "class-summary":
				return tinyMCE.getButtonHTML(cn, 'lang_full_desc', '{$pluginurl}/images/summary.gif', 'mceClassSummary');

		}

		return "";
	},

	/**
	 * Executes the mceInsertDate command.
	 */
	execCommand : function(editor_id, element, command, user_interface, value) {

		function insertCol(class) {

			var selection = tinyMCE.selectedInstance.selection.getSelectedHTML();

			innerSelection = removeClass(selection, class);

			if (class && innerSelection == selection) {
				innerSelection = removeCol(selection);
				content = '<div class="'+class+'">' + innerSelection + '</strong>';
			} else {
				innerSelection = removeCol(selection);
				content = innerSelection;
			}

			return content;

		}

		function removeCol(content) {

		    var rx1 = new RegExp("<div class=\"right\">(.*?)</div>","mi");
		    var rx2 = new RegExp("<div class=\"full\">(.*?)</div>","mi");
			
		    rx1.exec(content);
		    content = content.replace(rx1,RegExp.$1);

		    rx2.exec(content);
		    content = content.replace(rx2,RegExp.$1);

		    return content;

		}
		
		function insertDiv(class) {

			var selection = tinyMCE.selectedInstance.selection.getSelectedHTML();
			
			innerSelection = removeClass(selection, class);

			if (class && innerSelection == selection) {
				innerSelection = removeDiv(selection);
				content = '<div class="'+class+'">' + innerSelection + '</strong>';
			} else {
				content = innerSelection;
			}

			return content;

		}
		
		function removeClass(content, class) {

		    var rx = new RegExp("<div class=\""+class+"\">(.*?)</div>","mi");
			
		    rx.exec(content);
		    content = content.replace(rx,RegExp.$1);

		    return content;
			
		}

		function removeDiv(content, class) {

		    var rx = new RegExp("<div .*?>(.*?)</div>","mi");
			
		    rx.exec(content);
		    content = content.replace(rx,RegExp.$1);

		    return content;
		}

		// Handle commands
		switch (command) {
			case "mceColumnRight":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertCol('right'));
				return true;
				
			case "mceColumnFull":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertCol('full'));
				return true;
				
			case "mceColumnDefault":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertCol());
				return true;

			case "mceClassQuote":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertDiv('quote'));
				return true;

			case "mceClassObject":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertDiv('object'));
				return true;

			case "mceClassImage":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertDiv('image'));
				return true;

			case "mceClassFacts":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertDiv('facts'));
				return true;

			case "mceClassRelated":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertDiv('related'));
				return true;

			case "mceClassExcercise":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertDiv('excercise'));
				return true;

			case "mceClassSummary":
				tinyMCE.execInstanceCommand(editor_id, 'mceReplaceContent', false, insertDiv('summary'));
				return true;

		}

		// Pass to next handler in chain
		return false;
	}
};

tinyMCE.addPlugin("ndla", TinyMCE_NDLAPlugin);