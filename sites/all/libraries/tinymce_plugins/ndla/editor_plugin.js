/* Import plugin specific language pack */
tinymce.PluginManager.requireLangPack('ndla'); 

(function() {
	tinymce.create('tinymce.plugins.NDLA', {

	getInfo : function() {
		return {
			longname : 'ndla ting',
			author : 'VMdata',			
			version : tinyMCE.majorVersion + "." + tinyMCE.minorVersion
		};
	},

	init : function(ed, url) {
		o = this;
		
		ed.addCommand('mceColumnRight', function() {
				tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertCol(o, 'right'));
				return true;
		});
		
		ed.addCommand('mceColumnFull', function() {
			tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertCol(o, 'full'));
		});

		ed.addCommand('mceColumnDefault', function() {
			 tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertCol(o));
		});
		
		ed.addCommand('mceClassQuote', function() {
			tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertDiv(o, 'quote'));
		});

		ed.addCommand('mceClassObject', function() {
			tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertDiv(o, 'object'));
		});

		ed.addCommand('mceClassImage', function() {
			tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertDiv(o, 'image'));
		});			

		ed.addCommand('mceClassFacts', function() {
			tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertDiv(o, 'facts'));
		});			

		ed.addCommand('mceClassRelated', function() {
			tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertDiv(o, 'related'));
		});			

		ed.addCommand('mceClassExcercise', function() {
			tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertDiv(o, 'excercise'));
		});			

		ed.addCommand('mceClassSummary', function() {
			tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceReplaceContent', false, o.insertDiv(o, 'summary'));
		});

		ed.addButton('columnfull', {
			title : 'columnfull',
			cmd : 'mceColumnFull',
			image : url + '/images/col-full.gif'
		});
		
		ed.addButton('columnright', {
			title : 'columnright',
			cmd : 'mceColumnRight',
			image : url + '/images/col-right.gif'
		});
		
		ed.addButton('columndefault', {
			title : 'columndefault',
			cmd : 'mceColumnDefault',
			image : url + '/images/col-default.gif'
		});
		
		ed.addButton('class-quote', {
			title : 'class-quote',
			cmd : 'mceClassQuote',
			image : url + '/images/quote.gif'
		});
		
		ed.addButton('class-object', {
			title : 'class-object',
			cmd : 'mceClassObject',
			image : url + '/images/object.gif'
		});

		ed.addButton('class-image', {
			title : 'class-image',
			cmd : 'mceClassImage',
			image : url + '/images/image.gif'
		});

		ed.addButton('class-facts', {
			title : 'class-facts',
			cmd : 'mceClassFacts',
			image : url + '/images/facts.gif'
		});

		ed.addButton('class-related', {
			title : 'class-related',
			cmd : 'mceClassRelated',
			image : url + '/images/related.gif'
		});

		ed.addButton('class-excercise', {
			title : 'class-excercise',
			cmd : 'mceClassExcercise',
			image : url + '/images/excercise.gif'
		});

		ed.addButton('class-summary', {
			title : 'class-summary',
			cmd : 'mceClassSummary',
			image : url + '/images/summary.gif'
		});
	},


	insertCol: function(space, className) {
		var selection = tinyMCE.activeEditor.selection.getContent();
		
		innerSelection = space.removeClass(selection, className);

		if (className && innerSelection == selection) {
			innerSelection = o.removeCol(selection);
			content = '<div class="'+className+'">' + innerSelection + '</strong>';
		} else {
			innerSelection = o.removeCol(selection);
			content = innerSelection;
		}

		return content;
	},

	removeClass: function(content, classToRemove) {
		var rx = new RegExp("<div class=\""+classToRemove+"\">(.*?)</div>","mi");
		rx.exec(content);
		content = content.replace(rx,RegExp.$1);
		return content;
	},

	removeCol: function(content) {
		//var rx1 = new RegExp("<div class=\"right\">(.*)</div>(.*?)</div>","mi");
		var rx1 = new RegExp("<div class=\"right\">(.*)","mi");
		//var rx2 = new RegExp("<div class=\"full\">(.*)</div>(.*?)</div>","mi");
		var rx2 = new RegExp("<div class=\"full\">(.*)","mi");

		rx1.exec(content);
		content = content.replace(rx1,RegExp.$1);

		rx2.exec(content);
		content = content.replace(rx2,RegExp.$1);

		return content;
	},
	
	insertDiv: function(space, className) {
		var selection = tinyMCE.activeEditor.selection.getContent();
		innerSelection = space.removeClass(selection, className);

		if (className && innerSelection == selection) {
			innerSelection = space.removeDiv(selection);
			content = '<div class="'+className+'">' + innerSelection + '</strong>';
		} else {
			content = innerSelection;
		}

		return content;
	},

	removeDiv: function(content) {
		var rx = new RegExp("<div .*?>(.*?)</div>","mi");
			
		rx.exec(content);
		content = content.replace(rx,RegExp.$1);

		return content;
	},  

});

// Adds the plugin class to the list of available TinyMCE plugins
tinymce.PluginManager.add('ndla', tinymce.plugins.NDLA);
})();