# The NDLA Open Source Project #

NDLA is a project to offer free digital learning learning resources for secondary schools in Norway.

A part of the project is to head an open source project platform for development of and access to such learning resources. 

This is the codebase that powers the main site, [ndla.no](http://ndla.no).
It's based on the [Pressflow](http://www.pressflow.org) distribution of [Drupal](http://drupal.org).

Unfortunately we have no installation profiles or distribution setup for a 'quick start' to development.

If you want to know more about NDLA, you can check out [om.ndla.no](http://om.ndla.no).